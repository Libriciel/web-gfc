#!/bin/bash
set -Eeo pipefail

# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
		exit 1
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}


	cp -a /var/www/webgfc/app/Config/database.php.default.docker /var/www/webgfc/app/Config/database.php
	cp -a /var/www/webgfc/app/webroot/files/wsdl/webgfc.wsdl.default /var/www/webgfc/app/webroot/files/wsdl/webgfc.wsdl
	sed -i "s|DB_HOST|${DB_HOST}|g" /var/www/webgfc/app/Config/database.php
	sed -i "s|DB_PORT|${DB_PORT}|g" /var/www/webgfc/app/Config/database.php
	sed -i "s|DB_USER|${DB_USER}|g" /var/www/webgfc/app/Config/database.php
	sed -i "s|DB_PASSWORD|${DB_PASSWORD}|g" /var/www/webgfc/app/Config/database.php
	sed -i "s|DB_NAME|${DB_NAME}|g" /var/www/webgfc/app/Config/database.php

	sed -i "s|PUBLIC_HOSTNAME|${WEBGFC_URL}|g" /var/www/webgfc/app/webroot/files/wsdl/webgfc.wsdl

	sed -i "s|webgfc.git|${WEBGFC_URL}|g" /var/www/webgfc/app/Console/Command/WsClientShell.php

	sed -i "s|webgfc.x.x.org|${WEBGFC_URL}|g" /var/www/webgfc/app/Config/webgfc.inc
	sed -i "s|<<URL_SERVEUR_GEDOOO>>|${WEBGFC_GEDOOO_HOST}|g" /var/www/webgfc/app/Config/webgfc.inc
	sed -i "s|<<URL_SERVEUR_CLOUDOOO>>|${WEBGFC_CLOUDOOO_HOST}|g" /var/www/webgfc/app/Config/webgfc.inc
# debug#
#echo $(cat /var/www/webgfc/app/Config/database.php)


# if variable DB_HOST definie =
#if [[ ! -z "${DB_HOST}" ]]; then
#      while ! pg_isready -h ${DB_HOST} -p ${DB_PORT} > /dev/null 2> /dev/null; do
#      echo "Postgres database attempt..."
#      echo "Wait 5s..."
#      sleep 5
#done
#      echo "Postgres ready - Let's Roll"
#                cd /var/www/webgfc
#                ./lib/Cake/Console/cake --app app AdminTools install
#                ./lib/Cake/Console/cake --app app AdminTools clear_cache
#                ./lib/Cake/Console/cake --app app AdminTools -o www-data fix_fs_rights
#fi

if [[ ! -z "${DB_HOST}" ]]; then
    while ! pg_isready -h ${DB_HOST} -p ${DB_PORT} > /dev/null 2> /dev/null; do
    echo "Postgres database attempt..."
    echo "Wait 5s..."
    sleep 5
done
		echo "Postgres ready - Let's Roll"
fi

cd /var/www/webgfc
pg_isready -h ${DB_HOST} -p ${DB_PORT}
psql postgres://postgres@$DB_HOST -c "CREATE ROLE webgfc WITH ENCRYPTED PASSWORD 'webgfc';"
psql postgres://postgres@$DB_HOST -c "ALTER ROLE webgfc WITH CREATEDB LOGIN SUPERUSER;"
psql postgres://postgres@$DB_HOST -c "ALTER ROLE webgfc WITH CREATEDB;"
psql postgres://postgres@$DB_HOST -c "CREATE DATABASE webgfc OWNER ${POSTGRES_USER};"

	# Création de la BDD de test
	psql postgres://$POSTGRES_USER:$DB_PASSWORD@$DB_HOST -c "CREATE DATABASE test OWNER ${POSTGRES_USER};"
	psql postgres://$POSTGRES_USER:$DB_PASSWORD@$DB_HOST -c "GRANT ALL PRIVILEGES ON DATABASE test TO ${POSTGRES_USER};"

	psql postgres://$POSTGRES_USER:$DB_PASSWORD@$DB_HOST -c "\l"
	psql postgres://$POSTGRES_USER:$DB_PASSWORD@$DB_HOST -c "\d"

./lib/Cake/Console/cake --app app AdminTools install
./lib/Cake/Console/cake --app app AdminTools installtest
./lib/Cake/Console/cake --app app AdminTools clear_cache
#./lib/Cake/Console/cake --app app AdminTools -o www-data fix_fs_rights

# lancement des tests unitaires
./lib/Cake/Console/cake test app All --stderr --configuration app/Config/phpunit.xml --coverage-text --coverage-clover app/coverage/coverage-clover.xml --log-junit app/coverage/junit.xml

	# clear out the relevant envrionment variables (so that stray "phpinfo()" calls don't leak secrets from our code)
	for e in "${envs[@]}"; do
		unset "$e"
	done

exec "$@"
