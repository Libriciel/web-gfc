FROM php:7.2-apache-buster
MAINTAINER Arnaud AUZOLAT <a.auzolat@libriciel.coop>

# fix for pg_client...
RUN mkdir -p /usr/share/man/man1
RUN mkdir -p /usr/share/man/man7
RUN set -ex; \

apt-get update -qq; \
apt-get install -y --no-install-recommends\
    libjpeg-dev \
    libpng-dev \
    libc-client-dev \
    libkrb5-dev \
    libldb-dev \
    libldap2-dev \
    libssh2-1 \
    libssh2-1-dev \
    libxml2-dev \
    locales \
    unzip \
    wget \
    make \
    git \
    ruby-full \
    compass-blueprint-plugin \
    libpq-dev \
    apt-utils \
    postgresql-client \
    zlib1g-dev ; \
    \
    docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
    docker-php-ext-install gd zip soap bcmath pdo pdo_pgsql; \
    docker-php-ext-configure imap --with-kerberos --with-imap-ssl; \
    docker-php-ext-install imap; \
\

    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
    rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        mcrypt \
        libssh2-1-dev
  # && docker-php-ext-install -j$(nproc) iconv mcrypt \
  # && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
  # && docker-php-ext-install -j$(nproc) gd

# Gestion des locales
RUN sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen
RUN echo 'LANG="fr_FR.UTF-8"'>/etc/default/locale
RUN dpkg-reconfigure --frontend=noninteractive locales
RUN update-locale LANG=fr_FR.UTF-8

ENV XDEBUG_MODE=coverage


# Extension LDAP
RUN ln -s /usr/lib/x86_64-linux-gnu/libldap.so /usr/lib/libldap.so && \
    ln -s /usr/lib/x86_64-linux-gnu/liblber.so /usr/lib/liblber.so && \
    docker-php-ext-install ldap
# Extension SSH2 (voir https://medium.com/php-7-tutorial/solution-how-to-compile-php7-with-ssh2-f23de4e9c319)
#RUN cd /tmp && \
#    wget https://github.com/Sean-Der/pecl-networking-ssh2/archive/php7.zip && \
#    unzip php7.zip && \
#    cd /tmp/pecl-networking-ssh2-php7 && \
#    phpize && \
#    ./configure && \
#    make && \
#    make install && \
#    docker-php-ext-enable ssh2


RUN cd /tmp && \
 	wget https://pecl.php.net/get/ssh2-1.3.1.tgz && \
 	tar -xvf ssh2-1.3.1.tgz && \
 	cd /tmp/ssh2-1.3.1 && \
 	phpize && \
 	./configure && \
	make && \
	make install && \
	docker-php-ext-enable ssh2

# PHP
RUN echo "include_path=".:/usr/local/lib/php:/usr/local/lib/composer"" > /usr/local/etc/php/conf.d/webgfc.ini
RUN echo "session.save_path=/var/lib/php/session/" >> /usr/local/etc/php/conf.d/webgfc.ini

#Pour PHPUnit
RUN echo "include_path=".:/usr/local/lib/php:/usr/local/lib/composer:/usr/local/bin"" > /usr/local/etc/php/conf.d/webgfc.ini

RUN { \
echo 'expose_php = Off'; \
echo 'date.timezone = « Europe/Paris »'; \
echo 'max_execution_time = 600'; \
echo 'post_max_size = 100M'; \
echo 'upload_max_filesize = 100M'; \
echo 'memory_limit = 512M'; \
echo 'session.gc_maxlifetime = 14400'; \
	} >> /usr/local/etc/php/conf.d/webgfc.ini


# Installation de composer
RUN cd /tmp/ && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php --install-dir=/usr/local/bin && \
    mv /usr/local/bin/composer.phar /usr/local/bin/composer

#Sessions PHP
RUN mkdir -p /var/lib/php/session/ && \
    chown www-data: /var/lib/php/session

# Source
COPY --chown=www-data:www-data ./ /var/www/webgfc/
RUN chmod +x /var/www/webgfc/lib/Cake/Console/cake
RUN chmod -R 777 /var/www/webgfc/app/tmp
RUN cd /var/www/webgfc/app/Config && cp -a core.php.default core.php && cp -a email.php.default email.php && cp -a webgfc.inc.default webgfc.inc

# Modules Apache
RUN a2enmod \
    dav \
    dav_fs \
    expires \
    rewrite \
    ssl

ENV PATH="${PATH}:/usr/local/lib/composer/vendor/bin"

EXPOSE 80

#RUN gem install rake compass sass bootstrap-sass
#RUN cd /var/www/webgfc/app/webroot/sass &&  compass clean && compass compile

COPY ./docker-entrypoint.sh /usr/local/bin/
RUN chmod a+x /usr/local/bin/docker-entrypoint.sh
RUN rm -rf /var/www/html && ln -s /var/www/webgfc /var/www/html
WORKDIR /var/www/webgfc

#Composer
#RUN composer update
ENV PATH="${PATH}:/var/www/webgfc/app/Vendor/bin/"

#phpUnit
RUN composer require --dev phpunit/phpunit 5.*

#phpmd
RUN composer require --dev phpmd/phpmd

#phpcs
RUN composer require --dev squizlabs/php_codesniffer:^1.5
RUN composer require --dev cakephp/cakephp-codesniffer:^1.0.0
RUN chmod -R 777 /var/www/webgfc/app/Vendor
RUN /var/www/webgfc/app/Vendor/bin/phpcs --config-set installed_paths "/var/www/webgfc/app/Vendor/cakephp/cakephp-codesniffer"

# ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["apache2-foreground"]
