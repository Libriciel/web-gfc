#!:/bin/sh
echo "set variables"
set -e
sed -i -e 's,GEDOOO_WSDL_CONFIG,'$GEDOOO_WSDL_CONFIG',' /var/www/webgfc/app/Config/webgfc.inc
sed -i -e 's/CLOUDOOO_HOST_CONFIG/'$CLOUDOOO_HOST_CONFIG'/' /var/www/webgfc/app/Config/webgfc.inc
sed -i -e 's/CLOUDOOO_PORT_CONFIG/'$CLOUDOOO_PORT_CONFIG'/' /var/www/webgfc/app/Config/webgfc.inc

sed -i -e 's/POSTGRES_HOST_CONFIG/'$POSTGRES_HOST_CONFIG'/g' /var/www/webgfc/app/Config/database.php
sed -i -e 's/POSTGRES_USER/'$POSTGRES_USER'/g' /var/www/webgfc/app/Config/database.php
sed -i -e 's/POSTGRES_PASSWORD/'$POSTGRES_PASSWORD'/g' /var/www/webgfc/app/Config/database.php
sed -i -e 's/POSTGRES_DB/'$POSTGRES_DB'/g' /var/www/webgfc/app/Config/database.php
