<?php

/**
 *
 * web-GFC configuration file
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt CeCiLL v2
 */
/**
 * Configuration GEDOOo
 */
Configure::write('PROTOCOLE_DL', 'vnd.sun.star.webdav');
Configure::write('GENERER_DOC_SIMPLE', false);
Configure::write('USE_GEDOOO', true);
Configure::write('GEDOOO_WSDL', 'GEDOOO_WSDL_CONFIG');

/**
 * Configuration cloudooo
 */
/* Configuration pour la conversion de format des documents */
Configure::write('CLOUDOOO_HOST', 'CLOUDOOO_HOST_CONFIG');
Configure::write('CLOUDOOO_PORT', 'CLOUDOOO_PORT_CONFIG'); //par défaut: 8011

/**
 * Configuration scan
 */
Configure::write('scan.remote.ftp', false);
Configure::write('scan.remote.ssh', false);
Configure::write('scan.remote.smb', false);
Configure::write('scan.imap', false);


/**
 * Configuration de la détection de terme similaire (contacts)
 */
Configure::write('levenshteinThreshold', 4);

/**
 * Configuration de la valeur du champ de type select dans les métadonnées
 * @param: integer id
 */
Configure::write('Selectvaluemetadonnee.id', 4);


/** Configuration du connecteur avec le i-parpaheur * */
/**
 * Parapheur (Signature électronique, délégation)
 */
/* Activer le parapheur */
Configure::write('USE_PARAPHEUR', true);
/* Service utilisé pour le parapheur ('PASTELL', 'IPARAPHEUR', ...) */
Configure::write('PARAPHEUR', 'IPARAPHEUR');


/* Nom du client webgfc */
Configure::write('WEBGFCCLIENT', 'VILLE DE BARLEDUC');

/* Utilisation des notificatons ou non */
Configure::write('SMTP_USE', false);

/* URL de Webgfc (pour mails) */
Configure::write('WEBGFC_URL', 'https://webgfc17.webgfc.fr');

/* Affichage ou non du bloc Document disponibles pour insérer dans le modèle (flux sortant) */
// false = angouleme
Configure::write('DOC_DEFAULT', true);

/**
 * GED (Gestion électronique documentaire)
 */
/* Activer la GED */
Configure::write('Parapheur.id', 1);
Configure::write('Ged.id', 2);
Configure::write('USE_GED', false);
/* Service utilisé pour la GED ('PASTELL' ou 'GED') */
Configure::write('GED', 'CMIS');

/* Suite au ticket 9380, mise en condition de l'affichage des onglets Accusé de réception et Composants de réponse */
/* Utilisé aussi pour l'envoi à la signature manuelle */
//false = bar le duc
Configure::write('DOC_FROM_GFC', true);

/* Désactivation de la possibilité d'ajouter des profils perosnnalisés */
// true = angouleme
Configure::write('ProfilPerso.actif', true);
Configure::write('Impression.PDFvalue', '"iParapheur_impression_dossier.pdf"');

/* Configuration par boîte mail utilisant du IMAP */
Configure::write('Conf.Imap', '/novalidate-cert');

Configure::write('PRODUIT', 'web-GFC');

/* Ajout pour les cas où les utilisateurs souhaitent que les flux en copie soient modifiables */
Configure::write('Copie.modified', false);
Configure::write('Sansorganisme.id', 656);

Configure::write('CONVERSION_TYPE', 'CLOUDOOO');

/* * ** Configuration CAS **** */
Configure::write('AuthManager.Authentification.use', false);
Configure::write('AuthManager.Authentification.type', '');

Configure::write('AuthManager.Cas.host', 'auth.webgfc.fr');
Configure::write('AuthManager.Cas.port', '443');
Configure::write('AuthManager.Cas.uri', '/cas');
Configure::write('AuthManager.Cas.cert_path', APP . 'Config/cert_cas/client.pem');
Configure::write('AuthManager.Cas.proxy', 'https://auth.webgfc.fr');

Configure::write('Suffixe.connexion', '');

Configure::write('Sanscontact.id', 3209);

// Affichage de la bannette Les flux de mes services
Configure::write('Bannette.messervices', false);

// Utilisation de la restriction des types/sous-types selon les droits
Configure::write('Use.AuthRights', true);

// Paramètre permettant de ne pas charger les droits en édition d'un utilisateur
Configure::write('Afficher.HabilitationsUser', false);

// Paramètre utilisé, dans le cas d'une authentification CAS avec multi-collectivités côté web-GFC
Configure::write('Cas.Multicoll', false);

// Paramétrage pour le compte de la SAERP
Configure::write('Conf.SAERP', false);

// Paramétrage pour les circuits fantômes
Configure::write('Soustype.Fantome', 'S0');

// Paramétrage pour récupérer l'URL de l'outil de statistiques
Configure::write('Url.Stat', 'http://webgfc.stats');

// Paramétrage pour ne pas supprimer les flux en copie une foisle flux clôturé (false = Angoulême)
Configure::write('Vidage.Bannettecopie', false);

// Paramétrage pour détecter la méta-donnée qui prend pour nom Montant HT
// utilisée uniquement par la SAERP
Configure::write('MetadonnneMontant.id', 5 );

// Paramétrage pour détecter la méta-donnée qui prend pour nom Montant HT
// utilisée uniquement par la SAERP
Configure::write('MetadonnneRar.id', 2 );

// Paramètre pour définir si on utilise le logo sur la page d'accueil ou non // default false
Configure::write('Conf.logo', false);

// Paramètre pour définir que le mail de refus ne sera adressé qu'à l'initiateur au lieu de tout le monde
Configure::write('Refus.InitiateurOnly', true);

// Paramètre pour définir si on affiche la pop up permettant de valider la diffusion
// du changement d'adresse des contacts suite à la modification de l'adresse de l'organisme
// valeur possible @param yes ou no
Configure::write('AdresseOrganisme.DiffuseContact', 'no');

// Paramétrage pour savoir si on utilise les gabarits ou les modèles liés aux sous-types
Configure::write('Conf.Gabarit', false);

// Paramétrage pour savoir si lorsqu'on intègre des contacts, on prend en compte les organismes ou non
Configure::write('Contactshell.Avecorganisme', true);

// Paramétrage pour savoir si la configuraitn concerne un CD
Configure::write('Conf.CD', false);

// Paramétrage pour connaître le n° du département client
Configure::write('CD', '');

// Paramétrage pour permettre de retrouver les flux adressés en copie mais clos
Configure::write('Recherche.FluxCopie', true);

// Paramétrage pour savoir si on crée le flux et les contacts dans une GRC
// true = angoulême avec Localeo
Configure::write('Webservice.GRC', false);

// Paramétrage pour définir si la base DN doit être concaténée si targetDn est !=
// false = cogitis
Configure::write('Conf.Ldap', true);

// PAramètres pour savoir si on utilise les flux internes et donc si on autorise les accès aux modèles de réponse pour ces types de flux
// default = true
Configure::write('Flux.interne', true);

// PAramètres pour savoir si on masque le bloc "Document(s) à insérer dans le modèle (webdav)"
// default = true
Configure::write('Affichage.Webdav', true);

// Paramètres pour afficher TOUS les profils disponibles lors d'un commentaire privé
// default = false
Configure::write('Commentaire.AllDesktops', true);

// Paramétrages pour la GRC Localeo
Configure::write('GRC.cityId', 73);
Configure::write('GRC.canalId', 4);
Configure::write('GRC.servId', 4128);
Configure::write('GRC.sqrtId', 12177);
Configure::write('GRC.qrtId', 125);

// Paramètre pour définir si on supprime ou non un document du parapheur une fois ce dernier récupéré
Configure::write('Parapheur.Archive', false);

// Paramètre pour définir si lors de la modification d'un bureau on ajoute ou non les nouveaux profils
// aux bannettes passées, celles avnt le jour de l'ajout
// true = on laisse comme avant, on ajoute quelque soit le passif
// false = on n'ajoute pas les profils aux bannettes
Configure::write('Bancontenu.AddDesktop', true);

// Paramètre afin de pouvoir définir un DNS différent de celui utilisé pour la connexion LDAP
Configure::write('Ldap.SuffixeDns', '');

// Paramétrage pour détecter la méta-donnée qui prend pour nom 41 - Accusé de réception envoyé
// utilisée uniquement par angoulême
Configure::write('MetadonnneAREnvoye.id', 5 );
?>
