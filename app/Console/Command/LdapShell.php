<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 *
 * CreateFromFiles shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class LdapShell extends XShell {

    public $uses = array(
        'Authentification',
        'ConnecteurLdap'
    );
	/**
	 *
	 * @var type
	 */
	public $files = array();

//public $tasks = array('LdapManager.Group','LdapManager.User');
	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

        $this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));


		Configure::write('conn', $this->params['connection']);

        ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->Connecteur = ClassRegistry::init('Connecteur');
        $this->Connecteur->useDbConfig = $this->_conn;

	}



	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {

        $optionParser = parent::getOptionParser();
        $optionParser->description(__("Outils d'administration"));
        $optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));

		return $optionParser;
	}


    /**
     *
     * @return string
     * @version 4.3
     */
    public function syncLdap() {
        $this->writeConfigLdap();

        $connecteurLdap = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%LDAP%'
                ),
                'contain' => false
            )
        );

        try {
            if ($connecteurLdap['Connecteur']['use_ldap']) {
                if ($this->dispatchShell("LdapManager.ldap_manager group_sync -c {$this->params['connection']}")) {
                    $output = $this->dispatchShell("LdapManager.ldap_manager user_update  -c {$this->params['connection']}");
//                    return Cron::MESSAGE_FIN_EXEC_SUCCES . $output;
                } else {
                    throw new Exception('Synchronisation des groupes echoués');
                }
            } else {
//                return Cron::MESSAGE_FIN_EXEC_SUCCES . __('Connecteur LDAP non configuré');
            }
        } catch (Exception $e) {
            $output = " Exception levée : \n" . $e->getMessage();
//            $this->log($e->getTraceAsString(), 'error');
        }

        return $output;
    }

    /**
     *
     * @param type $id
     * @return string
     * @throws Exception
     */
    public function writeConfigLdap() {
        $connecteurLdap = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%LDAP%'
                ),
                'contain' => false
            )
        );

        $connecteurLdap['Connecteur']['fields']['User'] = array(
            'username' => $connecteurLdap['Connecteur']['ldap_username'],
            'note' => $connecteurLdap['Connecteur']['ldap_note'],
            'nom' => $connecteurLdap['Connecteur']['ldap_nom'],
            'prenom' => $connecteurLdap['Connecteur']['ldap_prenom'],
            'email' => $connecteurLdap['Connecteur']['ldap_mail'],
            'telfixe' => $connecteurLdap['Connecteur']['ldap_numtel'],
            'telmobile' => $connecteurLdap['Connecteur']['ldap_portable'],
            'active' => $connecteurLdap['Connecteur']['ldap_active']
        );

        unset ($connecteurLdap['Connecteur']['ldap_username']);
        unset ($connecteurLdap['Connecteur']['ldap_note']);
        unset ($connecteurLdap['Connecteur']['ldap_nom']);
        unset ($connecteurLdap['Connecteur']['ldap_prenom']);
        unset ($connecteurLdap['Connecteur']['ldap_mail']);
        unset ($connecteurLdap['Connecteur']['ldap_numtel']);
        unset ($connecteurLdap['Connecteur']['ldap_portable']);
        unset ($connecteurLdap['Connecteur']['ldap_active']);


        Configure::write('LdapManager.Ldap', $connecteurLdap['Connecteur']);
//        Configure::write('AuthManager.Authentification', $authentification['Authentification']);
    }
}
