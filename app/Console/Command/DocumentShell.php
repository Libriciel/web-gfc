<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 *
 * ContentToFile shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class DocumentShell extends XShell {


	/**
	 *
	 * @var type
	 */
	public $files = array();


	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @var type
	 */
	public $Document;

	/**
	 *
	 * @var type
	 */
	public $Courrier;

	public $headers = array(
		"Référence du flux",
		"Nom du document",
		"Document principal ?"
	);

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

		$this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));


		Configure::write('conn', $this->params['connection']);

		ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->Document = ClassRegistry::init('Document');
		$this->Document->useDbConfig = $this->_conn;
		$this->Courrier = ClassRegistry::init('Courrier');
		$this->Courrier->useDbConfig = $this->_conn;

	}


	/**
	 *
	 */
	public function main() {

		$this->out('<info>Recherche des documents en cours ...</info>');
		$this->XProgressBar->start(1);

		$success = $this->deleteDocumentFromDisk();

		$this->hr();
		if ($success) {
			$this->out('<success>Opération terminée avec succès.</success>');
		} else {
			$this->out('<error>Opération terminée avec erreur(s).</error>');
		}
		$this->hr();

	}

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
		$this->Document = ClassRegistry::init('Document');

		$optionParser = parent::getOptionParser();
		$optionParser->description(__("Outils d'administration"));
		$optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));


		return $optionParser;
	}

	function getDirContents($dir, &$results = array()) {
		$files = scandir($dir);
		foreach ($files as $key => $value) {
			$path = realpath($dir . DS . $value);
			if (!is_dir($path)) {
				$results[] = $path;
			} else if ($value != "." && $value != ".." && $value != "import") {
				$this->getDirContents($path, $results);
				$results[] = $path;
			}
		}
		return $results;

	}
	/**
	 *
	 * @param type $filename
	 * @param type $marche_id
	 * @param type $foreign_key
	 * @return boolean
	 * sudo ./lib/Cake/Console/cake --app app ContentToFile -c webgfc_angouleme_backup
	 */
	public function deleteDocumentFromDisk() {
		$this->Document->useDbConfig = $this->_conn;
		$this->Courrier->useDbConfig = $this->_conn;

		$listOfDirectory = WORKSPACE_PATH . DS . $this->_conn;
		if( is_dir($listOfDirectory) ) {
			$results = $this->getDirContents($listOfDirectory);

			$this->out('<info>Recherche des documents...</info>');
			$success[] = true;
			foreach ($results as $key => $path) {
				$pathValidUTF8 = ! (false === mb_detect_encoding($path, 'UTF-8', true));
				if (!is_dir($path) && strpos($path, '.json') === false && $pathValidUTF8 ) {
					$docToDeleteFromDisk = $this->Document->find(
						'first',
						[
							'fields' => [
								'Document.id',
								'Document.courrier_id'
							],
							'conditions' => [
								'Document.path' => $path
							],
							'contain' => false,
							'recursive' => -1
						]
					);

					// Si le chemin du document n'est pas présent en BDD, on détache le fichier du disque
					if (empty($docToDeleteFromDisk['Document']['id'])) {
$this->log($path);
						$success[] = unlink($path);
					}
				} else if (is_dir($path)) {
					// Si le répertoire est vide, on supprime le répertoire
					$emptyDirectory = count(scandir($path));
					if ($emptyDirectory == 2) {
						rmdir($path);
					}
				}
				else if(!is_dir($path) && strpos($path, '.json') === false && !$pathValidUTF8) {
$this->log($path);
					unlink($path);
				}
			}
		}
		else {
			$success[] = false;
			$this->out("<warning>Le répertoire " . $listOfDirectory ." est introuvable.</warning>");
		}

		return !in_array(false, $success, true);

	}
}
