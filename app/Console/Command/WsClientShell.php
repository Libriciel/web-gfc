<?php

/**
 *
 * WsClient shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class WsClientShell extends Shell {

	/**
	 *
	 * @var type
	 */
	private $_client;

	/**
	 *
	 * @var type
	 */
	private $collectivite_id;

	/**
	 *
	 * @var type
	 */
	private $_promptUrl = false;

	/**
	 *
	 * @var type
	 */
	public $soapOptions = array(
		'exceptions' => false,
		'login' => '',
		'password' => '',
		'trace' => true,
        'collectiviteId' => '',
        'soustypeId' => '',
        'contact' => array(),
        'titre' => '',
        'objet' => '',
        'fcontenu' => '',
        'courriergrc_id' => ''
	);

	/**
	 *
	 * @var type
	 */
	public $url = '<< WEBGFC_URL >>/files/wsdl/webgfc.wsdl';

	/**
	 *
	 * @var type
	 */
	private $_start;

	/**
	 *
	 * @var type
	 */
	private $_end;

	/**
	 *
	 */
	private function _scritpStart() {
		list($usec, $sec) = explode(' ', microtime());
		$this->_start = (float) $sec + (float) $usec;
	}

	/**
	 *
	 */
	private function _scritpEnd() {
		list($usec, $sec) = explode(' ', microtime());
		$this->_end = (float) $sec + (float) $usec;
		$elapsed_time = round($this->_end - $this->_start, 5);
		$this->out('<info>Elapsed time : ' . $elapsed_time . 's</info>');
	}

	/**
	 *
	 * @param type $name
	 */
	private function _showFunctionName($name) {
		$this->out('<info>Fonction</info>');
		$this->out($name);
	}

	/**
	 *
	 * @param type $var
	 */
	private function _showFeedBack($var) {
		$this->out("<success>" . var_export($var, true) . "</success>");
	}

	/**
	 *
	 */
	private function _processParams() {
		if (isset($this->params['url']) && !empty($this->params['url'])) {
			$this->url = $this->params['url'];
		}
	}

	/**
	 *
	 * @return type
	 */
	private function _initCredentials() {
		$return = false;
		if (!empty($this->params['user']) && !empty($this->params['password']) && !empty($this->params['collectivite'])) {
			//TODO: getOptionParser -> recupérer les arguments de la commande
			$this->soapOptions['login'] = $this->params['user'];
			$this->soapOptions['password'] = $this->params['password'];

			if (is_int($this->params['collectivite'])) {
				$this->collectivite_id = $this->params['collectivite'];
				$return = true;
			} else if (is_string($this->params['collectivite'])) {
				$colls = ClassRegistry::init('Collectivite');
				$coll = $colls->find('first', array('fields' => array('Collectivite.id'), 'conditions' => array('Collectivite.conn' => $this->params['collectivite'])));
				if (!empty($colls)) {
					$this->collectivite_id = $coll['Collectivite']['id'];
					$return = true;
				}
			}
		}

            $this->soapOptions['collectiviteId'] = 4;
            $this->soapOptions['soustypeId'] = 2;
            $this->soapOptions['contact'] = array(
                'type' => 'citoyen',
                'name' => 'Arnaud AUZOLAT',
                'nom' => 'AUZOLAT',
                'prenom' => 'Arnaud'
            );
            $this->soapOptions['titre'] = 'Demande de candidature';
            $this->soapOptions['objet'] = 'Bonjour, suite à votre ouverture de poste, je candidate. Veuillez trouver ci-joint mon CV. Cordialement,';
            $this->soapOptions['fcontenu'] = 'CV.pdf';
            $this->soapOptions['courriergrcId'] = 6880;
		return $return;
	}

	/**
	 *
	 */
	private function _initUrl() {
		if ($this->_promptUrl) {
			$this->url = $this->in('url : ');
		}
	}

	/**
	 *
	 */
	public function startup() {
		$this->_scritpStart();
		// première étape : désactiver le cache lors de la phase de test
		ini_set("soap.wsdl_cache_enabled", "0");
		$this->_processParams();


		if ($this->_initCredentials() || $this->command == "getGFCCollectivites") {
			$this->_initUrl();
			$this->_client = new SoapClient($this->url, $this->soapOptions);

			$this->out('<info>Parmètres du client</info>');
			$this->out('url : ' . $this->url);
			$this->out('options SOAP : ' . var_export($this->soapOptions, true));
			$this->out('ID Collectivite : ' . var_export($this->collectivite_id, true));
		} else {
			$this->out('<error>Veuillez préciser votre collectivité, identifiant de connexion et mot de passe !</error>');
			die;
		}
	}

	/**
	 *
	 */
	public function echoTest() {
		$msg = 'Bonjour, ceci est un test';
		if (isset($this->args[0]) && !empty($this->args[0])) {
			$msg = $this->args[0];
		}

		$this->_showFunctionName(__FUNCTION__);
		$this->out('<info>Paramètres de la fonction : </info>');
		$this->out('msg = "' . $msg . '"');

		$return = $this->_client->echotest($msg);

		$this->_showFeedBack($return);
		$this->_scritpEnd();
	}

	/**
	 *
	 */
	public function getGFCCollectivites() {
		$this->hr();
		$this->out("Test de la fonction getGFCCollectivites : ");
		$this->hr();

		$return = $this->_client->getGFCCollectivites();
		$this->_showFeedBack($return);
		$this->_scritpEnd();
	}

	/**
	 *
	 * @param type $collectivite_id
	 */
	public function getGFCTypes() {
		$this->hr();
		$this->out("Test de la fonction getGFCTypes");
		$this->hr();

		$return = $this->_client->getGFCTypes($this->collectivite_id);
		$this->_showFeedBack($return);
		$this->_scritpEnd();
	}

	/**
	 *
	 * @param type $type_id
	 * @param type $stype_name
	 */
	public function getGFCSoustypes($type_id, $stype_name) {
		$this->hr();
		$this->out("Test de la fonction getGFCSoustypes");
		$this->hr();
		$this->out('Paramètres de la fonction : ');
		$this->out('ID Type = "' . $type_id . '"');
		$this->out('Nom du soustypes = "' . $stype_name . '"');
		$this->hr();


		$return = $this->_client->getGFCSoustypes(1, "Candidatures");

		$result = array();
		foreach ($return as $type) {
			$result[$type->anyType[0]] = $type->anyType[1];
		}

		print_r($return);
	}

	/**
	 *
	 */
	public function getGFCContact() {

	}

	/**
	 *
	 * @param type $stype_id
	 * @param type $contact_id
	 * @param string $titre
	 * @param string $objet
	 * @param type $contenu_fichier
	 * @param type $user_creator_username
	 * @param type $multi
	 */
//	public function createCourrier($titre = "", $objet = "", $contenu_fichier = "", $multi = 0, $contact = "") {
    public function createCourrier($collectiviteId ="" , $soustypeId="", $contact = array(), $titre="", $objet="", $fcontenu=null, $username="", $courriergrcId="", $contactId="", $organismeId="") {
		$this->hr();
		$this->out("Test de la fonction createCourrier\n");
		$this->hr();
		$this->out('Paramètres de la fonction : ');
		$this->out('ID Collectivité = "' . $this->collectivite_id . '"');
		$this->hr();

//		for ($i = 0; $i <= $multi; $i++) {
//			if ($titre == "") {
//				$titre = "WebGFC_phpcli titre par défaut" . "_" . mktime();
//			}
//			if ($objet == "") {
//				$objet = "WebGFC_phpcli objet par défaut" . "_" . mktime();
//			}
//
//			if ($contenu_fichier == "") {
//				$fcontenu = "text " . mktime();
//			}
            $collectiviteId = 74;
            $soustypeId = 737;
            $contact = array(
                'type' => 'citoyen',
                'name' => 'Arnaud AUZOLAT',
                'nom' => 'AUZOLAT',
                'prenom' => 'Arnaud'
            );
//            $contact = array(
//                'type' => 'organisme',
//                'name' => 'Libiricel'
//            );
            $contact = json_encode( $contact);
            $titre = 'Demande de candidature';
            $objet = 'Bonjour, suite à votre ouverture de poste, je candidate. Veuillez trouver ci-joint mon CV. Cordialement,';
            $fcontenu = 'CV.pdf';
            $courriergrcId = 6880;
            $username = 'Guiolet';


//			$return = $this->_client->createCourrier($this->collectivite_id, $this->params['soustype'], $contact, $titre, $objet, $fcontenu, $this->soapOptions['login']);
			$return = $this->_client->createCourrier($this->collectivite_id, $soustypeId, $contact, $titre, $objet, $fcontenu, $username, $courriergrcId, $contactId="", $organismeId="");
			print_r($return);
//		}
	}

	/**
	 *
	 * @param type $quantity
	 */
	public function simulScanMass() {
		$this->hr();
		$this->out("Test de la fonction scanMass\n");
		$this->hr();
		$this->out('Paramètres de la fonction : ');
		$this->out('ID Collectivité = "' . $this->collectivite_id . '"');
		$this->out('Nombre de flux générés = "' . $this->params['nbscan'] . '"');
		$this->hr();

		$quantity = $this->params['nbscan'];

		$return = array();
		for ($i = 1; $i <= $quantity; $i++) {



			$return[] = $this->_client->scanMass($this->collectivite_id, "text" . $i . '_' . date('YMd_His'), $this->soapOptions['login']);
			echo $i . '/' . $quantity . ($i <= $quantity ? ' ... ' : '');

			if (!empty($this->params['taillelot']) && $i % $this->params['taillelot'] == 0 && $i < $quantity) {
				echo 'waiting ....';
			}
		}
		print_r($return);
	}

	/**
	 *
	 */
	public function getStatut() {
		$this->hr();
		$this->out("Test de la fonction getStatut\n");
		$this->hr();
		$this->out('Paramètres de la fonction : ');
		$this->out('ID Collectivité = "' . $this->collectivite_id . '"');
		$this->hr();

		$return = $this->_client->getStatut($this->in('Courrier id: '));
		print_r($return);
	}

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
		$actions = array(
			'echoTest' => 'Test du webservice. Renvoie une chaine de caractère (usage: cake WsClient echoTest <ma_chaine_de_test>)',
			'getGFCCollectivites' => 'Recupération de la liste des collectivités présentes au sein de l\'application. Renvoie la liste sous forme de tableau',
			'getGFCTypes' => 'Recupération de la liste des types d\'une des collectivités présentes au sein de l\'application. Renvoie la liste sous forme de tableau',
			'getGFCSoutypes' => 'Recupération de la liste des soustypes d\'un type. Renvoie la liste sous forme de tableau',
			'getGFCContacts' => 'Recupération de la liste des contacts présents au sein de l\'application. Renvoie la liste sous forme de tableau',
			'simulScanMass' => 'Simulation du scan de masse.',
			'createCourrier' => 'Création d\'un flux.',
			'createContact' => 'Création d\'un contact.',
			'getStatut' => 'Renvoie les informations concernant un flux sous forme de tableau.',
		);
		ksort($actions);
		$optionParser = parent::getOptionParser();
		$optionParser->description(__("web-GFC Webservice Client"));
		foreach ($actions as $action => $description) {
			$optionParser->addSubcommand($action, array('help' => $description));
		}


		$optionParser->addOption('url', array(
			'short' => 'u',
			'help' => __('Permet de préciser l\'url du webservice à contacter (si non spécifié, on utilise la valeur par defaut).')
		));
		$optionParser->addOption('user', array(
			'short' => 'U',
			'help' => __('Permet de préciser l\'utilisateur autorisé à se connecter.')
		));
		$optionParser->addOption('password', array(
			'short' => 'W',
			'help' => __('Permet de préciser le mot de passe.')
		));
		$optionParser->addOption('collectivite', array(
			'short' => 'C',
			'help' => __('Permet de préciser l\identifiant ou le suffixe de connexion de la collectivité auprès de laquelle l\'utilisateur est enregistré.')
		));
		$optionParser->addOption('type', array(
			'short' => 'T',
			'help' => __('Permet de préciser le type.')
		));
		$optionParser->addOption('soustype', array(
			'short' => 'S',
			'help' => __('Permet de préciser le soustype')
		));
		$optionParser->addOption('nbscan', array(
			'short' => 'n',
			'help' => __('Permet de préciser le nombre de flux générés par la simulation de scan de masse'),
			'default' => 10
		));
		$optionParser->addOption('taillelot', array(
			'short' => 'l',
			'help' => __('Permet de préciser le nombre de lots de flux générés par la simulation de scan de masse')
		));

		return $optionParser;
	}


    /**
	 *
	 * @param type $stype_id
	 * @param type $contact_id
	 * @param string $titre
	 * @param string $objet
	 * @param type $contenu_fichier
	 * @param type $user_creator_username
	 * @param type $multi
	 */
	public function createContact($data, $courrierId ) {
		$this->hr();
		$this->out("Test de la fonction createContact\n");
		$this->hr();
		$this->out('Paramètres de la fonction : ');
		$this->out('ID Collectivité = "' . $this->collectivite_id . '"');
		$this->hr();

        $return = $this->_client->createContact($this->collectivite_id, $data, $courrierId );
        print_r($return);
	}

    /**
	 *
	 *
	 */
	public function getGFCDesktopsmanagers() {
		$this->hr();
		$this->out("Test de la fonction getGFCDesktopsmanagers");
		$this->hr();

		$return = $this->_client->getGFCDesktopsmanagers($this->collectivite_id);
		$this->_showFeedBack($return);
		$this->_scritpEnd();
	}
}

?>
