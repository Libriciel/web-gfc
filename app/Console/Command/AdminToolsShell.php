<?php

App::uses('Cache', 'Cache');

App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
App::uses('ComponentCollection', 'Controller');

App::uses('AuthComponent', 'Controller/Component');
App::uses('AclComponent', 'Controller/Component');

App::uses('Model', 'AppModel');
App::uses('DbAcl', 'Model');
App::uses('AclNode', 'Model');
App::uses('DataAclNode', 'DataAcl.Model');
App::uses('Aco', 'Model');
App::uses('Aro', 'Model');

App::uses('ConnectionManager', 'Model');

App::uses('AclSync', 'AuthManager.Lib');

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 *
 * AdminTools shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class AdminToolsShell extends AppShell {

	/**
	 *
	 * @var type
	 */
	private $User;

	/**
	 *
	 * @var type
	 */
	private $Collectivite;

	/**
	 *
	 * @var type
	 */
	private $Profil;

	/**
	 *
	 * @var type
	 */
	private $Desktop;

	/**
	 *
	 * @var type
	 */
	private $_showHelp;

	/**
	 *
	 * @var type
	 */
	private $_bkpdir;

	/**
	 *
	 * @var type
	 */
	private $_bkpdate;

	/**
	 *
	 * @var type
	 */
	private $_fsOwner = "www-data";

	/**
	 *
	 * @var type
	 */
	private $_fsProfil = "www-data";

	/**
	 *
	 * @var type
	 */
	private $_conn;

	const SUCCESS = 0;

	/**
	 *
	 * @var type
	 */
	private $Courrier;

	/**
	 *
	 * @var type
	 */
	private $Soustype;

	/**
	 *
	 *
	 */
	public function startup() {
		parent::startup();

		$this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		if ($this->_conn != 'default') {

            ClassRegistry::init(('User'));
            $this->User = new User();
            $this->User->setDataSource($this->_conn);

			ClassRegistry::init(('Desktop'));
			$this->Desktop = new Desktop();
			$this->Desktop->setDataSource($this->_conn);

			ClassRegistry::init(('Profil'));
			$this->Profil = new Profil();
			$this->Profil->setDataSource($this->_conn);

			$collection = new ComponentCollection();
			$this->Acl = new AclComponent($collection);
			$this->Acl->startup(new AppController());

			$this->Aco = $this->Acl->Aco;
			$this->Aco->setDataSource($this->_conn);

			$this->Aro = $this->Acl->Aro;
			$this->Aro->setDataSource($this->_conn);


			ClassRegistry::init(('Courrier'));
			$this->Courrier = new Courrier();
			$this->Courrier->setDataSource($this->_conn);

			ClassRegistry::init(('Connecteur'));
			$this->Connecteur = new Connecteur();
			$this->Connecteur->setDataSource($this->_conn);

			ClassRegistry::init(('Journalevent'));
			$this->Journalevent = new Journalevent();
			$this->Journalevent->setDataSource($this->_conn);

			ClassRegistry::init(('Service'));
			$this->Service = new Service();
			$this->Service->setDataSource($this->_conn);
		} else {
			ClassRegistry::init(('Collectivite'));
			$this->Collectivite = new Collectivite();
			$this->Collectivite->setDataSource($this->_conn);
		}
	}

	/**
	 *
	 */
	public function main() {
		$this->_showHelp = false;

		list($usec, $sec) = explode(' ', microtime());
		$script_start = (float) $sec + (float) $usec;

		if (!empty($this->args[0])) {
			if ($this->args[0] == "clear_cache") {
				$this->_clear_cache();
			} else if ($this->args[0] == "hash_pass") {
				$this->_hash_pass($this->args[1]);
			} else if ($this->args[0] == "rights") {
				$this->_rights();
			} else if ($this->args[0] == "clear_logs") {
				$this->_clear_logs();
			} else if ($this->args[0] == "clean_tree_tables") {
				if (!empty($this->args[1]) && $this->args[1] == "with_acl") {
					$this->_clean_tree_tables(true);
				} else {
					$this->_clean_tree_tables();
				}
			} else if ($this->args[0] == "clean") {
				$this->_clean();
			} else if ($this->args[0] == "reset") {
				$this->_reset();
			} else if ($this->args[0] == "install") {
				if (isset($this->args[1]) && isset($this->args[2]) ) {
					$this->_install($this->args[1], $this->args[2]);
				}
				else {
					$this->out("<error>You forget to give the username or the password.</error>");
				}
			} else if ($this->args[0] == "installtest") {
				$this->_installtest();
			} else if ($this->args[0] == "add") {
				$this->_add();
			} else if ($this->args[0] == "fix_fs_rights") {
				$this->_fixFsRigths();
			} else if ($this->args[0] == "backup") {
				$this->_backup();
			} else if ($this->args[0] == "list_collectivites") {
				$this->_getListColl();
			} else if ($this->args[0] == "repair_addressbook_slugs") {
				$this->_repairAdressBookSlugs();
			} else if ($this->args[0] == "delegation") {
				$this->_delegation();
			} else if ($this->args[0] == "set_light_rights") {
				$this->_setLightRights();
			} else if ($this->args[0] == "retard") {
				$this->_fluxEnRetard();
			} else if ($this->args[0] == "update_rights") {
				$this->_updateRights();
			} else if ($this->args[0] == "notif") {
				$this->_fluxNotifies();
			} else if ($this->args[0] == "copie") {
				$this->_fluxEnCopie();
			} else if ($this->args[0] == "clos") {
				$this->_fluxClos();
			} else if ($this->args[0] == "cert") {
				$this->_getCert();
			} else if ($this->args[0] == "bloqueparapheur") {
				if (isset($this->args[1])) {
					$this->_fluxBloqueDansParapheur($this->args[1]);
				} else {
					$this->_fluxBloqueDansParapheur();
				}
			} else if ($this->args[0] == "soustypeiparapheur") {
				$this->_updateSoustypeParapheur();
			} else if ($this->args[0] == "visaiparapheur") {
				$this->_updateSoustypeVisaParapheur();
			} else if ($this->args[0] == "purgejournal") {
                if (isset($this->args[1]) && isset($this->args[2]) ) {
					$this->_purgeJournal($this->args[1], $this->args[2]);
				} else {
					$this->_purgeJournal();
				}
			} else if ($this->args[0] == "servicecreator") {
				$this->_serviceCreator();
			} else if( $this->args[0] == "purgewebdav" )  {
				if (isset($this->args[1])) {
					$this->_purgeWebdavByColl($this->args[1]);
				} else {
					$this->_purgeWebdavByColl();
				}
			}
			else {
                $this->_showHelp = true;
            }
		} else {
			$this->_showHelp = true;
		}

		if ($this->_showHelp) {
			$this->_displayHelp('');
		} else {
			list($usec, $sec) = explode(' ', microtime());
			$script_end = (float) $sec + (float) $usec;

			$elapsed_time = round($script_end - $script_start, 5);
			$this->out('Elapsed time : ' . $elapsed_time . 's');
			$this->out("<warning>Don't forget to check right access on APP/tmp.</warning>");
			$this->hr();
		}
	}

	private function _getListColl() {

		$qd = array(
			'fields' => array(
				'Collectivite.name',
				'Collectivite.id',
				'Collectivite.conn',
				'Collectivite.active'
			),
			'recursive' => -1,
			'order' => array(
				'Collectivite.active',
				'Collectivite.name'
			)
		);
		$colls = $this->Collectivite->find('all', $qd);

		$this->out('Liste des collectivités paramétrées :');
		$this->out('identifiant - statut - nom - connexion');
		foreach ($colls as $coll) {
			$this->out($coll['Collectivite']['id'] . ' - ' . ($coll['Collectivite']['active'] ? 'activée' : 'désactivée') . ' - ' . $coll['Collectivite']['name'] . ' - ' . $coll['Collectivite']['conn']);
		}
	}

	/**
	 *
	 */
	private function _fixFsRigths() {
		if (!empty($this->params['owner'])) {
			$this->_fsOwner = $this->params['owner'];
		}
		if (!empty($this->params['group'])) {
			$this->_fsProfil = $this->params['group'];
		}
		$cmdCakeChown = "sudo chown " . $this->_fsOwner . ":" . $this->_fsProfil . " " . CAKE . " -Rvf";
		$cmdCakeChmod = "sudo chmod u+rw,g+rw,o-w " . CAKE . " -Rvf";
		$cmdAppChown = "sudo chown " . $this->_fsOwner . ":" . $this->_fsProfil . " " . ROOT . " -Rvf";
		$cmdAppChmod = "sudo chmod u+rw,g+rw,o-w " . ROOT . " -Rvf";
		passthru(escapeshellcmd($cmdCakeChown));
		passthru(escapeshellcmd($cmdCakeChmod));
		passthru(escapeshellcmd($cmdAppChown));
		passthru(escapeshellcmd($cmdAppChmod));
	}

	/**
	 *
	 * @return string
	 */
	private function _backupDb($connName = "") {
		$conns = ConnectionManager::enumConnectionObjects();
		if ($connName == "") {
			$connName = $this->User->setDataSource($this->_conn);
		}
		$conn = $conns[$connName];
		$filename = '';
		if ($conn) {

			$tmpBkpSchema = ROOT . "/webgfc_bd_" . $connName . "_schema_" . $this->_bkpdate . ".sql";
			$tmpBkpData = ROOT . "/webgfc_bd_" . $connName . "_data_" . $this->_bkpdate . ".sql";

			$cmdSchema = "pg_dump --host " . $conn['host'] . " --username " . $conn['login'] . (!empty($conn['port']) ? " --port " . $conn['port'] : "") . " -s " . $conn['database'] . " -f " . $tmpBkpSchema;
			$cmdData = "pg_dump --host " . $conn['host'] . " --username " . $conn['login'] . (!empty($conn['port']) ? " --port " . $conn['port'] : "") . " --insert --data-only " . $conn['database'] . " -f " . $tmpBkpData;

			passthru(escapeshellcmd($cmdSchema));
			passthru(escapeshellcmd($cmdData));

			$filename = $this->_bkpdir . "/webgfc_bd_" . $connName . "_" . $this->_bkpdate . ".tar.bz2";
			$cmd = "tar --create --bzip --remove-files --file " . $filename . " " . $tmpBkpSchema . " " . $tmpBkpData;
			passthru(escapeshellcmd($cmd));
		}
		return $filename;
	}

	/**
	 *
	 * @return type
	 */
	private function _backupApp() {
		$filename = $this->_bkpdir . "/webgfc_app_" . $this->_bkpdate . ".tar.bz2";
		$cmd = "tar --create --verbose --bzip --file " . $filename . " --exclude=.svn --exclude=*.tar.bz2 " . ROOT;
		passthru(escapeshellcmd($cmd));
		return($filename);
	}

	/**
	 *
	 */
	private function _backup() {
		$this->_bkpdir = ROOT;
		if (!empty($this->params['backup_direcory']) && is_dir($this->params['backup_directory'])) {
			$this->_bkpdir = $this->params['backup_direcory'];
		}
		$this->_bkpdate = date('Ymd_His');
		if ($this->params['backup_mode'] == 'full') {
			$filenames = array($this->_backupApp());
			$conns = ConnectionManager::enumConnectionObjects();
			foreach ($conns as $kConn => $conn) {
				$filenames[] = $this->_backupDb($kConn);
			}
			$filename = $this->_bkpdir . "/webgfc_full_" . $this->_bkpdate . ".tar.bz2";
			$cmd = "tar --create --bzip --remove-files --file " . $filename;
			foreach ($filenames as $file) {
				$cmd .= " " . $file;
			}
			passthru(escapeshellcmd($cmd));
			$this->hr(1);
			$this->out("fichier généré : " . $filename);
			$this->hr(1);
		} else if ($this->params['backup_mode'] == 'db') {
			$filename = $this->_backupDb();
			$this->hr(1);
			$this->out("fichier généré : " . $filename);
			$this->hr(1);
		} else if ($this->params['backup_mode'] == 'all_db') {
			$conns = ConnectionManager::enumConnectionObjects();
			foreach ($conns as $kConn => $conn) {
				$filename = $this->_backupDb($kConn);
				$this->hr(1);
				$this->out("fichier généré : " . $filename);
				$this->hr(1);
			}
		} else if ($this->params['backup_mode'] == 'app') {
			$filename = $this->_backupApp();
			$this->hr(1);
			$this->out("fichier généré : " . $filename);
			$this->hr(1);
		}
	}

	/**
	 *
	 */
	private function _install( $username, $password) {
		$this->hr(1);
		$this->out('Install starts. Please wait ...');
		$this->hr(1);

		$conns = ConnectionManager::enumConnectionObjects();
		$filename = APP . 'Config/sql/001_admin_schema.sql';
		$filenameTemplate = APP . 'Config/sql/webgfc_template.sql';
		putenv("PGPASSWORD={$conns['default']['password']}");

		$apitoken = bin2hex(random_bytes(30));
		try {
			$cmd = 'psql -U ' . $conns['default']['login'] . ' -p ' . $conns['default']['port'] . ' -h ' . $conns['default']['host'] . ' -f ' . $filename . ' ' . $conns['default']['database'];
			$cmdTemplate = 'psql -U ' . $conns['template']['login'] . ' -p ' . $conns['template']['port'] . ' -h ' . $conns['template']['host'] . ' -f ' . $filenameTemplate . ' ' . $conns['template']['database'];

			$this->hr(1);
			$this->out('<warning>Résultat de la commande</warning>');
			passthru(escapeshellcmd($cmd), $success);
			passthru(escapeshellcmd($cmdTemplate), $success);
			$this->hr(1);


			if ($success == self::SUCCESS) {
				$this->out('Base de données superadmin créée');
				$createUser = false;
				$this->out('Information du compte superadmin :');
//				$username = 'admin';
//				$password = bin2hex(random_bytes(15));
				$hashPassword = Security::hash($password, 'sha256', true);
				$nom = 'Super';
				$prenom = 'Administrateur';
				$mail = 'admin@localhost.fr';

				$user = array(
					'User' => array(
						'username' => $username,
						'password' => $hashPassword,
						'nom' => $nom,
						'prenom' => $prenom,
						'mail' => $mail,
						'apitoken' => $apitoken
					)
				);

				ClassRegistry::init(('User'));
				$this->User = new User();
				$this->User->setDataSource($this->_conn);
				$this->User->create($user);
				if ($this->User->save()) {
					$createUser = true;
				}

				$this->hr(1);
				$this->out('Install done.');
				if ($createUser) {
					$this->out("<info>L'utilisateur a été créé. username = $username password = $password </info>");
				} else {
					$this->out("<error>L'utilisateur n'a pas été créé.</error>");
				}
			} else {
				$this->out('Aucune base créée');
			}
			$this->hr(1);
			return $apitoken;
		}
		catch (Exception $e) {
			$this->out("<info>L'application a déjà été installée.</info>");
			return $apitoken;
		}
	}

    /**
	 *
	 */
	private function _installtest() {
		$this->hr(1);
		$this->out('Install test starts. Please wait ...');
		$this->hr(1);

		$conns = ConnectionManager::enumConnectionObjects();
        // Insertion du fichier SQL contenant les infos de la BDD "modèle"
        $filename = APP . 'Config/sql/webgfc_template.sql';

        putenv('PGPASSWORD=' . $conns['template']['password']);
        $Contact = ClassRegistry::init('Contact');
		$Contact->setDataSource('test');
        $cmdCreate = 'psql -U ' . $conns['test']['login'] . ' -p ' . $conns['test']['port'] . ' -h ' . $conns['test']['host'] . ' -f ' . $filename . ' ' . strtolower( $conns['test']['database'] );

		$this->hr(1);
		$this->out('<warning>Résultat de la commande</warning>');
        passthru(escapeshellcmd($cmdCreate),$success);
		$this->hr(1);
		if( $success == self::SUCCESS ) {

            $this->hr(1);
            $this->out('Install test done.');
		} else {
			$this->out( 'Aucune base créée' );
		}
		$this->hr(1);
	}

	/**
	 * Normalize simplified Rights table (fields 'model' and 'foreign_key')
	 */
	private function _normalizeRights() {
		$return = true;
		$Right = ClassRegistry::init('Right');
		$Right->setDataSource($this->_conn);
        $str = '';

		$rights = $Right->find('all');
		foreach ($rights as $right) {
			$aro = $this->Aro->find('first', array('conditions' => array('Aro.id' => $right['Right']['aro_id'])));
//debug($right['Right']['aro_id']);
			$alreadyNormalized = true;
			if( !empty( $aro ) ) {
				if (!empty($right['Right']['aro_id'])) {
					$str = 'Normalize ' . $aro['Aro']['model'] . ' ' . $aro['Aro']['foreign_key'];
				}

				if (empty($right['Right']['model'])) {
					$right['Right']['model'] = $aro['Aro']['model'];
					$alreadyNormalized = false;
				}
				if (empty($right['Right']['foreign_key'])) {
					$right['Right']['foreign_key'] = $aro['Aro']['foreign_key'];
					$alreadyNormalized = false;
				}
			}

			if ($alreadyNormalized) {
				$str .= ': <info>Already normalized</info>';
			} else {
				$Right->create();
				$saved = $Right->save($right);
				if (!empty($saved)) {
					$str .= ': <success>Normalized</success>';
				} else {
					$str .= ': <error>Error</error>';
					$return = false;
				}
			}
			$this->out($str);
		}
		return $return;
	}

	/**
	 *
	 */
	private function _repairAdressBookSlugs() {

		/**
		 * Contact slugs
		 */
		$Contact = ClassRegistry::init('Contact');
		$Contact->setDataSource($this->_conn);


		$this->hr();
		$this->out('Repairing Contact slugs');
		$this->hr();

		$Contact->begin();
		$validContact = array();

		$contacts = $Contact->find('all', array('fields' => array('Contact.id', 'Contact.name'), 'recursive' => -1));
		foreach ($contacts as $contact) {
			$Contact->id = $contact['Contact']['id'];
			$slug = strtolower(Inflector::slug($contact['Contact']['name']));
			$ok = $Contact->saveField('slug', $slug);
			$this->out($contact['Contact']['name'] . ' : ' . $slug . ' - ' . ($ok ? '<success>ok</success>' : '<error>nok</error>'));
			$validContact[] = $ok;
		}

		if (!empty($validContact) && !in_array(false, $validContact, true)) {
			$Contact->commit();
			$this->out('<success>Contact repair slug done</success>');
		} else {
			$Contact->rollback();
			$this->out('<warning>Contact repair slug ended with error (rollback)</warning>');
		}



		/**
		 * Contactinfo slugs
		 */
		$Contactinfo = ClassRegistry::init('Contactinfo');
		$Contactinfo->setDataSource($this->_conn);


		$this->hr();
		$this->out('Repairing Contactinfo slugs');
		$this->hr();

		$Contactinfo->begin();
		$validContactinfo = array();

		$contactinfos = $Contactinfo->find('all', array('fields' => array('Contactinfo.id', 'Contactinfo.nom', 'Contactinfo.prenom'), 'recursive' => -1));
		foreach ($contactinfos as $contactinfo) {
			$Contactinfo->id = $contactinfo['Contactinfo']['id'];
			$slug = strtolower(Inflector::slug($contactinfo['Contactinfo']['nom'] . ' ' . $contactinfo['Contactinfo']['prenom']));
			$ok = $Contactinfo->saveField('slug', $slug);
			$this->out($contactinfo['Contactinfo']['nom'] . ' ' . $contactinfo['Contactinfo']['prenom'] . ' : ' . $slug . ' - ' . ($ok ? '<success>ok</success>' : '<error>nok</error>'));
			$validContactinfo[] = $ok;
		}

		if (!empty($validContactinfo) && !in_array(false, $validContactinfo, true)) {
			$Contactinfo->commit();
			$this->out('<success>Contactinfo repair slug done</success>');
		} else {
			$Contactinfo->rollback();
			$this->out('<warning>Contactinfo repair slug ended with error (rollback)</warning>');
		}
	}

	/**
	 *
	 */
	private function _reindexRights() {
		$return = true;
		$Right = ClassRegistry::init('Right');
		$Right->setDataSource($this->_conn);
        $str = '';
		$rights = $Right->find('all');
		foreach ($rights as $right) {
			$aro = $this->Aro->find('first', array('conditions' => array('Aro.model' => $right['Right']['model'], 'Aro.foreign_key' => $right['Right']['foreign_key'])));

            if( !empty($aro) && !empty($right['Right']['aro_id'])) {
                $str = 'Reindexing ' . $aro['Aro']['model'] . ' ' . $aro['Aro']['foreign_key'];
                $right['Right']['aro_id'] = $aro['Aro']['id'];
            }

			$valid = array();
			$Right->begin();
			$valid[] = $Right->delete($right['Right']['id']);

			$Right->create();
			$saved = $Right->save($right);
			$valid[] = !empty($saved);

			if (!in_array(false, $valid, true)) {
				$Right->commit();
				$str .= ': <success>Ok</success>';
				$return = true;
			} else {
				$Right->rollback();
				$str .= ': <error>Error</error>';
			}
			$this->out($str);
		}
		return $return;
	}

	/**
	 * Call of DbToolsShell delete_db method ( Delete ACL Table's content (MySQL: autoincrement reseted, PostgreSQL: serial not reseted) )
	 *
	 * @return boolean
	 */
	private function _delete_db() {
		$ret = false;
		$this->out(__('**** ACL tables delete starts. Please wait ... ****'));
		$this->Aro->query("TRUNCATE table acos CASCADE;");
		$this->Aro->query("ALTER SEQUENCE acos_id_seq RESTART WITH 1;");
		$this->Aro->query("TRUNCATE table aros CASCADE;");
		$this->Aro->query("ALTER SEQUENCE aros_id_seq RESTART WITH 1;");
		$this->Aro->query("TRUNCATE table aros_acos CASCADE;");
		$this->Aro->query("ALTER SEQUENCE aros_acos_id_seq RESTART WITH 1;");
		$this->out(__('==== ACL tables delete done. ===='));
		$ret = true;
		return $ret;
	}

	/**
	 * Call of DbToolsShell aro_sync method ( Synchronisation between Aro table and User/Profil tables )
	 *
	 * @return boolean
	 */
	private function _aro_sync($admin = false) {
		$ret = false;
		$this->out(__('**** Aro Sync starts. Please wait ... ****'));
		$this->_aro_sync_group();
		$this->_aro_sync_desktop();
		$this->out(__('==== Aro Sync done. ===='));
		$ret = true;
		return $ret;
	}

	/**
	 * Call of AclExtrasShell aco_sync method ( Sync the ACO table )
	 *
	 * @return boolean
	 * @link http://mark-story.com
	 */
	private function _aco_sync() {
		$ret = false;
		$this->out(__('**** Aco Sync starts. Please wait ... ****'));
//		$this->dispatchShell('AuthManager.AclSync aco_sync');
		$this->dispatchShell('AclExtras.AclExtras aco_sync');
		$this->out(__('==== Aco Sync done. ===='));
		$ret = true;
		return $ret;
	}

	/**
	 * Full rigths init
	 *
	 * @return void
	 */
	private function _rights() {
		$this->out(__('**** Full rights init starts. Please wait ... ****'));
		if ($this->_normalizeRights()) {
//			if ($this->_delete_db()) {
				if ($this->_aro_sync()) {
					if ($this->_aco_sync()) {
						if ($this->_reindexRights()) {
							if ($this->_setLightRights()) {
								$this->out(__('==== Full rights init done. ===='));
							} else {
								$this->out('<error>Error while setting simplified rights table.</error>');
							}
						} else {
							$this->out('<error>Error while reindexing simplified rights table.</error>');
						}
					} else {
						$this->out('<error>Error while syncing Acos.</error>');
					}
				} else {
					$this->out('<error>Error while syncing Aros.</error>');
				}
//			} else {
//				$this->out('<error>Error while purging Acls.</error>');
//			}
		} else {
			$this->out('<error>Error while normalizing simplified rights table.</error>');
		}
	}

	/**
	 *
	 */
	private function _clear_logs() {
		App::uses('Folder', 'Utility');
		$logs_folder = new Folder(TMP . 'logs');
		$logs = $logs_folder->read(true, false, true);
		foreach ($logs[1] as $file) {
			if (file_put_contents($file, '') !== false) {
				$this->out('File ' . $file . ' cleared');
			}
		}
	}

	/**
	 * CakePHP cache cleaner
	 *
	 * @return void
	 */
	private function _clear_cache() {
		if (Cache::clear()) {
			$this->out('Cached data : cleared');
		}
		$cachePaths = array('js', 'css', 'menus', 'views', 'persistent', 'models');
		foreach ($cachePaths as $config) {
			if (clearCache(null, $config)) {
				$this->out('Cache ' . $config . ' : cleared');
			} else {
				$this->out('Cache ' . $config . ' : no files removed');
			}
		}
	}

	/**
	 *
	 * @param type $pass
	 */
	private function _hash_pass($pass) {
		$this->out(Security::hash($pass, 'sha256', true));
	}

	/**
	 *
	 * @param type $with_acl
	 */
	private function _clean_tree_tables($with_acl = false) {
		$this->out(__('**** Cleaning tree tables starts. Please wait ... ****'));
		$modelNames = $this->_modelsList();
		foreach ($modelNames as $modelName) {
			if ($this->_hasTreeBehavior($modelName)) {
				$modelClass = ClassRegistry::init($modelName);
				$status = $modelClass->recover();
				$this->out($modelName . ': ' . ($status ? 'ok' : 'error'));
			}
		}

		if ($with_acl) {
			$this->Acl = new AclComponent(new ComponentCollection());
			$this->Acl->startup(null);
			$this->out('Aro :' . ($this->Acl->Aro->recover() ? 'ok' : 'error' ));
			$this->out('Aco :' . ($this->Acl->Aco->recover() ? 'ok' : 'error'));

			App::uses('DataAclComponent', 'DataAcl.Controller/Component');

			$this->DataAcl = new DataAclComponent(new ComponentCollection());
			$this->DataAcl->startup(null);
			$this->DataAcl->Daro->recover();
			$this->DataAcl->Daco->recover();
		}
		$this->out(__('==== Cleaning tree table done. ===='));
	}

	/**
	 *
	 * @param type $modelName
	 * @return boolean
	 */
	protected function _hasTreeBehavior($modelName) {
		/* if( in_array( $modelName, array( 'Aco', 'Aro', 'Daro', 'Daco' ) ) ) {
		  return false; // FIXME
		  } */
		$file = APP . DS . 'Model' . DS . $modelName . '.php';
		if (!file_exists($file)) {
			return false;
		}
		$modelClass = ClassRegistry::init($modelName);
		if (empty($modelClass->actsAs)) {
			return false;
		}
		$behaviors = array_keys(Set::normalize($modelClass->actsAs));
		return in_array('Tree', $behaviors);
	}

	/**
	 *
	 * @param type $accepted
	 * @return type
	 */
	protected function _modelsList($accepted = array()) {
		$models = array();
		$dirName = sprintf('%sModel' . DS, APP);
		$dir = opendir($dirName);
		while (( $file = readdir($dir) ) !== false) {
			$explose = explode('~', $file);
			if (( count($explose) == 1 ) && (!is_dir($dirName . $file) ) && (!in_array($file, array('empty', '.svn')) )) {
				$model = Inflector::classify(preg_replace('/\.php$/', '', $file));
				if (empty($accepted) || in_array($model, $accepted)) {
					$models[] = $model;
				}
			}
		}
		closedir($dir);
		sort($models);
		return $models;
	}

	/**
	 *
	 */
	protected function _clean() {
		$this->out(__('**** Running maintenance tasks, Please wait ... ****'));
		$this->_clean_tree_tables(true);
		$this->_clear_logs();
		$this->_clear_cache();
		$this->out(__('==== Maintenance tasks done. ===='));
	}

	/**
	 *
	 */
	protected function _reset() {
		$this->out(__('**** Running reset tasks, Please wait ... ****'));
		$this->_clean();
		$this->_rights();
		$this->out(__('==== Reset tasks done. ===='));
	}

	/**
	 *
	 */
	protected function _setLightRights() {
		$ret = false;
		$this->out(__('**** Setting rights starts. Please wait ... ****'));

		$this->out(__(' -- Base groups --'));
		$baseProfilAros = $this->Acl->Aro->find('all', array('recursive' => -1, 'conditions' => array('model' => 'Profil', 'id > ' => 1, 'id < ' => 8)));
		$this->_setAclFromRights($baseProfilAros, 'baseProfil');

//		$this->out(__(' -- Other groups --'));
//		$otherProfilAros = $this->Acl->Aro->find('all', array('recursive' => -1, 'conditions' => array('model' => 'Profil', 'id >= ' => 8)));
//		$this->_setAclFromRights($otherProfilAros, 'otherProfil');

		$this->out(__(' -- Desktops --'));
		$desktopAros = $this->Acl->Aro->find('all', array('recursive' => -1, 'conditions' => array('model' => 'Desktop')));
		$this->_setAclFromRights($desktopAros, 'desktop');

		$this->out(__('==== Setting rights done. ===='));
		$ret = true;
		return $ret;
	}

	/**
	 *
	 * @param type $aros
	 * @param type $mode : 'baseProfil' ,'otherProfil', 'desktop'
	 */
	private function _setAclFromRights($aros, $mode) {
		$Right = ClassRegistry::init('Right');
		$Right->setDataSource($this->_conn);
		$Profil = ClassRegistry::init('Profil');
		$Profil->setDataSource($this->_conn);
		$Desktop = ClassRegistry::init('Desktop');
		$Desktop->setDataSource($this->_conn);


		foreach ($aros as $aro) {

			$rights = array();
			$rightSets = array();

			if ($mode == 'baseProfil') {
				$rightSets = $Right->getRightSetsFromProfilName($aro['Aro']['alias']);
			} else if ($mode == 'otherProfil') {
				$group = $Profil->find('first', array('conditions' => array('Profil.id' => $aro['Aro']['foreign_key'], 'Profil.active' => true)));
				if (!empty($group)) {
					$baseAro = $this->Aro->find('first', array('conditions' => array('Aro.model' => 'Profil', 'Aro.foreign_key' => $group['Profil']['profil_id'])));
					if (!empty($baseAro)) {
						$rightSets = $Right->getRightSetsFromProfilName($baseAro['Aro']['alias']);
					}
				}
				$rights = $Right->getRightSets($aro['Aro']['id']);
			} else if ($mode == 'desktop') {
				$right = $Right->find('first', array('conditions' => array('Right.model' => 'Desktop', 'Right.foreign_key' => $aro['Aro']['foreign_key'])));
				if (!empty($right)) {
					$desktop = $Desktop->find('first', array('conditions' => array('Desktop.id' => $aro['Aro']['foreign_key'])));
					if (!empty($desktop)) {
						$baseAro = $this->Aro->find('first', array('conditions' => array('Aro.model' => 'Profil', 'Aro.foreign_key' => $desktop['Desktop']['profil_id'])));
						if (!empty($baseAro)) {
							$rightSets = $Right->getRightSetsFromProfilName($baseAro['Aro']['alias']);
						}
					}
					$rights = $Right->getRightSets($aro['Aro']['id']);
				}
			}

			$this->out($aro['Aro']['alias']);

			if ($mode == 'desktop' && empty($rights)) {
				$this->out('  Save lightGrid -> <info>Nothing to do.</info>');
			} else if (empty($rightSets)) {
				$this->out('  Save lightGrid -> <error>right sets not found.</error>');
			} else {
				$valid = array();
				$rights = $Right->saveRightSets($aro['Aro']['id'], array('rightSets' => $rightSets));
				$this->out('  Save lightGrid -> ' . ($rights ? '<success>ok</success>' : '<error>error</error>'));

				foreach ($rights as $right => $val) {
					if ($val == 1) {
						$validAllow = $this->Acl->allow($aro['Aro']['alias'], $right);
						$this->out('    allow ' . $right . ' : ' . ($validAllow ? '<success>ok</success>' : '<error>error</error>'));
						$valid[] = $validAllow;
					} else {
						$validDeny = $this->Acl->deny($aro['Aro']['alias'], $right);
						$this->out('    allow ' . $right . ' : ' . ($validDeny ? '<success>ok</success>' : '<error>error</error>'));
						$valid[] = $validDeny;
					}
				}
				$this->out('  Apply rights -> ' . (!in_array(false, $valid, true) ? '<success>ok</success>' : '<error>error</error>'));
			}
			$this->out();
		}
	}

	/**
	 *
	 */
	protected function _delegation() {
		$date_act = date('Y-m-d');
		$conns = ConnectionManager::enumConnectionObjects();
		foreach ($conns as $conn => $connValue) {
			if ($conn != 'default' && $conn != 'template' && $conn != 'test') {

				ClassRegistry::init(('Plandelegation'));
				$this->Plandelegation = new Plandelegation();
				$this->Plandelegation->setDataSource($conn);

				ClassRegistry::init(('DesktopsUser'));
				$this->DesktopsUser = new DesktopsUser();
				$this->DesktopsUser->setDataSource($conn);

				$continue = false;
				try {
					$delegs = $this->Plandelegation->find('all', array('contain' => false, 'recursive' => -1));
					$continue = true;
					$this->out('<info>Base ' . $conn . ' :</info> traitement des délégations planifiées.');
				} catch (Exception $exc) {
					if (Configure::read('debug') > 0) {
						$this->out($exc->getTraceAsString());
					}
					$this->out('<info>Base ' . $conn . ' :</info> <error>aucune délégation planifiée.</error>');
				}

				if ($continue) {
					if (empty($delegs)) {
						$this->out('Aucune délégation.');
					} else {
						$countDeleg = 1;
						foreach ($delegs as $deleg) {
							$msgDeleg = 'traitement de la délégation ' . $countDeleg . ' : ';

							$date_start = $deleg['Plandelegation']['date_start'];
							$date_end = $deleg['Plandelegation']['date_end'];

							$tmp = explode(' ', $date_start);
							$start = $tmp[0];
							$tmp = explode(' ', $date_end);
							$end = $tmp[0];

//							if ($start <= $date_act && (intval($end) - intval($date_act) > 0)) {
                            if ($start <= $date_act && $end >= $date_act) {
								$data = $this->DesktopsUser->create();
								foreach ($deleg['Plandelegation'] as $key => $val) {
									if (!in_array($key, array('created', 'modified', 'date_start', 'date_end', 'id'))) {
										$data['DesktopsUser'][$key] = $val;
									}
								}
								$data['DesktopsUser']['delegation'] = true;

								$success = false;
								try {
                                    $alreadyDelegated = $this->DesktopsUser->find(
                                        'first',
                                        array(
                                            'conditions' => array(
                                                'DesktopsUser.user_id' => $data['DesktopsUser']['user_id'],
                                                'DesktopsUser.desktop_id' => $data['DesktopsUser']['desktop_id'],
                                                'DesktopsUser.delegation' => $data['DesktopsUser']['delegation']
                                            ),
											'contain' => false,
											'recursive' => -1
                                        )
                                    );

                                    if(empty($alreadyDelegated)) {
                                        $this->DesktopsUser->save($data);
                                        $success = true;
										$this->out($msgDeleg . ($success ? '<success>la délégation est établie.</success>' : '<error>impossible d\'établir la délégation.</error>'));
                                    }
                                    else {
                                        $success = true;
                                        $this->out($msgDeleg . '<info>la délégation existe déjà.</info>');
                                    }
								} catch (Exception $exc) {
									if (Configure::read('debug') > 0) {
										$this->out($exc->getTraceAsString());
									}
								}

//							} else if ($end <= $date_act) {
							}

//                            if( $start <= $date_act && (intval($end) - intval($date_act) < 0 ) ) {
                            if ( $end <= $date_act ) {
//                            if (intval($end) - intval($date_act) <= 0) {

								$qd = array(
									'conditions' => array(
										'DesktopsUser.user_id' => $deleg['Plandelegation']['user_id'],
										'DesktopsUser.desktop_id' => $deleg['Plandelegation']['desktop_id'],
										'DesktopsUser.delegation' => true
									),
									'contain' => false,
									'recursive' => -1
								);
								$listDeleg = $this->DesktopsUser->find('all', $qd);
//debug($listDeleg);
								$success = false;
								try {
									foreach ($listDeleg as $item) {
										$this->DesktopsUser->delete($item['DesktopsUser']['id']);
									}
									$this->Plandelegation->delete($deleg['Plandelegation']['id']);
									$success = true;
								} catch (Exception $exc) {
									if (Configure::read('debug') > 0) {
										$this->out($exc->getTraceAsString());
									}
								}
								$this->out($msgDeleg . ($success ? '<success>la délégation est annulée.</success>' : '<error>impossible d\'annuler la délégation.</error>'));
							}
							$countDeleg++;
						}
					}
				}
			}
			$this->out();
		}
	}

	/**
	 *  Check if aro is not set
	 *
	 * @param array $aro Aro to check
	 * @return boolean
	 * */
	private function _aro_not_set($aro = null) {
		$retval = false;
		if ($aro != null) {
			if (!empty($aro['Aro']['model']) && !empty($aro['Aro']['foreign_key'])) {
				$conditions = array(
					'model' => $aro['Aro']['model'],
					'foreign_key' => $aro['Aro']['foreign_key']
				);
				if (!empty($aro['Aro']['alias'])) {
					$conditions['alias'] = $aro['Aro']['alias'];
				}
				if (!empty($aro['Aro']['parent_id'])) {
					$conditions['parent_id'] = $aro['Aro']['parent_id'];
				}
				$exists = $this->Aro->find('list', array('conditions' => $conditions));
				if (count($exists) == 0) {
					$retval = true;
				}
			}
		}
		return $retval;
	}

	/**
	 *  Create an Aro
	 *
	 * @param array $aro Aro to create
	 * @return boolean
	 * */
	private function _aro_create($aro = null) {
		$retval = false;
		if ($aro != null) {
			if ($this->_aro_not_set($aro)) {
				$this->Aro->create($aro);
				if ($this->Aro->save()) {
					$this->out($aro['Aro']['model'] . ' ' . $aro['Aro']['alias'] . ' -> <success>ok</success>');
					$retval = true;
				} else {
					$this->out($aro['Aro']['model'] . ' ' . $aro['Aro']['alias'] . ' -> <error>error</error>');
				}
			}
		}
		return $retval;
	}

	/**
	 *  Synchronisation between Aro table and Profil table (here: Role)
	 *
	 * @return boolean
	 * */
	private function _aro_sync_group() {
		$retval = true;
		$groups = $this->Profil->find('list');
		foreach ($groups as $key => $group) {
			$aro = array('Aro' => array());
			$aro['Aro']['model'] = 'Profil';
			$aro['Aro']['foreign_key'] = $key;
			$aro['Aro']['alias'] = $group;
			$this->_aro_create($aro);
		}
		return $retval;
	}

	/**
	 *  Synchronisation between Aro table and User table (here: Utilisateur)
	 *
	 * @return boolean
	 * */
	private function _aro_sync_desktop() {
		$retval = true;
		$desktops = $this->Desktop->find('all');
		foreach ($desktops as $desktop) {
			$aro = array('Aro' => array());
			$aro['Aro']['model'] = 'Desktop';
			$aro['Aro']['foreign_key'] = $desktop['Desktop']['id'];
			$aro['Aro']['alias'] = $desktop['Desktop']['name'];
			$aroParent = $this->Aro->find('first', array('conditions' => array('model' => 'Profil', 'foreign_key' => $desktop['Desktop']['profil_id'])));
			$aro['Aro']['parent_id'] = $aroParent['Aro']['id'];
			$this->_aro_create($aro);
		}
		return $retval;
	}


    private function _fluxEnRetard() {
		$ret = false;
		$this->out(__('**** Analyse des flux en cours. Merci de patienter... ****'));


        $retard = false;
        $courriers = $this->Courrier->find(
            'all',
            array(
                'contain' => false
            )
        );

        $countCourrier = 0;
        foreach( $courriers as $i => $flux) {
            $courrierId = $flux['Courrier']['id'];
            if( !empty($courrierId)) {

//               $courrier = $this->Courrier->find(
//                    'first',
//                    array(
//                        'conditions' => array(
//                            'Courrier.id' => $courrierId,
//                            'Courrier.mail_retard_envoye' => false
//                        ),
//                        'contain' => false
//                    )
//                );

                $sqBancontenu = $this->Courrier->Bancontenu->sqDerniereBannette('Courrier.id');
                $courrier = $this->Courrier->find(
                    'first',
                    array(
                        'fields' => array_merge(
                            $this->Courrier->fields(),
                            $this->Courrier->Bancontenu->fields(),
                            $this->Courrier->Origineflux->fields()
                        ),
                        'conditions' => array(
                            'Courrier.id' => $courrierId,
                            'Courrier.mail_retard_envoye' => false
                        ),
                        'contain' => false,
                        'joins' => array(
                            $this->Courrier->join('Bancontenu', array('type' => 'INNER','conditions' => array("Bancontenu.id IN ( {$sqBancontenu} )") ) ),
                            $this->Courrier->join('Origineflux')
                        )
                    )
                );
//debug($courrier);
//debug( !empty( $courrier['Courrier']['delai_nb'] ) );
//die();
            if ( isset($courrier['Bancontenu']) && !in_array( $courrier['Bancontenu']['etat'], array( '-1', '2' ) ) ) {
                if ( !empty( $courrier['Courrier']['delai_nb'] ) ) {

					// On calcule le délai selon la valeur du ratio appliqué sur l'origine du flux
					if( !empty($courrier['Courrier']['origineflux_id']) ) {
						$origine = $this->Courrier->Origineflux->find(
							'first', array(
								'conditions' => array(
									'Origineflux.id' => $courrier['Courrier']['origineflux_id']
								),
								'contain' => false
							)
						);
						if( !empty($origine['Origineflux']['ratio']) ) {
							$courrier['Courrier']['delai_nb'] = ceil($origine['Origineflux']['ratio'] * $courrier['Courrier']['delai_nb']);
						}
					}

                     $tmpTime = explode(' ', $courrier['Courrier']['datereception']);
                     $tmpTime1 = explode('-', $tmpTime[0]);
                     if ($courrier['Courrier']['delai_unite'] == 0) {
                         $endTime = mktime(0, 0, 0, $tmpTime1[1], $tmpTime1[2] + $courrier['Courrier']['delai_nb'], $tmpTime1[0]);
                     } else if ($courrier['Courrier']['delai_unite'] == 1) {
                         $endTime = mktime(0, 0, 0, $tmpTime1[1], $tmpTime1[2] + (7 * $courrier['Courrier']['delai_nb']), $tmpTime1[0]);
                     } else if ($courrier['Courrier']['delai_unite'] == 2) {
                         $endTime = mktime(0, 0, 0, $tmpTime1[1] + $courrier['Courrier']['delai_nb'], $tmpTime1[2], $tmpTime1[0]);
                     }
                     if ($endTime < mktime()) {
                         $retard = true;
                         if( $retard ) {
                             $this->Courrier->notifier( $courrier );
                             $this->out(__('<success> Mail déclenché pour le flux : '. $courrier['Courrier']['name'].' </success>'));
                             $countCourrier++;
                          }
                     }
                 }
                 // cas où les agents n'ont pas renseignés de type/sous-type (sous 15 jours)
                 else if( Configure::read('Notifretard.Sanstype') ) {
                    if( !empty($courrier['Courrier']['created']) && empty( $courrier['Courrier']['delai_nb'] ) ) {
                        $tmpTime = explode(' ', $courrier['Courrier']['created']);
                        $tmpTime1 = explode('-', $tmpTime[0]);
                        $endTime = mktime(0, 0, 0, $tmpTime1[1], $tmpTime1[2] + 15, $tmpTime1[0]);

                        if ($endTime < mktime()) {
                            $retard = true;
                            if( $retard ) {
                                $this->Courrier->notifier( $courrier );
                                $this->out(__('<success> Mail déclenché pour le flux : '. $courrier['Courrier']['name'].' ne possédant pas de type/soustype </success>'));
                                $countCourrier++;
                             }
                        }
                    }
                 }
                 else {
                     $this->out(__('<info> Aucun mail à envoyer</info>'));
                 }
                }
                else {
                    $this->out(__('<info> Aucun mail à envoyer</info>'));
                }
            }
        }


		$this->out(__( ' ==== '.$countCourrier.' Mail(s) de retard déclenché(s). ===='));
		$ret = true;
		return $ret;
	}



    public function _updateRights() {
        $this->out(__('<info>**** Mise à jour des nouveaux droits. Merci de patienter... ****</info>'));
		if ($this->_normalizeRights()) {
				if ($this->_aro_sync()) {
					if ($this->_aco_sync()) {
						if ($this->_reindexRights()) {
							$this->out(__('==== Mise à jour réalisée. ===='));
						} else {
							$this->out('<error>Erreur détectée lors de la réindexation des droits.</error>');
						}
					} else {
						$this->out('<error>Erreur détectée lors de la synchronisation des données de la table Acos.</error>');
					}
				} else {
					$this->out('<error>Erreur détectée lors de la synchronisation des données de la table Aros.</error>');
				}
		} else {
			$this->out('<error>Erreur lors de la mise à jour des droits.</error>');
		}
    }

	/**
	 *
	 * @return optionParser
	 */
	public function getOptionParser() {
		$actions = array(
			'repair_addressbook_slugs' => 'Recalcule les informations pour la recherche d\'homonymie des contacts.',
			'list_collectivites' => 'Récupère la liste des collectivités, leur identifiant et leur statut.',
			'rights' => 'Génération complète des droits fonction.',
			'clear_cache' => 'Nettoyage du cache CakePHP.',
			'hash_pass' => 'Renvoi la valeur hashée d\'une chaine passée en argument.',
			'clean_tree_tables' => 'Nettoie les tables utilisant le behavior Tree.',
			'clear_logs' => 'Vide les fichiers de logs.',
			'clean' => 'Opérations de maintenance (clean_tree_tables, clear_logs, clear_cache).',
			'reset' => 'Opérations de remises à zéro (clean, rights, data).',
			'install' => "Importation de la structure de la base de données d'administration et création du superadmin.",
			'backup' => "Sauvegarde de la base de données définie par la connexion, toutes les bases, l'application, ou toutes les bases et l'application dans un fichier .tar.bz2.\n" .
			" -m ou --backup_mode\n" .
			"  full (par défaut) : sauvegarde toute l'application et toutes les base de données de l'application\n" .
			"  db : sauvegarde toute la base de données définie par l'option -c ou --connection\n" .
			"  all_db : sauvegarde toutes les base de données de l'application\n" .
			"  app : sauvegarde les fichiers de l'application\n" .
			" -d ou --backup_directory\n" .
			"      répertoire de destination du fichier de sauvegarde (le répertoire courant par défaut)",
			"fix_fs_rights" => "Définis les droits sur les fichiers de l'application et de CakePHP\n" .
			" -o ou --owner : définis le propriétaire (par défaut : www-data)\n" .
			" -g ou --group : définis le groupe (par défaut : www-data)",
			"delegation" => "Vérifie et applique les planification de délégations de profil",
			"set_light_rights" => "Génération des droits simplifiés",
			"retard" => "Déclenchement de mails pour les flux en retard",
			"update_rights" => "Mise à jour des droits avec les nouvelles fontionnalités, sans toucher aux droits existants",
            "notif" => "Déclenchement de mails pour les abonnements au résumé journalier",
            "copie" => "Détachement des flux en coie au bout de 15 jours",
            "purgejournal" => "Suppression des entrées de la table journalevents",
			"bloqueparapheur" => "Notification déclenchée à l'administrateur pour tous flux présents dans le i-Parapheur depuis + d'1 semaine (par défaut)",
			"servicecreator" => "Mise à jour des service_creator_id vides",
			"purgewebdav" => "Suppression des fichiers odt stockés dans le répertoire webdav"
		);
		ksort($actions);
		$optionParser = parent::getOptionParser();
		$optionParser->description(__("Outils d'administration"));
		foreach ($actions as $action => $description) {
			$optionParser->addSubcommand($action, array('help' => $description));
		}
		$optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));
		$optionParser->addOption('backup_mode', array(
			'short' => 'm',
			'help' => 'backup_mode',
			'default' => 'full',
			'choices' => array('full', 'db', 'all_db', 'app')
		));
		$optionParser->addOption('backup_directory', array(
			'short' => 'd',
			'help' => 'backup_directory'
		));
		$optionParser->addOption('owner', array(
			'short' => 'o',
			'help' => 'propriétaires des fichiers suivant le schéma UNIX'
		));
		$optionParser->addOption('group', array(
			'short' => 'g',
			'help' => 'groupe des fichiers suivant le schéma UNIX'
		));
		return $optionParser;
	}

    private function _fluxNotifies() {
		$ret = false;
		$this->out(__('**** Analyse des flux en cours. Merci de patienter... ****'));


        $notified = false;


        $notifquotidiens = $this->Courrier->Notifquotidien->find(
            'all',
            array(
                'conditions' => array(
                    'DATE(Notifquotidien.created)' => date('Y-m-d')
                ),
                'contain' => array(
                    'Courrier',
                    'User',
                    'Notification'
                ),
                'order' => array('Notifquotidien.user_id ASC')
            )
        );


        $data = array();
        if( !empty($notifquotidiens) ) {
            $countCourrier = 0;
            foreach($notifquotidiens as $i => $notif) {

                $courrierId = $notif['Courrier']['id'];
                if( !empty($courrierId)) {
                    $sqBancontenu = $this->Courrier->Bancontenu->sqDerniereBannette('Courrier.id');
                    $courrier = $this->Courrier->find(
                        'first',
                        array(
                            'fields' => array_merge(
                                $this->Courrier->fields()
                            ),
                            'conditions' => array(
                                'Courrier.id' => $courrierId
                            ),
                            'contain' => false
                        )
                    );

                }

                $this->Notification = ClassRegistry::init('Notification');
                if( !empty( $notif['Notifquotidien']['notification_id'] ) ) {

                    $notification = $this->Notification->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Notification.id' => $notif['Notifquotidien']['notification_id']
                            ),
                            'contain' => false
                        )
                    );
                    $notifName =  $notification['Notification']['name'];
                    $notifDescription =  $notification['Notification']['description'];
                }
                else {
                    $notifName =  $notif['Notifquotidien']['name'];
                }
                $data[$notif['Notifquotidien']['user_id']][$notifName][] = array_merge( $courrier, $notif );
            }

            $notified = true;
            $success = false;
            foreach( array_keys($data) as $userId ) {
                $user = $this->Courrier->Notifquotidien->User->find(
                    'first',
                    array(
                        'conditions' => array(
                            'User.id' => $userId
                        ),
                        'contain' => false
                    )
                );
                $userName = $user['User']['prenom'].' '.$user['User']['nom'];

                $success = $this->Courrier->Notifquotidien->User->sendNotifQuotidien( $user, $data );
                if( $success ) {
                    $countCourrier++;
                    $this->out(__('<success> Mail déclenché pour l\'agent : '. $userName.' </success>'));
                    foreach($notifquotidiens as $i => $notif) {
                        $this->Courrier->Notifquotidien->delete($notif['Notifquotidien']['id']);
                    }
                }
                else {
                    $this->out(__('<info> Aucun mail à envoyer</info>'));
                }
            }
            $this->out(__( ' ==== '.$countCourrier.' Mail(s) quotidien(s) déclenché(s). ===='));
        }
        else {
            $this->out(__('<info> Aucun mail à envoyer</info>'));
        }

		$ret = true;
		return $ret;
	}

    /**
     * _fluxEnCopie
     * @return boolean
     */
    private function _fluxEnCopie() {
		$ret = false;
		$this->out(__('**** Analyse des flux en cours. Merci de patienter... ****'));


        $retard = false;
        // Condition sur tous les flux traités hormis ceux en copie
        $conditions = array(
            'bancontenus.etat' => 2,
            'bancontenus.bannette_id' => array(1,2,3,4,5,6)
        );

        // Condition sur les flux en copie non traités
        // on regarde la date de dernière modification d'un flux en copie non détaché depuis 15 jours
        $conditions2 = array(
            'bancontenus.etat' => 1,
            'bancontenus.bannette_id' => 7,
            "bancontenus.modified >=" => date('Y-m-d', strtotime("- 15 days")),
            "bancontenus.modified <" => date('Y-m-d', strtotime("- 14 days"))
        );

        // 1ère condition pour avoir les flux traités non présents en copie
        $sql[] = $this->Courrier->Bancontenu->sq(
            array(
                'fields' => array(
                    'bancontenus.courrier_id'
                ),
                'alias' => 'bancontenus',
                'conditions' => $conditions
            )
        );
        // 2ème condition pour avoir les flux présents en copie depuis 15 jours
        $sql[] = $this->Courrier->Bancontenu->sq(
            array(
                'fields' => array(
                    'bancontenus.courrier_id'
                ),
                'alias' => 'bancontenus',
                'conditions' => $conditions2
            )
        );

        // On regarde l'intersection entre les 2 conditions pour obtenir la liste des flux en copie traités mais non détachés depuis 15 jours
        $bannettesCopies = $this->Courrier->Bancontenu->query(implode(' INTERSECT ', $sql));

        // pour chacun des flux on va les détacher de la bannette en copie en les passant en traités (etat=2)
        foreach( $bannettesCopies as $i => $flux) {
            $courrierId = $flux['bancontenus']['courrier_id'];
            $retval = $this->Courrier->Bancontenu->updateAll(
                array('Bancontenu.etat' => 2),
                array(
                    'Bancontenu.courrier_id' => $courrierId,
                    'Bancontenu.etat' => 1,
                    'Bancontenu.bannette_id' => 7
                )
            );
            if( $retval) {
                $this->out(__('<success> Flux en copie : '. $courrierId.' détaché </success>'));
            }
        }

        $this->out(__('<info> Aucun flux en copie à détacher</info>'));
		$ret = true;
		return $ret;
	}



    /**
     * _fluxClos
     * @return boolean
     */
    private function _fluxClos() {
		$ret = false;
        $success = false;
		$this->out(__('**** Analyse des flux en cours. Merci de patienter... ****'));

        // Condition sur tous les flux traités issus d'une bannette d'insertion (donc de l'initiateur)
        // On ne regarde que les flux
        //  clos + le dernier intiateur étant intervenu dessus + à partir du 01/07/2017 (sinon trop de mails)
        $conditions = array(
            'Bancontenu.etat' => 2,
            'Bancontenu.bannette_id' => 1,
            'Bancontenu.modified >=' => date('Y').'-01-01 00:00:00'
        );

        // liste des abnenttes concernées
        $bannettesInitClos = $this->Courrier->Bancontenu->find(
            'all',
            array(
                'contain' => false,
                'conditions' => $conditions,
                'order' => 'Bancontenu.id DESC'
            )
        );
        // pour chacun des flux on va déclencher un mail pour l'initiateur lui indiquant que le fux est clos
        $countCourrier = 0;

        foreach( $bannettesInitClos as $i => $bannette) {
            $courrierId = $bannette['Bancontenu']['courrier_id'];
            $initDesktopId = $bannette['Bancontenu']['desktop_id'];

            $courrier = $this->Courrier->find(
                'first',
                array(
                    'fields' => array_merge(
                        $this->Courrier->fields()
                    ),
                    'conditions' => array(
                        'Courrier.id' => $courrierId,
                        'Courrier.mail_clos_envoye' => false
                    ),
                    'contain' => false
                )
            );

            $userId = $this->Courrier->Bancontenu->Desktop->User->userIdByDesktop($initDesktopId);
            // On vérifie que l'on n'a pas déjà envoyé un mail pour ce flux
            if( !empty($courrier) ) {
                // si pas de mails adressés, on envoie
                $success = $this->Courrier->Bancontenu->Desktop->User->notifier($courrierId, $userId, null, 'clos');
                if( $success ) {
                    $this->Courrier->updateAll(
                        array(
                            'Courrier.mail_clos_envoye' => true
                        ),
                        array(
                            'Courrier.id' => $courrierId
                        )
                    );
                    $this->out(__('<success> Mail déclenché pour le flux : '. $courrier['Courrier']['name'] . ' '.$courrier['Courrier']['reference']. ' qui a été clos </success>'));
                }
                else {
                    $this->out(__('<info> Aucun Mail déclenché</info>'));
                }
            }
            else {
                $this->out(__('<info> Aucun Mail déclenché</info>'));
            }
            $countCourrier++;
        }

        // On ne met à jour la valeur qu'une fois les traitements terminés,
        // sinon certains initiateurs ne recevront pas les mails de notif
        // (dans le cas de pusierus intiateurs sur un flux)
        foreach( $bannettesInitClos as $i => $bannette) {
            if( $success ) {
                // On met à jour le champ pour indiquer que le mail a déjà été adressé
                $courrierId = $bannette['Bancontenu']['courrier_id'];
                $this->Courrier->updateAll(
                    array(
                        'Courrier.mail_clos_envoye' => true
                    ),
                    array(
                        'Courrier.id' => $courrierId
                    )
                );
            }
        }

        $this->out(__( ' ==== '.$countCourrier.' Mail(s) de flux clos déclenché(s). ===='));
		$ret = true;
		return $ret;
	}


    /**
     * Fonction permettant de vider la table journalevents tous les X temps (X = paramètre àrenseigner)
     * @return boolean
     */
    public function _getCert() {
        $acpem = file_get_contents(APP . DS . 'Config' . DS . 'cert_parapheur' . DS . 'ac.pem');
        $certpem = file_get_contents(APP . DS . 'Config' . DS . 'cert_parapheur' . DS . 'cert.pem');
        $parapheur = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true,
					'Connecteur.signature_protocol'=> 'IPARAPHEUR'
                ),
                'contain' => false
            )
        );
//debug($parapheur);
        if( !empty( $acpem ) && !empty( $certpem ) ) {
            $dataConnecteur = array();
            $confinc = Configure::read('IPARAPHEUR_HOST' );
            if( !empty($parapheur) && empty( $parapheur['Parapheur']['clientcert']) && !empty($confinc) ){
                $dataConnecteur['Connecteur']['id'] = $parapheur['Connecteur']['id'];
                $dataConnecteur['Connecteur']['host'] = Configure::read('IPARAPHEUR_HOST' );
                $dataConnecteur['Connecteur']['login'] = Configure::read('IPARAPHEUR_LOGIN');
                $dataConnecteur['Connecteur']['pwd'] = Configure::read('IPARAPHEUR_PWD');
                $dataConnecteur['Connecteur']['type'] = Configure::read('IPARAPHEUR_TYPE');
                $dataConnecteur['Connecteur']['visibility'] = Configure::read('IPARAPHEUR_VISIBILITY'); // CONFIDENTIEL pour Angoulême
                $dataConnecteur['Connecteur']['wsdl'] = Configure::read('IPARAPHEUR_WSDL');
                $dataConnecteur['Connecteur']['cacert'] = $certpem;
                $dataConnecteur['Connecteur']['clientcert'] = $acpem;
                $dataConnecteur['Connecteur']['cert'] = Configure::read('IPARAPHEUR_CERT');
                $dataConnecteur['Connecteur']['nom_cert'] = Configure::read('IPARAPHEUR_CERT');
                $dataConnecteur['Connecteur']['certpwd'] = Configure::read('IPARAPHEUR_CERTPWD');
                $dataConnecteur['Connecteur']['signature_protocol'] = 'IPARAPHEUR';

                $success = $this->Connecteur->save( $dataConnecteur );
                if( $success ) {
                    $this->out(__( ' ==== Connecteur Parapheur mis à jour. ===='));
                    $ret = true;
                }
                else {
                    $ret = false;
                }
                return $ret;
            }

        }

    }

    /**
     * Fonction permettant d'intégrer le certificat stocké sur le disque et de le basculer en base de données
     * $delai = délai avant suppression (par défaut 6 mois) @integer
     * $type = jour, semaines, mois, année, (days, week, months, year) @string
     * sudo ./lib/Cake/Console/cake --app app AdminTools purgejournal 1 month -c VARIABLE_NOM_COLLECTIVITE
     * @return boolean
     */
    public function _purgeJournal( $delai = null, $type = null ) {


        $typeTraduit = array(
            'days' => 'jour(s)',
            'weeks' => 'semaine(s)',
            'month' => 'mois',
            'year' => 'année(s)'
        );
        $default = '6 month';
        if( !empty( $delai ) && !empty( $type ) ) {
            $default = "$delai $type";
        }
        $dateAvecDelai = date('Y-m-d H:i:s', strtotime("-$default"));
        $journaux = $this->Journalevent->find(
            'all',
            array(
                'conditions' => array(
                    'Journalevent.date <=' => $dateAvecDelai
                ),
                'contain' => false,
                'recursive' => -1
            )
        );
        if(!empty( $journaux ) ) {
            $nbJournaux = count($journaux);
            foreach( $journaux as $journal ) {
                $success = $this->Journalevent->delete($journal['Journalevent']['id']);
                $nbJournaux--;
                $this->out(__( ' ==== Suppression de '. $nbJournaux .' lignes en cours .... ===='));
            }
            if( $success ) {
                if(empty($delai)) {
                    $this->out(__( ' ==== '.$nbJournaux.' lignes concernant la dernière année ont été supprimées du journal. ===='));
                }
                else {
                    $this->out(__( ' ==== '.$nbJournaux.' lignes concernant les '.$delai.' derniers '.$typeTraduit[$type].' ont été supprimées du journal. ===='));
                }

                $ret = true;
            }
            else {
                $ret = false;
            }
        }
    }


	/**
	 * Fonction permettant de notifier l'administrateur qu'un flux est bloqué dans le IP
	 * @return bool
	 * * ./lib/Cake/Console/cake --app app AdminTools -c demo bloqueparapheur 14
	 *
	 */
	private function _fluxBloqueDansParapheur( $delaiFourni = null) {
		$ret = false;
		$this->out(__('**** Analyse des flux en cours. Merci de patienter... ****'));

		$delai = '7';
		if( !empty( $delaiFourni ) ) {
			$delai = "$delaiFourni";
		}

		$retard = false;
		$sqBancontenu = $this->Courrier->Bancontenu->sqDerniereBannette('Courrier.id');
		$courriers = $this->Courrier->find(
			'all',
			array(
				'fields' => array_merge(
					$this->Courrier->fields(),
					$this->Courrier->Bancontenu->fields()
				),
				'conditions' => array(
					'Courrier.mail_bloque_parapheur' => false,
					'Courrier.dateenvoiparapheur <' => date('Y-m-d', strtotime( "-".$delai." days") ),
					'Bancontenu.desktop_id' => -1,
					'Bancontenu.etat' => 1
				),
				'contain' => false,
				'joins' => array(
					$this->Courrier->join('Bancontenu', array('type' => 'INNER','conditions' => array("Bancontenu.id IN ( {$sqBancontenu} )") ) )
				)
			)
		);

		$desktops = $this->Desktop->find(
			'first',
			array(
				'conditions' => array(
					'Desktop.profil_id' => ADMIN_GID,
				),
				'contain' => array(
					'User',
					'SecondaryUser'
				)
			)
		);
		$countCourrier = 0;
		foreach( $courriers as $i => $flux) {
			$dateCreationFluxPlus1Semaine = date('Y-m-d', strtotime("+".$delai." day", strtotime($flux['Courrier']['dateenvoiparapheur'] )) );
			if (date('Y-m-d') > $dateCreationFluxPlus1Semaine ) {
				$retard = true;
				if( $retard ) {
					$this->Courrier->notifierAdmin( $flux['Courrier']['id'], $desktops );
					$this->out(__('<success> Notification déclenchée pour le flux : '. $flux['Courrier']['reference'].' </success>'));
					$countCourrier++;
				}
			}
		}


		$this->out(__( ' ==== '.$countCourrier." Notification(s) de flux bloqués dans le i-Parapheur depuis + de ".$delai." jours déclenché(s). ===="));
		$ret = true;
		return $ret;
	}



	/**
	 * Fonction permettant de mettre à jour les informations des sous-types issus du iParapheur directement en BDD
	 * On remplace les eniters (0, 1, 2 ...) par les valeurs réelles (Visa, Signature ,....)
	 * @return bool
	 *
	 * ./lib/Cake/Console/cake --app app AdminTools -c demo soustypeiparapheur
	 *
	 */
	private function _updateSoustypeParapheur() {
		$ret = false;
		$this->out(__('**** Analyse des sous-types i-Parapheur en base de données. Merci de patienter... ****'));

		$listeSoustypeIp = file_get_contents( '/data/workspace/' . $this->params['connection'] . '/old_liste_soustype_ip.json');

		$oldsoustype = json_decode($listeSoustypeIp, true);

		$Visa = ClassRegistry::init('Cakeflow.Visa');
		$Visa->setDataSource($this->_conn);
		$Etape = ClassRegistry::init('Cakeflow.Etape');
		$Etape->setDataSource($this->_conn);
		$Composition = ClassRegistry::init('Cakeflow.Composition');
		$Composition->setDataSource($this->_conn);

		foreach( $oldsoustype as $key => $value ) {

			// MAJ des infos du circuit (visa)
			$visas = $Visa->find(
				'all',
				array(
					'fields' => array(
						'Visa.id',
						'Visa.soustype'
					),
					'conditions' => array(
						'Visa.soustype' => $key
					),
					'contain' => false,
					'recursive' => -1
				)
			);

			if( !empty( $visas ) ) {
				foreach( $visas as $visa ) {
					$Visa->updateAll(
						array('Visa.soustype' => "'".$value."'" ),
						array(
							'Visa.id' => $visa['Visa']['id']
						)
					);
				}
			}

			// MAJ des infos des étapes
			$etapes = $Etape->find(
				'all',
				array(
					'fields' => array(
						'Etape.id',
						'Etape.soustype'
					),
					'conditions' => array(
						'Etape.soustype' => $key
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			if( !empty( $etapes ) ) {
				foreach( $etapes as $etape ) {
					$Etape->updateAll(
						array('Etape.soustype' => "'".$value."'" ),
						array(
							'Etape.id' => $etape['Etape']['id']
						)
					);
				}
			}

			// MAJ des infos des compositions
			$compos = $Composition->find(
				'all',
				array(
					'fields' => array(
						'Composition.id',
						'Composition.soustype'
					),
					'conditions' => array(
						'Composition.soustype' => $key
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			if( !empty( $compos ) ) {
				foreach( $compos as $compo ) {
					$Composition->updateAll(
						array('Composition.soustype' => "'".$value."'" ),
						array(
							'Composition.id' => $compo['Composition']['id']
						)
					);
				}
			}
		}

		$this->out(__( ' ==== Les sous-types i-Parapheur ont été mis à jour. ===='));
		$ret = true;
		return $ret;
	}


	/**
	 * Fonction permettant de mettre à jour les informations des sous-types issus du iParapheur directement en wkf_visas
	 * @return bool
	 *
	 * ./lib/Cake/Console/cake --app app AdminTools -c demo visaiparapheur
	 *
	 */
	private function _updateSoustypeVisaParapheur() {
		$ret = false;
		$this->out(__('**** Analyse des sous-types i-Parapheur en base de données. Merci de patienter... ****'));

		$Visa = ClassRegistry::init('Cakeflow.Visa');
		$Visa->setDataSource($this->_conn);

		// MAJ des infos du circuit (visa)
		$visas = $Visa->find(
			'all',
			array(
				'fields' => array(
					'Visa.id',
					'Visa.soustype',
					'Visa.etape_id',
					'Etape.soustype'
				),
				'conditions' => array(
					'Visa.soustype is null',
					'Visa.trigger_id' => '-1'
				),
				'contain' => array(
					'Etape'
				),
				'recursive' => -1
			)
		);

		foreach( $visas as $visa ) {
			$Visa->updateAll(
				array('Visa.soustype' => "'".$visa['Etape']['soustype']."'" ),
				array(
					'Visa.id' => $visa['Visa']['id']
				)
			);
		}

		$this->out(__( ' ==== Les sous-types i-Parapheur ont été mis à jour. ===='));
		$ret = true;
		return $ret;
	}


	/**
	 * Fonction permettant de mettre à jour les informations du service créateur
	 * @return bool
	 *
	 * ./lib/Cake/Console/cake --app app AdminTools -c demo servicecreator
	 *
	 */
	private function _serviceCreator() {
		$ret = false;
		$this->out(__('**** Analyse des flux dont le service_creator_id est vide. Merci de patienter... ****'));

		$courriers = $this->Courrier->find(
			'all',
			array(
				'conditions' => array(
					'Courrier.soustype_id is not null',
					'Courrier.service_creator_id is null'
				),
				'contain' => false,
				'recursive' => -1
			)
		);

		foreach( $courriers as $flux ) {
			$bancontenu = $this->Courrier->Bancontenu->find(
				'first',
				array(
					'conditions' => array(
						'Bancontenu.courrier_id' => $flux['Courrier']['id'],
						'Bancontenu.bannette_id' => 1
					),
					'order' => array('Bancontenu.modified desc'),
					'contain' => false,
					'recursive' => -1.
				)
			);
			if( !empty( $bancontenu ) ) {
				$desktopService = $this->Courrier->Bancontenu->Desktop->DesktopsService->find(
					'first',
					array(
						'conditions' => array(
							'DesktopsService.desktop_id' => $bancontenu['Bancontenu']['desktop_id']
						)
					)
				);

				if (!empty($desktopService)) {
					$this->Courrier->updateAll(
						array('Courrier.service_creator_id' => $desktopService['Service']['id']),
						array(
							'Courrier.id' => $flux['Courrier']['id']
						)
					);
				}
			}
		}


		$this->out(__( ' ==== Les services ont été mis à jour. ===='));
		$ret = true;
		return $ret;
	}


	/**
	 * Fonction permettant de supprimer les fichiers odt stockés dans le répertoire webdav mais non utilisé par la suite depuis + de 7 jours
	 * @return bool
	 *
	 * ./lib/Cake/Console/cake --app app AdminTools purgewebdav -c demo
	 * ./lib/Cake/Console/cake --app app AdminTools purgewebdav 10 -c demo == pour augmenter le délai à 10 jours
	 *
	 */
	private function _purgeWebdavByColl( $delaiFourni = null ) {
		$ret = false;
		$this->out(__('**** Analyse des documents présents dans le webdav. Merci de patienter... ****'));

		$delai = '7';
		if( !empty( $delaiFourni ) ) {
			$delai = $delaiFourni;
		}

		$courriers = $this->Courrier->find(
			'all',
			array(
				'contain' => false
			)
		);

		$this->out(__( '<info> ==== Les documents dont la date de modification est supérieure à '.$delai.' jours vont être supprimés. ====</info>'));

		foreach( $courriers as $i => $flux) {
			$courrierId = $flux['Courrier']['id'];

			$modeleDir = WEBDAV_DIR . DS . $this->_conn . DS . $courrierId;
			$folderToLook = new Folder($modeleDir, false, 0777);
			$odtFiles = $folderToLook->find( '.*\.odt', true);
			$countFiles = 0;
			$deletionAuthorized = false;
			foreach ($odtFiles as $odtFile) {
				$dateCreationFluxPlus1Semaine = date('Y-m-d', strtotime("+".$delai." day", filemtime($modeleDir . DS . $odtFile) ) );
				if (date('Y-m-d') > $dateCreationFluxPlus1Semaine ) {
					$deletionAuthorized = true;
					if( $deletionAuthorized ) {
						$webdavFile = new File($modeleDir . DS . $odtFile);
						$webdavFile->delete();
						$this->out(__('<success> Le fichier : '. $odtFile.' a été suppprimé </success>'));
					}
				}
			}
		}

		$ret = true;
		return $ret;
	}




}
