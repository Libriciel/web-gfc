<?php

/**
 * Installations Shell
 *
 * web-DPO : Outil de gestion de vos traitements dans le cadre de la
 * réglementation relative à la protection des données personnelles (RGPD)
 *
 * Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 *
 * Licensed under the GNU Affero General Public License version 3 License - AGPL v3
 * For full copyright and license information, please see the "LICENSE" file.
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (https://www.libriciel.fr/)
 * @link        https://www.libriciel.fr/web-dpo/
 * @since       web-DPO v1.0.0
 * @license     [GNU Affero General Public License version 3](http://www.gnu.org/licenses/agpl-3.0.html) - AGPL v3
 * @version     v2.1.0
 * @package     App.Console.Command.Shell
 */

App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');

class InstallationsShell extends XShell {

	public $uses = [
		'Admin',
		'User'
	];

	/**
	 * Function getOptionParser
	 *
	 * Options d'exécution et validation des arguments
	 *
	 * @return ConsoleOptionParser $parser
	 * @version v1.0.0
	 */
	public function getOptionParser()
	{
		$parser = parent::getOptionParser();

		$actions = array(
			'updateSaltAndCipherSeed' => 'Mie à jour du SALT et du CypherSeed suite au passage en sha256',
			'newColl' => 'Création d \'une nouvelle collectivité'
		);
		ksort($actions);

		$parser->description([
			'Ajout du SALT et du cipherSeed dans le core.php',
			'',
			'Exemple: cake users updateSaltAndCipherSeed -s xxxxxxxxxxxxxxxxxxxxxxxxx -c 00000000000000000000000',
		]);
		foreach ($actions as $action => $description) {
			$parser->addSubcommand($action, array('help' => $description));
		}

		$options = [
			'salt' => [
				'short' => 's',
				'help' => "Chaîne aléatoire de 40 caractères minimum composée de lettre de l'alphabet de A à Z (minuscule et majuscule) et de chiffre entre 0 et 9.",
				'default' => null,
				'required' => false,
			],
			'cipher' => [
				'short' => 'c',
				'help' => "Chaîne aléatoire de 30 chiffres minimum composée uniquement de chiffre entre 0 et 9.",
				'default' => null,
				'required' => false,
			]
		];
		$parser->addOptions($options);

		$parser->addOption('file', array(
			'short' => 'f',
			'help' => 'file'
		));

		return $parser;
	}

	/**
	 * @throws Exception
	 */
	public function updateSaltAndCipherSeed()
	{
		$pathCore = CONFIG . 'core.php';

		if (is_readable($pathCore) !== true) {
			$this->out("<error>Le fichier {$pathCore} n'est pas accessible en lecture.</error>");
		}

		if (is_writable($pathCore) !== true) {
			$this->out("<error>Le fichier {$pathCore} n'est pas accessible en écriture.</error>");
		}

		$salt = Hash::get($this->params, 'salt');
		if (empty($salt)) {
			$salt = bin2hex(random_bytes(20));
		} else {
			if (strlen($salt) < 40) {
				$this->out("<error>Le SALT défini ne respect pas la contrainte de taille : 40 caractères minimum.</error>");
			}

			if (ctype_alnum($salt) === false) {
				$this->out("<error>Le SALT défini ne respect pas la contrainte de composition : lettre de l'alphabet de A à Z (minuscule et majuscule) et de chiffre entre 0 et 9.</error>");
			}
		}

		$refNbCipherSeed = 30;
		$cipherSeed = Hash::get($this->params, 'cipher');
		if (empty($cipherSeed)) {
			$cipherSeed = $this->generateRandomString($refNbCipherSeed);
		} else {
			if (strlen($cipherSeed) < $refNbCipherSeed) {
				$this->out("<error>Le cipherSeed défini ne respect pas la contrainte de taille : 30 chiffres minimum.</error>");
			}

			if (is_numeric($cipherSeed) === false) {
				$this->out("<error>Le cipherSeed défini ne respect pas la contrainte numérique : uniquement de chiffre entre 0 et 9.</error>");
			}
		}

		$keySalt = 'webgfc_security_salt_core';
		$keycipherSeed = 'webgfc_security_cipherSeed_core';

		$str = file_get_contents($pathCore);

		if (strpos($str, $keySalt) === false) {
			$this->out("<error>La clé {$keySalt} n'a pas été trouvé dans le fichier {$pathCore}</error>");
			return;
		}

		if (strpos($str, $keycipherSeed) === false) {
			$this->out("<error>La clé {$keycipherSeed} n'a pas été trouvé dans le fichier {$pathCore}</error>");
			return;
		}

		$str = str_replace($keySalt, $salt, $str);
		$str = str_replace($keycipherSeed, $cipherSeed, $str);

		file_put_contents($pathCore, $str);

		echo ("Les clés {$keySalt} et {$keycipherSeed} ont été modifiées dans le fichier {$pathCore} \n");
		echo ("{$keySalt} = {$salt} \n");
		echo ("{$keycipherSeed} = {$cipherSeed} \n");
	}

	private function generateRandomString($length = 30)
	{
		$characters = '0123456789';
		$charactersLength = strlen($characters);
		$randomString = '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}



	/**
	 * @throws Exception
	 * ./lib/Cake/Console/cake --app app Installations newColl -f JSON_FILE
	 */
	public function newColl( )
	{

		$ret = false;
		$this->out(__('**** Création d\'une nouvelle collectivité. Merci de patienter... ****'));

		if( !empty($this->params['file']) ) {

			$this->loadModel('Collectivite');
			$this->Collectivite->create();
			$collectivite = json_decode(file_get_contents($this->params['file']), true );
			if( !empty( $collectivite) ) {
				$success = false;
				$this->Collectivite->query('CREATE DATABASE ' . $collectivite['database'] . ' TEMPLATE webgfc_template;');
				$this->Collectivite->begin();
				$collSaved = $this->Collectivite->save($collectivite);
//$this->Log($this->Collectivite->validationErrors);
				if (!$collSaved) {
					$this->out('<error> la collectivité n\'a pas été créée</error>');
					$error = $this->Collectivite->validationErrors;
					$this->log($error);
					$this->Collectivite->rollback();
					$this->Collectivite->query('DROP DATABASE ' . $collectivite['database'] .';');
					return;
				}

				$this->loadModel('Desktop');
				$this->Desktop->setDataSource($collectivite['conn']);
				$this->loadModel('User');
				$this->User->setDataSource($collectivite['conn']);
				$this->loadModel('Aco');
				$this->Aco->setDataSource($collectivite['conn']);
				$this->loadModel('Aro');
				$this->Aro->setDataSource($collectivite['conn']);
				$this->loadModel('Permission');
				$this->Permission->setDataSource($collectivite['conn']);

				$desktop_admin = $this->Desktop->create();
				$desktop_admin['Desktop']['name'] = 'Profil administrateur';
				$desktop_admin['Desktop']['profil_id'] = ADMIN_GID;

				if ($this->Desktop->save($desktop_admin)) {

					$this->loadModel('Desktopmanager');
					$this->Desktopmanager->setDataSource($collectivite['conn']);
					$bureau_admin = $this->Desktopmanager->create();
					$bureau_admin['Desktopmanager']['name'] = 'Bureau ' . $desktop_admin['Desktop']['name'];
					$bureau_admin['Desktopmanager']['active'] = true;
					$bureau_admin['Desktopmanager']['isautocreated'] = true;

					if ($this->Desktopmanager->save($bureau_admin)) {
						$this->loadModel('DesktopDesktopmanager');
						$this->DesktopDesktopmanager->setDataSource($collectivite['conn']);
						$desktopDesktopmanager = $this->DesktopDesktopmanager->create();
						$desktopDesktopmanager['DesktopDesktopmanager']['desktop_id'] = $this->Desktop->id;
						$desktopDesktopmanager['DesktopDesktopmanager']['desktopmanager_id'] = $this->Desktopmanager->id;
						$this->DesktopDesktopmanager->save($desktopDesktopmanager);
					}

					$user = array('User' => $collectivite['admin']);
					$user['User']['desktop_id'] = $this->Desktop->id;
					$user['User']['password'] = Security::hash($collectivite['admin']['password'], 'sha256', true);

					$this->User->create($user);
					if ($this->User->save()) {
						$success = true;
					}
				}
				if ($success) {
					$this->Collectivite->commit();
					$ret = true;

					// Création des répertoires de stockage sur disque par collectivité pour le workspace et le webdav
					$workspaceFolder = new Folder(WORKSPACE_PATH . DS . $collectivite['conn'] , true, 0777);
					$webdavFolder = new Folder(WEBDAV_DIR . DS . $collectivite['conn'] . DS . 'modeles' , true, 0777);
				} else {
					$this->Collectivite->rollback();
					$ret = false;
				}
			}
		}
		else {
			$this->out('<error> Merci de renseigner un fichier json valide avec les informations utiles pour la création de votre collectivité</error>' );
			$this->out('<error> Vous trouverez un exemple valide ci-dessous</error>' );

			$jsonFileExemple = APP . WEBROOT_DIR .DS . 'files/newcoll_libriciel.json';
			$vueJson = "cat $jsonFileExemple";
			$this->out('<error>'.shell_exec(escapeshellcmd($vueJson) ).'</error>'  );
		}
	}

	/**
	 *
	 */
	public function main() {

		if( !empty( $this->params['file']) ){
			$createdColl = array();
			$this->out('<info>Création de la collectivité en cours ...</info>');
			$this->XProgressBar->start(1);

			$createdColl = $this->newColl($this->params['file']);

			$this->hr();
			if (!in_array(false, $createdColl, true)) {
				$this->out('<success>Opération terminée avec succès.</success>');
			} else {
				$this->out('<error>Opération terminée avec erreur(s).</error>');
			}
			$this->hr();
		}
	}
	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

		$this->_conn = 'default';

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';


	}


}
