<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');

/**
 *
 * UserAway shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class UserAwayShell extends Shell {

	/**
	 * Contains database source to use
	 *
	 * @var string
	 * @access public
	 */
	public $uses = array('User');

	/**
	 *
	 */
	public function status($id = null) {
		$user_id = null;
		if (isset($this->args[0]) && !empty($this->args[0])) {
			$user_id = $this->args[0];
		} else if ($id != null) {
			$user_id = $id;
		}

		if ($user_id != null) {
			$user = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
			if (!empty($user)) {
				$this->out('User :');
				$this->out('  username : ' . $user['User']['username']);
				$this->out('  nom : ' . $user['User']['nom']);
				$this->out('  prenom : ' . $user['User']['prenom']);
				$this->out('  delegue : ' . $user['User']['delegue']);
				$this->out('  date de depart : ' . $user['User']['date_dep']);
				$this->out('  date de retour : ' . $user['User']['date_ret']);
				if ($this->User->isAway($this->args[0])) {
					$this->out('<success>Utilisateur absent</success>');
				} else {
					$this->out('<success>Utilisateur présent</success>');
				}
			} else {
				$this->out('<error>Auncun utilisateur !!</error>');
			}
		} else {
			$this->out('<error>Veuillez préciser l\'identifiant de l\'utilisateur.</error>');
			$this->out('<error>exemple : cake UserAway status 43</error>');
		}
	}

	public function away() {
		$user = $this->User->find('first', array('conditions' => array('User.id' => $this->args[0])));
		if (isset($this->args[1]) && !empty($this->args[1])) {
			$delegue = $this->args[1];
		} else {
			$querydata = array(
				'conditions' => array(
					'User.id <>' => $this->args[0],
					'User.profil_id <>' => SUPERADMIN_GID,
					'User.profil_id <>' => ADMIN_GID
				)
			);
			$users = $this->User->find('list', $querydata);
			foreach ($users as $id => $username) {
				$this->out($id . ': ' . $username);
			}
			$delegue = $this->in('Choisir un utilisateur délégué : ');
		}

		if ($this->User->setAway($this->args[0], $delegue)) {
			$this->out('<success>' . __d('user', 'abs.away') . '</success>');
		} else {
			$this->out('<error>' . __d('user', 'abs.error') . '</error>');
		}
		$this->status($this->args[0]);
	}

	public function present() {
		$user = $this->User->find('first', array('conditions' => array('User.id' => $this->args[0])));
		if ($this->User->setPresent($this->args[0])) {
			$this->out('<success>' . __d('user', 'abs.present') . '</success>');
		} else {
			$this->out('<error>' . __d('user', 'abs.error') . '</error>');
		}
		$this->status($this->args[0]);
	}

	/**
	 * get the option parser.
	 *
	 * @return void
	 */
	public function getOptionParser() {
		return parent::getOptionParser()
						->description(__("Outil de gestion des abscences"))
						->addSubcommand('status', array(
							'help' => __('informations d\'absence d\'un utilisateur (ex: cake UserAway status 43)')
						))
						->addSubcommand('away', array(
							'help' => __('rendre un utilisateur absent (ex: cake UserAway away 43')
						))
						->addSubcommand('present', array(
							'help' => __('rendre un utilisateur present (ex: cake UserAway present 43')
						));
	}

}
