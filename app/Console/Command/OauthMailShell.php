<?php

use Webklex\PHPIMAP\ClientManager;
use Webklex\PHPIMAP\Client;
use Webklex\PHPIMAP\Support\MessageCollection;

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 *
 * OauthMail shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par Libriciel SCOP
 * @link https://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 *
 * ex:
 *  Lecture d'une boîte mail : sudo ./lib/Cake/Console/cake --app app OauthMail process -t gmail -m scriptname -c collectivite
 *  Lecture d'une boîte mail : sudo ./lib/Cake/Console/cake --app app OauthMail process -t office -m scriptname -c collectivite
 *
 *
 */
class OauthMailShell extends XShell {

	/**
	 *
	 * @var type
	 */
	public $repository;

	/**
	 *
	 * @var type
	 */
	public $files = array();

	/**
	 *
	 * @var type
	 */
	public $invalidFiles = array();


	/**
	 *
	 * @var type
	 */
	public $destDesktopId;

	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @var type
	 */
	public $Courrier;

	/**
	 *
	 * @var type
	 */
	public $Scanemail;

	public $scanTypeId;
	public $scanSoustypeId;

	/**
	 *
	 * @var type
	 */
	public $purgeableFiles = array();
	/**
	 *
	 * @var type
	 */
	public $Document;

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));


		$this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}
		ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->Courrier = ClassRegistry::init('Courrier');

		$this->Scanemail = ClassRegistry::init('Scanemail');
		$this->Scanemail->useDbConfig = $this->_conn;

		$this->Service = ClassRegistry::init('Service');
		$this->Service->useDbConfig = $this->_conn;

		$this->Document = ClassRegistry::init('Document');
		$this->Document->useDbConfig = $this->_conn;


		$scan = $this->Scanemail->find('first',array(
			'conditions' => array(
				'Scanemail.scriptname' => $this->params['mailbox']
			),
			'recursive' => -1
		));
		if (empty($scan) ) {
			throw new FatalErrorException('Le paramètre '.$this->params['mailbox'].' n\'est pas défini dans la configuration de la boîte mail');
		}
		$this->destDesktopId = $scan['Scanemail']['desktop_id'];
		$this->destDesktopmanagerId = $scan['Scanemail']['desktopmanager_id'];
		$this->scanTypeId = isset( $scan['Scanemail']['type_id'] ) ? $scan['Scanemail']['type_id'] : null;
		$this->scanSoustypeId = isset( $scan['Scanemail']['soustype_id'] ) ? $scan['Scanemail']['soustype_id'] : null;
		$this->scanFolderName = isset( $scan['Scanemail']['name'] ) ? $scan['Scanemail']['name'] : null;
		$this->scanArchiveFolderName = isset( $scan['Scanemail']['archive_folder'] ) ? $scan['Scanemail']['archive_folder'] : null;

		if (!is_int($this->destDesktopmanagerId) || $this->destDesktopmanagerId <= 0) {
			throw new FatalErrorException('Le bureau destinataire de la boîte mail n\'est pas défini');
		}
		$this->collId = $coll['Collectivite']['id'];

		// Si le jeotn d'accès est expiré, on le rafraîchit avant lancement du traitement
		if( !empty($scan['Scanemail']['expires_in']) ) {
			$expiresIn = $scan['Scanemail']['expires_in'];
			$currentTime = date('Y-m-d H:i:s');
			if( $currentTime > $expiresIn ) {
				$this->refreshToken($scan['Scanemail']['id']);
			}
		}

		$this->files = $this->_getMailByBox($this->params['mailbox'], $this->params['typeserveroauth']);
	}

	/**
	 *
	 * @param type $file
	 * @return type
	 */
	private function _create($file, $conn, $fluxName = null) {

//		$remoteReturn = $this->Courrier->createMailWithOauth($this->destDesktopmanagerId, $file, $this->collId, $conn, $this->scanTypeId, $this->scanSoustypeId);
		$remoteReturn = $this->Courrier->remoteCreateMail($this->destDesktopmanagerId, $file, $this->collId, $conn, $this->scanFolderName,  $this->scanArchiveFolderName, $this->scanTypeId, $this->scanSoustypeId);

		$return = false;
		if($remoteReturn['CodeRetour'] == 'CREATED' && !empty( $this->params['mailbox'] ) ) {
			$return = true;
		}else{
			// Suppression du flux généré si aucun fichier associé n'a pu être créé
			$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
			$desktopsIds[] = $this->Desktopmanager->getDesktops($this->destDesktopId);
			foreach( $desktopsIds as $i => $desktopId) {
				$banscontenus = $this->Courrier->Bancontenu->find(
					'all',
					array(
						'conditions' => array(
							'Bancontenu.courrier_id' => $remoteReturn['CourrierId'],
							'Bancontenu.desktop_id' => $desktopsIds[$i]
						),
						'recursive' => -1
					)
				);

				if (!empty($banscontenus)) {
					foreach( $banscontenus as $b => $bancontenu ) {
						$this->Courrier->Bancontenu->delete($bancontenu['Bancontenu']['id'], true);
					}
					$this->Courrier->delete($remoteReturn['CourrierId']);
				}
			}
		}

		return $return;
	}

	/**
	 *
	 */
	public function process() {
		if (!empty($this->files) ) {

			$createdFlux = array();
			$this->out('<info>Acquisition des mails en cours ...</info>');
			$this->XProgressBar->start(count($this->files));

			foreach ($this->files as $file) {
				$this->_create($file,$this->_conn);
				$this->out('<info>Acquisition en cours ...</info>');
				$this->XProgressBar->next();
			}
			$this->XProgressBar->finish();

			$this->hr();
			if (!in_array(false, $createdFlux, true)) {
				$this->out('<success>Opération terminée avec succès.</success>');
			} else {
				$this->out('<error>Opération terminée avec erreur(s).</error>');
			}
			$this->hr();
		}
	}

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
		$optionParser = parent::getOptionParser();
		$optionParser->addOption('service', array(
			'short' => 's',
			'help' => 'Nom du service devant être analysé'
		));
		$optionParser->addOption('typeserveroauth', array(
			'short' => 't',
			'values' => [
				'gmail' => 'Gmail',
				'office' => 'Office365',
				'default' => 'serveurmail'
			],
			'help' => 'Type du serveur mail cible'
		));
		$optionParser->addOption('mailbox', array(
			'short' => 'm',
			'help' => 'Nom de la boîte mail définie en paramétrage'
		));
		$optionParser->description(__("Outil de gestion des scans"))
			->addSubcommand('process', array(
				'help' => __('Lecture, creation et purge des fichiers du répertoire de dépot (acquisitions précédentes)')
			));

		return $optionParser;
	}

	private function _getMailByBox( $scriptname, $typeserveroauth ) {
		$this->Scanemail = ClassRegistry::init('Scanemail');
		$scan = $this->Scanemail->find(
			'first',
			array(
				'conditions' => array(
					'Scanemail.scriptname' => $scriptname
				),
				'recursive' => -1
			)
		);

		$boiteMail = $scan['Scanemail']['hostname'];
		$port = $scan['Scanemail']['port'];
		$login = $scan['Scanemail']['username'];
		$folderToScan = $scan['Scanemail']['name'];
		$configuration = $scan['Scanemail']['configuration'];
		$authentication = $scan['Scanemail']['authentication'];
		$accessToken = $scan['Scanemail']['access_token'];

		$imapConf = [
			'accounts' => [
				$typeserveroauth => [
					'host'  => $boiteMail,
					'port'  => $port,
					'protocol'  => $configuration,
					'encryption'    => 'ssl',
					'validate_cert' => true,
					'username' => $login,
					'password' => $accessToken,
					'authentication' => $authentication
				]
			]
		];

		$cm = new ClientManager($imapConf);
		/** @var \Webklex\PHPIMAP\Client $client */
		$client = $cm->account($typeserveroauth);

		//Connect to the IMAP Server
		$client->connect();

		//Get all Mailboxes
		/** @var \Webklex\PHPIMAP\Support\FolderCollection $folder */
		$folder = $client->getFolderByName($folderToScan);
		if ( empty( $folder ) ) {
			$this->out('<error>Le répertoire '.$folderToScan.' n\'existe pas</error>');
			return false;
		}


		//Get only unseen Messages of the current Mailbox $folder
		/** @var MessageCollection $messages */
		$messages =  $folder->messages()->unseen()->get();


		if (FALSE === $messages) {
			$this->out('<error>Impossible de lire le contenu de la boite mail</error>');
			return false;
		}

		return $messages;
	}

	/**
	 * @param $scanemailId
	 * @return bool|string
	 * @throws Exception
	 */
	public function refreshToken($scanemailId) {
		$this->Scanemail = ClassRegistry::init('Scanemail');
		$scan = $this->Scanemail->find(
			'first',
			array(
				'conditions' => array(
					'Scanemail.id' => $scanemailId
				),
				'contain' => false
			)
		);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST'); //FIXME
		if( Configure::read('Curl.UseProxy' ) ) {
			curl_setopt($curl, CURLOPT_PROXY, Configure::read('Curl.ProxyHost' ) );
		}
		$api = $scan['Scanemail']['url_api'];

		$scan['Scanemail']['refresh_token'] = $scan['Scanemail']['access_token'];
		unset( $scan['Scanemail']['access_token']);


		curl_setopt($curl, CURLOPT_URL, $api);

		curl_setopt($curl, CURLOPT_POSTFIELDS, $scan['Scanemail']);

		$response = curl_exec($curl);

		if ($response === false) {
			CakeLog::error(curl_error($curl), 'Oauth');
			throw new Exception(curl_error($curl));
		}
		curl_close($curl);
		$result = json_decode( $response );

		// On récupère le nouveau token
		$scan['Scanemail']['access_token'] = $result->access_token;
		$expiresIn = json_decode($response)->expires_in;
		// On récupère le nouveau délai d'expiration
		if( !empty($expiresIn) ) {
			$currentTime = date('Y-m-d H:i:s');
			$scan['Scanemail']['expires_in'] = date("Y-m-d H:i:s", strtotime( $currentTime.'+'.$expiresIn.' seconds'));
			$this->Scanemail->save($scan);
		}
		$this->Scanemail->save($scan);

		return $result->access_token;
	}
}

