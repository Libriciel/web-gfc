<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 *
 * CreateFromFiles shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 * @source http://adresse.data.gouv.fr
 */
class BanInsertShell extends XShell {


	/**
	 *
	 * @var type
	 */
	public $files = array();


	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @var type
	 */
	public $Ban;

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

        $this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));


		Configure::write('conn', $this->params['connection']);

        ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->Ban = ClassRegistry::init('Ban');
        $this->Ban->useDbConfig = $this->_conn;
	}


	/**
     *
     * Intégration pour le département de l'hérault
     *  sudo ./lib/Cake/Console/cake --app app BanInsert process -d 340 -f app/tmp/file.csv -c coll
     *
     * Mise à jour des informatins en BDD
     *  sudo ./lib/Cake/Console/cake --app app BanInsert process -d 340 -u true -f app/tmp/file.csv -c coll
	 */
	public function process() {
        $createdBan = array();
        if( !empty( $this->params['file']) && !empty( $this->params['departement']) ){
            $this->out('<info>Acquisition des adresses en cours ...</info>');
            $this->XProgressBar->start(1);

            if( !Configure::read('BanInsert.Update') ) {
                $this->importBanCsv($this->params['file'], $this->params['departement']);
            }
            else {
                $this->importUpdateBanCsv($this->params['file'], $this->params['departement']);
            }

            $this->hr();
            if (!in_array(false, $createdBan, true)) {
                $this->out('<success>Opération terminée avec succès.</success>');
            } else {
                $this->out('<error>Opération terminée avec erreur(s).</error>');
            }
            $this->hr();
        }
        else if( !empty( $this->params['file']) && !empty( $this->params['update'] ) ) {
            $this->out('<info>Acquisition des adresses en cours ...</info>');
            $this->XProgressBar->start(1);

            $this->updateBanadresseCsv($this->params['file']);

            $this->hr();
            if (!in_array(false, $createdBan, true)) {
                $this->out('<success>Mise à jour terminée avec succès.</success>');
            } else {
                $this->out('<error>Mise à jour terminée avec erreur(s).</error>');
            }
            $this->hr();
        }
    }

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
        $this->Ban = ClassRegistry::init('Ban');
        $departements = $this->Ban->listeDepartements();

        $optionParser = parent::getOptionParser();
        $optionParser->addOption('file', array(
			'short' => 'f',
            'help' => 'Nom du fichier BAN (format CSV) à intégrer'
		));

        $optionParser->addOption('departement', array(
			'short' => 'd',
            'help' => 'Nom du département concerné',
			'choices' => array_keys( $departements )
		));

        $optionParser->addOption('update', array(
			'short' => 'u',
            'help' => 'Mise à jour'
		));

		return $optionParser;
	}



    /*
     * Fonction permettant d'intégrer le fichier BAN d'un département
     *
     *
     */

    private function _processCsv($ban) {
        //infos
        $libelleacheminement = ucwords( $ban['libelle_acheminement'] );
        $infos = array(
            'ident' => $ban['id'],
            'name' => !empty( $libelleacheminement ) ? $libelleacheminement : ucwords( $ban['nom_commune'] ),
            'nom_voie' => ucwords( $ban['nom_voie'] ),
            'id_fantoir' => $ban['id_fantoir'],
            'numero' => $ban['numero'],
            'rep' => $ban['rep'],
            'code_insee' => $ban['code_insee'],
            'code_post' => isset( $ban['code_post'] ) ? $ban['code_post'] : $ban['code_postal'],
            'alias' => $ban['alias'],
            'nom_ld' => $ban['nom_ld'],
            'nom_afnor' => $ban['nom_afnor'],
            'libelle_acheminement' => $ban['libelle_acheminement'],
            'x' => $ban['x'],
            'y' => $ban['y'],
            'lon' => $ban['lon'],
            'lat' => $ban['lat'],
            'nom_commune' => $ban['nom_commune'],
            'canton' => addslashes( @$ban['NOM'] ),
            'active' => 1,
            'code_insee_ancienne_commune' => $ban['code_insee_ancienne_commune'],
            'nom_ancienne_commune' => $ban['nom_ancienne_commune'],
            'source_position' => $ban['source_position'],
            'source_nom_voie' => $ban['source_nom_voie'],
			'date_der_maj' => date('Y-m-d')

        );


        $res = array(
            'infos' => $infos
        );
        return $res;
    }


    /**
     *
     * @param type $filename
     * @param type $ban_id
     * @param type $foreign_key
     * @return boolean
     */
    public function importBanCsv($filename = null, $departement_id = null,  $foreign_key = null) {
        $this->Ban->useDbConfig = $this->_conn;

ini_set( 'memory_limit', '3000M');
        $return = array();
        if ($filename != null && is_file($filename) && is_readable($filename)) {
            if (($handle = fopen($filename, 'r')) !== FALSE) {
                //création du BAN selon le département
                $dept = substr( $departement_id, 0, -1 );
                $listeDepartements = $this->Ban->listeDepartements();
                $libelledir = Hash::get($listeDepartements, $departement_id );
				$ban['Ban']['name'] = strtoupper( replace_accents($libelledir) );

				$banExisting = $this->Ban->find('first', array('conditions' => array('Ban.name' => $ban['Ban']['name'], 'Ban.active' => true), 'contain' => false));
				if(!empty( $banExisting ) ){
					$banAd = $banExisting;
					$this->out("Département déjà créé, mise à jour des données en cours ...");
				}
				else {
					$this->Ban->create();
					$banAd = $this->Ban->save($ban);
					$this->out("Département créé");
				}


                $banscommunes = array();
                $header = NULL;
                $separator = '';
                if( Configure::read('CD') == 81 ) {
                    $separator = ',';
                }
                else {
                    $separator = ';';
                }
                while (($row = fgetcsv($handle, 0, $separator)) !== FALSE) {
                    if (!$header) {
                        $header = $row;
                    } else {
                        $tmp = array();
                        for ($i = 0; $i < count($header); $i++) {
                            $tmp[$header[$i]] = $row[$i];
                        }
                        $banscommunes[] = $tmp;
                    }
                }
                fclose($handle);

                $this->XProgressBar->start(count($banscommunes));
                $this->out('<info>Enregistrement des adresses...</info>');
                foreach ($banscommunes as $bancommune) {
                    $adresseINFO = $this->_processCsv($bancommune);

                    // Sauvegarde des communes
                    $communeINFO = array();
					$communeINFO['Bancommune']['name'] =  mb_strtoupper(html_entity_decode($adresseINFO['infos']['nom_commune']), "UTF-8");
                    $communeINFO['Bancommune']['ban_id'] = $banAd['Ban']['id'];

                    $commune = $this->Ban->Bancommune->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Bancommune.name' => $communeINFO['Bancommune']['name']
                            ),
                            'recursive' => -1
                        )
                    );
                    if( !empty( $commune ) ) {
                        $bancommuneId = $commune['Bancommune']['id'];
                    }
                    else {
                        $this->Ban->Bancommune->create();
                        $savedCommune = $this->Ban->Bancommune->save($communeINFO);
                        $bancommuneId = $this->Ban->Bancommune->id;
                    }


                    // Sauvegarde des adresses
                    if(!empty( $bancommuneId )) {

                        //un seul ficher contact, realiser model contactInfo
                        $adresse = array();
                        // infos
                        $adresse['Banadresse'] = $adresseINFO['infos'];
//                        $adresseINFO['Banadresse']['name'] = $bancommuneId;
                        $adresse['Banadresse']['name'] = $adresseINFO['infos']['numero'].' '.$adresseINFO['infos']['rep'].' '.$adresseINFO['infos']['nom_voie'].' '.$adresseINFO['infos']['code_post'].' '.$adresseINFO['infos']['name'];
                        $adresse['Banadresse']['bancommune_id'] = $bancommuneId;
                        $this->Ban->Bancommune->Banadresse->create();
                        $savedAdresse = $this->Ban->Bancommune->Banadresse->save($adresse);
                        $saved = !empty($savedAdresse);

                        if ($saved) {
                            $return[] = true;
                            $this->Ban->Bancommune->Banadresse->commit();

                        } else {
                            $return[] = false;
                            $this->Ban->Bancommune->Banadresse->rollback();
                        }

                    }
                    $this->XProgressBar->next();
                }

                $this->XProgressBar->finish();
            }
        }
        return !in_array(false, $return, true);
    }


    /**
     *
     * @param type $filename
     * @param type $ban_id
     * @param type $foreign_key
     * @return boolean
     * sudo ./lib/Cake/Console/cake --app app BanInsert process -u true -f app/tmp/BAN_Les_Hautes_Terres_d_Oc.csv -c webgfc_tarn
     */
    public function updateBanadresseCsv($filename = null,  $foreign_key = null) {
        $this->Ban->useDbConfig = $this->_conn;

ini_set( 'memory_limit', '3000M');
        $return = array();
        if ($filename != null && is_file($filename) && is_readable($filename)) {
            if (($handle = fopen($filename, 'r')) !== FALSE) {
                //création du BAN selon le département

                $banscommunes = array();
                $header = NULL;
                $separator = '';
                if( Configure::read('CD') == 81 ) {
                    $separator = ',';
                }
                else {
                    $separator = ';';
                }
                while (($row = fgetcsv($handle, 0, $separator)) !== FALSE) {
                    if (!$header) {
                        $header = $row;
                    } else {
                        $tmp = array();
                        for ($i = 0; $i < count($header); $i++) {
                            $tmp[$header[$i]] = $row[$i];
                        }
                        $banscommunes[] = $tmp;
                    }
                }
                fclose($handle);

                $this->XProgressBar->start(count($banscommunes));
                $this->out('<info>Mise à jour des adresses...</info>');
                foreach ($banscommunes as $bancommune) {
                    $adresseINFO = $this->_processCsv($bancommune);

                    $this->Ban->Bancommune->Banadresse->begin();

                    // Sauvegarde des adresses
                    if(!empty( $adresseINFO )) {
                        $adresse = array();
                        // infos
                        $adresse['Banadresse']['ident'] = $adresseINFO['infos']['ident'];

                        $banadresse = $this->Ban->Bancommune->Banadresse->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Banadresse.ident' => $adresseINFO['infos']['ident']
                                ),
                                'recursive' => -1,
                                'contain' => false,
                            )
                        );
                        if( !empty($banadresse) ) {
                            $saved = $this->Ban->Bancommune->Banadresse->updateAll(
                                array(
                                    'Banadresse.canton' => "'".mb_convert_encoding($adresseINFO['infos']['canton'], 'UTF-8', 'ISO-8859-1')."'"
                                ),
                                array(
                                    'Bandresse.id' => $banadresse['Banadresse']['id']
                                )
                            );
                        }

                        if ($saved) {
                            $return[] = true;
                            $this->Ban->Bancommune->Banadresse->commit();

                        } else {
                            $return[] = false;
                            $this->Ban->Bancommune->Banadresse->rollback();
                        }

                    }
                    $this->XProgressBar->next();
                }

                $this->XProgressBar->finish();
            }
        }
        return !in_array(false, $return, true);
    }

    /**
     *
     * @param type $filename
     * @param type $ban_id
     * @param type $foreign_key
     * @return boolean
     */
    public function importUpdateBanCsv($filename = null, $departement_id = null,  $foreign_key = null) {
        $this->Ban->useDbConfig = $this->_conn;

ini_set( 'memory_limit', '3000M');
        $return = array();
        if ($filename != null && is_file($filename) && is_readable($filename)) {
            if (($handle = fopen($filename, 'r')) !== FALSE) {
                //création du BAN selon le département
                $dept = substr( $departement_id, 0, -1 );
                $listeDepartements = $this->Ban->listeDepartements();
                $libelledir = Hash::get($listeDepartements, $departement_id );
                $ban['Ban']['name'] = strtoupper( replace_accents($libelledir) );

                $banExisting = $this->Ban->find('first', array('conditions' => array('Ban.name' => $ban['Ban']['name'], 'Ban.active' => true), 'contain' => false));
                if(!empty( $banExisting ) ){
                    $banAd = $banExisting;
                    $this->out("Département déjà créé, mise à jour des données en cours ...");
                }
                else {
                    $this->Ban->create();
                    $banAd = $this->Ban->save($ban);
                    $this->out("Département créé");
                }

                $banscommunes = array();
                $header = NULL;
                $separator = '';
                if( Configure::read('CD') == 81 ) {
                    $separator = ',';
                }
                else {
                    $separator = ';';
                }
                while (($row = fgetcsv($handle, 0, $separator)) !== FALSE) {
                    if (!$header) {
                        $header = $row;
                    } else {
                        $tmp = array();
                        for ($i = 0; $i < count($header); $i++) {
                            $tmp[$header[$i]] = $row[$i];
                        }
                        $banscommunes[] = $tmp;
                    }
                }
                fclose($handle);

                $this->XProgressBar->start(count($banscommunes));
                $this->out('<info>Enregistrement des adresses...</info>');
                foreach ($banscommunes as $bancommune) {
                    $adresseINFO = $this->_processCsv($bancommune);

					$this->Ban->Bancommune->Banadresse->begin();

                    // Sauvegarde des communes
                    $communeINFO = array();
                    $communeINFO['Bancommune']['name'] =  mb_strtoupper(html_entity_decode($adresseINFO['infos']['nom_commune']), "UTF-8");
					$communeINFO['Bancommune']['ban_id'] = $banAd['Ban']['id'];

                    $commune = $this->Ban->Bancommune->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Bancommune.name' => strtoupper($communeINFO['Bancommune']['name'])
                            ),
                            'recursive' => -1
                        )
                    );

                    if( !empty( $commune ) ) {
                        $bancommuneId = $commune['Bancommune']['id'];
						$this->Ban->Bancommune->updateAll(
							array(
								'Bancommune.date_der_maj' => "'" . date('Y-m-d H:i:s') . "'"
							),
							array('Bancommune.id' => $bancommuneId)
						);
//						$this->out('<info>Commune mise à jour...</info>');
                    }
                    else {
                        $this->Ban->Bancommune->create();
                        $savedCommune = $this->Ban->Bancommune->save($communeINFO);
                        $bancommuneId = $this->Ban->Bancommune->id;
                        $this->out('<info>Commune créée</info>');
                    }


                    // Sauvegarde des adresses
                    if(!empty( $bancommuneId )) {
                        $Banadressename = $adresseINFO['infos']['numero'].' '.$adresseINFO['infos']['rep'].' '.$adresseINFO['infos']['nom_voie'].' '.$adresseINFO['infos']['code_post'].' '.$adresseINFO['infos']['name'];

						$bansadresseInfo = $this->Ban->Bancommune->Banadresse->find(
							'first',
							array(
								'conditions' => array(
									'Banadresse.bancommune_id' => $bancommuneId,
									'Banadresse.nom_afnor ILIKE' => '%'.$adresseINFO['infos']['nom_afnor'].'%',
									'Banadresse.numero' => $adresseINFO['infos']['numero']
								),
								'recursive' => -1,
								'contain' => false
							)
						);
//$this->Log($bansadresseInfo);
                        // l'adresse existe déjà en base
                        if( !empty( $bansadresseInfo )) {
							if( empty( $bansadresseInfo['Banadresse']['date_der_maj'] ) ) {
								$this->Ban->Bancommune->Banadresse->updateAll(
									array(
										'Banadresse.ident' => "'" . $adresseINFO['infos']['ident'] . "'",
										'Banadresse.name' => "'" . Sanitize::clean($Banadressename, array('encode', false)) . "'",
										'Banadresse.nom_voie' => "'" . Sanitize::clean(ucwords($adresseINFO['infos']['nom_voie']), array('encode', false)) . "'",
										'Banadresse.id_fantoir' => "'" . $adresseINFO['infos']['id_fantoir'] . "'",
										'Banadresse.numero' => "'" . $adresseINFO['infos']['numero'] . "'",
										'Banadresse.rep' => "'" . $adresseINFO['infos']['rep'] . "'",
										'Banadresse.code_insee' => "'" . $adresseINFO['infos']['code_insee'] . "'",
										'Banadresse.code_post' => "'" . isset($adresseINFO['infos']['code_post']) ? $adresseINFO['infos']['code_post'] : $adresseINFO['infos']['code_postal'] . "'",
										'Banadresse.alias' => "'" . Sanitize::clean($adresseINFO['infos']['alias'], array('encode', false)) . "'",
										'Banadresse.nom_ld' => "'" . Sanitize::clean($adresseINFO['infos']['nom_ld'], array('encode', false)) . "'",
										'Banadresse.nom_afnor' => "'" . Sanitize::clean($adresseINFO['infos']['nom_afnor'], array('encode', false)) . "'",
										'Banadresse.libelle_acheminement' => "'" . Sanitize::clean($adresseINFO['infos']['libelle_acheminement'], array('encode', false)) . "'",
										'Banadresse.x' => "'" . $adresseINFO['infos']['x'] . "'",
										'Banadresse.y' => "'" . $adresseINFO['infos']['y'] . "'",
										'Banadresse.lon' => "'" . $adresseINFO['infos']['lon'] . "'",
										'Banadresse.lat' => "'" . $adresseINFO['infos']['lat'] . "'",
										'Banadresse.nom_commune' => "'" . Sanitize::clean(strtoupper($adresseINFO['infos']['nom_commune']), array('encode', false)) . "'",
										'Banadresse.canton' => "'" . Sanitize::clean(addslashes(@$adresseINFO['infos']['NOM']), array('encode', false)) . "'",
										'Banadresse.active' => 1,
										'Banadresse.code_insee_ancienne_commune' => "'" . $adresseINFO['infos']['code_insee_ancienne_commune'] . "'",
										'Banadresse.nom_ancienne_commune' => "'" . Sanitize::clean($adresseINFO['infos']['nom_ancienne_commune'], array('encode', false)) . "'",
										'Banadresse.source_position' => "'" . $adresseINFO['infos']['source_position'] . "'",
										'Banadresse.source_nom_voie' => "'" . $adresseINFO['infos']['source_nom_voie'] . "'",
										'Banadresse.date_der_maj' => "'" . date('Y-m-d H:i:s') . "'"
									),
									array('Banadresse.id' => $bansadresseInfo['Banadresse']['id'])
								);
								$this->out('<info>Adresse mise à jour...</info>');
							}
                            $saved = true;
                        }
                        else {
                            $adresse = array();
                            // infos
                            $adresse['Banadresse'] = $adresseINFO['infos'];
                            $adresse['Banadresse']['name'] = $adresseINFO['infos']['numero'].' '.$adresseINFO['infos']['rep'].' '.$adresseINFO['infos']['nom_voie'].' '.$adresseINFO['infos']['code_post'].' '.$adresseINFO['infos']['name'];
                            $adresse['Banadresse']['nom_commune'] = $adresseINFO['infos']['name'];
                            $adresse['Banadresse']['bancommune_id'] = $bancommuneId;
                            $this->Ban->Bancommune->Banadresse->create();
                            $savedAdresse = $this->Ban->Bancommune->Banadresse->save($adresse);
                            $saved = !empty($savedAdresse);
                            $this->out('<info>Adresse créée...</info>');
                        }

                        if ($saved) {
                            $return[] = true;
                            $this->Ban->Bancommune->Banadresse->commit();

                        } else {
                            $return[] = false;
                            $this->Ban->Bancommune->Banadresse->rollback();
                        }

                    }
                    $this->XProgressBar->next();
                }

                $this->XProgressBar->finish();
            }
        }
        return !in_array(false, $return, true);
    }
}

