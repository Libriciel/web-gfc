<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 *
 * CreateFromFiles shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class MarcheShell extends XShell {


	/**
	 *
	 * @var type
	 */
	public $files = array();


	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @var type
	 */
	public $Marche;

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

        $this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));


		Configure::write('conn', $this->params['connection']);

        ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->Marche = ClassRegistry::init('Marche');
        $this->Marche->useDbConfig = $this->_conn;

	}


	/**
	 *
	 */
	public function main() {

        if( !empty( $this->params['file']) ){
            $createdMarche = array();
            $this->out('<info>Acquisition des marchés en cours ...</info>');
            $this->XProgressBar->start(1);

            $this->importMarcheCsv($this->params['file']);

            $this->hr();
            if (!in_array(false, $createdMarche, true)) {
                $this->out('<success>Opération terminée avec succès.</success>');
            } else {
                $this->out('<error>Opération terminée avec erreur(s).</error>');
            }
            $this->hr();
        }
    }

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
        $this->Marche = ClassRegistry::init('Marche');

        $optionParser = parent::getOptionParser();
        $optionParser->description(__("Outils d'administration"));
        $optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));
        $optionParser->addOption('file', array(
			'short' => 'f',
            'help' => 'Nom du fichier Marche (format CSV) à intégrer'
		));


		return $optionParser;
	}



    /*
     * Fonction permettant d'intégrer le fichier BAN d'un département
     *
     *
     */

    private function _processCsv($marche) {
        //infos
//        debug($marche['N° Marché']);die();
//        $libelleacheminement = ucwords( $marche['libelle_acheminement'] );
        $infos = array(
            'name' => $marche['N° Marché'],
            'numero' => $marche['N° Marché'],
            'objet' => $marche['Objet'],
            'operation_id' => $marche['N° OP'],
            'titulaire' => $marche['Titulaire'],
            'datenotification' => $marche['Date notification'],
            'premiermois' => $marche['Mois M0'],
            'contractant_id' => $marche['Type contractant'],
            'uniterif_id' => $marche['RIF ou autres collect.'],
            'active' => 1

        );


        $res = array(
            'infos' => $infos
        );
        return $res;
    }


    /**
     *
     * @param type $filename
     * @param type $marche_id
     * @param type $foreign_key
     * @return boolean
     */
    public function importMarcheCsv($filename = null, $foreign_key = null) {
        $this->Marche->useDbConfig = $this->_conn;

//ini_set( 'memory_limit', '3000M');
        $return = array();
        if ($filename != null && is_file($filename) && is_readable($filename)) {
            if (($handle = fopen($filename, 'r')) !== FALSE) {

                $marches = array();
                $header = NULL;

                while (($row = fgetcsv($handle, 1000000, ',')) !== FALSE) {
                    if (!$header) {
                        $header = $row;
                    } else {
                        $tmp = array();
                        for ($i = 0; $i < count($header); $i++) {
                            $tmp[$header[$i]] = $row[$i];
                        }
                        $marches[] = $tmp;
                    }
                }
                fclose($handle);

                $this->XProgressBar->start(count($marches));
                $this->out('<info>Enregistrement des marchés...</info>');
               foreach ($marches as $marche) {

                    $marcheINFO = $this->_processCsv($marche);

                    // Reformatage du Mois M0
                    // On récupère les 2 derniers caractères de la colonne Mois M0
                    $getTheYearOnTwoCharacter = substr($marcheINFO['infos']['premiermois'], -2);
                    // On remplace les 2 caractères par 20 + les 2 caractères trouvés
                    $getTheFullYear = str_replace( $getTheYearOnTwoCharacter, '20'.$getTheYearOnTwoCharacter, $getTheYearOnTwoCharacter  );
                    // On récupère les caractères situés avant le - que l'on explose en tableau
                    $getBeforeTheYear = explode("-", $marcheINFO['infos']['premiermois'], 2);
                    // On prend la première entrée du tableau
                    $month = $getBeforeTheYear[0];
                    // On traduit les valeurs de chaque mois par leur numéro dans le calendrier
                    $monthsAbrev = array(
                        'janv.' => '01',
                        'févr.' => '02',
                        'mars' => '03',
                        'avr.' => '04',
                        'mai' => '05',
                        'juin' => '06',
                        'juil.' => '07',
                        'août' => '08',
                        'sept.' => '09',
                        'oct.' => '10',
                        'nov.' => '11',
                        'déc.' => '12'
                    );
                    // On traduit la valeur du mois trouvée avec le tableau précédent
                    $monthWellFormed = $monthsAbrev[$month];
                    // On concatène les informations pour les enregistrer
                    $marcheINFO['infos']['premiermois'] = $monthWellFormed.$getTheFullYear;


                    $operation = $this->Marche->Operation->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Operation.name' => $marcheINFO['infos']['operation_id']
                            ),
                            'recursive' => -1
                        )
                    );
                    if( !empty($operation) ) {
                        $operationId = $operation['Operation']['id'];
                        $marcheINFO['infos']['operation_id'] = $operationId;


                                // Sauvegarde des unités RIF
                        $uniterifINFO = array();
                        $uniterifINFO['Uniterif']['name'] = $marcheINFO['infos']['uniterif_id'];
                        $uniterif = $this->Marche->Uniterif->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Uniterif.name' => $uniterifINFO['Uniterif']['name']
                                ),
                                'recursive' => -1
                            )
                        );
                        if( !empty($uniterif) ) {
                            $UniterifId = $uniterif['Uniterif']['id'];
                        }
                        else {
                            $this->Marche->Uniterif->create();
                            $this->Marche->Uniterif->save($uniterifINFO);
                            $UniterifId = $this->Marche->Uniterif->id;
                        }
                        $marcheINFO['infos']['uniterif_id'] = $UniterifId;

                        // Sauvegarde des types de contractant
                        $contractantINFO = array();
                        $contractantINFO['Contractant']['name'] = $marcheINFO['infos']['contractant_id'];
                        $contractant = $this->Marche->Contractant->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Contractant.name' => $contractantINFO['Contractant']['name']
                                ),
                                'recursive' => -1
                            )
                        );
                        if( !empty($contractant) ) {
                            $ContractantId = $contractant['Contractant']['id'];
                        }
                        else {
                            $this->Marche->Contractant->create();
                            $this->Marche->Contractant->save($contractantINFO);
                            $ContractantId = $this->Marche->Contractant->id;
                        }
                        $marcheINFO['infos']['contractant_id'] = $ContractantId;





                        $marche = $marcheINFO['infos'];
                        $marcheExist = $this->Marche->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Marche.name' => $marche['name'],
                                    'Marche.operation_id' => $marche['operation_id'],
                                    'Marche.titulaire' => $marche['titulaire']
                                ),
                                'recursive' => -1
                            )
                        );
                        if( !empty($marcheExist) ) {
//                            $marcheId = $marcheExist['Marche']['id'];
                            $saved = false;
                        }
                        else {
                            $this->Marche->create();
                            $saved = $this->Marche->save($marche);
                        }


                        if ($saved) {
                            $return[] = true;
                            $this->Marche->commit();

                        } else {
                            $return[] = false;
                            $this->Marche->rollback();
                        }
                        $this->XProgressBar->next();
                    }

                }

                $this->XProgressBar->finish();
            }
        }
        return !in_array(false, $return, true);
    }


}
