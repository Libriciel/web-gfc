<?php

/**
 *
 * Organismes/get_organismes.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author yjin
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//debug($contacts);
//debug($organisme_id);
$this->Paginator->options(array(
   'update' => '#zone_contact .panel-body',
   'evalScripts' => true,
   'before' => 'gui.loader({ element: $("#zone_contact .panel-body"), message: gui.loaderMessage });',
   'complete' => '$(".loader").remove()'
   ));
$pagination = '';
if (Set::classicExtract($this->params, 'paging.Contact.pageCount') > 1) {
   $pagination = '<div class="pagination">
   ' . implode(
     '', Set::filter(
       array(
          $this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
          $this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
          )
       )
     ) . '
   </div>';
}
?>
<?php echo "<b>Nom de l'organisme lié : </b>".$organismename . ' - '.$organismeville;?>
<table id="table_contact"
       data-toggle="table"
       data-locale = "fr-CA"
       data-height="459"
       >
</table>
<div>
    <?php echo $pagination; ?>
    <?php if(!empty($contacts)):
        $formSearch = array(
            'name' => 'exportContact',
            'label_w' => 'col-sm-4',
            'input_w' => 'col-sm-6',
            'form_url' =>array('controller' => 'contacts', 'action' => 'export'),
            'form_class'=>'pull-right',
            'input' => array(
                'SearchContact.organisme_id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden',
                        'value'=>$organisme_id
                    )
                )
            )
        );
         echo $this->Formulaire->createForm($formSearch);
    ?>
    <button type="submit" class="btn btn-info-webgfc pull-right" id="exportResultat" style="margin:20px auto;"><i class="fa fa-download" aria-hidden="true"></i></button>
    <?php
    $this->Form->end();
    endif; ?>
</div>
<script>

    $(document).ready(function () {
        $('#table_contact')
                .bootstrapTable({
                    data: <?php echo json_encode($contacts); ?>,
                    columns: [
                        {
                            field: "name",
                            title: "Nom",
                            class: "name"
                        },
                        {
                            field: "active",
                            title: "<?php echo __d('contact', 'Contact.active'); ?>",
                            class: "active_column"
                        },
                        {
                            field: "view",
                            title: "",
                            align: "center"

                        },
                        {
                            field: "edit",
                            title: "",
                            align: "center"
                        },
                        {
                            field: "delete",
                            title: "",
                            align: "center"
                        },
                        {
                            field: "exportcsv",
                            title: "",
                            align: "center"
                        }
                    ]
                }).on('search.bs.table', function (e, text) {
            chargerFunctionJquery();
        });
        chargerFunctionJquery();
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
    });
    function chargerFunctionJquery() {
        // $.getScript("/js/addressbook.js");

        // visualisation des contacts
        /*$('.viewContact').click(function () {
            var id = $(this).attr('id').substring(8);
            var href = '/contacts/view/' + id;
            var chargeZone = '#infos .content';
            viewItem(href, chargeZone);
        });
        function viewItem(href, chargeZone) {
            gui.request({
                url: href,
                updateElement: $(chargeZone),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
            if (chargeZone == "#infos .content") {
                $('#infos .panel-title').empty();
                $('#infos').modal('show');
            }
        }

        // édition des contacts
        $('.editContact').click(function () {
            var id = $(this).attr('id').substring(8);
            var href = '/contacts/edit/' + id;
            editItem(href);
        });
        function editItem(href) {
            $('#infos').modal('show');
            gui.request({
                url: href,
                updateElement: $('#infos .content'),
                loader: true,
                loaderShowDiv: $('#liste .content'),
                loaderMessage: gui.loaderMessage
            });
        }*/

        // export des contacts
        $('.exportContact').click(function () {
            var id = $(this).attr('id').substring(8);
            var href = '/contacts/export/' + id;
            exportItem(href);
        });
        function exportItem(href) {
            window.location.href = href;
        }
    }

	$('.editContact').click(function () {
		var id = $(this).attr('id').substring(8);
		var href = '/contacts/edit/' + id;
		var isSansContact = $(this).parent().parent().text();
		isSansContact.includes("Sans contact");
		if( !isSansContact.includes("Sans contact") ) {
			editItemContact(href);
		}
		else {
			var title = "Modifier le contact";
			var text = "Ce contact ne peut être modifié";
			swal(
				'Oops...',
				title + "<br>" + text,
				'warning'
			);
		}
	});

	$('.deleteContact').click(function () {
		var id = $(this).attr('id').substring(8);
		var href = '/contacts/delete/' + id;
		var organismeId = $(this).attr('organismeId');
		var section = "contact";
		deleteItem(href, section, organismeId);
	});

	function editItemContact(href) {
		gui.formMessage({
			url: href,
			loader: true,
			loaderMessage: gui.loaderMessage,
			updateElement: $('#ContactEdit').parents('fieldset'),
			width: 700,
			buttons: {
				"<?php echo __d('default', 'Button.cancel'); ?>": function () {
					$(this).parents('.modal').modal('hide');
					$(this).parents('.modal').empty();
				},
				"<?php echo __d('default', 'Button.submit'); ?>": function () {
					var form = $('#ContactEditForm');
					gui.request({
						url: form.attr('action'),
						data: form.serialize()
					}, function (data) {
						contactInfos = jQuery.parseJSON(data);
						layer.msg("Les informations du contact ont bien été enregistrées");
					});
					$(this).parents('.modal').modal('hide');
					$(this).parents('.modal').empty();
				}
			}
		});
	}



	$(document).ready(function () {
        addClassActive();
    });

    function addClassActive() {
        $('#table_contact .active_column').hide();
        $('#table_contact .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }
</script>
<?php echo $this->Js->writeBuffer();?>
