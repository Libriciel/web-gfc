<?php

/**
 *
 * Organismes/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
echo $this->Html->script('jquery/jquery.ui.autocomplete.html.js', array('inline' => true));
?>
<div class="row">
  <?php
    $formOrganisme = array(
        'name' => 'Organisme',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Organisme.addressbook_id' => array(
                'labelText' =>__d('organisme', 'Organisme.addressbook_id'),
                'inputType' => 'select',
                'items'=>array(
                    'required'=>true,
                    'options' => $addressbooks,
                    'type'=>'select'
                )
            ),
            'Organisme.infoscontact' => array(
                'labelText' =>__d('organisme', 'Organisme.infoscontact'),
                'inputType' => 'checkbox',
                'items'=>array(
                    'type'=>'checkbox'
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formOrganisme);
  ?>
    <hr/>
    <div class="OrganismeInfo col-sm-6">
        <?php
    $formOrganismeInfos = array(
        'name' => 'Organisme',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-7',
        'input' => array(
            'Organisme.name' => array(
                'labelText' =>__d('organisme', 'Organisme.name'),
                'inputType' => 'text',
                'items'=>array(
                    'required'=>true,
                    'type'=>'text'
                )
            ),
            'Organisme.email' => array(
                'labelText' =>__d('organisme', 'Organisme.email'),
                'inputType' => 'email',
                'items'=>array(
                    'type'=>'email'
                )
            ),
            'Organisme.tel' => array(
                'labelText' =>__d('organisme', 'Organisme.tel'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Organisme.portable' => array(
                'labelText' =>__d('organisme', 'Organisme.portable'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Organisme.fax' => array(
                'labelText' =>__d('organisme', 'Organisme.fax'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Organisme.antenne' => array(
                'labelText' =>__d('organisme', 'Organisme.antenne'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Organisme.siret' => array(
                'labelText' =>__d('organisme', 'Organisme.siret'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Activite.Activite' => array(
                'labelText' =>__d('organisme', 'Organisme.activite_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'empty' => true,
                    'multiple' => true,
                    'options' => $activites
                )
            )
        )
    );
    if( Configure::read( 'Conf.SAERP') ) {
//        $formOrganismeInfos['input']['Activite.Activite'] = array(
//            'labelText' =>__d('organisme', 'Organisme.activite_id'),
//            'inputType' => 'select',
//            'items'=>array(
//                'type'=>'select',
//                'empty' => true,
//                'multiple' => true,
//                'options' => $activites
//            )
//        );
        $formOrganismeInfos['input']['Operation.Operation'] = array(
            'labelText' =>__d('organisme', 'Organisme.operation_id'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'empty' => true,
                'multiple' => true,
                'options' => $operations
            )
        );
    }
    echo $this->Formulaire->createForm($formOrganismeInfos);
    ?>
    </div>
    <div class="OrganismeAdresse col-sm-6">
  <?php
        $formOrganismeAdd = array(
            'name' => 'Organisme',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-6',
            'input' => array(
                'Organisme.adressecomplete' => array(
                    'labelText' =>__d('contact', 'Contact.adressecompleteLable'),
                    'inputType' => 'text',
                    'items'=>array(
                        'title'=>__d('contact', 'Contact.adressecompleteTitle'),
                        'type'=>'text'
                    )
                ),
                'Organisme.ban_id' => array(
                    'labelText' =>__d('organisme', 'Organisme.ban_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'options' => array()
                    )
                ),
                'Organisme.bancommune_id' => array(
                    'labelText' =>__d('organisme', 'Organisme.bancommune_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'options' => array()
                    )
                ),
                'Organisme.banadresse' => array(
                    'labelText' =>__d('organisme', 'Organisme.banadresse'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'options' => array()
                    )
                ),
                'Organisme.numvoie' => array(
                    'labelText' =>__d('organisme', 'Organisme.numvoie'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.nomvoie' => array(
                    'labelText' =>__d('organisme', 'Organisme.nomvoie'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.compl' => array(
                    'labelText' =>__d('organisme', 'Organisme.compl'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
				'Organisme.compl2' => array(
					'labelText' =>__d('organisme', 'Organisme.compl2'),
					'inputType' => 'text',
					'items'=>array(
						'type'=>'text'
					)
				),
                'Organisme.cp' => array(
                    'labelText' =>__d('organisme', 'Organisme.cp'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.ville' => array(
                    'labelText' =>__d('organisme', 'Organisme.ville'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
				'Organisme.cedex' => array(
					'labelText' =>__d('organisme', 'Organisme.cedex'),
					'inputType' => 'text',
					'items'=>array(
						'type'=>'text'
					)
				),
                'Organisme.pays' => array(
                    'labelText' =>__d('organisme', 'Organisme.pays'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                )
            )
        );
        if( Configure::read('CD') == 81 ) {
            $formOrganismeAdd['input']['Organisme.canton'] = array(
                'labelText' =>__d('organisme', 'Organisme.canton'),
                'inputType' => 'text',
                'items'=>array(
                    'title'=>__d('organisme', 'Organisme.canton'),
                    'type'=>'text'
                )
            );
        }
        echo $this->Formulaire->createForm($formOrganismeAdd);
        asort($civilite);
    ?>
    </div>
    <div class="infoscontact">
        <div class="ContactInfos col-sm-6">
            <legend>Contact associé</legend>
            <?php
                $formContactInfos = array(
                    'name' => 'Organisme',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-7',
                    'input' => array(
                        'Contact.civilite' => array(
                            'labelText' =>__d('addressbook', 'Contact.civilite'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'empty' => true,
                                'options' => $civilite
                            )
                        ),
                        'Contact.nom' => array(
                            'labelText' =>__d('contact', 'Contact.nom'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.prenom' => array(
                            'labelText' =>__d('contact', 'Contact.prenom'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.name' => array(
                            'labelText' =>__d('contact', 'Contact.name'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.email' => array(
                            'labelText' =>__d('contact', 'Contact.email'),
                            'inputType' => 'email',
                            'items'=>array(
                                'type'=>'email'
                            )
                        ),
                        'Contact.tel' => array(
                            'labelText' =>__d('contact', 'Contact.tel'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.portable' => array(
                            'labelText' =>__d('contact', 'Contact.portable'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.lignedirecte' => array(
                            'labelText' =>__d('contact', 'Contact.lignedirecte'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        )
                    )
                );
                if( Configure::read('Conf.SAERP')) {
                    $formContactInfos['input']['Contact.Operation'] = array(
                        'labelText' =>__d('contact', 'Contact.operation_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'empty' => true,
                            'multiple' => true,
                            'options' => $operations
                        )
                    );
                    $formContactInfos['input']['Contact.Event'] = array(
                        'labelText' =>__d('contact', 'Contact.event_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'empty' => true,
                            'multiple' => true,
                            'options' => $events
                        )
                    );
                    $formContactInfos['input']['Contact.observations'] = array(
                        'labelText' =>__d('contact', 'Contact.observations'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    );
                    $formContactInfos['input']['Contact.fonction_id'] = array(
                        'labelText' =>__d('contact', 'Contact.fonction_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'empty' => true,
                            'multiple' => true,
                            'options' => $fonctions
                        )
                    );
                }else{
                    $formContactInfos['input']['Contact.role'] = array(
                        'labelText' =>__d('contact', 'Contact.role'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    );
					$formContactInfos['input']['Contact.titre_id'] = array(
						'labelText' =>__d('contact', 'Contact.titre_id'),
						'inputType' => 'select',
						'items'=>array(
							'type'=>'select',
							'empty' => true,
							'options' => $titres
						)
					);
                }
                echo $this->Formulaire->createForm($formContactInfos);
            ?>
        </div>
        <div class="ContactAdd col-sm-6">
            <legend><?php echo __d('contact', 'Contact.adresses') ?></legend>
            <?php
                $formContactAdd = array(
                    'name' => 'Organisme',
                    'label_w' => 'col-sm-6',
                    'input_w' => 'col-sm-6',
                    'input' => array(
                        'Contact.adressecomplete' => array(
                            'labelText' =>__d('contact', 'Contact.adressecompleteLable'),
                            'inputType' => 'text',
                            'items'=>array(
                                'title'=>__d('contact', 'Contact.adressecompleteTitle'),
                                'type'=>'text'
                            )
                        ),
                        'Contact.ban_id' => array(
                            'labelText' =>__d('contact', 'Contact.ban_id'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'empty' => true,
                                'options' => array()
                            )
                        ),
                        'Contact.bancommune_id' => array(
                            'labelText' =>__d('contact', 'Contact.bancommune_id'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'empty' => true,
                                'options' => array()
                            )
                        ),
                        'Contact.banadresse' => array(
                            'labelText' =>__d('contact', 'Contact.banadresse'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'empty' => true,
                                'options' => array()
                            )
                        ),
                        'Contact.numvoie' => array(
                            'labelText' =>__d('contact', 'Contact.numvoie'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.nomvoie' => array(
                            'labelText' =>__d('contact', 'Contact.nomvoie'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.compl' => array(
                            'labelText' =>__d('contact', 'Contact.compl'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
						'Contact.compl2' => array(
							'labelText' =>__d('contact', 'Contact.compl2'),
							'inputType' => 'text',
							'items'=>array(
								'type'=>'text'
							)
						),
                        'Contact.cp' => array(
                            'labelText' =>__d('contact', 'Contact.cp'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.ville' => array(
                            'labelText' =>__d('contact', 'Contact.ville'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
						'Contact.cedex' => array(
							'labelText' =>__d('contact', 'Contact.cedex'),
							'inputType' => 'text',
							'items'=>array(
								'type'=>'text'
							)
						),
                        'Contact.pays' => array(
                            'labelText' =>__d('contact', 'Contact.pays'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        )
                    )
                );
                if( Configure::read('CD') == 81 ) {
                    $formContactAdd['input']['Contact.canton'] = array(
                        'labelText' =>__d('contact', 'Contact.canton'),
                        'inputType' => 'text',
                        'items'=>array(
                            'title'=>__d('contact', 'Contact.canton'),
                            'type'=>'text'
                        )
                    );
                }
                echo $this->Formulaire->createForm($formContactAdd);
            ?>
        </div>
    </div>
    <?php
    echo $this->Form->end();
    ?>
</div>
<script type="text/javascript">
    $('#OrganismeAddressbookId').select2({allowClear: true, placeholder: "Sélectionner un carnet d\'adresse"});
    $('#OrganismeBanId').select2({allowClear: true, placeholder: "Sélectionner un département"});
    $('#OrganismeBancommuneId').select2({allowClear: true, placeholder: "Sélectionner une commune"});
    $('#OrganismeBanadresse').select2({allowClear: true, placeholder: "Sélectionner une adresse"});
    $('#ContactCivilite').select2({allowClear: true, placeholder: "Sélectionner une civilité"});
    $('#ContactBanId').select2({allowClear: true, placeholder: "Sélectionner un département"});
    $('#ContactBancommuneId').select2({allowClear: true, placeholder: "Sélectionner une commune"});
    $('#ContactBanadresse').select2({allowClear: true, placeholder: "Sélectionner une adresse"});

    // Cas à cocher des infos organisme
    $('#OrganismeInfoscontact').change(function () {
        if ($('#OrganismeInfoscontact').attr('checked') == 'checked') {
            $('.infoscontact').show();
            $('#ContactOrganismeId').parent().parent().hide();
        }
        else {
            $('.infoscontact').hide();
            $('#ContactOrganismeId').parent().parent().show();
        }
    });

    if ($('#OrganismeInfoscontact').attr('checked') == 'checked') {
        $('.infoscontact').show();
        $('#ContactOrganismeId').parent().hide();
    }
    else {
        $('.infoscontact').hide();
        $('#ContactOrganismeId').parent().show();
    }

    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this,
                    currentCategory = "";
            $.each(items, function (index, item) {
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                self._renderItem(ul, item);
            });

        }
    });
    //contruction du tableau de types / soustypes
    var bans = [];
    <?php foreach ($bans as $ban) { ?>
    var ban = {
        id: "<?php echo $ban['Ban']['id']; ?>",
        name: "<?php echo $ban['Ban']['name']; ?>",
        banscommunes: []
    };
        <?php foreach ($ban['Bancommune'] as $communeB) { ?>
    var bancommune = {
        id: "<?php echo $communeB['id']; ?>",
        name: "<?php echo $communeB['name']; ?>"
    };
    ban.banscommunes.push(bancommune);
        <?php } ?>
    bans.push(ban);
    <?php } ?>

    function fillBanscommunes(ban_id, bancommune_id) {
        $("#OrganismeBancommuneId").empty();
        $("#OrganismeBancommuneId").append($("<option value=''> --- </option>"));

        for (i in bans) {
            if (bans[i].id == ban_id) {
                for (j in bans[i].banscommunes) {
                    if (bans[i].banscommunes[j].id == bancommune_id) {
                        $("#OrganismeBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "' selected='selected'>" + bans[i].banscommunes[j].name + "</option>"));
                    } else {
                        $("#OrganismeBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "'>" + bans[i].banscommunes[j].name + "</option>"));
                    }
                }
            }
        }
    }

    //remplissage de la liste des types
    for (i in bans) {
        $("#OrganismeBanId").append($("<option value='" + bans[i].id + "'>" + bans[i].name + "</option>"));
    }

    //definition de l action sur type
    $("#OrganismeBanId").bind('change', function () {
        var ban_id = $('option:selected', this).attr('value');
        fillBanscommunes(ban_id);
    });

    var checkAdresses = function (bancommune_id) {
        $("#OrganismeBanadresse").empty();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Organismes/getAdresses/' + bancommune_id,
            data: $('#OrganismeAddForm').serialize(),
            success: function (data) {
                if (data.length > 0) {
                    $("#OrganismeBanadresse").append($("<option value=''> --- </option>"));
                    // remplissage de la partie adresse
                    for (var i in data) {
                        $("#OrganismeBanadresse").append($("<option value='" + data[i].Banadresse.nom_afnor + "'>" + data[i].Banadresse.nom_afnor + "</option>"));
                    }
                }
            }
        });
    }


    $("#OrganismeBancommuneId").bind('change', function () {
        var bancommune_id = $('option:selected', this).attr('value');
        $("#ContactBanId").val($("#OrganismeBanId").attr('value')).change();
        $("#ContactBancommuneId").val($("#OrganismeBancommuneId").attr('value')).change();
        checkAdresses(bancommune_id);
    });

    $('#OrganismeInfoscontactBtn').click(function () {
        $('.infoscontact').toggle();
        if ($('.infoscontact').css('display') == 'block') {
            $('#OrganismeInfoscontact').val(1);
            $('#OrganismeInfoscontactBtn i').removeClass("fa-toggle-off").addClass("fa-toggle-on");
        } else {
            $('#OrganismeInfoscontactBtn i').removeClass("fa-toggle-on").addClass("fa-toggle-off");
        }
    });

    var checkHomonyms = function () {
        $('.homonyms').remove();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Contacts/getHomonyms',
            data: $('#OrganismeAddForm').serialize(),
            success: function (data) {

                if (data.length > 0) {
                    var homonyms = $('<div></div>').html('<?php echo __d('contact', 'Contact.HomonymsListTitle'); ?>').addClass('homonyms').addClass('alert alert-warning');
                    var homonymList = $('<ul></ul>');

                    for (var i in data) {
                        homonymList.append($('<li></li>').html(data[i].Contact.name + ' (' + data[i].Addressbook.name + ' / ' + data[i].Organisme.name + ')'));
                    }
                    homonyms.append(homonymList);

                    $('#ContactName').parent().after(homonyms);

                }
            }
        });

    }
    $('#ContactNom, #ContactPrenom').blur(function () {
        if ($('#ContactPrenom').val() != "" || $('#ContactNom').val() != "") {
            $('#ContactName').val($('#ContactPrenom').val() + " " + $('#ContactNom').val());
            checkHomonyms();
        }
    });

    $('#ContactName').change(function () {
        checkHomonyms();
    });
    $('#OrganismeAddForm').addClass('checkHomonyms');

    // Pour les organismes
    var checkHomonymsOrgs = function () {
        $('.homonymsorgs').remove();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Organismes/getHomonymsOrgs',
            data: $('#OrganismeAddForm').serialize(),
            success: function (data) {

                if (data.length > 0) {
                    var homonymsorgs = $('<div></div>').html('<?php echo __d('organisme', 'Organisme.HomonymsListTitle'); ?>').addClass('homonymsorgs').addClass('alert alert-warning');
                    var homonymorgList = $('<ul></ul>');

                    for (var i in data) {
                        homonymorgList.append($('<li></li>').html(data[i].Organisme.name + ' (' + data[i].Addressbook.name + ')'));
                    }
                    homonymsorgs.append(homonymorgList);

                    $('#OrganismeName').parent().after(homonymsorgs);

                }
            }
        });

    }

    $('#OrganismeName').blur(function () {
        checkHomonymsOrgs();
    });
    $('#OrganismeAddForm').addClass('checkHomonymsOrgs');

    function fillBanscommunesContact(ban_id, bancommune_id) {
        $("#ContactBancommuneId").empty();
        $("#ContactBancommuneId").append($("<option value=''> --- </option>"));

        for (i in bans) {
            if (bans[i].id == ban_id) {
                for (j in bans[i].banscommunes) {
                    if (bans[i].banscommunes[j].id == bancommune_id) {
                        $("#ContactBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "' selected='selected'>" + bans[i].banscommunes[j].name + "</option>"));
                    } else {
                        $("#ContactBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "'>" + bans[i].banscommunes[j].name + "</option>"));
                    }
                }
            }
        }
    }


    $("#ContactBanId").append($("<option value=''> --- </option>"));
    //remplissage de la liste des types
    for (i in bans) {
        $("#ContactBanId").append($("<option value='" + bans[i].id + "'>" + bans[i].name + "</option>"));
    }
    //definition de l action sur type
    $("#ContactBanId").bind('change', function () {
        var ban_id = $('option:selected', this).attr('value');
        fillBanscommunesContact(ban_id);
    });

    var checkAdressesContact = function (bancommune_id) {
        $("#ContactBanadresse").empty();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Organismes/getAdresses/' + bancommune_id,
            data: $('#ContactAddForm').serialize(),
            success: function (data) {
                if (data.length > 0) {
                    $("#ContactBanadresse").append($("<option value=''> --- </option>"));
                    // remplissage de la partie adresse
                    for (var i in data) {
                        $("#ContactBanadresse").append($("<option value='" + data[i].Banadresse.nom_afnor + "'>" + data[i].Banadresse.nom_afnor + "</option>"));
                    }
                }
            }
        });
    }

    $("#ContactBancommuneId").bind('change', function () {
        var bancommune_id = $('option:selected', this).attr('value');
        checkAdressesContact(bancommune_id);
    });

    <?php if( !Configure::read('Conf.SAERP')) { ?>
    $("#OrganismeBanId").val("<?php echo isset( $bans[0]['Ban']['id'] ) ? $bans[0]['Ban']['id'] : ''  ?>").change();
    $("#ContactBanId").val("<?php echo isset( $bans[0]['Ban']['id'] ) ? $bans[0]['Ban']['id'] : ''  ?>").change();
     <?php } ?>


    $("#OrganismeBanId").val("---").change();
    $("#ContactBanId").val("---").change();

    // Using jQuery UI's autocomplete widget:
    $('#OrganismeAdressecomplete').bind("keydown", function (event) {
        //keycodes - maj : 16, ctrl : 17, alt : 18
        if (event.keyCode != 16 &&
                event.keyCode != 17 &&
                event.keyCode != 18) {
            $('#OrganismeBanId').val(0);
            $('#OrganismeBancommuneId').html('');
        }
    }).catcomplete({
        minLength: 3,
        source: '/contacts/searchAdresse',
        select: function (event, ui) {
            gui.request({
                url: '/contacts/searchAdresseSpecifique/' + ui.item.id
            }, function (data) {
                $('#OrganismeBancommuneId').val(ui.item.id);
                var json = jQuery.parseJSON(data);
                var banadresse = json['Banadresse'];
                var ban = json['Ban'];
                var banville = json['Bancommune'];
                var num = banadresse['numero'];
                if (typeof banadresse['rep'] !== 'undefined') {
                    num = banadresse['numero'] + " " + banadresse['rep'];
                }
                if (typeof banadresse['id'] !== 'undefined') {
                    $('#OrganismeBanId').val(ban['id']).change();
                    $('#OrganismeBancommuneId').val(banville['id']).change();
                    $("#OrganismeBanadresse").append($("<option value='" + banadresse['nom_afnor'] + "' selected='selected'>" + banadresse['nom_afnor'] + "</option>"));
                    $("#OrganismeBanadresse").val(banadresse['nom_afnor']).change();
                    $('#OrganismeNumvoie').val(num);
                    $('#OrganismeNomvoie').val(banadresse['nom_afnor']);
                    $('#OrganismeCp').val(banadresse['code_post']);
                    $('#OrganismeVille').val(banville['name']);
                    <?php if( Configure::read('CD') == 81 ):?>
                        $('#OrganismeCanton').val(banadresse['canton']);
                    <?php endif;?>

                    $('#ContactBanId').val(ban['id']).change();
                    $('#ContactBancommuneId').val(banville['id']).change();
                    $("#ContactBanadresse").append($("<option value='" + banadresse['nom_afnor'] + "' selected='selected'>" + banadresse['nom_afnor'] + "</option>"));
                    $("#ContactBanadresse").val(banadresse['nom_afnor']).change();
                    $('#ContactNumvoie').val(num);
                    $('#ContactNomvoie').val(banadresse['nom_afnor']);
                    $('#ContactCp').val(banadresse['code_post']);
                    $('#ContactVille').val(banville['name']);
                    <?php if( Configure::read('CD') == 81 ):?>
                        $('#ContactCanton').val(banadresse['canton']);
                    <?php endif;?>
                }
            });
        }
    });

    // Partie Organisme
    // Partie pour mettre à jour le code postal selon le nom de voie sélectionné
	if( $("#OrganismeBancommuneId").val() != null ) {
		$('#OrganismeBanadresse').change(function () {
			refreshCodepostalOrganisme($("#OrganismeBancommuneId").val(), $("#OrganismeBanadresse").val());
		});
	}
    var refreshCodepostalOrganisme = function (bancommuneId, banadresseName) {
        var params = '';
        if (banadresseName != '') {
            var params = bancommuneId + '/' + banadresseName;
        }
        else {
            params = bancommuneId;
        }
        gui.request({
            url: '/bansadresses/getCodepostal/' + params
        }, function (data) {
            var json = jQuery.parseJSON(data);
            var codepostal = json;
            $('#OrganismeNomvoie').val(codepostal['nom_afnor']);
            $('#OrganismeCp').val(codepostal['codepostal']);
            $('#OrganismeVille').val(codepostal['ville']);
            <?php if( Configure::read('CD') == 81 ):?>
                $('#OrganismeCanton').val(codepostal['canton']);
            <?php endif;?>


            $("#ContactBanadresse").append($("<option value='" + codepostal['nom_afnor'] + "' selected='selected'>" + codepostal['nom_afnor'] + "</option>"));
            $("#ContactBanadresse").val(codepostal['nom_afnor']).change();
        });
    }

    // Partie Contact
	if( $("#ContactBancommuneId").val() != null ) {
		$('#ContactBanadresse').change(function () {
			refreshCodepostal($("#ContactBancommuneId").val(), $("#ContactBanadresse").val());
		});
	}
    var refreshCodepostal = function (bancommuneId, banadresseName) {
        var params = '';
        if (banadresseName != '') {
            var params = bancommuneId + '/' + banadresseName;
        }
        else {
            params = bancommuneId;
        }
        gui.request({
            url: '/bansadresses/getCodepostal/' + params
        }, function (data) {
            var json = jQuery.parseJSON(data);
            $('#ContactNomvoie').val(json['nom_afnor']);
            $('#ContactCp').val(json['codepostal']);
            $('#ContactVille').val(json['ville']);
            <?php if( Configure::read('CD') == 81 ):?>
                $('#ContactCanton').val(json['canton']);
            <?php endif;?>

        });
    }

    $('#OrganismeNumvoie').change(function () {
        $('#ContactNumvoie').val($('#OrganismeNumvoie').val());
    });
    $('#OrganismeNomvoie').change(function () {
        $('#ContactNomvoie').val($('#OrganismeNomvoie').val());
    });
    $('#OrganismeCompl').change(function () {
        $('#ContactCompl').val($('#OrganismeCompl').val());
    });
	$('#OrganismeCompl2').change(function () {
		$('#ContactCompl2').val($('#OrganismeCompl2').val());
	});
	$('#OrganismeCedex').change(function () {
		$('#ContactCedex').val($('#OrganismeCedex').val());
	});
    $('#OrganismeCp').change(function () {
        $('#ContactCp').val($('#OrganismeCp').val());
    });
    $('#OrganismeVille').change(function () {
        $('#ContactVille').val($('#OrganismeVille').val());
    });
    $('#OrganismePays').change(function () {
        $('#ContactPays').val($('#OrganismePays').val())
    });
    <?php if(Configure::read('CD') == 81 ) :?>
        $('#OrganismeCanton').change(function () {
            $('#ContactCanton').val($('#OrganismeCanton').val())
        });
    <?php endif;?>

$('#ContactTitreId').select2({allowClear: true, placeholder: "Sélectionner un titre"});
$('#ActiviteActivite').select2({allowClear: true, placeholder: "Sélectionner une activité"});
<?php if( Configure::read('Conf.SAERP') ) :?>
    $('#OperationOperation').select2({allowClear: true, placeholder: "Sélectionner une OP"});
    $('#ContactOperation').select2({allowClear: true, placeholder: "Sélectionner une OP"});
    $('#ContactEvent').select2({allowClear: true, placeholder: "Sélectionner un événement"});
    $('#ContactFonctionId').select2({allowClear: true, placeholder: "Sélectionner une fonction"});
<?php endif;?>
</script>
