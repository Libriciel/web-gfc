<?php

/**
 *
 * Organismes/get_organismes.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$this->Paginator->options(array(
   'update' => '#zone_contact .panel-body',
   'evalScripts' => true,
   'before' => 'gui.loader({ element: $("#zone_contact .panel-body"), message: gui.loaderMessage });',
   'complete' => '$(".loader").remove()'
   ));
$pagination = '';
if (Set::classicExtract($this->params, 'paging.Contact.pageCount') > 1) {
   $pagination = '<div class="pagination">
   ' . implode(
     '', Set::filter(
       array(
          $this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
          $this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
          )
       )
     ) . '
   </div>';
}
?>

<table id="table_contact"
       data-toggle="table"
       data-locale = "fr-CA"
       data-height="499"
       >
</table>
<p><?php echo $pagination; ?></p>
<script>
    $('#table_contact')
            .bootstrapTable({
                data: <?php echo json_encode($contacts); ?>,
                columns: [
                    {
                        field: "name",
                        title: "Nom",
                        class: "name"
                    },
//                    {
//                        field: "view",
//                        title: "",
//                        align: "center"
//
//                    },
                    {
                        field: "edit",
                        title: "",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "",
                        align: "center"
                    },
                    {
                        field: "exportcsv",
                        title: "",
                        align: "center"
                    }
                ]
            }).on('search.bs.table', function (e, text) {
        chargerFunctionJquery();
    });
    $(document).ready(function () {
        chargerFunctionJquery();
    });
    function chargerFunctionJquery() {
        $.getScript("/js/addressbook.js");
    }
</script>
<?php echo $this->Js->writeBuffer();?>
