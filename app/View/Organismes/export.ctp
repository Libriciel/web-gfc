<?php
	$this->Csv->preserveLeadingZerosInExcel = true;

//    $this->Csv->addRow( array( "Informations sur l'organisme : ".$results['Organisme']['name'] ));
    $this->Csv->addRow( array( "Informations sur les organismes" ));

    $this->Csv->addRow(
        array_merge(
            array(
                "ORGANISME",
                "ANTENNE",
                "ACTIVITE",
                "OPERATIONS DE L'ORGANISME",
                "SIRET",
                "FAX",
                'DATE DE CREATION FICHE OU DERNIERE MODIFICATION',
                "CIVILITE",
                "NOM",
                "PRENOM",
                "FONCTION",
                "TELEPHONE",
                "LIGNE DIRECTE",
                "LIGNE MOBILE",
                "MAIL",
                "ADRESSE",
                "COMPL ADRESSE",
                "COMPL ADRESSE 2",
                "CODE POSTAL",
                "VILLE",
				"CEDEX",
                "OPERATIONS DU CONTACT",
                "OBSERVATIONS"
            ),
            $eventColumns
        )
	);

    asort($civilite);

    if( isset($results[0]) && !empty($results[0]) ) {
       foreach( $results as $o => $organisme ) {
//debug($contact);
//die();
            $nomOrg = $organisme['Organisme']['name'];
            $antenneOrg = $organisme['Organisme']['antenne'];
            $rowOganisme[$o] = array_merge(
                array($nomOrg),
                array($antenneOrg),
                array($organisme['Organisme']['activites']),
                array($organisme['Organisme']['operationsorg']),
                array($organisme['Organisme']['siret']),
                array($organisme['Organisme']['fax'])
            );

            foreach( $organisme['Contact'] as $c => $contact) {
                $neweventColumns = array();
                if( !empty($contact['events']) ) {
                    foreach( $contact['events'] as $eventsIds => $eventsName ) {
                        if( in_array( $eventsName, $eventColumns ) ) {
                            $neweventColumns[$eventsIds] = 'Oui';
                        }
                        else {
                            $neweventColumns[$eventsIds] = 'Non';
                        }
                    }
                }

                $modified = $this->Html2->ukToFrenchDateWithHourAndSlashes($contact['modified']);

                $rowContact[$o] = array_merge(
                    array($modified),
                    array(@$civilite[$contact['civilite']]),
                    array($contact['nom']),
                    array($contact['prenom']),
                    array(@$contact['Fonction']['name']),
                    array($contact['tel']),
                    array($contact['lignedirecte']),
                    array($contact['portable']),
                    array($contact['email']),
                    array($contact['adresse']),
                    array($contact['compl']),
                    array($contact['compl2']),
                    array($contact['cp']),
                    array($contact['ville']),
					array($contact['cedex']),
                    array(@$contact['operationscontact']),
                    array($contact['observations']),
                    $neweventColumns
                );
                $this->Csv->addRow(
                    array_merge($rowOganisme[$o], $rowContact[$o])
                );
            }

        }
    }
    else {
        $organisme = $results;
        $nomOrg = $organisme['Organisme']['name'];
        $antenneOrg = $organisme['Organisme']['antenne'];
        $rowOganisme = array_merge(
            array($nomOrg),
            array($antenneOrg),
            array($organisme['Organisme']['activites']),
            array($organisme['Organisme']['operationsorg']),
            array($organisme['Organisme']['siret']),
            array($organisme['Organisme']['fax'])
        );

        foreach( $organisme['Contact'] as $c => $contact) {
            $neweventColumns = array();
            if( !empty($contact['events']) ) {
                foreach( $contact['events'] as $eventsIds => $eventsName ) {
                    if( in_array( $eventsName, $eventColumns ) ) {
                        $neweventColumns[$eventsIds] = 'Oui';
                    }
                    else {
                        $neweventColumns[$eventsIds] = 'Non';
                    }
                }
            }

            $modified = $this->Html2->ukToFrenchDateWithHourAndSlashes($contact['modified']);

            $rowContact[$c] = array_merge(
                array($modified),
                array(@$civilite[$contact['civilite']]),
                array($contact['nom']),
                array($contact['prenom']),
                array(@$contact['Fonction']['name']),
                array($contact['tel']),
                array($contact['lignedirecte']),
                array($contact['portable']),
                array($contact['email']),
                array($contact['adresse']),
                array($contact['compl']),
                array($contact['compl2']),
                array($contact['cp']),
                array($contact['ville']),
                array($contact['cedex']),
                array(@$contact['operationscontact']),
                array($contact['observations']),
                $neweventColumns
            );
            $this->Csv->addRow(
                array_merge($rowOganisme, $rowContact[$c])
            );
        }
    }


    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>
