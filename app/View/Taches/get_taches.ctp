<?php

/**
 *
 * Taches/get_taches.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
$nbTachesATraiter = count($taches['aTraiter']);
$nbTachesEnCoursPerso = count($taches['enCoursPerso']);
$nbTaches = $nbTachesATraiter + $nbTachesEnCoursPerso;
?>
<a class="notification_fullwrapper">
	<?php
	if (!empty($taches) && ($nbTachesATraiter > 0 || $nbTachesEnCoursPerso > 0 )) {
		$span_tache = $this->Html->tag('div', $nbTaches, array('class' => 'badge'));
		$span_tache .=$this->Html->tag('div','<i class="fa fa-tasks" style="color:#fff;" aria-hidden="true"></i>', array('id' => 'viewTaches', 'title' => __d('tache', 'Tache.new'),'escape' => false));
		echo $this->Html->tag('span', $span_tache, array('class' => 'notification_wrapper'));
		?>



    <script type="text/javascript">
        function updateTaskNotification() {
            $('#nbTaches').html("<img src='/img/ajax-loader-mini.gif' alt='' />");
            gui.request({
                url: '/taches/getTaches/',
                updateElement: $('#nbTaches')
            });
        }

        function updateFluxTab() {
            if ($('#tabs').length > 0 && $('#taskTab').length > 0) {
                if ($('#tabs').tabs('options', 'selected') == $('#taskTab').attr('tabRank')) {
                    $('#tabs').tabs('load', $('#taskTab').attr('tabRank'));
                }
            }
        }


        function notifValidTache(id) {
            gui.request({
                url: "/taches/checkTache/" + id
            }, function (data) {
                getJsonResponse(data);
                updateTaskNotification();
                updateFluxTab();
            });
        }

        function notifGrabTache(id) {
            gui.request({
                url: "/taches/grabTache/" + id
            }, function (data) {
                getJsonResponse(data);
                updateTaskNotification();
                updateFluxTab();
            });
        }

        function notifReleaseTache(id) {
            gui.request({
                url: "/taches/releaseTache/" + id
            }, function (data) {
                getJsonResponse(data);
                updateTaskNotification();
                updateFluxTab();
            });
        }
    </script>

		<?php
		$divTacheContent = '<div class="arrow"></div>  <div class="content_notification">';
		$tacheDivDelaiContent = '';
		$retardImg = $this->Html->image('/img/retard_16.png', array('alt' => __d('Tache', 'tache.retard'), 'title' => __d('Tache', 'tache.retard')));
		$tachesListIcon = array(
			"aTraiter" => $this->Html->image('/img/priority_unknown.png', array('alt' => __d('Tache', 'tache.attente'), 'title' => __d('Tache', 'tache.attente'))),
			"enCoursPerso" => $this->Html->image('/img/priority_mid.png', array('alt' => __d('Tache', 'tache.encours'), 'title' => __d('Tache', 'tache.encours')))
		);

		if (!empty($taches) && ($nbTachesATraiter > 0 || $nbTachesEnCoursPerso > 0 )) {
			foreach ($taches as $keyTList => $tacheslist) {
				if(!empty($tacheslist)){
						$divTacheContent .= $this->Html->tag('h3', $tachesListIcon[$keyTList] . ' ' . __d('tache', 'Taches.' . $keyTList));
				}
				foreach ($tacheslist as $tache) {
					$canValidImg = $this->Html->tag('i', '', array('style' => 'margin-left:10px;color:#5397a7;cursor:pointer;', 'class' => 'tacheCheck fa fa-check-square', 'alt' => __d('Tache', 'tache.validate'), 'title' => __d('Tache', 'tache.validate'), 'itemId' => $tache['Tache']['id']));
					$canGrabImg = $this->Html->tag('i', '', array('style' => 'margin-left:10px;color:#5397a7;cursor:pointer;', 'class' => 'tacheGrab fa fa-thumbs-up', 'alt' => __d('Tache', 'tache.grab'), 'title' => __d('Tache', 'tache.grab'), 'itemId' => $tache['Tache']['id']));
					$canReleaseImg = $this->Html->tag('i', '', array('style' => 'margin-left:10px;color:#5397a7;cursor:pointer;', 'class' => 'tacheRelease fa fa-thumbs-down', 'alt' => __d('Tache', 'tache.release'), 'title' => __d('Tache', 'tache.release'), 'itemId' => $tache['Tache']['id']));
					$viewImg = $this->Html->tag('i', '', array('style' => 'margin-left:10px;color:#5397a7;cursor:pointer;','alt' => 'Consulter le flux concerné', 'title' => 'Consulter le flux concerné', 'class' => 'tacheView fa fa-eye', 'itemId' => $tache['Tache']['id'], 'fluxId' => $tache['Tache']['courrier_id'], 'desktopId' => $tache['Desktop']['id']));

					$tacheDivContent = "<h4 class='panel-title'>" .$tache['Tache']['name'] . $this->Html->tag('span', $tache['retard'] ? ' ' . $retardImg : '', array('class' => 'etats'))."</h4>";


					if( !empty($tache['Tache']['delai_nb'])) {
						$tacheDivDelaiContent = 'Délai pour traiter la tâche: '.$tache['Tache']['delai_nb'] . ' ' . $delaiTranslate[$tache['Tache']['delai_unite']];
					}
					$tacheDivMainContent = $tache['User']['nom'] . ' ' .$tache['User']['prenom'];
					$tacheDivActionContent = $viewImg.' ';
					if ($keyTList == "aTraiter") {
						$tacheDivActionContent .= $tache['multi'] ? $canGrabImg : $canValidImg;
					} else if ($keyTList == "enCoursPerso") {
						$tacheDivActionContent .= $canReleaseImg . $canValidImg;
					}
					$tacheDivFooter = $this->Html->tag('div',$tacheDivActionContent,array('class' => 'tache_actions'));
					$tacheDivContent .= $this->Html->tag('p',$tacheDivDelaiContent,array('class' => 'message'));
					$tacheDivContent .= $this->Html->tag('p', $tacheDivMainContent, array('class' => 'message'));
					$tacheDivContent .= $this->Html->tag('div',$tacheDivFooter,array('class' => ' notif_actions'));
					$divTacheContent .= $this->Html->tag('div', $tacheDivContent, array('class' => 'notification_div', 'tache_id' => $tache['Tache']['id']));
				}
			}
                                        $divTacheContent .= '</div>';
		} else {
			$divTacheContent .= $this->Html->tag('br');
			$divTacheContent .= $this->Html->tag('div', __d('tache', 'Tache.void'), array('class' => 'alert alert-warning'));
			$divTacheContent .= $this->Html->tag('br');
		}

		echo $this->Html->tag('div', $divTacheContent, array('id' => 'divTache', 'class' => 'ui-widget ui-widget-content ui-corner-all notification_fullwrapper_div'));
		?>
</a>
<script type="text/javascript">
//		$('#divTache .tache').hover(function() {
//			$(this).addClass('alert alert-warning');
//		},
//                                function() {
//                                        $(this).removeClass('alert alert-warning');
//                                });

    $(".tacheGrab").button().click(function () {
        notifGrabTache($(this).attr('itemId'));
    });

    $(".tacheRelease").button().click(function () {
        notifReleaseTache($(this).attr('itemId'));
    });

    $(".tacheCheck").button().click(function () {
        notifValidTache($(this).attr('itemId'));
    });

    $(".tacheView").button().click(function () {

        var fluxId = $(this).attr('fluxId');
        var desktopId = $(this).attr('desktopId');

        gui.request({
            url: '/taches/setFromTask'
        }, function () {
            window.location.href = "/courriers/view/" + fluxId + "/" + desktopId + '/tache';
        });
    });
<?php
if (!CakeSession::read('Auth.User.notifyTache')) {
        ?>
    layer.msg("<?php echo __d('tache', 'Tache.new'); ?>", {});<?php
        CakeSession::write('Auth.User.notifyTache', true);
}
?>
</script>
<?php
} else {
        echo '<i class="fa fa-tasks" aria-hidden="true"></i>';
}
?>
