<?php
/**
 *
 * Repertoires/link_group.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Form->create('Repertoire');
echo $this->Form->input('Repertoire.user_id', array('type' => 'hidden', 'value' => $user['User']['id'] ));
?>

<script type="text/javascript">
    function fillSelect(){
        if ($('#RepertoireToLink option:selected').val() == "") {
            $('#newRepertoire').css('display', 'block');
            $('#newRepertoireName').removeAttr('disabled');
            $('#newRepertoireParent').removeAttr('disabled');
        } else {
            $('#newRepertoire').css('display', 'none');
            $('#newRepertoireName').attr('disabled', 'disabled');
            $('#newRepertoireParent').attr('disabled', 'disabled');
        }
    }
</script>
<div class=" form-horizontal">
    <div class="form-group">
        <label for="data[RepertoireToLink]" class="control-label col-sm-3 "><?php echo __d('repertoire', 'Nom') ?></label>
        <div class="controls col-sm-5">
        <select id="RepertoireToLink" name="data[RepertoireToLink]" class="form-control">
                <?php foreach ($repertoires as $key => $val) { ?>
                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                <?php } ?>
            <option value=""><?php echo __d('repertoire', 'New Repertoire'); ?> ... </option>
        </select>
        </div>
    </div>
    <?php foreach ($itemList['itemToLink'] as $item) { ?>
            <input type="hidden" name="data[itemToLink][]" value="<?php echo $item; ?>" />
    <?php } ?>
<!--<fieldset  id="newRepertoire">-->
    <legend><?php echo __d('repertoire', 'New Repertoire'); ?></legend>
        <div class="form-group">
            <label for="data[Repertoire][name]" class="control-label col-sm-3 " ><?php echo __d('repertoire', 'Repertoire.name'); ?></label>
            <div class="controls col-sm-5">
                <input type="text" name="data[Repertoire][name]" id="newRepertoireName" class="form-control"/>
            </div>
        </div>
        <div class="form-group">
            <label for="data[Repertoire][parent_id]" class="control-label col-sm-3 " ><?php echo __d('repertoire', 'Repertoire.parent_id'); ?></label>
            <div class="controls col-sm-5">
                <select id="newRepertoireParent" name="data[Repertoire][parent_id]" class="form-control">
                    <option value=""> --- </option>
                            <?php foreach ($repertoires as $key => $val) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                            <?php } ?>
                </select>
            </div>
        </div>
<!--</fieldset>-->
</div>
<?php
echo $this->Form->end();
?>

<script type="text/javascript">
    $('#newRepertoire').css('display', 'none');

    $('#RepertoireToLink').change(function(){
        fillSelect();
    });

    fillSelect();
</script>
