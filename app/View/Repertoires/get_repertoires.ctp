<?php

/**
 *
 * Repertoires/get_repertoires.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php

if (!empty($repertoires)) {
    $options = array(
        'nodes' => array(
            array(
                'model' => 'Repertoire',
                'field' => 'name',
                'actions' => array(
                    "view" => array(
                        "ico" => 'fa fa-eye',
                        "url" => '/repertoires/getFlux',
//                        "icoColor" => "#5397a7"
                    ),
                    "edit" => array(
                        "ico" => 'fa fa-pencil',
                        "url" => '/repertoires/edit',
//                        "icoColor" => "#5397a7"
                    ),
                    "delete" => array(
                        "ico" => 'fa fa-trash',
                        "url" => '/repertoires/delete',
//                        "icoColor" => "#FF0000"
                    )
                )
            )
        )
        ,
        'listId' => 'browserRepertoire',
        'listClass' => 'filetree'
    );

    $val = $this->Liste->jqueryThreadedView($repertoires, $options);
    //echo json_encode($repertoires);
    echo $val;
} else {
        echo $this->Html->div('alert alert-warning',__d('repertoire', 'Repertoire.void'));
}
?>

<script type="text/javascript">
    $("#browserRepertoire").treeview({collapsed: true});

    $('.treeview span.folder, .treeview span.file').addClass('ui-corner-all').hover(function () {
        $(this).addClass('alert alert-warning').css('border', 'none');
    }, function () {
        $(this).removeClass('alert alert-warning').css('border', 'none');
    });

    $('.treeviewBttn i').each(function () {
        if ($(this).hasClass('actionImg')) {
            $(this).button();
        }
        var parentLine = $(this).parent().parent().parent();
        var parentSpan = $(this).parent().parent();
        var parentLink = $(this).parent();
        var href = $(this).parent().attr('href');
        var img = $(this).detach();
        img.appendTo(parentSpan).click(function () {
            if (img.hasClass('editBttn')) {
                gui.formMessage({
                    url: href,
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                    updateElement: $('#repertoires'),
                    buttons: {
                        "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                            $(this).parents(".modal").modal('hide');
                        },
                        "<?php echo __d('default', 'Button.submit'); ?>": function () {
                            var form = $('#RepertoireEditForm');
                            if (form_validate(form)) {
                                gui.request({
                                    url: form.attr('action'),
                                    data: form.serialize()
                                }, function (data) {
                                    getJsonResponse(data);
                                    loadRepertoires();
                                });
                                $(this).parents(".modal").modal('hide');
                            } else {
                                swal({
                    showCloseButton: true,
                                    title: "Oops...",
                                    text: "Veuillez vérifier votre formulaire!",
                                    type: "error",

                                });
                            }
                        }
                    }
                });
            } else if (img.hasClass('deleteBttn')) {
                swal({
                    showCloseButton: true,
                    title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
                    text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
                    cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
                }).then(function (data) {
//                    $(this).parents(".modal").modal('hide');
                    if (data) {
                        gui.request({
                            url: href,
                            loaderElement: $('#repertoires_skel_liste .content'),
                            loaderMessage: gui.loaderMessage
                        }, function (data) {
                            getJsonResponse(data);
                            if (parentLine.hasClass('ui-state-focus')) {
                                $('#repertoires_skel_infos .content').empty();
                            }
                            loadRepertoires();
                        });
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Annulé!",
                            text: "Vous n'avez pas supprimé, ;) .",
                            type: "error",

                        });
                    }
                });
                $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
            } else {
                selectLineElement(parentLine);
                gui.request({
                    url: href,
                    loader: true,
                    updateElement: $('#repertoires_skel_infos .content'),
                    loaderMessage: gui.loaderMessage
                });
            }
        });
        parentLink.remove();
    });
</script>
