<?php

/**
 *
 * Repertoires/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script type="text/javascript">
    function loadRepertoires() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/repertoires/getRepertoires"; ?>",
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#repertoires_skel_liste .content')
        });
    }

    function loadRecherches() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/recherches/getRecherches"; ?>",
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#repertoires_skel_dyn .content')
        });
    }

    function loadRepertoire(id) {
        $('#liste .content .treeTr').each(function () {
            if ($(this).attr('itemId') == id) {
                $(this).click();
            }
        });
    }

    function selectLineElement(lineElement) {
        $('#browserRepertoire li > span, #browserRecherche li > span').removeClass('ui-state-focus');
        lineElement.addClass('ui-state-focus');
    }
</script>

<div class="container">
    <div class="row">
        <div class="espace_travail">
            <ul class="nav nav-tabs titre" role="tablist">

                <!--                <li class="active">
                                    <a href="#repertoires" role="tab" data-toggle="tab">
                        <?php // echo __d('repertoire', 'Repertoire.liste'); ?>
                                    </a>
                                </li>-->

                <li class>
                    <a href="#rechercheEnregistre" role="tab" data-toggle="tab">
                         <?php echo __d('recherche', 'Recherche.liste'); ?>
                    </a>
                </li>
            </ul>

            <div class="tab-content container">
                <!--                <div class="tab-pane fade active in" id="repertoires">
                                    <div class="panel col-sm-4" id="repertoires_skel_liste">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                                                <h4><?php // echo __d('repertoire', 'Repertoire.liste'); ?></h4>
                                            </div>
                                            <div class="panel-body content"></div>
                                            <div class="panel-footer controls"></div>
                                        </div>
                                    </div>
                                    <div class="panel col-sm-8" id="repertoires_skel_infos">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                                                <h4><?php // echo __d('repertoire', 'Repertoire.listeFlux'); ?></h4>
                                            </div>
                                            <div class="panel-body content">
                                                <div class="alert alert-warning">Aucun répertoire est choisi</div>
                                            </div>
                                            <div class="panel-footer controls"></div>
                                        </div>
                                    </div>
                                </div>-->
                <div class="tab-pane fade active in" id="rechercheEnregistre">
                    <div class="panel col-sm-4" id="repertoires_skel_dyn">
                        <div class="panel panel-default">
                            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                                <h4><?php echo __d('recherche', 'Recherche.liste'); ?></h4>
                            </div>
                            <div class="panel-body content"></div>
                            <div class="panel-footer controls"></div>
                        </div>
                    </div>
                    <div class="panel col-sm-8" id="recherche_skel_infos">
                        <div class="panel panel-default">
                            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                                <h4><?php echo __d('recherche', 'Recherche.listeFlux'); ?></h4>
                            </div>
                            <div class="panel-body content">
                                <div class="alert alert-warning">Aucun répertoire est choisi</div>
                            </div>
                            <div class="panel-footer controls " role="group"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    gui.buttonbox({
        element: $('#repertoires_skel_liste .controls')
    });

    gui.buttonbox({
        element: $('#repertoires_skel_infos .controls')
    });

    gui.buttonbox({
        element: $('#repertoires_skel_dyn .controls')
    });
    gui.buttonbox({
        element: $('#recherche_skel_infos .controls')
    });

    gui.addbutton({
        element: $('#repertoires_skel_liste .controls'),
        button: {
//			content: '<?php // echo $this->Html->image("icons/repertoire-add.png") . " " . __d('repertoire', 'New Repertoire'); ?>',
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> <i class="fa fa-folder-open" aria-hidden="true"></i>',
            title: '<?php echo __d('repertoire', 'New Repertoire'); ?>',
            action: function () {
                gui.formMessage({
                    url: "<?php echo Configure::read('BaseUrl') . '/repertoires/add' ?>",
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                    updateElement: $('#repertoires'),
                    buttons: {
                        "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();

                        },
                        "<?php echo __d('default', 'Button.submit'); ?>": function () {
                            var form = $('#RepertoireAddForm');
                            if (form_validate(form)) {
                                gui.request({
                                    url: form.attr('action'),
                                    data: form.serialize()
                                }, function (data) {
                                    getJsonResponse(data);
                                    loadRepertoires();
                                });
                                $(this).parents(".modal").modal('hide');
                                $(this).parents(".modal").empty();
                            } else {
                                swal({
                    showCloseButton: true,
                                    title: "Oops...",
                                    text: "Veuillez vérifier votre formulaire!",
                                    type: "error",

                                });
                            }
                        }
                    }
                }, function () {
                    loadRepertoires();
                });
            }
        }
    });

    gui.disablebutton({
        element: $('#repertoires_skel_infos .controls'),
        button: 1
    });

    gui.addbutton({
        element: $('#repertoires_skel_dyn .controls'),
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> <i class="fa fa-search" aria-hidden="true"></i>',
            title: '<?php echo __d('repertoire', 'New Recherche'); ?>',
            action: function () {
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . '/recherches/formulaire/0' ?>",
                    loader: true,
                    updateElement: $('#recherche_skel_infos .content'),
                    loaderMessage: gui.loaderMessage
                });
                gui.removebutton({
                    element: $('#recherche_skel_infos .panel-footer'),
                    button: "<?php echo __d('default', 'Button.submit'); ?>"
                });
                gui.removebutton({
                    element: $('#recherche_skel_infos .panel-footer'),
                    button: "<?php echo __d('default', 'Button.cancel'); ?>"
                });
                gui.addbuttons({
                    element: $('#recherche_skel_infos .panel-footer'),
                    buttons: [
                        {
                            content: "<?php echo __d('default', 'Button.cancel'); ?>",
                            class: "btn-danger-webgfc btn-inverse ",
                            action: function () {
                                $('#recherche_skel_infos .content').empty();
                                $('#recherche_skel_infos .panel-footer').empty();
                            }
                        },
                        {
                            content: "<?php echo __d('default', 'Button.submit'); ?>",
                            class: "btn-success ",
                            action: function () {
                                var form = $('#RechercheFormulaireForm');
                                if (form_validate(form)) {
                                    gui.request({
                                        url: form.attr('action'),
                                        data: form.serialize()
                                    }, function (data) {
                                        getJsonResponse(data);
                                        loadRecherches();
                                        $('#recherche_skel_infos .content').empty();
                                    });
                                } else {
                                    swal({
                    showCloseButton: true,
                                        title: "Oops...",
                                        text: "Veuillez vérifier votre formulaire!",
                                        type: "error",

                                    });
                                }
                            }
                        }
                    ]
                });
            }
        }
    });

    loadRepertoires();
        <?php if(isset($isRecherche)){ ?>
    loadRecherches();
        <?php }else{ ?>
    $('#repertoires_skel_dyn').empty();
    $('#repertoires_skel_liste .content').css("height", "92%");
        <?php } ?>
</script>
