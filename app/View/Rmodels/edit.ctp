<?php

/**
 *
 * Modeldocs/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    $formRmodel = array(
        'name' => 'Rmodel',
		'label_w' => 'col-sm-5',
		'input_w' => 'col-sm-5',
		'form_url' => array( 'controller' => 'rmodels', 'action' => 'edit', $rmodel['Rmodel']['id'] ),
		'form_target' =>'modelUploadFrame',
		'form_type' =>'file',
        'input' => array(
            'Rmodel.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value'=> $rmodel['Rmodel']['id']
                )
            ),
            'Rmodel.name' => array(
                'labelText' =>__d('rmodel', 'Rmodel.name'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'value'=>$rmodel['Rmodel']['name']
                )
            ),
            'Rmodel.file' => array(
                'labelText' =>__d('rmodel', 'Rmodel.file'),
                'inputType' => 'file',
                'items'=>array(
                    'type' => 'file',
                    'name' => 'myfile',
                    'class' => 'fileField'
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formRmodel);
    echo $this->Form->end();
?>
<script type="text/javascript">
    $("#modelUploadFrame").remove();
    $('body').append('<iframe name="modelUploadFrame" id="modelUploadFrame" src="#" style="width:0;height:0;border:none;"></iframe>');
<?php if (isset($uploadComplete) && $uploadComplete == "ok") { ?>
    if (window.top.window.updateModelTab) {
        window.top.window.updateModelTab();
    }
<?php } ?>
</script>
