<?php

/**
 *
 * Titres/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formTitre = array(
    'name' => 'Titre',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Titre.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Titre.name' => array(
            'labelText' =>__d('titre', 'Titre.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        )
    )
);
echo $this->Formulaire->createForm($formTitre);
echo $this->Form->end();
?>

<script type="text/javascript">

    $('#TitreEditForm').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });


</script>

