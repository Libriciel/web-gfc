<?php
/**
 *
 * Metadonnees/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="metadonnees view">
    <h2><?php __('Metadonnee'); ?></h2>
    <ul class="actions">
            <li><?php echo $this->Html->link(__('Edit Metadonnee', true), array('action' => 'edit', $metadonnee['Metadonnee']['id'])); ?> </li>
            <li><?php echo $this->Html->link(__('Delete Metadonnee', true), array('action' => 'delete', $metadonnee['Metadonnee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $metadonnee['Metadonnee']['id'])); ?> </li>
            <li><?php echo $this->Html->link(__('New Metadonnee', true), array('action' => 'add')); ?> </li>
    </ul>

    <dl>
        <?php
            $i = 0;
            $class = ' class="altrow"';
        ?>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('metadonnee', 'Metadonnee.id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $metadonnee['Metadonnee']['id']; ?>
                &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('metadonnee', 'Metadonnee.name'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $metadonnee['Metadonnee']['name']; ?>
                &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('metadonnee', 'Metadonnee.typemetadonnee_id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $this->Html->link($metadonnee['Typemetadonnee']['name'], array('controller' => 'typemetadonnees', 'action' => 'view', $metadonnee['Typemetadonnee']['id'])); ?>
                &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('metadonnee', 'Metadonnee.created'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $metadonnee['Metadonnee']['created']; ?>
                &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('metadonnee', 'Metadonnee.modified'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $metadonnee['Metadonnee']['modified']; ?>
                &nbsp;
        </dd>
    </dl>
    <div class="related">
        <h3><?php __('Related Courriers'); ?></h3>
        <?php if (!empty($metadonnee['Courrier'])): ?>
            <table cellpadding = "0" cellspacing = "0">
                <tr>
                    <th><?php __('Id'); ?></th>
                    <th><?php __('Name'); ?></th>
                    <th><?php __('Etat'); ?></th>
                    <th><?php __('Created'); ?></th>
                    <th><?php __('Modified'); ?></th>
                    <th><?php __('Typeflux'); ?></th>
                    <th><?php __('Emetteur'); ?></th>
                    <th><?php __('Recepteur'); ?></th>
                    <th><?php __('Lienmodele'); ?></th>
                    <th><?php __('Date'); ?></th>
                    <th><?php __('Datereception'); ?></th>
                    <th><?php __('Objet'); ?></th>
                    <th><?php __('Circuit Id'); ?></th>
                    <th><?php __('User Creator Id'); ?></th>
                    <th class="actions"><?php echo __d('default', 'Actions'); ?></th>
                </tr>
                    <?php
                    $i = 0;
                    foreach ($metadonnee['Courrier'] as $courrier):
                        $class = null;
                        if ($i++ % 2 == 0) {
                                $class = ' class="altrow"';
                        }
                    ?>
                <tr<?php echo $class; ?>>
                    <td><?php echo $courrier['id']; ?></td>
                    <td><?php echo $courrier['name']; ?></td>
                    <td><?php echo $courrier['etat']; ?></td>
                    <td><?php echo $courrier['created']; ?></td>
                    <td><?php echo $courrier['modified']; ?></td>
                    <td><?php echo $courrier['typeflux']; ?></td>
                    <td><?php echo $courrier['emetteur']; ?></td>
                    <td><?php echo $courrier['recepteur']; ?></td>
                    <td><?php echo $courrier['fichier_associe']; ?></td>
                    <td><?php echo $courrier['date']; ?></td>
                    <td><?php echo $courrier['datereception']; ?></td>
                    <td><?php echo $courrier['objet']; ?></td>
                    <td><?php echo $courrier['circuit_id']; ?></td>
                    <td><?php echo $courrier['user_creator_id']; ?></td>
                    <td class="actions">
                            <?php echo $this->Html->link(__('View', true), array('controller' => 'courriers', 'action' => 'view', $courrier['id'])); ?>
                            <?php echo $this->Html->link(__('Edit', true), array('controller' => 'courriers', 'action' => 'edit', $courrier['id'])); ?>
                            <?php echo $this->Html->link(__('Delete', true), array('controller' => 'courriers', 'action' => 'delete', $courrier['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $courrier['id'])); ?>
                    </td>
                </tr>
                    <?php endforeach; ?>
            </table>
            <?php endif; ?>
        <div class="actions">
            <ul>
                <li><?php echo $this->Html->link(__('New Courrier', true), array('controller' => 'courriers', 'action' => 'add')); ?> </li>
            </ul>
        </div>
    </div>
</div>
