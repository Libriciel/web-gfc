<?php

/**
 *
 * Metadonnees/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formMetadonnee = array(
    'name' => 'Metadonnee',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-7',
    'input' => array(
        'Metadonnee.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Metadonnee.name' => array(
            'labelText' =>__d('metadonnee', 'Metadonnee.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Metadonnee.typemetadonnee_id' => array(
            'labelText' =>__d('metadonnee', 'Metadonnee.typemetadonnee_id'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $typemetadonnees,
                'required'=>true,
                'empty' => true
            )
        ),
        'Soustype.Soustype' => array(
            'labelText' =>__d('metadonnee', 'Metadonnee.soustype_id'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'multiple' => true,
                'options' => $listeSoustypes
            )
        ),
        'Metadonnee.champfusion' => array(
            'labelText' =>__d('metadonnee', 'Metadonnee.champfusion'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text',
            )
        ),
        'Metadonnee.active' => array(
            'labelText' =>__d('metadonnee', 'Metadonnee.active'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['Metadonnee']['active']
            )
        ),
		'Metadonnee.isrepercutable' => array(
			'labelText' => 'Métadonnée répercutable dans le flux réponse',
			'inputType' => 'checkbox',
			'items'=>array(
				'type'=>'checkbox',
				'checked'=>$this->request->data['Metadonnee']['isrepercutable']
			)
		)
    )
);
echo $this->Formulaire->createForm($formMetadonnee);
echo $this->Form->end();
?>
<div id="metadonneesvalues" class="col-sm-12">
    <legend class="bannette">Liste des valeurs de la métadonnée</legend>
    <?php echo $this->Html->tag('span', $this->Html->tag('i',' Ajouter une valeur', array('title' => __d('selectvaluemetadonnee', 'Selectvaluemetadonnee.add'), 'alt' => __d('default', 'Button.add'),'class'=>'fa fa-plus-circle', 'aria-hidden'=>true)), array('title'=>__d('selectvaluemetadonnee', 'Selectvaluemetadonnee.add'),'id' => 'addValue','class'=>'btn btn-info-webgfc','style'=>'float: right;margin-top: -50px;margin-right: 25px;')); ?>
    <?php
        $fields = array(
            'name'
        );

        $actions = array(
            "edit" => array(
                "url" => '/selectvaluesmetadonnees/edit/',
                "updateElement" => "$('#infos .content')",
                "formMessage" => true,
                "loader" => true,
                "refreshAction" => "loadValues()"
            ),
            "delete" => array(
                "url" => '/selectvaluesmetadonnees/delete/',
                "updateElement" => "$('#infos .content')",
                "refreshAction" => "loadValues()"
            )
        );
        $options = array();
        ?>
    <div  class="bannette_panel panel-body">
        <table id="table_metadonnees_edit"
               data-toggle="table"
               data-locale = "fr-CA"
               data-pagination="true"
               data-page-size =" 5"
               >
        </table>
    </div>
</div>
<?php
    $data = array();
    $i=0;
    if(isset($selectvalues) &&!empty($selectvalues)){
        foreach($selectvalues as $value){
            $data[$i]['name'] = $value['Selectvaluemetadonnee']['name'];
            $data[$i]['active'] = $value['Selectvaluemetadonnee']['active'];
            $data[$i]['edit'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.edit'),
                    'alt' => __d('default', 'Button.edit'),
                    'class' => "fa fa-pencil itemEdit",
                    'itemId' => $value['Selectvaluemetadonnee']['id'],
                    'onclick' => "return editElementSelectvaluemetadonnee(" . $value['Selectvaluemetadonnee']['id'] . ");",
//                    'style' => 'color: #5397a7;cursor:pointer'
                        )
                );
            $data[$i]['delete'] = $this->Html->tag('i', '', array(
                        'title' => __d('default', 'Button.delete'),
                        'alt' => __d('default', 'Button.delete'),
                        'class' => "fa fa-trash itemDelete",
                        'onclick' => "return deleteElementSelectvaluemetadonnee(" . $value['Selectvaluemetadonnee']['id'] . ")",
//                        'style' => 'color: #FF0000;cursor:pointer'
                            )
                    );
            $i++;
        }
    }
    ?>
<script type="text/javascript">
    function loadValues() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/metadonnees/edit/" . $metadonneeId; ?>",
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

    //title legend (nombre de données)
    if ($('#metadonneesvalues .bannette span').length < 1) {
        $('#metadonneesvalues .bannette ').append('<span> - total : <?php echo count($selectvalues);?></span>');
    }

    $('#table_metadonnees_edit')
            .bootstrapTable({
                data:<?php echo json_encode($data);?>,
                columns: [
                    {
                        field: 'name',
                        title: '<?php echo __d('selectvaluemetadonnee','Selectvaluemetadonnee.name'); ?>',
                        class: 'name'
                    },
                    {
                        field: "active",
                        title: "<?php echo __d('selectvaluemetadonnee', 'Selectvaluemetadonnee.active'); ?>",
                        class: "active_column"
                    },
                    {
                        field: 'edit',
                        title: 'Modifier',
                        class: 'actions thEdit',
                        align: 'center',
                        width: "80px"
                    },
                    {
                        field: 'delete',
                        title: 'Supprimer',
                        class: 'actions thDelete',
                        align: 'center',
                        width: "80px"
                    }
                ]
            });


</script>
<?php
echo $this->Js->writeBuffer();
?>

<script type="text/javascript">
    //Bouton Ajouter pour ajouter des valeurs à la métadonnée si de type select
    $('#editValue').css('display', 'none');
    $('#metadonneesvalues').css('display', 'none');

    <?php if(!empty(Configure::read('Selectvaluemetadonnee.id'))){ ?>
    var valOfSelect = <?php echo Configure::read('Selectvaluemetadonnee.id');?>;
    $(document).ready(function () {
        if ($('#MetadonneeTypemetadonneeId').val() == valOfSelect) {
            $('#editValue').css('display', 'block');
            $('#editValue').addClass('selectmetadonnees');
            $('#metadonneesvalues').css('display', 'block');
        } else {
            $('#editValue').css('display', 'none');
            $('#metadonneesvalues').css('display', 'none');
        }

        $('#MetadonneeTypemetadonneeId').change(function () {
            if ($('#MetadonneeTypemetadonneeId').val() == valOfSelect) {
                $('#editValue').css('display', 'block');
                $('#editValue').addClass('selectmetadonnees');
                $('#metadonneesvalues').css('display', 'block');
            } else {
                $('#editValue').css('display', 'none');
                $('#metadonneesvalues').css('display', 'none');
            }
        });
    });
    <?php } ?>



    function editElementSelectvaluemetadonnee(itemId) {
        gui.request({
            url: "/selectvaluesmetadonnees/edit/" + itemId,
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }



    $('#addValue').button().click(function () {
        var url = "<?php echo Router::url(array('controller' => 'selectvaluesmetadonnees', 'action' => 'add', $metadonneeId)); ?>";
        gui.request({
            url: url,
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    });
    $(document).ready(function () {
        $('#SoustypeSoustype').select2({allowClear: true, placeholder: "Sélectionner un soustype"});
        $('#MetadonneeTypemetadonneeId').select2();
        addClassActive();

        $('.itemEdit').css('cursor', 'pointer');
        $('.itemDelete').css('cursor', 'pointer');
    });

    function deleteElementSelectvaluemetadonnee(itemId) {
        swal({
                    showCloseButton: true,
            title: '<?php echo  __d("default", "Modal.suppressionTitle"); ?>',
            text: '<?php echo  __d("default", "Modal.suppressionContents"); ?>',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: "<?php echo  __d("default", "Button.delete"); ?>",
            cancelButtonText: "<?php echo  __d("default", "Button.cancel"); ?>",
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: '<?php echo Configure::read('BaseUrl') . '/selectvaluesmetadonnees/delete'; ?>' + '/' + itemId,
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    getJsonResponse(data);
                    loadValues();
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n\'avez pas supprimé.",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    }

     function addClassActive() {
        $('#table_metadonnees_edit .active_column').hide();
        $('#table_metadonnees_edit .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }
</script>
