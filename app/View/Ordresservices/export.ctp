<?php
	$this->Csv->preserveLeadingZerosInExcel = true;
    $this->Csv->addRow( array( 'Informations sur les Ordres de Service concernant le marché n° '.$marcheNumero ));
    
    $this->Csv->addRow(
        array(
            'Organisme concerné',
            'N° de l\'OS',
            'Objet de l\'OS',
            'Montant de l\'OS',
            'Date de l\'OS'
        )
	);
    
	foreach( $results as $m => $os ){
        $numeroOS = $os['Ordreservice']['numero'];
        $objetOS = $os['Ordreservice']['objet'];
        $montantOS = $os['Ordreservice']['montant'];
        $nomOrganisme = $os['Organisme']['name'];
        $dateOS = $this->Html2->ukToFrenchDateWithSlashes( Hash::get( $os, 'Ordreservice.created' ) );
        $this->Csv->addRow(
            array(
                $nomOrganisme,
                $numeroOS,
                $objetOS,
                $montantOS,
                $dateOS
            )
        );
    }
    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>