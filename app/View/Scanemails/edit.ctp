<?php

/**
 *
 * Collectivites/scanemail_add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php

	$formScanemail = array(
        'name' => 'Scanemail',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Scanemail.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            ),
            'Scanemail.scriptname' => array(
                'labelText' => __d('scanemail', 'Scanemail.scriptname'),
                'inputType' => 'text',
                'labelPlaceholder' =>__d('scanemail', 'Scanemail.placeholderScriptnameAdd'),
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
            'Scanemail.name' => array(
                'labelText' =>__d('scanemail', 'Scanemail.name'),
                'inputType' => 'text',
                'labelPlaceholder' =>__d('scanemail', 'Scanemail.placeholderNameAdd'),
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
			'Scanemail.archive_folder' => array(
				'labelText' =>__d('scanemail', 'Scanemail.archive_folder'),
				'inputType' => 'text',
				'items'=>array(
					'type'=>'text',
					'required'=>false
				)
			),
            'Scanemail.mail' => array(
                'labelText' =>__d('scanemail', 'Scanemail.mail'),
               'inputType' => 'email',
                'items'=>array(
                    'type'=>'email'
                )
            ),
            'Scanemail.username' => array(
                'labelText' =>__d('scanemail', 'Scanemail.username'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
            'Scanemail.password' => array(
                'labelText' =>__d('scanemail', 'Scanemail.password'),
                'inputType' => 'password',
                'items'=>array(
                    'type'=>'password',
                    'required'=>true,
                    'value'=>$this->request->data['Scanemail']['password']
                )
            ),
            'Scanemail.hostname' => array(
                'labelText' =>__d('scanemail', 'Scanemail.hostname'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
			'Scanemail.authentication' => array(
                'labelText' => "Protocole d'authentification",
                'inputType' => 'select',
                'items'=>array(
                    'options' => [
						'imap' => 'imap',
						'oauth' => 'oauth'
					],
                    'type'=>'select',
                    'empty' => true
                )
            ),
            'Scanemail.configuration' => array(
                'labelText' => 'Options de connexion',
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required'=>true,
                    'value'=>$this->request->data['Scanemail']['configuration']
                )
            ),
            'Scanemail.port' => array(
                'labelText' =>__d('scanemail', 'Scanemail.port'),
                'inputType' => 'number',
                'items'=>array(
                    'type'=>'number',
                    'required'=>true,
                    'min'=>0
                )
            ),
        )
    );
    echo $this->Formulaire->createForm($formScanemail);

		?>
	<div id="config_oauth">
		<fieldset>
			<legend>Oauth</legend>
			<?php
			$formScanemailOauth = array(
				'name' => 'Scanemail',
				'label_w' => 'col-sm-5',
				'input_w' => 'col-sm-6',
				'input' => array(
					'Scanemail.url_api' => array(
						'labelText' => "URL d'API",
						'inputType' => 'text',
						'items'=>array(
							'type'=>'text'
						)
					),
					'Scanemail.client_id' => array(
						'labelText' => 'Client ID',
						'inputType' => 'text',
						'items'=>array(
							'type'=>'text'
						)
					),
					'Scanemail.client_secret' => array(
						'labelText' => 'Client secret',
						'inputType' => 'text',
						'items'=>array(
							'type'=>'text'
						)
					),
					'Scanemail.scope' => array(
						'labelText' => 'Scope',
						'inputType' => 'text',
						'items'=>array(
							'type'=>'text'
						)
					),
					'Scanemail.grant_type' => array(
						'labelText' => 'Grant_type',
						'inputType' => 'text',
						'items'=>array(
							'type'=>'text'
						)
					),
					'Scanemail.access_token' => array(
						'labelText' => "Token d'accès",
						'inputType' => 'text',
						'items'=>array(
							'type'=>'text'
						)
					),
				)
			);
			echo $this->Formulaire->createForm($formScanemailOauth);

			$apiTokenGen = '<i class="fa fa-refresh" id=apiTokenGen name=apiTokenGen title="Récupérer"></i>'; ?>
			<script type="text/javascript">
				var apiTokenGen = $('<?php echo $apiTokenGen; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -50px auto auto', cursor: 'pointer'}).click(function () {
					swal({
						showCloseButton: true,
						title: "<?php echo __d('default', 'Récupération du jeton d\'accès'); ?>",
						text: "<?php echo __d('default', "Êtes-vous sûr de vouloir récupérer un nouveau jeton d'accès ? <br />  <br />Si oui, il vous faudra communiquer le nouveau jeton aux applications tierces <br />se connectant précédemment avec l'ancien jeton"); ?>",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#d33",
						cancelButtonColor: "#3085d6",
						confirmButtonText: '<i class="fa fa-refresh" aria-hidden="true"></i> Récupérer',
						cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
					}).then(function (data) {
						if (data) {
							gui.request({
								url: "/scanemails/getAccessToken/" + <?php echo $this->request->data['Scanemail']['id'];?>,
								data: $(this).parents('form').serialize(),
								updateElement: $('#ScanemailEditFrom'),
								loader: true,
								loaderMessage: gui.loaderMessage
							}, function (data) {
								var s = getJsonResponse(data);
								if( !isEmpty(s['access_token'])) {
									$('#ScanemailAccessToken').val(s['access_token']);
									getJsonResponse(data);
								}
								else if( !isEmpty(s['error_description'])) {
									swal({
										showCloseButton: true,
										text: "<strong>Erreur: </strong>" + s['error_description'],
										type: "error",

									});
								}
							});
						} else {
							swal({
								showCloseButton: true,
								title: "Annulé!",
								text: "Vous n\'avez pas récupéré de nouveau jeton.",
								type: "error",

							});
						}
					});
				}).appendTo($('#ScanemailAccessToken').parent());
			</script>
		</fieldset>
	</div>
<?php
	$formScanemailSuite = array(
		'name' => 'Scanemail',
		'label_w' => 'col-sm-5',
		'input_w' => 'col-sm-6',
		'input' => array(
			'Scanemail.desktopmanager_id' => array(
				'labelText' =>__d('scanemail', 'Scanemail.desktop_id'),
				'inputType' => 'select',
				'items'=>array(
					'options' => $listDesktopsManagers,
					'type'=>'select',
					'empty' => true,
					'required'=>true
				)
			),
			'Scanemail.type_id' => array(
				'labelText' =>__d('scanemail', 'Scanemail.type_id'),
				'inputType' => 'select',
				'items'=>array(
					'options' => $types,
					'type'=>'select',
					'empty' => true,
					'value'=>$this->request->data['Type']['id']
				)
			),
			'Scanemail.soustype_id' => array(
				'labelText' =>__d('scanemail', 'Scanemail.soustype_id'),
				'inputType' => 'select',
				'items'=>array(
					'options' => $soustypes,
					'type'=>'select',
					'empty' => true,
					'value'=>$this->request->data['Soustype']['id']
				)
			)
		)
);
echo $this->Formulaire->createForm($formScanemailSuite);

    echo $this->Form->end();

?>

<script type="text/javascript">

    $('#ScanemailDesktopmanagerId').select2({allowClear: true, placeholder: "Sélectionner un bureau"});
    $("#ScanemailTypeId").select2({allowClear: true, placeholder: "Sélectionner un type"});
    $("#ScanemailSoustypeId").select2({allowClear: true, placeholder: "Sélectionner un sous-type"});

    //contruction du tableau de types / soustypes
    $('#ScanemailTypeId').change(function () {
        var typeChoix = new Array();
        typeChoix.push($('#ScanemailTypeId').val());
        chargeSoustypes(typeChoix);
    });
    function chargeSoustypes(types) {
        $.ajax({
            type: 'post',
            url: '/recherches/charge_soustypes',
            data: {TypeChoix: types},
            success: function (data) {
                $('#ScanemailSoustypeId').html(data);
            }
        });
    }
    $("<span class='formatNomScanemail alert alert-warning'><?php echo  __d('scanemail', 'Scanemail.formatScriptnameAdd'); ?></span>").insertAfter("#ScanemailScriptname");


    $('#ScanemailPassword').parent().append('<i class="fa fa-eye-slash unmaskscanemail" aria-hidden="true"></i>');

    $('.unmaskscanemail').on('click', function(){
        if($(this).parent().find('input').attr('type') == 'password') {
            changeType($(this).parent().find('input'), 'text');
            $(this).parents().find('#ScanemailEditForm i.unmaskscanemail').removeClass('fa-eye-slash').addClass('fa-eye');
        }
        else {
            changeType($(this).parent().find('input'), 'password');
            $(this).parents().find('#ScanemailEditForm i.unmaskscanemail').removeClass('fa-eye').addClass('fa-eye-slash');
        }
        return false;
    });
    /*
      function from : https://gist.github.com/3559343
      Thank you bminer!
    */
    // x = élément du DOM, type = nouveau type à attribuer
    function changeType(x, type) {
       if(x.prop('type') == type)
          return x; // ça serait facile.
       try {
          // Une sécurité d'IE empêche ceci
          return x.prop('type', type);
       }
       catch(e) {
          // On tente de recréer l'élément
          // En créant d'abord une div
          var html = $("<div>").append(x.clone()).html();
          var regex = /type=(\")?([^\"\s]+)(\")?/;
          // la regex trouve type=text ou type="text"
          // si on ne trouve rien, on ajoute le type à la fin, sinon on le remplace
          var tmp = $(html.match(regex) == null ?
             html.replace(">", ' type="' + type + '">') :
             html.replace(regex, 'type="' + type + '"') );

          // on rajoute les vieilles données de l'élément
          tmp.data('type', x.data('type') );
          var events = x.data('events');
          var cb = function(events) {
             return function() {
                //Bind all prior events
                for(i in events) {
                   var y = events[i];
                   for(j in y) tmp.bind(i, y[j].handler);
                }
             }
          }(events);
          x.replaceWith(tmp);
          setTimeout(cb, 10); // On attend un peu avant d'appeler la fonction
          return tmp;
       }
    }
	$('#ScanemailAuthentication').select2();

	$(document).ready(function () {
		$('#config_oauth').show();
		$('#config_oauth').css('display', 'block');
		changeProtocol();

		$('#ScanemailAuthentication').change(function () {
			changeProtocol();
		})
	});
	function changeProtocol() {
		var protocol = $('#ScanemailAuthentication').val();
		if (protocol == 'oauth') {
			$('#config_oauth').show();
			$('#config_oauth').css('display', 'block');
		} else if (protocol == 'imap') {
			$('#config_oauth').hide();
			$('#config_oauth').css('display', 'none');
		}
	}

	function isEmpty(value) {
		return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
	}
</script>
