<?php

/**
 *
 * Scanemail/get_scanemails.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (!empty($scanemails)) {
    $fields = array(
        'scriptname',
        'name',
        'mail',
        'desktopName',
        'type'
    );


    $actions = array(
        'edit' => array(
            "url" => Configure::read('BaseUrl') . '/scanemails/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        'delete' => array(
            "url" => Configure::read('BaseUrl') . '/scanemails/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadScanemail();"
        )
    );
    $options = array();
    $data = $this->Liste->drawTbody($scanemails, 'Scanemail', $fields, $actions, $options);
    ?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_scanemails"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-show-refresh="false"
           data-show-toggle="false"
           data-show-columns="true"
           data-toolbar="#toolbar"
           data-pagination = "true"
           >
    </table>
</div>

<script>
    //title legend (nombre de données)
    $('.table-list h3 span').remove();
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($scanemails,$options); ?>');

    $('#table_scanemails')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "scriptname",
                        title: "<?php echo __d("scanemail","Scanemail.scriptname"); ?>",
                        class: "scriptname"
                    },
                    {
                        field: "name",
                        title: "<?php echo __d("scanemail","Scanemail.name"); ?>",
                        class: "name"
                    },
                    {
                        field: "mail",
                        title: "<?php echo __d("scanemail","Scanemail.mail"); ?>",
                        class: "mail"
                    },
                    {
                        field: "desktopName",
                        title: "<?php echo __d("scanemail","Scanemail.desktopName"); ?>",
                        class: "desktopName"
                    },
                    {
                        field: "type",
                        title: "<?php echo __d("scanemail","Scanemail.type"); ?>",
                        class: "type"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            });
    $(document).ready(function () {
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }

</script>

<?php
    echo $this->LIste->drawScript($scanemails, 'Scanemail', $fields, $actions, $options);
} else {
    echo $this->Html->div('alert alert-warning',__d('scanemail', 'Scanemail.void'));
}
?>
