<?php

/**
 *
 * Consultations/get_consultations.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="bouton-search btn-group"  role="group" id="searchControls" style="margin-top:-40px;">
    <?php
    if( !empty($consultations) ){
        echo $this->Html->link(
            '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
            '#',
            array( 'escape' => false,
                      'title' => 'Accès à la recherche',
                      'onclick' => "$('#zoneConsultationGetConsultationsForm').toggle(); return false;",
                      'id' => 'formulaireConsultationButton',
                      'class' => 'btn btn-primary '
            )
        );
    }
    echo $this->Html->link(
            '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
            '#',
            array( 'escape' => false,
                      'title' => 'Réinitialiser la recherche',
                      'onclick' => "loadConsultation(); return false;",
                      'class' => 'btn btn-primary '
            )
        );
    ?>
</div>
    <?php
    $formConsultation = array(
        'name' => 'Consultation',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-4',
        'input' => array(
            'SearchConsultation.numero' => array(
                'labelText' =>__d('consultation', 'Consultation.numero'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required'=>false
                )
            ),
            'SearchConsultation.objet' => array(
                'labelText' =>__d('consultation', 'Consultation.objet'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required'=>false
                )
            ),
            'SearchConsultation.datelimitedebut' => array(
                'labelText' =>__d('consultation', 'Consultation.datelimitedebut'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text',
                    'readonly' => true,
                    'class' => 'datepicker',
                    'data-format'=>'dd/MM/yyyy',
                    'required'=>false
                )
            ),
            'SearchConsultation.datelimitefin' => array(
                'labelText' =>__d('consultation', 'Consultation.datelimitefin'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text',
                    'readonly' => true,
                    'class' => 'datepicker',
                    'data-format'=>'dd/MM/yyyy',
                    'required'=>false
                )
            ),
            'SearchConsultation.active' => array(
                'labelText' => __d('consultation', 'Consultation.active'),
                'inputType' => 'checkbox',
                'items'=>array(
                    'type'=>'checkbox',
                    'checked' => true,
                    'value'=>true
                )
            ),
            'SearchConsultation.operation_id' => array(
                'labelText' =>__d('consultation', 'Consultation.operation_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type' => 'select',
                    'required' => false,
                    'options' => $operationsList,
                    'empty' => true
                )
            ),
            'SearchConsultation.operationactive' => array(
                'labelText' => __d('operation', 'Operation.active'),
                'inputType' => 'checkbox',
                'items'=>array(
                    'type'=>'checkbox',
                    'checked' => true,
                    'value'=>true
                )
            )
        )
    );
    ?>
<div id="zoneConsultationGetConsultationsForm" class="zone-form">
    <legend>Recherche de marché</legend>
    <?php
        echo $this->Formulaire->createForm($formConsultation);
    ?>
    <div class="controls text-center" role="group" >
        <a id="reset" class="aere btn btn-info-webgfc btn-inverse "><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>
        <a id="searchConsultationButton" class="aere btn btn-success "><i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
    </div>
    <?php echo $this->Form->end(); ?>
</div>



<script type="text/javascript">
    $(document).ready(function () {
        $('.form_datetime input').datepicker({
            language: 'fr-FR',
            format: "dd/mm/yyyy",
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked'
        });
    });

    $('#ConsultationGetConsultationsForm div.required').removeClass('required');

    $('#formulaireConsultationButton').button();
    $('#searchConsultationButton').button();
    $('#reset').button();

    $('#searchConsultationButton').click(function () {
        gui.request({
            url: '/consultations/getConsultations',
            data: $("#ConsultationGetConsultationsForm").serialize(),
            loader: true,
            updateElement: $('#liste .content'),
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $('#liste .content').html(data);
        });
    });

    // Ajout de l'action de recherche via la touche Entrée
    $('#ConsultationGetConsultationsForm').keyup(function (e) {
        if (e.keyCode == 13) {
            gui.request({
                url: '/consultations/getConsultations',
                data: $("#ConsultationGetConsultationsForm").serialize(),
                loader: true,
                updateElement: $('#liste .content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#liste .content').html(data);
            });
        }
    });

    $('#zoneConsultationGetConsultationsForm').hide();

    $('#SearchConsultationName').select2({allowClear: true, placeholder: "Sélectionner un marché"});
    $('#SearchConsultationOperationId').select2({allowClear: true, placeholder: "Sélectionner une OP"});

    $('#reset').click(function () {
        location.reload();
    });



    $('#zoneDesktopmanagerGetBureauxForm').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#zoneConsultationGetConsultationsForm #searchConsultationButton').trigger('click');
            return false;
        }
        if (e.keyCode === 27) {
            $('#zoneConsultationGetConsultationsForm #reset').trigger('click');
            return false;
        }
    });
</script>

<?php if( !empty($consultations) ) {

    $fields = array(
        'numero',
        'objet',
        'operationname',
        'nbplis',
        'datelimite',
        'heurelimite',
        'active',
    );
    $actions = array(
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/consultations/edit/',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/consultations/delete/',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadConsultation();;"
        )
    );
    $options = array();
    $data = $this->Liste->drawTbody($consultations, 'Consultation', $fields, $actions, $options);
?>


<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_consultations"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-pagination = "true"
           >
    </table>
</div>

<script type="text/javascript">
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($consultations,$options); ?>');
    $('#table_consultations')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "numero",
                        title: "<?php echo __d('consultation', 'Consultation.numero'); ?>",
                        class: "numero"
                    },
                    {
                        field: "objet",
                        title: "<?php echo __d('consultation', 'Consultation.objet'); ?>",
                        class: "objetnumero"
                    },
                    {
                        field: "operationname",
                        title: "<?php echo __d('consultation', 'Consultation.operation_id'); ?>",
                        class: "operation_id"
                    },
                    {
                        field: "nbplis",
                        title: "<?php echo __d('consultation', 'Consultation.nbplis'); ?>",
                        class: "nbplis"
                    },
                    {
                        field: "datelimite",
                        title: "<?php echo 'Date limite'; ?>",
                        class: "datelimite"
                    },
                    {
                        field: "heurelimite",
                        title: "<?php echo 'Heure limite'; ?>",
                        class: "heurelimite"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            });
    $(document).ready(function () {
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }
    $('.export_consultations').show();
</script>
<?php
    echo $this->LIste->drawScript($consultations, 'Consultation', $fields, $actions, $options);
    echo $this->Js->writeBuffer();
}else{
    echo $this->Html->tag('div', __d('consultation', 'Consultation.void'), array('class' => 'alert alert-warning'));
?>
<script>
    $('.export_consultations').hide();
</script>
<?php
 }
 ?>


