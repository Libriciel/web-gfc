<?php

/**
 *
 * Consultations/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
    $formConsultation = array(
        'name' => 'Consultation',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-5',
        'form_url' => array( 'controller' => 'consultations', 'action' => 'add'),
        'input' => array(
            'Consultation.operation_id' => array(
                'labelText' =>__d('consultation', 'Consultation.operation_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type' => 'select',
                    'required' => true,
                    'empty' => true,
                    'options' => $operations
                )
            ),
            'Consultation.numero' => array(
                'labelText' =>__d('consultation', 'Consultation.numero'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
            'Consultation.objet' => array(
                'labelText' =>__d('consultation', 'Consultation.objet'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Consultation.datelimite' => array(
                'labelText' =>__d('consultation', 'Consultation.datelimite'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text',
                    'readonly' => true,
                    'class' => 'datepicker',
                    'data-format'=>'dd/MM/yyyy',
                    'required'=>true
                )
            ),
            'Consultation.heurelimite' => array(
                'labelText' => __d('consultation', 'Consultation.heurelimite'),
                'inputType' => 'time',
                'items'=>array(
                    'type' => 'time',
                    'timeFormat' => '24',
                    'interval' => 05,
                    'empty' => true,
                    'required' =>true
                )
            ),
            'Consultation.active' => array(
                'labelText' => __d('consultation', 'Consultation.active'),
                'inputType' => 'checkbox',
                'items'=>array(
                    'type'=>'checkbox',
                    'checked' => true
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formConsultation);
    echo $this->Form->end();
?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.form_datetime input').datepicker({
            language: 'fr-FR',
            format: "dd/mm/yyyy",
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked'
        });

    });

    $('#ConsultationOperationId').select2({allowClear: true, placeholder: "Sélectionner une OP"});
    $('#ConsultationHeurelimiteHour').select2({allowClear: true});
    $('#ConsultationHeurelimiteMin').select2({allowClear: true});

//    function checkConsultationNameUnique() {
//        gui.request({
//            url: "/consultations/ajaxformvalid/consultationname",
//            data: $('#ConsultationAddForm').serialize()
//        }, function (data) {
//            processJsonFormCheck(data);
//        });
//    }
//
//    $("#ConsultationName").blur(function () {
//        checkConsultationNameUnique();
//    });

    $('#s2id_ConsultationHeurelimiteHour').css({
        width: '48%',
        float: 'left'
    });

    $('#s2id_ConsultationHeurelimiteMin').css({
        width: '48%',
        float: 'right'
    });

</script>



