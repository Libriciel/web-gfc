<?php

/**
 *
 * Metadonnees/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php foreach( $intitulesagents as $b => $intituleName ) :?>
<script type="text/javascript">

//contruction du tableau de intitulesagents / bureaux
    var intitulesagents = [];
<?php /*foreach ($intitulesagents as $intitule) { */?>
    var intitule = {
        id: "<?php echo $intituleName['Intituleagent']['id']; ?>",
        name: "<?php echo addslashes( $intituleName['Intituleagent']['name'] ); ?>",
        desktopsmanagers: []
    };
    <?php foreach ($intituleName['Desktopmanager'] as $desktopmanager) { ?>
    var desktopmanager = {
        id: "<?php echo $desktopmanager['id']; ?>",
        name: "<?php echo addslashes( $desktopmanager['name'] ); ?>"
    };
    intitule.desktopsmanagers.push(desktopmanager);
        <?php } ?>
    intitulesagents.push(intitule);
        <?php /*}*/ ?>

//remplissage de la liste des intitulesagents
    for (i in intitulesagents) {
        fillDesktopsmanagers(intitulesagents[i].id);
    }

    function fillDesktopsmanagers(intituleagent_id, desktomanager_id) {
        for (i in intitulesagents) {
            if (intitulesagents[i].id == intituleagent_id) {
                for (j in intitulesagents[i].desktopsmanagers) {
                    if (intitulesagents[i].desktopsmanagers[j].id == desktomanager_id) {

                        $("#Intituleagentbyoperation<?php echo $b;?>DesktopmanagerId").append($("<option value='" + intitulesagents[i].desktopsmanagers[j].id + "' selected='selected'>" + intitulesagents[i].desktopsmanagers[j].name + "</option>"));
                    } else {
                        $("#Intituleagentbyoperation<?php echo $b;?>DesktopmanagerId").append($("<option value='" + intitulesagents[i].desktopsmanagers[j].id + "'>" + intitulesagents[i].desktopsmanagers[j].name + "</option>"));
                    }
                }
            }
        }
    }
</script>
<?php endforeach;?>
<div class="alert alert-info">Veuillez choisir au moins un bureau.</div>
<?php
$formIntituleagentbyoperation = array(
    'name' => 'Intituleagentbyoperation',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-5',
    'form_type'=>'post',
    'input' => array(
        'Intituleagentbyoperation.operation_id' => array(
            'labelText' =>__d('intituleagentbyoperation', 'Intituleagentbyoperation.operation_id'),
            'inputType' => 'select',
            'items'=>array(
                'required'=>true,
                'options' => $operations,
                'type'=>'select',
                'empty' => true
                )
            )
        )
    );
foreach( $intitulesagents as $i => $intituleName ) {
    $formIntituleagentbyoperation['input']["Intituleagentbyoperation.".$i.".intituleagent_id"] = array(
        'inputType' => 'hidden',
        'items'=>array(
            'type'=>'hidden',
            'value'=>$intituleName['Intituleagent']['id']
            ));
    $formIntituleagentbyoperation['input']["Intituleagentbyoperation.".$i.".desktopmanager_id"] = array(
        'labelText' =>$intituleName['Intituleagent']['name'],
        'inputType' => 'select',
        'items'=>array(
            'type'=>'select',
            'empty' => true
            ));
}
echo $this->Formulaire->createForm($formIntituleagentbyoperation);
echo $this->Form->end();

?>

<script type="text/javascript">
    $('#IntituleagentbyoperationOperationId').select2({allowClear: true, placeholder: "Sélectionner une OP"});


<?php foreach( $intitulesagents as $i => $intituleName ) :?>
    $('#Intituleagentbyoperation<?php echo $i;?>DesktopmanagerId').select2({allowClear: true, placeholder: "Sélectionner un bureau"});
<?php endforeach;?>
</script>
