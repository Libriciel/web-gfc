<?php

/**
 *
 * Configs/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$pagination = '<p>' . $this->Paginator->counter(
                                                array(
                                                    'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
                                                )
                ) . '</p>';

if (Set::classicExtract($this->params, 'paging.Config.pageCount') > 1) {
	$pagination = '<div class="pagination">
                                        ' . implode(
                                            '', Set::filter(
                                                array(
                                                    $this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
                                                    $this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
                                                    $this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
                                                    $this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
                                                    $this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
                                                )
                                            )
                                        ) . '
                                    </div>';
}
?>

<div class="configs index">
    <h2><?php echo __d('config', 'Configs'); ?></h2>

    <ul class="actions">
        <li><?php echo $this->Html->link(__d('config', 'New Config'), array('action' => 'add')); ?></li>
    </ul>

<?php echo $pagination; ?>
    <table>
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Config.id', __d('config', 'Config.id')); ?></th>
                <th><?php echo $this->Paginator->sort('Config.name', __d('config', 'Config.name')); ?></th>
                <th class="actions" colspan="3"><?php echo __d('default', 'Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
<?php
$i = 0;
foreach ($configs as $config):
	$i++;
	$class = ( $i % 2 ? 'odd' : 'even' );
	?>
            <tr class="<?php echo $class; ?>">
                <td><?php echo $config['Config']['id']; ?>&nbsp;</td>
                <td><?php echo $config['Config']['name']; ?>&nbsp;</td>
                <td class="actions"><?php echo $this->Html->link(__('View', true), array('action' => 'view', $config['Config']['id'])); ?></td>
                <td class="actions"><?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $config['Config']['id'])); ?></td>
                <td class="actions"><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $config['Config']['id']), null, sprintf(__('Are you sure you want to delete « %s » ?', true), $config['Config']['name'])); ?></td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
<?php echo $pagination; ?>
</div>
<script>
    $(document).ready(function () {
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
    });
</script>
