<?php

/**
 *
 * Configs/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="configs view">
    <h2><?php echo __d('config', 'Config'); ?></h2>
    <ul class="actions">
        <li><?php echo $this->Html->link(__('Edit Config', true), array('action' => 'edit', $config['Config']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('Delete Config', true), array('action' => 'delete', $config['Config']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $config['Config']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('New Config', true), array('action' => 'add')); ?> </li>
    </ul>

    <dl><?php $i = 0;
$class = ' class="altrow"'; ?>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('config', 'Config.id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo $config['Config']['id']; ?>&nbsp;</dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('config', 'Config.name'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>><?php echo $config['Config']['name']; ?>&nbsp;</dd>
    </dl>

    <div class="related">
        <h3><?php __('Related Collectivites'); ?></h3>
		<?php if (!empty($config['Collectivite'])): ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <th><?php echo __d('collectivite', 'Collectivite.id'); ?></th>
                <th><?php echo __d('collectivite', 'Collectivite.name'); ?></th>
                <th><?php echo __d('collectivite', 'Collectivite.config_id'); ?></th>
                <th><?php echo __d('collectivite', 'Collectivite.created'); ?></th>
                <th><?php echo __d('collectivite', 'Collectivite.modified'); ?></th>
                <th class="actions"><?php echo __d('default', 'Actions'); ?></th>
            </tr>
            <?php
                $i = 0;
                foreach ($config['Collectivite'] as $collectivite):
                    $class = null;
                    if ($i++ % 2 == 0) {
                            $class = ' class="altrow"';
                }
            ?>
            <tr<?php echo $class; ?>>
                <td><?php echo $collectivite['id']; ?></td>
                <td><?php echo $collectivite['name']; ?></td>
                <td><?php echo $collectivite['config_id']; ?></td>
                <td><?php echo $collectivite['created']; ?></td>
                <td><?php echo $collectivite['modified']; ?></td>
                <td class="actions">
                    <?php echo $this->Html->link(__('View', true), array('controller' => 'collectivites', 'action' => 'view', $collectivite['id'])); ?>
                    <?php echo $this->Html->link(__('Edit', true), array('controller' => 'collectivites', 'action' => 'edit', $collectivite['id'])); ?>
                    <?php echo $this->Html->link(__('Delete', true), array('controller' => 'collectivites', 'action' => 'delete', $collectivite['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $collectivite['id'])); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>

        <div class="actions">
            <ul>
                <li><?php echo $this->Html->link(__d('collectivite', 'New Collectivite'), array('controller' => 'collectivites', 'action' => 'add')); ?> </li>
            </ul>
        </div>
    </div>
</div>
