<?php

/**
 *
 * Courriers/set_tache.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<script type="text/javascript">
    var newCourrierId = <?php echo $id; ?>;
    function updateTaskNotification() {
        $('#nbTaches').html("<img src='/img/ajax-loader-mini.gif' alt='' />");
        gui.request({
            url: '/taches/getTaches/',
            updateElement: $('#nbTaches')
        });
    }

    function validTache(id) {
        gui.request({
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#tabs'),
            url: "/taches/checkTache/" + id
        }, function (data) {
            getJsonResponse(data);
            $('#tabs').tabs('load', $('#tabs').tabs('option', 'selected'));
            updateTaskNotification();
        });
    }

    function grabTache(id) {
        gui.request({
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#tabs'),
            url: "/taches/grabTache/" + id
        }, function (data) {
            getJsonResponse(data);
            $('#tabs').tabs('load', $('#tabs').tabs('option', 'selected'));
            updateTaskNotification();
        });
    }

    function releaseTache(id) {
        gui.request({
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#tabs'),
            url: "/taches/releaseTache/" + id
        }, function (data) {
            getJsonResponse(data);
            $('#tabs').tabs('load', $('#tabs').tabs('option', 'selected'));
            updateTaskNotification();
        });
    }

    function setTache(id) {
        var url;
        var title;
        if (id != parseInt(id)) {
            url = "<?php echo "/taches/add/" . $id; ?>";
            title = "<?php echo __d('tache', 'New Tache'); ?>";
        } else {
            url = "<?php echo "/taches/edit/"; ?>" + id;
            title = "<?php echo __d('tache', 'Edit Tache'); ?>";
        }
        $(".modal").empty();
        gui.formMessage({
            url: url,
            title: title,
            loader: true,
            width: 470,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#taches'),
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents(".modal").modal('hide');
                    $(this).parents(".modal").empty();
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $(this).parents(".modal").find('form');
                    if (form_validate(form)) {
                        gui.request({
                            url: url,
                            data: form.serialize()
                        }, function (data) {
                            getJsonResponse(data);
                            if (typeof affichageEtapes !== 'undefined' && $.isFunction(affichageEtapes)) {
                                affichageEtapes(newCourrierId);
                            } else {
                                $("#taches").empty();
                                gui.request({
                                    url: "/courriers/setTache/" + newCourrierId,
                                    updateElement: $('#taches')
                                });
                            }
                            updateTaskNotification();
                        });
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        });

    }

    function deleteTache(id) {
        swal({
                    showCloseButton: true,
            title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
            text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
            cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: "/taches/delete/" + id,
                    updateElement: $('#taches'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    getJsonResponse(data);
                    if (typeof affichageEtapes !== 'undefined' && $.isFunction(affichageEtapes)) {
                        affichageEtapes(newCourrierId);
                    } else {
                        $("#taches").empty();
                        gui.request({
                            url: "/courriers/setTache/" + newCourrierId,
                            updateElement: $('#taches')
                        });
                    }
                    updateTaskNotification();
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n'avez pas supprimé, ;) .",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    }

</script>
<?php
if (!$canAddTask) {
    echo $this->Html->tag('div', __d('tache', 'Tache.cantAddTask'), array('class' => 'alert alert-warning'));
}
if (count($taches) > 0):
?>
<div style="display: none" id="panel_taches">
    <legend id="tacheHeader">
        <h4 id="tacheHeader"><?php echo __d('courrier','Taches'); ?>
            <?php if ($canAddTask): ?>
            <a href='#' class='btn btn-info-webgfc addTache'>
                <i class='fa fa-plus' alt='Créer une nouvelle tâche' title='Créer une nouvelle tâche'  aria-hidden='true'></i>
            </a>
            <?php endif; ?>
        </h4>
    </legend>
    <div class="panel-body" id="setTacheRow" style="padding: 0px">
        <?php

        foreach ($taches as $tache){
            switch ($tache['Tache']['statut']) {
                case 2:
                    $titleSpanClass =  "tacheFinish";
                    $titleIconTitle = __d("tache", "tache.traitee");
                    break;
                case 1:
                    $titleSpanClass =  "tacheNormal";
                    $titleIconTitle = "Tache en cours de traitement";
                    break;
                case 0:
                    if($tache['retard']){
                        $titleSpanClass =  "tacheLate";
                        $titleIconTitle = __d("tache", "tache.retard");
                    }else{
                        $titleSpanClass =  "tacheNormal";
						$titleIconTitle = "Tâche en attente de prise en charge";
                    }
                    break;
            }
        ?>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><?php echo $tache['Tache']['name']; ?>
                        <span class="<?php echo $titleSpanClass; ?>">
                            <i class="fa fa-dot-circle-o" alt ="<?php echo $titleIconTitle; ?>"  title ="<?php echo $titleIconTitle; ?>"  aria-hidden="true"></i>
                        </span>
                    </h4>
                </div>
                <div class="panel-body">
					<?php if( !empty($tache['Tache']['delai_nb'])):?>
						<div class="delai">
							<p style="margin-left: 10px">Délai pour traiter la tâche: <?php echo $tache['Tache']['delai_nb'].' ' .$delaiTranslate[$tache['Tache']['delai_unite']]; ?></p>
						</div>
					<?php endif;?>

                    <ul>
                        <?php
                        foreach ($tache['Desktop'] as $desktop) {
                                $actionDesktop = '';
                                $actionDesktopClass = '';
                                if ($tache['Tache']['statut'] == 2 && $tache['Tache']['act_desktop_id'] == $desktop['id']) {
									$actionDesktop = $this->Html->tag('i','',array('class'=>'fa fa-check', 'style' => 'color:green;', 'alt' => __d('Tache', 'tache.validatedBy'), 'title' => __d('Tache', 'tache.validatedBy'), 'aria-hidden'=>true));
                                    $actionDesktopClass = 'ui-state-valid';
                                }
                                if ($tache['Tache']['statut'] == 1 && $tache['Tache']['act_desktop_id'] == $desktop['id']) {
									$actionDesktop = $this->Html->tag('i','',array('class'=>'fa fa-cogs', 'alt' => __d('Tache', 'tache.grabedBy'), 'title' => __d('Tache', 'tache.grabedBy'), 'aria-hidden'=>true));
                                    $actionDesktopClass = 'alert alert-warning';
                                }
                        ?>
                        <li class="list-group-item"><?php echo $actionDesktop . ' ' .  $desktop['User'][0]['nom'] . ' ' . $desktop['User'][0]['prenom']; ?></li>
                        <?php
                            }
                        ?>
                    </ul>
                </div>
                <div class="panel-footer " role="group">
                    <div class="btn-group">
                    <?php
                    if($rights['edit'] && $tache['Tache']['statut'] != 2 && $tache['isOwner']):
                    ?>
                        <a href="#" class="tacheEdit btn btn-info-webgfc btn-secondary" title="<?php echo  __d('default', 'Button.edit'); ?>" idTache = "<?php echo $tache['Tache']['id']; ?>">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    <?php endif;
					if($rights['delete'] && $tache['Tache']['statut'] != 2 && $tache['isOwner']):
                    ?>
                        <a href="#" class="tacheDelete btn btn-danger btn-secondary" title="<?php echo  __d('default', 'Button.delete'); ?>" idTache = "<?php echo $tache['Tache']['id']; ?>">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </a>
                    <?php
                    endif;
                    if($tache['canGrab']):
                    ?>
                        <a href="#" class="tacheGrab btn btn-info-webgfc btn-secondary" title="<?php echo  __d('Tache', 'tache.grab'); ?>" itemId = "<?php echo $tache['Tache']['id']; ?>">
                            <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                        </a>
                    <?php
                    endif;
                    if($tache['canRelease']):
                    ?>
                        <a href="#" class="tacheRelease btn btn-info-webgfc btn-secondary" title="<?php echo  __d('Tache', 'tache.release'); ?>" itemId = "<?php echo $tache['Tache']['id']; ?>">
                            <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                        </a>
                    <?php
                    endif;
                    if($tache['canValid']):
                    ?>
                        <a href="#" class="tacheValid btn btn-info-webgfc btn-secondary" title="<?php echo  __d('Tache', 'tache.validate'); ?>" itemId = "<?php echo $tache['Tache']['id']; ?>">
                            <i class="fa fa-check-square-o" aria-hidden="true"></i>
                        </a>
                    <?php
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        </div>
        <?php  } ?>
    </div>
</div>
    <?php
else:
    ?>
<!-- aucune taches -->
<div style="display: none" id="panel_taches">
    <legend id="tacheHeader">
        <h4 id="tacheHeader"><?php echo __d('courrier','Taches'); ?>
            <?php if ($canAddTask): ?>
            <a href='#' class='btn btn-info-webgfc addTache'>
                <i class='fa fa-plus' alt='Créer une nouvelle tâche' title='Créer une nouvelle tâche'  aria-hidden='true'></i>
            </a>
            <?php endif; ?>
        </h4>
    </legend>
    <div class="panel-body">
        <div class="alert alert-warning"><?php echo __d('tache', 'Tache.void'); ?></div>
    </div>
</div>
<?php
endif;
?>
<script type="text/javascript">
    if ($("#tacheFooter").length == 0) {
        $('#panel_taches').show();
        $(".setTachesContents").appendTo($('#setTacheRow').show());
    } else {
        $('#setTacheRow').show();
        $(".setTachesContents").appendTo($('#panel_taches').show());
    }
    $('.tacheValid').button().click(function () {
        validTache($(this).attr("itemId"));
    });

    $('.tacheGrab').button().click(function () {
        grabTache($(this).attr("itemId"));
    });

    $('.tacheRelease').button().click(function () {
        releaseTache($(this).attr("itemId"));
    });

    $('.tacheEdit').button().click(function () {
        setTache($(this).attr("idTache"));
    });

    $('.tacheDelete').button().click(function () {
        deleteTache($(this).attr("idTache"));
    });

<?php if ($canAddTask) { ?>
    $('.addTache').button().click(function () {
        setTache();
    });
<?php } ?>

    $('.listTaches li.tache').hover(function () {
        $(this).addClass('alert alert-warning');
    }, function () {
        $(this).removeClass('alert alert-warning');
    });


	$('.delai').css('background', '#f5f5f5');
	$('.delai').css('color', '#000');
	$('.delai').css('text-shadow', 'none');
</script>
