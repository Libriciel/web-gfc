<?php

/**
 *
 * Courriers/set_qualification.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script type="text/javascript">
    //contruction du tableau de types / soustypes
    var types = [];
    <?php foreach ($types as $type) { ?>
    var type = {
        id: "<?php echo $type['Type']['id']; ?>",
        name: "<?php echo $type['Type']['name']; ?>",
        soustypes: []
    };
        <?php foreach ($type['Soustype'] as $stype) { ?>
    var soustype = {
        id: "<?php echo $stype['id']; ?>",
        name: "<?php echo $stype['name']; ?>"
    };
    type.soustypes.push(soustype);
        <?php } ?>
    types.push(type);
    <?php } ?>

    function fillSoustypes(type_id, stype_id) {
        $("#CourrierSoustypeId").empty();
        $("#CourrierSoustypeId").append($("<option value=''> --- </option>"));
        for (i in types) {
            if (types[i].id == type_id) {
                for (j in types[i].soustypes) {
                    if (types[i].soustypes[j].id == stype_id) {
                        $("#CourrierSoustypeId").append($("<option value='" + types[i].soustypes[j].id + "' selected='selected'>" + types[i].soustypes[j].name + "</option>"));
                    }
                    else {
                        $("#CourrierSoustypeId").append($("<option value='" + types[i].soustypes[j].id + "'>" + types[i].soustypes[j].name + "</option>"));
                    }

                }
            }
        }
    }
</script>

<?php
$direction = array(
    0 => array(
        'img' => '<i class="fa fa-sign-out" style="color:#e51809;" aria-hidden="true"></i>',
        'txt' => __d('courrier', 'Courrier.courrier_sortant')
    ),
    1 => array(
        'img' => '<i class="fa fa-sign-in" style="color:#2ec07e;" aria-hidden="true"></i>',
        'txt' => __d('courrier', 'Courrier.courrier_entrant')
    ),
    2 => array(
        'img' => '<i class="fa fa-level-up" style="color:#5397a7;" aria-hidden="true"></i>',
        'txt' => __d('courrier', 'Courrier.courrier_interne')
    )
);

$tabDelaiUnite = array('jour', 'semaine', 'mois');

$formOptions = array(
    'type' => 'file',
    'url' => array(
        'controller' => 'courriers',
        'action' => 'setQualification',
        $this->data['Courrier']['id']
    )
);

echo $this->Form->create('CourrierQualification', $formOptions);
?>

<fieldset class="fieldsetEdit">
    <legend><?php echo __d('courrier', 'Qualification'); ?></legend>
    <?php
    echo $this->Form->input('Courrier.id');
    echo $this->Form->input('Types.Type', array('label' => __d('type', 'Type'), "options" => array(), 'disabled' => $isInCircuit ? 'disabled' : ''));
    echo $this->Form->input('Courrier.soustype_id', array('label' => __d('soustype', 'Soustype'), "options" => array(), 'disabled' => $isInCircuit ? 'disabled' : ''));
    ?>
</fieldset>

<fieldset class="fieldsetEdit">
    <legend><?php echo __d('courrier', 'Informations complémentaires'); ?></legend>
    <?php
    echo $this->Form->input('Courrier.reference', array('disabled' => 'disabled', 'label' => __d('courrier', 'Courrier.reference')));
    ?>
    <div class="input text">
        <?php
        echo $this->Html->tag('label', __d('courrier', 'Courrier.entrant/sortant'), array('for' => 'SoustypeEntrant'));
        $soustypeEntrantContent = (isset($this->data['Soustype']['entrant']) ? $direction[$this->data['Soustype']['entrant']]['img'] . " " . $direction[$this->data['Soustype']['entrant']]['txt'] : '');
        echo $this->Html->tag('span', $soustypeEntrantContent, array('class' => 'fakeDisabledInputText'));
        ?>
    </div>
    <div class="input text">
        <?php
        echo $this->Html->tag('label', __d('soustype', 'Soustype.delai'), array('for' => 'SoustypeDelai'));
        $soustypeDelaiContent = ((isset($this->data['Soustype']['delai_nb']) && isset($this->data['Soustype']['delai_unite'])) ? $this->data['Soustype']['delai_nb'] . " " . $tabDelaiUnite[$this->data['Soustype']['delai_unite']] . (($this->data['Soustype']['delai_nb'] > 1) ? 's' : '' ) : '&nbsp;');
        echo $this->Html->tag('span', $soustypeDelaiContent, array('class' => 'fakeDisabledInputText'));
        ?>
    </div>
</fieldset>
<?php echo $this->Form->end(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $("#TypesType").append($("<option value=''> --- </option>"));
        //remplissage de la liste des types
        for (i in types) {
            $("#TypesType").append($("<option value='" + types[i].id + "'>" + types[i].name + "</option>"));
        }
        //definition de l action sur type
        $("#TypesType").bind('change', function () {
            var type_id = $('option:selected', this).attr('value');
            fillSoustypes(type_id);
        });
        //definition de l action sur stype
        $("#CourrierSoustypeId").bind('change', function () {
            var stype_id = $('option:selected', this).attr('value');
            $('.msgChange').remove();

            var reloadImgString = '<?php echo $this->Html->image('/img/reload.png'); ?>';
            var reloadImgFullString = '<?php echo $this->Html->image('/img/reload.png', array('alt' => __d('courrier', 'Courrier.messagechange.short'), 'title' => __d('courrier', 'Courrier.messagechange'))); ?>';

            $('.fakeDisabledInputText').after($('<span></span>').addClass('msgChange').html(reloadImgString + ' ' + "<?php echo __d('courrier', 'Courrier.messagechange'); ?>"));
            $('.context_accordion .ui-accordion-header').each(function () {
                if ($('a', this).length > 0) {
                    $(this).append($('<span></span>').addClass('msgChange').html(reloadImgFullString));
                }
            });
        });
<?php
//initialisation du type et du soustype si un soustype est defini pour le courrier
if (!empty($this->data['Soustype']['id'])) {
?>
        $("#TypesType option").each(function () {
            if ($(this).attr('value') == "<?php echo $this->data['Soustype']['type_id']; ?>") {
                $(this).attr('selected', 'selected');
                var type_id = parseInt("<?php echo $this->data['Soustype']['type_id']; ?>");
                var stype_id = parseInt("<?php echo $this->data['Soustype']['id']; ?>");

                if (stype_id != "") {
                    fillSoustypes(type_id, stype_id);
                }
                else {
                    fillSoustypes(type_id);
                }
            }
        });
<?php
}
?>
    });
</script>
