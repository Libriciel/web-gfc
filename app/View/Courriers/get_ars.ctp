<?php

/**
 *
 * Courriers/get_ars.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$icons = array(
    'audio' => '/img/audio.png',
    'video' => '/img/video.png',
    'ott' => '/img/document.png',
    'email' => '/img/email.png',
    'sms' => '/img/phone.png'
);

//liste des modeles
if (empty($ar) || (!empty($ar) && !$ar['Ar']['used'])) {
    if (!empty($armodels)) {
        echo $this->Form->create('Armodel',array(
                'class' => 'form-horizontal',
                'inputDefaults' => array(
                    'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                    'div' => array('class' => 'form-group'),
                    'label' => array('class' => 'control-label  col-sm-3'),
                    'between' => '<div class="controls  col-sm-5">',
                    'after' => '</div>',
                    'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                )));
?>
<legend><?php echo __d('relement', 'Relement.Rmodels'); ?></legend>
<div class="panel-body">
    <?php
        foreach ($armodels as $armodel) {
            //todo changement affichage radio
    ?>
    <input type="radio" name="data[Armodel][choice]" id="armodel_input_<?php echo $armodel['id']; ?>" value="<?php echo $armodel['id']; ?>" class $armodel="col-sm-1"/>
    <?php
            echo $this->Element('ctxdoc', array('ctxdoc' => $armodel, 'gfcDocType' => 'Armodel', 'cssClass' => 'ctxdoc', 'path' => null));
            ?>
    <script>
        $('#armodel_input_<?php echo $armodel['id']; ?>').prependTo('#Armodel_<?php echo  $armodel['id']; ?> .ctxdoc_name');
    </script>
        <?php
        }
    ?>
</div>
<?php
        echo $this->Form->end();
        if (!$fromForbiddenLocation) {
    ?>
        <?php if ( Configure::read('CD') != 81 ) :?>
            <span class="bttn_gen_doc btn btn-info-webgfc bttnGenerateModel"><i class="fa fa-cog" title="<?php echo __d('ar', 'Ar.generate') ?>"></i> <?php echo __d('ar', 'Ar.generate') ?></span>
        <?php else :?>
            <span class="bttn_gen_doc btn btn-info-webgfc bttnGenerateModel"><i class="fa fa-cog" title="<?php echo __d('ar', 'Ar.generatecd') ?>"></i> <?php echo __d('ar', 'Ar.generatecd') ?></span>
        <?php endif;?>
            <?php
//            echo $this->Html->tag('span', __d('ar', 'Ar.generate'), array('class' => 'bttnGenerateModel btn btn-info-webgfc'));
        }
    } else {
        echo $this->Html->tag('div', __d('courrier', 'Ars.void'), array('class' => 'alert alert-warning'));
    }
}


//accusé de réception généré
if (!empty($ar)) {
    if ($ar['Ar']['used']) { //accusé de réception utilisé
        $arTitle = $this->Html->tag('legend', __d('ar', 'Ar.ar_used'), array('class' => ''));
        $arElement = $this->Element('ctxdoc', array('ctxdoc' => $ar['Ar'], 'gfcDocType' => 'Ar', 'cssClass' => 'arCtxdoc'));
        if ($ar['Ar']['format'] == 'sms') {
                $dlContent = $this->Html->tag('dt', __d('ar', 'Ar.sms_dest'), array('class' => ''));
                $dlContent .= $this->Html->tag('dd', $ar['Ar']['sms_dest']);
                $dlContent .= $this->Html->tag('dt', __d('ar', 'Ar.sms_content'), array('class' => ''));
                $dlContent .= $this->Html->tag('dd', $ar['Ar']['sms_content']);
                $arElement .= $this->Html->tag('dl', $dlContent);
        } else if ($ar['Ar']['format'] == 'email') {
                $dlContent = $this->Html->tag('dt', __d('ar', 'Ar.email_dest'), array('class' => ''));
                $dlContent .= $this->Html->tag('dd', $ar['Ar']['email_dest']);
                $dlContent .= $this->Html->tag('dt', __d('ar', 'Ar.email_title'), array('class' => ''));
                $dlContent .= $this->Html->tag('dd', $ar['Ar']['email_title']);
                $dlContent .= $this->Html->tag('dt', __d('ar', 'Ar.email_content'), array('class' => ''));
                $dlContent .= $this->Html->tag('dd', $ar['Ar']['email_content']);
                $arElement .= $this->Html->tag('dl', $dlContent);
        }
        $arElement .= $this->Html->tag('div', __d('ar', 'Ar.created') . ": " . $this->Html2->ukToFrenchDateWithHour($ar['Ar']['created']), array('class' => 'arCreatedAt'));
        echo $this->Html->tag('fieldset', $arTitle . $arElement, array('id' => 'arElement'));
    } else { //accusé de réception choisi et non utilisé
        $arTitle = $this->Html->tag('legend', __d('ar', 'Ar.ar_generated'), array('class' => ''));
        echo $this->Form->create('Ar',array(
                'class' => 'form-horizontal',
                'inputDefaults' => array(
                    'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                    'div' => array('class' => 'form-group'),
                    'label' => array('class' => 'control-label  col-sm-3'),
                    'between' => '<div class="controls  col-sm-5">',
                    'after' => '</div>',
                    'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                )));
        $arElement = $this->Form->input('Ar.id', array('value' => $ar['Ar']['id']));
        $arElement .= $this->Element('ctxdoc', array('ctxdoc' => $ar['Ar'], 'gfcDocType' => 'Ar', 'cssClass' => 'arCtxdoc'));
        if ($ar['Ar']['format'] == 'sms') {
            $arElement .= $this->Form->input('Ar.sms_dest', array('value' => $ar['Ar']['sms_dest'], 'label' =>array('text'=> __d('ar', 'Ar.sms_dest'),'class'=>'control-label  col-sm-5'),'class' => 'form-control'));
            $arElement .= $this->Form->input('Ar.sms_content', array('value' => $ar['Ar']['sms_content'], 'label' =>array('text'=> __d('ar', 'Ar.sms_content'),'class'=>'control-label  col-sm-5'),'class' => 'form-control'));
        } else if ($ar['Ar']['format'] == 'email') {
            $arElement .= $this->Form->input('Ar.email_dest', array('value' => $ar['Ar']['email_dest'], 'label' => array('text'=>__d('ar', 'Ar.email_dest'),'class'=>'control-label  col-sm-5'),'class' => 'form-control'));
            $arElement .= $this->Form->input('Ar.email_title', array('value' => $ar['Ar']['email_title'], 'label' => array('text'=>__d('ar', 'Ar.email_title'),'class'=>'control-label  col-sm-5'),'class' => 'form-control'));
            $arElement .= $this->Form->input('Ar.email_content', array('value' => $ar['Ar']['email_content'], 'type' => 'textarea', 'label' =>array('text'=> __d('ar', 'Ar.email_content'),'class'=>'control-label  col-sm-5'),'class' => 'form-control'));
        }
        $arElement .= $this->Html->tag('div', __d('ar', 'Ar.created') . " " . $this->Html2->ukToFrenchDateWithHour($ar['Ar']['created']), array('class' => 'arCreatedAt'));
        $arElement .= $this->Form->input('Ar.used', array('type' => 'hidden', 'value' => 1));

        $arElement .= $this->Form->end();
        echo $this->Html->tag('fieldset', $arTitle . $arElement, array('id' => 'arElement'));
        if (!$fromForbiddenLocation  ) {
            echo $this->Html->tag('span', __d('ar', 'Ar.use'), array('id' => 'bttnArSave', 'class' => 'bttnArSave btn btn-info-webgfc'));
        }
    }
}
?>


<script type="text/javascript">

    $('.bttnGenerateModel').button().click(function () {
        var updateElement = $('#ars');
        var active = false;
        $('#ArmodelGetArsForm input[type=radio]').each(function () {
            if ($(this).prop("checked")) {
                active = true;
            }
        });

        if (active) {
            gui.request({
                url: '/courriers/setArs/<?php echo $fluxId; ?>',
                data: $('#ArmodelGetArsForm').serialize(),
                loader: true,
                loaderMessage: gui.loaderMessage,
                updateElement: updateElement
            }, function (data) {
                getJsonResponse(data);
                updateElement.removeAttr('loaded');
                affichageTabContext('<?php echo $fluxId; ?>');
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "<?php echo __d('armodel', 'Bttn.generate.disabled') ?><br/><?php echo __d('armodel', 'Bttn.generate.disabled.text') ?>",
                type: "error",

            });


        }
    });


                    $('#bttnArSave').button().click(function () {
//            var updateElement = $('.context_accordion').find('.ui-state-active');
                        var updateElement = $('#ars');
                        gui.request({
                            url: '/ars/select',
                            data: $('#ArGetArsForm').serialize(),
                            loader: true,
                            loaderMessage: gui.loaderMessage,
                            updateElement: updateElement
                        }, function (data) {
                            getJsonResponse(data);
                            updateElement.removeAttr('loaded');
                            affichageTabContext('<?php echo $fluxId; ?>');
                        });
                    });


                    initContextDocumentBttnActions();

                    $('.ctxdocBttnDelete').unbind('click').click(function () {
                        var updateElement = $('.context_accordion').find('.ui-state-active');
                        var itemId = $(this).parent().parent().attr('itemId');
                        swal({
                    showCloseButton: true,
                            title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
                            text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#d33',
                            cancelButtonColor: '#3085d6',
                            confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
                            cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
                        }).then(function (data) {
                            if (data) {
                                gui.request({
                                    url: '/ars/delete/' + itemId,
                                    loader: true,
                                    loaderMessage: gui.loaderMessage,
                                    updateElement: updateElement
                                }, function (data) {
                                    getJsonResponse(data);
                                    updateElement.removeAttr('loaded');
                                    loadContextTab(updateElement);
                                });
                            } else {
                                swal({
                    showCloseButton: true,
                                    title: "Annulé!",
                                    text: "Vous n'avez pas supprimé, ;) .",
                                    type: "error",

                                });
                            }
                        });
                        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
                    });

</script>
