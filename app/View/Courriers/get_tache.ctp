<?php

/**
 *
 * Courriers/get_tache.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (isset($viewOnly) && $viewOnly === true) {
    echo $this->Element('viewOnly');
    ?>

	<script type="text/javascript">
		$('.saveEditBtn').hide();
		$('.undoEditBtn').hide();
	</script>
<?php
}
?>
<script type="text/javascript">
    function validTache(id) {
        gui.request({
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#tabs'),
            url: "/taches/checkTache/" + id
        }, function (data) {
            getJsonResponse(data);
            $('#tabs').tabs('load', $('#tabs').tabs('option', 'selected'));
            updateTaskNotification();
        });
    }

    function grabTache(id) {
        gui.request({
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#tabs'),
            url: "/taches/grabTache/" + id
        }, function (data) {
            getJsonResponse(data);
            $('#tabs').tabs('load', $('#tabs').tabs('option', 'selected'));
            updateTaskNotification();
        });
    }

    function releaseTache(id) {
        gui.request({
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#tabs'),
            url: "/taches/releaseTache/" + id
        }, function (data) {
            getJsonResponse(data);
            $('#tabs').tabs('load', $('#tabs').tabs('option', 'selected'));
            updateTaskNotification();
        });
    }
</script>
<?php
if (!$canAddTask){
    echo $this->Html->tag('div', __d('tache', 'Tache.cantAddTask'), array('class' => 'alert alert-warning'));
}

if (count($taches) > 0){
?>
<div class="row listTaches" id="setTacheRow">
    <?php
    foreach ($taches as $tache) {
        if ($tache['Tache']['statut'] == 2) {
            $titleSpanClass =  "tacheFinish";
            $titleIconTitle = __d("tache", "tache.traitee");
        } else if ($tache['Tache']['statut'] == 1) {
             $titleSpanClass =  "tacheNormal";
			$titleIconTitle = "Tâche en attente de prise en charge";
        } else if ($tache['Tache']['statut'] == 0) {
			if($tache['Tache']['retard']){
				$titleSpanClass =  "tacheLate";
				$titleIconTitle = __d("tache", "tache.retard");
			}else{
				$titleSpanClass =  "tacheNormal";
				$titleIconTitle = "Tâche en attente de prise en charge";
			}
        } else {
            $titleSpanClass =  "tacheNormal";
            $titleIconTitle = __d("tache", "tache.encours");
        }
    ?>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"><?php echo $tache['Tache']['name']; ?>
                    <span class="<?php echo $titleSpanClass; ?>">
                        <i class="fa fa-dot-circle-o" alt ="<?php echo $titleIconTitle; ?>"  title ="<?php echo $titleIconTitle; ?>"  aria-hidden="true"></i>
                    </span>
                </h4>
            </div>
            <div class="panel-body">
				<?php if( !empty($tache['Tache']['delai_nb'])):?>
					<div class="delai">
						<p style="margin-left: 10px">Délai pour traiter la tâche: <?php echo $tache['Tache']['delai_nb'].' ' .$delaiTranslate[$tache['Tache']['delai_unite']]; ?></p>
					</div>
				<?php endif;?>
                <ul>
                    <?php
                    foreach ($tache['Desktop'] as $desktop) {
                        $actionDesktop = '';
                        $actionDesktopClass = '';
                        if ($tache['Tache']['statut'] == 2 && $tache['Tache']['act_desktop_id'] == $desktop['id']) {
							$actionDesktop = $this->Html->tag('i','',array('class'=>'fa fa-check', 'style' => 'color:green;', 'alt' => __d('Tache', 'tache.validatedBy'), 'title' => __d('Tache', 'tache.validatedBy'), 'aria-hidden'=>true));
                            $actionDesktopClass = 'ui-state-valid';
                        }
                        if ($tache['Tache']['statut'] == 1 && $tache['Tache']['act_desktop_id'] == $desktop['id']) {
							$actionDesktop = $this->Html->tag('i','',array('class'=>'fa fa-cogs', 'alt' => __d('Tache', 'tache.grabedBy'), 'title' => __d('Tache', 'tache.grabedBy'), 'aria-hidden'=>true));
                            $actionDesktopClass = 'alert alert-warning';
                        }
                    ?>
					<li class="list-group-item"><?php echo $actionDesktop . ' ' .  $desktop['User'][0]['nom'] . ' ' . $desktop['User'][0]['prenom']; ?></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <?php
    }
    ?>
</div>
<?php
    }else{
?>
<div class="alert alert-warning"><?php echo __d('tache', 'Tache.void'); ?></div>
<?php
    }
?>

<script type="text/javascript">
    $('.tacheValid').button().click(function () {
        validTache($(this).attr("itemId"));
    });

    $('.tacheGrab').button().click(function () {
        grabTache($(this).attr("itemId"));
    });

    $('.tacheRelease').button().click(function () {
        releaseTache($(this).attr("itemId"));
    });
</script>
