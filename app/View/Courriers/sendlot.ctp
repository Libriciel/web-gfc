<?php

/**
 *
 * Courriers/sendlot.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$formSend = array(
    'name' => 'Send',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Send.desktop_id' => array(
            'labelText' =>__d('courrier', 'Courrier.desktop_to_send'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => @$desktopsToSend,
                'empty' => true
            )
        ),
        'Send.getback' => array(
            'labelText' =>__d('courrier', 'Courrier.getback'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox'
            )
        ),
        'Comment.objet' => array(
            'labelText' =>__d('comment', 'Comment.objet'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea'
            )
        )
    )
);

echo $this->Formulaire->createForm($formSend);
?>
<?php foreach ($this->request->data['checkItem'] as $item) { ?>
<input type="hidden" name="data[checkItem][]" value="<?php echo $item; ?>" />
<input type="hidden" name="data[Send][courrier_id][]" value="<?php echo $item; ?>" />
<?php } ?>
<?php
    echo $this->Form->end();
?>
<div class="modal-footer zone-form">
    <div class="controls ">
        <a class="btn btn-danger-webgfc btn-inverse " id="btn_cancel"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a class="btn btn-success " id="btn_ok"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#SendDesktopId').select2({allowClear: true, placeholder: "Sélectionner un destinataire"});
        $('#btn_ok').click(function () {
            gui.request({
                url: "/courriers/sendlot/execute",
                data: $('#SendSendlotForm').serialize(),
                loader: true,
                loaderMessage: gui.loaderMessage,
            }, function (data) {
                window.location.href = "/environnement/index/0/user";
            })
        });
        $('#btn_cancel').click(function () {
            $(this).parents('.modal').modal('hide');
            $(this).parents('.modal').empty();
        });
    });
</script>
