<?php

/**
 *
 * Courriers/get_ars.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<span class="btn btn-info-webgfc ctxdocBttnsendSms" style="padding-bottom:5px;"><i class="fa fa-envelope" title="Envoyer un SMS"></i> Envoyer un SMS</span>
<div style="padding-top:15px;"></div>

<?php
if (empty($smsDatas)) {
	echo $this->Html->tag('div', "Aucun SMS n'a encore été envoyé", array('class' => 'alert alert-warning'));
}
else {
	?>
	<div class="ctxdoc" style="padding-top:10px;">
    <span>
        <?php
			foreach( $smsDatas as $s => $sms ) {
				echo '<dl>'
					. '<dt>SMS envoyé à : </dt><dd>'.$sms['Sms']['numero']. '</dd>'
					. '<dt>Message : </dt><dd>'.$sms['Sms']['message']. '</dd>'
					. '<dt>Envoyé le : </dt><dd>'.  $this->Html2->ukToFrenchDateWithHourAndSlashes( $sms['Sms']['created'] ). '</dd>'
					. '</dl>';
			}
        ?>
    </<pan>
</div>
<?php
}
?>
<script type="text/javascript">
	$('.ctxdocBttnsendSms').click(function () {
		var updateElement = $('.context_accordion').find('.ui-state-active');
		gui.formMessage({
			title: "Envoi de SMS",
			url: '/sms/sendSms/' + <?php echo $fluxId;?>,
			buttons: {
				'<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
					$(this).parents('.modal').modal('hide');
					$(this).parents('.modal').empty();
				},
				'<i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer': function () {
					$('#SmsSendSmsForm').validator();
					var form = $('#SmsSendSmsForm');

					// validation du n° de téléphone
					var correctNumber = false;
					var tel = $('#SmsNumero').val();
					tel = tel.replace(/\s+/g, '');
					re = new RegExp(/^0(6|7)\d{8}$/);
					if(tel.match(re)) {
						correctNumber = true;
					}

					if (form_validate($(form)) && correctNumber) {
						gui.request({
							url: '/sms/sendSms/' + <?php echo $fluxId;?>,
							data: form.serialize()
						}, function (data) {
							getJsonResponse(data);
							updateElement.removeAttr('loaded');
							affichageTabContext(<?php echo $fluxId;?>);
						});

						$(this).parents('.modal').modal('hide');
						$(this).parents('.modal').empty();
					}
					else {
						swal({
							showCloseButton: true,
							title: "Oops...",
							text: "Veuillez vérifier votre formulaire!",
							type: "error",

						});
					}
				}
			}
		});
	});

</script>
