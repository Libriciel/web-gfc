<?php

/**
 *
 * Courriers/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php echo $this->Html->script('contactinfo.js', array('inline' => true));?>

<script type="text/javascript">
    //obtenir les boutons gauches(bouton fonction)
    function getFlowControls(fromMesServices) {
        var url = "<?php echo '/courriers/getFlowControls/' . $courrier['Courrier']['id']; ?>";
        if (fromMesServices) {
            url += "/1";
        }

        gui.request({
            url: url,
            noErrorMsg: true
        }, function (data) {
            $("body").append(data);
        });
    }

<?php if ($rights['edit'] && !$isClosed) { ?>
    //Enregistre les changement
    function submitAll(submits, index) {
        if (submits[index] != 'end') {
            gui.request({
                url: submits[index].url,
                data: submits[index].data
            }, function (data) {
                var json = getJsonResponse(data);
                if (json.loadContext == "reload") {
                    $('.context_content .ui-accordion-header').removeAttr('loaded');
                    $('.context_content .ui-accordion-header .msgChange').remove();
                    loadContextTab($('.context_accordion').find('.ui-state-active'));
                }
                submitAll(submits, index + 1);
            });
        } else {
            <?php if ($rights['getFlowControls']) { ?>
            getFlowControls();
            <?php } ?>
            <?php
                if( $envactuel === 'disp' ) {
                    $desktopId = $desktopidactuel;
                }
                else {
                    $desktopId = $desktopidactuel;
                }
                if( $fromMesServices ) {
                    $desktopId = $courrier['Courrier']['desktop_creator_id'];
                }
            ?>
        }
    }
<?php } ?>
</script>
<div class="container">
	<div id="leftButton" title="Flux Précédent" style="font-size:24px;margin-right:55px;"><i class='fa fa-chevron-left fa-3x'></i></div>
	<h3 class=""  style="margin-left: 150px;">
		<div id="backButton" style="margin-left:-100px;position:absolute;margin-top:-5px;"></div>
		Flux: <?php  echo $courrier['Courrier']['name'];?> (<?php  echo !empty($courrier['Courrier']['reference']) ? $courrier['Courrier']['reference'] : '';?>)
	</h3>
	<div id="rightButton" title="Flux Suivant" style="font-size:24px;"><i class='fa fa-chevron-right fa-3x' ></i></div>


    <div class="row">
        <div class="table-list">

            <div class="row">
                <div class="infos col-sm-6">
                    <div id="flux_infos" class="tabbable tabs-left">
                        <ul class="nav nav-tabs">
							<?php
							if ($rights['getInfos']) {
								echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-info fa-fw" aria-hidden="true"></i>', '#infos', array('class' => ($rights['setInfos'] && !$isClosed) ? 'editable' : '', "data-toggle"=>"tab" ,'escape' => false)),array('class'=>'active', 'title'=>__d('courrier', 'Informations')));
							}
							if ($rights['getMeta'] || $isAdmin ) {
//								echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-sticky-note fa-fw" aria-hidden="true"></i>', '#medas', array('class' => ( ($rights['setMeta'] && !$isClosed)  || $isAdmin ) ? 'editable' : '', "data-toggle"=>"tab" ,'escape' => false)),array('title'=>__d('courrier', 'Metadonnees')));
								echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-sticky-note fa-fw" aria-hidden="true"></i>'.' ('.$nbMeta. ')', '#medas', array('class' => ( ($rights['setMeta'] )  || $isAdmin ) ? 'editable' : '', "data-toggle"=>"tab" ,'escape' => false)),array('title'=>__d('courrier', 'Metadonnees').' ( Nb obligatoire ) '));
							}
							if ($rights['getTache']) {
								echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-tasks fa-fw" aria-hidden="true"></i>', '#taches', array('class' => ($rights['setTache'] && !$isClosed) ? 'editable' : '', "data-toggle"=>"tab" ,'escape' => false)), array('id' => 'taskTab','title'=>__d('courrier', 'Taches')));
							}
							if ($rights['getAffaire']) {
								echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-folder-open fa-fw" aria-hidden="true"></i>', '#affaires', array('class' => ($rights['setAffaire'] && !$isClosed) ? 'editable' : '', "data-toggle"=>"tab" ,'escape' => false)),array('title'=>__d('courrier', 'Affaire')));
							}
							echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-commenting fa-fw" aria-hidden="true"></i>'.' ('.$nbComment. ')', '#comments', array('class' => 'editable', "data-toggle"=>"tab" ,'escape' => false)),array('title'=>__d('comment', 'Comment.tab')));
							?>
                        </ul>
                        <div class="tab-content row">

						<div id="controls2" style="height: 40px;">
							<div id="flowControlsBis" class="btn-group col-sm-6" style="float:left;"></div>
							<div id="dataControlsBis" class="btn-group col-sm-4" style="float:right;"></div>
						</div>
                <?php if ($rights['getInfos']) { ?>
                            <div  class="tab-pane fade in active col-sm-10"  id="infos">
                            </div>
                <?php }?>
                <?php if ($rights['getMeta']) {?>
                            <div  class="tab-pane fade col-sm-10" id="medas">
                            </div>
                <?php }?>
                <?php if ($rights['getTache']) {?>
                            <div  class="tab-pane fade  col-sm-10"  id="taches">
                            </div>
                <?php }?>
                <?php if ($rights['getAffaire']) {?>
                            <div  class="tab-pane fade col-sm-10" id="affaires">
                            </div>
                <?php }?>
                            <div  class="tab-pane fade col-sm-10    " id="comments">
                            </div>
                        </div>
                    </div>
                    <div id="controls">
                        <div id="flowControls" class="btn-group col-sm-6"></div>
                        <div id="dataControls" class="btn-group col-sm-4" style="float:right;"></div>
                    </div>
                </div>


                <div id="flux_context" class="context  col-sm-6  tabs-left">
            <?php
            $context_params = array(
                    'fluxId' => $courrier['Courrier']['id'],
                    'rights' => $rights,
                    'parent' => $parent,
                    'sortant' => $sortant,
                    'mainDoc' => $mainDoc
            );
            echo $this->Element('context', $context_params);
            ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    //Mode de Modification
    function editMode(fluxId) {
        affichageTab('edit');
        affichageMode = "edit";
        $('#flux_infos .tab-content').children('.tab-pane').empty();
        $('.saveEditBtn').show();
        $('.undoEditBtn').show();
    }

    function viewMode(fluxId) {
        affichageTab('view');
        affichageMode = "view";
        // $('.saveEditBtn').hide();
        // $('.undoEditBtn').hide();
    }
    //initialisation acces page (modifier/view)
    var courrierId = <?php echo $courrier['Courrier']['id']; ?>;
    $(document).ready(function () {
        <?php if($initMode=="edit"): ?>
        var affichageMode = "edit";
        editMode(courrierId);
        <?php else: ?>
        var affichageMode = "view";
        viewMode(courrierId);
        <?php endif;?>


    });

    //gestion Nav tabs
    $('#flux_infos .nav-tabs a').on('shown.bs.tab', function (e) {
        affichageTab(affichageMode);
    });


    //gestion quel Tab on affiche
    function affichageTab(mode) {
        setOrget = 'get';
        if (mode == "edit") {
            setOrget = 'set';
        }
        if ($('#infos').length > 0 && $('#infos').is(':visible')) {
            <?php if(!isset($rights['setInfos']) && !$rights['setInfos']): ?>
                if (mode == "edit") {
                    setOrget = 'get';
                }
            <?php endif;?>
            gui.request({
                url: "<?php echo "/courriers/"?>" + setOrget + "<?php echo "Infos/" . $courrier['Courrier']['id']; ?>",
                updateElement: $('#infos')
            });
        }
        if ($('#medas').length > 0 && $('#medas').is(':visible')) {
            setOrget = 'set';
            gui.request({
                url: "<?php echo "/courriers/"?>" + setOrget + "<?php echo "Meta/" . $courrier['Courrier']['id']; ?>",
                updateElement: $('#medas')
            });
        }
        if ($('#taches').length > 0 && $('#taches').is(':visible')) {
            <?php if(!$rights['setTache']):?>
                setOrget = 'get';
            <?php else :?>
                setOrget = 'set';
            <?php endif;?>
            if (mode == "edit") {
                setOrget = 'set';
            }
            else {
                setOrget = 'get';
            }
            gui.request({
                url: "<?php echo "/courriers/"?>" + setOrget + "<?php echo "Tache/" . $courrier['Courrier']['id']; ?>",
                updateElement: $('#taches')
            });
        }
        if ($('#affaires').length > 0 && $('#affaires').is(':visible')) {
            <?php if(!isset($rights['setAffaire']) && !$rights['setAffaire']): ?>
                if (mode == "edit") {
                    setOrget = 'get';
                }
            <?php endif;?>
            <?php if(!$rights['setAffaire']):?>
                setOrget = 'get';
            <?php else :?>
                setOrget = 'set';
            <?php endif;?>
            gui.request({
                url: "<?php echo "/courriers/"?>" + setOrget + "<?php echo "Affaire/" . $courrier['Courrier']['id']; ?>",
                updateElement: $('#affaires')
            });
        }
        if ($('#comments').length > 0 && $('#comments').is(':visible')) {
            gui.request({
                url: "<?php echo "/courriers/"?>" + 'set' + "<?php echo "Comments/" . $courrier['Courrier']['id']; ?>",
                updateElement: $('#comments')
            });
        }
    }


    //activation des boites de boutons
    gui.buttonbox({element: $('#flowControls, #flowControlsBis'), align: "left"});
    gui.buttonbox({element: $('#dataControls, #dataControlsBis'), align: "center"});


    //bouton enregistre et retourner

    gui.addbutton({
        element: $('#dataControls, #dataControlsBis'),
        button: {
            content: "<i class='fa fa-times-circle-o' aria-hidden='true'></i> <span class='link-title'><?php echo __d('courrier', 'Annuler'); ?></span>",
            title: '<?php echo __d('courrier', 'Annuler'); ?>',
            class: "btn-danger-webgfc btn-inverse  undoEditBtn",
            action: function () {
                window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index"; ?>";
            }
        }
    });

    $('.undoEditBtn').css( 'margin-right', '5px');
    $('.undoEditBtn').css( 'border-radius', '4px');

    gui.addbutton({
        element: $('#dataControls, #dataControlsBis'),
        button: {
            content: "<i class='fa fa-floppy-o' aria-hidden='true'></i> <span class='link-title'><?php echo __d('courrier', 'Enregistrer'); ?></span>",
            title: '<?php echo __d('courrier', 'Enregistrer'); ?>',
            class: "btn-success  saveEditBtn",
            action: function () {
                var formActive = $('#flux_infos').children('.tab-content').find('.in').find('form');
                $.each(formActive, function (index, form) {
                    var url = form.action;
                    var id = form.id;

                    if (form_validate($('#' + id))) {
                        gui.request({
                            url: url,
                            data: $('#' + id).serialize()
                        }, function (data) {
                            var obj = jQuery.parseJSON(data);
                            if (obj.newCourrierSoustypeSens != false && obj.newCourrierSoustypeSens != undefined && obj.newCourrierDelaiNb != undefined && obj.newCourrierDelaiUnite != undefined && obj.newCourrierId != undefined) {
                                updateQualification(obj.newCourrierSoustypeSens, obj.newCourrierDelaiNb, obj.newCourrierDelaiUnite);
                                updateContextRight(obj.newCourrierId);
                            }
                            layer.msg(obj.message);
                        });
                    } else {
                        swal({
                            showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error"
                        });
                    }
                });
            }
        }
    });
    $('.saveEditBtn').css( 'border-radius', '4px');
    //definition des boutons de controle du flux dans son circuit
    <?php if ($rights['getFlowControls'] && !$fromHistorique && !$fromMesServices && !$fromMesCopies && !$fromBannetteCopy && !$fromTask ) { ?>
    getFlowControls();
    <?php } else if ($rights['getFlowControls'] && (!$fromHistorique && !$fromTask && $fromMesServices) ) { ?>
    getFlowControls(true);
    <?php } ?>


    $('#leftButton').click(function () {
        var url = window.location.href;
        var urlCouper = url.split('/');
        var desktopnextId = urlCouper[urlCouper.length - 1];
        gui.request({
            url: "<?php echo "/desktops/nextFlux/".$courrier['Courrier']['id']."/" ?>" + desktopnextId,
            data: $('#CourrierSetInfosForm').serialize()
        }, function (data) {
            var json = JSON.parse(data);
            if (json.void != undefined) {
                layer.msg(json.void);
                window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index"; ?>";
            } else {
                if (json.dataFluxComplet['fin'] != undefined) {
                    layer.msg(json.dataFluxComplet['fin']);
                }
                window.location.href = "<?php echo '/courriers/view/'?>" + json.dataFluxComplet["next"] + "/" + desktopnextId;
            }
        });
    });
    $('#rightButton').click(function () {
        var url = window.location.href;
        var urlCouper = url.split('/');
        var desktopnextId = urlCouper[urlCouper.length - 1];
        gui.request({
            url: "<?php echo "/desktops/nextFlux/".$courrier['Courrier']['id']."/" ?>" + desktopnextId,
            data: $('#CourrierSetInfosForm').serialize()
        }, function (data) {
            var json = JSON.parse(data);
            if (json.void != undefined) {
                layer.msg(json.void);
                window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index"; ?>";
            } else {
                if (json.dataFluxComplet['debut'] != undefined) {
                    layer.msg(json.dataFluxComplet['debut']);
                }
                window.location.href = "<?php echo '/courriers/view/'?>" + json.dataFluxComplet["before"] + "/" + desktopnextId;
            }
        });
    });

    $(window).resize(function () {
        iconTextTabLeft();
        buttonText();
    });
    iconTextTabLeft();
    buttonText();
    function iconTextTabLeft() {
        if ($(window).width() < 1280) {
            $('.tabbable-text').hide();
            $('#flux_infos .tab-content').css('margin-left', '50px');
        } else {
            $('.tabbable-text').show();
            $('#flux_infos .tab-content').css('margin-left', '24px');
        }
    }
    function buttonText() {
        if ($(window).width() < 1380) {
            $('.link-title').hide();
        } else {
            $('.link-title').show();
        }
    }

    // Bouton de Retour
    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                title: 'Retour aux bannettes',
                class: "btn-info-webgfc",
				id: "back",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
                }
            }
        ]
    });

    // $('#back').css( 'margin-left', '-90%');
</script>


<script type="text/javascript">
	<?php if( $newcommentAvailable && Configure::read('Comment.Alert') ) :?>
		var affichageMode = "edit";
		swal({
			showCloseButton: true,
			title: "Nouveau commentaire non lu",
			text: "Vous avez un nouveau commentaire non lu!",
			type: "info"
		});
		$('.fa-commenting ').trigger("click");
	<?php endif;?>
</script>
