<?php

/**
 *
 * Courriers/set_affaire.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (!empty($dossiers)) {

    $fields = array(
        'name'
    );
    $actions = array(
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/affaires/edit',
            "updateElement" => "$('#flux_content')",
            "formMessage" => false
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/affaires/delete',
            "updateElement" => "$('#flux_content')",
//            "refreshAction" => "loadTypesSoutypes();"
        )
    );

//    $options = array('check_inactive' => true, 'inactive_value' => 0, 'inactive_field' => 'active', 'inactive_model' => 'Collectivite');
    $tabactions = array();
    $tabupdateElements = array();

    foreach ($actions as $key => $val) {
        $tabactions[$key] = $val['url'];
        $tabupdateElements[$key] = $val['updateElement'];
    }

    $rights = array();

    $rights['read'] = true;
    $rights['delete'] = true;
    $data= '['.substr($this->Liste->drawTree($dossiers, 'Dossier', $fields, $tabactions, $tabupdateElements, $rights),0,-1).']';

}
?>

<?php


/*$formAffaireNew = array(
    'name' => 'Courrier',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-7',
    'input' => array(
        'Affaire.name' => array(
            'labelText' =>__d('affaire', 'Affaire.name'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text',
                'value'=>$newAffaireName
            )
        ),
        'Affaire.comment' => array(
            'labelText' =>__d('affaire', 'Affaire.comment'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea'
            )
        ),
        'Affaire.dossier_id' => array(
            'labelText' =>__d('affaire', 'Affaire.dossier'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $listDossiers,
                'empty' => true
            )
        ),
        'Dossier.name' => array(
            'labelText' =>__d('dossier', 'Dossier.name'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text',
//                'value'=>$newDossierName
            )
        ),
        'Dossier.comment' => array(
            'labelText' =>__d('dossier', 'Dossier.comment'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea'
            )
        )
    )
);
$form = $this->Formulaire->createForm($formAffaireNew);
$form .= $this->Form->end();*/
?>

    <div class="tab-content clearfix tab-pane-affaire" id="affaire_assoc_exist">

<!--        <ul class="nav nav-tabs titre" role="tablist">
            <li class="active"><a href="#affaire_assoc_exist"  data-toggle="tab"><?php echo __d('courrier', 'Courrier.affaire.associate'); ?></a></li>
            <li><a href="#affaire_assoc_new"  data-toggle="tab"><?php echo __d('courrier', 'Courrier.affaire.assoc.new'); ?></a></li>
        </ul>-->

        <legend id="tacheHeader">
            <h4 id="tacheHeader"><?php echo __d('courrier','Dossiers / Affaires'); ?>
                <a href='#' class='btn btn-info-webgfc addAffaire'>
                    <i class='fa fa-plus' alt='Associer à une nouvelle affaire' title='Associer à une nouvelle affaire'  aria-hidden='true'></i>
                </a>
            </h4>
        </legend>


        <?php if (!empty($dossiers)): ?>
        <table id="dossiers_table" data-toggle="table">
            <tbody>

            </tbody>
        </table>
        <!--<div class="tab-pane active divContent" id="affaire_assoc_exist">-->
            <?php /*echo $list; */?>
        <!--</div>-->
        <?php endif; ?>
        <!-- <div class="tab-pane divContent" id="affaire_assoc_new">
            <?php /*echo $form;*/ ?>
        </div> -->
    </div>
<script type="text/javascript">
    <?php if (!empty($dossiers)): ?>

        $('#dossiers_table').bootstrapTable({
            data: <?php echo $data;?>,
            width: "1200px",
            columns: [
                {
                    field: 'name',
                    title: 'Dossiers / Affaires',
                    class: 'name'
                },
                {
                    field: 'edit',
                    title: 'Modifier',
                    class: 'actions thEdit',
                    width: "80px",
                    align: "center"
                },
                {
                    field: 'attach',
                    title: 'Associer',
                    class: 'actions thAttach',
                    width: "80px",
                    align: "center"
                },
                {
                    field: 'info',
                    title: 'Information',
                    class: 'actions thInfo',
                    width: "80px",
                    align: "center"
                }
            ]
        })
        .on('search.bs.table', function (e, text) {
            itemEdit();
            itemAttach();
            itemInfo();
        })
        .on('page-change.bs.table', function (number, size) {
            addStateFocus();
        });
    <?php endif;?>



    $(document).ready(function () {
        addStateFocus();
    });


    function addStateFocus() {
        var selectedAffaire = "<?php echo $selectedAffaire;?>";
        $('#dossiers_table tr td.thAttach').children('i').each(function () {
            var idAffaire = $(this).attr('id');
            if( idAffaire == selectedAffaire) {
                $(this).parents('tr').css('background', '#d0e5f5');
                $(this).parents('tr').css('color', '#1d5987');
                $('#dossiers_table tr td.thAttach').children('i#' + selectedAffaire).toggleClass('fa-chain-broken fa-link');
                $('#dossiers_table tr td.thAttach').children('i#' + selectedAffaire).attr('title' ,'Dissocier');
            }
        });

        $('#dossiers_table tr td.thEdit').children('i').each(function () {
            var idDossier  = $(this).attr('id');
            if(idDossier == '-1') {
                $('#dossiers_table tr td.thEdit').children('i#' + idDossier).hide();
                $('#dossiers_table tr td.thInfo').children('i#' + idDossier).hide();
            }
        });

    }



    function itemEdit() {
        $('#dossiers_table .itemEdit').click(function () {
            var itemid = $(this).attr('itemid');

            var url = "";
            var hasimg = $(this).parents('tr').find('i').hasClass('treeCollapseButton');

            var hasParent = $(this).attr('parentId');

            if( hasimg ) {
                url = "/dossiers/edit/" + itemid;
            }
            else  {
                url = "/affaires/edit/" + itemid;
            }
            gui.formMessage({
                url: url,
                loader: true,
                loaderMessage: gui.loaderMessage,
                updateElement: $('#flux_content'),
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {

                        var areValid = true;
                        $('.modal-body #dossiers_tabs form').each(function () {
                            if ($(this).attr('action') !== undefined && $(this).attr('enctype') != "multipart/form-data") {
                                if (!form_validate($(this))) {
                                    areValid = false;
                                }
                            }
                        });
                        if (areValid) {
                            var submits = {};
                            var index = 0;
                            $('.modal-body form').each(function () {
                                if ($(this).attr('action') !== undefined && $(this).attr('enctype') != "multipart/form-data") {
                                    submits[index] = {'url': $(this).attr('action'), 'data': $(this).serialize()};
                                    index++;
                                }
                            });
                            submits[index] = 'end';
                            gui.submitAll({
                                submits: submits,
                                index: 0,
                                endFunction: "loadDossiersAffaires()",
                                loader: true,
                                loaderElement: $('#flux_content'),
                                loaderMessage: gui.loaderMessage
                            });
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        } else {
                            swal({
                                showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }

            });
        });
    }

    function itemAttach() {
        $('#dossiers_table .itemAttach').click(function () {

            var itemid = $(this).attr('itemid');
            var url = "";
            if( itemid != 'undefined' ) {
                url = "/courriers/setAffaire/" + "<?php echo $this->data['Courrier']['id'];?>" + "/" + itemid;
            }
            else {
                url = "/courriers/setAffaire/" + "<?php echo $this->data['Courrier']['id'];?>";
            }

            gui.request({
                url: url,
                loaderElement: $("#affaire_assoc_exist"),
                loaderMessage: gui.loaderMessage,
                data: $('#CourrierSetAffaireForm').serialize(),
            }, function (data) {
                getJsonResponse(data);
                $('#tabs').tabs('load', $('#tabs').tabs('option', 'selected'));
                $("#affaire_assoc_exist").empty();
                gui.request({
                    url: "/courriers/setAffaire/" + "<?php echo $this->data['Courrier']['id'];?>",
                    updateElement: $("#affaire_assoc_exist")
                });
            });
        });
    }

    function itemInfo() {
        $('#dossiers_table .itemInfo').click(function (e) {
            var itemid = $(this).attr('itemid');
            var url = "";
            var hasimg = $(this).parents('tr').find('i').hasClass('treeCollapseButton');
            var hasParent = $(this).attr('parentId');

            if( hasimg ) {
                url = "/dossiers/getComment/" + itemid;
            }
            else {
                url = "/affaires/getComment/" + itemid;
            }

            var informationPanel = $(this);
            $.ajax({
                type: 'post',
                url: url,
                data: itemid,
                success: function (data) {
                    if (data != "") {
                        informationPanel.popover({content: data, container: informationPanel});
                    } else {
                        informationPanel.popover({content: "Aucune information", container: informationPanel});
                    }
                }
            });
        });
    }
</script>
<script type="text/javascript">
    itemEdit();
    itemAttach();
    itemInfo();
</script>
<script type="text/javascript">
    function loadDossiersAffaires() {
        $("#affaire_assoc_exist").empty();
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . '/courriers/setAffaire/' . $this->data['Courrier']['id'];?>",
            updateElement: $("#affaire_assoc_exist"),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

    $('.addAffaire').button().click(function () {
        setAffaire();
    });

    function setAffaire(id) {
        var url = '/affaires/add/' + "<?php echo $this->data['Courrier']['id'];?>";
        $('.modal').empty();
        gui.formMessage({
            url: url,
            loader: true,
            width: 470,
            loaderMessage: gui.loaderMessage,
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents(".modal").modal('hide');
                    $(this).parents(".modal").empty();
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $(this).parents(".modal").find('form');
                    if (form_validate(form)) {
                        gui.request({
                            url: url,
                            data: form.serialize(),
                        }, function (data) {
                            getJsonResponse(data);
                            $("#affaire_assoc_exist").empty();
                            gui.request({
                                url: "/courriers/setAffaire/" + "<?php echo $this->data['Courrier']['id'];?>",
                                updateElement: $("#affaire_assoc_exist")
                            });
                        });
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        });

    }
</script>

<script type="text/javascript">
    $('#AffaireDossierId').select2();

    //Pouvoir tout déplier/replier d'un clic
    $('.treeCollapseButton').css('cursor', 'pointer');
    $('.treeCollapseButton').toggle(function () {
        $(this).attr('src', '/img/arrow_right.png');
        var tempThis = $(this).parents('tr').find('td:first-child').html();
        var countThis = (tempThis.match(/&nbsp;/g) || []).length;
        var tempNext = $(this).parents('tr').next().find('td:first-child').html();
        var countNext = (tempNext.match(/&nbsp;/g) || []).length;
        var indexThisTr = $('#dossiers_table').find('tr').index($(this).parents('tr'));
        for (var i = indexThisTr + 1; i < $('#dossiers_table').find('tr').length; i++) {
            var tr = $('#dossiers_table').find('tr').eq(i);
            if (tr.find('td:first-child').html() != undefined && (tr.find('td:first-child').html().match(/&nbsp;/g) || []).length != countThis) {
                if ((tr.find('td:first-child').html().match(/&nbsp;/g) || []).length >= countNext) {
                    tr.hide();
                }
            } else {
                return false;
            }
        }
    }, function () {
        var temp = $(this).parents('tr').next().find('td:first-child').html();
        var count = (temp.match(/&nbsp;/g) || []).length;
        $('#dossiers_table').find('td').each(function () {
            if ($(this).html().match(/&nbsp;/g) != undefined && ($(this).html().match(/&nbsp;/g) || []).length >= count) {
                $(this).parent('tr').show();
            }
        });
        $(this).attr('src', '/img/arrow_down.png');
    });
</script>
