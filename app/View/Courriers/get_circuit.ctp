<?php

/**
 *
 * Courriers/get_circuit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script>
    function set_comment_description_height(etapid, heightComment) {
        $("#comment_description_" + etapid).css('height', heightComment);
        $("#test_" + etapid).css('height', heightComment);
    }
</script>
<?php
if (empty($circuit['Circuit']['id'])) {
    echo $this->Html->tag('div', __d('circuit', 'Circuit.void'), array('class' => 'alert alert-warning'));
} else {
    $typesEtape = array(
        ETAPESIMPLE => 'Simple',
        ETAPEET => 'Collaborative',
        ETAPEOU => 'Concurrente'
    );
    if (empty($visas)) {
?>
<legend><?php echo $circuit['Circuit']['nom']; ?></legend>
<table class="show-circuit" id="circuitGetCircuit">
    <thead ><th class="etapeInfo"></th><th class="arrow"></th><th class="etapeDescription" ></th></thead>
<tbody>
    <?php
        foreach ($etapes as $etape){
            $ulCompoContent = array();
            foreach ($etape['Composition'] as $composition) {
                if( !empty( $composition['Desktopmanager']) ) {

                    // Pour distinguer un desktop du parapheur
                    $compositionName = '';
                    if( $composition['trigger_id'] == '-1') {
                        if( !empty($soustypes) ) {
                            if( !in_array( $composition['soustype'],  array_keys($soustypes)) ) {
                                $compositionName = "<i style='color:red;'>Etape Parapheur : La valeur du sous-type sélectionné n'existe pas dans le Parapheur cible</i>";
                            }
                            else {
                                $sousTypeSelectionne = $soustypes[$composition['soustype']];
                                if( !empty( $sousTypeSelectionne ) || $sousTypeSelectionne == '0' ) {
                                    $compositionName = 'Etape Parapheur : '.$sousTypeSelectionne;
                                }
                                else {
                                    $compositionName = 'Etape Parapheur';
                                }
                            }
                        }
                        else {
                            $compositionName = '<i style="color:red;">Etape Parapheur : Connecteur Parapheur en erreur</i>';
                        }
                    }
					else if( $composition['trigger_id'] == '-3') {
						if( !empty($documents) ) {
							$listeDocumentSelectionne = $documents[$composition['type_document']];
							if( in_array( $composition['type_document'], [ Configure::read('Pastell.fluxStudioName').'-mailsec', Configure::read('Pastell.fluxStudioName').'-visa', Configure::read('Pastell.fluxStudioName').'-ged'] ) ) {
								$listeDocumentSelectionne = Configure::read('Pastell.typesdoccircuit')[$composition['type_document']];
							}
							if( !empty( $listeDocumentSelectionne )) {
								$compositionName = 'Etape Pastell : '.$listeDocumentSelectionne;
								if( $composition['type_document'] == 'document-a-signer' ) {
									$typeSelectionne = $composition['inforequired_type_document'];
									$compositionName = 'Etape Pastell : '.$listeDocumentSelectionne.' : '.$typeSelectionne;
								}
							}
							else {
								$compositionName = 'Etape Pastell';
							}

							if(!$hasCheminement
									&& $listeDocumentSelectionne == Configure::read('Pastell.typesdoc')[Configure::read('Pastell.fluxStudioName')]
									&& $composition['trigger_id'] == '-3' && empty( $composition['inforequired_type_document'] )
							) {
								$compositionName = '<i style="color:red;">Etape Pastell : Aucun cheminement défini dans le sous-type sélectionné</i>';
							}
						}
						else {
							$compositionName = '<i style="color:red;">Etape Pastell : Connecteur Pastell en erreur</i>';
						}
					}
					else {
                        $compositionName = $composition[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS];
                    }
//                    if( $composition['trigger_id'] == '-1') {
//                        $compositionName = 'Etape Parapheur';
//                    }else {
//                        $compositionName = $composition[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS];
//                    }
                    $ulCompoContent[] = $this->Html->div('desktop_name',$compositionName);
                }
                else {
                    $ulCompoContent[] = $this->Html->div('desktop_name_error', "Aucun bureau n'est associé à cette étape. <br />Veuillez contacter votre administrateur.");
                }
            }
    ?>
    <tr>
        <td class="etapeInfo">
            <div class="panel panel-default" id="comment_<?php echo $etape['Etape']['id']; ?>">
                <div class="panel-heading">
                    <h4 class="panel-title"><?php echo $etape['Etape']['ordre']; ?><span><?php echo $typesEtape[$etape['Etape']['type']]; ?></span></h4>
                </div>
                <div class="panel-body">
                    <legend><?php echo $etape['Etape']['nom']; ?></legend>
                    <ul>
                        <?php foreach ($ulCompoContent as $composition): ?>
                        <li  class="composition"><?php echo $composition;?></li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
        </td>
        <td class="arrow"><div><div id="test_<?php echo $etape['Etape']['id']; ?>"></div><i class="fa fa-long-arrow-down" aria-hidden="true" ></i></div></td>
        <td class="etapeDescription">
            <div class="panel panel-default " id="comment_description_<?php echo $etape['Etape']['id']; ?>" >
                <div class="panel-body">
                    <legend>Description: </legend>
                    <p><?php echo $etape['Etape']['description']; ?></p>
                </div>
            </div>
        </td>
    </tr>
<script>
    $('.desktop_name_error').css({color: 'red'});

    var etapid = "<?php echo $etape['Etape']['id']; ?>";
    $(window).resize(function () {
        var heightComment = $("#comment_" + etapid).height();
        set_comment_description_height(etapid, heightComment);
    });
    var heightComment = $("#comment_" + etapid).height();
    set_comment_description_height(etapid, heightComment);
</script>
    <?php  }//endforeach ?>
</tbody>
</table>
        <?php
    } else {
?>
<legend><?php echo $circuit['Circuit']['nom']; ?></legend>
<table class="show-circuit" id="circuitGetCircuit">
    <thead ><th class="etapeInfo"></th><th class="arrow"></th><th class="etapeDescription"></th></thead>
<tbody>
    <?php
        $chkValid = '<i class="fa fa-check" aria-hidden="true" style="color:#3C765A"></i>';
        $refusImg = '<i class="fa fa-times-o" aria-hidden="true" style="color:#A94464"></i>';
        $convertVisaAction = array(
            'RI' => '',
            'OK' => $chkValid,
            'KO' => $refusImg,
            'IL' => $chkValid,
            'IP' => $chkValid,
            'JP' => '',
            'JS' => '',
            'ST' => '',
            'IN' => ''
        );
        $numTraitementAct = $visas[0]['Traitement']['numero_traitement'];
        $isTreatedTraitementAct = $visas[0]['Traitement']['treated'];

        $inserted = false;
        $cptInserted = 0;
        foreach ($visas as $i => $visa){

            if ($visa['Visa']['numero_traitement'] < $numTraitementAct || $isTreatedTraitementAct) {
                $specialClassEtape = ' panel-success';
                $descriptionEtapeTitle = 'Etape validée';
                $arrowDirectionColor = '#B6CDA2';
            } else if ($visa['Visa']['numero_traitement'] == $numTraitementAct) {
                $specialClassEtape = ' panel-danger';
                $descriptionEtapeTitle = 'Etape en cours';
                $arrowDirectionColor = '#A94464';
            } else if ($visa['Visa']['numero_traitement'] > $numTraitementAct) {
                $specialClassEtape = ' panel-default';
                $descriptionEtapeTitle = 'Etape non traitée';
                $arrowDirectionColor = '#b2b2b2';
            }

            //preparation de l'affichage des étapes insérées
            $specialClassEtapeInserted="";
            if ($inserted && $cptInserted > 1) {
                    $cptInserted--;
                    $specialClassEtapeInserted .= 'etape-insert';
            } else if ($inserted && $cptInserted == 1) {
                    $cptInserted = 0;
                    $inserted = false;
                    $specialClassEtapeInserted .= 'etape-insert';
            }

            $ulCompoContent = array();
            foreach ($visa['Composition'] as $composition) {

                if ($composition[CAKEFLOW_TRIGGER_MODEL]['action'] == 'IL') {
                    $inserted = true;
                    $cptInserted = 2;
                } else if ($composition[CAKEFLOW_TRIGGER_MODEL]['action'] == 'IP') {
                    $inserted = true;
                    $cptInserted = 1;
                }



				// Pour distinguer un desktop du parapheur
				$compositionName = '';
				if( $composition[CAKEFLOW_TRIGGER_MODEL]['trigger_id'] == '-1') {
					if( !empty($soustypes) ) {
						if( !in_array( $composition[CAKEFLOW_TRIGGER_MODEL]['soustype'],  array_keys($soustypes)) ) {
							$compositionName = "<i style='color:red;'>Etape Parapheur : La valeur du sous-type sélectionné n'existe pas dans le Parapheur cible</i>";
						}
						else {
							if( !empty($composition[CAKEFLOW_TRIGGER_MODEL]['soustype']) || $composition[CAKEFLOW_TRIGGER_MODEL]['soustype'] == '0' ) {
								$sousTypeSelectionne = $soustypes[$composition[CAKEFLOW_TRIGGER_MODEL]['soustype']];
								if( !empty( $sousTypeSelectionne ) || $sousTypeSelectionne == '0' ) {
									$compositionName = 'Etape Parapheur : ' . $sousTypeSelectionne;
								}
							}
							else {
								$compositionName = 'Etape Parapheur';
							}
						}
					}
					else {
						$compositionName = '<i style="color:red;">Etape Parapheur : Connecteur Parapheur en erreur</i>';
					}
				}
				else if( $composition[CAKEFLOW_TRIGGER_MODEL]['trigger_id'] == '-3') {
					if( !empty($documents) ) {

						$listeDocumentSelectionne = @$documents[$composition[CAKEFLOW_TRIGGER_MODEL]['type_document']];
						if( in_array( $composition[CAKEFLOW_TRIGGER_MODEL]['type_document'], [ Configure::read('Pastell.fluxStudioName').'-mailsec', Configure::read('Pastell.fluxStudioName').'-visa', Configure::read('Pastell.fluxStudioName').'-ged'] ) ) {
							$listeDocumentSelectionne = Configure::read('Pastell.typesdoccircuit')[$composition[CAKEFLOW_TRIGGER_MODEL]['type_document']];
						}
						if( !empty( $listeDocumentSelectionne )) {
							$compositionName = 'Etape Pastell : '.$listeDocumentSelectionne;
							if( $composition[CAKEFLOW_TRIGGER_MODEL]['type_document'] == 'document-a-signer' ) {
								$typeSelectionne = $composition[CAKEFLOW_TRIGGER_MODEL]['inforequired_type_document'];
								$compositionName = 'Etape Pastell : '.$listeDocumentSelectionne.' : '.$typeSelectionne;
							}
						}
						else {
							if( in_array($composition[CAKEFLOW_TRIGGER_MODEL]['type_document'], array_keys(Configure::read('Pastell.typesdoc')))) {
								$compositionName = 'Etape Pastell : '.Configure::read('Pastell.typesdoc')[$composition[CAKEFLOW_TRIGGER_MODEL]['type_document']];
							}
							else {
								$compositionName = 'Etape Pastell';
							}

						}

						if(!$hasCheminement
								&& $listeDocumentSelectionne == Configure::read('Pastell.typesdoc')[Configure::read('Pastell.fluxStudioName')]
								&& $composition[CAKEFLOW_TRIGGER_MODEL]['trigger_id'] == '-3' && empty( $composition[CAKEFLOW_TRIGGER_MODEL]['inforequired_type_document'] )
						) {
							$compositionName = '<i style="color:red;">Etape Pastell : Aucun cheminement défini dans le sous-type sélectionné</i>';
						}
					}
					else {
						$compositionName = '<i style="color:red;">Etape Pastell : Connecteur Pastell en erreur</i>';
					}
				}
				else {
					$compositionName = $composition[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS];
				}
				$ulCompoContent[] = $this->Html->div('desktop_name',$compositionName);
            }
    ?>

    <tr title="<?php echo $descriptionEtapeTitle; ?>"  >
        <td class="etapeInfo <?php echo $specialClassEtapeInserted; ?>" >
            <div class="panel <?php echo $specialClassEtape; ?>" id="comment_<?php echo $visa['Etape']['id']; ?>">
                <div class="panel-heading">
                    <h4 class="panel-title"><?php echo $visa['Visa']['numero_traitement']; ?><span><?php echo $typesEtape[$visa['Visa']['etape_type']]; ?></span></h4>
                </div>
                <div class="panel-body">
                    <legend><?php echo $visa['Visa']['etape_nom']; ?></legend>
                    <ul>
                        <?php foreach ($ulCompoContent as $composition): ?>
                        <li class="composition"><?php echo $composition; ?></li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
        </td>
        <td class="arrow" style="color:<?php echo $arrowDirectionColor; ?>; "><div ><div style="background:<?php echo $arrowDirectionColor; ?>;" id="test_<?php echo $visa['Etape']['id']; ?>"></div><i class="fa fa-long-arrow-down" aria-hidden="true" ></i></div></td>
        <td class="etapeDescription">
            <div class="panel panel-default " id="comment_description_<?php echo $visa['Etape']['id']; ?>" style="border-color:<?php echo $arrowDirectionColor; ?>; ">
                <div class="panel-body">
                    <legend>Description: </legend>
                    <p><?php echo $visa['Etape']['description']; ?></p>
                </div>
            </div>
        </td>
    </tr>
<script>

    var etapid = "<?php echo $visa['Etape']['id']; ?>";
    $(window).resize(function () {
        var heightComment = $("#comment_" + etapid).height();
        set_comment_description_height(etapid, heightComment);
    });
    var heightComment = $("#comment_" + etapid).height();
    set_comment_description_height(etapid, heightComment);

    $('#circuit').css('height', '200px');
</script>
    <?php } ?>
</tbody>
</table>
<?php
    }
}
?>
