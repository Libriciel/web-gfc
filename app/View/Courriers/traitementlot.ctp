<?php

/**
 *
 * Courriers/send_back_to_disp.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Form->create('Courrier');
?>
<div id="traitementlot_page" >
    <legend class="panel-title" id="fluxSelectionnes">Liste des flux sélectionnés</legend>
    <div class="panel-body">
        <div class="list-flux">
                 <?php foreach ($checkItem['treatableItem'] as $item => $values) { ?>
            <input type="hidden" name="data[checkItem][]" value="<?php echo $item; ?>" />
                <?php
                if( $item == 'info' ) {
                    $fields = array(
                        'reference',
                        'name'
                    );
                    $actions = array(
                        'apercu' =>array(
                             "url" => Configure::read('BaseUrl') . '/activites/edit/',
                             "updateElement" => "$('#infos .content')",
                             "formMessage" => false
                        )
                    );
                    $options = array();


                    $panelBodyId ="";
                    if(!empty($options['panel_id'])){
                        $panelBodyId = "id='".$options['panel_id']."'";
                    }

                    $data = $this->Liste->drawTbody($values, 'Courrier', $fields, $actions, $options);
                ?>
            <script>
                $('#fluxSelectionnes').append('<?php echo $this->Liste->drawPanelHeading($values,$options); ?>');
            </script>
            <div  class="bannette_panel panel-body" <?php echo $panelBodyId; ?>>
                <table id="table_flux"
                       data-toggle="table"
                       data-search="true"
                       data-locale = "fr-CA"
                       data-height="399"
                       >
                </table>
            </div>
                <?php
                }
                ?>
        <?php
                }
        ?>
        </div>
        <div class="prevoir-document">
            <div class="content"></div>
            <div class="controls"><a id="close_prevoir_btn" class="btn  btn-info-webgfc btn-inverse pull-right" title="Annuler"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a></div>
        </div>
    </div>
    <div>
        <div class="panel-footer buttonbox controls " role="group" id="flowControlsByLot">
        </div>
    </div>
<?php echo $this->Form->end();
    echo $this->Js->writeBuffer();
?>
    <script>
        $('#table_flux').bootstrapTable({
            data:<?php echo $data;?>,
            columns: [
                {
                    field: "reference",
                    title: "<?php echo __d("courrier","Courrier.reference"); ?>",
                    class: "reference"
                },
                {
                    field: "name",
                    title: "<?php echo __d("courrier","Courrier.name"); ?>",
                    class: "name"
                },
                {
                    field: "apercu",
                    title: "Aperçu",
                    class: "actions voir",
                    align: 'center',
                    width: "80px"
                }
            ]
        }).on('all.bs.table', function (e) {
            $('.fixed-table-container').css('padding-bottom', '40px');
        });
    </script>
<?php echo $this->LIste->drawScript($values, 'Courrier', $fields, $actions, $options); ?>
    <!-- Cas de l'insertion dans un circuit-->
<?php
    if ( $isInCircuit == '0' && !$cpyOnly && !$isDetachable) {
        //le courrier n a pas de circuit / soustype_id associé

		if ($hasSoustype != $nbSelected && $hasTheSameCourrierId != $hasSoustype ) {
			?>
			<script type="text/javascript">
				gui.addbutton({
					element: $('#flowControlsByLot'),
					button: {
						content: '<i class="fa fa-road" aria-hidden="true"></i> ',
						title: '<?php echo __d("courrier", "Insert Courrier"); ?>',
						action: function () {
							swal(
									'Oops...',
									"Insertion dans le circuit impossible.<br>Veuillez choisir un sous-type dans la section qualification.",
									'warning'
									);
						}
					}
				});
				gui.disablebutton({
					element: $('#flowControlsByLot'),
					button: ' '
				})
			</script>
		<?php
        } else { //le sous-type associé au courrier possède-t-il un circuit
            if( $hasCircuit != $nbSelected && $hasTheSameCourrierId != $hasCircuit ) {
				?>
				<script type="text/javascript">
					gui.addbutton({
						element: $('#flowControlsByLot'),
						button: {
							content: '<i class="fa fa-road" aria-hidden="true"></i> ',
							title: ' <?php echo __d("courrier", "Insert Courrier"); ?>',
							action: function () {
								swal(
										'Oops...',
										"Insertion dans le circuit impossible.<br>Aucun circuit défini, veuillez contacter votre administrateur.",
										'warning'
										);
							}
						}
					});
					gui.disablebutton({
						element: $('#flowControlsByLot'),
						button: ' ',
					})
				</script>
			<?php
				}else if( $hasEtapeParapheur == $nbSelected ) {
                	if( $hasDoc != $nbSelected  && $hasTheSameCourrierId != $hasDoc ) {
					?>
					<script type="text/javascript">
						gui.addbutton({
							element: $('#flowControlsByLot'),
							button: {
								content: '<i class="fa fa-road" aria-hidden="true"></i> ',
								title: ' <?php echo __d("courrier", "Insert Courrier"); ?>',
								action: function () {
									swal(
											'Oops...',
											"Insertion dans le circuit impossible.<br>Aucun document défini. L'envoi dans un circuit comportant une étape parapheur nécessite l'association d'un document.",
											'warning'
											);
								}
							}
						});
						gui.disablebutton({
							element: $('#flowControlsByLot'),
							button: ' ',
						})
					</script>
				<?php
                }else {
					?>
					<script type="text/javascript">
						gui.addbutton({
							element: $('#flowControlsByLot'),
							button: {
								content: '<i class="fa fa-road" aria-hidden="true"></i> ',
								title: ' <?php echo __d("courrier", "Insert Courrier"); ?>',
								action: function () {
									var tabChecked = new Array();
									var desktopChecked = new Array();
									$('.selected').each(function () {
										tabChecked.push($(this).find('.checkItem').attr('itemid'));
										desktopChecked.push($(this).find('.checkDesktop').attr('desktopid'));
									});
									$('.checkItem').each(function () {
										if ($(this).prop('checked')) {
											tabChecked.push($(this).attr('itemId'));
											desktopChecked.push($(this).find('.checkDesktop').attr('desktopid'));
										}
									});
									var form = "<form id='formChecked'>";
									for (i in tabChecked) {
										form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
										form += '<input type="hidden" name="data[checkDesktop][]" value="' + desktopChecked[i] + '">';
									}
									form += "</form>";
									if (!$(this).hasClass('ui-state-disabled')) {

										// for( let i =0; i < desktopChecked.length; i++) {
											gui.request({
													url: "/courriers/execTraitementlot/" + desktopChecked[i] + "/insert",
													data: $(form).serialize(),
													loader: true,
													loaderMessage: gui.loaderMessage,
													updateElement: $('#CourrierTraitementlotForm')
												},
												function (data) {
													$('.ui-dialog div').dialog('close');
													location.reload();
												});
										// }
									}
								}
							}
						});</script>
				<?php
                }
            }else {
?>
    <script type="text/javascript">
        //ajout du bouton insérer dans le circuit
        gui.addbutton({
            element: $('#flowControlsByLot'),
            button: {
                content: '<i class="fa fa-road" aria-hidden="true"></i> ',
                title: ' <?php echo __d("courrier", "Insert Courrier"); ?>',
                action: function () {
                    var tabChecked = new Array();
                    var desktopChecked = new Array();
                    $('.selected').each(function () {
                        tabChecked.push($(this).find('.checkItem').attr('itemid'));
                        desktopChecked.push($(this).find('.checkDesktop').attr('desktopid'));
                    });
                    $('.checkItem').each(function () {
                        if ($(this).prop('checked')) {
                            tabChecked.push($(this).attr('itemId'));
                            desktopChecked.push($(this).find('.checkDesktop').attr('desktopid'));
                        }
                    });
                    if (tabChecked.length == 0) {
                        swal({
                            showCloseButton: true,
                            type: 'warning',
                            title: 'Envoi de flux à un aiguilleur ou un autre initiateur',
                            text: 'Veuillez choisir au moins un flux',

                        });
                    } else {
                        var form = "<form id='formChecked'>";
                        for (i in tabChecked) {
                            form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
                            form += '<input type="hidden" name="data[checkDesktop][]" value="' + desktopChecked[i] + '">';
                        }
                        form += "</form>";
                        if (!$(this).hasClass('ui-state-disabled')) {

							// for( let i =0; i < desktopChecked.length; i++) {
								gui.request({
									url: "/courriers/execTraitementlot/" + desktopChecked[i] + "/insert",
									data: $(form).serialize(),
									loader: true,
									loaderMessage: gui.loaderMessage,
									updateElement: $('#CourrierTraitementlotForm')
								},
								function (data) {
									$('.ui-dialog div').dialog('close');
									location.reload();
								});
							// }
                        }
                    }
                }
            }
        });</script>
<?php
            }
        }
    }else {
		if ( ( !$cpyOnly || $isInCircuit && $isUserInCircuit && $desktopCurrentEtapeCircuit && $endCircuitIsCollaborative && $endCircuitIsCollaborativeDerniereUtilisateur ) && !$isDetachable) {
?>
    <script type="text/javascript">
        gui.addbutton({
            element: $('#flowControlsByLot'),
            button: {
				content: "<?php echo  (!$endCircuit || $endCircuitIsCollaborative) ? '<i id=\'validbutton\' class=\'fa fa-check\' ></i> ' : '<i id=\'clorebutton\' class=\'fa fa-check\'></i><i class=\'fa fa-lock\' ></i> '; ?>",
				title: "<?php echo  (!$endCircuit || $endCircuitIsCollaborative) ?__d('courrier', 'Valid Courrier') : __d('courrier', 'Cloture Courrier'); ?>",
                action: function () {
                    var tabChecked = new Array();
                    var desktopChecked = new Array();
                    $('.selected').each(function () {
                        tabChecked.push($(this).find('.checkItem').attr('itemid'));
                        desktopChecked.push($(this).find('.checkDesktop').attr('desktopid'));
                    });
                    $('.checkItem').each(function () {
                        if ($(this).prop('checked')) {
                            tabChecked.push($(this).attr('itemId'));
                            desktopChecked.push($(this).attr('desktopid'));
                        }
                    });
                    var form = "<form id='formChecked'>";
                     for (i in tabChecked) {
                        form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
                        form += '<input type="hidden" name="data[checkDesktop][]" value="' + desktopChecked[i] + '">';
                    }
                    form += "</form>";

					if ($(this)[0].title == 'Clore') {
						if (!$(this).hasClass('ui-state-disabled')) {
							swal({
								showCloseButton: true,
								title: "<?php echo __d('courrier', 'Cloture Courrier'); ?>",
								text: "<?php echo __d('default', 'Etes-vous sûr de vouloir clore ce(s) flux ?'); ?>",
								type: "warning",
								showCancelButton: true,
								confirmButtonColor: "#d33",
								cancelButtonColor: "#3085d6",
								confirmButtonText: "<?php echo __d('default', 'Button.valid'); ?>",
								cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
							}).then(function (data) {
								if (data) {
									if (!$(this).hasClass('ui-state-disabled')) {

										// for( let i =0; i < desktopChecked.length; i++) {
											gui.request({
												url: "/courriers/execTraitementlot/" + desktopChecked[i] + "/valid",
												data: $(form).serialize(),
												loader: true,
												loaderMessage: gui.loaderMessage,
												updateElement: $('#CourrierTraitementlotForm')
											},
											function (data) {
												//                            $('.ui-dialog div').dialog('close');
												window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/userenv/flux/undefined/model:BannetteFlux"; ?>";
											});
										// }
									}
								} else {
									swal({
										showCloseButton: true,
										title: "Annulé!",
										text: "Vous n\'avez pas clos de flux.",
										type: "error",

									});
								}
							});

							$('.swal2-cancel').css('margin-left', '-320px');
							$('.swal2-cancel').css('background-color', 'transparent');
							$('.swal2-cancel').css('color', '#5397a7');
							$('.swal2-cancel').css('border-color', '#3C7582');
							$('.swal2-cancel').css('border', 'solid 1px');

							$('.swal2-cancel').hover(function () {
								$('.swal2-cancel').css('background-color', '#5397a7');
								$('.swal2-cancel').css('color', 'white');
								$('.swal2-cancel').css('border-color', '#5397a7');
							}, function () {
								$('.swal2-cancel').css('background-color', 'transparent');
								$('.swal2-cancel').css('color', '#5397a7');
								$('.swal2-cancel').css('border-color', '#3C7582');
							});
						}
					}
					else if ($(this)[0].title == 'Valider' ) {
						if (!$(this).hasClass('ui-state-disabled')) {
							// for( let i =0; i < desktopChecked.length; i++) {
								gui.request({
									url: "/courriers/execTraitementlot/" + desktopChecked[i] + "/valid",
									data: $(form).serialize(),
									loader: true,
									loaderMessage: gui.loaderMessage,
									updateElement: $('#CourrierTraitementlotForm')
								},
								function (data) {
									//                            $('.ui-dialog div').dialog('close');
									window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/userenv/flux/undefined/model:BannetteFlux"; ?>";
								});
							// }
						}
					}
                }
            }
        });
        </script>
        <?php
        if( !$isCircuitActif ) {
		?>
			<script type="text/javascript">
				gui.disablebutton({
					element: $('#flowControlsByLot'),
					button: " "
				});
				$('#validbutton').click(function () {
					swal({
						showCloseButton: true,
						title: "<?php echo __d('courrier', 'Validation de flux'); ?>",
						text: "<?php echo __d('default', 'Ce(s) flux ne peut(peuvent) être validé(s). <br /> Au moins un des circuits associés au sous-type est désactivé.'); ?>",
						type: "warning"
					});
				});
				$('#clorebutton').click(function () {
					swal({
						showCloseButton: true,
						title: "<?php echo __d('courrier', 'Clôture de flux'); ?>",
						text: "<?php echo __d('default', 'Ce(s) flux ne peut(peuvent) être clos. <br /> Au moins un des circuits associés au sous-type est désactivé.'); ?>",
						type: "warning"
					});
				});
			</script>
		<?php
		}
        ?>
<?php
        }
?>
    <!-- Ajout du bouton refuser -->
    <script type="text/javascript">
        <?php if ( ( !$cpyOnly || ($isInCircuit == '0') && $isUserInCircuit && $desktopCurrentEtapeCircuit) && !$isDetachable ) {  ?>
        gui.addbutton({
            element: $('#flowControlsByLot'),
            button: {
                content: "<i class='fa fa-times'  title='<?php echo __d('courrier', 'Refus Courrier'); ?>' ></i>",
                action: function () {
                    var tabChecked = new Array();
                    $('.selected').each(function () {
                        tabChecked.push($(this).find('.checkItem').attr('itemid'));
                    });
                    $('.checkItem').each(function () {
                        if ($(this).prop('checked')) {
                            tabChecked.push($(this).attr('itemId'));
                        }
                    });
                    var form = "<form id='formChecked'>";
                    for (i in tabChecked) {
                        form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
                    }
                    form += "</form>";
                    if (!$(this).hasClass('ui-state-disabled')) {
                        gui.request({
                            url: "/courriers/refusparlot",
                            data: $(form).serialize(),
                            updateElement: $(this).parents('.modal').find('.modal-body'),
                            loader: true,
                            loaderMessage: gui.loaderMessage
                        });
                    }
                }
            }
        });
        <?php } ?>
    </script>

		<?php if( $isInCircuit == $nbSelected  && $isDetachable != $nbSelected):?>
			<!-- Ajout du bouton envoyer pour les valideurs uniquement -->
			<script type="text/javascript">
				gui.addbutton({
					element: $('#flowControlsByLot'),
					button: {
						content: "<i class='fa fa-paper-plane'  ></i> ",
						title: "<?php echo __d('default', 'Button.send'); ?>",
						action: function () {
							var tabChecked = new Array();
							$('.selected').each(function () {
								tabChecked.push($(this).find('.checkItem').attr('itemid'));
							});
							$('.checkItem').each(function () {
								if ($(this).prop('checked')) {
									tabChecked.push($(this).attr('itemId'));
								}
							});
							if (tabChecked.length == 0) {
								swal({
							showCloseButton: true,
									type: 'error',
									title: 'Envoi de flux à un aiguilleur ou un autre initiateur',
									text: 'Veuillez choisir au moins un flux',

								});
							} else {
								var form = "<form id='formChecked'>";
								for (i in tabChecked) {
									form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
								}
								form += "</form>";
								gui.request({
									url: "/courriers/sendlot",
									data: $(form).serialize(),
									updateElement: $(this).parents('.modal').find('.modal-body'),
									loader: true,
									loaderMessage: gui.loaderMessage
								});
							}
						}
					}
				});
			</script>

		<?php endif;?>
<?php
    }
?>
    <!-- Fin du else, à savoir pas d'insertion possible -->
    <!-- Ajout du bouton envoyer pour copie pour les initiateurs uniquement -->
    <script type="text/javascript">
        gui.addbutton({
            element: $('#flowControlsByLot'),
            button: {
                content: "<i class='fa fa-paper-plane'  ></i><i class='fa fa-files-o'  ></i> ",
                title: "<?php echo __d('default', 'Button.sendcpy'); ?>",
                action: function () {
                    var tabChecked = new Array();
                    $('.selected').each(function () {
                        tabChecked.push($(this).find('.checkItem').attr('itemid'));
                    });
                    $('.checkItem').each(function () {
                        if ($(this).prop('checked')) {
                            tabChecked.push($(this).attr('itemId'));
                        }
                    });
                    if (tabChecked.length == 0) {
                        swal({
                    showCloseButton: true,
                            type: 'error',
                            title: 'Envoi de flux à un aiguilleur ou un autre initiateur',
                            text: 'Veuillez choisir au moins un flux',

                        });
                    } else {
                        var form = "<form id='formChecked'>";
                        for (i in tabChecked) {
                            form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
                        }
                        form += "</form>";
                        gui.request({
                            url: "/courriers/sendcopylot",
                            data: $(form).serialize(),
                            updateElement: $(this).parents('.modal').find('.modal-body'),
                            loader: true,
                            loaderMessage: gui.loaderMessage
                        });
                    }
                }
            }
        });</script>
    <!-- Ajout du bouton supprimer pour les utilisateurs propriétaires du flux -->
    <script type="text/javascript">
    <?php if( $isDeletable != $nbSelected ) :?>
        gui.addbutton({
            element: $('#flowControlsByLot'),
            button: {
                content: '<i class="fa fa-trash" aria-hidden="true"></i> ',
                title: "<?php echo __d('courrier', 'Button.deletecopy'); ?>",
                class: 'btn  btn-danger',
                action: function () {
                    swal(
                            'Oops...',
                            "Suppression des flux impossible.<br>Au moins un des flux sélectionnés n'est pas supprimable.",
                            'warning'
                            );
                }
            }
        });
        gui.disablebutton({
            element: $('#flowControlsByLot'),
            button: " ",
        });
    <?php else:?>
        gui.addbutton({
            element: $('#flowControlsByLot'),
            button: {
				content: '<i class="fa fa-trash" aria-hidden="true"></i> ',
                title: "<?php echo __d('courrier', 'Button.deletecopy'); ?>",
                class: 'btn  btn-danger',
                action: function () {
                    var tabChecked = new Array();
                    $('.selected').each(function () {
                        tabChecked.push($(this).find('.checkItem').attr('itemid'));
                    });
                    $('.checkItem').each(function () {
                        if ($(this).prop('checked')) {
                            tabChecked.push($(this).attr('itemId'));
                        }
                    });
                    var form = "<form id='formChecked'>";
                    for (i in tabChecked) {
                        form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
                    }
                    form += "</form>";
                    swal({
                        showCloseButton: true,
                        title: "<?php echo __d('default', 'Confirmation de suppression'); ?>",
                        text: "<?php echo __d('default', 'Voulez-vous supprimer ces éléments ?'); ?>",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
						confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
                        cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
                    }).then(function (data) {
                        if (data) {
                            $(this).parents(".modal").modal('hide');
                            gui.request({
                                url: "/courriers/deletelot",
                                data: $(form).serialize(),
                                loader: true,
                                loaderMessage: gui.loaderMessage
                            }, function (data) {
                                $('.ui-dialog div').dialog('close');
                                window.location.href = "/environnement/index/0/user";
                            });
                            $(this).parents(".modal").empty();
                        } else {
                            swal({
                                showCloseButton: true,
                                title: "Annulé!",
                                text: "Vous n'avez pas supprimé, ;) .",
                                type: "error",

                            });
                        }
                    });
                    $('.swal2-cancel').css('margin-left', '-320px');
                    $('.swal2-cancel').css('background-color', 'transparent');
                    $('.swal2-cancel').css('color', '#5397a7');
                    $('.swal2-cancel').css('border-color', '#3C7582');
                    $('.swal2-cancel').css('border', 'solid 1px');

                    $('.swal2-cancel').hover(function () {
                        $('.swal2-cancel').css('background-color', '#5397a7');
                        $('.swal2-cancel').css('color', 'white');
                        $('.swal2-cancel').css('border-color', '#5397a7');
                    }, function () {
                        $('.swal2-cancel').css('background-color', 'transparent');
                        $('.swal2-cancel').css('color', '#5397a7');
                        $('.swal2-cancel').css('border-color', '#3C7582');
                    });
                }
            }
        });
        <?php endif;?>
    </script>
    <!-- Ajout du bouton détacherpour les flux en copie -->
    <script type="text/javascript">
		<?php if( $isDetachable != $nbSelected  && $hasTheSameCourrierId != $isDetachable ) :?>
			gui.addbutton({
				element: $('#flowControlsByLot'),
				button: {
					content: "<i class='fa fa-unlock-alt' ></i> ",
					title: "<?php echo __d('courrier', 'Button.detachablelot'); ?>",
					action: function () {
						swal(
								'Oops...',
								"Au moins un des flux n\'est pas détachable.<br>Détachement des flux impossible.",
								'warning'
								);
					}
				}
			});
			gui.disablebutton({
				element: $('#flowControlsByLot'),
				button: " "
			});
		<?php else:?>
			gui.addbutton({
				element: $('#flowControlsByLot'),
				button: {
					content: "<i class='fa fa-unlock-alt' ></i> ",
					title: "<?php echo __d('courrier', 'Button.detachablelot'); ?>",
					action: function () {
						var tabChecked = new Array();
						var desktopChecked = new Array();
						$('.selected').each(function () {
							tabChecked.push($(this).find('.checkItem').attr('itemid'));
							desktopChecked.push($(this).find('.checkDesktop').attr('desktopid'));
						});
						$('.checkItem').each(function () {
							if ($(this).prop('checked')) {
								tabChecked.push($(this).attr('itemId'));
								desktopChecked.push($(this).attr('desktopid'));
							}
						});
						var form = "<form id='formChecked'>";
						for (i in tabChecked) {
							form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
							form += '<input type="hidden" name="data[checkDesktop][]" value="' + desktopChecked[i] + '">';
						}
						form += "</form>";
						swal({
							showCloseButton: true,
							title: "<?php echo __d('default', 'Confirmation de détachement'); ?>",
							text: "<?php echo __d('default', 'Voulez-vous détacher ces éléments de votre bannette ?'); ?>",
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#d33',
							cancelButtonColor: '#3085d6',
							confirmButtonText: "<?php echo __d('default', 'Button.valid'); ?>",
							cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
						}).then(function (data) {
							if (data) {
								gui.request({
									url: "/courriers/detachelot",
									data: $(form).serialize(),
									updateElement: $(this).parents('.modal').find('.modal-body'),
									loader: true,
									loaderMessage: gui.loaderMessage
								}, function (data) {
									if( data === '1' ) {
										location.reload();
										$(this).parents(".modal").modal('hide');
										$(this).parents(".modal").empty();
									}
									//window.location.href = "<?php //echo Configure::read('BaseUrl') . "/environnement/index/0/user"; ?>//";
								});
							} else {
								swal({
									showCloseButton: true,
									title: "Annulé!",
									text: "Vous n'avez pas supprimé, ;) .",
									type: "error",

								});
							}
						});
					}
				}
			});
		<?php endif;?>
        $('#traitementlot_page .prevoir-document').hide();
        $("#close_prevoir_btn").click(function () {
            $('#traitementlot_page .list-flux').removeClass('col-sm-4')
            $('#traitementlot_page .prevoir-document').removeClass('col-sm-8').hide();
        });
    </script>
