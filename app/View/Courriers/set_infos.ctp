<?php

/**
 *
 * Courriers/set_infos.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//echo $this->Html->script('formValidate.js', array('inline' => true));
echo $this->Html->script('contactinfo.js', array('inline' => true));
echo $this->Html->script('setInfos.js', array('inline' => true));
?>
<?php if( Configure::read('Webservice.GRC') && $connecteurGRCActif && !empty( $courriergrcId ) ):?>
<div class="alert alert-info"><a href="https://grc28.localeo.fr/admin/query_view.php?stag_id=1;7&query_id=<?php echo $courriergrcId;?>" target="_blank">Lien vers la requête présente dans la GRC Localeo : N° de requête <?php echo $courriergrcId;?></a></div>
<br />
<?php  endif;?>

<?php if( !empty($motifRefus ) ):?>
    <div class="alert alert-info"><b>Motif du refus : </b><?php echo $motifRefus; ?></div>
<?php  endif;?>

<?php if( $isAdmin && $isClosed ):?>
	<div class="alert alert-warning"><b>ATTENTION: vous intervenez sur un flux clos</div>
<?php  endif;?>

<?php
if (!empty($newCourrierId)) {
    echo $newCourrierId;
} else {
    $direction = array(
        0 => array(
//            'img' => $this->Html->image('/img/folder_outbox.png'),
            'img' => '<i class="fa fa-sign-out" style="color:#e51809;" aria-hidden="true"></i>',
            'txt' => __d('courrier', 'Courrier.courrier_sortant'),
            'contact' => 'dest'
        ),
        1 => array(
//            'img' => $this->Html->image('/img/folder_inbox.png'),
            'img' => '<i class="fa fa-sign-in" style="color:#2ec07e;" aria-hidden="true"></i>',
            'txt' => __d('courrier', 'Courrier.courrier_entrant'),
            'contact' => 'exp'
        ),
        2 => array(
            'img' => '<i class="fa fa-level-up" style="color:#5397a7;" aria-hidden="true"></i>',
            'txt' => __d('courrier', 'Courrier.courrier_interne'),
            'contact' => 'exp'
        )
    );

//    $tabDelaiUnite = array('jour', 'semaine', 'mois');
    $tabDelaiUnite = array(
        0 => 'jour(s)',
        1 => 'semaine(s)',
        2 => 'mois'
    );

    $formOptions = array(
        'type' => 'file',
        'url' => array(
            'controller' => 'courriers',
            'action' => 'setQualification',
            $this->data['Courrier']['id']
        )
    );
?>
<script type="text/javascript">
    var contactLists = <?php echo json_encode($contactOptions); ?>;
</script>

<?php
    $url = array('controller' => 'courriers', 'action' => 'setInfos');
    if (isset($this->data['Courrier']['id'])) {
        $url[] = $this->data['Courrier']['id'];
    }
    $formCourrier = array(
        'name' => 'Courrier',
        'label_w' => 'col-sm-3',
        'input_w' => 'col-sm-5',
        'form_url' => $url,
        'input' => array(
            'Courrier.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formCourrier);

    $priorites = array(
        0 => __d('courrier', 'Courrier.priorite_basse'),
        1 => __d('courrier', 'Courrier.priorite_moyenne'),
        2 => __d('courrier', 'Courrier.priorite_haute')
    );

    $formCourrierFlux = array(
        'name' => 'Courrier',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-8',
        'form_url' => $url,
        'input' => array(
            'Courrier.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            )
        )
    );
    $formCourrierFlux['input']['Courrier.name'] =array(
        'labelText' =>__d('courrier', 'Courrier.name'),
        'inputType' => 'text',
        'items'=>array(
            'required'=>true,
            'type'=>'text'
        )
    );
    $formCourrierFlux['input']['Courrier.origineflux_id'] =array(
        'labelText' =>__d('courrier', 'Courrier.origineflux_id'),
        'inputType' => 'select',
        'items'=>array(
            'type' => 'select',
            'options' => $origineflux,
            'empty' => true
        )
    );
    $formCourrierFlux['input']['Courrier.direction'] =array(
        'labelText' =>__d('courrier', 'Courrier.direction'),
        'inputType' => 'hidden',
        'items'=>array(
            'type'=>'hidden'
        )
    );
    $formCourrierFlux['input']['Courrier.objet'] =array(
        'labelText' =>__d('courrier', 'Courrier.objet'),
        'inputType' => 'textarea',
        'class' => 'textareaspecial',
        'items'=>array(
            'type'=>'textarea',
            'spelecheck'=>'textareaspecial'
        )
    );

	if ($hasOcrData && Configure::read('Ocerisation.active') ) {
		$formCourrierFlux['input']['Document.ocr_data'] = array(
			'labelText' => __d('document', 'Document.ocr_data'),
			'inputType' => 'textarea',
			'class' => 'textareaspecial',
			'items' => array(
				'type' => 'textarea',
				'spelecheck' => 'textareaspecial'
			)
		);
	}

    $formCourrierFlux['input']['Courrier.priorite'] =array(
        'labelText' =>__d('courrier', 'Courrier.priorite'),
        'inputType' => 'select',
        'items'=>array(
            'type' => 'select',
            'options' => $priorites,
            'default' => 0
        )
    );
    if( !Configure::read('Conf.SAERP') ) {
        $formCourrierFlux['input']['Courrier.affairesuiviepar_id'] = array(
            'labelText' => __d('courrier', 'Courrier.affairesuiviepar_id'),
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                'options' => $allDesktops,
                'empty' => true
            )
        );
    }

 ?>
<!--<div class="row">-->

    <?php if( $isInCircuit && !empty($descriptionEtape) ):?>
<div class="alert alert-info"><b>Description de l'étape de traitement en cours : </b><?php echo $descriptionEtape; ?></div>
    <?php  endif;?>
<div class="zone" >
    <legend id="flux" ><i class="fa fa-minus" aria-hidden="true"></i> <?php echo __d('courrier', 'Courrier.fluxFieldset'); ?></legend>
    <div id="fluxdata" class="panel-body form-horizontal">
    <?php echo $this->Formulaire->createForm($formCourrierFlux); ?>
    </div>
</div>

<script type="text/javascript">
    $('#flux').click(function () {
        $('#fluxdata').toggle();
        var btn = $(this).find('i');
        if( btn.hasClass('fa-minus') ) {
            btn.removeClass('fa-minus').addClass('fa-plus');
        }
        else if( btn.hasClass('fa-plus') ) {
            btn.removeClass('fa-plus').addClass('fa-minus');
        }
    });
</script>
<?php

    $listeUnite = Set::enum( $this->data['Soustype']['delai_unite'], $tabDelaiUnite );
    $soustypeDelaiNb = (isset($this->data['Courrier']['delai_nb'])) ? $this->data['Courrier']['delai_nb'] : $this->data['Soustype']['delai_nb'];
    $soustypeDelaiUnite = (isset($this->data['Courrier']['delai_unite'])) ? $this->data['Courrier']['delai_unite'] : $this->data['Soustype']['delai_unite'];

    $soustypeEntrantContent = (isset($this->data['Soustype']['entrant']) ? $direction[$this->data['Soustype']['entrant']]['img'] . " " . $direction[$this->data['Soustype']['entrant']]['txt'] : '');
    $spanSoustypeEntrantContent = $this->Html->tag('span', $soustypeEntrantContent, array('class' => 'form-control fakeDisabledInputText','readonly'=> true));

    $formCourrierQualification = array(
            'name' => 'Courrier',
            'label_w' => 'col-sm-4',
            'input_w' => 'col-sm-8',
            'form_url' => $url,
            'input' => array(
                'Courrier.id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                )
            )
        );


    $formCourrierQualification['input']['Types.Type'] = array(
        'labelText' =>__d('type', 'Type'),
        'inputType' => 'select',
        'items'=>array(
            'type'=>'select',
//            "options" =>array(),
            "options" =>$typeOptions,
            'disabled' => $isInCircuit ? 'disabled' : '',
            'empty' => true,
            'value'=>$this->request->data['Type']['id']
        )
    );

    $formCourrierQualification['input']['Courrier.soustype_id'] = array(
        'labelText' =>__d('soustype', 'Soustype'),
        'inputType' => 'select',
        'items'=>array(
            'type' => 'select',
            "options" =>array(),
            'disabled' => $isInCircuit ? 'disabled' : '',
            'empty' => true,
            'value'=>$this->request->data['Soustype']['id']
        )
    );
    $formCourrierQualification['input']['Courrier.reference'] = array(
        'labelText' =>__d('courrier', 'Courrier.reference'),
        'inputType' => 'text',
        'items'=>array(
            'type'=>'text',
            'readonly' => true,
            'class' => 'fakeDisabledInputText',
            'value' =>(isset($this->data['Courrier']['reference']) ? $this->data['Courrier']['reference'] : '')
        )
    );
    $formCourrierQualification['input']['Courrier.entrant'] = array(
        'labelText' =>__d('courrier', 'Courrier.entrant/sortant'),
        'inputType' => 'text',
        'items'=>array(
            'type' => 'text',
            'readonly' => true
        )
    );
    $formCourrierQualification['input']['Courrier.delai_nb'] = array(
        'labelText' =>'Délai de traitement',
        'inputType' => 'text',
        'items'=>array(
            'type' => 'text',
            'empty' => true,
            'value' => $soustypeDelaiNb
        )
    );
    $formCourrierQualification['input']['Courrier.delai_unite'] = array(
        'labelText' =>false,
        'inputType' => 'select',
        'items'=>array(
            'type' => 'select',
            'options' => $tabDelaiUnite,
            'value' => "{$soustypeDelaiUnite}",
            'empty' => true,
            'class' => 'unitecreate'
        )
    );
?>
<div class="zone" >
    <legend id="qualif"><i class="fa fa-minus" aria-hidden="true"></i> <?php echo __d('courrier', 'Qualification'); ?></legend>
    <div  id="qualifdata" class="panel-body  form-horizontal" >
    <?php echo $this->Formulaire->createForm($formCourrierQualification); ?>
        <?php if( isset($this->data['Soustype']['delai_nb']) && !empty($this->data['Soustype']['delai_nb'])) :?>
            <div class='col-sm-10 defautDelaiInfo'><div class='col-sm-6'></div>(par défaut <?php echo $this->data['Soustype']['delai_nb'].' '.$listeUnite;?>)</div>
            <div class='col-sm-10 updateDelaiInfo'><div class='col-sm-6'></div></div>
        <?php else :?>
            <div class='col-sm-10 defautDelaiInfo'><div class='col-sm-6'></div></div>
        <?php endif;?>

    </div>
</div>

<script type="text/javascript">
    $('#qualif').click(function () {
        $('#qualifdata').toggle();
        var btn = $(this).find('i');
        if( btn.hasClass('fa-minus') ) {
            btn.removeClass('fa-minus').addClass('fa-plus');
        }
        else if( btn.hasClass('fa-plus') ) {
            btn.removeClass('fa-plus').addClass('fa-minus');
        }
    });
</script>
<?php if(Configure::read('Affiche.Cycledevie') ) :?>
<div class="zone">
    <legend id="acteur"><i  class="fa fa-minus" aria-hidden="true"></i> <?php echo __d('courrier', 'Acteurs'); ?></legend>
    <?php
	if(Configure::read('AdminCanDispatchFlux.ToInitiateurOrOther')) {
		if ($isAdmin && !$isInCircuit) {
			$formCourrierAdmin = array(
				'name' => 'Courrier',
				'label_w' => 'col-sm-6',
				'input_w' => 'col-sm-5',
				'input' => array(
					'Bancontenu.desktop_id' => array(
						'inputType' => 'select',
						'labelText' => 'Positionner le flux vers le profil suivant :',
						'items' => array(
							'type' => 'select',
							'options' => $desktopsAdmin,
							'empty' => true
						)
					)
				)
			);
			echo $this->Formulaire->createForm($formCourrierAdmin);
		}
	}
    ?>
    <div id="acteurdata" class="panel-body form-horizontal fieldsetView">
        <table  id="acteursTable" data-toggle="table"  data-locale = "fr-CA" data-show-columns="true">
            <thead>
            <th><i class="fa fa-desktop" style="color:grey;" aria-hidden="true" title="Nom du profil ayant traité le flux"></i></th>
            <th><i class="fa fa-user" style="color:grey;" aria-hidden="true" title="Nom de l'agent traitant le flux"></i></th>
			<th><i class="fa fa-users" style="color:grey;" aria-hidden="true" title="Utilisateur ayant réalisé l'action"></i></th>
            <th><i class="fa fa-id-card-o" style="color:grey;" aria-hidden="true" title="Profil de l'agent ayant traité le flux"></i></th>
            <th><i class="fa fa-calendar-check-o" style="color:grey;" aria-hidden="true" title="Date d'arrivée chez l'agent"></i></th>
            <th><i class="fa fa-calendar-check-o" style="color:grey;" aria-hidden="true" title="Date de dernière modification"></i></th>
            <th><i class="fa fa-calendar-check-o" style="color:grey;" aria-hidden="true" title="Date de validation"></i></th>
            <th><i class="fa fa-files-o" style="color:grey;" aria-hidden="true"  title="Pour copie ?"></i></th>
            </thead>
            <tbody>
            <?php foreach( $bannettes as $key => $bannette ):?>
                <?php
                // Affichage de l'icône pour copie ou non
                    $bannetteName = $bannette['Bannette']['name'];
                    // Utilisateur ayant réalisé l'action
                    $user = @$bannette['Desktop']['User'][0]['nom'].' '.@$bannette['Desktop']['User'][0]['prenom'];
                    if( $user == ' ' ) {
                        $user = @$bannette['Desktop']['SecondaryUser'][0]['nom'].' '.@$bannette['Desktop']['SecondaryUser'][0]['prenom'];
                    }
					if(isset($bannette['Bancontenu']['parapheurname']) && !empty($bannette['Bancontenu']['parapheurname'])) {
						$user = $bannette['Bancontenu']['parapheurname'];
					}

                    $gras = @$bannette['Bancontenu']['gras'];
                    if($gras) {
                        $val = 'bold';
                    }
                    else {
                        $val = 'thin';
                    }
					$lineColored = '';
					if( empty($bannette['Bancontenu']['bannette_id'])) {
						$lineColored = 'Colored';
					}

					$userValideur = $bannette['Bancontenuuser']['nom']. ' '.$bannette['Bancontenuuser']['prenom'];
					if( !empty( $bannette['Desktop']['User'] )) {
						if ($bannette['Desktop']['User'][0]['username'] != $bannette['Bancontenuuser']['username']) {
							$userValideur = $bannette['Bancontenuuser']['nom'] . ' ' . $bannette['Bancontenuuser']['prenom'] . nl2br("\n (par délégation)");
						}
					}

					$rejet ='';
					if( empty($bannette['Bancontenu']['bannette_id'] ) ){
						$rejet = 'Refus du flux par '. $bannette['Desktop']['name'];
					}

					if(isset($bannette['Bancontenu']['pastellname']) && !empty($bannette['Bancontenu']['pastellname'])) {
						$user = $bannette['Bancontenu']['pastellname'];
					}
				?>
				<tr class="trCycle<?php echo $val.$lineColored;?>" >
                    <td class="tdDate" title="<?php echo $rejet;?>"><?php echo $bannette['Desktop']['name'];?></td>
                    <td class="tdDate" title="<?php echo $rejet;?>"><?php echo $user;?></td>
                    <td class="tdDate" title="<?php echo $rejet;?>"><?php echo isset($bannette['Bancontenuuser']['username']) ? $userValideur : null;?></td>
                    <td class="tdDate" title="<?php echo $rejet;?>"><?php echo $bannette['Desktop']['Profil']['name'];?></td>
                    <td class="tdDate" title="<?php echo $rejet;?>"><?php echo strftime( '%d/%m/%Y %T', strtotime( $bannette['Bancontenu']['created'] ) );?></td>
                    <td class="tdDate" title="<?php echo $rejet;?>"><?php echo !empty($bannette['Bancontenu']['modified']) ? strftime( '%d/%m/%Y %T', strtotime( $bannette['Bancontenu']['modified'] ) ) : '';?></td>
                    <td class="tdDate" title="<?php echo $rejet;?>"><?php echo !empty($bannette['Bancontenu']['validated']) ? strftime( '%d/%m/%Y %T', strtotime( $bannette['Bancontenu']['validated'] ) ) : '';?></td>
                    <td class="tdState"><?php echo ($bannetteName == 'copy' ) ? $this->Html->image('/img/valid_16.png', array('alt' => 'Pour copie', 'title' => 'Flux envoyé pour copie' ) ) : null;?></td>
                </tr>
                        <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <div style="margin-left: 15px;"><?php echo '<b>* Les lignes en gras représentent les agents possédant le flux actuellement.</b>';?></div>
</div>

<script type="text/javascript">
    $('#acteur').click(function () {
        $('#acteurdata').toggle();
        var btn = $(this).find('i');
        if( btn.hasClass('fa-minus') ) {
            btn.removeClass('fa-minus').addClass('fa-plus');
        }
        else if( btn.hasClass('fa-plus') ) {
            btn.removeClass('fa-plus').addClass('fa-minus');
        }
    });
</script>
<?php endif;?>

<?php

    $formCourrierContact = array(
        'name' => 'Courrier',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-5',
        'input' => array(
            'Courrier.organisme_id' => array(
                'inputType' => 'select',
                'labelText' =>__d('courrier', 'Courrier.contact'),
                'items'=>array(
                    'type'=>'select',
                    'options' =>$organismOptions,
                    'value' => isset( $this->request->data['Courrier']['organisme_id'] ) ? $this->request->data['Courrier']['organisme_id'] : $sansOrganisme
                )
            ),
            'Courrier.contact_id' => array(
                'inputType' => 'select',
                'labelText' =>__d('courrier', 'Courrier.contact_id'),
                'items'=>array(
                    'type'=>'select',
                    'options' => $contactOptions,
                    'value' => isset( $this->request->data['Courrier']['contact_id'] ) ? $this->request->data['Courrier']['contact_id'] : $sansContact
                )
            )
        )
    );
?>
<div class="zone" >
    <legend id="contact"><i class="fa fa-minus" aria-hidden="true"></i> <?php echo __d('courrier', 'Courrier.contactFieldset'); ?></legend>
    <div id="contactdata" class="panel-body  form-horizontal" >
    <?php echo $this->Formulaire->createForm($formCourrierContact); ?>
    </div>
</div>

<script type="text/javascript">

    <?php if(isset($this->request->data['Courrier']['organisme_id']) && isset($this->request->data['Courrier']['contact_id']) ):?>
        $('#CourrierOrganismeId').val("<?php echo $this->request->data['Courrier']['organisme_id'];?>").change();
        $('#CourrierContactId').val( "<?php echo $this->request->data['Courrier']['contact_id'];?>").change();
    <?php else:?>
        $('#CourrierOrganismeId').val("<?php echo $sansOrganisme;?>").change();
        $('#CourrierContactId').val( "<?php echo $sansContact;?>").change();
    <?php endif;?>

    $('#contact').click(function () {
        $('#contactdata').toggle();
        var btn = $(this).find('i');
        if( btn.hasClass('fa-minus') ) {
            btn.removeClass('fa-minus').addClass('fa-plus');
        }
        else if( btn.hasClass('fa-plus') ) {
            btn.removeClass('fa-plus').addClass('fa-minus');
        }
    });

    var fakeDisabledSelected = function(selector) {
        var original = $(selector),
            hidden = $('<input id="Hidden'+$(original).attr('id')+'" type="hidden" name="'+$(original).attr('name')+'" value="'+$(original).attr('value')+'" />');
        $(original).after(hidden);
        if(selector === '#CourrierOrganismeId' ) {
            $('#s2id_CourrierOrganismeId .select2-arrow').hide();
        }
        if(selector === '#CourrierContactId' ) {
            $('#s2id_CourrierContactId .select2-arrow').hide();
        }
    };

    $(document).ready(function() {
        fakeDisabledSelected('#CourrierOrganismeId');
        fakeDisabledSelected('#CourrierContactId');

		<?php if( $hasOcrData ) :?>
			$('#DocumentOcrData').css('height', '300px');
		<?php endif;?>
    });

</script>
<?php
 $formCourrierDate = array(
            'name' => 'Courrier',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-5',
            'input' => array(
                'Courrier.datereception' => array(
                    'inputType' => 'text',
                    'labelText' =>__d('courrier', 'Courrier.datereception'),
                    'dateInput' =>true,
                    'items'=>array(
                        'type'=>'text',
                        'readonly' => true,
                        'data-format'=>'dd/MM/yyyy'
                    )
                ),
				'Courrier.date' => array(
					'inputType' => 'text',
					'labelText' =>__d('courrier', 'Courrier.date'),
					'items'=>array(
						'type'=>'text',
						'readonly' => true
					)
				)
            )
        );

?>
<div class="zone">
    <legend id="dates"><i  class="fa fa-minus" aria-hidden="true"></i> Date</legend>
    <div id="datesdata" class="panel-body  form-horizontal" >
                <?php echo $this->Formulaire->createForm($formCourrierDate); ?>
    </div>
</div>

<script type="text/javascript">
    $('#dates').click(function () {
        $('#datesdata').toggle();
        var btn = $(this).find('i');
        if( btn.hasClass('fa-minus') ) {
            btn.removeClass('fa-minus').addClass('fa-plus');
        }
        else if( btn.hasClass('fa-plus') ) {
            btn.removeClass('fa-plus').addClass('fa-minus');
        }
    });
</script>
<?php
echo $this->Form->end();
?>
<div class="alert alert-info col-sm-10" id="autoSaveInfo">
</div>

<script type="text/javascript">
    //contruction du tableau de types / soustypes
    $('#TypesType').change(function () {
        var type_id = $(this).val();
        var soustypeLists = <?php echo json_encode($soustypeOptions); ?>;
        $('#CourrierSoustypeId option').remove();

        // On est obligés de passer par un array car on ne peut trier les clés d'un objet
        var options = [];
        for (var value in soustypeLists[type_id]) {
            options.push([value, soustypeLists[type_id][value]]);
        }
        options.sort(function(a, b) {
            if(a[1] < b[1]) return -1;
            else if(a[1] > b[1]) return 1;
            return 0;
        });

        $.each(options, function (index, value) {
            $('#CourrierSoustypeId').append($("<option value='" + value[0] + "'>" + value[1] + "</option>"));
        });
    });
        <?php if(!empty( $typeId ) ) :?>
    $("#TypesType").val("<?php echo $typeId;?>").change();
        <?php endif;?>
        <?php if(!empty( $soustypeid ) ) :?>
    $("#CourrierSoustypeId").val("<?php echo $soustypeid;?>").change();
        <?php endif;?>


    var fieldsetContactLegendExp = "<?php echo __d('courrier', 'Courrier.contactFieldset.exp'); ?>";
    var fieldsetContactLabelCourrierContactNameExp = "<?php echo __d('courrier', 'Courrier.contact.exp'); ?>";
    var fieldsetContactLabelCourrierContactExp = "<?php echo __d('courrier', 'Courrier.contactinfo.exp'); ?>";

    var fieldsetContactLegendDest = "<?php echo __d('courrier', 'Courrier.contactFieldset.dest'); ?>";
    var fieldsetContactLabelCourrierContactNameDest = "<?php echo __d('courrier', 'Courrier.contact.dest'); ?>";
    var fieldsetContactLabelCourrierContactDest = "<?php echo __d('courrier', 'Courrier.contactinfo.dest'); ?>";



    //definition de l action sur stype
    $("#CourrierSoustypeId").change(function () {
        var stype_id = $('option:selected', this).attr('value');
        var reloadImgString = '<?php echo $this->Html->image('/img/reload.png'); ?>';
        var reloadImgFullString = '<?php echo $this->Html->image('/img/reload.png', array('alt' => __d('courrier', 'Courrier.messagechange.short'), 'title' => __d('courrier', 'Courrier.messagechange'))); ?>';
        $('.fakeDisabledInputText').after($('<span></span>').addClass('msgChange').html(reloadImgString + ' ' + "<?php echo __d('courrier', 'Courrier.messagechange'); ?>"));
        $('#delai').after($('<div></div>').addClass('msgChange').html(reloadImgString + ' ' + "<?php echo __d('courrier', 'Courrier.messagechange'); ?>"));
        $('.context_accordion .ui-accordion-header').each(function () {
            if ($('a', this).length > 0) {
                $(this).append($('<span></span>').addClass('msgChange').html(reloadImgFullString));
            }
        });
        if ($('.msgChange').is(':visible')) {
            $('.unitecreate').css('top', '-50px');
        }
    });

    $('#CourrierOrganismeId').attr('disabled', 'disabled');
    $('#CourrierContactId').attr('disabled', 'disabled');


<?php if( $isAdmin && !$isInCircuit):?>
    $('#BancontenuDesktopId').select2({allowClear: true, placeholder: "Sélectionner un nouveau profil"});
<?php endif;?>
    // Actions pour la partie Organisme
    <?php
        $searchOrganismeImg = '<i class="fa fa-search table" id=searchOrganisme name=searchOrganisme title="Rechercher un organisme"></i>';
    ?>
    <?php if( isset( $newCourrierId ) ):?>
        courrierId = <?php echo $newCourrierId;?>;
    <?php endif;?>
    <?php if(isset($this->data['Courrier']['id'])) :?>
        courrierId = <?php echo $this->data['Courrier']['id']; ?>;
    <?php endif;?>
        var url = window.location.href;
        var urlCouper = url.split('/');
        var desktopId = urlCouper[urlCouper.length - 1];
        var searchOrganismeImg = $('<?php echo $searchOrganismeImg; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -30px auto auto', cursor: 'pointer'}).click(function () {
        gui.formMessage({
            url: "/organismes/search_organismes_courriers/",
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#searchOrganisme').parents('fieldset'),
            width: 700,
            top: "-10px",
            buttons: {
               '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    $(this).parents(".modal").modal('hide');
                    $(this).parents(".modal").empty();
                }
            }
        });
    }).appendTo($('#CourrierOrganismeId').parent());

	<?php if( $right_action_carnet_adresse_flux ) :?>

    <?php $addOrganismeImg = '<i class="fa fa-plus-circle table" id=addOrganisme name=addOrganisme title="Ajouter un organisme"></i>';    ?>
    var addOrganismeImg = $('<?php echo $addOrganismeImg; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -50px auto auto', cursor: 'pointer'}).click(function () {
        gui.formMessage({
            url: "/courriers/add_organisme",
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#addOrganisme').parents('fieldset'),
            width: 700,
            top: "-10px",
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $('#CourrierAddOrganismeForm');
                    if (form_validate(form)) {
                        gui.request({
                            url: form.attr('action'),
                            data: form.serialize()
                        }, function (data) {
                            orgInfos = jQuery.parseJSON(data);
                            layer.msg("Les informations ont bien été enregistrées");
                            refreshOrganismes(orgInfos);
                            $('#HiddenCourrierOrganismeId').val(orgInfos['id']);
                            $('#HiddenCourrierContactId').val(orgInfos['contacts']['id']);
                        });
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        });
    }).appendTo($('#CourrierOrganismeId').parent());
    <?php
//    $editOrganismeImg = $this->Html->image('/img/tool.png', array('id' => 'editOrganisme', 'name' => 'editOrganisme', 'title' => __d('organisme', 'Organisme.edit')));
    $editOrganismeImg = '<i class="fa fa-pencil table" id=editOrganisme name=editOrganisme title="Modification"></i>';
    ?>
    var editOrganismeImg = $('<?php echo $editOrganismeImg; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -70px auto auto', cursor: 'pointer'}).click(function () {
        orgId = $('#CourrierOrganismeId').val();
        contactId = $('#CourrierContactId').val();
        <?php if( isset( $newCourrierId ) ):?>
            courrierId = <?php echo $newCourrierId;?>;
        <?php endif;?>
        <?php if(isset($this->data['Courrier']['id'])) :?>
            courrierId = <?php echo $this->data['Courrier']['id']; ?>;
        <?php endif;?>

        if (orgId == "<?php echo $sansOrganisme; ?>" || orgId == "Sans organisme"  || $('#select2-chosen-1').text() == "Sans organisme" ) {
            var title = "<?php echo __d('organisme', 'Organisme.edit') ?>";
            var text = "<?php echo __d('organisme', 'Organisme.edit.forbidden') ?>";
            swal(
                'Oops...',
                title + "<br>" + text,
                'warning'
            );
        }
        else {
            url = "/courriers/edit_organisme/" + orgId + "/" + courrierId;

            gui.formMessage({
                url: url,
                loader: true,
                loaderMessage: gui.loaderMessage,
                updateElement: $('#editOrganisme').parents('fieldset'),
                width: 700,
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        var form = $('#CourrierEditOrganismeForm');
                        if (form_validate(form)) {
                            if( "<?php echo Configure::read('AdresseOrganisme.DiffuseContact');?>" === 'yes' ) {
                                swal({
                                    showCloseButton: true,
                                    title: "Diffusion de l'adresse",
                                    text: "En validant, vous allez remplacer l'adresse de TOUS les contacts liés à cet organisme avec celle nouvellement renseignée.<br /> Etes-vous sûr de vouloir faire cela ?",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#d33",
                                    cancelButtonColor: "#3085d6",
                                    confirmButtonText: '<i class="fa fa-check" aria-hidden="true"></i> Diffuser',
                                    cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Ne pas diffuser',
                                }).then(function (data) {
                                    if (data) {
                                        $('#OrganismeDiffuseContact').val('Oui');
                                    } else {
                                        $('#OrganismeDiffuseContact').val('Non');
                                    }

                                    gui.request({
                                        url: url,
                                        data: form.serialize()
                                    }, function (data) {
                                        orgInfos = jQuery.parseJSON(data);
                                        layer.msg("Les informations ont bien été enregistrées");
                                        $('.organismeInfoCard').remove();
                                        refreshOrganismes(orgInfos);
                                        refreshContactInfos($('#CourrierContactId').val());
                                        $('#HiddenCourrierOrganismeId').val(orgInfos['id']);
                                        $(this).parents('.modal').modal('hide');
                                        $(this).parents('.modal').empty();
                                    });
                                    $('.modal').modal('hide');
                                    $('.modal').empty();
                                });
                            }
                            else {
                                gui.request({
                                    url: url,
                                    data: form.serialize()
                                }, function (data) {
                                    orgInfos = jQuery.parseJSON(data);
                                    layer.msg("Les informations ont bien été enregistrées");
                                    $('.organismeInfoCard').remove();
                                    refreshOrganismes(orgInfos);

                                    $( '#select2-chosen-1' ).html(orgInfos['name']);
                                });
                                $(this).parents('.modal').modal('hide');
                                $(this).parents('.modal').empty();
                            }
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }
            });
        }
    }).appendTo($('#CourrierOrganismeId').parent());

<?php endif;?>
    <?php

        $searchContactImg = '<i class="fa fa-search table" id=searchContact name=searchContact title="Rechercher un contact"></i>';
    ?>
        <?php if( isset( $newCourrierId ) ):?>
            courrierId = <?php echo $newCourrierId;?>;
        <?php endif;?>
        <?php if(isset($this->data['Courrier']['id'])) :?>
            courrierId = <?php echo $this->data['Courrier']['id']; ?>;
        <?php endif;?>
        var url = window.location.href;
        var urlCouper = url.split('/');
        var desktopId = urlCouper[urlCouper.length - 1];
        var searchContactImg = $('<?php echo $searchContactImg; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -30px auto auto', cursor: 'pointer'}).click(function () {

        var orgId = $('#CourrierOrganismeId').val();
        gui.formMessage({
            url: "/contacts/search_contacts_courriers/" + orgId,
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#searchContact').parents('fieldset'),
            width: 700,
            top: "-10px",
            buttons: {
               '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    $(this).parents(".modal").modal('hide');
                    $(this).parents(".modal").empty();
                }
            }
        });
    }).appendTo($('#CourrierContactId').parent());
	<?php if( $right_action_carnet_adresse_flux ) :?>
    <?php
    $addContactImg = '<i class="fa fa-plus-circle table" id=addContact name=addContact title="Ajouter un contact"></i>';
    ?>
    var addContactImg = $('<?php echo $addContactImg; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -50px auto auto', cursor: 'pointer'}).click(function () {
        orgId = $('#CourrierOrganismeId').val();
        if (orgId == '') {
            var title = "<?php echo __d('contact', 'Contact.add') ?>";
            var text = "<?php echo __d('contact', 'Contact.add.undefined') ?>";
            swal(
                    'Oops...',
                    title + "<br>" + text,
                    'warning'
                    );
        }
        $(".modal").empty();
        gui.formMessage({
            url: "/courriers/add_contact/" + orgId,
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#addContact').parents('fieldset'),
            width: 550,
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents(".modal").modal('hide');
                    $(this).parents(".modal").empty();
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $('#CourrierAddContactForm');
                    if (form_validate(form)) {
                        gui.request({
                            url: form.attr('action'),
                            data: form.serialize()
                        }, function (data) {
                            contactInfos = jQuery.parseJSON(data);
                            layer.msg("Les informations ont bien été enregistrées");
                            refreshContact(contactInfos);
                            $('#HiddenCourrierContactId').val(contactInfos['contacts']['id']);
                        });
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }

        });
    }).appendTo($('#CourrierContactId').parent());
// Modification contact
<?php
//    $editContactImg = $this->Html->image('/img/tool.png', array('id' => 'editContact', 'name' => 'editContact', 'title' => __d('contact', 'Contact.edit')));
    $editContactImg = '<i class="fa fa-pencil table" id=editContact name=editContact title="Modifier le contact"></i>';
?>
    var editContactImg = $('<?php echo $editContactImg; ?>').css({height: '16px', color: '#5397a7', width: '16px', float: 'right', margin: '-42px -70px auto auto', cursor: 'pointer'}).click(function () {
        orgId = $('#CourrierOrganismeId').val();
        contactId = $('#CourrierContactId').val();

		if (contactId == "<?php echo $sansContact; ?>" || contactId == "Sans contact" || $('#select2-chosen-2').text() === " Sans contact" || $('#CourrierContactId').text().replace(/\s+/g, '') === "Sanscontact" ) {
            var title = "<?php echo __d('contact', 'Contact.edit') ?>";
            var text = "<?php echo __d('contact', 'Contact.edit.forbidden') ?>";
            swal(
                'Oops...',
                title + "<br>" + text,
                'warning'
                );
        }else {
            url = "/courriers/edit_contact/" + contactId;
            gui.formMessage({
                url: url,
                loader: true,
                loaderMessage: gui.loaderMessage,
                updateElement: $('#editContact').parents('fieldset'),
                width: 600,
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        var form = $('#CourrierEditContactForm');
                        if (form_validate(form)) {
                            gui.request({
                                url: form.attr('action'),
                                data: form.serialize()
                            }, function (data) {
                                contactInfos = jQuery.parseJSON(data);
                                $('#HiddenCourrierContactId').val(contactInfos['contacts']['id']);
                                layer.msg("Les informations ont bien été enregistrées");
                                $('.organismeInfoCard').remove();
                                refreshOrganismeInfos(contactInfos['contacts']['organisme_id']);
                                $("#CourrierOrganismeId").val(contactInfos['contacts']['organisme_id']).change();
                                refreshContact(contactInfos);
                                $( '#select2-chosen-2' ).html(contactInfos['contacts']['name']);
                            });
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        } else {
                            swal({
                                showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }
            });
        }
    }).appendTo($('#CourrierContactId').parent());

	<?php endif;?>

	var jsContactinfo = $.parseJSON('<?php echo json_encode($contactinfos, JSON_HEX_APOS); ?>');
    if (jsContactinfo != '') {
        $('#CourrierContactId').parent().append(createContactinfo(jsContactinfo['Contactinfo']));
    }

    var jsOrganismeinfo = $.parseJSON('<?php echo json_encode($organismeinfos, JSON_HEX_APOS); ?>');
    if (jsOrganismeinfo != '') {
        $('#CourrierOrganismeId').parent().append(createOrganismeinfo(jsOrganismeinfo['Organisme']));
    }



</script>
<!-- on désactive les champs date d'entrée en collectivité + date de création de flux, une fois inséré dans un circuit -->
<?php if($isInCircuit ) :?>
<script type="text/javascript">
    $('.ui-datepicker-trigger').remove();
    $('#dateAltField').addClass('fakeDisabledInputText');
    $('#datereceptionAltField').addClass('fakeDisabledInputText');</script>
<?php endif;?>

<script type="text/javascript">
    /*debut soustype description*/
    <?php
//        $toolTipImg = $this->Html->image('/img/help-contents.png', array('id' => 'tooltipImage','name' => 'tooltipImage' , 'title' => __d('soustype', 'Soustype.information')) );
        $toolTipImg = '<i class="fa fa-info table" id=tooltipImage name=tooltipImage title="Informations"></i>';
    ?>
//    $('<?php /*echo $toolTipImg; */?>').appendTo($('#s2id_CourrierSoustypeId').parent()).css("cursor", "pointer").css("margin", "-42px -20px auto auto").css("float", "right");
//    chargeSoustypeInfo(<?php /*echo $this->request->data['Soustype']['id']; */?>);
    $('<?php echo $toolTipImg; ?>').css({height: '20px', width: '20px', color:'#5397a7'}).appendTo($('#s2id_CourrierSoustypeId').parent()).css("cursor", "pointer").css("margin", "-42px -29px auto auto").css("float", "right").click(function () {
        chargeSoustypeInfo(<?php echo $this->request->data['Soustype']['id']; ?>);
    });

    /*fin soustype description*/

	$("#CourrierDatereception").removeAttr('readonly');
    //ajouter valeur pour entrant/sortant flux
    $('<div class="form-control col-sm-5 fakeDisabledInputText" ><span><?php echo $soustypeEntrantContent; ?></span></div>').insertAfter($('#CourrierEntrant'));
    $('#CourrierEntrant').hide();
    $('#autoSaveInfo').hide();
	<?php if( Configure::read('Use.AutosaveFlux' ) ):?>
		var timeoutId;
		$('#CourrierSetInfosForm input,#CourrierSetInfosForm  select,#CourrierSetInfosForm  textarea').on('input propertychange change', function () {
			clearTimeout(timeoutId);
			timeoutId = setTimeout(function () {
				// Runs 5 second (5000 ms) after the last change
				saveAuto();
			}, <?php echo Configure::read('Saveauto.UpdateTimeout') ;?> );
		});
	<?php endif;?>
    function saveAuto() {
        var fromdisp = <?php echo $fromDisp;?>;
        var hasInitRights = <?php echo $isInit;?>;
        var url = $('#CourrierSetInfosForm').attr('action');
        var soutypeOrigine = '';
        <?php if(!empty($this->request->data['Courrier']['soustype_id'])): ?>
        soutypeOrigine = <?php echo $this->request->data['Courrier']['soustype_id']; ?>;
        <?php endif; ?>

        // Gestion des dates du flux
        $('#CourrierDatereception').change(function () {
            gui.request({
                url: "/courriers/ajaxformvalid/datereception",
                data: $('#CourrierSetInfosForm').serialize()
            }, function (data) {
                processJsonFormCheck(data);
            });
        });

		var dateisover = false;
		var datereception = $('#CourrierDatereception').val();
		var datecreation = $('#CourrierDate').val();
		var daterecu = datereception.split("/");
		var newDatereception = daterecu[1]+"/"+daterecu[0]+"/"+daterecu[2];
		var entree = new Date(newDatereception).getTime();
		var datecree = datecreation.split("/");
		var newDatecree = datecree[1]+"/"+datecree[0]+"/"+datecree[2];
		var cree = new Date(newDatecree).getTime();
		var contactid = $('#CourrierContactId').val();


		var hasPastell = '<?php echo $hasPastell;?>';
		// On checke si le contact possède un mail
		if( hasPastell == 'true') {
			if( contactid != null) {
				gui.request({
					url: "/courriers/ajaxcontact/" + contactid,
					data: $('#CourrierSetInfosForm').serialize()
				}, function (data) {
					if (data) {
						getFlowControls();
					}
				});
			}
		}

		if(entree > cree) {
			dateisover = true;
		}
		if( dateisover ) {
			$('#autoSaveInfo').addClass('alert-danger');
			$('#autoSaveInfo').html('Sauvegarde impossible:<br /> la date d\'entrée ne peut être postérieure à la date de création (' + datecreation + ')');
			$('#autoSaveInfo').show();

			gui.disablebutton({
				element: $('#dataControls, #dataControlsBis'),
				button: "<i class='fa fa-floppy-o' aria-hidden='true'></i> <span class='link-title'><?php echo __d('courrier', 'Enregistrer'); ?></span>"
			});
			$(".saveEditBtn").addClass("disabled").removeAttr("href");
		}
		else{

			$(".saveEditBtn").removeClass("disabled").attr("href", "#");
			$('#autoSaveInfo').removeClass('alert-danger');
			// var getUpdate = false;
			//
			// if ($('#CourrierSoustypeId').val() != soutypeOrigine) {
				getUpdate = true;
			// }
			gui.request({
				url: url,
				data: $('#CourrierSetInfosForm').serialize()
			}, function (data) {
				var update = $.parseJSON(data);
				if (getUpdate) {
					updateQualification(update.newCourrierSoustypeSens, update.newCourrierDelaiNb, update.newCourrierDelaiUnite, update.newCourrierDelaiUniteTranslated);
					updateContextRight(update.newCourrierId);
					if( !fromdisp || hasInitRights ) {
						getFlowControls();
					}
				}
				var d = new Date();
				$('#autoSaveInfo').addClass('alert-info');
				$('#autoSaveInfo').html('Dernier enregistrement : ' + d.toLocaleTimeString());
				$('#autoSaveInfo').show();
				$('.msgChange').remove();
				$('.unitecreate').css('top', '-50px');
			});
		}
    }

    function updateQualification(newCourrierSoustypeSens, newCourrierDelaiNb, newCourrierDelaiUnite, newCourrierDelaiUniteTranslated) {
// console.log(newCourrierSoustypeSens);
		var delaiNbToDisplay = newCourrierDelaiNb;
		var delaiUnitToDisplay = newCourrierDelaiUnite;
		if (newCourrierSoustypeSens == 'Sortant' || newCourrierSoustypeSens == 'Entrant' || newCourrierSoustypeSens == 'Interne') {
			var direction = {
				'Sortant': {
					'img': '<i class="fa fa-sign-out" style="color:#e51809;" aria-hidden="true"></i>',
					'txt': '<?php echo __d('courrier', 'Courrier.courrier_sortant'); ?>',
					'contact': 'dest'
				},
				'Entrant': {
					'img': '<i class="fa fa-sign-in" style="color:#2ec07e;" aria-hidden="true"></i>',
					'txt': '<?php echo __d('courrier', 'Courrier.courrier_entrant'); ?>',
					'contact': 'exp'
				},
				'Interne': {
					'img': '<i class="fa fa-level-up" style="color:#5397a7;" aria-hidden="true"></i>',
					'txt': '<?php echo __d('courrier', 'Courrier.courrier_interne'); ?>',
					'contact': 'exp'
				}
			}
			var newSens = direction[newCourrierSoustypeSens]['img'] + ' ' + direction[newCourrierSoustypeSens]['txt'];
			$('.fakeDisabledInputText').html('<span>' + newSens + '</span>');

			$('#CourrierEntrant').hide();

			$('.defautDelaiInfo').hide();
			if (newCourrierDelaiNb == null) {
				$('.updateDelaiInfo').html('<center> (aucun délai par défaut)  </center>');
			} else {
				$('.updateDelaiInfo').html('<center> (par défaut ' + newCourrierDelaiNb + ' ' + newCourrierDelaiUniteTranslated + ')  </center>');
			}

			<?php if( Configure::read('Display.RatioOrigineflux' ) ):?>
				if( $('#CourrierDelaiNb').val() === '') {
					$('#CourrierDelaiNb').val(delaiNbToDisplay);
					$('#CourrierDelaiUnite').val(delaiUnitToDisplay);

						if( $('#CourrierOriginefluxId').val() != '') {
							gui.request({
								url: "/originesflux/ajaxformRatio/" + $('#CourrierOriginefluxId').val(),
								data: $(this).parents('form').serialize()
							}, function (data) {
								processJsonFormCheck(data);
								newCourrierDelaiNb = delaiNbToDisplay * data;
								$('#CourrierDelaiNb').val(newCourrierDelaiNb);
								$('#CourrierDelaiUnite').val(newCourrierDelaiUnite);
							});
						}
				}
				else {
					if( delaiNbToDisplay != $('#CourrierDelaiNb').val()){
						$('#CourrierDelaiNb').val(delaiNbToDisplay);
						$('#CourrierDelaiUnite').val(delaiUnitToDisplay);
					}
					if( $('#CourrierOriginefluxId').val() != '') {
						gui.request({
							url: "/originesflux/ajaxformRatio/" + $('#CourrierOriginefluxId').val(),
							data: $(this).parents('form').serialize()
						}, function (data) {
							processJsonFormCheck(data);
							newCourrierDelaiNb = $('#CourrierDelaiNb').val() * data;
							$('#CourrierDelaiNb').val(newCourrierDelaiNb);
							$('#CourrierDelaiUnite').val(newCourrierDelaiUnite);
						});
					}
				}
			<?php endif;?>
		}
    }

    function updateContextRight(newCourrierId) {
        var url = window.location.href;
        var urlCouper = url.split('/');
        var desktopId = urlCouper[urlCouper.length - 1];
        gui.request({
            url: '/courriers/updateContext/' + newCourrierId + '/' + desktopId
        }, function (data) {
            $('#flux_context').html(data);
        });

    }

    $('#CourrierName').prop('spellcheck', true);
    $('.trCyclebold').css('font-weight', 'bold') ;
	$('.trCyclethinColored').css('font-style', 'italic') ;
	$('.trCyclethinColored').css('color', '#b11526') ;
	$('.trCycleboldColored').css('font-style', 'italic') ;
	$('.trCycleboldColored').css('color', '#b11526') ;



    // on désactive le curseur pour les input désactivés
    $('.fakeDisabledInputText' ).css('cursor', 'not-allowed');
    $('#CourrierDate' ).css('cursor', 'not-allowed');
    $('#select2-chosen-2').css('cursor', 'not-allowed');
    $('#select2-chosen-1').css('cursor', 'not-allowed');

    $('.organismeInfoCard dd' ).css('margin', '10px 0');
    $('.contactInfoCard dd' ).css('margin', '10px 0');



    $('#organismeInfoCard' + $('#CourrierOrganismeId option:selected').val()).css({'margin-bottom' : '15px'});

	genPriorityImg();
</script>

<?php } ?>
