<?php

/**
 *
 * Courriers/get_affaire.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php

if (isset($viewOnly) && $viewOnly === true) {
    echo $this->Element('viewOnly');
?>
	<script type="text/javascript">
		$('.saveEditBtn').hide();
		$('.undoEditBtn').hide();
	</script>
<?php
}
if (!empty($courrier['Affaire']['id'])) {
?>
<div class="zone">
    <legend>Affaire: </legend>
    <div class="panel-body fieldsetView">
        <dl>
            <dt><?php echo __d('affaire', 'Affaire.name'); ?></dt>
            <dd><?php echo $courrier['Affaire']['name']; ?></dd>
            <dt><?php echo __d('affaire', 'Affaire.comment'); ?></dt>
            <dd><?php echo $courrier['Affaire']['comment']; ?></dd>
        </dl>
    </div>
</div>

<?php
    if (!empty($courrier['Affaire']['Dossier']['id'])) {
?>
<hr/>
<div class="zone">
    <legend><?php echo  __d('affaire', 'Affaire.dossier.assoc'); ?></legend>
    <div class="panel-body fieldsetView">
        <dl>
            <dt><?php echo __d('dossier', 'Dossier.name'); ?></dt>
            <dd><?php echo $courrier['Affaire']['Dossier']['name']; ?></dd>
            <dt><?php echo __d('dossier', 'Dossier.comment'); ?></dt>
            <dd><?php echo $courrier['Affaire']['Dossier']['comment']; ?></dd>
        </dl>
    </div>
</div>
<?php
    } else {
        echo $this->Html->tag('div', __d('affaire', 'Affaire.dossier.void'), array('class' => 'alert alert-warning'));
    }
} else {
    echo $this->Html->tag('div', __d('courrier', 'Courrier.affaire.void'), array('class' => 'alert alert-warning'));
}
?>
