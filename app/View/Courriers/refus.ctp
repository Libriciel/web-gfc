<?php

/**
 *
 * Courriers/refus.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$user_id = $this->Session->read('Auth.User.id');
echo $this->Form->create('Notification',array(
                                        'controller' => 'courriers',
                                        'url' => 'refus',
                                        'class' => 'form-horizontal',
                                        'inputDefaults' => array(
                                            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                                            'div' => array('class' => 'form-group'),
                                            'label' => array('class' => 'control-label  col-sm-3'),
                                            'between' => '<div class="controls  col-sm-5">',
                                            'after' => '<div class="help-block with-errors"></div></div>',
                                            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                                             'data-toggle'=>"validator"
                                        )));
echo $this->Form->hidden('Notification.name', array('value' => 'refus'));
echo $this->Form->label( 'Notification.message', __d('courrier', 'Objet du refus').'*', array( 'class' => 'required' ));
?>
<br />
<br />
<textarea id="NotificationMessage" name="data[Notification][message]" required="required" class="form-control"></textarea>
<?php
echo $this->Form->hidden('Notification.user_id', array('value' => $user_id));
echo $this->Form->hidden('User.id', array('value' => $courrier['Courrier']['user_creator_id']));
echo $this->Form->end();
?>
