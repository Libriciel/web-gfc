<?php

/**
 *
 * Courriers/get_flow_controls.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script type="text/javascript">
    $('#flowControls, #flowControlsBis').empty();
</script>
<?php
//le courrier n est pas refusé
if (!$isRefused && !$isClosed) {
    //le courrier n est pas dans un circuit
	/*if ($isdisp) {
		?>
		<script type="text/javascript">

			gui.addbutton({
				element: $('#flowControls, #flowControlsBis'),
				button: {
					content: "<i class='fa fa-paper-plane'></i>",
					title: '<?php echo __d('courrier', 'Button.sendMultiple'); ?>',
					action: function () {
						gui.formMessage({
							title: '<?php echo __d('courrier', 'Button.sendMultiple'); ?>',
							width: '600px',
							url: "/courriers/dispmultiplesend/<?php echo $courrier['Courrier']['id']; ?>",
							buttons: {
								"<?php echo __d('default', 'Button.cancel'); ?>": function () {
									$(this).parents(".modal").modal('hide');
									$(this).parents(".modal").empty();
								},
								"<?php echo __d('default', 'Button.submit'); ?>": function () {
									var form = $('#CourrierDispmultiplesendForm');
									var url = "/courriers/dispmultiplesend/<?php echo $courrier['Courrier']['id']; ?>";
									gui.request({
										url: "/courriers/dispmultiplesend/<?php echo $courrier['Courrier']['id']; ?>",
										data: form.serialize()
									}, function (data) {
										window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
									});
									$(this).parents(".modal").modal('hide');
									$(this).parents(".modal").empty();
								}
							}
						});
					}
				}
			});
		</script>
		<?php
	}
	else { */




    if (!$isInCircuit && !$cpyOnly) {
        //le courrier n a pas de circuit / soustype_id associé
        if (empty($courrier['Courrier']['soustype_id']) ) {
?>
		<script type="text/javascript">
			gui.addbutton({
				element: $('#flowControls, #flowControlsBis'),
				button: {
					content: '<i class="fa fa-road" aria-hidden="true"></i> ',
					title: ' <?php echo __d("courrier", "Insert Courrier"); ?>',
					action: function () {
						swal(
								'Oops...',
								"Insertion dans le circuit impossible.<br>Veuillez choisir un sous-type dans la section qualification.",
								'warning'
								);
					}
				}
			});
			gui.disablebutton({
				element: $('#flowControls'),
				button: '<i class="fa fa-road" aria-hidden="true"></i> '
			});
			gui.disablebutton({
				element: $('#flowControlsBis'),
				button: '<i class="fa fa-road" aria-hidden="true"></i> '
			});
		</script>
<?php
        }	 else { //le sous-type associé au courrier possède-t-il un circuit
            if(empty($courrier['Soustype']['circuit_id']) && ($courrier['Soustype']['name'] == $SoustypeFantome) ) {
?>
<script type="text/javascript">
    gui.addbutton({
        element: $('#flowControls, #flowControlsBis'),
        button: {
            content: "<i class='fa fa-road'></i> ",
            title: '<?php echo __d('courrier', 'Insert Courrier'); ?>',
            action: function () {
                if (!$(this).hasClass('ui-state-disabled')) {
                    swal({
                    	showCloseButton: true,
                        title: "<?php echo __d('courrier', 'Cloture Courrier'); ?>",
                        text: "<?php echo __d('default', 'ConfirmFantom'); ?>",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#d33",
                        cancelButtonColor: "#3085d6",
                        confirmButtonText: "<?php echo __d('default', 'Button.valid'); ?>",
                        cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
                    }).then(function (data) {
                        if (data) {
                            gui.loader({
                                message: gui.loaderMessage,
                                element: $('#courrier_skel')
                            });

                            var updateElement = $(this);
                            gui.request({
                                url: "/courriers/insert/<?php echo $courrier['Courrier']['id']; ?>"
                            }, function () {
                                //redirection vers l environement
                                window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index"; ?>";
                                //todo problème de aiguilleur
                            });
                        } else {
                            swal({
                    			showCloseButton: true,
                                title: "Annulé!",
                                text: "Vous n\'avez pas supprimé.",
                                type: "error",

                            });
                        }
                    });
                }
				$('.swal2-cancel').css('margin-left', '-320px');
				$('.swal2-cancel').css('background-color', 'transparent');
				$('.swal2-cancel').css('color', '#5397a7');
				$('.swal2-cancel').css('border-color', '#3C7582');
				$('.swal2-cancel').css('border', 'solid 1px');

				$('.swal2-cancel').hover(function () {
					$('.swal2-cancel').css('background-color', '#5397a7');
					$('.swal2-cancel').css('color', 'white');
					$('.swal2-cancel').css('border-color', '#5397a7');
				}, function () {
					$('.swal2-cancel').css('background-color', 'transparent');
					$('.swal2-cancel').css('color', '#5397a7');
					$('.swal2-cancel').css('border-color', '#3C7582');
				});
            }
        }
    });
    gui.disablebutton({
        element: $('#flowControls'),
        button: "<i class='fa fa-road'></i> "
    });
    gui.disablebutton({
        element: $('#flowControlsBis'),
        button: "<i class='fa fa-road'></i> "
    });
</script>
<?php
            } else if (empty($courrier['Soustype']['circuit_id']) ) {
				?>
				<script type="text/javascript">
					gui.addbutton({
						element: $('#flowControls, #flowControlsBis'),
						button: {
							content: '<i class="fa fa-road" aria-hidden="true"></i> ',
							title: ' <?php echo __d("courrier", "Insert Courrier"); ?>',
							action: function () {
								swal(
										'Oops...',
										"Insertion dans le circuit impossible.<br>Aucun circuit trouvé pour le sous-type sélectionné.",
										'warning'
								);
							}
						}
					});
					gui.disablebutton({
						element: $('#flowControls'),
						button: '<i class="fa fa-road" aria-hidden="true"></i> '
					});
					gui.disablebutton({
						element: $('#flowControlsBis'),
						button: '<i class="fa fa-road" aria-hidden="true"></i> '
					});
				</script>
				<?php
			} else if ( !$circuitIsActivated ) {
				?>
				<script type="text/javascript">
					gui.addbutton({
						element: $('#flowControls, #flowControlsBis'),
						button: {
							content: '<i class="fa fa-road" aria-hidden="true"></i> ',
							title: ' <?php echo __d("courrier", "Insert Courrier"); ?>',
							action: function () {
								swal(
										'Oops...',
										"Insertion dans le circuit impossible.<br>Le circuit associé au sous-type sélectionné est désactivé.",
										'warning'
								);
							}
						}
					});
					gui.disablebutton({
						element: $('#flowControls'),
						button: '<i class="fa fa-road" aria-hidden="true"></i> '
					});
					gui.disablebutton({
						element: $('#flowControlsBis'),
						button: '<i class="fa fa-road" aria-hidden="true"></i> '
					});
				</script>
				<?php
			}
            else if( $hasEtapeParapheur ) {
?>
<script type="text/javascript">
    gui.addbutton({
        element: $('#flowControls, #flowControlsBis'),
        button: {
            content: "<i class='fa fa-road'></i>",
            title: '<?php echo __d('courrier', 'Insert Courrier'); ?>',
            action: function () {
                if (!$(this).hasClass('ui-state-disabled')) {

                    gui.loader({
                        message: gui.loaderMessage,
                        element: $('.table-list')
                    });
                    gui.request({
                        url: "/courriers/insert/<?php echo $courrier['Courrier']['id']; ?>"
                    },
                    function () {
                        window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index"; ?>";
                    });
                }
            }
        }
    });
</script>
<?php
            }else {
?>
<script type="text/javascript">
    //ajout du bouton insérer dans le circuit
    gui.addbutton({
        element: $('#flowControls, #flowControlsBis'),
        button: {
            content: "<i class='fa fa-road'></i>",
            title: '<?php echo __d('courrier', 'Insert Courrier'); ?>',
            action: function () {
                if (!$(this).hasClass('ui-state-disabled')) {

                    gui.loader({
                        message: gui.loaderMessage,
                        element: $('.table-list')
                    });
                    gui.request({
                        url: "/courriers/insert/<?php echo $courrier['Courrier']['id']; ?>"
                    },
                    function (data) {
                        window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index"; ?>";
                    });
                }
            }
        }
    });
</script>
<?php
            }
        }
?>


<!-- Ajout du bouton ré-aiguillage pour les initiateurs uniquement -->
<script type="text/javascript">
	gui.addbutton({
		element: $('#flowControls, #flowControlsBis'),
		button: {
			content: "<i class='fa fa-backward'></i>",
			title: 'Ré-aiguillage du flux',
			action: function () {

				var form = "<form id='formChecked'>";
				form += '<input type="hidden" name="data[checkItem][]" value="' + <?php echo $courrier['Courrier']['id'];?> + '">';
				form += "</form>";
				gui.formMessage({
					title: 'Ré-aiguillage du flux',
					url: "/courriers/sendBackToDisp/" + <?php echo $courrier['Courrier']['id'];?>,
					data: $(form).serialize(),
					buttons: {
						'<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
							$(this).parents(".modal").modal('hide');
							$(this).parents(".modal").empty();
						},
						'<i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer': function () {
							var form = $(this).parents('.modal').find('form');
							if( form_validate(form) ) {
								gui.request({
									url: "/courriers/sendBackToDisp/" + <?php echo $courrier['Courrier']['id'];?>,
									data: form.serialize()
								}, function () {
									window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
									layer.msg('Les informations ont été enregistrées', {});
								});
								$(this).parents(".modal").modal('hide');
								$(this).parents(".modal").empty();
							}
						}
					}
				});
			}
		}
	});
</script>

<!-- Ajout du bouton envoyer pour copie pour les initiateurs uniquement -->
<script type="text/javascript">
    gui.addbutton({
        element: $('#flowControls, #flowControlsBis'),
        button: {
            content: "<i class='fa fa-paper-plane'></i>",
            title: '<?php echo __d('default', 'Button.sendcpy'); ?>',
            action: function () {
                gui.formMessage({
                    title: '<?php echo __d('default', 'Button.send'); ?>',
                    width: '600px',
                    url: '/courriers/send/<?php echo $courrier['Courrier']['id'] . '/1' ; ?>',
                    buttons: {
                        "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        },
                        "<?php echo __d('default', 'Button.submit'); ?>": function () {
                            var form = $(this).parents('.modal').find('form');
                            var url = '/courriers/send';
							if( form_validate( form ) ) {
								gui.request({
									url: '/courriers/send/<?php echo $courrier['Courrier']['id'] . ($cpyOnly ? '/1' : ''); ?>',
									data: form.serialize()
								}, function (data) {
									if (data.length > 4) { //FIXME avant avec 0 cela marchait
										getJsonResponse(data);
									} else {
										window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
									}
								});
								$(this).parents(".modal").modal('hide');
								$(this).parents(".modal").empty();
							}
                        }
                    }
                });
            }
        }
    });
</script>
<?php
    } else { //le courrier est dans un circuit
//        echo $this->Form->input('Insert.desktop_id', array('id' => 'rebondSelect', 'style' => 'display: none;', 'type' => 'select', 'div' => false, 'label' => 'CI ICIZFK', 'options' => $desktopsToSend));
        if (!$cpyOnly || $isInCircuit && $isUserInCircuit && $desktopCurrentEtapeCircuit && $endCircuitIsCollaborative && $endCircuitIsCollaborativeDerniereUtilisateur) {
?>
<script type="text/javascript">
	var hasEmptyRequiredMeta = '<?php echo $hasEmptyRequiredMeta;?>';
	var hasMailSecWithPastell = '<?php echo $hasMailSecWithPastell;?>';
	var hasCheminement = '<?php echo $hasCheminement;?>';

    gui.addbutton({
        element: $('#flowControls, #flowControlsBis'),
        button: {
            content: "<?php echo  (!$endCircuit || $endCircuitIsCollaborative) ? '<i id=\'validbutton\' class=\'fa fa-check\' ></i>' : '<i class=\'fa fa-check\'></i><i class=\'fa fa-lock\' ></i> '; ?>",
            title: "<?php echo  (!$endCircuit || $endCircuitIsCollaborative) ?__d('courrier', 'Valid Courrier') : __d('courrier', 'Cloture Courrier'); ?>",
            action: function () {

                var url = window.location.href;
                var urlCouper = url.split('/');
                var desktopnewId = urlCouper[urlCouper.length - 1];

				if ($(this)[0].title == 'Clore') {

					if( hasEmptyRequiredMeta == 1  && $('#flowControlsBis > a ').children('.fa-check').parent().hasClass('ui-state-disabled') ) {
						swal({
							showCloseButton: true,
							title: "<?php echo __d('courrier', 'Cloture Courrier'); ?>",
							text: "<?php echo __d('default', 'Ce flux ne peut être clos. <br /> Veuillez renseigner la (les) métadonnée(s) obligatoire(s)'); ?>",
							type: "warning"
						});
					}
					else if( hasMailSecWithPastell == 1 && $('#flowControlsBis > a ').children('.fa-check').parent().hasClass('ui-state-disabled')) {
						swal({
							showCloseButton: true,
							title: "<?php echo __d('courrier', 'Cloture Courrier'); ?>",
							text: "Aucun mail défini pour l'envoi par mail sécurisé. <br />Veuillez associer un email à votre contact.",
							type: "warning"
						});
					}
					else {

						swal({
							showCloseButton: true,
							title: "<?php echo __d('courrier', 'Cloture Courrier'); ?>",
							text: "<?php echo __d('default', 'Etes-vous sûr de vouloir clore ce flux ?'); ?>",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#d33",
							cancelButtonColor: "#3085d6",
							confirmButtonText: "<?php echo __d('default', 'Button.valid'); ?>",
							cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
						}).then(function (data) {
							if (data) {
								gui.loader({
									message: gui.loaderMessage,
									element: $('#courrier_skel')
								});
								var updateElement = $(this);
								gui.request({
											url: "<?php echo '/courriers/valid/' . $courrier['Courrier']['id'];?>" + "/" + desktopnewId,
											updateElement: $('#webgfc_content'),
											loader: true,
											loaderMessage: gui.loaderMessage
										},
										function () {
											//redirection vers l environement
											window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index"; ?>";
										});
							} else {
								swal({
									showCloseButton: true,
									title: "Annulé!",
									text: "Vous n\'avez pas clos ce flux.",
									type: "error",

								});
							}
						});
					}

					$('.swal2-cancel').css('margin-left', '-320px');
					$('.swal2-cancel').css('background-color', 'transparent');
					$('.swal2-cancel').css('color', '#5397a7');
					$('.swal2-cancel').css('border-color', '#3C7582');
					$('.swal2-cancel').css('border', 'solid 1px');

					$('.swal2-cancel').hover(function () {
						$('.swal2-cancel').css('background-color', '#5397a7');
						$('.swal2-cancel').css('color', 'white');
						$('.swal2-cancel').css('border-color', '#5397a7');
					}, function () {
						$('.swal2-cancel').css('background-color', 'transparent');
						$('.swal2-cancel').css('color', '#5397a7');
						$('.swal2-cancel').css('border-color', '#3C7582');
					});
				}
				else {
					<?php if( $hasMailSecWithPastell ) :?>
						$('.fa-check ').click(function () {
							swal({
								showCloseButton: true,
								title: "Oops...",
								text: "Aucun mail défini pour l'envoi par mail sécurisé. <br />Veuillez associer un email à votre contact.",
								type: "error",
							});
							return;
						});
					<?php elseif( !$hasCheminement && !empty($hasPastellActif) && $nextStep[0] == '-3') :?>
						$('.fa-check ').click(function () {
							swal({
								showCloseButton: true,
								title: "Oops...",
								text: "Ce flux ne peut être validé. <br />L'étape Pastell suivante ne possède pas de cheminement.",
								type: "error",
							});
							return;
						});
					<?php elseif( !$fairesuivre && $nextStep[0] == '-3') :?>
						$('.fa-check ').click(function () {
							swal({
								showCloseButton: true,
								title: "Oops...",
								text: "Aucun document associé à ce flux. <br />",
								type: "error",
							});
							return;
						});
					<?php else: ?>

						gui.request({
							url: "<?php echo '/courriers/valid/'.$courrier['Courrier']['id'];?>" + "/" +  desktopnewId,
							updateElement: $('#webgfc_content'),
							loader: true,
							loaderMessage: gui.loaderMessage
						},
						function () {
							//redirection vers l environement
							window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index"; ?>" ;
						});
					<?php endif; ?>
				}
            }
        }
    });
    $('#validbutton').click(function () {
        $('#validbutton').addClass('ui-state-disabled');
    });

	<?php if( !$hasCheminement && !empty($hasPastellActif)  && $nextStep[0] == '-3' ) {?>
	$('#flowControlsBis > a ').children('.fa-check').parent().addClass('ui-state-disabled');
	$('#flowControls > a ').children('.fa-check').parent().addClass('ui-state-disabled');
	$('.fa-check ').click(function () {
		swal({
			showCloseButton: true,
			title: "Oops...",
			text: "Ce flux ne peut être validé. <br />L'étape Pastell suivante ne possède pas de cheminement.",
			type: "error",
		});
		return;
	});
	<?php }?>

	<?php if( $hasMailSecWithPastell) {?>
	$('#flowControlsBis > a ').children('.fa-check').parent().addClass('ui-state-disabled');
	$('#flowControls > a ').children('.fa-check').parent().addClass('ui-state-disabled');
	$('.fa-check ').click(function () {
		swal({
			showCloseButton: true,
			title: "Oops...",
			text: "Aucun mail défini pour l'envoi par mail sécurisé. <br />Veuillez associer un email à votre contact.",
			type: "error",
		});
		return;
	});
	<?php }?>

	<?php if( $hasEmptyRequiredMeta && empty($nextStep) ) {?>
		$('#flowControlsBis > a ').children('.fa-check').parent().addClass('ui-state-disabled');
		$('#flowControls > a ').children('.fa-check').parent().addClass('ui-state-disabled');
		$('.fa-check ').click(function () {
			swal({
				showCloseButton: true,
				title: "Oops...",
				text: "Ce flux ne peut être clos. <br /> Veuillez renseigner la (les) métadonnée(s) obligatoire(s)",
				type: "error",
			});
			return;
		});
	<?php }?>
</script>
<?php
        }
        //ajout du bouton reponse
        if ($reponse && $endCircuit && !$cpyOnly  /*&& $desktopCurrentEtapeCircuit*/ && !$endCircuitIsCollaborative ) {
?>
<script type="text/javascript">
    gui.addbutton({
        element: $('#flowControls, #flowControlsBis'),
        button: {
            content: "<i class='fa fa-check' ></i><i class='fa fa-arrow-left'></i>",
            title: "<?php echo __d('courrier', 'Cloture Reponse Courrier'); ?>",
            action: function () {

                var url = window.location.href;
                var urlCouper = url.split('/');
                var desktopnewId = urlCouper[urlCouper.length - 1];
                if (!$(this).hasClass('ui-state-disabled')) {
                    gui.formMessage({
                        title: "<?php echo __d('courrier', 'Courrier.response_choice_title') ?>",
                        url: "<?php echo '/courriers/reponse/' . $courrier['Courrier']['id']; ?>",
                        buttons: {
                            "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                                $(this).parents(".modal").modal('hide');
                                $(this).parents(".modal").empty();
                            },
                            "<?php echo __d('default', 'Button.submit'); ?>": function () {
                                var form = $(this).parents('.modal').find('form');
                                var insertAuto = 0;
                                if ($('#CourrierReponseInsertAuto').prop("checked")) {
                                    insertAuto = 1;
                                }

								desktopnewId = desktopnewId.replace('#', '');
	// console.log($('#CourrierReponseInsertAuto').prop("checked"));
	console.log(desktopnewId);
	// console.log($('select option:selected', form).val());
	// return false;
                                if (form_validate(form)) {
                                    gui.request({
                                        url: "<?php echo Configure::read('BaseUrl') . '/courriers/valid/' . $courrier['Courrier']['id']; ?>" + "/" + desktopnewId + '/1/' + $('select option:selected', form).val() + "/" + insertAuto
                                    }, function () {
                                        window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index" ?>";
                                    });
                                } else {
                                    swal({
                    					showCloseButton: true,
                                        title: "Oops...",
                                        text: "Veuillez vérifier votre formulaire!",
                                        type: "error",

                                    });

                                }
                            }
                        }
                    });
                }
            }
        }
    });
</script>
<?php
        }
?>
<script type="text/javascript">
    //ajout du bouton refuser
<?php
if (!$cpyOnly || $isInCircuit && $isUserInCircuit && $desktopCurrentEtapeCircuit) {
?>
    gui.addbutton({
        element: $('#flowControls, #flowControlsBis'),
        button: {
            content: "<i class='fa fa-times'></i>",
            title: '<?php echo __d('courrier', 'Refus Courrier'); ?>',
            action: function () {
                if (!$(this).hasClass('ui-state-disabled')) {
                    gui.formMessage({
                        url: "/courriers/refus/<?php echo $courrier['Courrier']['id']; ?>",
                        buttons: {
                            "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                                $(this).parents(".modal").modal('hide');
                                $(this).parents(".modal").empty();
                            },
                            "<?php echo __d('default', 'Button.submit'); ?>": function () {
                                var form = $(this).parents('.modal').find('form');
                                if (form_validate(form) && $('#NotificationMessage').val() != '') {
                                    gui.request({
                                        url: "/courriers/refus/<?php echo $courrier['Courrier']['id']; ?>",
                                        data: $(form).serialize()
                                    }, function (data) {
                                        window.location.href = "/environnement/index";
                                    })
                                }
                                else {
                                    swal(
                                        'Oops...',
                                        "Veuillez renseigner un motif de refus s'il vous plaît.",
                                        'warning'
                                    );
                                }
                            }
                        }
                    });
                }
            }
        }
    });
<?php
}
if ($sendcpy || $sendwkf || $cpyOnly) {
    if ($sendcpy && $sendwkf) {
            $url = '/courriers/send';
    } else if (($sendcpy && !$sendwkf) || $cpyOnly) {
            $url = '/courriers/sendcpy';
    } else if (!$sendcpy && $sendwkf) {
            $url = '/courriers/sendwkf';
    }
?>

    //ajout du bouton envoyer
    gui.addbutton({
        element: $('#flowControls, #flowControlsBis'),
        button: {
            content: "<i class='fa fa-paper-plane'></i>",
            title: '<?php echo __d('default', 'Button.send'); ?>',
            action: function () {

				$('#flowControlsBis > a ').children('.fa-paper-plane').parent().addClass('ui-state-disabled');
				$('#flowControls > a ').children('.fa-paper-plane').parent().addClass('ui-state-disabled');

                gui.formMessage({
                    title: '<?php echo __d('default', 'Button.send'); ?>',
                    width: '600px',
                    url: '/courriers/send/<?php echo $courrier['Courrier']['id'] . ($cpyOnly ? '/1' : ''); ?>',
                    buttons: {
                        "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();

							$('#flowControlsBis > a ').children('.fa-paper-plane').parent().removeClass('ui-state-disabled');
							$('#flowControls > a ').children('.fa-paper-plane').parent().removeClass('ui-state-disabled');
                        },
                        "<?php echo __d('default', 'Button.submit'); ?>": function () {
                            var form = $(this).parents(".modal").find('form');
                            var url = '<?php echo $url; ?>';
                            var desktopCible = $('#SendDesktopId').val();
							var typeDocPastell = $('#SendPastellTypeDocument').val();
							var sousTypeDocPastell = $('#SendPastellInforequiredTypeDocument').val();
							var emailValue = $('#SendEmail').val();
							var isMailSec = '<?php echo $courrier['Soustype']['envoi_mailsec'];?>';
                            <?php if( !empty( $courrier['Document']['id'] ) ):?>
								var doc = '<?php echo $courrier['Document']['id'];?>';
							<?php else :?>
								var doc = '<?php echo $docId;?>';
                            <?php endif;?>
                            //var doc = '<?php //echo $courrier['Document']['id'];?>//';
                            // Si envoi au i-Parapheur
							if (desktopCible === "-1") {

								if (!form_validate(form)) {
									return;
								}

								if ($.isNumeric(doc)) {
									if (form_validate(form)) {
										gui.request({
											url: '/courriers/send/<?php echo $courrier['Courrier']['id'] . ($cpyOnly ? '/1' : ''); ?>',
											data: form.serialize()
										}, function (data) {
											window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
										});
									} else {
										swal(
											'Oops...',
											"Envoi au i-Parapheur impossible.<br>Veuillez sélectionner un sous-type pour l'adresser au i-Parapheur.",
											'warning'
										);
									}
								} else {
									swal(
										'Oops...',
										"Envoi au i-Parapheur impossible.<br>Veuillez attacher un document au flux pour l'adresser au i-Parapheur.",
										'warning'
									);
								}
							} else if (desktopCible === "-3") {

								if (!form_validate(form)) {
									return;
								}

								if (!$.isNumeric(doc) && !isMailSec) {
									swal(
										'Oops...',
										"Envoi à Pastell impossible.<br>Veuillez attacher un document au flux pour l'adresser à Pastell.",
										'warning'
									);
									return;
								}
								var email = false;

								if (typeDocPastell == 'document-a-signer') {
									var text = "<?php echo __d('default', 'Ce flux va être adressé uniquement au Parapheur  <br /> <br />Confirmez-vous cet envoi ?'); ?>";
								} else if (typeDocPastell == 'pdf-generique') {
									var email = true;
									var text = "<?php echo __d('default', 'Ce flux va être adressé au Parapheur <br />et sera envoyé par mail sécurisé à l\'adresse mail sélectionnée.</b> <br /> <br />Confirmez-vous cet envoi ?'); ?>";
								} else {
									var text = "<?php echo __d('default', 'Confirmez-vous cet envoi ?'); ?>";
								}

								if (sousTypeDocPastell == 'undefined' || sousTypeDocPastell == '' && '<?php echo $courrier['Soustype']['envoi_signature'];?>') {
									swal(
										'Oops...',
										"Envoi impossible.<br>Veuillez sélectionner un sous-type pour l'envoi visa-signature.",
										'warning'
									);
									if ($('#flowControlsBis > a ').children('.fa-paper-plane').parent().hasClass('ui-state-disabled')) {
										$('#flowControlsBis > a ').children('.fa-paper-plane').parent().removeClass('ui-state-disabled');
										$('#flowControls > a ').children('.fa-paper-plane').parent().removeClass('ui-state-disabled');
									}
								} else if ($('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>' || $('#SendPastellTypeDocument').val() == 'pdf-generique') {
									if (isMailSec && emailValue == '') {
										swal({
											showCloseButton: true,
											title: "Annulé!",
											text: "Envoi impossible.<br>Veuillez définir une adresse mail pour l'envoi par mail sécurisé.",
											type: "error",

										});

										if ($('#flowControlsBis > a ').children('.fa-paper-plane').parent().hasClass('ui-state-disabled')) {
											$('#flowControlsBis > a ').children('.fa-paper-plane').parent().removeClass('ui-state-disabled');
											$('#flowControls > a ').children('.fa-paper-plane').parent().removeClass('ui-state-disabled');
										}
									}
									else {
										swal({
											showCloseButton: true,
											title: "Information importante",
											text: text,
											type: "info",
											showCancelButton: true,
											confirmButtonColor: "#d33",
											cancelButtonColor: "#3085d6",
											confirmButtonText: "<?php echo __d('default', 'Button.valid'); ?>",
											cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
										}).then(function (data) {
											if (data) {
												gui.loader({
													message: gui.loaderMessage,
													element: $('#webgfc_content')
												});
												var updateElement = $(this);
												if (form_validate(form)) {
													gui.request({
														url: '/courriers/send/<?php echo $courrier['Courrier']['id'] . ($cpyOnly ? '/1' : ''); ?>',
														data: form.serialize()
													}, function (data) {
														window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
													});
												} else {
													swal(
														'Oops...',
														"Envoi impossible.<br>Veuillez définir un mail pour le contact sélectionné.",
														'warning'
													);
												}

											} else {
												swal({
													showCloseButton: true,
													title: "Annulé!",
													text: "Vous n\'avez pas envoyé dans le circuit.",
													type: "error",

												});
												if ($('#flowControlsBis > a ').children('.fa-paper-plane').parent().hasClass('ui-state-disabled')) {
													$('#flowControlsBis > a ').children('.fa-paper-plane').parent().removeClass('ui-state-disabled');
													$('#flowControls > a ').children('.fa-paper-plane').parent().removeClass('ui-state-disabled');
												}
											}
										});
									}
								} else {
									swal({
										showCloseButton: true,
										title: "Information importante",
										text: text,
										type: "info",
										showCancelButton: true,
										confirmButtonColor: "#d33",
										cancelButtonColor: "#3085d6",
										confirmButtonText: "<?php echo __d('default', 'Button.valid'); ?>",
										cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
									}).then(function (data) {
										if (data) {
											gui.loader({
												message: gui.loaderMessage,
												element: $('#webgfc_content')
											});
											var updateElement = $(this);
											if (form_validate(form)) {
												gui.request({
													url: '/courriers/send/<?php echo $courrier['Courrier']['id'] . ($cpyOnly ? '/1' : ''); ?>',
													data: form.serialize()
												}, function (data) {
													window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
												});
											} else {
												swal(
													'Oops...',
													"Envoi impossible.<br>Veuillez définir un mail pour le contact sélectionné.",
													'warning'
												);
											}

										} else {
											swal({
												showCloseButton: true,
												title: "Annulé!",
												text: "Vous n\'avez pas envoyé dans le circuit.",
												type: "error",

											});
										}
									});
								}


								$('.swal2-cancel').css('margin-left', '-320px');
								$('.swal2-cancel').css('background-color', 'transparent');
								$('.swal2-cancel').css('color', '#5397a7');
								$('.swal2-cancel').css('border-color', '#3C7582');
								$('.swal2-cancel').css('border', 'solid 1px');

								$('.swal2-cancel').hover(function () {
									$('.swal2-cancel').css('background-color', '#5397a7');
									$('.swal2-cancel').css('color', 'white');
									$('.swal2-cancel').css('border-color', '#5397a7');
								}, function () {
									$('.swal2-cancel').css('background-color', 'transparent');
									$('.swal2-cancel').css('color', '#5397a7');
									$('.swal2-cancel').css('border-color', '#3C7582');
								});
							} else {
								if( form_validate( form ) ) {
									gui.loader({
										message: gui.loaderMessage,
										element: $('#webgfc_content')
									});
									gui.request({
										url: '/courriers/send/<?php echo $courrier['Courrier']['id'] . ($cpyOnly ? '/1' : ''); ?>',
										data: form.serialize()
									}, function (data) {
										var reload = true;
										var dataSent = form.serializeArray();
										for( var i=0; i<dataSent.length; i++) {
											if( dataSent[i].name === "data[Send][copy]" ) {
												if(dataSent[i].value === "1"){
													reload = false;
												}
											}
										}
										<?php if(Configure::read('ReloadPage.AfterCopy')):?>
											window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
										<?php else :?>
											if( reload ) {
												window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
											}
											else {
												window.location.reload();
											}
										<?php endif;?>
									});
									$(this).parents(".modal").modal('hide');
									$(this).parents(".modal").empty();
								}
							}

                        }
                    }
                });
            }
        }
    });
<?php
}
?>
</script>
<?php
        if ($endCircuit && $isAuthVersGed && $sendGED) {
?>
<script type="text/javascript">
    gui.addbutton({
        element: $('#flowControls, #flowControlsBis'),
        button: {
            content: "<i class='fa fa-share-square-o'></i>",
            title: '<?php echo __d('courrier', 'Verser en GED'); ?>',
            action: function() {
                if (!$(this).hasClass('ui-state-disabled')) {

            //        gui.message({
            //            title: "<?php //echo __d('default', 'Verser en GED'); ?>//",
            //            msg: "<?php //echo __d('default', 'Etes-vous sûr de vouloir verser ce flux en GED ?'); ?>//",
            //            buttons: [{
            //                    text: buttonSubmit,
            //                    class: 'btn btn-info-webgfc',
            //                    click: function () {
            //                        $(this).parents('.modal').modal('hide');
            //                        gui.loader({
            //                            message: gui.loaderMessage,
            //                            element: $('#courrier_skel')
            //                        });
            //                        var updateElement = $(this);
            //                        gui.request({
            //                            url: "/courriers/sendToGed/" + <?php //echo $courrier['Courrier']['id'];?>//,
            //                            updateElement: updateElement
            //                        }, function () {
            //                            updateElement.removeAttr('loaded');
            ////                            affichageTabContext(fluxId);
            //                            window.location.reload();
            //                        });
            //                        $(this).parents('.modal').empty();
            //                    }
            //                }, {
            //                    text: buttonCancel,
            //                    class: 'btn btn-danger',
            //                    click: function () {
            //                        $(this).parents('.modal').modal('hide');
            //                    }
            //                }]
            //        });

					swal({
						showCloseButton: true,
						title: "<?php echo __d('default', 'Verser en GED'); ?>",
						text: "<?php echo __d('default', 'Etes-vous sûr de vouloir verser ce flux en GED ?'); ?>",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#d33",
						cancelButtonColor: "#3085d6",
						confirmButtonText: "<?php echo __d('default', 'Button.valid'); ?>",
						cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
					}).then(function (data) {
						if (data) {
							gui.loader({
								message: gui.loaderMessage,
								element: $('#webgfc_content')
							});
							var updateElement = $(this);
							gui.request({
								url: "/courriers/sendToGed/" + <?php echo $courrier['Courrier']['id'];?>,
								updateElement: updateElement
							}, function () {
								updateElement.removeAttr('loaded');
								window.location.reload();
							});
						} else {
							swal({
								showCloseButton: true,
								title: "Annulé!",
								text: "Vous n\'avez pas versé ce flux en GED.",
								type: "error",

							});
						}
					});
					$('.swal2-cancel').css('margin-left', '-320px');
					$('.swal2-cancel').css('background-color', 'transparent');
					$('.swal2-cancel').css('color', '#5397a7');
					$('.swal2-cancel').css('border-color', '#3C7582');
					$('.swal2-cancel').css('border', 'solid 1px');

					$('.swal2-cancel').hover(function () {
						$('.swal2-cancel').css('background-color', '#5397a7');
						$('.swal2-cancel').css('color', 'white');
						$('.swal2-cancel').css('border-color', '#5397a7');
					}, function () {
						$('.swal2-cancel').css('background-color', 'transparent');
						$('.swal2-cancel').css('color', '#5397a7');
						$('.swal2-cancel').css('border-color', '#3C7582');
					});
                }
            }
        }
    });
</script>

<?php
    }
}

    if ( Configure::read('Webservice.GRC') && $hasGrcActif && $isSelectvaluemetaSendGRC ) {
?>
    <script type="text/javascript">

        gui.addbutton({
            element: $('#flowControls'),
            button: {
                content: "<img width='20px' height='20px' src='/img/bottom_signature.png' ></i>",
                title: '<?php echo __d('courrier', 'Envoi à la GRC'); ?>',
                action: function() {
                    if (!$(this).hasClass('ui-state-disabled')) {

                        swal({
                            showCloseButton: true,
                            title: "<?php echo __d('default', 'Envoyer le flux à la GRC'); ?>",
                            text: "<?php echo __d('default', 'Etes-vous sûr de vouloir envoyer ce flux dans la GRC ?'); ?>",
                            type: "info",
                            showCancelButton: true,
                            confirmButtonColor: "#d33",
                            cancelButtonColor: "#3085d6",
                            confirmButtonText: "<?php echo  __d('default', 'Button.valid') ; ?>",
                            cancelButtonText: "<?php echo  __d('default', 'Button.cancel'); ?>",
                        }).then(function (data) {
                            if (data) {
                                var updateElement = $(this);
                                gui.request({
                                    url: "/courriers/sendToGrc/" + <?php echo $courrier['Courrier']['id'];?>,
                                    updateElement: updateElement
                                }, function () {
                                    updateElement.removeAttr('loaded');
                                    window.location.reload();
                                });
                            } else {
                                swal({
                                showCloseButton: true,
                                    title: "Annulé!",
                                    text: "Vous n\'avez pas envoyé.",
                                    type: "error",

                                });
                            }
                        });

						$('.swal2-cancel').css('margin-left', '-320px');
						$('.swal2-cancel').css('background-color', 'transparent');
						$('.swal2-cancel').css('color', '#5397a7');
						$('.swal2-cancel').css('border-color', '#3C7582');
						$('.swal2-cancel').css('border', 'solid 1px');

						$('.swal2-cancel').hover(function () {
							$('.swal2-cancel').css('background-color', '#5397a7');
							$('.swal2-cancel').css('color', 'white');
							$('.swal2-cancel').css('border-color', '#5397a7');
						}, function () {
							$('.swal2-cancel').css('background-color', 'transparent');
							$('.swal2-cancel').css('color', '#5397a7');
							$('.swal2-cancel').css('border-color', '#3C7582');
						});
                    }
                }
            }
        });

		gui.addbutton({
			element: $('#flowControlsBis'),
			button: {
				content: "<img width='14px' height='14px' src='/img/bottom_signature.png' ></i>",
				title: '<?php echo __d('courrier', 'Envoi à la GRC'); ?>',
				action: function() {
					if (!$(this).hasClass('ui-state-disabled')) {

						swal({
							showCloseButton: true,
							title: "<?php echo __d('default', 'Envoyer le flux à la GRC'); ?>",
							text: "<?php echo __d('default', 'Etes-vous sûr de vouloir envoyer ce flux dans la GRC ?'); ?>",
							type: "info",
							showCancelButton: true,
							confirmButtonColor: "#d33",
							cancelButtonColor: "#3085d6",
							confirmButtonText: "<?php echo  __d('default', 'Button.valid') ; ?>",
							cancelButtonText: "<?php echo  __d('default', 'Button.cancel'); ?>",
						}).then(function (data) {
							if (data) {
								var updateElement = $(this);
								gui.request({
									url: "/courriers/sendToGrc/" + <?php echo $courrier['Courrier']['id'];?>,
									updateElement: updateElement
								}, function () {
									updateElement.removeAttr('loaded');
									window.location.reload();
								});
							} else {
								swal({
									showCloseButton: true,
									title: "Annulé!",
									text: "Vous n\'avez pas envoyé.",
									type: "error",

								});
							}
						});

						$('.swal2-cancel').css('margin-left', '-320px');
						$('.swal2-cancel').css('background-color', 'transparent');
						$('.swal2-cancel').css('color', '#5397a7');
						$('.swal2-cancel').css('border-color', '#3C7582');
						$('.swal2-cancel').css('border', 'solid 1px');

						$('.swal2-cancel').hover(function () {
							$('.swal2-cancel').css('background-color', '#5397a7');
							$('.swal2-cancel').css('color', 'white');
							$('.swal2-cancel').css('border-color', '#5397a7');
						}, function () {
							$('.swal2-cancel').css('background-color', 'transparent');
							$('.swal2-cancel').css('color', '#5397a7');
							$('.swal2-cancel').css('border-color', '#3C7582');
						});
					}
				}
			}
		});
    </script>
<?php
    }
//    }
}

// Si aucun document lié, on ne peut pas faire suivre au parapheur
if(!$fairesuivre && $hasEtapeParapheur) { ?>
<script type="text/javascript">
    gui.disablebutton({
        element: $('#flowControls, #flowControlsBis'),
        button: "<?php echo (!$endCircuit) ? '<i class=\'fa fa-floppy-o\'></i>' : '<i class=\'fa fa-floppy-o\'></i><i class=\'fa fa-lock\'></i> '; ?>",
    });
</script>
<?php } ?>

