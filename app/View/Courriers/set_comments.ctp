<?php

if (!$canAddPrivateComment) {
    $commentVide = $this->Html->tag('div', __d('comment', 'Comment.cantAddPrivateComment'), array('class' => 'alert alert-warning'));
}
if(count($comments['public'])>0  || count($comments['private'])>0 || count($comments['written'])>0 ){
    $panelHead = $this->Html->tag('legend',$this->Html->tag('h4',__d('comment','Comment.tab')),array('id'=>'panelHeader'));
    $panelBody = $this->Html->div('panel-body',$this->GFCComment->drawComments($comments, true));
    $panelDefault = $this->Html->div('panel',$panelHead.$panelBody);
    $panel = $this->Html->div('panel',$panelDefault);
}else{
    $panelBody = $this->Html->div('panel-body',$this->Html->tag('div', __d('comment', 'Comment.empty'), array('class' => 'alert alert-warning')));
    $panelHead = $this->Html->tag('legend',$this->Html->tag('h4',__d('comment','Comment.tab')),array('id'=>'panelHeader'));
    $panelDefault = $this->Html->div('panel ',$panelHead.$panelBody);
    $panel = $this->Html->div('panel',$panelDefault);
}

?>
<div id="setCommentsContents"></div>
<script type="text/javascript">
    var newCourrierId = <?php echo $flux_id; ?>;
    if ($('#commentFooter').length != 0) {
        $('#setCommentsContents').append('<?php echo $panelBody; ?>');
    } else {
        $('#setCommentsContents').append('<?php echo $panel; ?>');
    }
    $('#commentFooter').html('<a id="addComment" title="Ajouter" alt="Ajouter un commentaire" class="btn afficheOne btn-info-webgfc" data-toggle="" data-target="" data-dismiss=""><i class="fa fa-plus" aria-hidden="true"></i> </a>');
    $('#panelHeader h4').append('  <a id="addComment" title="Ajouter" alt="Ajouter un commentaire" class="btn afficheOne btn-info-webgfc" data-toggle="" data-target="" data-dismiss=""><i class="fa fa-plus" aria-hidden="true"></i> </a>');
    $('#addComment').button().click(function () {
        var url = "<?php echo Router::url(array('controller' => 'comments', 'action' => 'add', $flux_id, $canAddPrivateComment)); ?>";
        $(".modal").empty();
        gui.formMessage({
            url: url,
            title: "<?php echo __d('comment', 'Comment.add'); ?>",
            loader: true,
            width: 470,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#flux_infos'),
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents(".modal").modal('hide');
                    $(this).parents(".modal").empty();
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $(this).parents(".modal").find('form');
                    if (form_validate(form)) {
                        gui.request({
                            url: url,
                            data: form.serialize()
                        }, function (data) {
                            getJsonResponse(data);
                            if (typeof affichageEtapes !== 'undefined' && $.isFunction(affichageEtapes)) {
                                affichageEtapes(newCourrierId);
                            } else {
                                $("#comments").empty();
                                gui.request({
                                    url: "/courriers/setComments/" + newCourrierId,
                                    updateElement: $('#comments')
                                });
                            }
                        });
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }

        });
    });


    $('.sc-edit-btn').click(function () {
        var id = $(this).attr('idComment');
        var canAdd = "<?php echo $canAddPrivateComment;?>";
        if (canAdd == "") {
            canAdd = false;
        }
        var url = "<?php echo Router::url(array('controller' => 'comments', 'action' => 'edit', $flux_id)); ?>/" + canAdd + "/" + id;
        $(".modal").empty();
        gui.formMessage({
            url: url,
            title: "<?php echo __d('comment', 'Comment.edit'); ?>",
            loader: true,
            width: 470,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#commentaires'),
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents(".modal").modal('hide');
                    $(this).parents(".modal").empty();
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $(this).parents(".modal").find('form');
                    if (form_validate(form)) {
                        gui.request({
                            url: url,
                            data: form.serialize()
                        }, function (data) {
                            getJsonResponse(data);
                            if (typeof affichageEtapes !== 'undefined' && $.isFunction(affichageEtapes)) {
                                affichageEtapes(newCourrierId);
                            } else {
                                $("#comments").empty();
                                gui.request({
                                    url: "/courriers/setComments/" + newCourrierId,
                                    updateElement: $('#comments')
                                });
                            }
                        });
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        });
    });

    $('.sc-delete-btn').click(function () {
        var id = $(this).attr('idComment');
        swal({
                    showCloseButton: true,
            title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
            text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
            cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: "/comments/delete/" + id,
                    updateElement: $('#commentaires'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    getJsonResponse(data);
                    if (typeof affichageEtapes !== 'undefined' && $.isFunction(affichageEtapes)) {
                        affichageEtapes(newCourrierId);
                    } else {
                        $("#comments").empty();
                        gui.request({
                            url: "/courriers/setComments/" + newCourrierId,
                            updateElement: $('#comments')
                        });
                    }
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n'avez pas supprimé, ;) .",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    });

    $('.sc-delete-btn, .sc-edit-btn').button();
</script>
