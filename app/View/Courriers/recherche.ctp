<?php

/**
 *
 * Courriers/recherche.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php // echo $this->Html->css(array('recherche'), null, array('inline' => false)); ?>
<script type="text/javascript">

    //au clic on fait apparaitre le select correspondant
    function affectcritere(id) {

        $('#' + id + 'Crit').show();
        $('h2').hide();

    }
    //lorsqu'on clic sur l'image, on ferme le select correspondant
    function fermer(id) {
        $('#' + id).hide();
        $('#' + id + ' select').val("");
        $('#' + id + ' input').val("");

    }

    $(function () {
        var Object = [
            "Plainte contre le bruit au centre ville",
            "Candidature de Christelle DUBOIS",
            "Candidature de Matthieu PAUL",
            "Demande de subvention pour l'association de Cécile DUJEAN",
            "Candidature de Céline DOE",
            "Candidature de Mélanie STACH",
            "ceci est un nouveau test"
        ];
        var Name = [
            "Plainte",
            "Candidature",
            "Subvention",
            "ceci est un nouveau test"
        ];
        $("#CourrierName").autocomplete({
            source: Name
        });
        $("#CourrierObjet").autocomplete({
            source: Object
        });
    });

</script>


<div id="recherchePanels">
    <h4><?php echo __d('recherche', 'Recherche.choisircriteres'); ?><br /><i>(cliquez sur un critère pour l'ajouter)</i></h4>

    <span onClick="affectcritere(this.id);" id="Name" class="button"><?php echo __d('recherche', 'Recherche.nomflux'); ?></span><br/>
    <span onClick="affectcritere(this.id);" id="Objet" class="button"><?php echo __d('recherche', 'Recherche.objetflux'); ?></span>  <br/>
    <span onClick="affectcritere(this.id);" id="Creator" class="button"><?php echo __d('recherche', 'Recherche.createurflux'); ?></span><br/>
    <span onClick="affectcritere(this.id);" id="Circuit" class="button"><?php echo __d('recherche', 'Recherche.circuitflux'); ?></span><br/>
    <span onClick="affectcritere(this.id);" id="Soustype" class="button"><?php echo __d('recherche', 'Recherche.soustypeflux'); ?></span><br/>
    <span onClick="affectcritere(this.id);" id="Date" class="button"><?php echo __d('recherche', 'Courrier.datecreation'); ?></span><br/>
    <span onClick="affectcritere(this.id);" id="Meta" class="button"><?php echo __d('recherche', 'Courrier.meta'); ?></span><br/>
    <span onClick="affectcritere(this.id);" id="Tache" class="button"><?php echo __d('recherche', 'Courrier.tache'); ?></span><br/>
</div>
<div id="recherchecritere" >
    <h4><?php echo __d('recherche', 'Recherche.critereschoisis'); ?><br/><i>(entrez les valeurs des différents critères : les critères sont independants)</i></h4>
    <h2><?php echo __d('recherche', 'Recherche.void'); ?></h2>
    <?php echo $this->Form->create('Courrier', array('controller' => 'courriers', 'action' => 'resultat')); ?>

    <div id="NameCrit" class="crit">
        <fieldset>
            <legend><?php echo __d('recherche', 'Recherche.nomflux'); ?></legend>
            <table>
                <tr>
                    <td>
                        <?php echo $this->Form->text('Courrier.name', array('label' => false)); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("NameCrit");')); ?>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div  id="ObjetCrit" class="crit">
        <fieldset>
            <legend><?php echo __d('recherche', 'Recherche.objetflux'); ?></legend>
            <table>
                <tr>
                    <td>
                        <?php echo $this->Form->text('Courrier.objet', array('label' => false)); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("ObjetCrit");')); ?>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div id="CreatorCrit" class="crit">
        <fieldset>
            <legend><?php echo __d('recherche', 'Recherche.createurflux'); ?></legend>
            <table>
                <tr>
                    <td>
                        <?php echo $this->Form->select('Courrier.creator', $creator); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("CreatorCrit");')); ?>
                    </td>
                </tr>
            </table>

        </fieldset>
    </div>
    <div id="CircuitCrit" class="crit">
        <fieldset>
            <legend><?php echo __d('circuit', 'Circuit'); ?></legend>
            <table>
                <tr>
                    <td>
                        <?php echo $this->Form->select('Courrier.circuit', $circuits); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("CircuitCrit");')); ?>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div id="SoustypeCrit" class="crit">
        <fieldset>
            <legend><?php echo __d('soustype', 'Soustype'); ?></legend>
            <table>
                <tr>
                    <td>
                        <?php echo $this->Form->select('Courrier.soustype', $soustypes); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("SoustypeCrit");')); ?>
                    </td>
                </tr>
            </table>

        </fieldset>
    </div>
    <div id="DateCrit" class="crit">
        <fieldset>
            <legend><?php echo __d('recherche', 'Courrier.datecreation'); ?></legend>
            <table>
                <tr>
                    <td>
                        <?php echo $this->Form->input('Courrier.datecreation', array('label' => false, 'empty' => true)); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("DateCrit");')); ?>
                    </td>
                </tr>
            </table>

        </fieldset>
    </div>

    <div id="MetaCrit" class="crit">
        <fieldset>
            <legend><?php echo __d('metadonnee', 'Metadonnees'); ?></legend>
            <table>
                <tr>
                    <td>
                        <?php echo $this->Form->select('Courrier.meta', $meta); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("MetaCrit");')); ?>
                    </td>
                </tr>
            </table>

        </fieldset>
    </div>
    <div id="TacheCrit" class="crit">
        <fieldset>
            <legend><?php echo __d('tache', 'Taches'); ?></legend>
            <table>
                <tr>
                    <td>
                        <?php echo $this->Form->select('Courrier.tache', $tache); ?>
                    </td>
                    <td>
                        <?php echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("TacheCrit");')); ?>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <?php echo $this->Form->end(); ?>
</div>


<script type="text/javascript">
    //activation des boutons
    $('.button').button();
    gui.datepicker({
        element: $("#CourrierDatecreation"),
        image: "icons/arreter.png"
    });
    $('#Tabsearch').tabs('load', 1);

    $('#buttonbox').empty();
    gui.addbutton({element: $('#buttonbox'),
        button: {
            content: "Rechercher",
            action: function () {
                $.ajax({
                    type: "POST",
                    url: "<?php echo Configure::read('BaseUrl') . '/courriers/resultat'; ?>",
                    data: $('#CourrierResultatForm').serialize(),
                    success: function (data) {

                        $("#Tabsearch").tabs("select", 1);
                        $('#ui-tabs-2').html(data);
                        $('#CourrierResultatForm').each(function () {
                            this.reset();
                        });
                    }
                });
            }
        }
    });
</script>
<?php ?>
