<?php

/**
 *
 * Courriers/get_insert_end.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */

echo $this->Html->script('contactinfo.js', array('inline' => true));
echo $this->Html->script('contextFunction.js', array('inline' => true));
?>
<script type="text/javascript">

    gui.addbutton({
        element: $('#webgfc_content .panel-footer'),
        button: {
            content: '<i id="insertend" class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo __d('courrier', 'Insert Courrier End'); ?>',
            title: ' <?php echo __d('courrier', 'Insert Courrier End'); ?>',
            class: 'btn-info-webgfc ',
            action: function () {
<?php
				if ($soustypeDefined) {
					if ($circuitIsActivated) {
						if ($canInsert) {
				?>
							gui.request({
								url: "<?php echo Configure::read('BaseUrl'); ?>/courriers/insert/<?php echo $courrier['Courrier']['id']; ?>",
								loaderElement: $('.table-list'),
								loaderMessage: gui.loaderMessage
							}, function () {
								//redirection vers l environement
								window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/user"; ?>";
							});
	<?php
						} else if(empty($courrier['Document']['id'])) {
	?>
										swal({
											showCloseButton: true,
											title: "Oops...",
											text: "Insertion dans le circuit impossible.<br>Un document doit être associé pour envoi au parapheur.",
											type: "warning"
										});
										$("#insertend").addClass('disabled').removeAttr("href");
	<?php
						} else if(!empty($courrier['Document']['id'])) {
	?>
										swal({
											showCloseButton: true,
											title: "Oops...",
											text: "Insertion dans le circuit impossible.<br>Le soustype choisi n'a pas de circuit associé.",
											type: "warcning"
										});
										$("#insertend").addClass('disabled').removeAttr("href");
	<?php
						}
					}
					else if($canInsert) {
						?>
							gui.request({
								url: "<?php echo Configure::read('BaseUrl'); ?>/courriers/insert/<?php echo $courrier['Courrier']['id']; ?>",
								loaderElement: $('.table-list'),
								loaderMessage: gui.loaderMessage
							}, function () {
								//redirection vers l environement
								window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/user"; ?>";
							});
					<?php } else {
		?>
						swal({
							showCloseButton: true,
							title: "Oops...",
							text: "Insertion dans le circuit impossible.<br>Le circuit associé au sous-type sélectionné est désactivé.",
							type: "warning"
						});
						$("#insertend").addClass('disabled').removeAttr("href");
		<?php
					}
				}
				else {
?>
					swal({
						showCloseButton: true,
						title: "Oops...",
						text: "Insertion dans le circuit impossible.<br>Veuillez choisir un sous-type dans la section qualification.",
						type: "warning"
					});
					$("#insertend").addClass('disabled').removeAttr("href");
<?php
				}
?>
                                }
                            }
                        });
<?php
    if (!$canInsert || !$soustypeDefined || !$circuitIsActivated) {
?>
						gui.disablebutton({
							element: $('#webgfc_content .panel-footer'),
							button: '<i id="insertend" class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo __d('courrier', 'Insert Courrier End'); ?>',
						});
						$("#insertend").addClass('disabled').removeAttr("href");
<?php
    }
?>
	$("#insertend").removeClass("disabled").attr("href", "#");
</script>
