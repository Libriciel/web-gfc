<?php

/**
 *
 * Courriers/historiqueGlobal.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->css(array('SimpleComment.comment.css'), null, array('inline' => false));
?>
<!-- Affichage des états des flux-->
<?php
$nbBannettes = count($bannettes);
$etat = $bannettes[$nbBannettes-1]['Bancontenu']['etat'];
foreach($bannettes as $bannette){
	if($bannette['Bancontenu']['etat'] == 2 ){
		$etat = 2;
	}
}
// Gestion de l'affichage des états du flux
$class = 'fa fa-flag-checkered';
$title = __d('courrier', 'Courrier.courrier_valide');
$alt = 'validé';
$style = '';
if ($etat == -1) {
	$class = 'fa fa-exclamation-circle';
	$title = __d('courrier', 'Courrier.courrier_refuse');
	$alt = 'refusé';
	$style = 'color:red;cursor:pointer;';
}  else if ($etat == 1 && $bannettes[$nbBannettes-1]['Bancontenu']['bannette_id'] == 2 ) {
	$class = 'fa fa-exclamation-circle';
	$title = __d('courrier', 'Courrier.courrier_refuse');
	$alt = 'refusé';
	$style = 'color:red;cursor:pointer;';
} else if ($etat == 1 || $etat == 0) {
	$class = 'fa fa-cogs';
	$title = __d('courrier', 'Courrier.courrier_a_traite');
	$alt = 'traitement';
	$style = 'color:#5397a7;cursor:pointer;';
} else if ($etat == 2) {
	$class = 'fa fa-flag-checkered';
	$title = __d('courrier', 'Courrier.courrier_cloture');
	$alt = 'clos';
	$style = 'color:#2ec07e;cursor:pointer;';
} else if ($etat == 3) {
	$class = 'fa fa-cogs';
	$title = __d('courrier', 'Courrier.courrier_a_traite');
	$alt = 'traitement';
	$style = 'color:#5397a7;cursor:pointer;';
}

// Gestion de l'affichage des priorités
$imgPriority = 'priority_unknown.png';
$priority = $courrier['Courrier']['priorite'];
$altPriority = $priority;
$titlePriority = __d('courrier', 'Courrier.priorite_indefinie');
if ($priority == 0) {
	$imgPriority = 'priority_low.png';
	$titlePriority = __d('courrier', 'Courrier.priorite_basse');
} else if ($priority == 1) {
	$imgPriority = 'priority_mid.png';
	$titlePriority = __d('courrier', 'Courrier.priorite_moyenne');
} else if ($priority == 2) {
	$imgPriority = 'priority_high.png';
	$titlePriority = __d('courrier', 'Courrier.priorite_haute');
}

// Gestion de l'affichage des directions
$direction = $courrier['Courrier']['direction'];
$directionDef = array(
		'0' => array(
				'img' => '<i class="fa fa-sign-out" style="color:#e51809;" aria-hidden="true"></i>',
				'title' => __d('courrier', 'Courrier.courrier_sortant')
		),
		'1' => array(
				'img' => '<i class="fa fa-sign-in" style="color:#2ec07e;" aria-hidden="true"></i>',
				'title' => __d('courrier', 'Courrier.courrier_entrant')
		),
		'2' => array(
				'img' => '<i class="fa fa-level-up" style="color:#5397a7;" aria-hidden="true"></i>',
				'title' => __d('courrier', 'Courrier.courrier_interne')
		)
);
?>

<?php
$contactInfos = "";
if( !empty( $courrier['Organisme']['name']) && empty($courrier['Contact']["name"]) ) {
	$contactInfos = $courrier['Organisme']["name"];
}
if( !empty( $courrier['Organisme']['name']) && !empty($courrier['Contact']["name"]) ) {
	$contactInfos = $courrier['Organisme']["name"]. ' / '.$courrier['Contact']["name"];
}
if( empty( $courrier['Organisme']['name']) && !empty($courrier['Contact']["name"]) ) {
	$contactInfos = $courrier['Contact']["name"];
}
asort($civilite);
?>

<div class="container">
	<div class="row">
		<div class="table-list">
			<h3>Flux: <?php echo $courrier['Courrier']['name'].' - Numéro de référence : '.$courrier['Courrier']['reference'];?></h3>
			<div class="row">
				<div  class="infos col-sm-6" style="margin-right: -13px;">
					<div id="flux_infos" class="tabs-left">
						<div class="panel panel-default fieldsetView">
							<?php if( Configure::read('Webservice.GRC') && $connecteurGRCActif && !empty( $courriergrcId ) ):?>
								<div class="alert alert-info"><a href="https://grc28.localeo.fr/admin/query_view.php?stag_id=1;7&query_id=<?php echo $courriergrcId;?>" target="_blank">Lien vers la requête présente dans la GRC Localeo : N° de requête <?php echo $courriergrcId;?></a></div>
							<?php  endif;?>
							<div class="panel-heading">
								<div id="flowControlsBis" class="btn-group col-sm-6" style="float:left;margin-top:-5px;"></div>
								<h4 class="panel-title">Informations
									<span class="action-title btn-group" style="margin-top: -5px;">
                                        <?php if( Configure::read('CD') == 81 ) :?>
											<?php
											$img = $this->Html->tag('i','',array('class'=>'fa fa-print fa-fw ctxdocBttnDownload','aria-hidden'=>true));
											echo $this->Html->link($img, '/courriers/historiqueGlobal/' . $courrier['Courrier']['id'] . "/true", array('escape' => false, 'class' => 'print btn btn-info-webgfc btn-secondary'));
											?>
										<?php else:?>
											<a href="#" title="<?php echo  __d('default', 'Button.sendcpy'); ?>"  alt="<?php echo  __d('default', 'Button.sendcpy'); ?>" class="sendcpy btn btn-info-webgfc btn-secondary"><i class="fa fa-paste" aria-hidden="true"></i></a>
										<?php endif;?>
										<?php /*if( $connecteurgedIsActif ) :?>
											<a href="#" title="<?php echo  __d('default', 'Button.export'); ?>"  alt="<?php echo  __d('default', 'Button.export'); ?>" class="sendtoged btn btn-info-webgfc btn-secondary"><i class="fa fa-share-square-o" aria-hidden="true"></i></a>
										<?php endif;*/?>

										<?php if(isset($displayBecauseUserIsInBancontenu) && $displayBecauseUserIsInBancontenu): ?>
                                        	<a href="#" title="<?php echo  __d('default', 'Button.edit'); ?>"  alt="<?php echo  __d('default', 'Button.edit'); ?>" class="editFlux btn btn-info-webgfc btn-secondary"><i class="fa fa-pencil" aria-hidden="true"></i></a>
										<?php endif;?>
                                        <?php if(isset($deletable) && $deletable): ?>
											<a href="#" title="<?php echo  __d('default', 'Button.delete'); ?>"  alt="<?php echo  __d('default', 'Button.delete'); ?>" class="deleteFlux btn btn-danger btn-secondary"><i class="fa fa-trash" aria-hidden="true"></i></a>
										<?php endif; ?>
                                    </span>
								</h4>
							</div>
							<div class="panel-body">
								<dl class="col-sm-2">
									<dt><?php echo __d('bannette', 'Courrier.priorite');?></dt><dd><?php echo $this->Html->image('/img/' . $imgPriority, array('alt' => $altPriority, 'title' => $titlePriority));?></dd>
									<dt><?php echo __d('bannette', 'Courrier.direction');?></dt><dd><?php echo isset($directionDef[$direction]) ? $directionDef[$direction]['img'] : '';?></dd>
									<!--                                    <dt>--><?php //echo __d('bannette', 'Courrier.etat');?><!--</dt><dd>--><?php //echo $this->Html->image('/img/' . $imgEtat, array('alt' => $altEtat, 'title' => $titleEtat));?><!--</dd>-->
									<dt><?php echo __d('bannette', 'Courrier.etat');?></dt><dd><?php echo $this->Html->tag('i', '', array('class' => $class, 'style' => $style, 'alt' => $alt, 'title' => $title));?></dd>
								</dl>
								<dl class="col-sm-9">
									<dt><?php echo __d('courrier', 'Courrier.reference');?></dt><dd><?php echo $courrier['Courrier']["reference"];?></dd>
									<dt><?php echo __d('courrier', 'Courrier.name');?></dt><dd><?php echo $courrier['Courrier']['name'];?></dd>
									<dt><?php echo __d('courrier', 'Courrier.origineflux_id');?></dt><dd><?php echo $courrier['Origineflux']['name'];?></dd>
									<dt><?php echo __d('courrier', 'Courrier.objet');?></dt>
									<dd><?php echo $courrier['Courrier']["objet"];?></dd>
									<?php if( $hasOcrData && Configure::read('Ocerisation.active') ) :?>
										<dt><?php echo __d('document', 'Document.ocr_data');?></dt><dd><?php echo $this->String->troncateStr($courrier['Document']["ocr_data"], 255);?></dd>
									<?php endif;?>
									<dt>Identité du contact</dt><dd><?php echo $contactInfos;?></dd>
									<dt><?php echo __d('courrier', 'Courrier.affairesuiviepar_id');?></dt><dd><?php echo @$courrier['Affairesuivie']["name"];?></dd>
									<dt><?php echo __d('courrier', 'Courrier.datereception');?></dt><dd><?php echo isset( $courrier['Courrier']['datereception'] ) ? strftime( '%d/%m/%Y', strtotime( $courrier['Courrier']['datereception'] ) ) : '' ;?></dd>
									<dt><?php echo __d('courrier', 'Courrier.date');?></dt><dd><?php echo isset( $courrier['Courrier']['date'] ) ? strftime( '%d/%m/%Y', strtotime( $courrier['Courrier']['date'] ) ) : '' ;?></dd>
									<dt>Type / Sous-type</dt><dd><?php echo $courrier['Type']["name"].' / '.$courrier['Soustype']["name"];?></dd>
									<?php if(Configure::read('Conf.SAERP')) :?>
										<dt>Type de document</dt><dd><?php echo !empty($courrier['Courrier']["typedocument"]) ? $typesdocument[$courrier['Courrier']['typedocument']] : '';?></dd>
										<dt>Marché concerné</dt><dd><?php echo !empty( $courrier['Marche']["name"] ) ? $courrier['Marche']['name']." - ".$courrier['Marche']['titulaire'] : '';?></dd>
									<?php endif;?>
								</dl>
							</div>
						</div>

						<div class="panel panel-default fieldsetView">
							<div class="panel-heading">
								<h4 class="panel-title">Informations de l'organisme</h4>
							</div>
							<div class="panel-body">
								<?php if(!empty($courrier['Organisme']['id']) && $courrier['Organisme']['id'] != Configure::read('Sansorganisme.id')  ):?>
									<dl class="col-sm-5">
										<dt class="strong">Nom de l'organisme</dt>
										<dd class="tdField"><?php echo $courrier['Organisme']['name'];?></dd>

										<dt class="strong"><?php echo __d('organisme', 'Organisme.numvoie');?></dt>
										<dd class="tdField"><?php echo $courrier['Organisme']['numvoie'];?></dd>

										<dt class="strong"><?php echo __d('organisme', 'Organisme.compl');?></dt>
										<dd class="tdField"><?php echo $courrier['Organisme']['compl'];?></dd>

										<dt class="strong"><?php echo __d('organisme', 'Organisme.cp');?></dt>
										<dd class="tdField"><?php echo $courrier['Organisme']['cp'];?></dd>

										<dt class="strong"><?php echo __d('Organisme', 'Organisme.email');?></dt>
										<dd class="tdField"><?php echo $courrier['Organisme']['email'];?></dd>


									</dl>
									<dl class="col-sm-5">
										<dt class="strong"><?php echo 'Adresse complète';?></dt>
										<dd class="tdField"><?php echo $courrier['Organisme']['adressecomplete'];?></dd>


										<dt class="strong"><?php echo __d('organisme', 'Organisme.nomvoie');?></dt>
										<dd class="tdField"><?php echo !empty($courrier['Organisme']['banadresse']) ? $courrier['Organisme']['banadresse'] : $courrier['Organisme']['nomvoie'];?></dd>


										<dt class="strong"><?php echo __d('Organisme', 'Organisme.tel');?></dt>
										<dd class="tdField"><?php echo $courrier['Organisme']['tel'];?></dd>

										<dt class="strong"><?php echo __d('organisme', 'Organisme.ville');?></dt>
										<dd class="tdField"><?php echo $courrier['Organisme']['ville'];?></dd>

										<dt class="strong"><?php echo 'Pays';?></dt>
										<dd class="tdField"><?php echo $courrier['Organisme']['pays'];?></dd>

									</dl>
								<?php else:?>
									<?php echo $this->Html->tag('div', 'Aucun organisme associé au flux', array('class' => 'alert alert-warning'));?>
								<?php endif;?>
							</div>
						</div>
						<div class="panel panel-default fieldsetView">
							<div class="panel-heading">
								<h4 class="panel-title"><?php echo __d('courrier', 'Contactinfo'); ?></h4>
							</div>
							<div class="panel-body">
								<?php if(!empty($courrier['Contact']['id']) && $courrier['Contact']['id'] != Configure::read('Sanscontact.id')  ):?>
									<dl class="col-sm-5">
										<dt class="strong">Nom / Prénom</dt>
										<dd class="tdField"><?php echo ( isset( $civilite[$courrier['Contact']['civilite']] ) ? $civilite[$courrier['Contact']['civilite']] : null  ).' '.$courrier['Contact']['nom'].' '.$courrier['Contact']['prenom'];?></dd>

										<dt class="strong"><?php echo __d('contact', 'Contact.numvoie');?></dt>
										<dd class="tdField"><?php echo $courrier['Contact']['numvoie'];?></dd>

										<dt class="strong"><?php echo __d('contact', 'Contact.nomvoie');?></dt>
										<dd class="tdField"><?php echo !empty($courrier['Contact']['banadresse']) ? $courrier['Contact']['banadresse'] : $courrier['Contact']['nomvoie'];?></dd>

										<dt class="strong"><?php echo __d('contact', 'Contact.compl');?></dt>
										<dd class="tdField"><?php echo $courrier['Contact']['compl'];?></dd>
										<dt class="strong"><?php echo __d('contact', 'Contact.cp');?></dt>
										<dd class="tdField"><?php echo $courrier['Contact']['cp'];?></dd>
										<dt class="strong"><?php echo __d('contact', 'Contact.ville');?></dt>
										<dd class="tdField"><?php echo $courrier['Contact']['ville'];?></dd>
									</dl>
									<dl class="col-sm-5">
										<dt class="strong"><?php echo __d('contact', 'Contact.name');?></dt>
										<dd class="tdField"><?php echo $courrier['Contact']['name'];?></dd>

										<dt class="strong"><?php echo 'Adresse complète';?></dt>
										<dd class="tdField"><?php echo $courrier['Contact']['adressecomplete'];?></dd>

										<dt class="strong"><?php echo 'Pays';?></dt>
										<dd class="tdField"><?php echo $courrier['Contact']['pays'];?></dd>


										<dt class="strong"><?php echo __d('contact', 'Contact.tel');?></dt>
										<dd class="tdField"><?php echo $courrier['Contact']['tel'];?></dd>
										<dt class="strong"><?php echo __d('contact', 'Contact.email');?></dt>
										<dd class="tdField"><?php echo $courrier['Contact']['email'];?></dd>

										<dt class="strong"><?php echo __d('contact', 'Contact.role');?></dt>
										<dd class="tdField"><?php echo $courrier['Contact']['role'];?></dd>

									</dl>
								<?php else:?>
									<?php echo $this->Html->tag('div', __d('addressbook', 'Contactinfo.void'), array('class' => 'alert alert-warning'));?>
								<?php endif;?>
							</div>
						</div>

						<?php if(Configure::read('Affiche.Cycledevie') ) :?>
							<div class="panel panel-default fieldsetView zone">
								<div class="panel-heading">
									<h4 class="panel-title"><?php echo __d('courrier', 'Acteurs'); ?></h4>
								</div>
								<div class="panel-body">
									<table data-toggle="table" data-locale = "fr-CA" data-show-refresh="false" data-show-toggle="false" data-show-columns="true" data-search="false" data-select-item-name="toolbar1" >
										<thead>
										<th><i class="fa fa-desktop"  style="color:grey;" aria-hidden="true" text-align="center" title="Nom du profil ayant traité le flux" ></i></th>
										<th><i class="fa fa-user"  style="color:grey;" aria-hidden="true" title="Nom de l'agent traitant le flux"></i></th>
										<th><i class="fa fa-users" style="color:grey;" aria-hidden="true" title="Utilisateur ayant réalisé l'action"></i></th>
										<th><i class="fa fa-id-card-o"  style="color:grey;" aria-hidden="true" title="Profil de l'agent ayant traité le flux"></i></th>
										<th><i class="fa fa-calendar-check-o"  style="color:grey;" aria-hidden="true" title="Date d'arrivée chez l'agent"></i></th>
										<th><i class="fa fa-calendar-check-o"  style="color:grey;" aria-hidden="true" title="Date de dernière modification"></i></th>
										<th><i class="fa fa-calendar-check-o"  style="color:grey;" aria-hidden="true" title="Date de validation"></i></th>
										<th><i class="fa fa-files-o"  style="color:grey;" aria-hidden="true"  title="Pour copie ?"></i></th>
										</thead>
										<tbody>
										<?php foreach( $bannettes as $key => $bannette ):?>
											<?php
												// Affichage de l'icône pour copie ou non
												$bannetteName = $bannette['Bannette']['name'];
												// Utilisateur ayant réalisé l'action
												$user = @$bannette['Desktop']['User'][0]['nom'].' '.@$bannette['Desktop']['User'][0]['prenom'];
												if( $user == ' ' ) {
													$user = @$bannette['Desktop']['SecondaryUser'][0]['nom'].' '.@$bannette['Desktop']['SecondaryUser'][0]['prenom'];
												}
												if(isset($bannette['Bancontenu']['parapheurname']) && !empty($bannette['Bancontenu']['parapheurname'])) {
													$user = $bannette['Bancontenu']['parapheurname'];
												}
												$gras = @$bannette['Bancontenu']['gras'];
												if($gras) {
													$val = 'bold';
												}
												else {
													$val = 'thin';
												}

												$lineColored = '';
												if( empty($bannette['Bancontenu']['bannette_id'])) {
													$lineColored = 'Colored';
												}

												$userValideur = $bannette['Bancontenuuser']['nom']. ' '.$bannette['Bancontenuuser']['prenom'];
												if( !empty( $bannette['Desktop']['User'] )) {
													if ($bannette['Desktop']['User'][0]['username'] != $bannette['Bancontenuuser']['username']) {
														$userValideur = $bannette['Bancontenuuser']['nom'] . ' ' . $bannette['Bancontenuuser']['prenom'] . nl2br("\n (par délégation)");
													}
												}
												$rejet ='';
												if( empty($bannette['Bancontenu']['bannette_id'] ) ){
													$rejet = 'Refus du flux par '. $bannette['Desktop']['name'];
												}

												if(isset($bannette['Bancontenu']['pastellname']) && !empty($bannette['Bancontenu']['pastellname'])) {
													$user = $bannette['Bancontenu']['pastellname'];
												}
											?>
											<tr class="trCycle<?php echo $val.$lineColored;?>">
												<td class="tdDate" title="<?php echo $rejet;?>"><?php echo $bannette['Desktop']['name'];?></td>
												<td class="tdDate" title="<?php echo $rejet;?>"><?php echo $user;?></td>
												<td class="tdDate" title="<?php echo $rejet;?>"><?php echo isset($bannette['Bancontenuuser']['username']) ? $userValideur : null;?></td>
												<td class="tdDate" title="<?php echo $rejet;?>"><?php echo $bannette['Desktop']['Profil']['name'];?></td>
												<td class="tdDate" title="<?php echo $rejet;?>"><?php echo strftime( '%d/%m/%Y %T', strtotime( $bannette['Bancontenu']['created'] ) );?></td>
												<td class="tdDate" title="<?php echo $rejet;?>"><?php echo !empty($bannette['Bancontenu']['modified']) ? strftime( '%d/%m/%Y %T', strtotime( $bannette['Bancontenu']['modified'] ) ) : '';?></td>
												<td class="tdDate" title="<?php echo $rejet;?>"><?php echo !empty($bannette['Bancontenu']['validated']) ? strftime( '%d/%m/%Y %T', strtotime( $bannette['Bancontenu']['validated'] ) ) : '';?></td>
												<td class="tdState" title="<?php echo $rejet;?>"><?php echo ($bannetteName == 'copy' ) ? $this->Html->image('/img/valid_16.png', array('alt' => 'Pour copie', 'title' => 'Flux envoyé pour copie' ) ) : null;?></td>
											</tr>
										<?php endforeach;?>
										</tbody>
									</table>
								</div>
								<div style="margin-left: 15px;"><?php echo '<b>* Les lignes en gras représentent les agents possédant le flux actuellement.</b>';?></div>
							</div>
							<script type="text/javascript">
								$('.trCyclethinColored').css('margin', '100px;') ;
							</script>
						<?php endif;?>


						<!-- Liste des métadonnées déposées sur le flux -->
						<?php
						$divContent = $this->Html->tag('div', __d('metadonnee', 'Metadonnee.void'), array('class' => 'alert alert-warning'));

						$valOfSelect =  Configure::read('Selectvaluemetadonnee.id');
						if (!empty($metas)) {
							$dlContent = '';
							foreach ($metas as $meta) {
//            if ($meta['Metadonnee']['typemetadonnee_id'] != 3 || ($meta['Metadonnee']['typemetadonnee_id'] == 3 && $meta['CourrierMetadonnee']['valeur'] != 'Non')) {
								$dlContent .= $this->Html->tag('dt', $meta['Metadonnee']['name']);
								if ($meta['Metadonnee']['typemetadonnee_id'] == 3) {
									$dlContent .= $this->Html->tag('dd', ($meta['CourrierMetadonnee']['valeur'] != '') ? $meta['CourrierMetadonnee']['valeur'] : 'Aucune information');
								} else if ($meta['Metadonnee']['typemetadonnee_id'] == $valOfSelect) {
									$dlContent .= $this->Html->tag('dd', (isset( $meta['Selectvaluemetadonnee'][$meta['CourrierMetadonnee']['valeur']] ) ) ? $meta['Selectvaluemetadonnee'][$meta['CourrierMetadonnee']['valeur']] : 'Aucune information');
								} else if ($meta['Metadonnee']['typemetadonnee_id'] == 1) {
									$dlContent .= $this->Html->tag('dd', (isset( $meta['CourrierMetadonnee']['valeur'] ) ) ? $meta['CourrierMetadonnee']['valeur'] : 'Aucune information');
								} else {
									$dlContent .= $this->Html->tag('dd', ($meta['CourrierMetadonnee']['valeur'] != '') ? $meta['CourrierMetadonnee']['valeur'] : 'Aucune information');
								}
//            }else{
//                $dlContent .= $this->Html->tag('dt', $meta['Metadonnee']['name']);
//                if ($meta['Metadonnee']['typemetadonnee_id'] == 3 ) {
//                    $dlContent .= $this->Html->tag('dd', ($meta['CourrierMetadonnee']['valeur'] != '') ? $meta['CourrierMetadonnee']['valeur'] : 'Aucune information');
//                }
//            }
							}
							$divContent = $this->Html->tag('dl', $dlContent);
						}
						?>

						<div class="panel panel-default fieldsetView">
							<div class="panel-heading">
								<h4 class="panel-title"><?php echo __d('courrier', 'Metadonnees'); ?></h4>
							</div>
							<div class="panel-body">
								<?php echo $divContent; ?>
							</div>
						</div>


						<!-- Liste des comentaires (privés / publics) émis sur le flux -->

						<div class="panel panel-default fieldsetView">
							<div class="panel-heading">
								<h4 class="panel-title">Commentaires</h4>
							</div>
							<div class="panel-body">
								<?php echo $this->GFCComment->drawComments($comments); ?>
							</div>
						</div>


						<!-- Dossiers / Affaires rattachés -->
						<?php
						$divDossier = $this->Html->tag('div', 'Aucun dossier / affaire attaché pour ce flux', array('class' => 'alert alert-warning'));
						if (!empty($courrier['Dossier'])) {
							$dosContent = $this->Html->tag('dt', 'Dossier');
							$dosContent .= $this->Html->tag('dd', ($courrier['Dossier']['name'] != '') ? $courrier['Dossier']['name'] : $courrier['Dossier']['name']);
							$affContent = $this->Html->tag('dt', 'Affaire');
							$affContent .= $this->Html->tag('dd', ($courrier['Affaire']['name'] != '') ? $courrier['Affaire']['name'] : $courrier['Affaire']['name']);
							$divDossier = $this->Html->tag('dl', $dosContent);
							$divDossier .= $this->Html->tag('dl', $affContent);
						}
						?>
						<div class="panel panel-default fieldsetView">
							<div class="panel-heading">
								<h4 class="panel-title">Dossiers / Affaires</h4>
							</div>
							<div class="panel-body">
								<?php echo $divDossier; ?>
							</div>
						</div>
					</div>
				</div>
				<div id="flux_context" class="context col-sm-6  tabbable tabs-left">
					<?php
					$context_params = array(
							'fluxId' => $courrier['Courrier']['id'],
							'rights' => $rights,
							'parent' => $parent,
							'sortant' => $sortant,
							'mainDoc' => $mainDoc,
							'documentList' => $documents
					);
					echo $this->Element('context', $context_params);
					?>
				</div>
			</div>
		</div>
	</div>
</div>


<?php
	if( $displayBecauseUserIsInBancontenu ) {
?>
	<script type="text/javascript">
		//obtenir les boutons gauches(bouton fonction)
		function getFlowControls() {
			var url = "<?php echo '/courriers/getFlowControls/' . $courrier['Courrier']['id']; ?>";

			gui.request({
				url: url,
				noErrorMsg: true
			}, function (data) {
				$("body").append(data);
			});
		}
		getFlowControls();
		// $('.sendcpy').hide();
		gui.buttonbox({element: $('#flowControlsBis'), align: "left"});

		$('.editFlux').click(function () {
			gui.request({
				url: "/courriers/setInfos/<?php echo $courrier['Courrier']['id']; ?>",
				updateElement: $(this)
			}, function () {
				<?php if( $isAdmin ) :?>
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/courriers/view/{$courrier['Courrier']['id']}"; ?>";
				<?php else :?>
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/courriers/view/{$courrier['Courrier']['id']}/{$desktopIdToUse}"; ?>";
				<?php endif;?>
			});
		});
	</script>
<?php
	}
?>


<script type="text/javascript">
	$('.deleteFlux').click(function () {

		swal({
			showCloseButton: true,
			title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
			text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#d33",
			cancelButtonColor: "#3085d6",
			confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
			cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
		}).then(function (data) {
			if (data) {
				gui.loader({
					message: gui.loaderMessage,
					element: $('#courrier_skel')
				});

				var updateElement = $(this);
				gui.request({
					url: "/courriers/delete/<?php echo $courrier['Courrier']['id']; ?>",
					updateElement: updateElement
				}, function () {
					updateElement.removeAttr('loaded');
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index"; ?>";
				});
			} else {
				swal({
					showCloseButton: true,
					title: "Annulé!",
					text: "Vous n\'avez pas supprimé.",
					type: "error",

				});
			}
		});
		$('.swal2-cancel').css('margin-left', '-320px');
		$('.swal2-cancel').css('background-color', 'transparent');
		$('.swal2-cancel').css('color', '#5397a7');
		$('.swal2-cancel').css('border-color', '#3C7582');
		$('.swal2-cancel').css('border', 'solid 1px');

		$('.swal2-cancel').hover(function () {
			$('.swal2-cancel').css('background-color', '#5397a7');
			$('.swal2-cancel').css('color', 'white');
			$('.swal2-cancel').css('border-color', '#5397a7');
		}, function () {
			$('.swal2-cancel').css('background-color', 'transparent');
			$('.swal2-cancel').css('color', '#5397a7');
			$('.swal2-cancel').css('border-color', '#3C7582');
		});
	});

	<?php if( Configure::read('CD' ) != 81 ) :?>
	$('.print').click(function () {
		window.print();
	});
	<?php endif;?>

	$('.sendcpy').click(function () {
		gui.formMessage({
			title: '<?php echo __d('default', 'Button.send'); ?>',
			width: '600px',
			url: '/courriers/send/<?php echo $courrier['Courrier']['id'] . '/1'; ?>',
			buttons: {
				"<?php echo __d('default', 'Button.cancel'); ?>": function () {
					$(this).parents(".modal").modal('hide');
					$(this).parents(".modal").empty();
				},
				"<?php echo __d('default', 'Button.submit'); ?>": function () {
					var form = $(this).parents(".modal").find('form');
					gui.request({
						url: '/courriers/send/<?php echo $courrier['Courrier']['id'] . '/1'; ?>',
						data: form.serialize()
					}, function (data) {
						if (data.length > 4) { //FIXME avant avec 0 cela marchait
							getJsonResponse(data);
						} else {
							window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
						}
					});
					$(this).parents(".modal").modal('hide');
					$(this).parents(".modal").empty();
				}
			}
		});
	});

	$('.trCyclebold').css('font-weight', 'bold') ;
	$('.trCyclethinColored').css('font-style', 'italic') ;
	$('.trCyclethinColored').css('color', '#b11526') ;
	$('.trCycleboldColored').css('font-style', 'italic') ;
	$('.trCycleboldColored').css('color', '#b11526') ;

	function updateContextRight(newCourrierId) {
		var url = window.location.href;
		var urlCouper = url.split('/');
		var desktopId = urlCouper[urlCouper.length - 1];
		gui.request({
			url: '/courriers/updateContext/' + newCourrierId + '/' + desktopId
		}, function (data) {
			$('#flux_context').html(data);
		});
	}
</script>
