<?php

/**
 *
 * Courriers/send.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php

$formCourrier= array(
    'name' => 'Courrier',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Send.initDesktopService' => array(
            'labelText' =>__d('courrier', 'chooseInitDesktopService'),
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
//                'multiple' => true,
                'class' => 'destination',
                'options' => $services,
                'empty' =>true
            )
        ),
        'Send.flux_id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Send.copy' => array(
            'labelText' =>__d('courrier', 'Courrier.copy'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox'
            )
        ),
        'Send.copy_id' => array(
            'labelText' =>__d('courrier', 'Courrier.desktop_to_send'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
//                'multiple' => true,
                'empty' => true,
                'options' => @$allDesktops
            )
        ),
        'Comment.content' => array(
            'labelText' => __d('comment', 'Comment.content'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea'
            )
        )
    )
);
?>
<?php
if(!empty($itemList)) {
    foreach ($itemList['DispForm']['itemToLink'] as $i => $item) {
        $formCourrier['input']["DispForm.itemToLink.{$i}"] = array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value'=>$item
            )
        );
    }
}
?>
<legend id="legend">Aiguillage vers les services suivants</legend>
<?php
echo $this->Formulaire->createForm($formCourrier);
echo $this->Form->end();
?>

<script type="text/javascript">
    $("<legend>Liste des utilisateurs pour l'envoi d'une copie</legend>").insertBefore($('#SendCopy').parents('.form-group'));
    $('#SendCopyId').parents('.form-group').css('display', 'none');
    $('#SendCopy').change(function () {
        if ($(this).prop("checked")) {
            $('#SendCopyId').parents('.form-group').css('display', 'block');
            $("#SendCopyId").select2();
        } else {
            $('#SendCopyId').parents('.form-group').css('display', 'none');
        }
    });


    <?php if( Configure::read( 'Disp.Multipledestination') ) :?>
        $('#CourrierDispmultiplesendForm select').each(function () {
            setJselectNew($(this));
        });
    <?php else:?>
        setJselectNew($('#SendCopyId'));
    <?php endif;?>
    $(document).ready(function () {
        $("#SendInitDesktopService").select2();
    });

    <?php if(isset( $cpyOnly ) && $cpyOnly && !$isdisp):?>
        $("#SendInitDesktopService").parents('.form-group').css('display', 'none');
        $("#legend").css('display', 'none');
    <?php endif;?>

	<?php if($cpyOnly) :?>
		$('#SendInitDesktopService').parents('.form-group').css('display', 'none');
		$("#legend").css('display', 'none');
	<?php endif;?>
</script>
