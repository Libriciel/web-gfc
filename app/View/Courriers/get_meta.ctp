<?php

/**
 *
 * Courriers/get_meta.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (isset($viewOnly) && $viewOnly === true) {
    echo $this->Element('viewOnly');
}
$divContent = $this->Html->tag('div', __d('metadonnee', 'Metadonnee.void'), array('class' => 'alert alert-warning'));
$valOfSelect =  Configure::read('Selectvaluemetadonnee.id');
if (!empty($metas)) {
    $dlContent = '';
    foreach ($metas as $meta) {
        if ($meta['Metadonnee']['typemetadonnee_id'] != 3 || ($meta['Metadonnee']['typemetadonnee_id'] == 3 && $meta['CourrierMetadonnee']['valeur'] != 'no')) {
            $dlContent .= $this->Html->tag('dt', $meta['Metadonnee']['name']);
            if ($meta['Metadonnee']['typemetadonnee_id'] == 3) {
                $img = $this->Html->tag('i','',array('class'=>'fa fa-floppy-o-square-o','aria-hidden'=>true));
                $dlContent .= $this->Html->tag('dd', $img . ' ' . __d('metadonnee', 'Metadonnee.checked'));
            } else if ($meta['Metadonnee']['typemetadonnee_id'] == $valOfSelect) {
                $dlContent .= $this->Html->tag('dd', (isset( $meta['Selectvaluemetadonnee'][$meta['CourrierMetadonnee']['valeur']] ) ) ? $meta['Selectvaluemetadonnee'][$meta['CourrierMetadonnee']['valeur']] : 'Aucune information');
            } else {
                $dlContent .= $this->Html->tag('dd', ($meta['CourrierMetadonnee']['valeur'] != '') ? $meta['CourrierMetadonnee']['valeur'] : 'Aucune information');
            }
        }else{
            $dlContent .= $this->Html->tag('dt', $meta['Metadonnee']['name']);
            if ($meta['Metadonnee']['typemetadonnee_id'] == 3 && $meta['CourrierMetadonnee']['valeur'] == 'no') {
                $img = $this->Html->tag('i','',array('class'=>'fa fa-window-close-o','aria-hidden'=>true));
                $dlContent .= $this->Html->tag('dd', $img . ' ' . __d('metadonnee', 'Metadonnee.checked'));
            }
        }
    }
    $divContent = $this->Html->tag('dl', $dlContent);
}
?>
<div class="panel" >
    <div class="panel panel-default fieldsetView">
        <div class="panel-heading">
            <h4 class="panel-title"><?php echo __d('courrier', 'Metadonnees'); ?></h4>
        </div>
        <div class="panel-body form-horizontal">
            <?php echo $divContent; ?>
        </div>
    </div>
</div>

