<?php

/**
 *
 * Courriers/get_documents.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div style="height:auto;">
    <div class="main_document" style="height:150px;">
        <legend><?php echo __d('document', 'Document.main_doc'); ?></legend>
        <div class="panel-body">
        <?php
        if (!empty($mainDoc)) {
        ?>
            <div class="ctxdoc">
            <?php
            echo $this->Element('ctxdoc', array(
                    'ctxdoc' => $mainDoc['Document'],
                    'gfcDocType' => 'Document',
                    'cssClass' => 'mainDoc',
                    'isGeneratedFile' => !empty($mainDoc['Document']['isgenerated']) ? true : false
            ));
            ?>
            </div>
        <?php
        } else {
            echo $this->Html->tag('div', __d('document', 'Document.mainDoc.void'), array('class' => 'alert alert-warning'));
        }
        ?>
        </div>
    </div>
	<div class="panel-body" style="height:auto;">
		<span class="btn_send_mail btn btn-info-webgfc ctxdocBttnSendmail"><i class="fa fa-paper-plane" title="Envoyer un simple mail"></i> Envoyer un simple mail</span>
	</div>

	<legend><?php echo __d('document', 'Document.list'); ?></legend>
    <div class="panel-body" style="height:auto;">
        <?php
            if (!empty($documents)) {
        ?>
        <ul class="list-group">
        <?php echo $this->Element('documentList', array('documentList' => $documents)); ?>
        </ul>
        <?php
            } else {
                echo $this->Html->tag('div', __d('courrier', 'Document.void'), array('class' => 'alert alert-warning'));
            }
        ?>
    </div>
    <?php if (!empty($documentssignes)):?>
		<legend class="panel-title"><?php echo __d('document', 'Document.signe_list'); ?></legend>
		<div class="panel-body"  id="docssignes">
			<div class="ctxdoc">
				<ul class="list-group">
					<?php echo $this->Element('documentList', array('documentList' => $documentssignes)); ?>
				</ul>
				<?php
				/*echo $this->Element('ctxdoc', array(
					'ctxdoc' => $documentssignes['Document'],
					'gfcDocType' => 'Document',
				));*/
				?>
			</div>
		</div>
    <?php endif;?>
    <div class="upload" >
        <legend><?php echo __d('document', 'Document.upload'); ?></legend>
        <div class="panel-body">
            <div class="content">
                <div class="alert alert-warning">
                    <?php echo __d('document', ($fromDispEnv || !$right_upload) ? 'Document.upload.notallowed' : 'Document.upload.undefined'); ?>
                </div>
            </div>
        </div>
    </div>

    <?php if( !empty($mailsSent) ):?>
    	<legend><?php echo __d('document', 'Document.mailssent'); ?></legend>
		<div class="panel-body" id="mailssent">
			<div class="ctxdoc">
			<?php
			echo $this->Element('listmail', array(
				'ctxdoc' => $mailsSent
			));
			?>
			</div>
		</div>
    <?php endif;?>
</div>

<script type="text/javascript">
<?php /*if ($right_upload && !$fromDispEnv || Configure::read('Conf.SAERP')) {*/ ?>
    var ulfile = gui.createupload({
        button: "<?php echo __d('default', 'Button.uploadFile'); ?>",
        buttonIco: "/img/upload_16.png",
        label: "Choisir un fichier",
        url: "<?php echo "/Documents/upload"; ?>",
        fileInputName: "myfile",
        hiddenItem: {
            name: "courrierId",
            value: <?php echo $fluxId; ?>
        },
        fluxId:<?php echo $fluxId; ?>
    });
    $('.upload .content').html(ulfile);
<?php /*}*/ ?>
    initContextDocumentBttnActions();

	// pour permettre de lire les mails envoyés vu que le panneau latéral "descend" désormais avec la page
	$('#mailssent').css('height', '1000px');
	$('#documents').css('height', '20px');


$('.btnSendMail').button().click(function () {
	gui.request({
		url: '/courriers/setArs/<?php echo $fluxId; ?>',
		data: $('#ArmodelGetArsForm').serialize(),
		loader: true,
		loaderMessage: gui.loaderMessage,
		updateElement: updateElement
	}, function (data) {
		getJsonResponse(data);
		updateElement.removeAttr('loaded');
		affichageTabContext('<?php echo $fluxId; ?>');
	});
});


	var url = window.location.href;
	var urlCouper = url.split('/');
	if( urlCouper[4] == 'add' ) {
		$('#controls').css('margin-top', '300px');
	}
</script>
