<?php

/**
 *
 * Courriers/get_contacts.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$selectContent = '';
foreach ($contacts as $kAddressbook => $addressbook) {
    $optgroupContent = '';
    foreach ($addressbook as $kContact => $contact) {
        $optgroupContent .= $this->Html->tag('option', $contact, array('value' => $kContact));
    }
    $selectContent .= $this->Html->tag('optgroup', $optgroupContent, array('label' => $kAddressbook));
}
echo $selectContent;
?>
