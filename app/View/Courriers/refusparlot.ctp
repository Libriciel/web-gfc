<?php

/**
 *
 * Courriers/refusparlot.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Form->create('Notification', array('controller' => 'courriers', 'url' => 'refusparlot', 'refus'));
?>
<div class='panel-body'>
<?php
foreach ($flux as $i => $courrier) {
    $form = '<div>'.__d('courrier', 'Objet du refus').' concernant le flux :  <br />"'.$courrier['Courrier']['name'].'"<br /> de référence: "'.$courrier['Courrier']['reference'].'"</div>';
    $form .= '<textarea id="NotificationMessage" name="data[][Notification][message]" style="width: 300px;height: 150px;">Motif = </textarea>';
    $form .= $this->Form->hidden("{$i}.Notification.courrier_id", array('value' => $courrier['Courrier']['id']));
    echo $this->Html->tag('div', $form, array('class' => 'col-sm-6'));
}
?>
</div>
<?php
echo $this->Form->end();
?>
<div class="modal-footer zone-form">
    <div class=" controls ">
        <a class="btn btn-danger-webgfc btn-inverse " id="btn_cancel"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a class="btn btn-success " id="btn_ok"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn_ok').click(function () {
            var form = $('form', this);
            $("#NotificationRefusparlotForm").validate({
                'rules': {
                    'data[][Notification][message]': 'required'
                }
            });
            if (form_validate($(form))) {
                gui.request({
                    url: "/courriers/refusparlot/refus",
                    data: $('#NotificationRefusparlotForm').serialize(),
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                }, function () {
                    window.location.href = "/environnement/index/0/user";
                })
            } else {
                swal({
                    showCloseButton: true,
                    title: "Oops...",
                    text: "Veuillez vérifier votre formulaire!",
                    type: "error",

                });
            }
        });
        $('#btn_cancel').click(function () {
            $(this).parents('.modal').modal('hide');
            $(this).parents('.modal').empty();
        });
    });
</script>
