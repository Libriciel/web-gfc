<?php

/**
 *
 * Courriers/get_qualification.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (isset($viewOnly) && $viewOnly === true) {
    echo $this->Element('viewOnly');
}

$direction = array(
    0 => array(
        'img' => '<i class="fa fa-sign-out" style="color:#e51809;" aria-hidden="true"></i>',
        'txt' => __d('courrier', 'Courrier.courrier_sortant')
    ),
    1 => array(
        'img' => '<i class="fa fa-sign-in" style="color:#2ec07e;" aria-hidden="true"></i>',
        'txt' => __d('courrier', 'Courrier.courrier_entrant')
    ),
    2 => array(
        'img' => '<i class="fa fa-level-up" style="color:#5397a7;" aria-hidden="true"></i>',
        'txt' => __d('courrier', 'Courrier.courrier_interne')
    )
);

$tabDelaiUnite = array('jour', 'semaine', 'mois');
if (!empty($courrier['Soustype']['id'])) {
?>
    <fieldset class="fieldsetView">
        <legend><?php echo __d('courrier', 'Qualification'); ?></legend>
        <dl>
            <dt class="ui-state-focus"><?php echo __d('type', 'Type'); ?></dt>
            <dd><?php echo $types[$courrier['Soustype']['type_id']]; ?></dd>
            <dt class="ui-state-focus"><?php echo __d('soustype', 'Soustype'); ?></dt>
            <dd><?php echo $courrier['Soustype']['name']; ?></dd>
        </dl>
    </fieldset>

    <fieldset class="fieldsetView">
        <legend><?php echo __d('courrier', 'Informations complémentaires'); ?></legend>
        <dl>
            <dt class="ui-state-focus"><?php echo __d('courrier', 'Courrier.reference'); ?></dt>
            <dd><?php echo $courrier['Courrier']['reference']; ?></dd>
            <dt class="ui-state-focus"><?php echo __d('courrier', 'Courrier.entrant/sortant'); ?></dt>
            <dd>
                                    <?php echo $direction[$courrier['Soustype']['entrant']]['img'] . " " . $direction[$courrier['Soustype']['entrant']]['txt']; ?>
            </dd>
            <dt class="ui-state-focus"><?php echo __d('soustype', 'Soustype.delai'); ?></dt>
            <dd><?php echo (isset($courrier['Soustype']['delai_nb']) && isset($courrier['Soustype']['delai_unite'])) ? $courrier['Soustype']['delai_nb'] . " " . $tabDelaiUnite[$courrier['Soustype']['delai_unite']] . (($courrier['Soustype']['delai_nb'] > 1) ? 's' : '' ) : '&nbsp;'; ?></dd>

        </dl>
    </fieldset>
<?php
} else {
    echo $this->Html->tag('div', __d('soustype', 'TypeSoustype.void'), array('class' => 'voidMessage'));
}
?>




