<?php

/**
 *
 * Courriers/set_infos.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (!empty($newCourrierId)) {
    echo $newCourrierId;
} else {
    echo $this->Html->script('contactinfo.js', array('inline' => true));
    echo $this->Html->script('setInfos.js', array('inline' => true));
?>
<?php
$url = array('controller' => 'courriers', 'action' => 'createInfos');
if (isset($this->data['Courrier']['id'])) {
  $url[] = $this->data['Courrier']['id'];
}

$tabDelaiUnite = array(
    0 => 'jour',
    1 => 'semaine',
    2 => 'mois'
);
$priorites = array(
  0 => __d('courrier', 'Courrier.priorite_basse'),
  1 => __d('courrier', 'Courrier.priorite_moyenne'),
  2 => __d('courrier', 'Courrier.priorite_haute')
  );
$formCourrier = array(
    'name' => 'Courrier',
    'label_w' => 'col-sm-3',
    'input_w' => 'col-sm-5',
    'input' => array(
        'Courrier.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        )
    )
);
echo $this->Formulaire->createForm($formCourrier);
?>

<div class="row">
    <?php

$formCourrierFlux = array(
        'name' => 'Courrier',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-8',
        'form_url' => $url,
        'input' => array(
            'Courrier.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            )
        )
    );
    $formCourrierFlux['input']['Courrier.name'] =array(
        'labelText' =>__d('courrier', 'Courrier.name'),
        'inputType' => 'text',
        'items'=>array(
            'required'=>true,
            'type'=>'text'
        )
    );
    $formCourrierFlux['input']['Courrier.origineflux_id'] =array(
        'labelText' =>__d('courrier', 'Courrier.origineflux_id'),
        'inputType' => 'select',
        'items'=>array(
            'type' => 'select',
            'options' => $origineflux,
            'empty' => true
        )
    );
    $formCourrierFlux['input']['Courrier.direction'] =array(
        'labelText' =>__d('courrier', 'Courrier.direction'),
        'inputType' => 'hidden',
        'items'=>array(
            'type'=>'hidden'
        )
    );
    $formCourrierFlux['input']['Courrier.objet'] =array(
        'labelText' =>__d('courrier', 'Courrier.objet'),
        'inputType' => 'textarea',
        'items'=>array(
            'type'=>'textarea'
        )
    );
    $formCourrierFlux['input']['Courrier.priorite'] =array(
        'labelText' =>__d('courrier', 'Courrier.priorite'),
        'inputType' => 'select',
        'items'=>array(
            'type' => 'select',
            'options' => $priorites,
            'default' => 0
        )
    );
    if( !Configure::read('Conf.SAERP') ) {
        $formCourrierFlux['input']['Courrier.affairesuiviepar_id'] = array(
            'labelText' => __d('courrier', 'Courrier.affairesuiviepar_id'),
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                'options' => $allDesktops,
                'empty' => true
            )
        );
    }
    ?>
    <div class="col-sm-6">
        <legend><?php echo  __d('courrier', 'Courrier.fluxFieldset'); ?></legend>
        <div  class="panel-body  form-horizontal" >
            <?php echo $this->Formulaire->createForm($formCourrierFlux); ?>
        </div>
    </div>
    <?php
    //réalisation des formulaire du parite Qualification
        $soustypeDelaiNb = (isset($this->data['Soustype']['delai_nb'])) ? $this->data['Soustype']['delai_nb']  : null;
        $soustypeDelaiUnite = (isset($this->data['Soustype']['delai_unite'])) ? $this->data['Soustype']['delai_unite']  : null;
        $formCourrierQualification = array(
            'name' => 'Courrier',
            'label_w' => 'col-sm-5',
            'input_w' => 'col-sm-6',
            'input' => array(
                'Courrier.id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                )
            )
        );

    if( Configure::read('Conf.SAERP') ) {
        $formCourrierQualification['input']['Courrier.typedocument'] =array(
            'labelText' =>__d('courrier', 'Courrier.typedocument'),
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                "options" => $typesdocument,
                'empty' => true
            )
        );
        $formCourrierQualification['input']['Courrier.marche_id'] =array(
            'labelText' =>__d('courrier', 'Courrier.marche_id'),
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                "options" => !empty($marches)? $marches: array(),
                'empty' => true
            )
        );
        $formCourrierQualification['input']['Courrier.consultation_id'] =array(
            'labelText' =>__d('courrier', 'Courrier.consultation_id'),
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                "options" => $consultations,
                'empty' => true
            )
        );
        $formCourrierQualification['input']['Ordreservice.numero'] =array(
            'labelText' =>__d('ordreservice', 'Ordreservice.numero'),
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text'
            )
        );
        $formCourrierQualification['input']['Ordreservice.objet'] =array(
            'labelText' =>__d('ordreservice', 'Ordreservice.objet'),
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text'
            )
        );
        $formCourrierQualification['input']['Ordreservice.montant'] =array(
            'labelText' =>__d('ordreservice', 'Ordreservice.montant'),
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text'
            )
        );
        $formCourrierQualification['input']['Ordreservice.marche_id'] =array(
            'inputType' => 'hidden',
            'items'=>array(
                'type' => 'hidden'
            )
        );
        $formCourrierQualification['input']['Pliconsultatif.origineflux_id'] =array(
            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.origineflux_id'),
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                'empty' => true,
                'options' => $origineflux
            )
        );
        $formCourrierQualification['input']['Pliconsultatif.numero'] =array(
            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.numero'),
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text'
            )
        );
        $formCourrierQualification['input']['Pliconsultatif.lot'] =array(
            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.lot'),
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text'
            )
        );
        $formCourrierQualification['input']['Pliconsultatif.objet'] =array(
            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.objet'),
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text'
            )
        );
        $formCourrierQualification['input']['Pliconsultatif.date'] =array(
            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.date'),
            'inputType' => 'text',
            'dateInput' =>true,
            'items'=>array(
                'type' => 'text',
                'class' => 'datepickerPli',
                'readonly' => true,
                'data-format'=>'dd/mm/yyyy'
            )
        );
        $formCourrierQualification['input']['Pliconsultatif.marche_id'] =array(
            'inputType' => 'hidden',
            'items'=>array(
                'type' => 'hidden'
            )
        );
        $formCourrierQualification['input']['Pliconsultatif.heure'] =array(
            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.heure'),
            'inputType' => 'time',
            'items'=>array(
                'type' => 'time',
                'timeFormat' => '24',
                'interval' => 05,
                'empty' => true
            )
        );
    }
        if( Configure::read('Use.AuthRights') && !Configure::read('Conf.SAERP') ) {
            $formCourrierQualification['input']['Types.Type'] = array(
                'labelText' =>__d('type', 'Type'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
    //                "options" => array(),
    //                "options" =>$typeOptions,
                    'disabled' => $isInCircuit ? 'disabled' : '',
                    'empty' => true
                )
            );
        }else {
            $formCourrierQualification['input']['Types.Type'] = array(
                'labelText' =>__d('type', 'Type'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
    //                "options" => array(),
                    "options" =>$typeOptions,
                    'disabled' => $isInCircuit ? 'disabled' : '',
                    'empty' => true
                )
            );
        }
        $formCourrierQualification['input']['Courrier.soustype_id'] = array(
            'labelText' =>__d('soustype', 'Soustype'),
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                "options" => array(),
                'disabled' => $isInCircuit ? 'disabled' : '',
                'empty' => true
            )
        );
        $formCourrierQualification['input']['Courrier.reference'] = array(
            'labelText' =>__d('courrier', 'Courrier.reference'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text',
                'readonly' => true,
                'class' => 'fakeDisabledInputText'
            )
        );
        $formCourrierQualification['input']['Courrier.entrant'] = array(
            'labelText' =>__d('courrier', 'Courrier.entrant/sortant'),
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text',
                'readonly' => true,
                'class' => 'fakeDisabledInputText'
            )
        );
        $formCourrierQualification['input']['Courrier.delai_nb'] = array(
            'labelText' =>'Délai de traitement',
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text',
                'empty' => true,
//                'class' =>'fakeDisabledInputText',
//                'readonly' => true,
                'value' => $soustypeDelaiNb
            )
        );
        $formCourrierQualification['input']['Courrier.delai_unite'] = array(
            'labelText' =>false,
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                'options' => $tabDelaiUnite,
                'value' => $soustypeDelaiUnite,
//                'readonly' => true,
                'empty' => true,
                'class' => 'unitecreate '
            )
        );
    ?>
    <div class="col-sm-6">
        <legend><?php echo  __d('courrier', 'Qualification');?></legend>
        <div  class="panel-body  form-horizontal" >
                <?php echo $this->Formulaire->createForm($formCourrierQualification); ?>
        </div>
    </div>
</div>
<div class='row'>
    <?php
    $formCourrierContact = array(
        'name' => 'Courrier',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-5',
        'input' => array(
            'Courrier.organisme_id' => array(
                'inputType' => 'select',
                'labelText' =>__d('courrier', 'Courrier.contact'),
                'items'=>array(
                    'type'=>'select',
                    'options' =>$organismOptions,
                    'value' => $sansOrganisme
                )
            ),
            'Courrier.contact_id' => array(
                'inputType' => 'select',
                'labelText' =>__d('courrier', 'Courrier.contact_id'),
                'items'=>array(
                    'type'=>'select',
                    'options' => $contactOptions/*,
                    'value' => isset( $this->request->data['Courrier']['contact_id'] ) ? $this->request->data['Courrier']['contact_id'] : $sansContact*/
                )
            )
        )
    );
    ?>
    <div class="col-sm-6">
        <legend><?php echo __d('courrier', 'Courrier.contactFieldset'); ?></legend>
        <div  class="panel-body  form-horizontal" >
                <?php echo $this->Formulaire->createForm($formCourrierContact); ?>
        </div>
    </div>
    <?php
    //réaliser les formulaire du partie Date
        $formCourrierDate = array(
            'name' => 'Courrier',
            'label_w' => 'col-sm-5',
            'input_w' => 'col-sm-6',
            'input' => array(
                'Courrier.datereception' => array(
                    'inputType' => 'text',
                    'labelText' =>__d('courrier', 'Courrier.datereception'),
                    'dateInput' =>true,
                    'items'=>array(
                        'type'=>'text',
                        'readonly' => true,
						'value'=> date('d/m/Y'),
                        'data-format'=>'dd/MM/yyyy'
                    )
                ),
					'Courrier.date' => array(
						'inputType' => 'text',
						'labelText' =>__d('courrier', 'Courrier.date'),
//                    'dateInput' => true,
						'items'=>array(
//                        'default'=> date('d/m/Y'),
								'value'=> date('d/m/Y'),
								'readonly' => true,
								'type'=>'text'
						)
					)
			)
        );
    ?>
    <div class="col-sm-6">
        <legend><?php echo  __d('courrier', 'Courrier.dateFieldset'); ?></legend>
        <div  class="panel-body  form-horizontal" >
                <?php echo $this->Formulaire->createForm($formCourrierDate); ?>
        </div>
    </div>
</div>
<?php
echo $this->Form->end();
?>

<script type="text/javascript">

	$("#CourrierDatereception").removeAttr('readonly');
	$('#CourrierDate' ).css('cursor', 'not-allowed');
	// Si la date de réception est supérieure à la date de création
	$("#CourrierDatereception").bind('change', function () {
		var dateisover = false;
		var datereception = $('#CourrierDatereception').val();
		var datecreation = $('#CourrierDate').val();
		var daterecu = datereception.split("/");
		var newDatereception = daterecu[1]+"/"+daterecu[0]+"/"+daterecu[2];
		var entree = new Date(newDatereception).getTime();
		var datecree = datecreation.split("/");
		var newDatecree = datecree[1]+"/"+datecree[0]+"/"+datecree[2];
		var cree = new Date(newDatecree).getTime();

		if(entree > cree) {
			dateisover = true;
		}
		if( dateisover ) {
			gui.disablebutton({
				element: $('#createFlux .panel-footer'),
				button: '<?php echo __d("courrier", "Suivant"); ?> '
			});
			$("#next").addClass('disabled').removeAttr("href");
		}
		else {
			gui.enablebutton({
				element: $('#createFlux .panel-footer'),
				button: '<?php echo __d("courrier", "Suivant"); ?> '
			});
			$("#next").removeClass("disabled").attr("href", "#");
		}
	});

	$("#CourrierName").bind('change', function () {
		gui.enablebutton({
			element: $('#createFlux .panel-footer'),
			button: '<?php echo __d("courrier", "Suivant"); ?> '
		});
	});

	<?php if(isset($this->request->data['Courrier']['organisme_id']) && isset($this->request->data['Courrier']['contact_id']) ):?>
        $('#CourrierOrganismeId').val("<?php echo $this->request->data['Courrier']['organisme_id'];?>").change();
        $('#CourrierContactId').val( "<?php echo $this->request->data['Courrier']['contact_id'];?>").change();
    <?php else:?>
        $('#CourrierOrganismeId').val("<?php echo $sansOrganisme;?>").change();
        $('#CourrierContactId').val( "<?php echo $sansContact;?>").change();
    <?php endif;?>


    $('#CourrierOrganismeId').attr('disabled', 'disabled');
    $('#CourrierContactId').attr('disabled', 'disabled');

    var fakeDisabledSelected = function(selector) {
        var original = $(selector),
            hidden = $('<input id="Hidden'+$(original).attr('id')+'" type="hidden" name="'+$(original).attr('name')+'" value="'+$(original).attr('value')+'" />');
        $(original).after(hidden);
        if(selector === '#CourrierOrganismeId' ) {
            $('#s2id_CourrierOrganismeId .select2-arrow').hide();
        }
        if(selector === '#CourrierContactId' ) {
            $('#s2id_CourrierContactId .select2-arrow').hide();
        }
    };

    $(document).ready(function() {
        fakeDisabledSelected('#CourrierOrganismeId');
        fakeDisabledSelected('#CourrierContactId');
    });


    $('document').ready(function () {
        $('#CourrierName').prop('spellcheck', true);

        $('.form_datetime input').datepicker({
            language: 'fr-FR',
            format: "dd/mm/yyyy",
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked'
        });

        $('#PliconsultatifHeureHour').select2({allowClear: true});
        $('#PliconsultatifHeureMin').select2({allowClear: true});

        $('select[name="data[Courrier][affairesuiviepar_id]"]').select2({allowClear: true, placeholder: "Sélectionner une personne"});
        $('select[name="data[Courrier][origineflux_id]"]').select2({allowClear: true, placeholder: "Sélectionner une origine"});
        $('select[name="data[Courrier][priorite]"]').select2({allowClear: true, placeholder: "Sélectionner une priorité"});

        $("#CourrierDelaiUnite").select2({allowClear: true, placeholder: "Sélectionner une unité"});

        $('#s2id_CourrierDelaiUnite').css({
            height: '0px',
            width: '46%',
            float: 'right',
            position: 'relative',
            top: '-52px'
        });
    });
    $('select[name="data[Types][Type]"]').select2({allowClear: true, placeholder: "Sélectionner un type"});
    $('select[name="data[Courrier][soustype_id]"]').select2({allowClear: true, placeholder: "Sélectionner un sous-type"});
    //contruction du tableau de types / soustypes
    ////contruction du tableau de types / soustypes
    <?php if (Configure::read('Use.AuthRights') && !Configure::read('Conf.SAERP')) :?>
            var types = [];
            <?php foreach ($types as $type) { ?>
            var type = {
                id: "<?php echo $type['Type']['id']; ?>",
                name: "<?php echo addslashes( $type['Type']['name'] ); ?>",
                soustypes: []
            };
                    <?php foreach ($type['Soustype'] as $stype) { ?>
            var soustype = {
                id: "<?php echo $stype['id']; ?>",
                name: "<?php echo addslashes( $stype['name'] ); ?>",
                entrant: "<?php echo $stype['entrant']; ?>"
            };
            type.soustypes.push(soustype);
                    <?php } ?>
            types.push(type);
            <?php } ?>
            function fillSoustypes(type_id, stype_id) {
                $("#CourrierSoustypeId").empty();
//                $("#CourrierSoustypeId").append($("<option value=''> --- </option>"));
                for (i in types) {
                    if (types[i].id == type_id) {
                        for (j in types[i].soustypes) {
                            if (types[i].soustypes[j].id == stype_id) {
                                $("#CourrierSoustypeId").append($("<option value='" + types[i].soustypes[j].id + "' selected='selected'>" + types[i].soustypes[j].name + "</option>"));
                            } else {
                                $("#CourrierSoustypeId").append($("<option value='" + types[i].soustypes[j].id + "'>" + types[i].soustypes[j].name + "</option>"));
                            }
                        }
                    }
                }
            }


//            $("#TypesType").append($("<option value=''> --- </option>"));
            //remplissage de la liste des types
            for (i in types) {
                $("#TypesType").append($("<option value='" + types[i].id + "'>" + types[i].name + "</option>"));
            }
            //definition de l action sur type
            $("#TypesType").bind('change', function () {
                var type_id = $('option:selected', this).attr('value');
                fillSoustypes(type_id);
            });
        <?php else :?>
            $("#TypesType").append(<?php $typeOptions;?>);
            $('#TypesType').change(function () {
                var type_id = $(this).val();
                var soustypeLists = <?php echo json_encode($soustypeOptions); ?>;
                $('#CourrierSoustypeId option').remove();

                // On est obligés de passer par un array car on ne peut trier les clés d'un objet
                var options = [];
                for (var value in soustypeLists[type_id]) {
                    options.push([value, soustypeLists[type_id][value]]);
                }
                options.sort(function(a, b) {
                    if(a[1] < b[1]) return -1;
                    else if(a[1] > b[1]) return 1;
                    return 0;
                });

                $.each(options, function (index, value) {
                    $('#CourrierSoustypeId').append($("<option value='" + value[0] + "'>" + value[1] + "</option>"));
                });
            });
        <?php endif;?>

    //definition de l action sur stype
    $("#CourrierSoustypeId").bind('change', function () {
        var stype_id = $('option:selected', this).attr('value');
        $('.msgChange').remove();
        var reloadImgString = '<?php echo $this->Html->image('/img/reload.png'); ?>';
        var reloadImgFullString = '<?php echo $this->Html->image('/img/reload.png', array('alt' => __d('courrier', 'Courrier.messagechange.short'), 'title' => __d('courrier', 'Courrier.messagechange'))); ?>';
        $('.fakeDisabledInputText').after($('<span></span>').addClass('msgChange').html(reloadImgString + ' ' + "<?php echo __d('courrier', 'Courrier.messagechange'); ?>"));
        $('#delai').after($('<div></div>').addClass('msgChange').html(reloadImgString + ' ' + "<?php echo __d('courrier', 'Courrier.messagechange'); ?>"));
        $('.context_accordion .ui-accordion-header').each(function () {
            if ($('a', this).length > 0) {
                $(this).append($('<span></span>').addClass('msgChange').html(reloadImgFullString));
            }
        });
        $('.unitecreate').css({top: '-66px'});
    });


    $('#CourrierPriorite').change(function () {
        $('#priorityImg').remove();
        genPriorityImg();
    });
    genPriorityImg();



// Actions pour la partie Organisme
    <?php
        $searchOrganismeImg = '<i class="fa fa-search table" id=searchOrganisme name=searchOrganisme title="Rechercher un organisme"></i>';
    ?>
        var searchOrganismeImg = $('<?php echo $searchOrganismeImg; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -30px auto auto', cursor: 'pointer'}).click(function () {
        gui.formMessage({
            url: "/organismes/search_organismes_courriers/",
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#searchOrganisme').parents('fieldset'),
            width: 700,
            top: "-10px",
            buttons: {
               '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    $(this).parents(".modal").modal('hide');
                    $(this).parents(".modal").empty();
                }
            }
        });
    }).appendTo($('#CourrierOrganismeId').parent());

	<?php if( $right_action_carnet_adresse_flux ) :?>
<?php
    $addOrganismeImg = '<i class="fa fa-plus-circle table" id=addOrganisme name=addOrganisme title="Ajouter un organisme"></i>';
?>
    var addOrganismeImg = $('<?php echo $addOrganismeImg; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -50px auto auto', cursor: 'pointer'}).click(function () {
//        $(".modal").empty();
        gui.formMessage({
            url: "/courriers/add_organisme",
            loader: true,
            updateElement: $('#addOrganisme').parents('fieldset'),
            loaderMessage: gui.loaderMessage,
            width: 600,
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents(".modal").modal('hide');
                    $(this).parents(".modal").empty();
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $('#CourrierAddOrganismeForm');
                    if (form_validate(form)) {
                        gui.request({
                            url: form.attr('action'),
                            data: form.serialize()
                        }, function (data) {
                            orgInfos = jQuery.parseJSON(data);
                            layer.msg("Les informations ont bien été enregistrées");
                            refreshOrganismes(orgInfos);
                            $('#HiddenCourrierOrganismeId').val(orgInfos['id']);
                            $('#HiddenCourrierContactId').val(orgInfos['contacts']['id']);
                        });
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        });
    }).appendTo($('#CourrierOrganismeId').parent());
    // Modification fiche contact
<?php
//$editOrganismeImg = $this->Html->image('/img/tool.png', array('id' => 'editOrganisme', 'name' => 'editOrganisme', 'title' => __d('organisme', 'Organisme.edit')));
$editOrganismeImg = '<i class="fa fa-pencil table" id=editOrganisme name=editOrganisme title="Modification"></i>';
?>

    var editOrganismeImg = $('<?php echo $editOrganismeImg; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -70px auto auto', cursor: 'pointer'}).click(function () {
        orgId = $('#CourrierOrganismeId').val();
//        contactId = $('#CourrierContactId').val();
//        if (contactId != '') {
//            url = "/courriers/edit_organisme/" + orgId + '/' + contactId;
//        }
//        else {
        url = "/courriers/edit_organisme/" + orgId;
//        }
        $(".modal").empty();
        if (orgId == "<?php echo $sansOrganisme; ?>" || orgId == "Sans organisme"  || $('#select2-chosen-1').text() == "Sans organisme" ) {
            var title = "<?php echo __d('organisme', 'Organisme.edit') ?>";
            var text = "<?php echo __d('organisme', 'Organisme.edit.forbidden') ?>";
            swal({
                    showCloseButton: true,
                title: 'Oops...',
                text: title + "<br>" + text,
                type: 'warning',

            });
        }
        else {

            gui.formMessage({
                url: url,
                loader: true,
                updateElement: $('#editOrganisme').parents('fieldset'),
                loaderMessage: gui.loaderMessage,
                width: 600,
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        var form = $(this).parents('.modal').find('form');
                        if (form_validate(form)) {
                            gui.request({
                                url: form.attr('action'),
                                data: form.serialize()
                            }, function (data) {
                                orgInfos = jQuery.parseJSON(data);
                                layer.msg("Les informations ont bien été enregistrées");
                                refreshOrganismes(orgInfos);
                                $('#HiddenCourrierOrganismeId').val(orgInfos['id']);
                            });
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }
            });
        }
    }).appendTo($('#CourrierOrganismeId').parent());

<?php endif;?>
    <?php
        $searchContactImg = '<i class="fa fa-search table" id=searchContact name=searchContact title="Rechercher un contact"></i>';
    ?>
        var searchContactImg = $('<?php echo $searchContactImg; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -30px auto auto', cursor: 'pointer'}).click(function () {
        var orgId = $('#CourrierOrganismeId').val();
        gui.formMessage({
            url: "/contacts/search_contacts_courriers/" + orgId,
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#searchContact').parents('fieldset'),
            width: 700,
            top: "-10px",
            buttons: {
               '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    $(this).parents(".modal").modal('hide');
                    $(this).parents(".modal").empty();
                }
            }
        });
    }).appendTo($('#CourrierContactId').parent());

	<?php if( $right_action_carnet_adresse_flux ) :?>
<?php
//$addContactImg = $this->Html->image('/img/contact_add_16.png', array('id' => 'addContact', 'name' => 'addContact', 'title' => __d('addressbook', 'Contact.add')));
$addContactImg = '<i class="fa fa-plus-circle table" id=addContact name=addContact title="Ajouter un contact"></i>';
?>
    var addContactImg = $('<?php echo $addContactImg; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -50px auto auto', cursor: 'pointer'}).click(function () {
        $(".modal").empty();
        orgId = $('#CourrierOrganismeId').val();
        if (orgId == '') {
            var title = "<?php echo __d('contact', 'Contact.add') ?>";
            var text = "<?php echo __d('contact', 'Contact.add.undefined') ?>";
            swal({
                    showCloseButton: true,
                title: 'Oops...',
                text: title + "<br>" + text,
                type: 'warning',

            });
        } else {
            gui.formMessage({
                url: "/courriers/add_contact/" + orgId,
                loader: true,
                loaderMessage: gui.loaderMessage,
                updateElement: $('#addContact').parents('fieldset'),
                width: 550,
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        var form = $(this).parents('.modal').find('form');
                        if (form_validate(form)) {
                            gui.request({
                                url: form.attr('action'),
                                data: form.serialize()
                            }, function (data) {
                                contactInfos = jQuery.parseJSON(data);
                                layer.msg("Les informations ont bien été enregistrées");
                                refreshContact(contactInfos);
                                $('#HiddenCourrierContactId').val(contactInfos['contacts']['id']);
                            });
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }

            });
        }
    }).appendTo($('#CourrierContactId').parent());

    // Modification contact
<?php
//$editContactImg = $this->Html->image('/img/tool.png', array('id' => 'editContact', 'name' => 'editContact', 'title' => __d('contact', 'Contact.edit')));
$editContactImg = '<i class="fa fa-pencil table" id=editContact name=editContact title="Modifier le contact"></i>';
?>
    var editContactImg = $('<?php echo $editContactImg; ?>').css({height: '16px', color: '#5397a7', width: '16px', float: 'right', margin: '-42px -70px auto auto', cursor: 'pointer'}).click(function () {
        orgId = $('#CourrierOrganismeId').val();
        contactId = $('#CourrierContactId').val();
        if (contactId == "<?php echo $sansContact; ?>" || contactId == "Sans contact" || $('#select2-chosen-2').text() === " Sans contact" ) {
            var title = "<?php echo __d('contact', 'Contact.edit') ?>";
            var text = "<?php echo __d('contact', 'Contact.edit.forbidden') ?>";
            swal({
                    showCloseButton: true,
                title: 'Oops...',
                text: title + "<br>" + text,
                type: 'warning',

            });

        }
        /*
         if (contactId == '') {
         var title = "<?php echo __d('contact', 'Contact.edit') ?>";
         var text = "<?php echo __d('contact', 'Contact.edit.undefined') ?>";
         swal(
         'Oops...',
         title + "<br>" + text,
         'warning'
         );
         }*/
        else {
            url = "/courriers/edit_contact/" + contactId;
            $(".modal").empty();
            gui.formMessage({
                url: url,
                loader: true,
                loaderMessage: gui.loaderMessage,
                updateElement: $('#editContact').parents('fieldset'),
                width: 600,
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        var form = $('#CourrierEditContactForm');
                        if (form_validate(form)) {
                            gui.request({
                                url: form.attr('action'),
                                data: form.serialize()
                            }, function (data) {
                                contactInfos = jQuery.parseJSON(data);
                                layer.msg("Les informations ont bien été enregistrées");
                                refreshContact(contactInfos);
                                $('#HiddenCourrierContactId').val(contactInfos['contacts']['id']);
                            });
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }
            });
        }
    }).appendTo($('#CourrierContactId').parent());

    <?php endif;?>
    showContactinfo();
    showOrganismeinfo();
    $("input[name='data[Courrier][datereception]']").bind('change', function () {
        gui.request({
            url: "/courriers/ajaxformvalid/datereception",
            data: $('#CourrierCreateInfosForm').serialize()
        }, function (data) {
            processJsonFormCheck(data);
        });
    });
    $("input[name='data[Courrier][date]']").bind('change', function () {
        gui.request({
            url: "/courriers/ajaxformvalid/date",
            data: $('#CourrierCreateInfosForm').serialize()
        }, function (data) {
            processJsonFormCheck(data);
        });
    });</script>

<script type="text/javascript">
<?php
//    $toolTipImg = $this->Html->image('/img/help-contents.png', array('id' => 'tooltipImage','name' => 'tooltipImage' , 'title' => __d('soustype', 'Soustype.information')) );
    $toolTipImg = '<i class="fa fa-info table" id=tooltipImage name=tooltipImage title="Informations"></i>';
?>

    $('<?php echo $toolTipImg; ?>').css({height: '20px', width: '20px', color:'#5397a7'}).appendTo($('#s2id_CourrierSoustypeId').parent()).css("cursor", "pointer").css("margin", "-42px -29px auto auto").css("float", "right");
    $('#CourrierSoustypeId').change(function () {
        var dataString = 'id=' + $('#CourrierSoustypeId').val();
        if ($('#CourrierSoustypeId').val() != null) {
            $.ajax({
                type: 'post',
                url: '/Soustypes/getInformation/' + $('#CourrierSoustypeId').val(),
                data: dataString,
                success: function (data) {
                    if (data != "") {
                        $('#tooltipImage').popover({content: data, container: $('#tooltipImage').parent()});
                    } else {
                        $('#tooltipImage').popover({content: "Aucune information", container: $('#tooltipImage').parent()});
                    }
                }
            });
        }
    });
    $('#s2id_PliconsultatifHeureHour').css({
        width: '48%',
        float: 'left'
    });
    $('#s2id_PliconsultatifHeureMin').css({
        width: '48%',
        float: 'right'
    });
    $('#CourrierDelaiNb').css({
        width: '50%'
    });
    function mousePosition(ev) {
        ev = ev || window.event;
        if (ev.pageX || ev.pageY) {
            return {x: ev.pageX, y: ev.pageY};
        }
        return {
            x: ev.clientX + document.body.scrollLeft - document.body.clientLeft,
            y: ev.clientY + document.body.scrollTop - document.body.clientTop
        };
    }




    var checkExists = function () {
        $('.exists').remove();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Ordresservices/getExistance',
            data: $('#CourrierSetInfosForm').serialize(),
            success: function (data) {
                if (data.length > 0) {
                    var exists = $('<div></div>').html('<?php echo __d('ordreservice', 'Ordreservice.ExistsListTitle'); ?>').addClass('exists').addClass('ui-state-highlight');
                    $('#OrdreserviceNumero').parent().after(exists);
                }
            }
        });
    }

    function updateContextRight(newCourrierId) {
        var url = window.location.href;
        var urlCouper = url.split('/');
        var desktopId = urlCouper[urlCouper.length - 1];
        gui.request({
            url: '/courriers/updateContext/' + newCourrierId + '/' + desktopId
        }, function (data) {
            $('#flux_context').html(data);
        });

    }

    $('.fakeDisabledInputText' ).css('cursor', 'not-allowed');
    // $('#CourrierDate' ).css('cursor', 'not-allowed');
    $('#select2-chosen-2').css('cursor', 'not-allowed');
    $('#select2-chosen-1').css('cursor', 'not-allowed');
</script>

<?php
}
?>
