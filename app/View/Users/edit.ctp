<?php

/**
 *
 * Users/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
//FIXME
if ($this->Session->check( 'Message.flash' ) ) {
    echo $this->Session->flash();
}
if ($this->Session->check( 'Message.auth' ) ) {
    echo $this->Session->flash( 'auth' );
}
?>

<script type="text/javascript">
    function reloadDelegDependances() {
        loadDeleg();
    }
</script>
<div class="row" id="modalEditUserBody">
    <ul class="nav nav-tabs titre" role="tablist">
        <li class="active"><a href="#info_user" role="tab" data-toggle="tab">Informations personnelles</a></li>
        <li><a href="#hablitations" role="tab" data-toggle="tab">Habilitations</a></li>
    </ul>
    <div class="tab-content">
        <div id="info_user" class="tab-pane fade active in" >
            <div id="panel" class="col-sm-12 infoZone">
                <div id="infoPerson">
                    <div class="panel-body">
                        <?php
                         $infoListFields = array(
                            'username',
                            'nom',
                            'prenom',
                            'mail',
                            'numtel'
                            );

                        $formUserEdit = array(
                            'name' => 'User',
                            'label_w' => 'col-sm-4',
                            'input_w' => 'col-sm-6',
                            'input' => array(
                                'User.id' => array(
                                    'inputType' => 'hidden',
                                    'items'=>array(
                                        'type'=>'hidden'
                                    )
                                ),
//                                'User.desktop_id' => array(
//                                    'inputType' => 'hidden',
//                                    'items'=>array(
//                                        'type'=>'hidden'
//                                    )
//                                ),
//                                'User.password' => array(
//                                    'inputType' => 'hidden',
//                                    'items'=>array(
//                                        'type'=>'hidden'
//                                    )
//                                ),
                                'User.username' => array(
                                    'labelText' =>__d('user', 'User.username'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type'=>'text'
                                    )
                                ),
                                'User.nom' => array(
                                    'labelText' =>__d('user', 'User.nom'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type'=>'text'
                                    )
                                ),
                                'User.prenom' => array(
                                    'labelText' =>__d('user', 'User.prenom'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type'=>'text'
                                    )
                                ),
                                'User.mail' => array(
                                    'labelText' =>__d('user', 'User.mail'),
                                    'inputType' => 'email',
                                    'items'=>array(
                                        'type'=>'email'
                                    )
                                ),
                                'User.numtel' => array(
                                    'labelText' =>__d('user', 'User.numtel'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type'=>'text'
                                    )
                                ),
                                'User.active' => array(
                                    'labelText' =>__d('user', 'User.active'),
                                    'inputType' => 'checkbox',
                                    'items'=>array(
                                        'type'=>'checkbox',
                                        'checked'=>$this->request->data['User']['active']
                                    )
                                ),
                                'User.addressbook_private' => array(
                                   'labelText' =>__d('user', 'User.addressbook_private'),
                                   'inputType' => 'checkbox',
                                   'items'=>array(
                                       'type'=>'checkbox',
                                       'checked'=>$this->request->data['User']['addressbook_private']
                                   )
                                )
                            )
                         );

//                         foreach ($user['User'] as $kInfo => $info) {
//                             if( $kInfo == 'active' ) {
//                                 $formUserEdit['input']['User.active'] = array(
//                                    'labelText' =>__d('user', 'User.active'),
//                                    'inputType' => 'checkbox',
//                                    'items'=>array(
//                                        'type'=>'checkbox',
//                                        'checked'=>$this->request->data['User']['active']
//                                    )
//                                 );
//                                 $formUserEdit['input']['User.addressbook_private'] = array(
//                                     'labelText' =>__d('user', 'User.addressbook_private'),
//                                     'inputType' => 'checkbox',
//                                     'items'=>array(
//                                         'type'=>'checkbox',
//                                         'checked'=>$this->request->data['User']['addressbook_private']
//                                     )
//                                 );
//                             }
//                             if( $kInfo == 'addressbook_private' ) {
//
//                             }
//                         }
                        echo $infoList = $this->Formulaire->createForm($formUserEdit);
                        ?>
                    </div>
                    <div class="panel-footer " role="group">
                        <a id="userInfoSaveBttn" class="btn btn-success " title ="<?php echo __d('default', 'Button.save'); ?>"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
                        <a id="userInfoEditPwdBttn" class="btn btn-info-webgfc  " title ="<?php echo __d('user', 'Change password'); ?>"><i class="fa fa-lock" aria-hidden="true"></i></a>
                        <a id="userInfoEditNotifBttn" class="btn btn-info-webgfc  " title ="<?php echo __d('user', 'Change notif'); ?>"><i class="fa fa-bell" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div id="hablitations" class="tab-pane fade" >
            <div id="panel" class="col-sm-12 habilsZone">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function checkDesktopNameUnique(form) {
        gui.request({
            url: "/users/ajaxformvalid/desktopname",
            data: $(form).serialize()
        }, function (data) {
            processJsonFormCheck(data);
        });
    }

    function loadHabilis() {
        $('.habilsZone').empty();
        $('#modalRD').hide();
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/users/habilPerson/" .$user['User']['id'] ; ?>",
            updateElement: $('.habilsZone'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

    loadHabilis();

    $('#UserUsername').blur(function () {
        gui.request({
            url: "/users/ajaxformvalid/username",
            data: $('#UserEditForm').serialize()
        }, function (data) {
            processJsonFormCheck(data);
        })
    });
    $('#UserMail').blur(function () {
        gui.request({
            url: "/users/ajaxformvalid/mail",
            data: $('#UserEditForm').serialize()
        }, function (data) {
            processJsonFormCheck(data);
        })
    });

    $('#userInfoSaveBttn').button().click(function () {
        if (form_validate($('#UserEditForm'))) {
            gui.request({
                url: $('#UserEditForm').attr('action'),
                data: $('#UserEditForm').serialize(),
                loader: true,
                loaderElement: $('#UserEditForm'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                getJsonResponse(data);
                $('.loader').remove();
                loadStruct();
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }
    });

    // Action permettant à l'administrateur de modifier le mot de passe de l'utilisateur
    $('#userInfoEditPwdBttn').button().click(function () {
        gui.request({
            url: '/users/changemdp/<?php echo $user['User']['id']; ?>',
            updateElement: $('.infoPerson '),
            loader: true,
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $("#infoPerson").hide();
            $(".infoZone").append(data);
        });
    });


    // Action permettant à l'administrateur de modifier les abonnements aux notificatioons
    $('#userInfoEditNotifBttn').button().click(function () {
        gui.request({
            url: '/users/changenotif/<?php echo $user['User']['id']; ?>',
            updateElement: $('.infoPerson '),
            loader: true,
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $("#infoPerson").hide();
            $(".infoZone").append(data);
        });
    });

</script>
