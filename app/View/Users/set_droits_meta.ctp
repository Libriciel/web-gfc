<?php

/**
 *
 * Users/set_droits_meta.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$this->Droits->printTabCRUD($droitsmeta, '', 'Metadonnées', array('update' => true, 'read' => true));
$formUserMetaRights = array(
    'name' => 'UserMetaRights',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-6',
    'form_url'=>array('controller' => 'users', 'action' => 'setDroitsMeta', $id),
    'input' => array(
        'User.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'User.rights' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value'=>'data'
            )
        )
    )
);
echo $this->Formulaire->createForm($formUserMetaRights);
echo $this->Form->end();
?>

<script type="text/javascript">
    $('#UserMetaRightsSetDroitsMetaForm table tr').each(function () {
        var firstChk = $('td:nth-child(2) input[type="checkbox"]', this);
        var secondChk = $('td:nth-child(3) input[type="checkbox"]', this);

        if (secondChk.prop("checked")) {
            firstChk.prop("checked", true);
        }
        if (typeof firstChk.attr('checked') == "undefined") {
            secondChk.prop("checked", false);
        }
        secondChk.change(function () {
            if ($(this).prop("checked")) {
                firstChk.prop("checked", true);
            }
        });
        firstChk.change(function () {
            if (typeof $(this).attr('checked') == "undefined") {
                secondChk.prop("checked", false);
            }
        });

    });
</script>
