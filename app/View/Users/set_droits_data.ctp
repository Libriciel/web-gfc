<?php

/**
 *
 * Users/set_droits_data.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$this->Droits->printTabCRUD($droitsdata, 'Types', 'Soustypes', array('read' => true));
$formUserSoustypeRights = array(
    'name' => 'UserSoustypeRights',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-6',
    'form_url'=>array('controller' => 'users', 'action' => 'setDroitsMeta', $id),
    'input' => array(
        'User.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'User.rights' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value'=>'data'
            )
        )
    )
);
echo $this->Formulaire->createForm($formUserSoustypeRights);
echo $this->Form->end();
?>
