<?php

/**
 *
 * Users/searchuser.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php // echo $this->Html->css(array('recherche'), null, array('inline' => false)); ?>
<script language="javascript">

    //au click on fait apparetre le select correspondant
    function affectcritere(id) {
        $('#' + id + 'Crit').show();
    }
    //losqu'on click sur l'image on ferme le select correspondant
    function fermer(id) {
        $('#' + id).hide();
    }



    function fillCollectiviteSelect(id) {
        $.ajax({
            async: true,
            beforeSend: function (XMLHttpRequest) {
                $("#busy-indicator").fadeIn();
            },
            complete: function (XMLHttpRequest, textStatus) {
                $("#busy-indicator").fadeOut();
            },
            dataType: "html",
            success: function (data, textStatus) {
                $("#CollectiviteSelect").html(data);
            },
            url: "<?php echo Configure::read('BaseUrl'); ?>/collectivites/fillOptionList/" + id
        });
    }


<?php if ($superadmin): ?>
    fillCollectiviteSelect(0);
<?php endif; ?>
</script>
<?php if ($superadmin):?>
<select id="CollectiviteSelect"></select>
<?php endif; ?>

<div id="recherchePanels">
    <h4><?php echo __d('recherche', 'Recherche.choisircriteres'); ?></h4>
    <span onClick="affectcritere(this.id);" id="Ident"><?php echo __d('recherche', 'Identifiant'); ?></span><br/><br/>
    <span onClick="affectcritere(this.id);" id="Profil"><?php echo __d('profil', 'Profil'); ?></span><br/><br/>
    <span onClick="affectcritere(this.id);" id="Service"><?php echo __d('service', 'Service'); ?></span><br/><br/>
    <span onClick="affectcritere(this.id);" id="Date"><?php echo __d('recherche', 'Courrier.datecreation'); ?></span><br/>
</div>
<div id="recherchecritere" class="panels">
<?php echo $this->Form->create('User', array('controller' => 'users', 'action' => 'searchuser')); ?>
    <table>

        <tr>
            <td id="Crit">
                <div id="IdentCrit" class="crit">
                    <legend><?php echo __d('recherche', 'Identifiants'); ?></legend>
                    <?php
                    echo $this->Form->text('User.ident', array('label' => false));
                    echo $this->jmycake->autocomplete('UserIdent', 'User/username');
                    echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("IdentCrit");'));
                    ?>
                </div>
                <div id="ProfilCrit" class="crit">
                    <legend><?php echo __d('profil', 'Profil'); ?></legend>
                    <?php
                    echo $this->Form->select('User.groups', $groups);
                    echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("ProfilCrit");'));
                    ?>
                </div>
                <div id="ServiceCrit" class="crit">
                    <legend><?php echo __d('service', 'Services'); ?></legend>
                    <?php
                    echo $this->Form->select('User.services', $services);
                    echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("ServiceCrit");'));
                    ?>
                </div>
                <div id="DateCrit" class="crit">
                    <legend><?php echo __d('recherche', 'Courrier.datecreation'); ?></legend>
                    <?php
                    echo $this->Form->input('User.date', array('type' => 'date', 'label' => false, 'empty' => true, 'dateFormat' => 'DMY'));
                    echo $this->Html->image('icons/croixrouge.png', array('onClick' => 'fermer("DateCrit");'));
                    ?>
                </div>
            </td>

        </tr>
    </table>
	<?php echo $this->Form->end(__d('default', 'Button.search')); ?>
</div>

<div id="results">
	<?php if (empty($resuls)) { ?>

    <h2><?php echo __d('recherche', 'Recherche.void'); ?></h2>

	<?php } else { ?>
    <table id="usersTable" class="tablesorted">
        <thead>
        <th class="Identifiant"><?php echo __d('user', 'User.username'); ?></th>
        <th class="nom"><?php echo __d('user', 'User.nom'); ?></th>
        <th class="prenom"><?php echo __d('user', 'User.prenom'); ?></th>

        <th class="actions" colspan="3"><?php echo __d('default', 'Actions'); ?></th>


        </thead>
        <tbody>
            <tr>
					<?php foreach ($resuls as $resul) { ?>

                <td><?php echo $resul["User"]["username"]; ?></td>
                <td><?php echo $resul["User"]["nom"]; ?></td>
                <td><?php echo $resul["User"]["prenom"]; ?></td>

                <td class="action"><?php echo $this->Html->image('icons/modifier.png', array('height' => '24px', 'width' => '24px', 'title' => __d('default', 'Button.edit'), 'alt' => __d('default', 'Button.edit'))); ?></td>

            </tr>
				<?php } ?>
        </tbody>
    </table>

</div>
<?php } ?>

