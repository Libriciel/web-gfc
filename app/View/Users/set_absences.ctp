<?php

/**
 *
 * Users/set_absences.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php // echo $this->Html->css(array('zones/user/moncompte'), null, array('inline' => false)); ?>

<table id="abs_skel">
    <tr>
        <td id="liste">
            <div class="title">
                <?php echo __d('menu', 'Gestion des absences'); ?>
            </div>
            <div class="content">
                <?php
                $formUserGestionAbsences = array(
                    'name' => 'User',
                    'label_w' => 'col-sm-6',
                    'input_w' => 'col-sm-6',
                    'form_id' =>'UserAbsenceGestionForm',
                    'input' => array(
                        'User.id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden'
                            )
                        ),
                        'User.delegue' => array(
                            'labelText' =>__d('user', 'User.delegue'),
                            'inputType' => 'select',
                            'items'=>array(
                                'required'=>true,
                                'type'=>'select',
                                'options' => $users,
                                'empty' => true
                            )
                        )
                    )
                );
                echo $this->Formulaire->createForm($formUserGestionAbsences);
                echo $this->Form->end();
                ?>
            </div>
            <div class="ui-widget ui-widget-content ui-corner-all alert alert-warning">En développement ...</div>
            <div class="controls">

            </div>
        </td>
        <td id="infos">
            <div class="title">
                <?php echo __d('menu', 'Planification des absences'); ?>
            </div>
            <div class="content">
                <?php
                $formUserPlanificationAbsences = array(
                    'name' => 'User',
                    'label_w' => 'col-sm-6',
                    'input_w' => 'col-sm-6',
                    'form_id' =>'UserAbsencePlanificationForm',
                    'input' => array(
                        'User.id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden'
                            )
                        ),
                        'User.date_dep' => array(
                            'labelText' =>__d('user', 'User.date_dep'),
                            'inputType' => 'text',
                            'items'=>array(
                                'required'=>true,
                                'type'=>'text',
                            )
                        ),
                        'User.date_ret' => array(
                            'labelText' =>__d('user', 'User.date_ret'),
                            'inputType' => 'text',
                            'items'=>array(
                                'required'=>true,
                                'type'=>'text',
                            )
                        ),
                        'User.delegue' => array(
                            'labelText' =>__d('user', 'User.delegue'),
                            'inputType' => 'select',
                            'items'=>array(
                                'required'=>true,
                                'type'=>'select',
                                'options' => $users,
                                'empty' => true
                            )
                        )
                    )
                );
                echo $this->Formulaire->createForm($formUserPlanificationAbsences);
                echo $this->Form->end();
                ?>
            </div>
            <div class="ui-widget ui-widget-content ui-corner-all alert alert-warning">En développement ...</div>
            <div class="controls">

            </div>
        </td>
    </tr>
</table>
<script type="text/javascript">

    $('.title').addClass('ui-widget-header-webgfc ui-state-default ui-corner-top');
    $('.content').addClass('ui-widget ui-widget-content ui-corner-all');

    gui.buttonbox({
        element: $('#liste .controls')
    });

    gui.addbuttons({
        element: $('#liste .controls'),
        buttons: [
            {
                content: "Départ",
                action: function () {
                    if (form_validate($('#UserAbsenceGestionForm'))) {
                        gui.request({
                            url: "/users/setAbsences/away",
                            data: $('#UserAbsenceGestionForm').serialize(),
                            loader: true,
                            loaderElement: $('#abs_skel'),
                            loaderMessage: gui.loaderMessage
                        }, function () {
                            window.location.reload();
                        });
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            },
            {
                content: "Retour",
                action: function () {
                    if (form_validate($('#UserAbsenceGestionForm'))) {
                        gui.request({
                            url: "/users/setAbsences/present",
                            data: $('#UserAbsenceGestionForm').serialize(),
                            loader: true,
                            loaderElement: $('#abs_skel'),
                            loaderMessage: gui.loaderMessage
                        }, function () {
                            window.location.reload();
                        });

                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        ]
    });

    gui.buttonbox({
        element: $('#infos .controls')
    });

    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: 'Planifier',
                action: function () {
                    if (form_validate($('#UserAbsencePlanificationForm'))) {
                        gui.request({
                            url: "/users/setAbsences/plan",
                            data: $('#UserAbsencePlanificationForm').serialize(),
                            loader: true,
                            loaderElement: $('#abs_skel'),
                            loaderMessage: gui.loaderMessage
                        }, function () {
                            window.location.reload();
                        });
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            },
            {
                content: 'Supprimer la planification',
                action: function () {
                    if (form_validate($('#UserAbsencePlanificationForm'))) {
                        gui.request({
                            url: "/users/setAbsences/unplan",
                            data: $('#UserAbsencePlanificationForm').serialize(),
                            loader: true,
                            loaderElement: $('#abs_skel'),
                            loaderMessage: gui.loaderMessage
                        }, function () {
                            window.location.reload();
                        });
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        ]
    });

    gui.datepicker({
        element: $('#UserDateDep'),
        image: "/img/date.png"
    });
    gui.datepicker({
        element: $('#UserDateRet'),
        image: "/img/date.png"
    });
</script>

