<?php

/**
 *
 * Users/set_absences.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div id="habilsPerson">
    <div class="panel-body" id="desktopsList">
        <div id="roleExiste">
            <div class="col-sm-6 firstDesktop desktopElement desktopElement_<?php echo $user['Desktop']['id'] . (in_array($user['Desktop']['id'], $delegateToDesktops) ? ' delegateTo' : ''); ?>" desktopId="<?php echo $user['Desktop']['id']; ?>">
                <legend><?php echo __d('desktop', 'Desktop.priDesktop'); ?></legend>
                <?php echo $this->element('dktpEditBttn', array('deletable' => false,'editable'=>($user['Desktop']['profil_id']!=2)? true: false, 'desktopId' => $user['Desktop']['id'])); ?>
                <div style="margin-left:35px"><?php echo $user['Desktop']['name']; ?></div>
            </div>
            <?php
                $secDesktopsListContent = '';
                if (!empty($user['SecondaryDesktop'])) {
                        foreach ($user['SecondaryDesktop'] as $desktop) {
                        $delegated = false;
                        if(in_array($desktop['id'], $delegatedDesktops)) {
                            $delegated = true;
                        }
                        $delegatedTo = false;
                        if(in_array($desktop['id'], $delegateToDesktops)) {
                            $delegatedTo = true;
                        }
                                $secDesktopsListContent.= $this->Html->tag('li', $this->element('dktpEditBttn', array( 'editable'=>($desktop['profil_id']!=2)? true: false,'delegated' => $delegated, 'delegatedTo' => $delegatedTo, 'deletable' => true, 'desktopId' => $desktop['id'], 'userId' => $user['User']['id'])) . $this->Html->tag('div', $desktop['name']), array('class' => 'desktopElement desktopElement_' . $desktop['id'] . (in_array($desktop['id'], $delegatedDesktops) ? ' delegated' : '') . (in_array($desktop['id'], $delegateToDesktops) ? ' delegateTo' : ''), 'desktopId' => $desktop['id']));
                        }
                }

                if ($secDesktopsListContent == '') {
                        $secDesktopsListContent = __d('desktop', 'secDesktop.void');
                }
            ?>
            <div class="col-sm-6">
                <legend><?php echo __d('desktop', 'Desktop.secDesktops'); ?></legend>
            <?php echo $this->Html->tag('ul', $secDesktopsListContent); ?>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <a id="DesktopAddBttn" class="btn btn-info-webgfc" title="<?php echo __d('desktop', 'Desktop.add'); ?>"><i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter un profil</a>
    </div>
</div>
<div id="modalRD" class="divRD">
    <div id="divRD" class="divRD"></div>
    <div id="divEditRD" class="divEditRD"></div>
    <div id="divDeleRD" class="divDeleRD"></div>
</div>
<script>

    $('#modalRD').hide();

<?php if( Configure::read( 'Afficher.HabilitationsUser')):?>
//    gui.request({
//        url: "/users/rights/" + <?php echo $user['User']['id']; ?>
//    }, function (data) {
//        $('#divRD').html(data);
//    });
<?php endif;?>

    $('#DesktopAddBttn').button().click(function () {
        gui.request({
            url: '/desktops/add/<?php echo $user['User']['id']; ?>',
            updateElement: $('#desktopsList'),
            loader: true,
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $('#habilsPerson').hide();
            $('.habilsZone').append(data);
        });
    });

    $('#show_habilitation').click(function () {
        if (!$('#modalRD').is(":visible")) {
            $('#habilsPerson').hide();
            $('.habilsZone').append($('#modalRD').show());
            $('.habilsZone').show();
        }
    });

    $('.setRights').each(function () {
        var img = $('img', $(this));
        var link = $('a', $(this));
        var url = link.attr('href');
        $(this).append(img);
        link.remove();
        $(this).click(function () {
            gui.request({
                url: url,
                updateElement: $('#divEditRD'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
            if (!$('#modalRD').is(":visible")) {
                $('#habilsPerson').hide();
                $('.habilsZone').append($('#divEditRD').show());
            }



        });
    });

    $('.setDeleg').each(function () {
        var img = $('img', $(this));
        var link = $('a', $(this));
        var url = link.attr('href');
        $(this).append(img);
        link.remove();
        $(this).click(function () {
            var dataString = "admin=admin";
            gui.request({
                url: url,
                data: dataString,
                updateElement: $('#divDeleRD'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
            if (!$('#modalRD').is(":visible")) {
                $('#habilsPerson').hide();
                $('.habilsZone').append($('#divDeleRD').show());
            }
        });
    });

    $('.desktopDelete').each(function () {
        var img = $('img', $(this));
        var link = $('a', $(this));
        var url = link.attr('href');

        $(this).append(img);
        link.remove();
        $(this).click(function () {
            swal({
                    showCloseButton: true,
                title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
                text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
                cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
            }).then(function (data) {
                if (data) {
                    gui.request({
                        url: url,
                        loaderElement: $('.ui-dialog-content'),
                        loader: true,
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        getJsonResponse(data);
                        gui.request({
                            url: '/users/edit/<?php echo $user['User']['id']; ?>',
                            loader: false
                        }, function (data) {
//                            $('.ui-dialog-content').html(data);
                            $('.loader').remove();
                            loadHabilis();
                        });
                    });
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n'avez pas supprimé, ;) .",
                        type: "error",

                    });
                }
            });
            $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
        });
    });


    $('.desktopEdit').each(function () {
        var img = $('img', $(this));
        var link = $('a', $(this));
        var url = link.attr('href');
        $(this).append(img);
        link.remove();
        $(this).click(function () {
            gui.request({
                url: url,
                updateElement: $('#desktopsList'),
                loader: true,
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#habilsPerson').hide();
                $('.habilsZone').append(data);
            });
        });
    });

    $('.desktopSetMain').each(function () {
        var img = $('img', $(this));
        var link = $('a', $(this));
        var url = link.attr('href');

        $(this).append(img);
        $(this).click(function () {
            gui.request({
                url: url,
                loaderElement: $('#desktopsList'),
                loader: true,
                loaderMessage: gui.loaderMessage
            }, function (data) {
                gui.request({
                    url: '/users/edit/<?php echo $user['User']['id']; ?>',
                    loader: false
                }, function (data) {
                    $('.ui-dialog-content').html(data);
                    $('.loader').remove();
                });
            });
        });
    });
</script>
