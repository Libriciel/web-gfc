<?php

/**
 *
 * Users/login.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');

    $cssFiles = array(
        'select2'
    );
    echo $this->Html->css($cssFiles);



    $jsFiles = array(
        'select2' //Arnaud
    );
    if (Configure::read('debug') > 0) {
        $jsFiles[] = 'debugTools';
    }
    echo $this->Html->script($jsFiles);
    echo $this->fetch('script');
?>
<div class="chooseLoginBox">
    <h3 class="ui-widget-header-webgfc ui-state-default ui-corner-top"><?php echo __d('user', 'User.chooseCollForCas').' : '.$username; ?></h3>
    <div class="ui-widget ui-widget-content ui-corner-bottom ui-widget-webgfc">
        <?php
                $formUser = array(
                    'name' => 'User',
                    'label_w' => 'col-sm-6',
                    'input_w' => 'col-sm-6',
                    'form_url' => array('controller' => 'users', 'action' => 'chooseCollForCas'),
                    'input' => array(
                        'User.collectivite' => array(
                            'labelText' =>__d('user', 'User.collectivite'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'options' => $conns,
                                'empty' => true
                            )
                        )
                    )
                );
                echo $this->Formulaire->createForm($formUser);
                echo $this->Form->end();
        ?>

    </div>
    <div id="connexionButtonBox"></div>
</div>

<script type="text/javascript">
    $('#UserCollectivite').select2({allowClear: true, placeholder: "Sélectionner une collectivité"});

    $('#UserChooseCollForCasForm').validate({
        'rules': {
            'data[User][collectivite]': 'required'
        }
    });

    gui.buttonbox({
        element: $('#connexionButtonBox')
    });

    gui.addbutton({
        element: $('#connexionButtonBox'),
        button:
            {
                content: "<?php echo __d('default', 'Button.login'); ?>",
                action: function () {
                    if ($("#UserChooseCollForCasForm").valid()) {
                        $('#UserChooseCollForCasForm').submit();
                    }
                }
            }
    });
</script>
