<?php

/**
 *
 * Users/get_rights.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
//init options
$options = array(
	'no' => $this->Html->image('/DataAcl/img/test-fail-icon.png', array('alt' => 'no')),
	'yes' => $this->Html->image('/DataAcl/img/test-pass-icon.png', array('alt' => 'yes')),
    'parentNodeMarkClose' => '<i class="fa fa-minus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
    'parentNodeMarkOpen' => '<i class="fa fa-plus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
	'edit' => false,
	'tableRootClass' => 'ui-widget ui-widget-content ui-corner-all',
	'legendClass' => '',
	'fieldsetClass' => 'ui-widget ui-widget-content ui-corner-all',
	'fieldsetStyle' => '',
	'legendStyle' => '',
	'requester' => $requester
);

if ($mode == 'func') {
	if (Configure::read('debug') >= 2) {
//		echo $this->Html->tag('span', __d('right', 'mode_classique'), array('class' => 'modeClassiqueBttn btn btn-info-webgfc'));
	}
	foreach ($rights as $desktopName => $right) {
		if ($right['Right']['mode_classique']) {
			echo $this->Html->div('alert alert-warning', __d('right', 'mode_classique.active'));
		}
                ?><legend ><?php echo __d('desktop', 'Desktop.Desktop'); ?> : <?php echo $desktopName; ?></legend><?php
		echo $this->Element('rights', array('rights' => $right, 'edit' => false));
	}
} else if ($mode == 'func_classique') {
	echo $this->Html->tag('span', __d('right', 'mode_simplifie'), array('class' => 'modeSimplifieBttn'));
	//show func rigths
	$options['fields'] = array(
		'read' => 'accès'
	);
	$options['fieldsetLegend'] = 'Fonctions';
	$options['aclType'] = 'Acl';
	$options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_rights';
	$this->DataAcl->drawTable($rights, true, $options);
} else if ($mode == 'data') {
//show data rigths
	$options['aclType'] = 'DataAcl';
	$options['fields'] = array(
		'read' => 'accès'
	);
	for ($i = 0; $i < count($desktops); $i++) {
		echo $this->Html->tag('legend', __d('desktop', 'Desktop.Desktop') . " : " . $desktops[$i]['name']);
		$dataRights = $desktops[$i]['dataRights'];
		$servicesNames = $desktops[$i]['servicesNames'];
		foreach ($dataRights as $desktopService => $rights) {
			$requester = array('model' => 'DesktopsAgent', 'foreign_key' => $desktopService);
			$options['requester'] = $requester;
			$options['fieldsetLegend'] = 'Service : ' . $servicesNames[$desktopService];
			$options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_data';
			$this->DataAcl->drawTable($rights, false, $options);
		}
	}
} else if ($mode == 'meta') {
//show meta rigths
	$options['aclType'] = 'DataAcl';
	$options['fields'] = array(
		'create' => 'création',
		'read' => 'lecture',
		'update' => 'édition',
		'delete' => 'suppression'
	);

	for ($i = 0; $i < count($desktops); $i++) {
		echo $this->Html->tag('legend', __d('desktop', 'Desktop.Desktop') . " : " . $desktops[$i]['name']);
		$metaRights = $desktops[$i]['metaRights'];
		$servicesNames = $desktops[$i]['servicesNames'];
		foreach ($metaRights as $desktopService => $rights) {
			$requester = array('model' => 'DesktopsAgent', 'foreign_key' => $desktopService);
			$options['requester'] = $requester;
			$options['fieldsetLegend'] = 'Service : ' . $servicesNames[$desktopService];
			$options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_meta';
			$this->DataAcl->drawTable($rights, false, $options);
		}
	}
//// ajout des infos si user dnas un circuit
} else if ($mode == 'circuit') {
//debug( $desktops );

//	for ($i = 0; $i < count($desktops); $i++) {
//	foreach( $desktops as $i => $desktop ) {
//		echo $this->Html->tag('h3', __d('desktop', 'Desktop.Desktop') . " : " . $desktops[$i]['name'], array('class' => 'ui-state-focus ui-corner-all', 'style' => 'padding: 5px;'));


        if( !empty( $desktops['circuitName'] ) ) {
            foreach( $desktops['circuitName'] as $desktopmanagerId => $circuits ) {
                echo $this->Html->tag('legend', __d('desktop', 'Desktop.Desktop') . " : " . $circuits['name']);

                echo "<div><legend>Liste des circuits auxquels le bureau est associé</legend>";
                echo "<table>";
                echo '<tbody>';
//debug($circuits);
                foreach ($circuits as $cId => $names) {
                    if( $circuits['multiple'] == false ) {
                        if( is_int($cId) ) {
                            echo '<tr>';
                            echo '<td >'. $names .'</td>';
                            echo '<td >'. $this->Html->tag('span', $this->Html->image('/img/edit-cut_22.png', array('class' => 'cutCircuits', 'alt' => 'Dissocier le circuit', 'title' => __d('user', 'User.cutCircuit'), 'url' => '/users/cutCircuit/' . $cId . '/'. $desktopmanagerId ))) .'</td>';
                            echo '</tr>';
                        }
                    }
                    else {
                        if( is_int($cId) ) {
                            echo '<tr>';
                            echo '<td >'. $names .'</td>';
                            echo '<td >'. $this->Html->tag('span', $this->Html->image('/img/edit-cut_22_desactivated.png', array('class' => 'uncutCircuits', 'alt' => __d('user', 'User.uncutCircuit') , 'title' => __d('user', 'User.uncutCircuit') ))) .'</td>';
                            echo '</tr>';
                        }
                    }
                }
                if( count( array_keys( $circuits ) ) == 2 ) {
                    echo $this->Html->tag( 'p', 'Aucun circuit associé à ce bureau', array( 'class' => 'alert alert-warning' ) );
                }
                echo '</tbody>';
                echo '</table>';
                echo "</div>";
            }
        }
        else {
            echo $this->Html->tag( 'p', 'Aucun circuit associé à cet utilisateur', array( 'class' => 'alert alert-warning' ) );
        }
}
?>

<script type="text/javascript">
    initTables(true);

<?php if ($mode == 'func' && Configure::read('debug') >= 2) { ?>
    $('.modeClassiqueBttn').button().click(function () {
        var cpt = 0;
        $('#rightsTabs a').each(function () {
            if (cpt == 2) {
                var href = $.data(this, 'href.tabs');
                href = href.replace('func', 'func_classique');
                $('#rightsTabs').tabs('url', cpt, href);
            }
            cpt++;
        });
        $('#rightsTabs').tabs('load', 2);
    });
<?php } else if ($mode == 'func_classique') { ?>
    $('.modeSimplifieBttn').button().click(function () {
        var cpt = 0;
        $('#rightsTabs a').each(function () {
            if (cpt == 2) {
                var href = $.data(this, 'href.tabs');
                href = href.replace('func_classique', 'func');
                $('#rightsTabs').tabs('url', cpt, href);
            }
            cpt++;
        });
        $('#rightsTabs').tabs('load', 2);
    });
<?php } ?>

    function refreshSchema() {
        $("#rightsTabs").tabs('load', 3);
    }
<?php if ($mode == 'circuit' ) { ?>
    $('.cutCircuits').click(function () {
        var container = $(this).parent();
        var url = container.attr('href');

        var form = $('form', $(this).parents('.ui-tabs-panel'));

        swal({
                    showCloseButton: true,
            title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
            text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
            cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: url,
                    data: form.serialize()
                }, function (data) {
                    getJsonResponse(data);
                    refreshSchema();
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n'avez pas supprimé, ;) .",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    });
<?php }?>
</script>
