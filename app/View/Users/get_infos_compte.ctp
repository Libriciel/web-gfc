<?php

/**
 *
 * Users/get_infos_compte.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
if (!$reloadDesktops) {

//	echo $this->Html->css(array('zones/user/infoscompte'), null, array('inline' => false));
//	echo $this->Html->css(array('zones/delegation/index'), null, array('inline' => false));
	?>
<script type="text/javascript">
    function reloadDelegDependances() {
        loadDeleg();
        gui.request({
            url: "/users/getInfosCompte/1",
            loader: true,
            updateElement: $('#iuContent'),
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $('#iuContent').html(data);
        });
    }
</script>
<div class="container" id="getInfosCompteContainer">
    <div class="row">
        <div class="container-formulaire">
            <h3><?php echo __d('user', 'User.staticInfos'); ?></h3>

            <div class="row">

                <!--role principal-->
            <?php
            $delegationImgIcon = '';
//            if( $okicondelegate ) {
                if (in_array(Set::classicExtract($user, 'Desktop.id'), $delegatedDesktops)) {
                    $delegationImgIcon .= $this->Html->image('/img/delegated_16.png', array('alt' => __d('desktop', 'Desktop.delegated'), 'title' => __d('desktop', 'Desktop.delegated')));
                }
                if (in_array(Set::classicExtract($user, 'Desktop.id'), $delegateToDesktops)) {
                    $delegationImgIcon .= $this->Html->image('/img/delegto_16.png', array('alt' => __d('desktop', 'Desktop.delegateTo'), 'title' => __d('desktop', 'Desktop.delegateTo')));
                }
//            }
//            else {
//                $delegationImgIcon = '';
//            }
//            $delegationImg = $this->Html->tag('span', $this->Html->image('/img/deleg_16.png', array('class' => 'setDeleg btn btn-info-webgfc', 'url' => '/desktops/setDeleg/' . $user['Desktop']['id'], 'alt' => __d('deleg', 'Deleg.new'), 'title' => __d('deleg', 'Deleg.new'))). ' Défini' );
//            $delegationImg = ;
            ?>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo __d('user', 'User.mainDesktop'); echo  '<div class="icon-title">'.Set::classicExtract($user, 'Desktop.name').' ' .$delegationImgIcon.'</div>';?></h4>
                        </div>
                        <div class="panel-body form-horizontal">
                            <div class="row">
                                <div class="label col-sm-3"><?php echo __d('desktop', 'Desktop.name'); ?></div>
                                <div class="contentInto col-sm-9"><?php echo Set::classicExtract($user, 'Desktop.name'); ?></div>
                            </div>
                            <div class="row">
                                <div class="label col-sm-3"><?php echo __d('profil', 'Profil.name'); ?></div>
                                <div class="contentInto col-sm-9"><?php echo Set::classicExtract($user, 'Desktop.Profil.name'); ?></div>
                            </div>
                            <div class="row">
                                <div class="label col-sm-3"><?php echo __d('desktop', 'Desktop.Services.name'); ?></div>
                                <div class="contentInto col-sm-9"><?php echo $this->Html->nestedList(Set::classicExtract($user, 'Desktop.Service.{n}.name')); ?></div>
                            </div>
                        </div>
                        <div class="panel-footer" >
                            <a class="btn btn-primary" href="/desktops/setDeleg/<?php echo $user['Desktop']['id'];?>"><i class="btn btn-info-webgfc setDeleg fa fa-share-alt" aria-hidden="true"> Définir une délégation</i> </a>

                        <?php /*echo $delegationImg; */?>
                        </div>
                    </div>
                </div>
            <?php
            foreach ($user['SecondaryDesktop'] as $desktop) {
                    $class = '';
                    $secondaryDesktopTitle = '';
                    if (in_array(Set::classicExtract($desktop, 'id'), $baseDesktops)) {
                        $secondaryDesktopTitle .= __d('user', 'User.secondaryDesktop');
                    }
                    if (in_array(Set::classicExtract($desktop, 'id'), $delegateToDesktops)) {
                        $secondaryDesktopTitle .= __d('user', 'User.secondaryDelegateToDesktop.short');
                    }
               $classPanel = ' panel-default';
                    if (in_array(Set::classicExtract($desktop, 'id'), $delegatedDesktops)) {
//                   $class= 'style="background-color:lightyellow;"';
                   $classPanel= ' panel-warning';
                        $secondaryDesktopTitle = __d('user', 'User.secondaryDelegatedDesktop');
                    }
                     $delegationImgIcon = '';
    //               if( $desktop['DesktopsUser']['delegation'] == true ) {
                    if (in_array(Set::classicExtract($desktop, 'id'), $delegatedDesktops)) {
                        $delegationImgIcon .= $this->Html->image('/img/delegated_16.png', array('alt' => __d('desktop', 'Desktop.delegated'), 'title' => __d('desktop', 'Desktop.delegated')));
                    }
                    if (in_array(Set::classicExtract($desktop, 'id'), $delegateToDesktops)) {
                        $delegationImgIcon .= $this->Html->image('/img/delegto_16.png', array('alt' => __d('desktop', 'Desktop.delegateTo'), 'title' => __d('desktop', 'Desktop.delegateTo')));
                    }
    //               }
                    $delegationImg = $this->Html->image('/img/deleg_16.png', array('class' => 'setDeleg btn btn-info-webgfc', 'url' => '/desktops/setDeleg/' . $desktop['id'], 'alt' => __d('deleg', 'Deleg.new'), 'title' => __d('deleg', 'Deleg.new')));
//                    $delegationImg = $this->Html->tag('span', $this->Html->tag('div', '<img src="/img/deleg_16.png" class="setDeleg btn btn-info-webgfc"> Définir une délégation',array( 'url' => '/desktops/setDeleg/' . $desktop['id'],  'alt' => __d('deleg', 'Deleg.new'), 'title' => __d('deleg', 'Deleg.new') )));
                    ?>
                <div class="col-sm-4">
                    <div class="panel <?php echo $classPanel;?>">
                        <div class="panel-heading">
                            <h4 class="panel-title " ><?php echo $secondaryDesktopTitle; echo '<span class="icon-title">'.Set::classicExtract($desktop, 'name'). ' ' .$delegationImgIcon.'</span>';?></h4>
                        </div>
                        <div class="panel-body form-horizontal">
                            <div class="row">
                                <div class="label col-sm-3"><?php echo __d('desktop', 'Desktop.name'); ?></div>
                                <div class="contentInto col-sm-9"><?php echo Set::classicExtract($desktop, 'name'); ?></div>
                            </div>
                            <div class="row">
                                <div class="label col-sm-3"><?php echo __d('profil', 'Profil.name'); ?></div>
                                <div class="contentInto col-sm-9"><?php echo Set::classicExtract($desktop, 'Profil.name'); ?></div>
                            </div>
                            <div class="row">
                                <div class="label col-sm-3"><?php echo __d('desktop', 'Desktop.Services.name'); ?></div>
                                <div class="contentInto col-sm-9"><?php echo $this->Html->nestedList(Set::classicExtract($desktop, 'Service.{n}.name')); ?></div>
                            </div>
                        </div>
                        <div class="panel-footer"  id="chNotif">
                            <a class="btn btn-primary" href="/desktops/setDeleg/<?php echo $desktop['id'];?>"><i class="btn btn-info-webgfc setDeleg fa fa-share-alt" aria-hidden="true"> Définir une délégation</i> </a>
                        </div>
                    </div>
                </div>

                <?php

            }
        ?>
            </div>

        </div>
    </div>
</div>



<?php
}
?>
<script type="text/javascript">
    $('.desktop').hover(function () {
        $(this).addClass('alert alert-warning');
    }, function () {
        $(this).removeClass('alert alert-warning');
    });

    $('.getRights').remove();
    $('.setDeleg').each(function () {
        var img = $(this);
        var link = $(this).parent();
        var url = link.attr('href');
        var container = link.parent();
        var desktop = img.parents('.desktop');
//console.log(url);
        container.append(img);
        link.remove();

        img.button().click(function () {

            $('.desktop').removeClass('ui-state-focus');
            $(desktop).addClass('ui-state-focus');
            /*
             gui.request({
             url: url,
             loader: true,
             updateElement: $('#iuContent'),
             loaderMessage: gui.loaderMessage
             }, function (data) {
             */
            gui.formMessage({
                width: '450px',
                title: '<?php echo __d('deleg', 'Deleg.info'); ?>',
                url: url,
                loader: true,
                updateElement: $('body'),
                loaderMessage: gui.loaderMessage,
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        if (form_validate($('#DesktopsUserSetDelegForm'))) {
							var planified = $('#DesktopsUserPlanification').attr('checked');
							var planifieddatestart = $('#PlandelegationDateStart').val();
							var planifieddateend = $('#PlandelegationDateEnd').val();
							if( ( planified == 'checked' && planifieddatestart != '' && planifieddateend != '' ) || planified == undefined) {
								var updateElement = $('.divRD');
								if (updateElement.length == 0) {
									updateElement = $('.ui-dialog-content');
								}

								var url = $('#DesktopsUserSetDelegForm').attr('action');

								gui.request({
									url: url,
									data: $('#DesktopsUserSetDelegForm').serialize(),
									loader: true,
									updateElement: updateElement,
									loaderMessage: gui.loaderMessage
								}, function (data) {
									getJsonResponse(data);
									gui.request({
										url: url,
										loader: true,
										updateElement: updateElement,
										loaderMessage: gui.loaderMessage
									}, function (data) {
										window.location.href = "<?php echo Configure::read('BaseUrl') . "/users/getInfosCompte"; ?>";
										updateElement.html(data);
									});
								});
								$(this).parents('.modal').modal('hide');
								$(this).parents('.modal').empty();
							} else {
								swal({
									showCloseButton: true,
									title: "Oops...",
									text: "Veuillez vérifier votre formulaire!",
									type: "error",

								});
							}
						}
						else {
							swal({
								showCloseButton: true,
								title: "Oops...",
								text: "Veuillez vérifier votre formulaire!",
								type: "error",

							});
						}
                    }

                }
            });
            // });
        });
    });


</script>
