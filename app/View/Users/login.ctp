<?php

/**
 *
 * Users/login.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
echo $this->Html->script('appAttendable.js', array('inline' => true));
?>




<!-- Content -->

<div class="col-xs-10
     col-sm-7
     col-md-6
     col-lg-5
     center-block login-block">


    <?php if (Configure::read('Conf.logo')) :?>
        <?php if (Configure::read('Conf.SAERP')) :?>
            <div class="hidden-xs
                hidden-sm
                " style="background-color: #FAB500; border-right: solid #080808 1px; padding: 10px; display: flex; align-items: center;">
                <img class="center-block" width=250px; height=150px;
                     src="/files/logo/SAERP_LOGO.png">
            </div>
        <?php else:?>
            <div class="hidden-xs
                hidden-sm
                col-md-3
                col-lg-3
                collectivity-block" style="background-color: #c02e74;">
                <img class="collectivity-logo center-block"
                     src="/files/logo/LOGO.jpg">
            </div>

        <!--  GAILLAC-GRAULHET
                <div class="hidden-xs
                    hidden-sm
                    " style="background-color: #fff; border-right: solid #080808 1px; padding: 10px; display: flex; align-items: center;">
                    <img class="center-block" width=240px; height=80px;
                         src="/files/logo/AGGLO_LogoCartoucheRVB.png">
                </div>
            -->

        <?php endif;?>
    <?php endif;?>
                     <!--src="/files/logo/SAERP_LOGO.png"-->
    <div class="form-block">

        <img class="row center-block main-logo"
             src="/img/web-gfc_color_h48px.png">

        <div class="row buffer-top text-center">
            Veuillez saisir vos identifiants de connexion
        </div>
        <form class="row double-buffer-top" id="UserLoginForm" role="form" action="" method="post" id="UserLoginForm" data-toggle="validator">
            <div class="form-horizontal form-group">

                <label for="login"
                       class="col-md-4
                       control-label normal-left">Identifiant *</label>  <!-- font-family: Arial, serif; -->

                <div class="col-md-8 input-group">
                    <span class="input-group-addon color-inverse"
                          id="sizing-addon-login">
                        <i class="fa fa-user"
                           aria-hidden="true"></i>
                    </span>
                    <input type="text"
                           class="form-control"
                           id="login"
                           name="data[User][username]"
                           required="required"
                           aria-describedby="sizing-addon-login">
                </div>
            </div>

            <div class="form-horizontal form-group" id="form_group_password">
                <label for="password"
                       class="col-md-4 control-label normal-left">Mot de passe *</label>  <!-- font-family: Arial, serif; -->

                <div class="col-md-8 input-group">
                    <span class="input-group-addon color-inverse"
                          id="sizing-addon-password">
                        <i class="fa fa-lock"
                           aria-hidden="true"></i>
                    </span>
                    <input type="password"
                           class="form-control"
                           id="password"
                           name="data[User][password]"
                           required="required"
                           aria-describedby="sizing-addon-password">
                     <i class="fa fa-eye-slash unmask"
                           aria-hidden="true"></i>
                </div>
            </div>
        </form>

        <div class="row form-horizontal form-group buffer-top " id="loginButtonBox">
            <!--<a class="pull-right btn btn-primary"  id="login_logiciel"><i class="fa fa-sign-in fa-fw"></i> Se connecter</a>-->
            </div>

        </form>

        <?php if(!$motdepasseoublieHide && Configure::read('Affiche.Motdepasseoublie') ):?>
            <div class="row buffer-top text-center">
                <?php echo $this->Html->link( 'Mot de passe oublié ?', array( 'action' => 'mdplost' ));?>
                <!--<a href="#">Mot de passe oublié ?</a>-->
            </div>
            <div class="show_login_table"></div>
        <?php endif;?>
    </div>
</div>


<?php if (SHOW_LOGIN_TABLE) { ?>
<div id="comptesdemoDiv">
    <div class="ui-widget ui-widget-content ui-corner-bottom ui-widget-webgfc">
        <table id="comptesdemo">
            <thead>
            <th class="text-center" style="width:33%;">Login</th>
            <th class="text-center" style="width:33%;">Mot de passe</th>
            <th class="text-center" style="width:33%;">Profil</th>
            </thead>
            <tbody>
                <tr>
                        <td>SecretaireSC@demo</td>
                        <td class="text-center">user</td>
                        <td>Aiguilleur</td>
                </tr>
                <tr>
                        <td>BCI@demo</td>
                        <td class="text-center">user</td>
                        <td>Initiateur</td>
                </tr>
                <tr>
                        <td>Agent1@demo</td>
                        <td class="text-center">user</td>
                        <td>Valideur-Editeur</td>
                </tr>
                <tr>
                        <td>Agent2@demo</td>
                        <td class="text-center">user</td>
                        <td>Valideur-Editeur</td>
                </tr>
                <tr>
                        <td>DirecteurG@demo</td>
                        <td class="text-center">user</td>
                        <td>Valideur-Editeur</td>
                </tr>
                <tr>
                        <td>DRH@demo</td>
                        <td class="text-center">user</td>
                        <td>Valideur-Editeur</td>
                </tr>
                <tr>
                        <td>DirecteurVOI@demo</td>
                        <td class="text-center">user</td>
                        <td>Valideur</td>
                </tr>
                <tr>
                        <td>DirecteurTECH@demo</td>
                        <td class="text-center">user</td>
                        <td>Valideur</td>
                </tr>
                <tr>
                        <td>Documentaliste@demo</td>
                        <td class="text-center">user</td>
                        <td>Documentaliste</td>
                </tr>
                <tr>
                        <td>administrateur@demo</td>
                        <td class="text-center">admin</td>
                        <td>Admin</td>
                </tr>
                <tr>
                        <td>admin@admin</td>
                        <td class="text-center">admin</td>
                        <td>Superadmin</td>
                </tr>

            </tbody>
        </table>
    </div>
</div>

<?php } ?>
<script type="text/javascript">

//    $('#UserLoginForm').validate({
//        'rules': {
//            'data[User][username]': 'required',
//            'data[User][password]': 'required'
//        }
//    });

    gui.buttonbox({
        element: $('#loginButtonBox')
    });


    gui.addbutton({
        element: $('#loginButtonBox'),
        button:
                {
                    content: '<i class="fa fa-sign-in fa-fw"></i> Se connecter',
                    class: "pull-right btn btn-primary waiter",
                    action: function () {
                        $('#UserLoginForm').attr('data-waiter-send', 'XHR').
                            attr('data-waiter-title','Génération du document').
                            attr('data-waiter-type', 'progress-bar').
                            attr('data-waiter-callback', 'getProgress')

                        if (form_validate($('#UserLoginForm'))) {
                            $('#UserLoginForm').submit();
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }
    });
    $('#UserLoginForm').keyup(function (e) {
        if (e.keyCode == 13) {
            if ($('#UserLoginForm .list-unstyled').length == 0) {
                $('#UserLoginForm').submit();
            } else {
                swal({
                    showCloseButton: true,
                    title: "Oops...",
                    text: "Veuillez vérifier votre formulaire!",
                    type: "error",

                });
            }
        }
    });

<?php if (SHOW_LOGIN_TABLE) { ?>
    $('#comptesdemoDiv').hide();
    gui.addbutton({
        element: $('#loginButtonBox'),
        button: {
            content: "Afficher les comptes de démonstration",
            action: function () {
                $('.show_login_table').append( $('#comptesdemoDiv'));
                $('#comptesdemoDiv').toggle();
            }
        }
    });

    $('#comptesdemo tr').css('cursor', 'pointer').hover(function () {
        $(this).addClass(' alert-warning');
    }, function () {
        $(this).removeClass(' alert-warning');
    }).click(function () {
        $('#login').val($('td:nth-child(1)', this).html());
        $('#password').val($('td:nth-child(2)', this).html());
    });
<?php } ?>

</script>
<script type="text/javascript">
    $('#loginButtonBox  a.btn-primary').click(function () {
        $('#loginButtonBox a.btn-primary').attr('data-target', '#waiter');
        $('#loginButtonBox a.btn-primary').attr('data-toggle', 'modal');
        $('#loginButtonBox a.btn-primary').attr('data-waiter-send', 'XHR');
        $('#loginButtonBox a.btn-primary').attr('data-waiter-callback', 'getProgress');
        $('#loginButtonBox a.btn-primary').attr('data-waiter-type', 'progress-bar');
        $('#loginButtonBox a.btn-primary').attr('data-waiter-title', 'Test');
    });


    $('#UserLoginForm').keyup(function (e) {
        if (e.keyCode == 13) {
            $('#waiter').modal('show');
        }
    });


    $('.unmask').on('click', function(){
        if($(this).prev('input').attr('type') == 'password') {
            changeType($(this).prev('input'), 'text');
            $(this).parents().find('#form_group_password i.unmask').removeClass('fa-eye-slash').addClass('fa-eye');
        }
        else {
            changeType($(this).prev('input'), 'password');
            $(this).parents().find('#form_group_password i.unmask').removeClass('fa-eye').addClass('fa-eye-slash');
        }
        return false;
    });

    /*
      function from : https://gist.github.com/3559343
      Thank you bminer!
    */
    // x = élément du DOM, type = nouveau type à attribuer
    function changeType(x, type) {
       if(x.prop('type') == type)
          return x; // ça serait facile.
       try {
          // Une sécurité d'IE empêche ceci
          return x.prop('type', type);
       }
       catch(e) {
          // On tente de recréer l'élément
          // En créant d'abord une div
          var html = $("<div>").append(x.clone()).html();
          var regex = /type=(\")?([^\"\s]+)(\")?/;
          // la regex trouve type=text ou type="text"
          // si on ne trouve rien, on ajoute le type à la fin, sinon on le remplace
          var tmp = $(html.match(regex) == null ?
             html.replace(">", ' type="' + type + '">') :
             html.replace(regex, 'type="' + type + '"') );

          // on rajoute les vieilles données de l'élément
          tmp.data('type', x.data('type') );
          var events = x.data('events');
          var cb = function(events) {
             return function() {
                //Bind all prior events
                for(i in events) {
                   var y = events[i];
                   for(j in y) tmp.bind(i, y[j].handler);
                }
             }
          }(events);
          x.replaceWith(tmp);
          setTimeout(cb, 10); // On attend un peu avant d'appeler la fonction
          return tmp;
       }
    }
</script>
<?php if ( !CakeSession::read('Auth.Fullcache') ) :?>
<div id="waiter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="waiterLabel"><?php echo __('Enregistrement des données en session en cours'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="waiter-progress">
                    <!--<div class="progress">-->
<!--                      <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>-->
                    <div class="spacer"></div>
                    <center><p  class="waiter-comment">Veuillez patienter...</p></center>
                </div>

                <div class="waiter-default" >
                    <center><?php echo $this->Html->image('/img/ajax-loader.gif', array('alt' => __('Loading'), 'id' => 'waiter-image')); ?></center>
                    <br />
                    <center><p  class="waiter-comment alert alert-info" >Afin d'améliorer les temps de traitement, différentes informations sont enregistrées en session lors de la connexion.<br /> Ceci peut prendre de quelques secondes à 1 minute.</p></center>
                </div>
              </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif;?>
