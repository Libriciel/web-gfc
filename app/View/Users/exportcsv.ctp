<?php
	$this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow(
        array(
            'Identifiant',
            'Nom',
            'Prénom',
            'Profils',
            'Services'
        )
	);
    
    foreach( $results as $i => $user ){
        $identifiant = Hash::get( $user, "User.username" );
        $nom = Hash::get( $user, "User.nom" );
        $prenom = Hash::get( $user, "User.prenom" );
        $profils = Hash::get( $user, "User.profils" );
        $services = Hash::get( $user, "User.services" );


        $this->Csv->addRow(
            array(
                $identifiant,
                $nom,
                $prenom,
                $profils,
                $services
            )
        );
    }

    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>