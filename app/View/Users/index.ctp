<?php

/**
 *
 * Users/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script type="text/javascript">
    function loadStruct() {
        var loader = false;
        if ($('#liste_users').length > 0 && $('#liste_users').is(':visible')) {
            gui.request({
                url: "<?php echo Configure::read('BaseUrl') . "/users/getUsers"; ?>",
                updateElement: $('#liste_users .content'),
                loader: loader,
                loaderMessage: gui.loaderMessage
            });
        }
        if ($('#liste_services').length > 0 && $('#liste_services').is(':visible')) {
            gui.request({
                url: "<?php echo Configure::read('BaseUrl') . "/services/getServices"; ?>",
                updateElement: $('#liste_services .content'),
                loader: loader,
                loaderMessage: gui.loaderMessage
            });
        }
        if ($('#liste_groups').length > 0 && $('#liste_groups').is(':visible')) {
            gui.request({
                url: "<?php echo Configure::read('BaseUrl') . "/profils/getProfils"; ?>",
                updateElement: $('#liste_groups .content'),
                loader: loader,
                loaderMessage: gui.loaderMessage
            });
        }
    }

    function loadStructure() {
        loadStruct();
    }

	function loadServices() {
		gui.request({
			url: "<?php echo Configure::read('BaseUrl') . "/services/getServices"; ?>",
			updateElement: $('#liste_services .content'),
			loader: true,
			loaderMessage: gui.loaderMessage
		});
	}
</script>
<div class="container" id="index_user">
    <div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
    <div class="row">
        <div class="table-list organisme-edit">
            <h3 style="border-radius:0px!important;"><?php echo __d('menu', 'structureCollectivite'); ?></h3>
            <ul class="nav nav-tabs titre" role="tablist" style="border-radius:0px 0px 0 12px!important;">
                <li class="active liste_users"><a href="#liste_users" role="tab" data-toggle="tab"><?php echo '<i class="fa fa-user" aria-hidden="true"></i> ';?><?php echo __d('user', 'User.liste'). ' : <span>'.$nbUsers.' </span>'; ?></a></li>
                <li class="liste_services"><a href="#liste_services" role="tab" data-toggle="tab"><?php echo '<i class="fa fa-sitemap" aria-hidden="true"></i> ';?><?php echo __d('service', 'Service.liste'). ' : <span>'.$nbServices.' </span>'; ?></a></li>
                <li class="liste_groups"><a href="#liste_groups" role="tab" data-toggle="tab"><?php echo '<i class="fa fa-group" aria-hidden="true"></i> ';?><?php echo __d('profil', 'Profil.liste'). ' : <span>'.$nbProfils.' </span>'; ?></a></li>
            </ul>

            <div class="tab-content">
                <div id="liste_users" class="tab-pane fade active in" >

                    <div class="content">

                    </div>
                    <div class="panel-footer controls " role="group">

                    </div>

                </div>
                <div id="liste_services" class="tab-pane fade" >

                    <div class="content">

                    </div>
                    <div class="panel-footer controls " role="group" >

                    </div>

                </div>
                <div id="liste_groups" class="tab-pane fade" >

                    <div class="content">

                    </div>
                    <!--<div class=" panel-footer controls"></div>-->

                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    $('#index_user .nav-tabs a').on('shown.bs.tab', function (e) {
        loadStructure();
    });

    gui.buttonbox({
        element: $('#liste_services .controls')
    });
    gui.buttonbox({
        element: $('#liste_users .controls')
    });
    gui.buttonbox({
        element: $('#liste_groups .controls')
    });

    gui.addbutton({
        element: $('#liste_services .controls'),
        button: {
            content: '<?php echo '<i class="fa fa-plus-circle" aria-hidden="true"></i>  Ajouter un service' ; ?>',
            title: '<?php echo  __d('service', 'New Service'); ?>',
            action: function () {
                gui.formMessage({
                    updateElement: $('#webgfc_content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                    title: '<?php echo __d('service', 'New Service'); ?>',
                    url: "/Services/add",
                    width: "550px",
                    buttons: {
                        "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        },
                        "<?php echo __d('default', 'Button.submit'); ?>": function () {
                            var form = $(this).parents(".modal").find('form');
                            if (form_validate($(form))) {
                                gui.request({
                                    url: form.attr('action'),
                                    data: form.serialize(),
                                    loaderElement: $('#webgfc_content'),
                                    loaderMessage: gui.loaderMessage,
                                }, function (data) {
                                    getJsonResponse(data);
                                    loadStruct();
                                });
                                $(this).parents(".modal").modal('hide');
                                $(this).parents(".modal").empty();
                            } else {
                                swal({
                    showCloseButton: true,
                                    title: "Oops...",
                                    text: "Veuillez vérifier votre formulaire!",
                                    type: "error",

                                });
                            }
                        }
                    }
                });
            }
        }
    });

    gui.addbutton({
        element: $('#liste_users .controls'),
        button: {
            content: '<?php echo '<i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter un utilisateur'; ?>',
            title: '<?php echo __d('user', 'New User'); ?>',
            class: 'afficheOne btn-info-webgfc',
            action: function () {
                gui.formMessage({
                    updateElement: $('#webgfc_content'),
                    loader: true,
                    width: '700px',
                    loaderMessage: gui.loaderMessage,
                    title: '<?php echo __d('user', 'New User'); ?>',
                    url: "/Users/add",
                    buttons: {
                        "<?php echo __d('default', 'Button.cancel'); ?>": function () {

                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        },
                        "<?php echo __d('default', 'Button.submit'); ?>": function () {
                            var form = $(this).parents(".modal").find('form');

                            if ($(".ui-dialog-content form .errorJsonFormCheck").length == 0) {
                                if (form_validate(form)) {
                                    gui.request({
                                        url: form.attr('action'),
                                        data: form.serialize(),
                                        loaderElement: $('#webgfc_content'),
                                        loaderMessage: gui.loaderMessage
                                    }, function (data) {
                                        getJsonResponse(data);
                                        loadStructure();
                                    });

                                    $(this).parents(".modal").modal('hide');
                                    $(this).parents(".modal").empty();
                                } else {
                                    swal({
                    showCloseButton: true,
                                        title: "Oops...",
                                        text: "Veuillez vérifier votre formulaire!",
                                        type: "error",

                                    });
                                }

                            }
                        }
                    }
                });
            }
        }
    });

    loadStructure();


    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/admin"; ?>";
                }
            }
        ]
    });
</script>
