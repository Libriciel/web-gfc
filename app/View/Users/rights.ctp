<?php

/**
 *
 * Users/rights.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */

$PanelHeader = $this->Html->tag('div', $this->Html->tag('h4',__d('user', 'User.user') . ' : ' . $userName,array('class'=>'panel-heading')), array('class' => 'panel-heading', 'id' => ''));

$ulContent = $this->Html->tag('li', $this->Html->link(__d('desktop', 'Tab.droits_data'), array('controller' => 'users', 'action' => $mode . 'Rights', $id, 'data')));
$ulContent .= $this->Html->tag('li', $this->Html->link(__d('desktop', 'Tab.droits_meta'), array('controller' => 'users', 'action' => $mode . 'Rights', $id, 'meta')));
$ulContent .= $this->Html->tag('li', $this->Html->link(__d('desktop', 'Tab.droits_func'), array('controller' => 'users', 'action' => $mode . 'Rights', $id, 'func')));
$ulContent .= $this->Html->tag('li', $this->Html->link(__d('desktop', 'Tab.droits_circuit'), array('controller' => 'users', 'action' => $mode . 'Rights', $id, 'circuit')));
$PanelBody = $this->Html->tag('div', $this->Html->tag('ul', $ulContent), array('id' => 'rightsTabs'));
$PanelFooter = $this->Html->div('panel-footer',$this->Html->link('Fermer','#',array('class'=>'btn btn-info-webgfc closeModal ')),array('escape'=>false));
$PanelDefault = $this->Html->tag('div', $PanelHeader.$PanelBody.$PanelFooter, array('class' => 'panel panel-default', 'id' => ''));
$Panel = $this->Html->tag('div', $PanelDefault, array('id' => 'panel','class'=>'detailHabils'));
echo $Panel;
?>

<script type="text/javascript">
    gui.tabs({
        element: $('#rightsTabs'),
        noErrorMsg: true,
        loaderMessage: gui.loaderMessage
    });
    $('.closeModal').click(function () {
        $("#modalRD").hide();
        $("#habilsPerson").show();
    });

</script>
