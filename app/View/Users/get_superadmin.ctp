<?php

/**
 *
 * users/getsuperadmin.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par Libriciel SCOP
 * @link http://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (!empty($users)) {
	$fields = array(
		'username',
		'nom',
		'prenom',
		'mail'
	);
	$actions = array(
		"edit" => array(
			"url" => '/users/edit_superadmin',
			"updateElement" => "$('#infos .content')",
			"formMessage" => false,
			'formMessageHeight' => '600',
			'formMessageWidth' => '1000',
			"refreshAction" => "loadUser();",
			"divshow" =>"infos",
			"divhide" =>"liste"
		),
		"lock" => array(
				"url" => '/users/edit_password',
				"updateElement" => "$('#infos .content')",
				"formMessage" => false,
				'formMessageHeight' => '600',
				'formMessageWidth' => '1000',
				"refreshAction" => "loadUser();",
				"divshow" =>"infos",
				"divhide" =>"liste"
		),
		"delete" => array(
			"url" => '/users/delete_superadmin',
			"updateElement" => "$('#infos .content')",
			"refreshAction" => "loadUser();"
		)
	);
	$options = array('check_inactive' => true, 'inactive_value' => 0, 'inactive_field' => 'active', 'inactive_model' => 'User');

	$data = $this->Liste->drawTbody($users, 'User', $fields, $actions, $options);
	?>
	<table id="table_users"
		   data-toggle="table"
		   data-search="true"
		   data-locale = "fr-CA"
		   data-height="300"
		   data-show-refresh="false"
		   data-show-toggle="false"
		   data-show-columns="true"
		   data-toolbar="#toolbar">
	</table>

	<script>
		if (!$('#liste.table-list h3 span').length < 1) {
			$('#liste.table-list h3 span').remove();
		}
		$('#liste.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($users,$options); ?>');
		$('#table_users')
			.bootstrapTable({
				data:<?php echo $data;?>,
				height: '600px',
				columns: [
					{
						field: "username",
						title: "<?php echo __d("user","User.username"); ?>",
						class: "username"
					},
					{
						field: "nom",
						title: "<?php echo __d("user","User.nom"); ?>",
						class: "nom"
					},
					{
						field: "prenom",
						title: "<?php echo __d("user","User.prenom"); ?>",
						class: "prenom"
					},
					{
						field: "mail",
						title: "<?php echo __d("user","User.mail"); ?>",
						class: "mail"
					},
					{
						field: "edit",
						title: "Modifier",
						class: "actions thEdit",
						width: "80px",
						align: "center"
					},
					{
						field: "lock",
						title: "Mot de passe",
						class: "actions thLock",
						width: "80px",
						align: "center"
					},
					{
						field: "delete",
						title: "Supprimer",
						class: "actions thDelete",
						width: "80px",
						align: "center"
					}
				]
			});
	</script>
	<?php
	echo $this->Liste->drawScript($users, 'User', $fields, $actions, $options);
	echo $this->Js->writeBuffer();
	?>
	<?php
} else {
	echo $this->Html->tag('div', __d('User', 'User.void'), array('class' => 'alert alert-warning'));
}
?>
