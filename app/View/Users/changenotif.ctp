<?php

/**
 *
 * Users/changenotif.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="panel panel-default" id="infosNotif">
    <div class="panel-heading">
        <h4 class="panel-title"><?php echo __d('user', 'Change notif'); ?></h4>
    </div>
    <div class="panel-body">
        <div class="row">
<?php
if ($this->Session->check( 'Message.flash' ) ) {
    echo $this->Session->flash();
}
if ($this->Session->check( 'Message.auth' ) ) {
    echo $this->Session->flash( 'auth' );
}

$formUserNotifiPrin = array(
    'name' => 'User',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-1',
    'input' => array(
        'User.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'User.accept_notif' => array(
            'labelText' =>__d('user', 'User.accept_notif'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['User']['accept_notif']
            )
        )
    )
);
echo  $this->Formulaire->createForm($formUserNotifiPrin);



$legendAbonnement =  $this->Html->tag('legend', __d('user', 'User.optionnotif'), array('class' => ''));
$formUserNotifiDetail1 = array(
    'name' => 'User',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-2',
    'input' => array(
        'User.mail_insertion' => array(
            'labelText' =>__d('user', 'User.mail_insertion'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['User']['mail_insertion']
            )
        ),
        'User.mail_traitement' => array(
            'labelText' =>__d('user', 'User.mail_traitement'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['User']['mail_traitement']
            )
        ),
        'User.mail_refus' => array(
            'labelText' =>__d('user', 'User.mail_refus'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['User']['mail_refus']
            )
        ),
        'User.mail_retard_validation' => array(
            'labelText' =>__d('user', 'User.mail_retard_validation'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['User']['mail_retard_validation']
            )
        ),
        'User.mail_copy' => array(
            'labelText' =>__d('user', 'User.mail_copy'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['User']['mail_copy']
            )
        )
    )
);
$formUserNotifiDetail2 = array(
    'name' => 'User',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-2',
    'input' => array(
        'User.mail_insertion_reponse' => array(
            'labelText' =>__d('user', 'User.mail_insertion_reponse'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['User']['mail_insertion_reponse']
            )
        ),
        'User.mail_aiguillage' => array(
            'labelText' =>__d('user', 'User.mail_aiguillage'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['User']['mail_aiguillage']
            )
        ),
        'User.mail_delegation' => array(
            'labelText' =>__d('user', 'User.mail_delegation'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['User']['mail_delegation']
            )
        ),
        'User.mail_clos' => array(
            'labelText' =>__d('user', 'User.mail_clos'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>!empty($this->request->data['User']['mail_clos'])? $this->request->data['User']['mail_clos']:false
            )
        ),
    )
);
$fieldsetOptionNotificationContent = "<div class='col-sm-6'>".$this->Formulaire->createForm($formUserNotifiDetail1)."</div>";
$fieldsetOptionNotificationContent .= "<div class='col-sm-6'>".$this->Formulaire->createForm($formUserNotifiDetail2)."</div>";

$formUserAbonnement = array(
    'name' => 'User',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-4',
    'input' => array(
        'User.typeabonnement' => array(
            'labelText' =>__d('user', 'User.typeabonnement'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options'=> $typeabonnement,
                'empty' => false
            )
        )
    )
);
$fieldsetAbonnementNotificationContent = $this->Formulaire->createForm($formUserAbonnement);
echo $this->Html->tag('fieldset', $legendAbonnement.$fieldsetOptionNotificationContent.'<hr class="col-sm-12">'.$fieldsetAbonnementNotificationContent, array( 'id' => 'UserOptionNotification', 'class' => 'notification' ));

echo $this->Form->end();

?>
        </div>
    </div>
    <div class="panel-footer " role="group">
        <a class="btn btn-danger-webgfc btn-inverse " id="cancel"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a class="btn btn-success " id="saveNewNotif"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script type="text/javascript">


    $(document).ready(function () {
        // Affichage des notifications
        $('#UserAcceptNotif').change(function () {
            if ($('#UserAcceptNotif').prop("checked")) {
                $('#UserOptionNotification').show();
            }
            else {
                $('#UserOptionNotification').hide();
            }
        });

        if ($('#UserAcceptNotif').prop("checked")) {
            $('#UserOptionNotification').show();
        }
        else {
            $('#UserOptionNotification').hide();
        }

        $('#saveNewNotif').click(function () {

            if (form_validate($('#UserChangenotifForm'))) {
                var id = $('#UserId').val();
                gui.request({
                    url: '/users/changenotif/' + id,
                    data: $('#UserChangenotifForm').serialize(),
                    loaderElement: $(".infoZone"),
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    getJsonResponse(data);
                    gui.request({
                        url: '/users/edit/' + id,
                        loader: false
                    }, function (data) {
                        $('.ui-dialog-content').html(data);
                        $('.loader').remove();
                    });
                });
                $("#infosNotif").remove();
                $("#infoPerson").show();
            } else {
                swal({
                    showCloseButton: true,
                    title: "Oops...",
                    text: "Veuillez vérifier votre formulaire!",
                    type: "error",

                });
            }

        });

        $('#cancel').click(function () {
            $("#infosNotif").remove();
            $("#infoPerson").show();
        });

        $('#UserTypeabonnement').select2();


    });
</script>

