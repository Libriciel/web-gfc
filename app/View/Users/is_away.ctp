<?php

/**
 *
 * Users/is_away.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if ($isAway > 0) {
	?>
<a class="notification_fullwrapper">
		<?php
		$span_notif = $this->Html->tag('div', $isAway, array('class' => 'badge'));
		$span_notif .= $this->Html->tag('div','<i class="fa fa-user-times"  aria-hidden="true" style="color:#fff;"></i>', array('id' => 'viewDeleg', 'title' => __d('deleg', 'Deleg.active'),'escape' => false));
		echo $this->Html->tag('span', $span_notif, array('class' => 'notification_wrapper'));
		?>

    <div id="divAbsence" class="panel">
        <div class="arrow"></div>
        <div class="content_notification">

            <h4 class="panel-title">Délégué:</h4>
            <div  class="notification_div" style="text-align:center;"><?php //echo __d('deleg', 'Deleg.active'); ?>
            <?php
            if(count($delegatedDesktopsContents)>0):
            ?>
                <legend>Profil Reçu</legend>
                <table class="tableDelegatedDesktops notification_div" data-toggle="table" data-locale = "fr-CA">
                    <thead>
                        <tr>
                            <th>Profil</th>
                            <th>Service</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($delegatedDesktopsContents as $delegatedDesktopsContent): ?>
                        <tr>
                            <td><?php echo $delegatedDesktopsContent['desktopName']; ?></td>
                            <td><?php echo @$delegatedDesktopsContent['serviceName']; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php
            endif;
            ?>
            <?php
            if(count($delegateToDesktopsContents)>0):
            ?>
                <legend><?php echo  __d( 'user', 'Delegate.role'); ?></legend>
                <table class="tableDelegatedToDesktops notification_div" data-toggle="table" data-locale = "fr-CA">
                    <thead>
                        <tr>
                            <th><?php echo __d( 'user', 'Delegate.user'); ?></th>
                            <th><?php echo __d( 'user', 'Delegate.desktop'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($delegateToDesktopsContents as $delegateToDesktopsContent): ?>
                        <tr>
                            <td><?php echo $delegateToDesktopsContent['UserInfo']; ?></td>
                            <td><?php echo $delegateToDesktopsContent['Role']; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php
            endif;
            ?>
            </div>

            <div class="notif_actions" id="gotoSetAbsence"></div>
        </div>
    </div>
</a>
<script type="text/javascript">
    $('.tableDelegatedDesktops').bootstrapTable();
    $('.tableDelegatedToDesktops').bootstrapTable();
    gui.buttonbox({
        element: $('#gotoSetAbsence'),
        align: "center"
    });

    gui.addbutton({
        element: $('#gotoSetAbsence'),
        button: {
            content: ' <i class="fa fa-info fa-fw" aria-hidden="true"></i>',
            class: 'pull-right',
            title: "<?php echo __d('menu', 'Informations du compte'); ?>",
            action: function () {
                window.location.href = "<?php echo Configure::read('BaseUrl') . "/users/getInfosCompte"; ?>";
            }
        }
    })

</script>
	<?php
} else {
	echo '<a><i class="fa fa-user-times" aria-hidden="true"></i></a>';
}
?>
