<?php

/**
 *
 * Users/get_users.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>


<script type="text/javascript">
    $('#formulaireButton').button();
    $('#searchButton').button();
    $('#returnButton').button();
    $('#UserProfilId').select2({allowClear: true, placeholder: "Recherche par profil"});
    $('#UserServiceId').select2({allowClear: true, placeholder: "Recherche par service"});
    function searchUser() {
        $('#UserUsername').val('');
        $('#UserProfilId').val('');
        $('#UserServiceId').val('');
        $('#UserNom').val('');
        $('#UserPrenom').val('');
        gui.request({
            url: $("#UserGetUsersForm").attr('action'),
            data: $("#UserGetUsersForm").serialize(),
            loader: true,
            updateElement: $('#liste_users .content'),
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $('#liste_users .content').html(data);
        });
    }


</script>


<?php

if( isset( $users ) ) {
//    if( empty( $users ) ) {
//        echo $this->Html->tag( 'p', 'Aucun utilisateur ne correspond à votre recherche', array( 'class' => 'voidMessage' ) );
//    }
//	else {
        $fields = array(
            'username',
            'nom',
            'prenom',
            'profils',
            'services',
            'active'
        );
        $actions = array(
            "edit" => array(
                "url" => Configure::read('BaseUrl') . '/users/edit',
                "updateElement" => "$('#webgfc_content')",
                "message" => true,
    //            "formMessage" => true,
                'formMessageHeight' => '650',
                'formMessageWidth' => '1200',
                "refreshAction" => "loadStructure();"
            ),
            "delete" => array(
                "url" => Configure::read('BaseUrl') . '/users/delete',
                "updateElement" => "$('#webgfc_content')",
                "refreshAction" => "loadStructure();"
            )
        );
        $options = array(
            "filter" => false,
            "paginator" => true
        );

        $data = $this->Liste->drawTbody($users, 'User', $fields, $actions, $options);
        ?>

<div  class="bannette_panel panel-body" id="get_users_contents">
    <span class="bouton-search action-title pull-right"  role="group" style="margin-top: 10px; margin-left: 5px;">
        <a href="#" class="btn btn-info-webgfc btn-inverse" data-target="#UserGetUserFormModal" data-toggle="modal" title="Recherche avancée"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
        <a href="#" class="btn btn-info-webgfc btn-inverse" id="searchCancel" title="Réinitialisation" style="margin-left: 15px;"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>
    </span>
    <table id="table_users"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height="570"
           data-show-refresh="false"
           data-show-toggle="false"
           data-show-columns="false"
           data-sortable ="true"
           data-pagination ="true"
           data-page-size ="10"
           data-toolbar="#toolbar">
    </table>
</div>
<script>
    <?php if(!empty($users)): ?>
    if ($('.liste_users span').length != 0) {
        $('.liste_users span').html('<?php echo count($users); ?>');
    }
    <?php endif; ?>
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
//    $('.table-list h3').append('<span> - total:<?php echo $nbusers;?><span>');
    $('#table_users')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: 'username',
                        title: '<i class="fa fa-id-card-o" aria-hidden="true"  title="<?php echo __d("user","User.username");?>" style="color:#000;"></i> &nbsp;',
                        class: 'username',
                        sortable: true
                    },
                    {
                        field: 'nom',
                        title: '<?php echo __d('user','User.nom'); ?>',
                        class: 'nom',
                        sortable: true
                    },
                    {
                        field: 'prenom',
                        title: '<?php echo __d('user','User.prenom'); ?>',
                        class: 'prenom',
                        sortable: true
                    },
                    {
                        field: 'profils',
                        title: '<?php echo __d('user','User.profils'); ?>',
                        class: 'profils',
                        sortable: true
                    },
                    {
                        field: 'services',
                        title: '<?php echo __d('user','User.services'); ?>',
                        class: 'services',
                        sortable: true
                    },
                    {
                        field: "active",
                        title: "",
                        class: "active_column"
                    },
                    {
                        field: 'edit',
                        title: 'Modifier',
                        class: 'actions thEdit',
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: 'delete',
                        title: 'Supprimer',
                        class: 'actions thDelete',
                        width: "80px",
                        align: "center"
                    }
                ]
            })
            .on('page-change.bs.table', function (number, size) {
                addClassActive();
            })
            .on('all.bs.table', function (e, name, order) {
                addClassActive();
            });

            $(window).resize(function () {
                $("#table_users").bootstrapTable( 'resetView' , {height: 570} );
            });

    $(document).ready(function () {
        addClassActive();
    });

    function addClassActive() {
        $('#table_users .active_column').hide();
        $('#table_users .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }



</script>
<?php
echo $this->Liste->drawScript($users, 'User', $fields, $actions, $options);
?>
<?php
}

    echo $this->Js->writeBuffer();

?>
<script type="text/javascript">
    $('#liste_users .listTr').hover(function () {
        var regexp = /^Desktop_/;
        var classes = $(this).attr('class').split(' ');
        for (i in classes) {
            if (regexp.searchUser(classes[i])) {
                $('.' + classes[i]).addClass('alert alert-warning');
            }
        }
        var regexp = /^SecondaryDesktop_/;
        var classes = $(this).attr('class').split(' ');
        for (i in classes) {
            if (regexp.searchUser(classes[i])) {
                $('.' + classes[i]).addClass('alert alert-warning');
                $('#liste_groups .' + classes[i].substring(9, classes[i].length) + ', #liste_services .' + classes[i].substring(9, classes[i].length)).addClass('alert alert-warning');
            }
        }
    }, function () {
        var regexp = /^Desktop_/;
        var classes = $(this).attr('class').split(' ');
        for (i in classes) {
            if (regexp.searchUser(classes[i])) {
                $('.' + classes[i]).removeClass('alert alert-warning');
            }
        }
        var regexp = /^SecondaryDesktop_/;
        var classes = $(this).attr('class').split(' ');
        for (i in classes) {
            if (regexp.searchUser(classes[i])) {
                $('.' + classes[i]).removeClass('alert alert-warning');
                $('#liste_groups .' + classes[i].substring(9, classes[i].length) + ', #liste_services .' + classes[i].substring(9, classes[i].length)).removeClass('alert alert-warning');
            }
        }
    });
    // Ajout d'un bouton d'export de recherche par utilisateur
    /**
     * Bouton pour l'export des résultats
     */
    var element = $(".afficheOne");
    if (!element.length == '1') {
        gui.addbutton({
            element: $('#liste_users>.panel-footer.controls'),
            button: {
                content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
                title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
                class: 'afficheOne btn-info-webgfc',
                action: function () {
                    window.location.href = "<?php echo Router::url( array( 'controller' => 'users', 'action' => 'exportcsv' ) + Hash::flatten( $this->request->data, '__' ) );?>";
                }
            }
        });
    }
    else {
        gui.removebutton({
            element: $('#liste_users>.panel-footer.controls'),
            button: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats'
        });
        gui.addbutton({
            element: $('#liste_users>.panel-footer.controls'),
            button: {
                content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
                title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
                class: 'afficheOne btn-info-webgfc',
                action: function () {
                    window.location.href = "<?php echo Router::url( array( 'controller' => 'users', 'action' => 'exportcsv' ) + Hash::flatten( $this->request->data, '__' ) );?>";
                }
            }
        });
    }
    // fin de l'ajout du bouton d'export
    // Dans la liste des utilisateurs, les colonnes Profils et Services possèdent un tri
    // Le paginator ne peut pas fonctionner correctement sur ces colonnes, on désactive donc le tri
    try {
        // On récupère l'intitulé de colonne possédant la classe profils
        var thProfils = $('div.bannette_panel table thead th.profils')[0];
        // On remplace la valeur de l'entête par le contenu du "fils" présent dans le a href
        // en gros <th class="profils"><a href=''>Profils</a></th> devient <th class="profils">Profils</th>
        $(thProfils).find('a').attr('href', '');
    } catch (e) {
        console.log(e);
    }

    try {
        // On récupère l'intitulé de colonne possédant la classe services
        var thServices = $('div.bannette_panel table thead th.services')[0];
        // On remplace la valeur de l'entête par le contenu du "fils" présent dans le a href
        // en gros <th class="profils"><a href=''>Services</a></th> devient <th class="profils">Services</th>
        $(thServices).find('a').attr('href', '');
    } catch (e) {
        console.log(e);
    }

</script>
<script>


    var element = $(".afficheOne");
    if (!element.is(':visible')) {
        gui.addbutton({
            element: $('#liste_users>.controls'),
            button: {
                content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
                title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
                class: 'afficheOne btn btn-info-webgfc btn-inverse',
                action: function () {
                    window.location.href = "<?php echo Router::url( array( 'controller' => 'users', 'action' => 'exportcsv' ) + Hash::flatten( $this->request->data, '__' ) );?>";
                }
            }
        });
    }
    else {
        gui.removebutton({
            element: $('#liste_users>.controls'),
            button: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats'
        });

        gui.addbutton({
            element: $('#liste_users>.controls'),
            button: {
                content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
                title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
                class: 'afficheOne btn btn-info-webgfc btn-inverse',
                action: function () {
                    window.location.href = "<?php echo Router::url( array( 'controller' => 'users', 'action' => 'exportcsv' ) + Hash::flatten( $this->request->data, '__' ) );?>";
                }
            }
        });

    }
    gui.addbutton({
        element: $('#UserGetUsersFormModalButton'),
        button: {
            content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
            class: 'btn btn-success',
            action: function () {
                gui.request({
                    url: $("#UserGetUsersForm").attr('action'),
                    data: $("#UserGetUsersForm").serialize(),
                    loader: true,
                    updateElement: $('#liste_users .content'),
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    location.reload();
                });
            }

        }
    });
    $('#searchCancel').button();
    $('#searchCancel').click(function () {
        loadStruct();
    });
    $(document).ready(function () {
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });

        gui.addbutton({
            element: $('#UserGetUserFormButton'),
            button: {
                content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
                class: 'btn btn-info-webgfc btn-inverse cancelSearch ',
                action: function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                    loadStruct();
                }

            }
        });
        gui.addbutton({
            element: $('#UserGetUserFormButton'),
            button: {
                content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
                class: 'btn btn-success searchBtn ',
                action: function () {
                    var form = $(this).parents('.modal').find('form');
                    gui.request({
                        url: form.attr('action'),
                        data: form.serialize(),
                        loader: true,
                        updateElement: $('#get_users_contents'),
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        $('#get_users_contents').empty();
                        $('#get_users_contents').html(data);
                    });
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();

                }

            }
        });

        $('.form-control').css('height', '34px');
    });

    $('#UserGetUserFormModal').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#UserGetUserFormButton .searchBtn').trigger('click');
            return false;
        }
        if (e.keyCode === 27) {
            $('#UserGetUserFormButton .cancelSearch').trigger('click');
            return false;
        }
    });


    $('input[type=checkbox]').css('height', '15px');
    $('input[type=checkbox]').css('margin-left', '-146px');
</script>
<?php
    $formUserSearch = array(
        'name' => 'User',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-5',
		'form_url' => array( 'controller' => 'users', 'action' => 'getUsers' ),
        'input' => array(
            'User.profil_id' => array(
                'labelText' => __d('user', 'User.profil_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type' => 'select',
                    'empty' => true,
                    'options' =>   $groups
                )
            ),
            'User.service_id' => array(
                'labelText' => __d('user', 'User.service_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type' => 'select',
                    'empty' => true,
                    'options' => $services
                )
            ),
            'User.username' => array(
                'labelText' =>__d('user', 'User.username'),
                'inputType' => 'text',
                'items'=>array(
                    'type' => 'text'
                )
            ),
            'User.nom' => array(
                'labelText' =>__d('user', 'User.nom'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'User.prenom' => array(
                'labelText' =>__d('user', 'User.prenom'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'User.active' => array(
                'labelText' =>__d('user', 'User.active'),
                'inputType' => 'checkbox',
                'items'=>array(
                    'type' => 'checkbox',
                    'checked' => true
                )
            )
        )
    );
?>
<div id="UserGetUserFormModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content zone-form">
            <div class="modal-header">
                <button data-dismiss="modal" class="close">×</button>
                <h4 class="modal-title">Recherche d'utilisateur</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Formulaire->createForm($formUserSearch);?>
            </div>
            <div id="UserGetUserFormButton" class="modal-footer controls " role="group">

            </div>
                <?php echo $this->Form->end();?>
        </div>
    </div>
</div>
