<?php

/**
 *
 * Metadonnees/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formBan = array(
    'name' => 'Ban',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-5',
    'input' => array(
        'Ban.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Ban.name' => array(
            'labelText' =>__d('ban', 'Ban.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        )
    )
);
echo $this->Formulaire->createForm($formBan);
echo $this->Form->end();
?>

