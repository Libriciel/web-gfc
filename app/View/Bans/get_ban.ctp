<?php

/**
 *
 * Dossiers/get_dossiers.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="ban-list">
<?php
if (!empty($bans)) {
    $options = array(
        'nodes' => array(
            array(
                'model' => 'Ban',
                'field' => 'name',
                'actions' => array(
                    "view" => array(
                        "ico" => 'fa fa-eye',
                        "url" => '/bans/edit',
                        "updateElement" => "$('#infos .content')",
                        "formMessage" => false,
//                        "icoColor" => "#5397a7"
                    ),
                    "delete" => array(
                        "ico" => 'fa fa-trash',
                        "url" => '/bans/delete',
                        "updateElement" => "$('#infos .content')",
                        "refreshAction" => "loadBans();",
//                        "icoColor" => "#FF0000"
                    )
                )
            ),
            array(
                'model' => 'Bancommune',
                'field' => 'name',
                'actions' => array(
                    "view" => array(
                        "ico" => 'fa fa-eye',
                        "url" => '/banscommunes/edit',
                        "updateElement" => "$('#infos .content')",
                        "formMessage" => false,
//                        "icoColor" => "#5397a7"
                    ),
                    "delete" => array(
                        "ico" => 'fa fa-trash',
                        "url" => '/banscommunes/delete',
                        "updateElement" => "$('#infos .content')",
                        "refreshAction" => "loadBans();",
//                        "icoColor" => "#FF0000"
                    )
                )
            )
        ),
        'listId' => 'browser',
        'listClass' => 'filetree bans'
    );
    echo $this->Liste->jqueryTreeView($bans, $options);
}else{
    echo $this->Html->div('alert alert-warning',__d('ban', 'Ban.void'));
    echo "<div class='alert alert-warning'>"
    . "Afin de retrouver votre base d'adresse nationale, veuillez vous rendre sur "
            . "<a href='http://adresse.data.gouv.fr/' target='external'>http://adresse.data.gouv.fr/</a> "
            . "et accéder aux données de votre département.</div>";
}
?>
</div>
<script type="text/javascript">
    $("#browser").treeview({collapsed: true});

    $('.treeviewBttn i').each(function () {
        var parentSpan = $(this).parent().parent();
        var parentLink = $(this).parent();
        var href = $(this).parent().attr('href');
        var img = $(this).detach();
        img.appendTo(parentSpan).click(function () {

            if ($(this).hasClass('viewBttn')) {
                gui.formMessage({
                    updateElement: $('#liste .table-list'),
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                    url: href,
                    buttons: {
                        "<i class='fa fa-times' aria-hidden='true'></i> Fermer": function () {
                            $(this).parents('.modal').modal('hide');
                            $(this).parents('.modal').empty();
                        }
                    }
                });

            }

            if ($(this).hasClass('deleteBttn')) {
                swal({
                    showCloseButton: true,
                    title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
                    text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
                    cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
                }).then(function (data) {
//                    $(this).parents(".modal").modal('hide');
                    if (data) {
                        gui.request({
                            url: href,
                            updateElement: $('#webgfc_content'),
                            loader: true,
                            loaderMessage: gui.loaderMessage
                        }, function (data) {
                            getJsonResponse(data);
                            loadBans();
                        });
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Annulé!",
                            text: "Vous n'avez pas supprimé, ;) .",
                            type: "error",

                        });
                    }
                });
                $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
            }


        });
        parentLink.remove();
    });
</script>
