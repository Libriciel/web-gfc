<?php

/**
 *
 * Referentielfantoir/import.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    $formImport = array(
        'name' => 'Import',
        'label_w' => 'col-sm-3',
        'input_w' => 'col-sm-5',
        'enctype' =>'multipart/form-data',
        'input' => array(
            'Import.departement_id' => array(
                'labelText' =>__d('ban', 'Ban.region_id'),
                'inputType' => 'select',
                'items'=>array(
                    'required'=>true,
                    'options'=> $departements,
                    'type'=>'select',
                    'empty' => true
                )
            ),
            'Armodel.file' => array(
                'labelText' =>__d('ban', 'Ban.file'),
                'inputType' => 'file',
                'items'=>array(
                    'type'=>'file',
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formImport);
    echo $this->Form->end();
?>


<script type="text/javascript">
    $('#ImportDepartementId').select2();
</script>
