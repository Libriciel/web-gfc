<?php
/**
 *
 * Profils/get_infos.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div id="group_info">
    <?php
    echo $this->Form->input('Profil.name', array('label' => __d('profil', 'Profil.name'), 'disabled' => 'disabled'));
    ?>
</div>
