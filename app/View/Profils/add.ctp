<?php

/**
 *
 * Profils/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formProfils = array(
    'name' => 'Profil',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Profil.name' => array(
            'labelText' =>__d('event', 'Event.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Profil.profil_id' => array(
            'labelText' =>($this->Html->tag('span', __d('profil', 'CopieDroitProfil'), array('class' => 'bold')) . $this->Html->tag('span', '*', array('style' => 'color: #ee3311;'))),
            'inputType' => 'select',
            'items'=>array(
                'options' => $groups,
                'type'=>'select',
                'empty' => true,
                'required'=>true
            )
        )
    )
);
echo $this->Formulaire->createForm($formProfils);
echo $this->Form->end();
?>

<script type="text/javascript">
    $('#ProfilName').blur(function () {
        gui.request({
            url: "/profils/ajaxformvalid/name",
            data: $('#ProfilAddForm').serialize()
        }, function (data) {
            processJsonFormCheck(data);
        });
    });

    $('#ProfilProfilId').select2();

</script>
