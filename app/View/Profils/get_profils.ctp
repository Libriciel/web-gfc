<?php

/**
 *
 * Profils/get_groups.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="groups_accordion">
    <div class="panel">
        <div class="panel">
            <div  class="bannette_panel panel-body" >
                <table id="table_groups_base"
                       data-toggle="table"
                       data-locale = "fr-CA"
                       >
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function viewElementProfil(itemId) {
        gui.formMessage({
            updateElement: $("body"),
            loader: true,
            loaderMessage: gui.loaderMessage,
            title: '<?php echo  __d('profil', 'Profil.base');?>',
            url: '<?php echo Configure::read('BaseUrl') . '/profils/view'; ?>' + '/' + itemId,
            buttons: {
                "<i class='fa fa-times' aria-hidden='true'></i> Fermer": function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                }
            }
        });
    }
    function editElementProfil(itemId) {
        gui.formMessage({
            updateElement: $("body"),
            loader: true,
            width: 200,
            loaderMessage: gui.loaderMessage,
            title: '<?php echo  __d('profil', 'Profil.base');?>',
            url: '<?php echo Configure::read('BaseUrl') . '/profils/edit'; ?>' + '/' + itemId,
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $('#ProfilEditForm');
                    if (form_validate(form)) {
                        gui.request({
                            url: form.attr('action'),
                            data: form.serialize()
                        }, function (data) {
                            orgInfos = jQuery.parseJSON(data);
                            layer.msg("Les informations ont bien été enregistrées");
                        });
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        });
    }

    $('#liste_groups .groups_accordion h3').each(function () {
        var content = $(this).html();
        var span = $('<a href="#"></a>').append(content);
        $(this).empty();
        $(this).append(span);

    });

    var showInactiveState = false;
    var showInactive = function () {
        if (showInactiveState) {
            $('#liste_groups tr.ui-state-inactive').show();
            showInactiveState = false;
        } else {
            $('#liste_groups tr.ui-state-inactive').hide();
            showInactiveState = true;
        }
    }


<?php
    $data = array();
    $i=0;
    foreach($groups_base as $profil){
        $data[$i]['name'] = $profil['Profil']['name'];
        $data[$i]['view'] = $this->Html->tag('i', '', array(
            'title' => __d('default', 'Button.view'),
            'alt' => __d('default', 'Button.view'),
            'class' => "fa fa-eye itemView",
            'onclick' => "return viewElementProfil(" . $profil['Profil']['id'] . ")",
            )
        );
        $data[$i]['edit'] = $this->Html->tag('i', '', array(
            'title' => __d('default', 'Button.edit'),
            'alt' => __d('default', 'Button.edit'),
            'class' => "fa fa-pencil itemEdit",
            'onclick' => "return editElementProfil(" . $profil['Profil']['id'] . ")",
            )
        );
        $i++;
    }
    ?>
    $('#table_groups_base')
        .bootstrapTable({
            data:<?php echo json_encode($data);?>,
            columns: [
                {
                    field: "name",
                    title: "<?php echo __d("profil","Profil.name"); ?>",
                    class: "name"
                },
                {
                    field: "active",
                    title: "<?php echo __d('profil', 'Profil.active'); ?>",
                    class: "active_column"
                },
                {
                    field: "view",
                    title: "Détail",
                    class: "actions thView",
                    width: "80px",
                    align: "center"
                },
                {
                    field: "edit",
                    title: "Modifier",
                    class: "actions thEdit",
                    width: "80px",
                    align: "center"
                   }
                ]
            })
            .on('page-change.bs.table', function (number, size) {
                addClassActive();
            })
            .on('all.bs.table', function (e, name, order) {
                addClassActive();
            });

    $(document).ready(function () {
        addClassActive();
    });

    function addClassActive() {
        $('#table_groups_base .active_column').hide();
        $('#table_groups_base .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }
</script>
