<?php
/**
 *
 * Profils/get_droits_func.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div id="group_droits_func">
<?php
if ($mode == 'func') {
    if( Configure::read( 'debug' ) > 0 ) {
    }
    if ($rights['Right']['mode_classique']) {
        echo $this->Html->tag('p', __d('right', 'mode_classique.active'), array('class' => 'alert alert-warning', 'style' => 'font-style: italic;padding: 2px;'));
    }
    echo $this->Element('rights', array('rights' => $rights, 'edit' => false));
} else if ($mode == 'func_classique') {
    echo $this->Html->tag('span', __d('right', 'mode_simplifie'), array('class' => 'modeSimplifieBttn btn btn-info-webgfc'));
    $options = array(
        'no' => $this->Html->image('/DataAcl/img/test-fail-icon.png', array('style' => 'width: 16px;height: 16px;vertical-align: middle;', 'alt' => 'no')),
        'yes' => $this->Html->image('/DataAcl/img/test-pass-icon.png', array('style' => 'width: 16px;height: 16px;vertical-align: middle;', 'alt' => 'yes')),
        'parentNodeMarkClose' => '<i class="fa fa-minus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
        'parentNodeMarkOpen' => '<i class="fa fa-plus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
        'edit' => false,
        'tableRootClass' => 'ui-widget ui-widget-content ui-corner-all',
        'legendClass' => 'ui-widget ui-widget-content ui-corner-all ui-state-focus',
        'fieldsetClass' => 'ui-widget ui-widget-content ui-corner-all',
        'fieldsetStyle' => 'margin-top: 15px;',
        'legendStyle' => 'padding-left: 55px;padding-right: 55px;padding-top: 3px;padding-bottom: 3px;',
        'requester' => $requester
    );
    $options['fields'] = array(
        'read' => 'accès'
    );
    $options['fieldsetLegend'] = 'Fonctions';
    $options['aclType'] = 'Acl';
    $options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_rights';
    $this->DataAcl->drawTable($rights, true, $options);
}
?>
</div>
<script type="text/javascript">
    initTables();
    <?php if ($mode == 'func') { ?>
        $('.modeClassiqueBttn').button().click(function(){
            var cpt = 0;
            $('#groups_tabs a').each(function() {
                if (cpt == 1){
                    var href = $.data(this, 'href.tabs');
                    href = href.replace('func', 'func_classique');
                    $('#groups_tabs').tabs('url', cpt, href);
                }
                cpt++;
            });
            $('#groups_tabs').tabs('load', 1);
        });
    <?php } else if ($mode == 'func_classique') { ?>
        $('.modeSimplifieBttn').button().click(function(){
            var cpt = 0;
            $('#groups_tabs a').each(function() {
                if (cpt == 1){
                    var href = $.data(this, 'href.tabs');
                    href = href.replace('func_classique', 'func');
                    $('#groups_tabs').tabs('url', cpt, href);
                }
                cpt++;
            });
            $('#groups_tabs').tabs('load', 1);
        });
<?php } ?>
</script>
