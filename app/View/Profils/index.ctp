<?php

/**
 *
 * Profils/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php //echo $this->Html->css(array('administration'), null, array('inline' => false)); ?>

<script type="text/javascript">
    function adminSubmitAll(submits, index) {
        if (submits[index] != 'end') {
            gui.request({
                url: submits[index].url,
                data: submits[index].data,
                loaderElement: $('#webgfc_content'),
                loaderMessage: gui.loaderMessage
            }, function () {
                adminSubmitAll(submits, index + 1);
            });
        }
        else {
            $('#infos .content').empty();
            $('#liste table tr').removeClass('ui-state-focus');
            gui.request({
                url: "<?php echo Configure::read('BaseUrl') . "/profils/getProfils"; ?>",
                updateElement: $('#liste .content'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
            layer.msg('Les informations ont été enregistrées', {});
        }
    }
</script>

<table id="admin_skel">
    <tr>
        <td id="liste">
            <div class="title">
                <?php echo __d('profil', 'Profil.liste'); ?>
            </div>
            <div class="content">

            </div>
            <div class="controls">

            </div>
        </td>
        <td id="infos">
            <div class="title">
                <?php echo __d('profil', 'Profil.infos'); ?>
            </div>
            <div class="content">

            </div>
            <div class="controls">

            </div>
        </td>
    </tr>
</table>

<script type="text/javascript">

    gui.buttonbox({
        element: $('#liste .controls')
    });

    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: "<?php echo __d('default', 'Button.add'); ?>",
            action: function () {
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/Profils/add"; ?>",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }
        }
    });


    gui.buttonbox({
        element: $('#infos .controls'),
        align: "center"
    });

    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                action: function () {
                    if (!$(this).hasClass('ui-state-disabled')) {
                        $('#infos .content').empty();
                        $('#liste table tr').removeClass('ui-state-focus');
                        gui.disablebutton({
                            element: $('#infos .controls'),
                            button: "<?php echo __d('default', 'Button.submit'); ?>"
                        });
                        gui.disablebutton({
                            element: $('#infos .controls'),
                            button: "<?php echo __d('default', 'Button.cancel'); ?>"
                        });
                    }
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                action: function () {
                    if (!$(this).hasClass('ui-state-disabled')) {

                        var errorFormTabs = [];
                        var areValid = true;
                        $('#infos .content form').each(function () {
                            if ($(this).attr('action') !== undefined && $(this).attr('enctype') != "multipart/form-data") {
                                if (!form_validate($(this))) {
                                    areValid = false;
                                }
                            }
                        });

                        if (areValid) {
                            var submits = {};
                            var index = 0;
                            $('#infos .content form').each(function () {
                                if ($(this).attr('action') !== undefined && $(this).attr('enctype') != "multipart/form-data") {
                                    submits[index] = {'url': $(this).attr('action'), 'data': $(this).serialize()};
                                    index++;
                                }
                            });
                            submits[index] = 'end';
                            adminSubmitAll(submits, 0);
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }
            }
        ]
    });


    gui.request({
        url: "<?php echo Configure::read('BaseUrl') . "/profils/getProfils"; ?>",
        updateElement: $('#liste .content'),
        loader: true,
        loaderMessage: gui.loaderMessage
    });

</script>
