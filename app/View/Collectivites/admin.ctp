<?php

/**
 *
 * Collectivites/admin.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */

?>
<?php
//echo $this->Html->css(array('administration'), null, array('inline' => false));
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<script type="text/javascript">
    function adminSubmitAll(submits, index) {
        if (submits[index] != 'end') {
            gui.request({
                url: submits[index].url,
                data: submits[index].data,
                loaderElement: $('#webgfc_content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                adminSubmitAll(submits, index + 1);
                getJsonResponse(data);
            });
        }
    }

    function getLogo(first) {
        var loaderElement = null;
        var loaderMessage = null;

        if (!first) {
            loaderElement = $("#logo").parent().parent();
            loaderMessage = gui.loaderMessage;
        }

        gui.request({
            url: '/collectivites/getLogo/' + <?php echo $this->request->data['Collectivite']['id']; ?>,
            noErrorMsg: true,
            loaderElement: loaderElement,
            loaderMessage: loaderMessage
        }, function (data) {
            $("#logo").empty();
            $("#logo").append(data);
        });
    }

</script>

<!--environnement/index/0/admin-->

<div class="container">
    <div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
    <div id="infos" class="row">
        <div class="table-list">
            <h3 style="border-radius:0px!important;"><?php echo __d('default', 'Informations'); ?></h3>
            <ul class="nav nav-tabs titre" role="tablist" style="border-radius:0px 0px 0 12px!important;">
                <li class="active"><a href="#tabs-2" role="tab" data-toggle="tab" class="nab-tabs-btn"><i class="fa fa-info" aria-hidden="true"></i> Informations générales</a></li>

                <li><a href="#tabs-4" role="tab" data-toggle="tab" class="nab-tabs-btn"><i class="fa fa-sliders" aria-hidden="true"></i> Configuration</a></li>
                <li><a href="#tabs-7" role="tab" data-toggle="tab" class="nab-tabs-btn"><i class="fa fa-user-secret" aria-hidden="true"></i> Politique de confidentialité</a></li>
            </ul>
            <?php
            $formCollectivite = array(
                'name' => 'Collectivite',
                'label_w' => 'col-sm-5',
                'input_w' => 'col-sm-6',
                'form_url' => array( 'controller' => 'collectivites', 'action' => 'admin'),
                'input' => array(
                )
            );
            echo $this->Formulaire->createForm($formCollectivite);
            ?>
            <div class="tab-content container">
                <div id="tabs-2" class="tab-pane fade active in" >
                    <div id="panel" class=" col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-info" aria-hidden="true"></i> <?php echo __d('collectivite', 'Informations'); ?></h4>
                            </div>
                            <div class="panel-body form-horizontal">
                                <?php
                                $formCollectiviteInfos = array(
                                    'name' => 'Collectivite',
                                    'label_w' => 'col-sm-4',
                                    'input_w' => 'col-sm-8',
                                    'form_url' => array( 'controller' => 'collectivites', 'action' => 'admin'),
                                    'input' => array(
                                        'Collectivite.id' => array(
                                            'inputType' => 'hidden',
                                            'items'=>array(
                                                'type' => 'hidden',
                                            )
                                        ),
                                        'Collectivite.name' => array(
                                            'labelText' => __d('collectivite', 'Collectivite.name'),
                                            'inputType' => 'text',
                                            'items'=>array(
                                                'type' => 'text',
                                                'required' => true
                                            )
                                        ),
                                        'Collectivite.siren' => array(
                                            'labelText' => __d('collectivite', 'Collectivite.siren'),
                                            'inputType' => 'number',
                                            'items'=>array(
                                                'type' => 'number',
//                                                'required' => true,
                                                'max'=>"999999999",
                                                'min'=>"0",
                                                'data-minlength'=>"9"
                                            )
                                        ),
                                        'Collectivite.codeinsee' => array(
                                            'labelText' => __d('collectivite', 'Collectivite.codeinsee'),
                                            'inputType' => 'number',
                                            'items'=>array(
                                                'type' => 'number',
//                                                'required' => true,
                                                'max'=>"99999",
                                                'min'=>"0",
                                                'data-minlength'=>"5"
                                            )
                                        )
                                    )
                                );
                                echo $this->Formulaire->createForm($formCollectiviteInfos);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div id="panel" class=" col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><i class="fa fa-book" aria-hidden="true"></i> <?php echo __d('collectivite', 'Coordonnées'); ?></h4>
                            </div>
                            <div class="panel-body form-horizontal">
                                <?php
                                $formCollectiviteCoordonnees = array(
                                    'name' => 'Collectivite',
                                    'label_w' => 'col-sm-5',
                                    'input_w' => 'col-sm-7',
                                    'form_url' => array( 'controller' => 'collectivites', 'action' => 'admin'),
                                    'input' => array(
                                        'Collectivite.adresse' => array(
                                            'labelText' => __d('collectivite', 'Collectivite.adresse'),
                                            'labelPlaceholder' => __d('collectivite', 'Collectivite.adresse.title'),
                                            'inputType' => 'text',
                                            'items'=>array(
                                                'type' => 'text',
                                                'title' => __d('collectivite', 'Collectivite.adresse.title'),
//                                                'required' => true
                                            )
                                        ),
                                        'Collectivite.complementadresse' => array(
                                            'labelText' => __d('collectivite', 'Collectivite.complementadresse'),
                                            'inputType' => 'text',
                                            'items'=>array(
                                                'type' => 'text'
                                            )
                                        ),
                                        'Collectivite.codepostal' => array(
                                            'labelText' =>  __d('collectivite', 'Collectivite.codepostal'),
                                            'inputType' => 'number',
                                            'items'=>array(
                                                'type' => 'number',
//                                                'required' => true,
                                                'max'=>"99999",
                                                'min'=>"0",
                                                'data-minlength'=>"5"
                                            )
                                        ),
                                        'Collectivite.ville' => array(
                                            'labelText' =>__d('collectivite', 'Collectivite.ville'),
                                            'inputType' => 'text',
                                            'items'=>array(
                                                'type' => 'text',
//                                                'required' => true
                                            )
                                        ),
                                        'Collectivite.telephone' => array(
                                            'labelText' =>__d('collectivite', 'Collectivite.telephone'),
                                            'inputType' => 'text',
                                            'items'=>array(
                                                'type' => 'text',
//                                                'required' => true
                                            )
                                        )
                                    )
                                );
                                echo $this->Formulaire->createForm($formCollectiviteCoordonnees);
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="tabs-4" class="tab-pane fade">
                    <div class="col-sm-6">
                        <div id="panel">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-dot-circle-o" style="font-size:16px;"  aria-hidden="true"></i> <?php echo __d('collectivite', 'Collectivite.active_state'); ?></h4>
                                </div>
                                <div class="panel-body form-horizontal" >
                                    <?php
                                    $formCollectiviteActiveState = array(
                                        'name' => 'Collectivite',
                                        'label_w' => 'col-sm-5',
                                        'input_w' => 'col-sm-5',
                                        'form_url' => array( 'controller' => 'collectivites', 'action' => 'admin'),
                                        'input' => array(
                                            'Collectivite.active' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.active'),
                                                'inputType' => 'checkbox',
                                                'items'=>array(
                                                    'type'=>'checkbox',
                                                    'checked'=>$this->request->data['Collectivite']['active']
                                                )
                                            )
                                        )
                                    );
                                    echo $this->Formulaire->createForm($formCollectiviteActiveState);
                                    ?>
                                </div>
                            </div>
                        </div>
						<div id="panelapiKey" >
							<div class="panel panel-default" >
								<div class="panel-heading">
									<h4 class="panel-title"><i class="fa fa-key" aria-hidden="true"></i> Clé d'API</h4>
								</div>
								<div class="panel-body form-horizontal" >
									<?php
									$formCollectiviteApiKey = array(
											'name' => 'Collectivite',
											'label_w' => 'col-sm-5',
											'input_w' => 'col-sm-5',
											'form_url' => array( 'controller' => 'collectivites', 'action' => 'edit'),
											'input' => array(
													'Collectivite.apitoken' => array(
															'labelText' => "Clé d'API",
															'inputType' => 'text',
															'items'=>array(
																	'type'=>'text',
																	'value' => $this->data['Collectivite']['apitoken']
															)
													)
											)
									);
									echo $this->Formulaire->createForm($formCollectiviteApiKey);
									?>
								</div>

								<?php $apiTokenGen = '<i class="fa fa-refresh" id=apiTokenGen name=apiTokenGen title="Regénérer"></i>';?>
								<script type="text/javascript">
									var apiTokenGen = $('<?php echo $apiTokenGen; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -50px auto auto', cursor: 'pointer'}).click(function () {
										swal({
											showCloseButton: true,
											title: "<?php echo __d('default', 'Confirmation de génération'); ?>",
											text: "<?php echo __d('default', "Êtes-vous sûr de vouloir modifier la clé d'API ? <br />  <br />Si oui, il vous faudra communiquer la nouvelle clé aux applications tierces <br />se connectant précédemment avec l'ancienne clé"); ?>",
											type: "warning",
											showCancelButton: true,
											confirmButtonColor: "#d33",
											cancelButtonColor: "#3085d6",
											confirmButtonText: '<i class="fa fa-refresh" aria-hidden="true"></i> Regénérer',
											cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
										}).then(function (data) {
											if (data) {
												gui.request({
													url: "/collectivites/genApiKey/" + <?php echo $this->request->data['Collectivite']['id'];?>,
													data: $(this).parents('form').serialize(),
													updateElement: $('#panelapiKey'),
													loader: true,
													loaderMessage: gui.loaderMessage
												}, function (data) {
													var s = getJsonResponse(data)['message'];
													var newapiValue = s.substring(  0, s.indexOf('<hr />') );
													$('#CollectiviteApitoken').val(newapiValue );
													getJsonResponse(data);
												});
											} else {
												swal({
													showCloseButton: true,
													title: "Annulé!",
													text: "Vous n\'avez pas modifié la clé.",
													type: "error",

												});
											}
										});
									}).appendTo($('#CollectiviteApitoken').parent());
								</script>
							</div>
						</div>
                        <div id="panel" >
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-sign-in" style="font-size:16px;" aria-hidden="true"></i> <?php echo __d('collectivite', 'cakephp.conn'); ?></h4>
                                </div>
                                <div class="panel-body form-horizontal">
                                    <?php
                                    $formCollectiviteCakephpConn = array(
                                        'name' => 'Collectivite',
                                        'label_w' => 'col-sm-5',
                                        'input_w' => 'col-sm-7',
                                        'form_url' => array( 'controller' => 'collectivites', 'action' => 'admin'),
                                        'input' => array(
                                            'Collectivite.conn' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.conn'),
                                                'inputType' => 'text',
                                                'items'=>array(
                                                    'type'=>'text',
                                                    'required'=>true
                                                )
                                            ),
                                            'Collectivite.login_suffix' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.login_suffix'),
                                                'inputType' => 'text',
                                                'items'=>array(
                                                    'type'=>'text',
                                                    'required'=>true
                                                )
                                            )
                                        )
                                    );
                                    echo $this->Formulaire->createForm($formCollectiviteCakephpConn);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div id="panel" >
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-cloud" style="font-size:16px;"  aria-hidden="true"></i> <?php echo __d('collectivite', 'scan.use'); ?></h4>
                                </div>
                                <div class="panel-body form-horizontal" >
                                    <?php
                                    $scanRemote = Configure::read('scan.remote');
                                    $optionsScanUse = array('local' => __d('collectivite', 'scan.local'));
                                    if (!empty($scanRemote) && in_array(true, $scanRemote, true)) {
                                        $optionsScanUse['remote'] = __d('collectivite', 'scan.remote');
                                    }
                                    if (Configure::read('scan.imap')) {
                                        $optionsScanUse['imap'] = __d('collectivite', 'scan.imap');
                                    }
                                    $optionsScanDesktopId = array(
                                        'Initiateurs' => $scanDestInitRoles,
                                        'Aiguilleurs' => $scanDestDispRoles
                                    );
                                    $formCollectiviteScanUse = array(
                                        'name' => 'Collectivite',
                                        'label_w' => 'col-sm-5',
                                        'input_w' => 'col-sm-7',
                                        'form_url' => array( 'controller' => 'collectivites', 'action' => 'admin'),
                                        'input' => array(
                                            'Collectivite.scan_active' => array(
                                                'labelText' => __d('collectivite', 'Collectivite.scan_active'),
                                                'inputType' => 'checkbox',
                                                'items'=>array(
                                                    'type'=>'checkbox',
                                                    'checked'=>$this->request->data['Collectivite']['scan_active']
                                                )
                                            ),
                                            'Collectivite.scan_access_type' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.scan_access_type'),
                                                'inputType' => 'select',
                                                'items'=>array(
                                                    'type'=>'select',
                                                    'class'=>'scanparam',
                                                    'options'=>$optionsScanUse
                                                )
                                            ),
                                            'Collectivite.scan_desktop_id' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.scan_desktop_id'),
                                                'inputType' => 'select',
                                                'items'=>array(
                                                    'type'=>'select',
                                                    'class'=>'scanparam',
                                                    'options'=>$scanDesktops,
                                                    'empty' => true
                                                )
                                            ),
                                            'Collectivite.scan_purge_delay' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.scan_purge_delay'),
                                                'inputType' => 'text',
                                                'items'=>array(
                                                    'type'=>'text',
                                                    'class'=>'scanparam'
                                                )
                                            )
                                        )
                                    );
                                    echo $this->Formulaire->createForm($formCollectiviteScanUse);
                                    ?>
                                    <legend><i class="fa fa-folder-open" style="font-size:16px;"  aria-hidden="true"></i> <?php echo __d('collectivite', 'scan.local'); ?></legend>
                                    <?php
                                    $formCollectiviteScanLocal = array(
                                        'name' => 'Collectivite',
                                        'label_w' => 'col-sm-5',
                                        'input_w' => 'col-sm-7',
                                        'form_url' => array( 'controller' => 'collectivites', 'action' => 'admin'),
                                        'input' => array(
                                            'Collectivite.scan_local_path' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.scan_local_path'),
                                                'inputType' => 'text',
                                                'items'=>array(
                                                    'type'=>'text',
                                                    'class'=>'scanparam'
                                                )
                                            )
                                        )
                                    );
                                    echo $this->Formulaire->createForm($formCollectiviteScanLocal);
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php
                        if (!empty($scanRemote) && in_array(true, $scanRemote, true)) {
                    ?>
                        <div id="panel">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="fa fa-folder-open" style="font-size:16px;"  aria-hidden="true"></i> <?php echo __d('collectivite', 'scan.remote'); ?></h4>
                                </div>
                                <div class="panel-body form-horizontal">
                                    <?php
                                    $optionsScanRemote = array();
                                    if (Configure::read('scan.remote.ftp')) {
                                            $optionsScanRemote['ftp'] = __d('collectivite', 'scan.remote.ftp');
                                    }
                                    if (Configure::read('scan.remote.ssh')) {
                                            $optionsScanRemote['ssh'] = __d('collectivite', 'scan.remote.ssh');
                                    }
                                    if (Configure::read('scan.remote.smb')) {
                                            $optionsScanRemote['smb'] = __d('collectivite', 'scan.remote.smb');
                                    }
                                    $formCollectiviteScanRemote= array(
                                        'name' => 'Collectivite',
                                        'label_w' => 'col-sm-5',
                                        'input_w' => 'col-sm-7',
                                        'form_url' => array( 'controller' => 'collectivites', 'action' => 'admin'),
                                        'input' => array(
                                            'Collectivite.scan_remote_type' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.scan_remote_type'),
                                                'inputType' => 'select',
                                                'items'=>array(
                                                    'type'=>'select',
                                                    'class'=>'scanparam',
                                                    'options' => $optionsScanRemote
                                                )
                                            ),
                                            'Collectivite.scan_remote_url' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.scan_remote_url'),
                                                'inputType' => 'text',
                                                'items'=>array(
                                                    'type'=>'text',
                                                    'class' =>'scanparam'
                                                )
                                            ),
                                            'Collectivite.scan_remote_port' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.scan_remote_port'),
                                                'inputType' => 'text',
                                                'items'=>array(
                                                    'type'=>'text',
                                                    'class'=>'scanparam'
                                                )
                                            ),
                                            'Collectivite.scan_remote_user' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.scan_remote_user'),
                                                'inputType' => 'text',
                                                'items'=>array(
                                                    'type'=>'text',
                                                    'class' =>'scanparam'
                                                )
                                            ),
                                            'Collectivite.scan_remote_pass' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.scan_remote_pass'),
                                                'inputType' => 'text',
                                                'items'=>array(
                                                    'type'=>'text',
                                                    'class'=>'scanparam'
                                                )
                                            ),
                                            'Collectivite.scan_remote_path' => array(
                                                'labelText' =>__d('collectivite', 'Collectivite.scan_remote_path'),
                                                'inputType' => 'text',
                                                'items'=>array(
                                                    'type'=>'text',
                                                    'class' =>'scanparam'
                                                )
                                            )
                                        )
                                    );
                                    echo $this->Formulaire->createForm($formCollectiviteScanRemote);
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                    <div class="col-sm-6">
                        <div id="panel">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">LDAP</h4>
                        </div>
                        <div class="panel-body form-horizontal">
                            <legend><i class="fa fa-database" aria-hidden="true"></i> Activation de l'identification via LDAP</legend>
                            <?php

                                $true_false = array('true' => 'Oui', 'false' => 'Non');
                                $formLDAP = array(
                                        'name' => 'Connecteur',
                                        'label_w' => 'col-sm-6',
                                        'input_w' => 'col-sm-6',
                                        'form_url' =>array('controller' => 'collectivites', 'action' => 'admin'),
                                        'form_type' => 'file',
                                        'input' => array(
                                            'Connecteur.id' => array(
                                                'inputType' => 'hidden',
                                                'items'=>array(
                                                    'type' => 'hidden',
                                                    'value' => $connecteurId
                                                )
                                            )
                                        )
                                    );
                                    echo $this->Formulaire->createForm($formLDAP);
                                ?>
                                    <?php
                                    $formLdapRadio = array(
                                        'name' => 'Connecteur',
                                        'label_w'=>'col-sm-5',
                                        'input_w'=>'col-sm-6',
                                        'form_url' =>array('controller' => 'collectivites', 'action' => 'admin'),
                                        'input' =>array(
                                            'use_ldap' => array(
                                                'labelText' =>"",
                                                'inputType' => 'radio',
                                                'items'=>array(
                                                    'legend' => false,
                                                    'separator'=> '</div><div  class="radioUseLdapLabel">',
                                                    'before' => '<div class="radioUseLdapLabel">',
                                                    'after' => '</div>',
                                                    'label' => true,
                                                    'type'=>'radio',
                                                    'options' => $true_false,
                                                    'value' => ( isset( $this->request->data['Connecteur']['use_ldap'] ) && $this->request->data['Connecteur']['use_ldap'] ) ? 'true' : 'false'
                                                )
                                            )
                                        )
                                    );
                                    echo $this->Formulaire->createForm($formLdapRadio);
                                    ?>
                            <div id='affiche' class="panel-body form-horizontal">
                                <legend><i class="fa fa-sliders" style="font-size:16px;" aria-hidden="true"></i> <?php echo __d('connecteur', 'Connecteur.ldap_server'); ?></legend>
                             <?php
                                $formConnecteurLdapServer = array(
                                   'name' => 'Connecteur',
                                   'label_w' => 'col-sm-5',
                                   'input_w' => 'col-sm-7',
                                   'form_url' =>array('controller' => 'collectivites', 'action' => 'admin'),
                                   'input' => array(
                                       'Connecteur.ldap_type' => array(
                                           'labelText' => __d('connecteur', 'Connecteur.ldap_type'),
                                           'inputType' => 'select',
                        //                   'changeWidth' =>true,
                                           'items'=>array(
                                               'type'=>'select',
                                               'class' =>'ldapparam',
                                               'options'=> $type,
                                               'empty' => true
                                           )
                                       ),
                                       'Connecteur.ldap_host' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_host'),
                                           'inputType' => 'text',
                                           'labelPlaceholder' => "Exemple : http://ldap.x.x.x",
                                           'items'=>array(
                                               'type'=>'text',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_has_fall_over' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_has_fall_over'),
                                           'inputType' => 'text',
                                           'labelPlaceholder' => "Exemple : http://ldap.x.x.x",
                                           'items'=>array(
                                               'type'=>'text',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_port' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_port'),
                                           'inputType' => 'number',
                                           'labelPlaceholder' => "Exemple : 389",
                                           'items'=>array(
                                               'type'=>'number',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_login' => array(
                                           'labelText' => __d('connecteur', 'Connecteur.ldap_login'),
                                           'inputType' => 'text',
                                           'items'=>array(
                                               'type'=>'text',
                                               'class' =>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_password' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_password'),
                                           'inputType' => 'password',
                                           'items'=>array(
                                               'type'=>'password',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_account_suffix' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_account_suffix'),
                                           'inputType' => 'text',
                                           'items'=>array(
                                               'type'=>'text',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_base_dn' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_base_dn'),
                                           'inputType' => 'text',
                                           'items'=>array(
                                               'type'=>'text',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_uniqueid' => array(
                                           'labelText' => __d('connecteur', 'Connecteur.ldap_uniqueid'),
                                           'inputType' => 'text',
                                           'items'=>array(
                                               'type'=>'text',
                                               'class' =>'ldapparam'
                                           )
                                       ),
                        //               'Connecteur.ldap_dn' => array(
                        //                   'labelText' =>__d('connecteur', 'Connecteur.ldap_dn'),
                        //                   'inputType' => 'text',
                        //                   'items'=>array(
                        //                       'type'=>'text',
                        //                       'class'=>'ldapparam'
                        //                   )
                        //               ),
                                       'Connecteur.ldap_tls' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_tls'),
                                           'inputType' => 'select',
                                           'items'=>array(
                                               'type'=>'select',
                                               'options' => array( true => 'Oui', false => 'Non'),
                                               'empty' => true,
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_version' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_version'),
                                           'inputType' => 'text',
                                           'labelPlaceholder' => "La version 3 du protocole est la plus courante",
                                           'items'=>array(
                                               'type'=>'text',
                                               'class'=>'ldapparam'
                                           )
                                       )

                                   )
                                );
                                echo $this->Formulaire->createForm($formConnecteurLdapServer);
                            ?>
								<fieldset class='ldap-infos'>
									<legend>Certificat d'authentification</legend>
									<?php
									$formLdapsCertificatAuthentification = array(
											'name' => 'Connecteur',
											'label_w' => 'col-sm-5',
											'input_w' => 'col-sm-6',
											'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'ldap'),
											'form_type' => 'file',
											'input' => array(
													'Connecteur.ldaps_cert' => array(
															'labelText' =>'Certificat (crt)',
															'inputType' => 'file',
															'items'=>array(
																	'type'=>'file',
																	'name' => 'myfile',
																	'class' => 'fileField'
															)
													)
											)
									);
									echo $this->Formulaire->createForm($formLdapsCertificatAuthentification);

									$value = @$this->request->data['Connecteur']['ldaps_nom_cert'];
									if( !empty( $value )) {
										echo 'Certificat utilisé : '.$value;
									}

									?>
								</fieldset>


                            </div>
                        </div>
                        </div>
                    </div>

                    </div>
                </div>

                 <?php
                    echo $this->Form->create('Collectivite', array(
                                        'class' => 'form-horizontal',
                                        'url' => array('controller' => 'collectivites', 'action' => 'admin'),
                                        'inputDefaults' => array(
                                            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                                            'div' => array('class' => 'form-group'),
                                            'label' => array('class' => 'control-label  col-sm-3'),
                                            'between' => '<div class="controls  col-sm-5">',
                                            'after' => '<div class="help-block with-errors"></div></div>',
                                            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                                            'data-toggle'=>"validator"
                                        )));
                ?>


				<div id="tabs-7" class="tab-pane fade">
					<div id="panel" class=" col-sm-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title"><?php echo __d('collectivite', 'Politique de confidentialité'); ?></h4>
							</div>
							<div class="panel-body form-horizontal">
								<?php
								$formCollectivitePolicy = array(
										'name' => 'Rgpd',
										'label_w' => 'col-sm-5',
										'input_w' => 'col-sm-5',
										'form_url' =>array('controller' => 'rgpds', 'action' => 'policy'),
										'input' => array(
												'Rgpd.id' => array(
													'inputType' => 'hidden',
													'items'=>array(
															'type' => 'hidden',
															'value' => $rgpdId
													)
												),
												'Rgpd.collectivite_id' => array(
														'inputType' => 'hidden',
														'items'=>array(
																'type' => 'hidden'
														)
												),
												'Rgpd.conn' => array(
														'inputType' => 'hidden',
														'items'=>array(
																'type' => 'hidden'
														)
												),
												'Rgpd.nomhebergeur' => array(
														'labelText' => __d('rgpd', 'Rgpd.nomhebergeur'),
														'inputType' => 'text',
														'items'=>array(
																'type' => 'text',
																'required' => true
														)
												),
												'Rgpd.adressecompletehebergeur' => array(
														'labelText' => __d('rgpd', 'Rgpd.adressecompletehebergeur'),
														'inputType' => 'text',
														'items'=>array(
																'type' => 'text',
																'required' => true
														)
												),
												'Rgpd.representanthebergeur' => array(
														'labelText' => __d('rgpd', 'Rgpd.representanthebergeur'),
														'inputType' => 'text',
														'items'=>array(
																'type' => 'text',
																'required' => true
														)
												),
												'Rgpd.qualiterepresentanthebergeur' => array(
														'labelText' => __d('rgpd', 'Rgpd.qualiterepresentanthebergeur'),
														'inputType' => 'text',
														'items'=>array(
																'type' => 'text',
																'required' => true
														)
												),
												'Rgpd.sirethebergeur' => array(
														'labelText' => __d('rgpd', 'Rgpd.sirethebergeur'),
														'inputType' => 'text',
														'items'=>array(
																'type' => 'text',
																'required' => true
														)
												),
												'Rgpd.codeapehebergeur' => array(
														'labelText' => __d('rgpd', 'Rgpd.codeapehebergeur'),
														'inputType' => 'text',
														'items'=>array(
																'type' => 'text',
																'required' => true
														)
												),
												'Rgpd.numtelhebergeur' => array(
														'labelText' => __d('rgpd', 'Rgpd.numtelhebergeur'),
														'inputType' => 'text',
														'items'=>array(
																'type' => 'text',
																'required' => true
														)
												),
												'Rgpd.mailhebergeur' => array(
														'labelText' => __d('rgpd', 'Rgpd.mailhebergeur'),
														'inputType' => 'text',
														'items'=>array(
																'type' => 'text',
																'required' => true
														)
												),
												'Rgpd.maildpo' => array(
														'labelText' => __d('rgpd', 'Rgpd.maildpo'),
														'inputType' => 'text',
														'items'=>array(
																'type' => 'text',
																'required' => true
														)
												)
										)
								);
								echo $this->Formulaire->createForm($formCollectivitePolicy);
								?>
							</div>
						</div>
					</div>
				</div>

                <?php echo $this->Form->end(); ?>


            </div>
        </div>

        <div class="controls panel-footer " role="group"></div>

    </div>
</div>
<script type="text/javascript">

    function showHideLdapSettings() {
        if ($('#CollectiviteLdapActive').prop("checked")) {
            $('#activeLdap').show();
//            $('.ldapparam').removeAttr('disabled');
        } else {
            $('#activeLdap').hide();
//            $('.ldapparam').attr('disabled', 'disabled');
        }
    }

    function showHideScanSettings() {
        if ($('#CollectiviteScanActive').prop("checked")) {
            $('.scanparam').removeAttr('disabled');
        } else {
            $('.scanparam').attr('disabled', 'disabled');
        }
    }

    gui.buttonbox({
        element: $('#infos .controls.panel-footer'),
        align: "center"
    });

    gui.addbuttons({
        element: $('#infos .controls.panel-footer'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success",
                action: function () {
                    var forms = $(this).parents('#infos').find('form');
                    $.each(forms, function (index, form) {
                        var url = form.action;
                        var id = form.id;
                        if ($(this).attr('action') !== undefined && $(this).attr('enctype') != "multipart/form-data") {
                            if (form_validate($('#' + id))) {
                                gui.request({
                                    url: url,
                                    data: $('#' + id).serialize()
                                }, function (data) {
                                    getJsonResponse(data);
                                });
                            } else {
                                swal({
                    showCloseButton: true,
                                    title: "Oops...",
                                    text: "Veuillez vérifier votre formulaire!",
                                    type: "error",

                                });
                            }
                        }
                    });
                }
            }
        ]
    });

    $(document).ready(function () {

        $('#UserUsername').blur(function () {
            gui.request({
                url: "/users/ajaxformvalid/username",
                data: $(this).parents('form').serialize()
            }, function (data) {
                processJsonFormCheck(data);
            })
        });
        $('#UserMail').blur(function () {
            gui.request({
                url: "/users/ajaxformvalid/mail",
                data: $(this).parents('form').serialize()
            }, function (data) {
                processJsonFormCheck(data);
            })
        });

        $('#CollectiviteName').blur(function () {
            gui.request({
                url: "/collectivites/ajaxformvalid/name",
                data: $(this).parents('form').serialize()
            }, function (data) {
                processJsonFormCheck(data);
            })
        });

        showHideLdapSettings();
        $('#CollectiviteUseLdap').change(function () {
            showHideLdapSettings();
        });

        showHideScanSettings();
        $('#CollectiviteScanActive').change(function () {
            showHideScanSettings();
        });

        var ulfile = gui.createupload({
            button: "<?php echo __d('default', 'Button.uploadFile'); ?>",
            label: "Associer un logo",
            url: "/collectivites/upload",
            fileInputName: "myfile",
            hiddenItem: {
                name: "collectiviteId",
                value: <?php echo $this->request->data['Collectivite']['id']; ?>
            }
        });
        $('#logoUpload').append(ulfile);
        $('#logoUpload a').css('margin', '-2.1% auto auto 30%');

        getLogo(true);

    });

    $('#CollectiviteScanDesktopId').select2();
</script>
<script type="text/javascript">

    $('#CollectiviteAdminForm input[type=radio]').change(function () {
        $('#affiche').toggle();
    });

    if ($('#ConnecteurUseLdapTrue').is(":checked")) {
        $('#affiche').show();
    }

    if ($('#ConnecteurUseLdapFalse').is(":checked")) {
        $('#affiche').hide();
    }

    $('#CollectiviteConn').attr('disabled', 'disabled');


    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/admin"; ?>";
                }
            }
        ]
    });
</script>
