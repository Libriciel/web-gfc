<?php

/**
 *
 * Collectivites/get_collectivites.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (!empty($collectivites)) {
    $fields = array(
        'name',
        'siren',
        'ville',
        'telephone',
        'conn',
        'adminname',
        'login_suffix'
    );
    $actions = array(
        "edit" => array(
            "url" => '/collectivites/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false,
            'formMessageHeight' => '600',
            'formMessageWidth' => '1000',
            "refreshAction" => "loadStructure();",
            "divshow" =>"infos",
            "divhide" =>"liste"
        ),
        "delete" => array(
            "url" => '/collectivites/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadStructure();msgDelete();"
        )
    );
    $options = array('check_inactive' => true, 'inactive_value' => 0, 'inactive_field' => 'active', 'inactive_model' => 'Collectivite');

    $data = $this->Liste->drawTbody($collectivites, 'Collectivite', $fields, $actions, $options);
?>
<table id="table_collectivites"
       data-toggle="table"
       data-search="true"
       data-locale = "fr-CA"
       data-height="300"
       data-show-refresh="false"
       data-show-toggle="false"
       data-show-columns="true"
       data-toolbar="#toolbar">
</table>

<script>
    if (!$('#liste.table-list h3 span').length < 1) {
        $('#liste.table-list h3 span').remove();
    }
    $('#liste.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($collectivites,$options); ?>');
    $('#table_collectivites')
            .bootstrapTable({
                data:<?php echo $data;?>,
                height: '600px',
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d("collectivite","Collectivite.name"); ?>",
                        class: "name"
                    },
                    {
                        field: "siren",
                        title: "<?php echo __d("collectivite","Collectivite.siren"); ?>",
                        class: "siren"
                    },
                    {
                        field: "ville",
                        title: "<?php echo __d("collectivite","Collectivite.ville"); ?>",
                        class: "ville"
                    },
                    {
                        field: "telephone",
                        title: "<?php echo __d("collectivite","Collectivite.telephone"); ?>",
                        class: "telephone"
                    },
					{
                        field: "conn",
                        title: "<?php echo __d("collectivite","Collectivite.conn"); ?>",
                        class: "conn"
                    },
					{
						field: "adminname",
						title: "Administrateur",
						class: "adminname"
					},
					{
                        field: "login_suffix",
                        title: "<?php echo __d("collectivite","Collectivite.login_suffix"); ?>",
                        class: "login_suffix"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            });
</script>
<?php
echo $this->LIste->drawScript($collectivites, 'Collectivite', $fields, $actions, $options);
echo $this->Js->writeBuffer();
?>
<?php
} else {
    echo $this->Html->tag('div', __d('collectivite', 'Collectivite.void'), array('class' => 'alert alert-warning'));
}
?>
