<?php

/**
 *
 * Collectivites/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//echo $this->Html->css(array('administration'), null, array('inline' => false));
?>

<script type="text/javascript">

    function loadStructure() {
        $('#infos .content').empty();
        gui.request({
            url: "/collectivites/getCollectivites",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

    function msgDelete() {
        swal(
                'Oops...',
                "Attention : à lire après une suppression de collectivité<br>Pensez a supprimer la base de données ainsi que ses paramètres de connexion dans le fichier \'APP/Config/database.php\'.<br /><br />Si vous conservez la base et les paramètres, pensez à supprimer toutes les tables de la base de données.",
                'warning'
                );
    }

</script>

<div class="container" >
    <div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
    <div class="row">
        <div class=" table-list liste-collectivites" id="liste">
            <h3><?php echo __d('collectivite', 'Collectivite.liste'); ?><a href="#" class="btn btn-primary btn-title-add" id="btn_add_coll" title="<?php echo __d('default', 'Button.add'); ?>"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></h3>
            <div class="panel-body content">
            </div>
            <div class="panel-footer controls">
            </div>
        </div>

        <div class="table-list infos-collectivites" id="infos" style="display: none">
            <h3 ><?php echo __d('collectivite', 'Collectivite.infos'); ?></h3>
            <div class="panel-body content">
            </div>
            <div class="panel-footer controls " role="group">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#btn_add_coll').click(function () {
        gui.request({
            url: "/Collectivites/add",
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage,
            showdiv: 'infos'
        });
        $('#liste').hide();
    });
    gui.buttonbox({
        element: $('#liste .controls')
    });
    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> <?php echo __d('default', 'Button.add'); ?>',
            action: function () {
                gui.request({
                    url: "/Collectivites/add",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                    showdiv: 'infos'
                });
                $('#liste').hide();
            }
        }
    });
    gui.buttonbox({
        element: $('#infos .controls'),
        align: "center"
    });
    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    if (!$(this).hasClass('ui-state-disabled')) {
                        $('#infos .content').empty();
                        $('#liste table tr').removeClass('ui-state-focus');
                        $('#liste').show();
                        $('#infos').hide();
                    }
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success ",
                action: function () {
                    var forms = $(this).parents('#infos').find('form');
                    $.each(forms, function (index, form) {
                        var url = form.action;
                        var id = form.id;
                        if (form_validate($('#' + id))) {
                            gui.request({
                                url: url,
                                data: $('#' + id).serialize()
                            }, function (data) {
                                getJsonResponse(data);
                                loadStructure();
                            });
                            $('#liste').show();
                            $('#infos').hide();
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    });
                }
            }
        ]
    });
    gui.request({
        url: "/collectivites/getCollectivites",
        updateElement: $('#liste .content'),
        loader: true,
        loaderMessage: gui.loaderMessage
    }, function (data) {
        $('#liste .content').html(data);
    });


    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
                }
            }
        ]
    });
</script>
