<?php

/**
 *
 * Collectivites/fill_option_list.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php

echo '<option value="">' . __d('collectivite', 'chooseCollectivite') . '</option>';
foreach ($collectiviteOptionList as $key => $val) {
    echo '<option value="' . $key . '"';
    if (!empty($id) && $id = $key) {
        echo ' selected="selected"';
    }
    echo '">' . $val . '</option>';
}
?>
