<?php

/**
 *
 * Collectivites/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */

?>
<?php
echo $this->Html->script('compteurs.js');
//echo $this->Html->css(array('administration'), null, array('inline' => false));
echo $this->Html->script('formValidate.js', array('inline' => true));
?>

<div id="tabsCollectivite">
    <ul class="nav nav-tabs titre" role="tablist">
        <li class="active"><a href="#tabs-1" role="tab" data-toggle="tab">Pré-requis</a></li>
        <li><a href="#tabs-2" role="tab" data-toggle="tab">Informations générales</a></li>
        <li><a href="#tabs-3" role="tab" data-toggle="tab">Administrateur</a></li>
        <li><a href="#tabs-4" role="tab" data-toggle="tab">Configuration</a></li>
        <li><a href="#tabs-5" role="tab" data-toggle="tab">Politique de confidentialité</a></li>
    </ul>

    <?php
        $formCollectivite = array(
            'name' => 'Collectivite',
            'label_w' => 'col-sm-4',
            'input_w' => 'col-sm-5',
            'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
            'input' => array(
            )
        );
        echo $this->Formulaire->createForm($formCollectivite);
    ?>

    <div class="tab-content row">
        <div id="tabs-1" class="tab-pane fade active in" style="margin-top: 10px;" >
            <div class="col-sm-5">
                <div class="alert alert-info"><i class="fa fa-info"  aria-hidden="true"></i> Avant de créer une collectivité, vous devez effectuer le paramétrage de la connexion!</div>
                <p >Editer le fichier 'APP/Config/database.php' et ajouter les informations suivantes après celles déjà définies et avant la dernière accoclade " } ":</p>
                <p class="alert alert-warning" role="alert"><b>Attention :</b> le nom des tables est prédéfini, l'attribut 'prefix' doit rester vide !</p>
            </div>
            <div class="col-sm-6 ">
                <pre>
        public $nom_de_la_connexion = array(<br />
            'datasource' => 'Database/Postgres',<br />
            'persistent' => false,<br />
            'host' => 'adresse_du_serveur_de_base_de_données',<br />
            'login' => 'identifiant_base_de_données',<br />
            'password' => 'mot_de_passe_base_de_données',<br />
            'database' => 'nom_de_la_base_de_données',<br />
            'prefix' => '',<br />
            'port' => 'port_du_serveur_de_base_de_données',<br />
            'encoding' => 'utf8'<br />
        );<br />
                </pre>
                <div class="form-group" id="btn_preparation">
                    <label class="control-label col-sm-8" for="paramconn">Opération effectuée  </label><div class="controls col-sm-1"><input type="checkbox" name="paramconn" id="paramconn" required></div><div class="help-block with-errors"></div>
                </div>
            </div>
        </div>

        <div id="tabs-2" class="tab-pane fade" >
            <div id="panel" class=" col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-info" aria-hidden="true"></i> <?php echo __d('collectivite', 'Informations'); ?></h4>
                    </div>
                    <div class="panel-body form-horizontal">
                        <?php
                        $formCollectiviteInfos = array(
                            'name' => 'Collectivite',
                            'label_w' => 'col-sm-5',
                            'input_w' => 'col-sm-5',
                            'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                            'input' => array(
//                                'Collectivite.id' => array(
//                                    'inputType' => 'hidden',
//                                    'items'=>array(
//                                        'type' => 'hidden',
//                                    )
//                                ),
                                'Collectivite.name' => array(
                                    'labelText' => __d('collectivite', 'Collectivite.name'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text',
                                        'required' => true
                                    )
                                ),
                                'Collectivite.siren' => array(
                                    'labelText' => __d('collectivite', 'Collectivite.siren'),
                                    'inputType' => 'number',
                                    'items'=>array(
                                        'type' => 'number',
//                                        'required' => true,
                                        'max'=>"999999999",
                                        'min'=>"0",
                                        'data-minlength'=>"9"
                                    )
                                ),
                                'Collectivite.codeinsee' => array(
                                    'labelText' => __d('collectivite', 'Collectivite.codeinsee'),
                                    'inputType' => 'number',
                                    'items'=>array(
                                        'type' => 'number',
//                                        'required' => true,
                                        'max'=>"99999",
                                        'min'=>"0",
                                        'data-minlength'=>"5"
                                    )
                                )
                            )
                        );
                        echo $this->Formulaire->createForm($formCollectiviteInfos);
                        ?>
                    </div>
                </div>
            </div>
            <div id="panel" class=" col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-book" aria-hidden="true"></i> <?php echo __d('collectivite', 'Coordonnées'); ?></h4>
                    </div>
                    <div class="panel-body form-horizontal">
                        <?php
                         $formCollectiviteCoordonnees = array(
                            'name' => 'Collectivite',
                            'label_w' => 'col-sm-5',
                            'input_w' => 'col-sm-5',
                            'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                            'input' => array(
                                'Collectivite.adresse' => array(
                                    'labelText' => __d('collectivite', 'Collectivite.adresse'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text',
//                                        'required' => true
                                    )
                                ),
                                'Collectivite.complementadresse' => array(
                                    'labelText' => __d('collectivite', 'Collectivite.complementadresse'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text'
                                    )
                                ),
                                'Collectivite.codepostal' => array(
                                    'labelText' =>  __d('collectivite', 'Collectivite.codepostal'),
                                    'inputType' => 'number',
                                    'items'=>array(
                                        'type' => 'number',
//                                        'required' => true,
                                        'max'=>"99999",
                                        'min'=>"0",
                                        'data-minlength'=>"5"
                                    )
                                ),
                                'Collectivite.ville' => array(
                                    'labelText' =>__d('collectivite', 'Collectivite.ville'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text',
//                                        'required' => true
                                    )
                                ),
                                'Collectivite.telephone' => array(
                                    'labelText' =>__d('collectivite', 'Collectivite.telephone'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text',
//                                        'required' => true
                                    )
                                )
                            )
                        );
                        echo $this->Formulaire->createForm($formCollectiviteCoordonnees);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="tabs-3" class="tab-pane fade">
            <div id="panel" class=" col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-user" aria-hidden="true"></i> <?php echo __d('collectivite', 'admin.infoperso'); ?></h4>
                    </div>
                    <div class="panel-body form-horizontal">
                        <?php
                        $formCollectiviteInfoperso = array(
                            'name' => 'Collectivite',
                            'label_w' => 'col-sm-5',
                            'input_w' => 'col-sm-5',
                            'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                            'input' => array(
//                                'User.id' => array(
//                                    'inputType' => 'hidden',
//                                    'items'=>array(
//                                        'type' => 'hidden',
//                                    )
//                                ),
                                'User.username' => array(
                                    'labelText' => __d('user', 'User.username'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text',
                                        'required' => true
                                    )
                                ),
                                'User.nom' => array(
                                    'labelText' => __d('user', 'User.nom'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text',
                                        'required' => true
                                    )
                                ),
                                'User.prenom' => array(
                                    'labelText' => __d('user', 'User.prenom'),
                                    'inputType' => 'number',
                                    'items'=>array(
                                        'type' => 'text',
                                        'required' => true
                                    )
                                ),
                                'User.mail' => array(
                                    'labelText' => __d('user', 'User.mail'),
                                    'inputType' => 'email',
                                    'items'=>array(
                                        'type' => 'email',
                                        'required' => true
                                    )
                                )
                            )
                        );
                        echo $this->Formulaire->createForm($formCollectiviteInfoperso);
                        ?>
                    </div>
                </div>
            </div>
            <div id="panel" class=" col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-lock" aria-hidden="true"></i> <?php echo __d('collectivite', 'admin.chpass'); ?></h4>
                    </div>
                    <div class="panel-body form-horizontal">
                        <?php
                        $formCollectiviteChpass = array(
                            'name' => 'Collectivite',
                            'label_w' => 'col-sm-5',
                            'input_w' => 'col-sm-5',
                            'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                            'input' => array(
                                'User.password' => array(
                                    'labelText' => __d('user', 'User.password'),
                                    'inputType' => 'password',
                                    'items'=>array(
                                        'type' => 'password',
                                        'required' => true
                                    )
                                ),
                                'User.password2' => array(
                                    'labelText' => __d('user', 'Verif.User.password'),
                                    'inputType' => 'password',
                                    'items'=>array(
                                        'type' => 'password',
                                        'required' => true,
                                        'data-match'=>'#UserPassword',
                                        'data-match-error'=>"Whoops, ceux-ci ne correspondent pas"
                                    )
                                )
                            )
                        );
                        echo $this->Formulaire->createForm($formCollectiviteChpass);
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="tabs-4" class="tab-pane fade">
            <div class="col-sm-6">
                <div id="panel">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-dot-circle-o" style="font-size:16px;" aria-hidden="true"></i> <?php echo __d('collectivite', 'Collectivite.active_state'); ?></h4>
                        </div>
                        <div class="panel-body form-horizontal">
                            <?php
                            $formCollectiviteActiveState = array(
                                'name' => 'Collectivite',
                                'label_w' => 'col-sm-5',
                                'input_w' => 'col-sm-5',
                                'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                                'input' => array(
                                    'Collectivite.active' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.active'),
                                        'inputType' => 'checkbox',
                                        'items'=>array(
                                            'type'=>'checkbox',
                                            'checked'=>true
                                        )
                                    )
                                )
                            );
                            echo $this->Formulaire->createForm($formCollectiviteActiveState);
                            ?>
                        </div>
                    </div>
                </div>
                <div id="panel" >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-sign-in" style="font-size:16px;" aria-hidden="true"></i> <?php echo __d('collectivite', 'cakephp.conn'); ?></h4>
                        </div>
                        <div class="panel-body form-horizontal">
                            <?php
                            $formCollectiviteCakephpConn = array(
                                'name' => 'Collectivite',
                                'label_w' => 'col-sm-5',
                                'input_w' => 'col-sm-5',
                                'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                                'input' => array(
                                    'Collectivite.conn' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.conn'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type'=>'select',
                                            'options'=>$conns,
                                            'required'=>true
                                        )
                                    ),
                                    'Collectivite.login_suffix' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.login_suffix'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'required'=>true
                                        )
                                    )
                                )
                            );
                            echo $this->Formulaire->createForm($formCollectiviteCakephpConn);
                            ?>
                        </div>
                    </div>
                </div>
                <div id="panel">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-cloud" style="font-size:16px;" aria-hidden="true"></i> <?php echo __d('collectivite', 'scan.use'); ?></h4>
                        </div>
                        <div class="panel-body form-horizontal">
                            <?php
                            $scanRemote = Configure::read('scan.remote');
                            $optionsScanUse = array('local' => __d('collectivite', 'scan.local'));
                            if (!empty($scanRemote) && in_array(true, $scanRemote, true)) {
                                    $optionsScanUse['remote'] = __d('collectivite', 'scan.remote');
                            }
                            if (Configure::read('scan.imap')) {
                                    $optionsScanUse['imap'] = __d('collectivite', 'scan.imap');
                            }
                            $formCollectiviteScanUse = array(
                                'name' => 'Collectivite',
                                'label_w' => 'col-sm-5',
                                'input_w' => 'col-sm-5',
                                'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                                'input' => array(
                                    'Collectivite.scan_active' => array(
                                        'labelText' => __d('collectivite', 'Collectivite.scan_active'),
                                        'inputType' => 'checkbox',
                                        'items'=>array(
                                            'type'=>'checkbox'
                                        )
                                    ),
                                    'Collectivite.scan_access_type' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.scan_access_type'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type'=>'select',
                                            'class'=>'scanparam',
                                            'options'=>$optionsScanUse
                                        )
                                    ),
                                    'Collectivite.scan_purge_delay' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.scan_purge_delay'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'class'=>'scanparam'
                                        )
                                    )
                                )
                            );
                            echo $this->Formulaire->createForm($formCollectiviteScanUse);
                            ?>
                            <legend><i class="fa fa-folder-open" style="font-size:16px;" aria-hidden="true"></i> <?php echo __d('collectivite', 'scan.local'); ?></legend>
                                <?php
                                $formCollectiviteScanLocal = array(
                                    'name' => 'Collectivite',
                                    'label_w' => 'col-sm-5',
                                    'input_w' => 'col-sm-5',
                                    'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                                    'input' => array(
                                        'Collectivite.scan_local_path' => array(
                                            'labelText' =>__d('collectivite', 'Collectivite.scan_local_path'),
                                            'inputType' => 'text',
                                            'items'=>array(
                                                'type'=>'text',
                                                'class'=>'scanparam'
                                            )
                                        )
                                    )
                                );
                                echo $this->Formulaire->createForm($formCollectiviteScanLocal);
                                ?>
                        </div>
                    </div>
                </div>
<?php
    if (!empty($scanRemote) && in_array(true, $scanRemote, true)) {
?>
                <div id="panel" >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="fa fa-folder-open" style="font-size:16px;" aria-hidden="true"></i> <?php echo __d('collectivite', 'scan.remote'); ?></h4>
                        </div>
                        <div class="panel-body form-horizontal">
                            <?php
                            $optionsScanRemote = array();
                            if (Configure::read('scan.remote.ftp')) {
                                    $optionsScanRemote['ftp'] = __d('collectivite', 'scan.remote.ftp');
                            }
                            if (Configure::read('scan.remote.ssh')) {
                                    $optionsScanRemote['ssh'] = __d('collectivite', 'scan.remote.ssh');
                            }
                            if (Configure::read('scan.remote.smb')) {
                                    $optionsScanRemote['smb'] = __d('collectivite', 'scan.remote.smb');
                            }
                            $formCollectiviteScanRemote= array(
                                'name' => 'Collectivite',
                                'label_w' => 'col-sm-5',
                                'input_w' => 'col-sm-5',
                                'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                                'input' => array(
                                    'Collectivite.scan_remote_type' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.scan_remote_type'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type'=>'select',
                                            'class'=>'scanparam',
                                            'options' => $optionsScanRemote
                                        )
                                    ),
                                    'Collectivite.scan_remote_url' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.scan_remote_url'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'class' =>'scanparam'
                                        )
                                    ),
                                    'Collectivite.scan_remote_port' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.scan_remote_port'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'class'=>'scanparam'
                                        )
                                    ),
                                    'Collectivite.scan_remote_user' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.scan_remote_user'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'class' =>'scanparam'
                                        )
                                    ),
                                    'Collectivite.scan_remote_pass' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.scan_remote_pass'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'class'=>'scanparam'
                                        )
                                    ),
                                    'Collectivite.scan_remote_path' => array(
                                        'labelText' =>__d('collectivite', 'Collectivite.scan_remote_path'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'class' =>'scanparam'
                                        )
                                    )
                                )
                            );
                            echo $this->Formulaire->createForm($formCollectiviteScanRemote);
                            ?>
                        </div>
                    </div>
                </div>
<?php } ?>

            </div>
            <div class="col-sm-6">
                <div id="panel" >
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">LDAP</h4>
                        </div>
                         <div class="panel-body form-horizontal">
                            <legend><i class="fa fa-database" aria-hidden="true"></i> Activation de l'identification via LDAP</legend>
                            <?php

                                $true_false = array('true' => 'Oui', 'false' => 'Non');

                                ?>
                                    <?php
                                    $formLdapRadio = array(
                                        'name' => 'Collectivite',
                                        'label_w'=>'col-sm-5',
                                        'input_w'=>'col-sm-6',
//                                        'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                                        'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'ldap'),
                                        'form_type' => 'file',
                                        'input' =>array(
                                            'Connecteur.use_ldap' => array(
                                                'labelText' =>"",
                                                'inputType' => 'radio',
                                                'items'=>array(
                                                    'legend' => false,
                                                    'separator'=> '</div><div  class="radioUseLdapLabel">',
                                                    'before' => '<div class="radioUseLdapLabel">',
                                                    'after' => '</div>',
                                                    'label' => true,
                                                    'type'=>'radio',
                                                    'options' => $true_false,
                                                    'value' => ( isset( $this->request->data['Connecteur']['use_ldap'] ) && $this->request->data['Connecteur']['use_ldap'] ) ? 'true' : 'false'
                                                )
                                            )
                                        )
                                    );
                                    echo $this->Formulaire->createForm($formLdapRadio);
                                    ?>
                            <div id='affiche' class="panel-body form-horizontal">
                                <legend><i class="fa fa-sliders" style="font-size:16px;" aria-hidden="true"></i> <?php echo __d('connecteur', 'Connecteur.ldap_server'); ?></legend>
                             <?php
                                $formConnecteurLdapServer = array(
                                   'name' => 'Collectivite',
                                   'label_w' => 'col-sm-5',
                                   'input_w' => 'col-sm-7',
//                                   'form_url' => array( 'controller' => 'collectivites', 'action' => 'add'),
                                   'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'ldap'),
                                   'input' => array(
                                       'Connecteur.ldap_type' => array(
                                           'labelText' => __d('connecteur', 'Connecteur.ldap_type'),
                                           'inputType' => 'select',
                                           'items'=>array(
                                               'type'=>'select',
                                               'class' =>'ldapparam',
                                               'options'=> $type,
                                               'empty' => true
                                           )
                                       ),
                                       'Connecteur.ldap_host' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_host'),
                                           'inputType' => 'text',
                                           'labelPlaceholder' => "Exemple : http://ldap.x.x.x",
                                           'items'=>array(
                                               'type'=>'text',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_has_fall_over' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_has_fall_over'),
                                           'inputType' => 'text',
                                           'labelPlaceholder' => "Exemple : http://ldap.x.x.x",
                                           'items'=>array(
                                               'type'=>'text',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_port' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_port'),
                                           'inputType' => 'number',
                                           'labelPlaceholder' => "Exemple : 389",
                                           'items'=>array(
                                               'type'=>'number',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_login' => array(
                                           'labelText' => __d('connecteur', 'Connecteur.ldap_login'),
                                           'inputType' => 'text',
                                           'items'=>array(
                                               'type'=>'text',
                                               'class' =>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_password' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_password'),
                                           'inputType' => 'password',
                                           'items'=>array(
                                               'type'=>'password',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_account_suffix' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_account_suffix'),
                                           'inputType' => 'text',
                                           'items'=>array(
                                               'type'=>'text',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_base_dn' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_base_dn'),
                                           'inputType' => 'text',
                                           'items'=>array(
                                               'type'=>'text',
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_uniqueid' => array(
                                           'labelText' => __d('connecteur', 'Connecteur.ldap_uniqueid'),
                                           'inputType' => 'text',
                                           'items'=>array(
                                               'type'=>'text',
                                               'class' =>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_tls' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_tls'),
                                           'inputType' => 'select',
                                           'items'=>array(
                                               'type'=>'select',
                                               'options' => array( true => 'Oui', false => 'Non'),
                                               'empty' => true,
                                               'class'=>'ldapparam'
                                           )
                                       ),
                                       'Connecteur.ldap_version' => array(
                                           'labelText' =>__d('connecteur', 'Connecteur.ldap_version'),
                                           'inputType' => 'text',
                                           'labelPlaceholder' => "La version 3 du protocole est la plus courante",
                                           'items'=>array(
                                               'type'=>'text',
                                               'class'=>'ldapparam'
                                           )
                                       )

                                   )
                                );
                                echo $this->Formulaire->createForm($formConnecteurLdapServer);
                            ?>
								<fieldset class='ldap-infos'>
									<legend>Certificat d'authentification</legend>
									<?php
									$formLdapsCertificatAuthentification = array(
											'name' => 'Connecteur',
											'label_w' => 'col-sm-5',
											'input_w' => 'col-sm-6',
											'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'ldap'),
											'form_type' => 'file',
											'input' => array(
													'Connecteur.ldaps_cert' => array(
															'labelText' =>'Certificat (crt)',
															'inputType' => 'file',
															'items'=>array(
																	'type'=>'file',
																	'name' => 'myfile',
																	'class' => 'fileField'
															)
													)
											)
									);
									echo $this->Formulaire->createForm($formLdapsCertificatAuthentification);

									$value = @$this->request->data['Connecteur']['ldaps_nom_cert'];
									if( !empty( $value )) {
										echo 'Certificat utilisé : '.$value;
									}

									?>
								</fieldset>

                        </div>
                    </div>
                </div>

            </div>
        </div>

		</div>
		<div id="tabs-5" class="tab-pane fade">
				<div id="panel" class=" col-sm-10">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title"><?php echo __d('collectivite', 'Politique de confidentialité'); ?></h4>
						</div>
						<div class="panel-body form-horizontal">
							<?php
							$formCollectivitePolicy = array(
									'name' => 'Collectivite',
									'label_w' => 'col-sm-5',
									'input_w' => 'col-sm-5',
									'form_url' =>array('controller' => 'rgpds', 'action' => 'policy'),
									'input' => array(
											'Rgpd.collectivite_id' => array(
													'inputType' => 'hidden',
													'items'=>array(
															'type' => 'hidden'
													)
											),
											'Rgpd.conn' => array(
													'inputType' => 'hidden',
													'items'=>array(
															'type' => 'hidden'
													)
											),
											'Rgpd.nomhebergeur' => array(
													'labelText' => __d('rgpd', 'Rgpd.nomhebergeur'),
													'inputType' => 'text',
													'items'=>array(
															'type' => 'text',
															'required' => true
													)
											),
											'Rgpd.adressecompletehebergeur' => array(
													'labelText' => __d('rgpd', 'Rgpd.adressecompletehebergeur'),
													'inputType' => 'text',
													'items'=>array(
															'type' => 'text',
															'required' => true
													)
											),
											'Rgpd.representanthebergeur' => array(
													'labelText' => __d('rgpd', 'Rgpd.representanthebergeur'),
													'inputType' => 'text',
													'items'=>array(
															'type' => 'text',
															'required' => true
													)
											),
											'Rgpd.qualiterepresentanthebergeur' => array(
													'labelText' => __d('rgpd', 'Rgpd.qualiterepresentanthebergeur'),
													'inputType' => 'text',
													'items'=>array(
															'type' => 'text',
															'required' => true
													)
											),
											'Rgpd.sirethebergeur' => array(
													'labelText' => __d('rgpd', 'Rgpd.sirethebergeur'),
													'inputType' => 'text',
													'items'=>array(
															'type' => 'text'
													)
											),
											'Rgpd.codeapehebergeur' => array(
													'labelText' => __d('rgpd', 'Rgpd.codeapehebergeur'),
													'inputType' => 'text',
													'items'=>array(
															'type' => 'text'
													)
											),
											'Rgpd.numtelhebergeur' => array(
													'labelText' => __d('rgpd', 'Rgpd.numtelhebergeur'),
													'inputType' => 'text',
													'items'=>array(
															'type' => 'text',
															'required' => true
													)
											),
											'Rgpd.mailhebergeur' => array(
													'labelText' => __d('rgpd', 'Rgpd.mailhebergeur'),
													'inputType' => 'text',
													'items'=>array(
															'type' => 'text',
															'required' => true
													)
											),
											'Rgpd.maildpo' => array(
													'labelText' => __d('rgpd', 'Rgpd.maildpo'),
													'inputType' => 'text',
													'items'=>array(
															'type' => 'text',
															'required' => true
													)
											)
									)
							);
							echo $this->Formulaire->createForm($formCollectivitePolicy);
							?>
						</div>
					</div>
				</div>
			</div>


			<?php echo $this->Form->end(); ?>

</div>


<script type="text/javascript">
    function showHideLdapSettings() {
        if ($('#ConnecteurUseLdap').prop("checked")) {
            $('#activeLdap').show();
        } else {
            $('#activeLdap').hide();
        }
    }

    function showHideScanSettings() {
        if ($('#CollectiviteScanActive').prop("checked")) {
            $('.scanparam').removeAttr('disabled');
        } else {
            $('.scanparam').attr('disabled', 'disabled');
        }
    }


    $(document).ready(function () {
        showHideLdapSettings();
        $('#ConnecteurUseLdap').change(function () {
            showHideLdapSettings();
        });
        showHideScanSettings();
        $('#CollectiviteScanActive').change(function () {
            showHideScanSettings();
        });
        $('#CollectiviteName').blur(function () {
            gui.request({
                url: "/collectivites/ajaxformvalid/name",
                data: $(this).parents('form').serialize()
            }, function (data) {
                processJsonFormCheck(data);
            })
        });
        $('#CollectiviteLoginSuffix').blur(function () {
            gui.request({
                url: "/collectivites/ajaxformvalid/login_suffix",
                data: $(this).parents('form').serialize()
            }, function (data) {
                processJsonFormCheck(data);
            })
        });

        $('#paramconn').change(function () {
            gui.request({
                url: "/collectivites/getConnList"
            }, function (data) {
                var json = jQuery.parseJSON(data);
                $('#CollectiviteConn').empty();
                $('#CollectiviteConn').append('<option value=""></option>');
                for (i in json) {
                    $('#CollectiviteConn').append('<option value="' + i + '">' + json[i] + '</option>');
                }
            });
        });


        $('#CollectiviteAddForm input[type=radio]').change(function () {
            $('#affiche').toggle();
        });

        if ($('#ConnecteurUseLdapTrue').is(":checked")) {
            $('#affiche').show();
        }

        if ($('#ConnecteurUseLdapFalse').is(":checked")) {
            $('#affiche').hide();
        }
    });
</script>
