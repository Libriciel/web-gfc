<?php

/**
 *
 * Collectivites/get_logo.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php if ( !empty($logo_url )) :?>
    <img src="<?php echo LOGO_URL . '/' . $logo_url; ?>" alt="" />
<?php endif;?>
<div id="buttonDeleteLogo"></div>

<script type="text/javascript">
<?php if(!empty($logo_url)):?>
    gui.addbuttons({
        element: $('#buttonDeleteLogo'),
        buttons: [
            {
                content: '<i class="fa fa-trash" aria-hidden="true"></i> <?php echo __d("default", "Button.delete"); ?>',
                class: "btn-danger btn-primary",
            }
        ]
    });

    $('#buttonDeleteLogo').click(function () {
        swal({
                    showCloseButton: true,
            title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
            text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
            cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: "/collectivites/deleteLogo/" + <?php echo $col_id; ?>,
                    loader: true,
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    $("#buttonDeleteLogo").empty();
                    $("#buttonDeleteLogo").append(data);
                    $("#logo img").attr("src", "");
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n'avez pas supprimé, ;) .",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    });
<?php endif;?>
</script>
