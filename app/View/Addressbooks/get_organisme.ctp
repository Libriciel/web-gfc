<?php

/**
 *
 * Addressbooks/get_addressbooks.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author yjin
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('contactinfo.js', array('inline' => true));
echo $this->Html->script('setInfos.js', array('inline' => true));


$this->Paginator->options(array(
   'update' => '#zone_organisme .panel-body',
   'evalScripts' => true,
   'before' => 'gui.loader({ element: $("#zone_organisme .panel-body"), message: gui.loaderMessage });',
   'complete' => '$(".loader").remove()'
   ));
$pagination = '';
if (Set::classicExtract($this->params, 'paging.Organisme.pageCount') > 1) {
   $pagination = '<div class="pagination">
   ' . implode(
     '', Set::filter(
       array(
          $this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
          $this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
          )
       )
     ) . '
   </div>';
}

?>
<?php echo "<b>Nom du carnet d'adresse lié : </b>".$carnetname;?>
<table id="table_organisme"
       data-toggle="table"
       data-locale = "fr-CA"
       data-height="459"
       >
</table>
<p><?php echo $pagination; ?></p>
<script>
    $('#table_organisme')
            .bootstrapTable({
                data: <?php echo json_encode($organismes); ?>,
                columns: [
                    {
                        field: "name",
                        title: "Nom",
                        class: "name"
                    },
                    {
                        field: "active",
                        title: "<?php echo __d('organisme', 'Organisme.active'); ?>",
                        class: "active_column"
                    },
                    {
                        field: "view",
                        title: "",
                        align: "center"

                    },
                    {
                        field: "edit",
                        title: "",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "",
                        align: "center"
                    },
                    {
                        field: "exportcsv",
                        title: "",
                        align: "center"
                    }
                ]
            }).on('search.bs.table', function (e, text) {
        chargerFunctionJquery();
    });
    $(document).ready(function () {
        chargerFunctionJquery();
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
    });
    function chargerFunctionJquery() {
        $.getScript("/js/addressbook.js");

        $('.viewOrganisme').click(function () {
            var id = $(this).attr('id').substring(10);
            var href = '/organismes/get_contact/' + id;
            var chargeZone = '#zone_contact .panel-body';
            $("#zone_carnet").removeAttr('class').addClass("col-sm-2 col-sm-2");
            $("#zone_organisme").removeAttr('class').addClass("col-sm-2 col-sm-2");
            $("#zone_contact").removeAttr('class').addClass("col-sm-8 col-sm-8");
            viewItem(href, chargeZone);
        });
        function viewItem(href, chargeZone) {
            gui.request({
                url: href,
                updateElement: $(chargeZone),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
            if (chargeZone == "#infos .content") {
                $('#infos .panel-title').empty();
                $('#infos').modal('show');
            }
        }

        // édition des organismes
        $('.editOrganisme').click(function () {
            var id = $(this).attr('id').substring(10);
            var href = '/organismes/edit/' + id;
            var isSansOrganisme = $(this).parent().parent().text();
            isSansOrganisme.includes("Sans organisme");
            if( !isSansOrganisme.includes("Sans organisme") ) {
                editItem2(href);
            }
            else {
                var title = "Modifier l'organisme";
                var text = "Cet organisme ne peut être modifié";
                swal(
                    'Oops...',
                    title + "<br>" + text,
                    'warning'
                );
            }
//            editItem2(href);
        });

        // visualisation des organismes
        function editItem2(href) {

            gui.formMessage({
                url: href,
                loader: true,
                loaderMessage: gui.loaderMessage,
                updateElement: $('#OrganismeEdit').parents('fieldset'),
                width: 700,
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        var form = $('#OrganismeEditForm');
                        if (form_validate(form)) {
                            if( "<?php echo Configure::read('AdresseOrganisme.DiffuseContact');?>" === 'yes' ) {
                                swal({
                    showCloseButton: true,
                                    title: "Diffusion de l'adresse",
                                    text: "En validant, vous allez remplacer l'adresse de TOUS les contacts liés à cet organisme avec celle nouvellement renseignée.<br /> Etes-vous sûr de vouloir faire cela ?",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#d33",
                                    cancelButtonColor: "#3085d6",
                                    confirmButtonText: '<i class="fa fa-check" aria-hidden="true"></i> Diffuser',
                                    cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Ne pas diffuser',
                                }).then(function (data) {
                                    if (data) {
                                        $('#OrganismeDiffuseContact').val('Oui');
                                    } else {
                                        $('#OrganismeDiffuseContact').val('Non');
                                    }

                                    gui.request({
                                        url: form.attr('action'),
                                        data: form.serialize()
                                    }, function (data) {
                                        orgInfos = jQuery.parseJSON(data);
                                        layer.msg("Les informations ont bien été enregistrées");
                                        $('.organismeInfoCard').remove();
//                                        refreshOrganismes(orgInfos);
//                                        refreshContactInfos($('#CourrierContactId').val());
                                        $(this).parents('.modal').modal('hide');
                                        $(this).parents('.modal').empty();
                                    });
                                    $('.modal').modal('hide');
                                    $('.modal').empty();
                                });
                            }
                            else {
                                gui.request({
                                    url: form.attr('action'),
                                    data: form.serialize()
                                }, function (data) {
                                    orgInfos = jQuery.parseJSON(data);
                                    layer.msg("Les informations de l'organisme ont bien été enregistrées");
                                });
                                $(this).parents('.modal').modal('hide');
                                $(this).parents('.modal').empty();
                            }
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }
            });
        }

        // export des organismes
        $('.exportOrganisme').click(function () {
            var id = $(this).attr('id').substring(10);
            var href = '/organismes/export/' + id;
            exportItem(href);
        });
        function exportItem(href) {
            window.location.href = href;
        }

    }

    $(document).ready(function () {
        addClassActive();
    });

    function addClassActive() {
        $('#table_organisme .active_column').hide();
        $('#table_organisme .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }
</script>
<?php echo $this->Js->writeBuffer();?>
