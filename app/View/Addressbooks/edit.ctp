<?php

/**
 *
 * Addressbooks/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="row">
<?php
$formAddressbook = array(
    'name' => 'Addressbook',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-5',
    'input' => array(
        'Addressbook.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Addressbook.name' => array(
            'labelText' =>__d('addressbook', 'Addressbooks.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Addressbook.description' => array(
            'labelText' =>__d('addressbook', 'Addressbooks.description'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'Addressbook.private' => array(
            'labelText' =>__d('addressbook', 'Addressbook.private'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['Addressbook']['private']
            )
        )
    )
);

echo $this->Formulaire->createForm($formAddressbook);
echo $this->Form->end();
?>
</div>
<script>
    $('#infos .panel-title').empty();
    $('#infos .panel-title').append('Modification : <?php echo $this->request->data['Addressbook']['name']; ?>');
</script>
