<?php
	$this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow( array( 'Informations sur la carnet : '.$carnetName ));
    
    $this->Csv->addRow(
        array_merge(
            array(
                "ORGANISME",
                "ANTENNE",
                "ACTIVITE",
                "SIRET",
                "FAX",
                'DATE DE CREATION FICHE OU DERNIERE MODIFICATION',
                "CIVILITE",
                "NOM",
                "PRENOM",
                "FONCTION",
                "TELEPHONE",
                "LIGNE DIRECTE",
                "LIGNE MOBILE",
                "MAIL",
                "ADRESSE",
                "COMPL ADRESSE",
                "CODE POSTAL",
                "VILLE",
                "OPERATIONS DU CONTACT",
                "OBSERVATIONS"
            ),
            $eventColumns
        )
	);

//	foreach( $results as $m => $addressbook ){

        foreach( $results['Organisme'] as $og => $org ) {
            $nomOrg = $org['name'];
            $antenneOrg = $org['antenne'];
            $rowOganisme[$og] = array_merge(
                array($nomOrg),
                array($antenneOrg),
                array($org['activites']),
                array($org['siret']),
                array($org['fax'])
            );

            foreach( $org['Contact'] as $c => $contact ) {
                $neweventColumns = array();
                if( !empty($contact['events']) ) {
                    foreach( $contact['events'] as $eventsIds => $eventsName ) {
                        if( in_array( $eventsName, $eventColumns ) ) {
                            $neweventColumns[$eventsIds] = 'Oui';
                        }
                        else {
                            $neweventColumns[$eventsIds] = 'Non';
                        }
                    }
                }
                
                $modified = $this->Html2->ukToFrenchDateWithHourAndSlashes($contact['modified']);
//debug($events);
                $rowContact[$og] = array_merge(
                    array($modified),
                    array($contact['civilite']),
                    array($contact['nom']),
                    array($contact['prenom']),
                    array(@$contact['Fonction']['name']),
                    array($contact['tel']),
                    array($contact['lignedirecte']),
                    array($contact['portable']),
                    array($contact['email']),
                    array($contact['adresse']),
                    array($contact['compl']),
                    array($contact['cp']),
                    array($contact['ville']),
                    array($contact['operationscontact']),
                    array($contact['observations']),
                    $neweventColumns
                );

                $this->Csv->addRow(
                    array_merge( $rowOganisme[$og], $rowContact[$og])
                );
            }

        }


    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>