<?php

/**
 *
 * Addressbooks/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7.0
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//echo $this->Html->css(array('administration'), null, array('inline' => false));
?>

<script type="text/javascript">
    function loadAddressbooks() {
        $('#liste .content').empty();
        gui.request({
            url: "/Addressbooks/get_addressbooks",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
    function refreshOrganisme(id) {
        $('#zone_organisme .panel-body').empty();
        gui.request({
            url: '/addressbooks/get_organisme/' + id,
            updateElement: $('#zone_organisme .panel-body'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

    function refreshContactFromOrganismeId(id) {
        $('#zone_contact .panel-body').empty();
        gui.request({
            url: '/organismes/get_contact/' + id,
            updateElement: $('#zone_contact .panel-body'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }


    function loadContacts(id) {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/contacts/get_contacts"; ?>/" + id,
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

    function loadContactsForm() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/contacts/get_contactform"; ?>",
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

    function loadOrganismes(id) {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/organismes/get_organismes"; ?>/" + id,
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

</script>


<div class="container">
    <div id="liste" class="row">
        <div  class="table-list">
            <h3><?php echo __d('addressbook', 'Addressbooks.liste'); ?></h3>
            <div  class="bannette_panel" style="padding:5px; margin-bottom: 15px;margin-right: 12px; ">
                <span class="bouton-search action-title pull-right"  role="group" >
                    <a href="#" class="btn btn-info-webgfc btn-inverse" data-target="#searchModal" data-toggle="modal" title="Recherche avancée"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
                    <a href="/addressbooks" class="btn btn-info-webgfc btn-inverse" id="cancelModalButton" data-target="#searchModal" title="Réinitialisation" style="margin-left: 15px;"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>
                </span>
            </div>
            <div class="content" id="addressbook_liste_content">
            </div>
        </div>
        <div class="controls panel-footer " role="group">
        </div>
    </div>
</div>

<div id="infos" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                        <h4 class="panel-title"><?php echo __d('addressbook', 'Addressbooks.infos'); ?></h4>
                    </div>
                    <div class="panel-body content">
                    </div>
                    <div class="panel-footer controls " role="group">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<script type="text/javascript">

    gui.buttonbox({
        element: $('#liste .controls')
    });

// Import contacts depuis un fichier csv/VCard
    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: '<i class="fa fa-upload" aria-hidden="true"></i> Importer',
            title: "<?php echo __d("contact", "Contact.import"); ?>",
            class: "btn-info-webgfc ",
            action: function () {
                $('#infos').modal('show');
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') .'/Contacts/import_contacts_index' ?>",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderShowDiv: $('#liste .content'),
                    loaderMessage: gui.loaderMessage
                });
                $('#infos .panel-title').html("<?php echo __d("contact", "Contact.import"); ?>");
            }
        }
    });


    gui.buttonbox({
        element: $('#infos .controls')
    });

    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    $(this).parents(".modal").modal('hide');
                    $("#infos .content").empty();
                    gui.enablebutton({
                        element: $('#infos .controls'),
                        button: "Valider"
                    });
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success ",
                action: function () {
                    var form = $(this).parents('.modal').find('form');
                    if (form_validate(form)) {
                        gui.request({
                            url: form.attr('action'),
                            data: form.serialize()
                        }, function (data) {
                            getJsonResponse(data);
                            loadAddressbooks();
                        });
                        $(this).parents(".modal").modal('hide');
                        $("#infos .content").empty();
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        ]
    });



    loadAddressbooks();

</script>
