<?php

/**
 *
 * Marches/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
 echo $this->Html->tag('div', $this->Html->tag('i',' ', array('class'=>'fa fa-plus-circle','aria-hidden'=>'true')), array('id' => 'addPlisValue','class'=>'btn btn-info-webgfc add-edit-btn','title'=>__d('marche', 'Marche.addPlis')));
 if(!empty($selectplis)) {
    echo $this->Html->tag('div', $this->Html->tag('i','', array('class'=>'fa fa-download','aria-hidden'=>'true')), array('id' => 'exportPlis','class'=>'btn btn-info-webgfc export-edit-btn','title'=>__d('pliconsultatif', 'Pliconsultatif.export')));
}
?>

<?php
    if(!empty($selectplis)) {
        $fields = array(
//            'name',
            'numero',
            'origine',
            'objet',
            'date',
            'heure'
        );

        $actions = array(
            "edit" => array(
                "url" => '/plisconsultatifs/edit/',
                "updateElement" => "$('#infos .content')",
                "formMessage" => true,
                "loader" => true,
                "refreshAction" => "loadValues();"
            ),
            "delete" => array(
                "url" => '/plisconsultatifs/delete/',
                "updateElement" => "$('#infos .content')",
                "refreshAction" => "loadValues();"
            )
        );
        $options = array();

        $data = $this->Liste->drawTbody($selectplis, 'Pliconsultatif', $fields, $actions, $options);
?>

<div  class="bannette_panel panel-body">
    <table id="table_plis"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height="400"
           >
    </table>
</div>


<script type="text/javascript">

    $('#table_plis')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "numero",
                        title: "<?php echo __d('pliconsultatif','Pliconsultatif.numero'); ?>",
                        class: "numero"
                    },
                    {
                        field: "origine",
                        title: "<?php echo __d('pliconsultatif','Pliconsultatif.origine'); ?>",
                        class: "origine"
                    },
                    {
                        field: "objet",
                        title: "<?php echo __d('pliconsultatif','Pliconsultatif.objet'); ?>",
                        class: "objet"
                    },
                    {
                        field: "date",
                        title: "<?php echo __d('pliconsultatif','Pliconsultatif.date'); ?>",
                        class: "date"
                    },
                    {
                        field: "heure",
                        title: "<?php echo __d('pliconsultatif','Pliconsultatif.heure'); ?>",
                        class: "heure"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            });

    // Pour la partie Edit
    $('#table_plis .itemEdit').prop('onclick', null).off('click');
    $('#table_plis .itemEdit').bind('click', function () {
        var itemId = $(this).attr('itemId');
        gui.formMessage({
            updateElement: $("#infos .content"),
            loader: true,
            loaderMessage: gui.loaderMessage,
            title: "<?php echo __d('ordreservice', 'Ordreservice.edit'); ?>",
            url: "/plisconsultatifs/edit/" + itemId,
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $('#PliconsultatifEditForm');
                    if (form_validate(form)) {
                        gui.request({
                            url: $('#PliconsultatifEditForm').attr('action'),
                            data: $('#PliconsultatifEditForm').serialize()
                        }, function () {
                            affichageTab()
                        });
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    }
                }
            }
        });
    });

    $('#table_plis .itemDelete').prop('onclick', null).off('click');
    $('#table_plis .itemDelete').bind('click', function () {
        var itemId = $(this).attr('itemId');
        swal({
                    showCloseButton: true,
            title: '<?php echo  __d('default', 'Confirmation de suppression'); ?>',
            text: '<?php echo __d('default', 'Voulez-vous supprimer cet élément ?'); ?>',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: "<?php echo  __d('default', 'Button.delete') ; ?>",
            cancelButtonText: "<?php echo  __d('default', 'Button.cancel'); ?>",
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: "/plisconsultatifs/delete/" + itemId,
                    updateElement: $('body'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    getJsonResponse(data);
                    affichageTab();
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n\'avez pas supprimé.",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    });

</script>


<?php
echo $this->Js->writeBuffer();
?>
<?php
    } else {
        echo $this->Html->div('alert alert-warning',__d('pliconsultatif', 'Pliconsultatif.void'));
    }
?>


<script>
    $('#addPlisValue').addClass('marcheAddPlis');

    $('#addPlisValue').button().click(function () {
        var url = "<?php echo Router::url(array('controller' => 'plisconsultatifs', 'action' => 'add', $marcheId)); ?>";

        gui.formMessage({
            updateElement: $('#infos'),
            loader: true,
            width: '500px',
            loaderMessage: gui.loaderMessage,
            title: "<?php echo __d('pliconsultatif', 'Pliconsultatif.newPlis'); ?>",
            data: $(this).serialize(),
            url: url,
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $('#PliconsultatifAddForm');
                    if (form_validate(form)) {
                        gui.request({
                            url: url,
                            data: $('#PliconsultatifAddForm').serialize(),
                            loaderElement: $('#infos .content'),
                            loaderMessage: gui.loaderMessage
                        }, function (data) {
                            affichageTab();
                        });
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    }

                }
            }
        });
    });

    function loadValues() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/marches/edit/".$marcheId; ?>",
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });

        $('#infos .content').empty();
    }

    $('#exportPlis').addClass('marcheAddPlis');
    $('#exportPlis').button().click(function () {
        window.location.href = "<?php echo Router::url( array( 'controller' => 'plisconsultatifs', 'action' => 'export', $marcheId ) );?>";
    });


</script>

