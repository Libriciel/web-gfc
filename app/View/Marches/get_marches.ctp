<?php

/**
 *
 * Marches/get_marches.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="bouton-search btn-group"  role="group" id="searchControls" style="margin-top:-40px;">
    <?php
    if( !empty($marches) ) {
        echo $this->Html->link(
            '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
            '#',
            array( 'escape' => false,
                      'title' => 'Accès à la recherche',
                      'onclick' => "$( '#zoneMarcheGetMarchesForm' ).toggle(); return false;",
                      'id' => 'formulaireMarcheButton',
                      'class' => 'btn btn-primary '
            )
        );
    }
    echo $this->Html->link(
            '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
            '#',
            array( 'escape' => false,
                      'title' => 'Réinitialiser la recherche',
                      'onclick' => "loadMarche(); return false;",
                      'class' => 'btn btn-primary '
            )
        );
    ?>
</div>
    <?php
    $formMarche = array(
        'name' => 'Marche',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-5',
        'form_url' => 'getMarches',
        'input' => array(
            'SearchMarche.numero' => array(
                'labelText' =>__d('marche', 'Marche.numero'),
                'inputType' => 'text',
                'items'=>array(
                    'type' => 'text',
                    'required' => false
                )
            ),
            'SearchMarche.contractant_id' => array(
                    'labelText' => __d('marche', 'Marche.contractant_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type' => 'select',
                        'empty' => true,
                        'required'=>false,
                        'options' => $contractants
                    )
            ),
            'SearchMarche.datenotificationdebut' => array(
                'labelText' =>__d('marche', 'Marche.datenotificationdebut'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text',
                    'readonly' => true,
                    'class' => 'datepicker',
                    'data-format'=>'dd/MM/yyyy'
                )
            ),
            'SearchMarche.datenotificationfin' => array(
                'labelText' =>__d('marche', 'Marche.datenotificationfin'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text',
                    'readonly' => true,
                    'class' => 'datepicker',
                    'data-format'=>'dd/MM/yyyy'
                )
            ),
            'SearchMarche.active' => array(
                'labelText' =>__d('marche', 'Marche.active'),
                'inputType' => 'checkbox',
                'items'=>array(
                    'type'=>'checkbox',
                    'checked' => true
                )
            ),
            'SearchMarche.operation_id' => array(
                'labelText' => __d('marche', 'Marche.operation_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'required' => false,
                    'options' => $operationsList,
                    'empty' => true
                )
            ),
            'SearchMarche.operationactive' => array(
                'labelText' =>__d('operation', 'Operation.active'),
                'inputType' => 'checkbox',
                'items'=>array(
                    'type'=>'checkbox',
                    'checked' => true
                )
            )
         )
    );
    ?>
<div id="zoneMarcheGetMarchesForm" class="zone-form">
    <legend>Recherche de marché</legend>
    <div class="panel row">
            <?php
              echo $this->Formulaire->createForm($formMarche);
            ?>
    </div>
    <div class="controls text-center" role="group" >
        <span id="reset" class="aere btn btn-info-webgfc btn-inverse"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</span>
        <span id="searchMarcheButton" class="aere btn btn-success "><i class="fa fa-search" aria-hidden="true"></i> Rechercher</span>
    </div>
        <?php echo $this->Form->end(); ?>
</div>



<script type="text/javascript">
    $('#zoneMarcheGetMarchesForm').hide();
    $('#MarcheGetMarchesForm div.required').removeClass('required');

    $('#formulaireMarcheButton').button();
    $('#searchMarcheButton').button();
    $('#reset').button();

    $('#searchMarcheButton').click(function () {
        gui.request({
            url: '/marches/getMarches',
            data: $("#MarcheGetMarchesForm").serialize(),
            loader: true,
            updateElement: $('#liste .content'),
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $('#liste .content').html(data);
        });
    });

    // Ajout de l'action de recherche via la touche Entrée
    $('#MarcheGetMarchesForm').keyup(function (e) {
        if (e.keyCode == 13) {
            gui.request({
                url: '/marches/getMarches',
                data: $("#MarcheGetMarchesForm").serialize(),
                loader: true,
                updateElement: $('#liste .content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#liste .content').html(data);
            });
        }
    });

    $('#MarcheGetMarchesForm.folded').hide();

    $('#SearchMarcheName').select2({allowClear: true, placeholder: "Sélectionner un marché"});
    $('#SearchMarcheOperationId').select2({allowClear: true, placeholder: "Sélectionner une OP"});
    $('#SearchMarcheContractantId').select2({allowClear: true, placeholder: "Sélectionner un type de contractant"});
    $('#SearchMarcheUniterifId').select2({allowClear: true, placeholder: "Sélectionner un service"});

    $('#reset').click(function () {
        resetJSelect('#MarcheGetMarchesForm');
        $('#zoneMarcheGetMarchesForm').hide();
    });

    $('#zoneMarcheGetMarchesForm').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#zoneMarcheGetMarchesForm #searchMarcheButton').trigger('click');
            return false;
        }
        if (e.keyCode === 27) {
            $('#zoneMarcheGetMarchesForm #reset').trigger('click');
            return false;
        }
    });

</script>
<?php if( !empty($marches) ) {

 $fields = array(
        'name',
        'numero',
//        'operation_id',
        'titulaire',
        'contractant_id',
        'datenotification',
        'objet'
    );
    $actions = array(
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/marches/edit/',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/marches/delete/',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadMarche();;"
        )
    );
    $options = array();

    $data = $this->Liste->drawTbody($marches, 'Marche', $fields, $actions, $options);
    $date_temp = json_decode($data,true);
    foreach ($marches as $key => $marche) {
        $date_temp[$key]['typecontractant'] =  $marche['Contractant']['name'];
        $date_temp[$key]['operation_name'] =  $marche['Operation']['name'];
    }
    $data = json_encode($date_temp);
    ?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_marches"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-sortable ="true"
           data-pagination = "true"
           >
    </table>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $('.form_datetime input').datepicker({
            language: 'fr-FR',
            format: "dd/mm/yyyy",
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked'
        });
    });

    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($marches,$options); ?>');
    $('#table_marches')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "numero",
                        title: "<?php echo __d('marche', 'Marche.numero'); ?>",
                        class: "numero",
                        sortable: true
                    },
                    {
                        field: "operation_name",
                        title: "<?php echo __d('operation', 'Operation.name'); ?>",
                        class: "operation_name",
                        sortable: true
                    },
                    {
                        field: "titulaire",
                        title: "<?php echo __d('marche', 'Marche.titulaire'); ?>",
                        class: "titulaire",
                        sortable: true
                    },
                    {
                        field: "objet",
                        title: "<?php echo __d('marche', 'Marche.objet'); ?>",
                        class: "objet",
                        sortable: true
                    },
                    {
                        field: "typecontractant",
                        title: "<?php echo __d('marche', 'Marche.typecontractant'); ?>",
                        class: "typecontractant",
                        sortable: true
                    },
                    {
                        field: "datenotification",
                        title: "<?php echo __d('marche', 'Marche.datenotification'); ?>",
                        class: "datenotification",
                        sortable: true
                    },
                    /*
                     {
                     field: "premiermois",
                     title: "<?php // echo __d('marche', 'Marche.premiermois'); ?>",
                     class: "premiermois"
                     },
                     */
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            });
    $(document).ready(function () {
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }
    function resetJSelect(formId) {
        $(formId).find(':input').each(function () {
            switch (this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'radio':
                    this.checked = false;
            }
        });

        $('.jSelectList', $(formId)).empty();
        $('select option', $(formId)).removeAttr('selected');
        $('select option', $(formId)).css('display', 'block');
        $('.delAllBttn').remove();

    }
    $('.export_marches').show();
</script>
<?php
    echo $this->Liste->drawScript($marches, 'Marche', $fields, $actions, $options);
    echo $this->Js->writeBuffer();
}else{
    echo $this->Html->tag('div', __d('marche', 'Marche.void'), array('class' => 'alert alert-warning'));
?>
<script>
    $('.export_marches').hide();
</script>
<?php
 }
 ?>
