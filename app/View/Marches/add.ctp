<?php

/**
 *
 * Marches/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="row">
<?php
$formMarche = array(
        'name' => 'Marche',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-5',
        'form_url' => array( 'controller' => 'marches', 'action' => 'add'),
        'input' => array(
            'Marche.numero' => array(
                'labelText' =>__d('marche', 'Marche.numero'),
                'inputType' => 'text',
                'items'=>array(
                    'type' => 'text',
                    'required' => true
                )
            ),
            'Marche.titulaire' => array(
                'labelText' =>__d('marche', 'Marche.titulaire'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
            'Marche.objet' => array(
                'labelText' =>__d('marche', 'Marche.objet'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Marche.datenotification' => array(
                'labelText' =>__d('marche', 'Marche.datenotification'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text',
                    'readonly' => true,
                    'class' => 'datepicker',
                    'data-format'=>'dd/MM/yyyy',
                    'required'=>true
                )
            ),
            'Marche.contractant_id' => array(
                'labelText' => __d('marche', 'Marche.contractant_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type' => 'select',
                    'empty' => true,
                    'required'=>true,
                    'options' => $contractants
                )
            ),
            'Marche.premiermois' => array(
                'labelText' =>__d('marche', 'Marche.premiermois'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text',
                    'readonly' => true,
                    'class' => 'datepicker',
                    'data-format'=>'mm/yyyy'
                )
            ),
            'Marche.uniterif_id' => array(
                'labelText' =>__d('marche', 'Marche.uniterif'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'empty' => true,
                    'options' => $uniterifs
                )
            ),
            'Marche.operation_id' => array(
                'labelText' =>__d('marche', 'Marche.operation_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'empty' => true,
                    'options' => $operations,
                    'required' => true
                )
            ),
            'Marche.chrono' => array(
                'labelText' =>__d('marche', 'Marche.chrono'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Marche.active' => array(
                'labelText' =>__d('marche', 'Marche.active'),
                'inputType' => 'checkbox',
                'items'=>array(
                    'type'=>'checkbox',
                    'checked' => true
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formMarche);
    echo $this->Form->end();
?>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.form_datetime input').datepicker({
            language: 'fr-FR',
            format: "dd/mm/yyyy",
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked'
        });
        $('.form_datetime #MarchePremiermois').datepicker({
            language: 'fr-FR',
            format: "mm/yyyy",
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked'
        });

    });

    $('#MarcheOperationId').select2({allowClear: true, placeholder: "Sélectionner une OP"});
    $('#MarcheContractantId').select2({allowClear: true, placeholder: "Sélectionner un type de contractant"});
    $('#MarcheUniterifId').select2({allowClear: true, placeholder: "Sélectionner un service"});


//    function checkMarcheNameUnique() {
//        gui.request({
//            url: "/marches/ajaxformvalid/marchename",
//            data: $('#MarcheAddForm').serialize()
//        }, function (data) {
//            processJsonFormCheck(data);
//        });
//    }
//
//    $("#MarcheName").blur(function () {
//        checkMarcheNameUnique();
//    });

    $('#s2id_MarcheHeurelimiteHour').css({
        width: '48%',
        float: 'left'
    });

    $('#s2id_MarcheHeurelimiteMin').css({
        width: '48%',
        float: 'right'
    });
</script>
