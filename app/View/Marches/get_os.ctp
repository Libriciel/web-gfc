<?php

/**
 *
 * Marches/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
 <?php
    echo $this->Html->tag('div', $this->Html->tag('i','', array('class'=>'fa fa-plus-circle','aria-hidden'=>'true')), array('id' => 'addOsValue','class'=>'btn btn-info-webgfc add-edit-btn','title'=> __d('marche', 'Marche.addOs')));
    if(!empty($selectordresservices)) {
       echo $this->Html->tag('div', $this->Html->tag('i','', array('class'=>'fa fa-download','aria-hidden'=>'true')), array('id' => 'exportOS', 'class'=>'btn btn-info-webgfc export-edit-btn', 'title'=>__d('ordreservice', 'Ordreservice.export')));
    }
?>
<?php
    if(!empty($selectordresservices)) {
        $fields = array(
            'name',
            'numero',
            'organisme_id',
            'objet',
            'montant'
        );

        $actions = array(
            "edit" => array(
                "url" => '/ordresservices/edit/',
                "updateElement" => "$('#infos .content')",
                "formMessage" => true,
                "loader" => true,
                "refreshAction" => "loadValues();"
            ),
            "delete" => array(
                "url" => '/ordresservices/delete/',
                "updateElement" => "$('#infos .content')",
                "refreshAction" => "loadValues();"
            )
        );
        $options = array();

        $data = $this->Liste->drawTbody($selectordresservices, 'Ordreservice', $fields, $actions, $options);
        $data_temp = json_decode($data,true);
        foreach ($selectordresservices as $key => $ordreservice) {
            $data_temp[$key]['organisme_id'] = $ordreservice['Organisme']['name'];
        }
        $data = json_encode($data_temp);
?>
<div  class="bannette_panel panel-body">
    <table id="table_os"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height="400"
           data-pagination = "true"
           >
    </table>
</div>



<script type="text/javascript">

    $('#table_os')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    //                    {
                    //                        field: "name",
                    //                        title: "<?php echo __d('ordreservice','Ordreservice.name'); ?>",
                    //                        class: "name"
                    //                    },
                    {
                        field: "numero",
                        title: "<?php echo __d('ordreservice','Ordreservice.numero'); ?>",
                        class: "numero"
                    },
                    {
                        field: "organisme_id",
                        title: "<?php echo __d('ordreservice','Ordreservice.organisme_id'); ?>",
                        class: "organisme_id"
                    },
                    {
                        field: "objet",
                        title: "<?php echo __d('ordreservice','Ordreservice.objet'); ?>",
                        class: "objet"
                    },
                    {
                        field: "montant",
                        title: "<?php echo __d('ordreservice','Ordreservice.montant'); ?>",
                        class: "montant"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            })
            .on('search.bs.table', function (e, text) {
                itemView();
                itemEdit();
                itemDelete();
            });

    function itemEdit() {
        // Pour la partie Edit
        $('#table_os .itemEdit').prop('onclick', null).off('click');
        $('#table_os .itemEdit').bind('click', function () {
            var itemId = $(this).attr('itemId');
            gui.request({
                url: "/ordresservices/edit/" + itemId,
                updateElement: $('#infos .content #marche_tabs_2'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
        });
    }

    function itemDelete() {
        $('#table_os .itemDelete').prop('onclick', null).off('click');
        $('#table_os .itemDelete').bind('click', function () {
            var itemId = $(this).attr('itemId');
            swal({
                    showCloseButton: true,
                title: '<?php echo  __d('default', 'Confirmation de suppression'); ?>',
                text: '<?php echo __d('default', 'Voulez-vous supprimer cet élément ?'); ?>',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#3085d6",
                confirmButtonText: "<?php echo  __d('default', 'Button.delete') ; ?>",
                cancelButtonText: "<?php echo  __d('default', 'Button.cancel'); ?>",
            }).then(function (data) {
                if (data) {
                    gui.request({
                        url: "/ordresservices/delete/" + itemId,
                        updateElement: $('body'),
                        loader: true,
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        getJsonResponse(data);
                        affichageTab();
                    });
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n\'avez pas supprimé.",
                        type: "error",

                    });
                }
            });
            $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
        });
    }

    function itemView() {
        // Pour la partie Edit
        $('#table_os .itemView').prop('onclick', null).off('click');
        $('#table_os .itemView').bind('click', function () {
            var itemId = $(this).attr('itemId');
            gui.request({
                url: "/ordresservices/view/" + itemId,
                updateElement: $('#infos .content #marche_tabs_2'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
        });
    }

    itemView();
    itemEdit();
    itemDelete();
</script>


<?php
echo $this->Js->writeBuffer();
?>
<?php
    } else {
        echo $this->Html->div('alert alert-warning',__d('ordreservice', 'Ordreservice.void'));
    }
?>
<script>

    $('#addOsValue').addClass('marcheAddOs');
    $('#addOsValue').button().click(function () {
        var url = "<?php echo Router::url(array('controller' => 'ordresservices', 'action' => 'add', $marcheId)); ?>";
        gui.request({
            url: url,
            updateElement: $('#infos .content #marche_tabs_2'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    });

    function loadValues() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/marches/getOs/".$marcheId; ?>",
            updateElement: $('#infos .content #marche_tabs_2'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

    $('#exportOS').addClass('marcheAddOS');
    $('#exportOS').button().click(function () {
        window.location.href = "<?php echo Router::url( array( 'controller' => 'ordresservices', 'action' => 'export', $marcheId ) );?>";
    });

</script>
