<?php

/**
 *
 * Outils/flux_clos_a_supprimer.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('zone/Recherches/recherchesFunction.js', array('inline' => true));
echo $this->Html->script('appAttendable.js', array('inline' => true));
?>
<?php
if (empty($flux)) {
	echo $this->Html->tag('div', "Aucun flux trouvé", array('class' => 'alert alert-warning'));
}else{
	$nbCourriers = Hash::get($this->request->params, 'paging.Courrier.count');

	$this->Paginator->options(array(
			'update' => '#infos .content',
			'evalScripts' => true,
			'before' => 'gui.loader({ element: $("#infos .content"), message: gui.loaderMessage });',
			'complete' => '$(".loader").remove()',
	));

	$pagination = '';
	if (Set::classicExtract($this->params, 'paging.Courrier.pageCount') > 1) {
		$pagination = '<ul class="pagination">' . implode(
						'', Set::filter(
						array(
								$this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
								$this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
								$this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
								$this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
								$this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
						)
				)
				) . '
                                </ul>';
	}

	?>
	<div class="bannette_panel panel-body">
		<script type="text/javascript">
			var screenHeight = $(window).height() * 0.5;
		</script>
		<!--  Tableau de résultat affichant les informations du flux -->
		<table class="-table bannette-table"
			   data-toggle="table"
			   data-height=screenHeight
			   data-show-refresh="false"
			   data-show-toggle="false"
			   data-show-columns="false"
			   data-search="false"
			   data-locale = "fr-CA"
			   data-select-item-name="toolbar1"
			   data-pagination="true"
			   data-page-size ="20"
			   id="table_res">
			<thead>
			<tr>
				<?php
				$ths = "";
				$ths .= $this->Html->tag('th', '', array('class' => 'bs-checkbox', 'data-field' => 'state', 'data-checkbox' => 'true'));
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.reference'), array('class' => 'reference tri', 'data-sortable' => 'true', 'data-field' => 'reference'));
				$ths .= $this->Html->tag('th', __d('bannette', 'Courrier.etat'), array('class' => 'etat tri', 'data-sortable' => 'true', 'data-field' => 'etat'));
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.intitule'), array('class' => 'nom tri', 'data-sortable' => 'true', 'data-field' => 'nom'));
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.objet'), array('class' => 'objet tri', 'data-sortable' => 'true', 'data-field' => 'objet'));
				$ths .= $this->Html->tag('th', __d('recherche', 'Courrier.datereception'), array('class' => 'datereception tri', 'data-sortable' => 'true', 'data-field' => 'datereception'));
				$ths .= $this->Html->tag('th', 'Actions', array('class' => 'actions thDeclore', 'data-field' => 'declore', 'data-align' => 'center', 'data-width' => '80px'));
				echo $ths;
				?>
			</tr>
			</thead>
		</table>
		<?php echo $pagination;?>

		<?php
		$drawTds = array();
		foreach($flux as $c => $courrier) {

			// traduction des données du flux
			$declore = $this->Html->tag('i', '', array('style' => 'cursor:pointer;', 'alt' =>'Déclore le flux', 'title' => 'Déclore le flux', 'class' => 'btn-secondary fa fa-backward'));
			$result = array(
					"id" => $courrier['Courrier']['id'],
					'reference' => $courrier['Courrier']["reference"],
					'etat' => $this->Html->tag('i', '', array('class' => 'fa fa-flag-checkered', 'style' => 'color:#2ec07e;cursor:pointer;', 'alt' => 'clos', 'title' => __d('courrier', 'Courrier.courrier_cloture') )),
					'nom' => $courrier['Courrier']['name'] . $this->Form->input('checkItem_' . $courrier['Courrier']['id'], array('type' => 'hidden', 'class' => 'checkItem', 'itemid' => $courrier['Courrier']['id'])),
					'objet' => replace_accents(substr($courrier['Courrier']['objet'], 0, 90)) . '...',
					'datereception' => strftime("%d/%m/%Y", strtotime($courrier['Courrier']["datereception"])),
					'declore' => $declore
			);
			array_push($drawTds, $result);
			$data = json_encode($drawTds);
		}
		?>
	</div>

	<script type="text/javascript">
		$(document).ready(function () {

			$('.pagination li span').click(function () {
				$(this).find('a').click();
			});

			var fluxChoix = [];
			var desktopChoix = [];
			if ($('.table-list h3 span').length < 1) {
				<?php if(isset($nbCourriers) && !empty($nbCourriers)): ?>
				$('.table-list h3').append(' <span> - total: <?php echo $nbCourriers;?></span>');
				<?php endif; ?>
			} else {
				$('.table-list h3 span').remove();
				<?php if(isset($nbCourriers) && !empty($nbCourriers)): ?>
				$('.table-list h3').append(' <span> - total: <?php echo $nbCourriers;?></span>');
				<?php endif; ?>
			}

			// affichage des données du flux
			var data = <?php echo $data; ?>;
			$('.bannette-table').bootstrapTable();
			$("#table_res").bootstrapTable('load', data);
			$("#table_res")
				.on("click-row.bs.table", function (e, row, element) {
					if (!$(element.context).hasClass("actions") && !$(element.context).hasClass("bs-checkbox")) {
						var itemId = element.find('.checkItem').attr('itemId');
						window.open('/courriers/historiqueGlobal/' + itemId);
					}
					else {
						$('.actions').css('cursor', 'pointer');
						// édition des organismes
						$('.actions').click(function () {
							var itemValue = element.find('.checkItem').attr('itemId');
							swal({
								showCloseButton: true,
								title: "<?php echo __d('default', 'Déclore le flux'); ?>",
								text: "<?php echo __d('default', 'Etes-vous sûr de vouloir déclore ce flux ?'); ?>",
								type: "warning",
								showCancelButton: true,
								confirmButtonColor: "#d33",
								cancelButtonColor: "#3085d6",
								confirmButtonText: "<i class='fa fa-backward' ></i> Déclore",
								cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
							}).then(function (data) {
								if (data) {
									gui.request({
										url: "<?php echo Configure::read('BaseUrl') . "/outils/declore/"; ?>" + itemValue,
										data: $('#OutilsSearchFluxClosEnErreurForm').serialize(),
										updateElement: $('#infos .content'),
										loader: true,
										loaderMessage: gui.loaderMessage
									}, function () {
										var updateElement = $(this);
										updateElement.removeAttr('loaded');
										window.location.reload();
									});
									$('#liste').hide();
									$('#infos').show();
								} else {
									swal({
										showCloseButton: true,
										title: "Annulé!",
										text: "Vous n\'avez pas rien fait.",
										type: "error",

									});
								}
							});


							$('.swal2-cancel').css('margin-left', '-320px');
							$('.swal2-cancel').css('background-color', 'transparent');
							$('.swal2-cancel').css('color', '#5397a7');
							$('.swal2-cancel').css('border-color', '#3C7582');
							$('.swal2-cancel').css('border', 'solid 1px');

							$('.swal2-cancel').hover(function () {
								$('.swal2-cancel').css('background-color', '#5397a7');
								$('.swal2-cancel').css('color', 'white');
								$('.swal2-cancel').css('border-color', '#5397a7');
							}, function () {
								$('.swal2-cancel').css('background-color', 'transparent');
								$('.swal2-cancel').css('color', '#5397a7');
								$('.swal2-cancel').css('border-color', '#3C7582');
							});
						});
					}
				})
				.on('check.bs.table', function (e, row) {
					var fluxId = row['id'];
					fluxChoix.push(fluxId);

					var desktopId = row['desktopId'];
					desktopChoix.push(desktopId);
				})
				.on('uncheck.bs.table', function (e, row) {
					var fluxId = row['id'];
					fluxChoix = jQuery.grep(fluxChoix, function (value) {
						return value != fluxId;
					});
					var desktopId = row['desktopId'];
					desktopChoix  = jQuery.grep(desktopChoix, function (valueDesktop) {
						return valueDesktop != desktopId;
					});
				})
				.on('check-all.bs.table', function (e, rows) {
					$.each(rows, function (i, row) {

						var fluxId = row['id'];
						if (jQuery.inArray(fluxId, fluxChoix)) {
							fluxChoix.push(fluxId);
						}
						var desktopId = row['desktopId'];
						if (jQuery.inArray(desktopId, desktopChoix)) {
							desktopChoix.push(desktopId);
						}
					});
				})
				.on('uncheck-all.bs.table', function (e, rows) {
					$.each(rows, function (i, row) {
						var fluxId = row['id'];
						fluxChoix = jQuery.grep(fluxChoix, function (value) {
							return value != fluxId;
						});

						var desktopId = row['desktopId'];
						desktopChoix = jQuery.grep(desktopChoix, function (valueDesktop) {
							return valueDesktop != desktopId;
						});
					});
				});

		});

	</script>
	<?php
	echo $this->Js->writeBuffer();
}?>
