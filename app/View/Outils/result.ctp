<?php

/**
 *
 * Outils/result.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php
echo $this->Html->script('appAttendable.js', array('inline' => true));
?>
	<?php
	if( isset($courriers) && !empty($courriers ) ) {
		?>
		<legend><h4>Liste des flux pouvant être ré-adressés à l'étape i-Parapheur</h4></legend>
		<table class="bannette-table col-sm-5 allcourriers"
			   data-toggle="table"
			   data-height=screenHeight
			   data-show-refresh="false"
			   data-show-toggle="false"
			   data-show-columns="false"
			   data-search="false"
			   data-locale = "fr-CA"
			   data-select-item-name="toolbar1"
			   id="table_res">
			<thead>
			<tr>
				<?php
				$ths = "";
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.reference'), array('class' => 'reference tri', 'data-sortable' => 'true', 'data-field' => 'reference'));
				$ths .= $this->Html->tag('th', __d('bannette', 'Courrier.etat'), array('class' => 'etat tri', 'data-sortable' => 'true', 'data-field' => 'etat'));
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.intitule'), array('class' => 'nom tri', 'data-sortable' => 'true', 'data-field' => 'nom'));
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.objet'), array('class' => 'objet tri', 'data-sortable' => 'true', 'data-field' => 'objet'));
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.contact_id'), array('class' => 'expediteur tri', 'data-sortable' => 'true', 'data-field' => 'expediteur'));
				$ths .= $this->Html->tag('th', __d('recherche', 'Courrier.datereception'), array('class' => 'datereception tri', 'data-sortable' => 'true', 'data-field' => 'datereception'));
				$ths .= $this->Html->tag('th', 'Injecter', array('class' => 'inject tri', /* 'data-sortable' => 'true',  */ 'data-field' => 'retard', 'data-align' => 'center', 'data-width' => '80px'));
				echo $ths;
				?>
			</tr>
			</thead>
		</table>

		<?php
		// traduction des données du flux
		$drawTds = array();
		foreach($courriers as $courrier) {
			$class = 'fa fa-arrows-h';
			$title = 'Injecter';
			$alt = 'Injecter';
			$style = 'cursor:pointer;';
			$inject = $this->Html->tag('i', '', array('id' => $courrier['Courrier']['reference'],  'class' => $class, 'style' => $style, 'alt' => $alt, 'title' => $title ));

			$result = array(
					'id' => $courrier['Courrier']["reference"],
					'reference' => $courrier['Courrier']["reference"],
					'etat' => $this->Html->image('/img/flux_a_traite.png', array('alt' => 'traitement', 'title' => __d('courrier', 'Courrier.courrier_a_traite'))),
					'nom' => $courrier['Courrier']['name'],
					'objet' => replace_accents(substr($courrier['Courrier']['objet'], 0, 90)) . '...',
					'expediteur' => $courrier['contactInfos'],
					'datereception' => strftime("%d/%m/%Y %T", strtotime($courrier['Courrier']["datereception"])),
					'inject' => $inject
			);
			array_push($drawTds, $result);
			$data = json_encode($drawTds);
		}
		?>
		<script type="text/javascript">
			'use strict';
			$(document).ready(function () {

				$('#table_res').bootstrapTable({
						data: <?php echo $data; ?>,
						columns: [
							{
								field: "id",
								title: "",
								class: "hidden"
							},
							{
								field: "reference",
								title: "Référence",
								class: "reference"
							},
							{
								field: "etat",
								title: "Etat",
								class: "etat"
							},
							{
								field: "nom",
								title: "Nom",
								class: "nom"
							},
							{
								field: "objet",
								title: "Objet",
								class: "objet"
							},
							{
								field: "expediteur",
								title: "Expéditeur",
								class: "expediteur"
							},
							{
								field: "datereception",
								title: "Date de réception",
								class: "datereception"
							},
							{
								field: "inject",
								title: "Injecter",
								align: "center",
								class: "action"
							}
						]
					});

				gui.disablebutton({
					button: '<i class="fa fa-arrows-h" aria-hidden="true"></i> Injecter',
					element: $('#infos .controls')
				});
				$("#inject").addClass('disabled').removeAttr("href");
				$("#inject").css('display', 'none');

			});

			// édition des organismes
			$('.action').click(function () {
				var itemValue = $(this)[0].children[0].id;
				swal({
					showCloseButton: true,
					title: "<?php echo __d('default', 'Injecter dans le i-Parapheur'); ?>",
					text: "<?php echo __d('default', 'Etes-vous sûr de vouloir ré-injecter ce flux dans le i-Parapheur ?'); ?>",
					type: "info",
					showCancelButton: true,
					confirmButtonColor: "#5cb85c",
					cancelButtonColor: "#3085d6",
					confirmButtonText: "<?php echo  __d('default', 'Button.valid') ; ?>",
					cancelButtonText: "<?php echo  __d('default', 'Button.cancel'); ?>",
				}).then(function (data) {
					if (data) {
						gui.request({
							url: "<?php echo Configure::read('BaseUrl') . "/outils/inject/"; ?>" + itemValue,
							data: $('#CourrierSearchParapheurForm').serialize(),
							updateElement: $('#infos .content'),
							loader: true,
							loaderMessage: gui.loaderMessage
						}, function () {
							var updateElement = $(this);
							updateElement.removeAttr('loaded');
							window.location.reload();
						});
						$('#liste').hide();
						$('#infos').show();
					}
					else {
						swal({
							showCloseButton: true,
							title: "Annulé!",
							text: "Vous n\'avez pas injecté.",
							type: "error",

						});
					}
				});


				$('.swal2-cancel').css('margin-left', '-320px');
				$('.swal2-cancel').css('background-color', 'transparent');
				$('.swal2-cancel').css('color', '#5397a7');
				$('.swal2-cancel').css('border-color', '#3C7582');
				$('.swal2-cancel').css('border', 'solid 1px');

				$('.swal2-cancel').hover(function () {
					$('.swal2-cancel').css('background-color', '#5397a7');
					$('.swal2-cancel').css('color', 'white');
					$('.swal2-cancel').css('border-color', '#5397a7');
				}, function () {
					$('.swal2-cancel').css('background-color', 'transparent');
					$('.swal2-cancel').css('color', '#5397a7');
					$('.swal2-cancel').css('border-color', '#3C7582');
				});
			});
		</script>
<?php
	}
	else {
		if (empty($courrier) || !$displayResult ) {
			echo $this->Html->tag('div', "Aucun flux trouvé (ou bien votre flux est déjà clos, ou votre flux n'a jamais été adressé au i-Parapheur)", array('class' => 'alert alert-warning'));
		}else{
		?>
			<div class="bannette_panel panel-body">

				<script type="text/javascript">
					var screenHeight = $(window).height() * 0.5;
				</script>

				<!--  Tableau de résultat affichant les informations du flux -->
				<table class="bannette-table col-sm-5"
					   data-toggle="table"
					   data-height=screenHeight
					   data-show-refresh="false"
					   data-show-toggle="false"
					   data-show-columns="false"
					   data-search="false"
					   data-locale = "fr-CA"
					   data-select-item-name="toolbar1"
					   id="table_res">
					<thead>
					<tr>
						<?php
						$ths = "";
						$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.reference'), array('class' => 'reference tri', 'data-sortable' => 'true', 'data-field' => 'reference'));
						$ths .= $this->Html->tag('th', __d('bannette', 'Courrier.etat'), array('class' => 'etat tri', 'data-sortable' => 'true', 'data-field' => 'etat'));
						$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.intitule'), array('class' => 'nom tri', 'data-sortable' => 'true', 'data-field' => 'nom'));
						$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.objet'), array('class' => 'objet tri', 'data-sortable' => 'true', 'data-field' => 'objet'));
						$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.contact_id'), array('class' => 'expediteur tri', 'data-sortable' => 'true', 'data-field' => 'expediteur'));
						$ths .= $this->Html->tag('th', __d('recherche', 'Courrier.datereception'), array('class' => 'datereception tri', 'data-sortable' => 'true', 'data-field' => 'datereception'));
						$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.retard'), array('class' => 'retard tri', /* 'data-sortable' => 'true',  */ 'data-field' => 'retard', 'data-align' => 'center', 'data-width' => '80px'));
						echo $ths;
						?>
					</tr>
					</thead>
				</table>

				<?php
					// traduction des données du flux
					$drawTds = array();
					if( $retard ) {
						$retardDisplay = $this->Html->image("/img/retard_16.png", array('height' => '16px;', 'style' => 'vertical-align: middle;', 'alt' => __d('courrier', 'Courrier.alertRetard'), 'title' => __d('courrier', 'Courrier.alertRetard')));
					}
					$result = array(
						'reference' => $courrier['Courrier']["reference"],
						'etat' => $this->Html->image('/img/flux_a_traite.png', array('alt' => 'traitement', 'title' => __d('courrier', 'Courrier.courrier_a_traite'))),
						'nom' => $courrier['Courrier']['name'],
						'objet' => replace_accents(substr($courrier['Courrier']['objet'], 0, 90)) . '...',
						'expediteur' => $contactInfos,
						'datereception' => strftime("%d/%m/%Y %T", strtotime($courrier['Courrier']["datereception"] ) ),
						'retard' => @$retardDisplay
					);
					array_push($drawTds, $result);
					$data = json_encode($drawTds);
				?>
				<!--  Tableau de résultat affichant les informations des bannettes des flux -->
				<table class="bannette-table"
					   data-toggle="table"
					   data-height=screenHeight
					   data-show-refresh="false"
					   data-show-toggle="false"
					   data-show-columns="false"
					   data-search="false"
					   data-locale = "fr-CA"
					   data-select-item-name="toolbar1"
					   id="table_resban">
					<thead>
					<tr>
						<?php
						$ths2 = "";
						$ths2 .= $this->Html->tag('th', 'Type de bannette', array('class' => 'etat tri', 'data-sortable' => 'true', 'data-field' => 'bannetteid'));
						$ths2 .= $this->Html->tag('th', 'Date de création', array('class' => 'objet tri', 'data-sortable' => 'true', 'data-field' => 'cree'));
						$ths2 .= $this->Html->tag('th', 'Date de modification', array('class' => 'expediteur tri', 'data-sortable' => 'true', 'data-field' => 'modifie'));
						$ths2 .= $this->Html->tag('th', 'Etat', array('class' => 'datereception tri', 'data-sortable' => 'true', 'data-field' => 'etat'));
						$ths2 .= $this->Html->tag('th', 'Agent concerné', array('class' => 'retard tri', 'data-field' => 'desktopid', 'data-align' => 'center', 'data-width' => '80px'));
						echo $ths2;
						?>
					</tr>
					</thead>
				</table>

				<?php
					// Infos des bancontenus
					$drawTds2 = array();
					foreach($courrier['Bancontenu'] as $b => $bancontenu) {
						$classRead = 1;
						if ($bancontenu['unread']) {
							$classRead = -1;
						}
						$result2 = array(
							"bannetteid" => __d( 'bannette', 'Bannette.'.@$bannettes[$bancontenu["bannette_id"]] ),
							'cree' => strftime("%d/%m/%Y %T", strtotime($bancontenu["created"] ) ),
							'modifie' => strftime("%d/%m/%Y %T", strtotime($bancontenu["modified"] ) ),
							'etat' => !($bancontenu["clos"]) ? $etat[$bancontenu["etat"]] : 'Flux clos',
							'desktopid' => $bancontenu["username"]
						);
						array_push($drawTds2, $result2);
					}
					$datas2 = json_encode($drawTds2);
				?>
		</div>

			<script type="text/javascript">
				'use strict';
				$(document).ready(function () {
					// affichage des données du flux
					var data = <?php echo $data; ?>;
					$('.bannette-table').bootstrapTable();
					$("#table_res").bootstrapTable('load', data);

					// affichage des données des bannettes du flux
					var data2 = <?php echo $datas2; ?>;
					$('.bannette-table').bootstrapTable();
					$("#table_resban").bootstrapTable('load', data2);

					gui.enablebutton({
						button: '<i class="fa fa-arrows-h" aria-hidden="true"></i> Injecter',
						element: $('#infos .controls')
					});
					$("#inject").removeClass("disabled").attr("href", "#");
					// $("#inject").css('display', 'block');
				});
			</script>

			<?php
		}
	}
?>
