<?php

/**
 *
 * Recherches/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//echo $this->Html->css(array('administration'), null, array('inline' => false));
//    echo $this->Html->css(array('zones/recherche/index'), null, array('inline' => false));
echo $this->Html->script('zone/Recherches/recherchesFunction.js', array('inline' => true));
echo $this->Html->script('appAttendable.js', array('inline' => true));
?>

<div class="container">
	<div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
	<div class="row">
		<div class="table-list rechercheCondition" id="liste">
			<h3>Conditions</h3>
			<div class="content">
				<div  class="table-list">
					<div class="panel-body form-horizontal">
						<?php echo $this->Html->tag('div', "Ce menu de recherche retournera le flux non clos qui est à l'étape i-Parapheur et qui doit être ré-injecté dans le i-Parapheur (car perdu ou mal aiguillé).", array('class' => 'alert alert-info', 'style'=>'margin-bottom:0px;'));?>
					</div>

					<div class="content" style="padding-top: 30px;">
						<?php

						$formOutil = array(
							'name' => 'Courrier',
							'label_w' => 'col-sm-2',
							'input_w' => 'col-sm-4',
							'input' => array(
								'Courrier.reference' => array(
									'inputType' => 'text',
									'labelText' => 'Référence',
									'required' => true,
									'items'=>array(
										'type'=>'text',
										'required' => true
									)
								)
							)
						);
						echo $this->Formulaire->createForm($formOutil);
						echo $this->Form->end();
						?>
					</div>
				</div>
			</div>
			<div class="controls panel-footer " role="group"></div>
		</div>
	</div>
	<div class="row">
		<div class="table-list rechercheCondition" id="infos" style="padding-bottom: 40px;overflow: hidden;">
			<h3>Résultats</h3>
			<div class="content"></div>
			<div class="controls panel-footer  " role="group"></div>
		</div>
	</div>
</div>
<script type="text/javascript">


	// Ajout de l'action de recherche via la touche Entrée
	$('#CourrierSearchParapheurForm').keypress(function (e) {
		if (e.keyCode == 13) {
			$('#liste .controls .searchBtn').trigger('click');
			return false;
		}
	});

	gui.buttonbox({
		element: $('#liste .controls'),
		align: 'center'
	});

	// Bouton  pour lancer la recherche
	gui.addbuttons({
		element: $('#liste .controls'),
		buttons: [
			{
				content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
				title: "<?php echo __d('default', 'Button.search'); ?>",
				class: 'btn-info-webgfc searchBtn',
				action: function () {
					var form = $('#CourrierSearchParapheurForm');
					if( form_validate(form) ) {
						gui.request({
							url: "<?php echo Configure::read('BaseUrl') . "/outils/result"; ?>",
							data: $('#CourrierSearchParapheurForm').serialize(),
							loader: true,
							updateElement: $('#infos .content'),
							loaderMessage: gui.loaderMessage
						});
						$('#liste').hide();
						$('#infos').show();
					}
				}
			}
		]
	});

	$('#infos').hide();
	$('.hide-search-formulaire').hide();

	// Si les résultats sont affichés, on affiche les actions possibles
	if ($('#infos .controls .btn').length == 0) {
		gui.buttonbox({
			element: $('#infos .controls'),
			align: "center"
		});


		// Bouton pour réouvrir le formulaire de recherche
		gui.addbutton({
			element: $('#infos .controls'),
			button: {
				content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
				title: "<?php echo __d('default', 'Button.search'); ?>",
				class: 'btn-info-webgfc',
				action: function () {
					$('#liste').show();
					$('.hide-search-formulaire').show();
				}
			}
		});



		gui.addbuttons({
			element: $('#infos .controls'),
			buttons: [
				{
					content: '<i class="fa fa-arrows-h" aria-hidden="true"></i> Injecter',
					title: "Injecter",
					id: "inject",
					class: 'btn-success searchBtn',
					action: function () {

						swal({
							showCloseButton: true,
							title: "<?php echo __d('default', 'Injecter dans le i-Parapheur'); ?>",
							text: "<?php echo __d('default', 'Etes-vous sûr de vouloir ré-injecter ce flux dans le i-Parapheur ?'); ?>",
							type: "info",
							showCancelButton: true,
							confirmButtonColor: "#5cb85c",
							cancelButtonColor: "#3085d6",
							confirmButtonText: "<?php echo  __d('default', 'Button.valid') ; ?>",
							cancelButtonText: "<?php echo  __d('default', 'Button.cancel'); ?>",
						}).then(function (data) {
							if (data) {
								var itemValue = $('#CourrierSearchParapheurForm').serializeArray()[0]['value'];
								gui.request({
									url: "<?php echo Configure::read('BaseUrl') . "/outils/inject/"; ?>" + itemValue,
									data: $('#CourrierSearchParapheurForm').serialize(),
									updateElement: $('#infos .content'),
									loader: true,
									loaderMessage: gui.loaderMessage
								}, function () {
									var updateElement = $(this);
									updateElement.removeAttr('loaded');
									window.location.reload();
								});
								$('#liste').hide();
								$('#infos').show();
							}
							else {
								swal({
									showCloseButton: true,
									title: "Annulé!",
									text: "Vous n\'avez pas injecté.",
									type: "error",

								});
							}
						});


						$('.swal2-cancel').css('margin-left', '-320px');
						$('.swal2-cancel').css('background-color', 'transparent');
						$('.swal2-cancel').css('color', '#5397a7');
						$('.swal2-cancel').css('border-color', '#3C7582');
						$('.swal2-cancel').css('border', 'solid 1px');

						$('.swal2-cancel').hover(function () {
							$('.swal2-cancel').css('background-color', '#5397a7');
							$('.swal2-cancel').css('color', 'white');
							$('.swal2-cancel').css('border-color', '#5397a7');
						}, function () {
							$('.swal2-cancel').css('background-color', 'transparent');
							$('.swal2-cancel').css('color', '#5397a7');
							$('.swal2-cancel').css('border-color', '#3C7582');
						});
					}
				},
			]
		});
	}



	gui.addbuttons({
		element: $('#backButton'),
		buttons: [
			{
				content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
				class: "btn-info-webgfc",
				action: function () {
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/outils"; ?>";
				}
			}
		]
	});
</script>

