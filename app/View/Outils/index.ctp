
<div id="backButton" style="margin-bottom: 10px;"></div>
<div class="col-sm-6 ">
	<div class="table-list row superadmin-table-list">
		<h3><?php echo __d('menu', "Outils pour l'administrateur"); ?></h3>
		<div class="group-btn-admin">
			<?php if($hasParapheur) :?>
				<a href="/outils/searchParapheur" class="btn btn-success btn-block" role="button"><?php echo __d('menu', " Rechercher un flux lié au i-Parapheur"); ?></a>
			<?php endif;?>
			<a href="/outils/searchFluxnonclos" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Flux non clos des agents non actifs"); ?></a>
			<a href="/outils/searchFluxEncoursBannetteAInserer" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Flux toujours dans les bannettes à insérer alors qu'ils sont insérés dans un circuit"); ?></a>
			<a href="/outils/searchFluxClos" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Suppression de flux clos"); ?></a>
			<?php if(Configure::read('Administration.DisplayDeleteAllMenu') ):?>
				<a href="#" class="btn btn-success btn-block" role="button" id="deleteEveryThing"><i class="fa fa-exclamation-triangle" aria-hidden="true"> </i> <?php echo __d('menu', "Suppression de TOUS les flux (à utiliser uniquement pour un passage en production, fonction irréversible)"); ?> <i class="fa fa-exclamation-triangle" aria-hidden="true"> </i></a>
			<?php endif;?>
			<a href="/outils/searchFluxClosErreur" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Flux clos par erreur"); ?></a>
		</div>
	</div>
</div>

<script type="text/javascript">
	gui.addbuttons({
		element: $('#backButton'),
		buttons: [
			{
				content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
				class: "btn-info-webgfc",
				action: function () {
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/admin#outils"; ?>";
				}
			}
		]
	});
	<?php if(Configure::read('Administration.DisplayDeleteAllMenu') ):?>

	$('#deleteEveryThing').button().click(function () {
		<?php if( !empty($nbCourriers) ) :?>
			var title = "<?php echo __d('default', 'Modal.suppressionTitle'); ?>";
			var text = "Vous vous apprêtez à supprimer <?php echo $nbCourriers;?> courrier<?php echo $nbCourriers > 1 ? 's' : '';?>, <br />ainsi que tous les éléments associés. <br />Êtes-vous sûr de vouloir le faire ?  <br /><br />Cette action est irréversible.";

			swal({
				showCloseButton: true,
				title: title,
				text: text,
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "<i class='fa fa-trash' aria-hidden='true'></i> Supprimer",
				cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
			}).then(function (data) {
				if (data) {
					swal({
						showCloseButton: true,
						title: "Etes-vous bien sûr ?",
						text: "Pour rappel : Vous vous apprêtez à supprimer <?php echo $nbCourriers;?> courrier<?php echo $nbCourriers > 1 ? 's' : '';?>, ainsi que tous les éléments associés. <br /><br />Cette action est irréversible.",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#d33',
						cancelButtonColor: '#3085d6',
						confirmButtonText: "<i class='fa fa-trash' aria-hidden='true'></i> Supprimer",
						cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
					}).then(function (data) {
						if (data) {
							gui.request({
								url: "/outils/deleteEveryThing",
								updateElement: $('.superadmin-table-list'),
								loader: true,
								loaderMessage: gui.loaderMessage
							});

							window.location.href = "<?php echo Configure::read('BaseUrl') . "/outils/index"; ?>";
						} else {
							swal({
								showCloseButton: true,
								title: "Annulé!",
								text: "Vous n'avez pas supprimé, ;) .",
								type: "error",

							});
						}
					});
					$('.swal2-cancel').css('margin-left', '-320px');
					$('.swal2-cancel').css('background-color', 'transparent');
					$('.swal2-cancel').css('color', '#5397a7');
					$('.swal2-cancel').css('border-color', '#3C7582');
					$('.swal2-cancel').css('border', 'solid 1px');

					$('.swal2-cancel').hover(function () {
						$('.swal2-cancel').css('background-color', '#5397a7');
						$('.swal2-cancel').css('color', 'white');
						$('.swal2-cancel').css('border-color', '#5397a7');
					}, function () {
						$('.swal2-cancel').css('background-color', 'transparent');
						$('.swal2-cancel').css('color', '#5397a7');
						$('.swal2-cancel').css('border-color', '#3C7582');
					});
				} else {
					swal({
						showCloseButton: true,
						title: "Annulé!",
						text: "Vous n'avez pas supprimé, ;) .",
						type: "error",

					});
				}
			});
			$('.swal2-cancel').css('margin-left', '-320px');
			$('.swal2-cancel').css('background-color', 'transparent');
			$('.swal2-cancel').css('color', '#5397a7');
			$('.swal2-cancel').css('border-color', '#3C7582');
			$('.swal2-cancel').css('border', 'solid 1px');

			$('.swal2-cancel').hover(function () {
				$('.swal2-cancel').css('background-color', '#5397a7');
				$('.swal2-cancel').css('color', 'white');
				$('.swal2-cancel').css('border-color', '#5397a7');
			}, function () {
				$('.swal2-cancel').css('background-color', 'transparent');
				$('.swal2-cancel').css('color', '#5397a7');
				$('.swal2-cancel').css('border-color', '#3C7582');
			});
		<?php else : ?>
			var title = "Suppression impossible";
			var text = "Aucun flux présent en base de données. <br /> Vous n'avez rien à supprimer";
			swal({
				showCloseButton: true,
				title: title,
				text: text,
				type: 'warning',
				showCancelButton: true,
				cancelButtonColor: '#5397a7',
				cancelButtonText: "<i class='fa fa-times-circle-o' aria-hidden='true'></i> Fermer",
			}).then(function (data) {
			});

			$('.swal2-cancel').css('background-color', 'transparent');
			$('.swal2-cancel').css('color', '#5397a7');
			$('.swal2-cancel').css('border-color', '#3C7582');
			$('.swal2-cancel').css('border', 'solid 1px');

			$('.swal2-cancel').hover(function () {
				$('.swal2-cancel').css('background-color', '#5397a7');
				$('.swal2-cancel').css('color', 'white');
				$('.swal2-cancel').css('border-color', '#5397a7');
			}, function () {
				$('.swal2-cancel').css('background-color', 'transparent');
				$('.swal2-cancel').css('color', '#5397a7');
				$('.swal2-cancel').css('border-color', '#3C7582');
			});
			$('.swal2-confirm').hide();
		<?php endif?>

	});
	<?php endif;?>
</script>
