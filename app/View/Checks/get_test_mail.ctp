<?php

/**
 *
 * Authentifications/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//echo $this->Html->css(array('administration'), null, array('inline' => false));
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="container">
    <div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
    <div id="liste" class="row">
        <div  class="table-list">
            <h3><?php echo 'Adresse mail de test'; ?></h3>
            <div class="content" style="padding-top: 70px;">
                    <?php
                    $formEnvironnement = array(
                        'name' => 'Environnement',
                        'label_w' => 'col-sm-4',
                        'input_w' => 'col-sm-5',
                        'input' => array(
                            'mail' => array(
                                'labelText' =>'Adresse mail à tester',
                                'inputType' => 'email',
                                'items'=>array(
                                    'required'=>true,
                                    'type'=>'email'
                                )
                            )
                        )
                    );
                    echo $this->Formulaire->createForm($formEnvironnement);
                    echo $this->Form->end();
                    ?>
            </div>
        </div>
        <div class="controls panel-footer " role="group" >
            <?php
//                    echo $this->Html->link(" <img src='/img/mail_send.png'></img>", '#', array('id'=>'send_mail_btn','class' => 'btn btn-info-webgfc', 'escape' => false, 'title' => 'Envoyer le mail'));
                    echo $this->Html->link('<i class="fa fa-paper-plane-o" aria-hidden="true"></i>  Envoyer le mail', '#', array('id'=>'send_mail_btn','class' => 'btn btn-info-webgfc', 'escape' => false, 'title' => 'Envoyer le mail'));
                    echo $this->Form->end();
                ?>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#send_mail_btn').click(function () {
        var form = $('#EnvironnementGetTestMailForm');
        if (form_validate(form)) {
            gui.request({
                url: '/checks/testenvoimail',
                data: $('#EnvironnementGetTestMailForm').serialize()
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }

    });

    $(document).on("keypress", 'form', function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
    });

    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
                }
            }
        ]
    });
</script>
