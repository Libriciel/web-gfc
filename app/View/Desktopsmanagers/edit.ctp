<?php

/**
 *
 * Desktopsmanagers/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="col-sm-12">
    <?php
    $formDesktopsmanagers = array(
        'name' => 'Desktopmanager',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Desktopmanager.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            ),
            'Desktopmanager.name' => array(
                'labelText' =>__d('desktopmanager', 'Desktopmanager.name'),
                'inputType' => 'text',
                'items'=>array(
                    'required'=>true,
                    'type'=>'text'
                )
            ),
			'Desktopmanager.profil_id' => array(
				'labelText' => 'Type de profil',
				'inputType' => 'select',
				'items'=>array(
					'empty'=>true,
					'type'=>'select',
					'options'=> $profils
				)
			),
            'Desktop.Desktop' => array(
				'labelText' => 'Profil associé',
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'options' => $desktopsmanagers,
                    'empty' => true,
                    'required' => true,
                    'multiple' => true
                )
            ),
            'Desktopmanager.isdispatch' => array(
                'labelText' =>__d('desktopmanager', 'Desktopmanager.isdispatch'),
                'inputType' => 'checkbox',
                'changeWidth' =>true,
                'items'=>array(
                    'type'=>'checkbox',
                    'checked'=>$this->request->data['Desktopmanager']['isdispatch'],
                    'new_label_w' =>'col-sm-8',
                    'new_input_w' =>'col-sm-4',
                )
            ),
            'Desktopmanager.modified' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value' => date( 'Y-m-d h:i:s' )
                )
            ),
            'Desktopmanager.parent_id' => array(
                'labelText' =>__d('desktopmanager', 'Desktopmanager.parent_id'),
                'inputType' => 'select',
                'items' => array(
                    'type' => 'select',
                    'options' => $bureauxChef,
                    'empty' => true
                )
            ),
			'Desktopmanager.active' => array(
                'labelText' => 'Bureau actif ?',
                'inputType' => 'checkbox',
                'items' => array(
                    'type' => 'checkbox',
					'checked'=>$this->request->data['Desktopmanager']['active']
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formDesktopsmanagers);
    echo $this->Form->end();
    ?>
    <br>
    <?php if(!empty($circuitName['name'])) { ?>
    <fieldset>
        <legend>Liste des circuits auxquels le bureau est associé</legend>
        <table class="liste-circuit-associe-bureau">
            <tbody>
                <?php foreach($circuitName['name'] as $cId => $name): ?>
                <tr>
                    <td class="col-sm-7"><?php echo  $name; ?></td>
<!--                    <td class="col-sm-3 actions">--><?php //echo $this->Html->tag('span', $this->Html->tag('i','', array( 'circuitId' => $cId, 'aria-hidden'=>true, 'desktopmanagerId' => $id, 'class' => 'cutCircuits fa fa-trash', 'alt' => 'Dissocier le circuit', 'title' => __d('user', 'User.cutCircuit')/*, 'url' => '/users/cutCircuit/' . $cId . '/'. $id*/ ))) ;?><!--</td>-->
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </fieldset>
    <?php
    }else {
    ?>
    <fieldset>
        <legend>Liste des circuits auxquels le bureau est associé</legend>
        <div class="panel-body">
            <div class='alert alert-info col-sm-10'>Ce bureau n'est associé à aucun circuit</div>
        </div>
    </fieldset>
    <?php
    }
    ?>
</div>
<script type="text/javascript">
    $('#DesktopDesktop').select2({allowClear: true, placeholder: "Sélectionner un profil"});

    // Bouton permettant de dissocier le bureau d'un circuit
    $('.cutCircuits').click(function () {
        var circuitId = $(this).attr('circuitId');
        var desktopmanagerId = $(this).attr('desktopmanagerId');
        var url = '/users/cutCircuit/' + circuitId + "/" + desktopmanagerId;
        var form = $('form', $(this));
        swal({
			showCloseButton: true,
            title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
            text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
            cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
        }).then(function (data) {
            if (data) {
                gui.loader({
                    message: gui.loaderMessage,
                    element: $('#infos .content')
                });
                var updateElement = $(this);
                gui.request({
                    url: url,
                    data: form.serialize(),
                    updateElement: updateElement
                }, function (data) {
                    $('.loader').remove();
                    getJsonResponse(data);
                    loadCircuits();
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n'avez pas supprimé, ;) .",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    });
</script>
<script type="text/javascript">
    function loadCircuits() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/desktopsmanagers/edit/" . $id; ?>",
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
    ;

    $('#DesktopmanagerParentId').select2({allowClear: true, placeholder: "Qui est mon responsable ?", empty: true});

	$('#DesktopmanagerProfilId').select2({allowClear: false, placeholder: "Quel profil sera associé ?", readonly: true, empty: false});

	$('#DesktopmanagerProfilId').change(function () {
		if( $('#DesktopmanagerProfilId').val() != '' ) {
			$.ajax({
				type: 'GET',
				url: "/desktopsmanagers/getAllDesktopsByProfils/" + $('#DesktopmanagerProfilId').val(),
				dataType: 'json',
				success: function (data) {
					for( i in data ) {
						$("#DesktopDesktop").append($("<option value='" + data[i].id + "'>" + data[i].name + "</option>"));
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					layer.msg("<strong>Erreur: </strong>Erreur de récupération de la liste des types de profils, veuillez vérifier les informations de connexion");
				}
			});
		}
	});

	<?php if( !empty($profilId) ):?>
		$("#DesktopmanagerProfilId").val('<?php echo $profilId;?>').change();
	<?php endif;?>

</script>
