<?php

/**
 *
 * Desktopsmanagers/get_bureaux.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par Adullact - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php

if( isset( $desktopsmanagers ) ) {

        $fields = array(
            'name',
            'desktops',
            'isdispatch',
            'parent_id'
        );
        $actions = array(
            "edit" => array(
                "url" => Configure::read('BaseUrl') . '/desktopsmanagers/edit',
                "updateElement" => "$('#infos .content')",
                "formMessage" => false
            ),
            "delete" => array(
                "url" => Configure::read('BaseUrl') . '/desktopsmanagers/delete',
                "updateElement" => "$('#infos .content')",
                "refreshAction" => "loadBureaux();"
            )
        );
        $options = array(
            "filter" => false,
            "paginator" => true,
            "order" => "name"
        );
        $panelBodyId ="";
        if(!empty($options['panel_id'])){
            $panelBodyId = "id='".$options['panel_id']."'";
        }

		$data = $this->Liste->drawTbody($desktopsmanagers, 'Desktopmanager', $fields, $actions, $options);
?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body" <?php echo $panelBodyId; ?>>
	<span class="bouton-search action-title pull-right"  role="group"  style="margin-top: 10px; margin-left: 5px; ">
		<a href="#" class="btn btn-info-webgfc btn-inverse" data-target="#zoneDesktopmanagerGetBureauxForm" data-toggle="modal" title="Recherche avancée"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
		<a href="/desktopsmanagers" class="btn btn-info-webgfc btn-inverse" id="cancelModalButton" data-target="#searchModal" title="Réinitialisation" style="margin-left: 15px;"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>
	</span>
	 <table id="table_bureaux"
			data-toggle="table"
			data-search="true"
			data-locale = "fr-CA"
			data-height="570"
			data-show-refresh="false"
			data-show-toggle="false"
			data-show-columns="false"
			data-sortable ="true"
			data-pagination ="true"
			data-page-size ="10"
			data-toolbar="#toolbar">
	 </table>
</div>

<!--    <p class='aere'>--><?php //echo $pagination; ?><!--</p>-->
<!--    <p class='aere liste_bureaux'></p>-->
<!--</div>-->
<script>
    if ($('.table-list h3 span').length < 1) {
        $('.table-list h3').append('<span> - Total: <?php echo $nbBureau; ?></span>');
    }
	$('#table_bureaux')
			.bootstrapTable({
				data:<?php echo $data;?>,
				columns: [
					{
						field: 'name',
						title: '<?php echo __d('desktopmanager','Desktopmanager.name'); ?>',
						class: 'name'
					},
					{
						field: 'desktops',
						title: 'Profils associés'
					},
					{
						field: 'isdispatch',
						title: 'Bureau récupérant les documents scannés ?'
					},
					{
						field: 'parent_id',
						title: 'Représentant hiérarchique'
					},
					{
						field: 'edit',
						title: 'Modifier',
						class: 'actions thEdit',
						width: "80px",
						align: "center"
					},
					{
						field: 'delete',
						title: 'Supprimer',
						class: 'actions thDelete',
						width: "80px",
						align: "center"
					}
				]
			})
			.on('page-change.bs.table', function (number, size) {
				addClassActive();
			})
			.on('all.bs.table', function (e, name, order) {
				addClassActive();
			});

	$(window).resize(function () {
		$("#table_bureaux").bootstrapTable( 'resetView' , {height: 570} );
	});

	$(document).ready(function () {
		addClassActive();
	});

	function addClassActive() {
		$('#table_bureaux .active_column').hide();
		$('#table_bureaux .active_column').each(function () {
			if ($(this).html() == 'false') {
				$(this).parents('tr').addClass('inactive');
			}
		});
	}



</script>
<?php
	echo $this->Liste->drawScript($desktopsmanagers, 'Desktopmanager', $fields, $actions, $options);
?>
<?php
}

echo $this->Js->writeBuffer();

?>
<?php

$formDesktopmanager = array(
		'name' => 'Desktopmanager',
		'label_w' => 'col-sm-4',
		'input_w' => 'col-sm-5',
		'input' => array(
				'Desktopmanager.name' => array(
						'labelText' =>__d('desktopmanager', 'Desktopmanager.name'),
						'inputType' => 'text',
						'items'=>array(
								'type' => 'text',
								'required' => false
						)
				),
				'Desktopmanager.isdispatch' => array(
						'labelText' => 'Bureau récupérant les documents scannés ?',
						'inputType' => 'select',
						'items'=>array(
								'type' => 'select',
								'options' => array( '1' => 'Oui' , '0' => 'Non'),
								'empty' => true
						)
				),

				'Desktopmanager.active' => array(
						'labelText' => 'Bureau actif ?',
						'inputType' => 'checkbox',
						'items'=>array(
								'type' => 'checkbox',
								'checked' => true
						)
				),
				'Desktopmanager.isautocreated' => array(
						'labelText' => 'Bureau créés automatiquement ?',
						'inputType' => 'select',
						'items'=>array(
							'type' => 'checkbox',
							'checked' => false
						)
				),
		)
);
?>
<!--<div id="zoneDesktopmanagerGetBureauxForm" class="zone-form">-->
<div id="zoneDesktopmanagerGetBureauxForm" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content zone-form">
			<div class="modal-header">
				<button data-dismiss="modal" class="close">×</button>
				<h4 class="modal-title">Recherche de bureaux</h4>
			</div>
			<div class="modal-body">
				<?php
				echo $this->Formulaire->createForm($formDesktopmanager);
				?>
			</div>
			<div id="zoneDesktopmanagerGetBureauxFormButton" class="modal-footer controls " role="group">

			</div>
			<?php echo $this->Form->end();?>
		</div>
	</div>
</div>


<script type="text/javascript">
	$('#formulaireButton').button();
	$('#searchButton').button();
	$(document).ready(function () {
		$('#zoneDesktopmanagerGetBureauxForm').hide();
	});


	$('#zoneDesktopmanagerGetBureauxForm').keypress(function (e) {
		if (e.keyCode == 13) {
			$('#zoneDesktopmanagerGetBureauxForm #searchButton').trigger('click');
			return false;
		}
		if (e.keyCode === 27) {
			$('#zoneDesktopmanagerGetBureauxForm #cancelButton').trigger('click');
			return false;
		}
	});



	$('#searchButton').click(function () {
		gui.request({
			url: $("#DesktopmanagerGetBureauxForm").attr('action'),
			data: $("#DesktopmanagerGetBureauxForm").serialize(),
			loader: true,
			updateElement: $('#liste .content'),
			loaderMessage: gui.loaderMessage,
		}, function (data) {
			$('#liste .content').html(data);
		});
	});
	// Ajout de l'action de recherche via la touche Entrée
	$(function () {
		$('#DesktopmanagerGetBureauxForm').keypress(function (event) {
			if (event.which == 13) {
				gui.request({
							url: $("#DesktopmanagerGetBureauxForm").attr('action'),
							data: $("#DesktopmanagerGetBureauxForm").serialize(),
							loader: true,
							updateElement: $('#liste .content'),
							loaderMessage: gui.loaderMessage,
						}, function (data) {
							$('#liste .content').html(data);
							$('div.modal-backdrop').hide();
						}
				);
				return false;
			}
		});
	});
	$('#DesktopmanagerGetBureauxForm.folded').hide();
	$(document).ready(function () {
		$('.pagination li span').click(function () {
			$(this).find('a').click();
		});

		// Gestion des actions
		gui.addbutton({
			element: $('#zoneDesktopmanagerGetBureauxFormButton'),
			button: {
				content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
				class: 'btn btn-info-webgfc btn-inverse cancelSearch ',
				action: function () {
					$(this).parents('.modal').modal('hide');
					loadBureaux();
				}

			}
		});

		gui.addbutton({
			element: $('#zoneDesktopmanagerGetBureauxFormButton'),
			button: {
				content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
				class: 'btn btn-success searchBtn',
				action: function () {
					var form = $(this).parents('.modal').find('form');
					gui.request({
						url: form.attr('action'),
						data: form.serialize(),
						loader: true,
						updateElement: $('#browser .filetree.typestype'),
						loaderMessage: gui.loaderMessage
					}, function (data) {
						$('#browser').empty();
						$('.content').html(data);
					});
					$(this).parents('.modal').modal('hide');
					$('div.modal-backdrop').hide();

				}

			}
		});
	});
	$('#DesktopmanagerIsdispatch').select2({allowClear: true});
</script>
