<?php

/**
 *
 * Desktopsmanagers/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="col-sm-12">
    <?php
		$formDesktopsmanagers = array(
			'name' => 'Desktopmanager',
			'label_w' => 'col-sm-5',
			'input_w' => 'col-sm-6',
			'input' => array(
				'Desktopmanager.name' => array(
					'labelText' =>__d('desktopmanager', 'Desktopmanager.name'),
					'inputType' => 'text',
					'items'=>array(
						'required'=>true,
						'type'=>'text'
					)
				),
				'Desktopmanager.profil_id' => array(
					'labelText' => 'Type de profil',
					'inputType' => 'select',
					'items'=>array(
						'required'=>true,
						'empty'=>true,
						'type'=>'select',
						'options'=> $profils
					)
				)
			)
		);

		echo $this->Formulaire->createForm($formDesktopsmanagers);
	?>
	<div class='profilscheck'>
		<?php
		$formDesktopsmanagersByProfil = array(
			'name' => 'Desktopmanager',
			'label_w' => 'col-sm-5',
			'input_w' => 'col-sm-6',
			'input' => array(
				'Desktop.Desktop' => array(
					'labelText' =>__d('desktopmanager', 'Desktopmanager.desktop_id'),
					'inputType' => 'select',
					'items'=>array(
						'required' => true,
						'type'=>'select',
						'options' => [],
						'empty' => true,
						'multiple' => true
					)
				),
				'Desktopmanager.isdispatch' => array(
					'labelText' =>__d('desktopmanager', 'Desktopmanager.isdispatch'),
					'inputType' => 'checkbox',
					'changeWidth' =>true,
					'items'=>array(
						'type'=>'checkbox',
						'new_label_w' =>'col-sm-8',
						'new_input_w' =>'col-sm-4',
					)
				),
				'Desktopmanager.parent_id' => array(
					'labelText' =>__d('desktopmanager', 'Desktopmanager.parent_id'),
					'inputType' => 'select',
					'items' => array(
						'type' => 'select',
						'options' => $bureauxChef,
						'empty' => true
					)
				)
			)
		);
		echo $this->Formulaire->createForm($formDesktopsmanagersByProfil);

		?>
	</div>

</div>
<?php
echo $this->Form->end();
?>
<script type="text/javascript">



    $('#DesktopDesktop').select2({allowClear: true, placeholder: "Sélectionner un profil"});
    $('#DesktopmanagerProfilId').select2({allowClear: true, placeholder: "Quel profil sera associé ?", empty: true});

	$('.profilscheck').hide();
	$('#DesktopmanagerProfilId').change(function () {
		$.ajax({
			type: 'GET',
			url: "/desktopsmanagers/getAllDesktopsByProfils/" + $('#DesktopmanagerProfilId').val(),
			dataType: 'json',
			success: function (data) {
				for( i in data ) {
					$("#DesktopDesktop").append($("<option value='" + data[i].id + "'>" + data[i].name + "</option>"));
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				layer.msg("<strong>Erreur: </strong>Erreur de récupération de la liste des profils, veuillez vérifier les informations de connexion");
			}
		});
		$('.profilscheck').show();
		$('#DesktopmanagerParentId').select2({allowClear: true, placeholder: "Qui est mon responsable ?", empty: true});
		$("#DesktopDesktop").empty();
	});

</script>
