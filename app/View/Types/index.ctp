<?php

/**
 *
 * Types/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$pagination = '<p>' . $this->Paginator->counter(
				array(
					'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
				)
		) . '</p>';

if (Set::classicExtract($this->params, 'paging.Type.pageCount') > 1) {
	$pagination = '<div class="pagination">
			' . implode(
					'', Set::filter(
							array(
								$this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
								$this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
								$this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
								$this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
								$this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
							)
					)
			) . '
		</div>';
}
?>

<div class="types index">
    <h2><?php echo __d('type', 'Types'); ?></h2>

    <ul class="actions">
        <li><?php echo $this->Html->link(__('New Type', true), array('action' => 'add')); ?></li>
    </ul>

<?php echo $pagination; ?>
    <table>
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Type.id', __d('type', 'Type.id')); ?></th>
                <th><?php echo $this->Paginator->sort('Type.name', __d('type', 'Type.name')); ?></th>
                <th><?php echo $this->Paginator->sort('Type.created', __d('type', 'Type.created')); ?></th>
                <th><?php echo $this->Paginator->sort('Type.modified', __d('type', 'Type.modified')); ?></th>
                <th class="actions" colspan="3"><?php echo __d('default', 'Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
<?php
$i = 0;
foreach ($types as $type):
	$i++;
	$class = ( $i % 2 ? 'odd' : 'even' );
	?>
            <tr class="<?php echo $class; ?>">
                <td><?php echo $type['Type']['id']; ?>&nbsp;</td>
                <td><?php echo $type['Type']['name']; ?>&nbsp;</td>
                <td><?php echo $type['Type']['created']; ?>&nbsp;</td>
                <td><?php echo $type['Type']['modified']; ?>&nbsp;</td>
                <td class="actions"><?php echo $this->Html->link(__('View', true), array('action' => 'view', $type['Type']['id'])); ?></td>
                <td class="actions"><?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $type['Type']['id'])); ?></td>
                <td class="actions"><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $type['Type']['id']), null, sprintf(__('Are you sure you want to delete « %s » ?', true), $type['Type']['name'])); ?></td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
<?php echo $pagination; ?>
</div>
<script>
    $(document).ready(function () {
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
    });
</script>
