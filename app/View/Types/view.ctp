<?php
/**
 *
 * Types/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="types view">
	<h2><?php echo __d('type', 'Type'); ?></h2>
	<ul class="actions">
		<li><?php echo $this->Html->link(__d('type', 'Edit Type'), array('action' => 'edit', $type['Type']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__d('type', 'Delete Type'), array('action' => 'delete', $type['Type']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $type['Type']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__d('type', 'New Type'), array('action' => 'add')); ?> </li>
	</ul>

	<dl><?php $i = 0;
$class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('type', 'Type.id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $type['Type']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('type', 'Type.name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $type['Type']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('type', 'Type.created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $type['Type']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('type', 'Type.modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $type['Type']['modified']; ?>
			&nbsp;
		</dd>
	</dl>

	<div class="related">
		<h3><?php __('Related Soustypes'); ?></h3>
		<?php if (!empty($type['Soustype'])): ?>
			<table cellpadding = "0" cellspacing = "0">
				<tr>
					<th><?php __('Id'); ?></th>
					<th><?php __('Name'); ?></th>
					<th><?php __('Created'); ?></th>
					<th><?php __('Modified'); ?></th>
					<th><?php __('Type Id'); ?></th>
					<th><?php __('Circuit Id'); ?></th>
					<th class="actions"><?php echo __d('default', 'Actions'); ?></th>
				</tr>
				<?php
				$i = 0;
				foreach ($type['Soustype'] as $soustype):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
					?>
					<tr<?php echo $class; ?>>
						<td><?php echo $soustype['id']; ?></td>
						<td><?php echo $soustype['name']; ?></td>
						<td><?php echo $soustype['created']; ?></td>
						<td><?php echo $soustype['modified']; ?></td>
						<td><?php echo $soustype['type_id']; ?></td>
						<td><?php echo $soustype['circuit_id']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('View', true), array('controller' => 'soustypes', 'action' => 'view', $soustype['id'])); ?>
							<?php echo $this->Html->link(__('Edit', true), array('controller' => 'soustypes', 'action' => 'edit', $soustype['id'])); ?>
							<?php echo $this->Html->link(__('Delete', true), array('controller' => 'soustypes', 'action' => 'delete', $soustype['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $soustype['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		<?php endif; ?>

		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('New Soustype', true), array('controller' => 'soustypes', 'action' => 'add')); ?> </li>
			</ul>
		</div>
	</div>
</div>
