<?php

/**
 *
 * Operations/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
 <?php
echo $this->Html->tag('div', $this->Html->tag('i',' Ajouter un sous-type', array('class'=>'fa fa-plus-circle','aria-hidden'=>'true')), array('id' => 'addSousoperationsValue','class'=>'btn btn-info-webgfc add-edit-btn','title'=> __d('operation', 'Operation.addSousoperations') . ' pour l\'OP '.$numOP));
if (!empty($selectsousoperations) ){
     $fields = array(
        'name'
    );

    $actions = array(
        "add" => array(
            "url" => '/sousoperations/addUsers/'.$operationId,
            "updateElement" => "$('#infos .content')",
            "formMessage" => true,
            "loader" => true,
            "refreshAction" => "affichageTab();"
        ),
        "edit" => array(
            "url" => '/sousoperations/edit/',
            "updateElement" => "$('#infos .content')",
            "formMessage" => true,
            "loader" => true,
            "refreshAction" => "affichageTab();"
        ),
        "delete" => array(
            "url" => '/sousoperations/delete/',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "affichageTab();"
        )
    );
    $options = array();

    $data_table = $this->Liste->drawTbody($selectsousoperations, 'Sousoperation', $fields, $actions, $options);
    $data = (json_decode($data_table,true));
    $i = 0;
    foreach ($selectsousoperations as $sousoperation){
        if(!empty($sousoperation['Sousoperation']['etape'])){
            ksort($sousoperation['Sousoperation']['etape']);
            $desktopmanagers = $sousoperation['Sousoperation']['etape'];
            $data[$i]['Desktopmanager']="<ul>";
            foreach ($desktopmanagers as $key => $ssoperation) {
                $data[$i]['Desktopmanager'].="<li>".$ssoperation."</li>";
            }
            $data[$i]['Desktopmanager'].="</ul>";
        }
        $i++;
    }
    $data = json_encode($data);
?>

<div  class="bannette_panel panel-body">
    <table id="table_sousoperations"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height="400"
           >
    </table>
</div>


<script type="text/javascript">
    $('#table_sousoperations')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d('operation', 'Sousoperation.name'); ?>",
                        class: "name"
                    },
                    {
                        field: "Desktopmanager",
                        title: "<?php echo __d('operation', 'Sousoperation.agent'); ?>",
                        class: "bureau"
                    },
                    {
                        field: "add",
                        title: "<?php echo "Ajouter un sous-type"; ?>",
                        class: "actions itemAddSousop",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions itemEditSousop",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions itemDeleteSousop",
                        width: "80px",
                        align: "center"
                    }
                ]
            })
            .on('search.bs.table', function (e, text) {
                itemAdd();
                itemEdit();
                itemDelete();
            });
</script>

<?php
 }else{
     echo $this->Html->div('alert alert-warning',__d('operation', 'DesktopmanagerOperation.void'));
 }
 ?>
<script type="text/javascript">
    function loadSousOperation() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/operations/getSousoperations/" . $operationId; ?>",
            updateElement: $('#infos .content #operation_tabs_2'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }


    $('#addSousoperationsValue').addClass('operationAddSousoperations');

    $('#addSousoperationsValue').button().click(function () {
        var url = "<?php echo Router::url(array('controller' => 'sousoperations', 'action' => 'add', $operationId)); ?>";
        gui.request({
            url: url,
            updateElement: $('#infos .content #operation_tabs_2'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    });

    function itemAdd() {
        $('#table_sousoperations .itemAddSousop .itemAdd').prop('onclick', null).off('click');
        $('#table_sousoperations .itemAddSousop .itemAdd').click(function () {
            var itemId = $(this).attr("itemId");
            var url = "<?php echo Router::url(array('controller' => 'sousoperations', 'action' => 'addUsers')); ?>/" + itemId;
            gui.request({
                url: url,
                updateElement: $('#infos .content #operation_tabs_2'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
        });
    }

    function itemEdit() {
        $('#table_sousoperations .itemEditSousop .itemEdit').prop('onclick', null).off('click');
        $('#table_sousoperations .itemEditSousop .itemEdit').click(function () {
            var itemId = $(this).attr("itemId");
            var url = "<?php echo Router::url(array('controller' => 'sousoperations', 'action' => 'edit')); ?>/" + itemId;
            gui.request({
                url: url,
                updateElement: $('#infos .content #operation_tabs_2'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
        });
    }

    function itemDelete() {
        $('#table_sousoperations .itemDeleteSousop .itemDelete').prop('onclick', null).off('click');
        $('#table_sousoperations .itemDeleteSousop .itemDelete').click(function () {
            var itemId = $(this).attr("itemId");
            swal({
                    showCloseButton: true,
                title: "<?php echo __d('default', 'Confirmation de suppression'); ?>",
                text: "<?php echo __d('default', 'Voulez-vous supprimer cet élément ?'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#3085d6",
                confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
                cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
            }).then(function (data) {
                if (data) {
                    gui.request({
                        url: "/sousoperations/delete/" + itemId,
                        loader: true,
                        loaderMessage: gui.loaderMessage,
                        updateElement: $('#infos .content')
                    }, function (data) {
                        getJsonResponse(data);
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                        affichageTab();
                        loadSousOperation();
                    });
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n\'avez pas supprimé.",
                        type: "error",

                    });
                }
            });
            $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });

        });
    }
    itemAdd();
    itemEdit();
    itemDelete();


    $('#table_sousoperations .itemDeleteSousop .itemDelete').css('cursor', 'pointer');
    $('#table_sousoperations .itemEditSousop .itemEdit').css('cursor', 'pointer');
    $('#table_sousoperations .itemAddSousop .itemAdd').css('cursor', 'pointer');
</script>
