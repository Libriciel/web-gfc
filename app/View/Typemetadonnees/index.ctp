<?php

/**
 *
 * Typemetadonnees/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$pagination = '<p>' . $this->Paginator->counter(
				array(
					'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
				)
		) . '</p>';

if (Set::classicExtract($this->params, 'paging.Typemetadonnee.pageCount') > 1) {
	$pagination = '<div class="pagination">
			' . implode(
					'', Set::filter(
							array(
								$this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
								$this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
								$this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
								$this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
								$this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
							)
					)
			) . '
		</div>';
}
?>

<div class="typemetadonnees index">
    <h2><?php __('Typemetadonnees'); ?></h2>

    <ul class="actions">
        <li><?php echo $this->Html->link(__('New Typemetadonnee', true), array('action' => 'add')); ?></li>
    </ul>

<?php echo $pagination; ?>
    <table>
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('Typemetadonnee.id', __d('typemetadonnee', 'Typemetadonnee.id')); ?></th>
                <th><?php echo $this->Paginator->sort('Typemetadonnee.name', __d('typemetadonnee', 'Typemetadonnee.name')); ?></th>
                <th><?php echo $this->Paginator->sort('Typemetadonnee.created', __d('typemetadonnee', 'Typemetadonnee.created')); ?></th>
                <th><?php echo $this->Paginator->sort('Typemetadonnee.modified', __d('typemetadonnee', 'Typemetadonnee.modified')); ?></th>
                <th class="actions" colspan="3"><?php echo __d('default', 'Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
<?php
$i = 0;
foreach ($typemetadonnees as $typemetadonnee):
	$i++;
	$class = ( $i % 2 ? 'odd' : 'even' );
	?>
            <tr class="<?php echo $class; ?>">
                <td><?php echo $typemetadonnee['Typemetadonnee']['id']; ?>&nbsp;</td>
                <td><?php echo $typemetadonnee['Typemetadonnee']['name']; ?>&nbsp;</td>
                <td><?php echo $typemetadonnee['Typemetadonnee']['created']; ?>&nbsp;</td>
                <td><?php echo $typemetadonnee['Typemetadonnee']['modified']; ?>&nbsp;</td>
                <td class="actions"><?php echo $this->Html->link(__('View', true), array('action' => 'view', $typemetadonnee['Typemetadonnee']['id'])); ?></td>
                <td class="actions"><?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $typemetadonnee['Typemetadonnee']['id'])); ?></td>
                <td class="actions"><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $typemetadonnee['Typemetadonnee']['id']), null, sprintf(__('Are you sure you want to delete « %s » ?', true), $typemetadonnee['Typemetadonnee']['name'])); ?></td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
<?php echo $pagination; ?>
</div>
<script>
    $(document).ready(function () {
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
    });
</script>
