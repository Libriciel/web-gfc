<?php

/**
 *
 * Typemetadonnees/add_edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<fieldset>
    <legend><?php __(sprintf('%s Typemetadonnee', Inflector::humanize($this->action))); ?></legend>
    <?php
        $formTypemetadonnee = array(
            'name' => 'Typemetadonnee',
            'label_w' => 'col-sm-4',
            'input_w' => 'col-sm-6',
            'input' => array(
                'Typemetadonnee.name' => array(
                    'labelText' =>__d('typemetadonnee', 'Typemetadonnee.name', true),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                )
            )
        );
        if ($this->action == 'edit') {
            $formTypemetadonnee['input']['Typemetadonnee.id'] = array(
                'Typemetadonnee.id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                )
            );
        }
        echo $this->Formulaire->createForm($formTypemetadonnee);
        echo $this->Form->end();
    ?>
</fieldset>
