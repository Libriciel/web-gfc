<?php

/**
 *
 * Dossiers/get_dossiers.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/wetextfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (!empty($dossiers)) {

    $fields = array(
        'name'
    );
    $actions = array(
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/affaires/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/affaires/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadDossiersAffaires();"
        )
    );

    $options = array();
    $tabactions = array();
    $tabupdateElements = array();

    foreach ($actions as $key => $val) {
        $tabactions[$key] = $val['url'];
        $tabupdateElements[$key] = $val['updateElement'];
    }

    $rights = array();

    if (isset($dossiers[0]['right_edit'])) {
        $rights['read'] = $dossiers[0]['right_edit'];
    }
    if (isset($dossiers[0]['right_delete'])) {
        $rights['delete'] = $dossiers[0]['right_delete'];
    }
    $data= '['.substr($this->Liste->drawTree($dossiers, 'Dossier', $fields, $tabactions, $tabupdateElements, $rights),0,-1).']';
} else {
    echo $this->Html->div('alert alert-warning',__d('affaire', 'Affaire.void'));
}
?>

<?php if (!empty($dossiers)) { ?>
<div class="bannette_panel panel-body">
    <div class="dossierReplierBtn"><?php echo $this->Html->tag('a','<i class="fa fa-minus-square-o" aria-hidden="true" ></i>',array('class'=>'btn btn-info-webgfc toggle-pile','escape'=>false, 'title'=>"Tout plier"));?> </div>
    <table id="dossiers_table" data-toggle="table">
        <tbody>

        </tbody>
    </table>
</div>
<?php } ?>
<script type="text/javascript">
    <?php if (!empty($dossiers)): ?>
        $('#dossiers_table').bootstrapTable({
            data: <?php echo $data;?>,
            columns: [
                {
                    field: 'name',
                    title: 'Dossiers / Affaires',
                    class: 'name'
                },
                {
                    field: 'edit',
                    title: 'Modifier',
                    class: 'actions thEdit',
                    width: "80px",
                    align: "center"
                },
                {
                    field: 'delete',
                    title: 'Supprimer',
                    class: 'actions thDelete',
                    width: "80px",
                    align: "center"
                }
            ]
        })
        .on('search.bs.table', function (e, text) {
            itemEdit();
            itemDelete();
        })
        .on('page-change.bs.table', function (number, size) {
            addClassActive();
        });
    <?php endif;?>

    $('.treeCollapseButton').css('cursor', 'pointer');

    $('.treeCollapseButton').toggle(function () {
        $(this).attr('src', '<i class="fa fa-minus-square-o" aria-hidden="true"></i>');
        $(this).removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
        var tempThis = $(this).parents('tr').find('td:first-child').html();
        var countThis = (tempThis.match(/&nbsp;/g) || []).length;
        var tempNext = $(this).parents('tr').next().find('td:first-child').html();
        var countNext = (tempNext.match(/&nbsp;/g) || []).length;
        var indexThisTr = $('#dossiers_table').find('tr').index($(this).parents('tr'));
        for (var i = indexThisTr + 1; i < $('#dossiers_table').find('tr').length; i++) {
            var tr = $('#dossiers_table').find('tr').eq(i);
            if (tr.find('td:first-child').html() != undefined && (tr.find('td:first-child').html().match(/&nbsp;/g) || []).length != countThis) {
                if ((tr.find('td:first-child').html().match(/&nbsp;/g) || []).length >= countNext) {
                    tr.hide();
                }
            } else {
                return false;
            }
        }
    }
    , function () {
        var temp = $(this).parents('tr').next().find('td:first-child').html();
        var count = (temp.match(/&nbsp;/g) || []).length;
        $('#dossiers_table').find('td').each(function () {
            if ($(this).html().match(/&nbsp;/g) != undefined && ($(this).html().match(/&nbsp;/g) || []).length >= count) {
                $(this).parent('tr').show();
            }
        });
        $(this).removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
    });

    //Pouvoir tout déplier/replier d'un clic
    $(".toggle-pile").click(function () {
        var btn = $(this).find('i');
        var link = $(this);
        if (btn.hasClass('fa-minus-square-o')) {
            $('.treeCollapseButton').each(function () {
                $(this).click();
            });
            btn.removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
            link.prop('title', 'Tout déplier');
        } else if (btn.hasClass('fa-plus-square-o')) {
            $('.treeCollapseButton').each(function () {
                $(this).click();
            });
            btn.removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
            link.prop('title', 'Tout plier');
        }
    });


    $(document).ready(function () {
        addClassActive();
    });

    function addClassActive() {
        $('#dossiers_table .active_column').hide();
        $('#dossiers_table .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }
    function addChildren() {
        $('#dossiers_table .children_column').hide();
        $('#dossiers_table .children_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('children');
            }
        });
    }

    function itemEdit() {
        $('#dossiers_table .itemEdit').click(function () {
            var itemid = $(this).attr('itemid');
            var url = "";
            var hasimg = $(this).parents('tr').find('i').hasClass('treeCollapseButton');

            var hasParent = $(this).attr('parentId');

            if( hasimg ) {
                url = "/dossiers/edit/" + itemid;
            }
            else if( !hasimg && hasParent == '1') {
                url = "/affaires/edit/" + itemid;
            }
            else {
                url = "/dossiers/edit/" + itemid;
            }
            gui.formMessage({
                url: url,
                loader: true,
                loaderMessage: gui.loaderMessage,
                updateElement: $('#webgfc_content'),
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {

                        var areValid = true;
                        $('.modal-body #dossiers_dos form').each(function () {
                            if ($(this).attr('action') !== undefined && $(this).attr('enctype') != "multipart/form-data") {
                                if (!form_validate($(this))) {
                                    areValid = false;
                                }
                            }
                        });
//console.log(areValid);
                        if (areValid) {
                            var submits = {};
                            var index = 0;
                            $('.modal-body form').each(function () {
                                if ($(this).attr('action') !== undefined && $(this).attr('enctype') != "multipart/form-data") {
                                    submits[index] = {'url': $(this).attr('action'), 'data': $(this).serialize()};
                                    index++;
                                }
                            });
                            submits[index] = 'end';
                            gui.submitAll({
                                submits: submits,
                                index: 0,
                                endFunction: "loadDossiersAffaires()",
                                loader: true,
                                loaderElement: $('#webgfc_content'),
                                loaderMessage: gui.loaderMessage
                            });
//                            $('#liste .table-list h3').empty();
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        } else {
                            swal({
                                showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }

            });
        });
    }
    function itemDelete() {
        $('#dossiers_table .itemDelete').click(function () {
            var itemid = $(this).attr('itemid');
            var url = "";
            var hasimg = $(this).parents('tr').find('i').hasClass('treeCollapseButton');

            var hasParent = $(this).attr('parentId');

            if( hasimg ) {
                url = "/dossiers/delete/" + itemid;
            }
            else if( !hasimg && hasParent == '1') {
                url = "/affaires/delete/" + itemid;
            }
            else {
                url = "/dossiers/delete/" + itemid;
            }

            swal({
                showCloseButton: true,
                title: '<?php echo  __d('default', 'Confirmation de suppression'); ?>',
                text: '<?php echo __d('default', 'Voulez-vous supprimer cet élément ?'); ?>',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#3085d6",
                cancelButtonText: "<?php echo  __d('default', 'Button.cancel'); ?>",
                confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
            }).then(function (data) {
                if (data) {
                    gui.request({
                        url: url,
                        updateElement: $('#webgfc_content'),
                        loader: true,
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        getJsonResponse(data);
                        loadDossiersAffaires();
                    });
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n\'avez pas supprimé.",
                        type: "error",

                    });
                }
            });
            $('.swal2-cancel').css('margin-left', '-320px');
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
            $('.swal2-cancel').css('border', 'solid 1px');

            $('.swal2-cancel').hover(function () {
                $('.swal2-cancel').css('background-color', '#5397a7');
                $('.swal2-cancel').css('color', 'white');
                $('.swal2-cancel').css('border-color', '#5397a7');
            }, function () {
                $('.swal2-cancel').css('background-color', 'transparent');
                $('.swal2-cancel').css('color', '#5397a7');
                $('.swal2-cancel').css('border-color', '#3C7582');
            });

            $('.swal2-icon swal2-warning' ).css('display', 'block');
        });
    }
    itemEdit();
    itemDelete();
    $('.export_dossiers').show();
</script>
