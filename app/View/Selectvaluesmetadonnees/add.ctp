<?php

/**
 *
 * Selectvaluesmetadonnees/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="col-sm-12 zone-form">
    <legend class="panel-title"><?php echo __d('selectvaluemetadonnee', 'Selectvaluemetadonnee.add'); ?></legend>
    <div class="panel-body">
            <?php
            $formSelectvaluemetadonnee = array(
                'name' => 'Selectvaluemetadonnee',
                'label_w' => 'col-sm-6',
                'input_w' => 'col-sm-6',
                'input' => array(
                    'Selectvaluemetadonnee.name' => array(
                        'labelText' => __d('selectvaluemetadonnee', 'Selectvaluemetadonnee.name'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Selectvaluemetadonnee.metadonnee_id' => array(
                        'inputType' => 'hidden',
                        'items'=>array(
                            'type'=>'hidden',
                            'value'=>$metadonneeId
                        )
                    ),
					'Selectvaluemetadonnee.active' => array(
						'inputType' => 'hidden',
						'items'=>array(
								'type'=>'hidden',
								'value' => true
						)
					)
                )
            );

            if( Configure::read( 'Webservice.GRC') ) {
                $formSelectvaluemetadonnee['input']['Selectvaluemetadonnee.sendgrc'] = array(
                    'labelText' =>__d('selectvaluemetadonnee', 'Selectvaluemetadonnee.sendgrc'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['Selectvaluemetadonnee']['sendgrc']
                    )
                );
            }
            echo $this->Formulaire->createForm($formSelectvaluemetadonnee);
            echo $this->Form->end();
            ?>
    </div>
    <div class="controls text-center" role="group" >
        <a id="cancel" class="btn btn-danger-webgfc btn-inverse "><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a id="valid" class="btn btn-success "><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script>
	$('div.panel-footer').hide();
	$('#valid').click(function () {
        var url = "<?php echo Router::url(array('controller' => 'selectvaluesmetadonnees', 'action' => 'add', $metadonneeId)); ?>";
        if (form_validate($('#SelectvaluemetadonneeAddForm'))) {
            gui.request({
                url: url,
                data: $('#SelectvaluemetadonneeAddForm').serialize(),
                loaderElement: $('#infos .content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                loadValues();
            });
            loadValues();

			$('div.panel-footer').show();
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }
    });
    $('#cancel').click(function () {
        loadValues();
    });
</script>
