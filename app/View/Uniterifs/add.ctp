<?php

/**
 *
 * Uniterifs/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$formUniterif = array(
    'name' => 'Uniterif',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Uniterif.name' => array(
            'labelText' =>__d('uniterif', 'Uniterif.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Uniterif.description' => array(
            'labelText' =>__d('uniterif', 'Uniterif.description'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'Uniterif.active' => array(
            'labelText' =>__d('uniterif', 'Uniterif.active'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>true,
            )
        )
    )
);
echo $this->Formulaire->createForm($formUniterif);
echo $this->Form->end();
?>
