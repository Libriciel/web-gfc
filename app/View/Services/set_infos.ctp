<?php

/**
 *
 * Services/set_infos.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="row panel-body">
<?php
$formService = array(
    'name' => 'Service',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Service.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Service.name' => array(
            'labelText' =>__d('service', 'Service.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Service.email' => array(
            'labelText' =>__d('service', 'Service.email'),
            'inputType' => 'email',
            'items'=>array(
                'type'=>'email'
            )
        ),
        'Service.parent_id' => array(
            'labelText' =>__d('service', 'Service.service_id'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $services,
                'empty' => true,
                'escape' => false
            )
        ),
        'Desktop.Desktop' => array(
            'labelText' => __d('service', 'Service.Desktop'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $desktops,
                'empty' => true,
                'escape' => false,
                'multiple' => true
            )
        ),
        'Service.active' => array(
            'labelText' =>__d('service', 'Service.active'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['Service']['active']
            )
        ),
    )
);
echo $this->Formulaire->createForm($formService);

?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Numérisation</h4>
        </div>
        <div class="panel-body form-horizontal">
            <legend style="width:95%"> Activation de la numérisation pour ce service</legend>
            <?php

                $true_false = array('true' => 'Oui', 'false' => 'Non');

                ?>
                    <?php
                    $formServiceRadio = array(
                        'name' => 'Service',
                        'label_w'=>'col-sm-5',
                        'input_w'=>'col-sm-6',
                        'form_url' =>array('controller' => 'services', 'action' => 'add'),
                        'input' =>array(
                            'scan_active' => array(
                                'labelText' =>"",
                                'inputType' => 'radio',
                                'items'=>array(
                                    'legend' => false,
                                    'separator'=> '</div><div  class="radioUseLdapLabel">',
                                    'before' => '<div class="radioUseLdapLabel">',
                                    'after' => '</div>',
                                    'label' => true,
                                    'type'=>'radio',
                                    'options' => $true_false,
                                    'value' => ( isset( $this->request->data['Service']['scan_active'] ) && $this->request->data['Service']['scan_active'] ) ? 'true' : 'false'
                                )
                            )
                        )
                    );
                    echo $this->Formulaire->createForm($formServiceRadio);
                    ?>
            <div id='affiche' class="panel-body form-horizontal">
                <?php
                   $formServiceScanUse = array(
                       'name' => 'Service',
                       'label_w' => 'col-sm-5',
                       'input_w' => 'col-sm-7',
                       'form_url' => array( 'controller' => 'services', 'action' => 'admin'),
                       'input' => array(
                           'Service.scan_service_name' => array(
                               'labelText' =>__d('service', 'Service.scan_service_name'),
                               'inputType' => 'text',
                               'labelPlaceholder' =>__d('service', 'Service.placeholderScriptnameAdd'),
                               'items'=>array(
                                   'type'=>'text',
                                   'class'=>'scanparam'
                               )
                           ),
                           'Service.scan_desktopmanager_id' => array(
                               'labelText' =>__d('service', 'Service.scan_desktopmanager_id'),
                               'inputType' => 'select',
                               'items'=>array(
                                   'type'=>'select',
                                   'class'=>'scanparam',
                                   'options'=>$scanDesktops,
                                   'empty' => true
                               )
                           ),
                           'Service.scan_local_path' => array(
                               'labelText' =>__d('service', 'Service.scan_local_path'),
                               'inputType' => 'text',
                               'items'=>array(
                                   'type'=>'text',
                                   'class'=>'scanparam'
                               )
                           ),
                           'Service.scan_purge_delay' => array(
                               'labelText' =>__d('service', 'Service.scan_purge_delay'),
                               'inputType' => 'text',
                               'items'=>array(
                                   'type'=>'text',
                                   'class'=>'scanparam'
                               )
                           )
                       )
                   );
                   echo $this->Formulaire->createForm($formServiceScanUse);
               ?>
            </div>
        </div>
        </div>

<?php
echo $this->Form->end();
?>
</div>
<script type="text/javascript">
    $('#ServiceParentId').select2({allowClear: true, placeholder: "Sélectionner un service parent"});
    $('#DesktopDesktop').select2();

    $('#ServiceParentId').parent().removeClass('required');


    $('#ServiceScanActive').css('cursor', 'pointer');

    $("#ServiceScanActiveTrue").parent().children('label').css( 'cursor', 'pointer' );
    $("#ServiceScanActiveFalse").parent().children('label').css( 'cursor', 'pointer' );

    $('#ServiceSetInfosForm input[type=radio]').change(function () {
        $('#affiche').toggle();
    });

    if ($('#ServiceScanActiveTrue').is(":checked")) {
        $('#affiche').show();
    }

    if ($('#ServiceScanActiveFalse').is(":checked")) {
        $('#affiche').hide();
    }
    $('#ServiceScanDesktopmanagerId').select2();

    $("<span class='formatScanServiceName alert alert-warning'><?php echo  __d('service', 'Service.formatScanServiceName'); ?></span>").insertAfter("#ServiceScanServiceName");
</script>
