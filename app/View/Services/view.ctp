<?php
/**
 *
 * Services/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="services view">
    <h2><?php echo __d('service', 'Service'); ?></h2>
    <ul class="actions">
        <li><?php echo $this->Html->link(__d('service', 'Edit Service'), array('action' => 'edit', $service['Service']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__d('service', 'Delete Service'), array('action' => 'delete', $service['Service']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $service['Service']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__d('service', 'New Service'), array('action' => 'add')); ?> </li>
    </ul>

    <dl><?php $i = 0;
$class = ' class="altrow"';
?>
        <dt<?php if ($i % 2 == 0)
	echo $class;
?>><?php echo __d('service', 'Service.id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
				echo $class;
?>>
<?php echo $service['Service']['id']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
	echo $class;
?>><?php echo __d('service', 'Service.name'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
				echo $class;
?>>
			<?php echo $service['Service']['name']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
				echo $class;
			?>><?php //echo __d('service', 'Service.collectivite_id');  ?></dt>
        <dd<?php if ($i++ % 2 == 0)
					echo $class;
			?>>
			<?php echo $this->Html->link($service['Collectivite']['name'], array('controller' => 'collectivites', 'action' => 'view', $service['Collectivite']['id'])); ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
				echo $class;
			?>><?php echo __d('service', 'Service.created'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
				echo $class;
			?>>
		<?php echo $service['Service']['created']; ?>
            &nbsp;
        </dd>
    </dl>

    <div class="related">
        <h3><?php __('Related Users'); ?></h3>
<?php if (!empty($service['User'])): ?>
			<table cellpadding = "0" cellspacing = "0">
				<tr>
					<th><?php __('Id'); ?></th>
					<th><?php __('Username'); ?></th>
					<th><?php __('Password'); ?></th>
					<th><?php __('Nom'); ?></th>
					<th><?php __('Prenom'); ?></th>
					<th><?php __('Profil Id'); ?></th>
					<th><?php __('Created'); ?></th>
					<th><?php __('Modified'); ?></th>
					<th class="actions"><?php echo __d('default', 'Actions'); ?></th>
				</tr>
				<?php
				$i = 0;
				foreach ($service['User'] as $user):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
					?>
					<tr<?php echo $class; ?>>
						<td><?php echo $user['id']; ?></td>
						<td><?php echo $user['username']; ?></td>
						<td><?php echo $user['password']; ?></td>
						<td><?php echo $user['nom']; ?></td>
						<td><?php echo $user['prenom']; ?></td>
						<td><?php echo $user['profil_id']; ?></td>
						<td><?php echo $user['created']; ?></td>
						<td><?php echo $user['modified']; ?></td>
						<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
		<?php echo $this->Html->link(__('Edit', true), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
		<?php echo $this->Html->link(__('Delete', true), array('controller' => 'users', 'action' => 'delete', $user['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $user['id'])); ?>
						</td>
					</tr>
	<?php endforeach; ?>
			</table>
<?php endif; ?>

        <div class="actions">
            <ul>
                <li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
            </ul>
        </div>
    </div>
</div>
