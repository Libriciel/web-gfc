<?php

/**
 *
 * Services/get_services.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (!empty($services)) {
	$fields = array(
		'name',
                                'active'
	);
	$actions = array(
		"edit" => array(
			"url" => '/services/edit',
			"updateElement" => "$('#webgfc_content')",
			"formMessage" => true,
			'formMessageHeight' => '600',
			'formMessageWidth' => '600',
			"refreshAction" => "loadStructure();"
		),
		"delete" => array(
			"url" => '/services/delete',
			"updateElement" => "$('#webgfc_content')",
			"refreshAction" => "loadStructure();"
		)
	);
	$options = array(
		"nbItem" => $nbItem
	);
            $tabactions = array();
            $tabupdateElements = array();

            foreach ($actions as $key => $val) {
                $tabactions[$key] = $val['url'];
                $tabupdateElements[$key] = $val['updateElement'];
            }

            $rights = array();

            if (isset($services[0]['right_view'])) {
                $rights['view'] = $services[0]['right_view'];
            }
            if (isset($services[0]['right_edit'])) {
                $rights['read'] = $services[0]['right_edit'];
            }
            if (isset($services[0]['right_delete'])) {
                $rights['delete'] = $services[0]['right_delete'];
            }

            $data= '['.substr($this->Liste->drawTree($services, 'Service', $fields, $tabactions, $tabupdateElements, $rights),0,-1).']';


?>


<div class="bannette_panel panel-body">
    <div class="serviceReplierBtn"><?php echo $this->Html->tag('a','<i class="fa fa-minus-square-o" aria-hidden="true" ></i>',array('class'=>'btn btn-info-webgfc toggle-pile','escape'=>false, 'title'=>"Tout plier"));?> </div>
    <table id="services_table" data-toggle="table">
        <tbody>

        </tbody>
    </table>
</div>
<script>
    <?php if(!empty($nbItem)): ?>
    $('.liste_services span').html('<?php echo $nbItem; ?>');
    <?php endif; ?>
    $('#services_table').bootstrapTable({
        data: <?php echo $data;?>,
        search: true,
        columns: [
            {
                field: 'name',
                title: '<?php echo __d('service','Service.name'); ?>',
                class: 'name'
            },
            {
                field: "active",
                title: "<?php echo __d('service', 'Service.active'); ?>",
                class: "active_column"
            },
            {
                field: 'edit',
                title: 'Modifier',
                class: 'actions thEdit',
                width: "80px",
                align: "center"
            },
            {
                field: 'delete',
                title: 'Supprimer',
                class: 'actions thDelete',
                width: "80px",
                align: "center"
            }
        ]
    })
            .on('search.bs.table', function (e, text) {
                itemEdit();
                itemDelete();
            })
            .on('page-change.bs.table', function (number, size) {
                addClassActive();
            });

    $(document).ready(function () {
        addClassActive();
    });

    function addClassActive() {
        $('#services_table .active_column').hide();
        $('#services_table .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }

    function itemEdit() {
        $('#services_table .itemEdit').click(function () {
            var itemid = $(this).attr('itemid');
            gui.formMessage({
                url: "/services/edit/" + itemid,
                loader: true,
                loaderMessage: gui.loaderMessage,
                updateElement: $('#webgfc_content'),
                title: '<?php echo  __d('service', 'Edit Service');?>',
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        var errorFormTabs = [];
                        var areValid = true;
                        $('.modal-body #services_tabs form').each(function () {
                            if ($(this).attr('action') !== undefined && $(this).attr('enctype') != "multipart/form-data") {
                                if (!form_validate($(this))) {
                                    areValid = false;
                                }
                            }
                        });
                        if (areValid) {
                            var submits = {};
                            var index = 0;
                            $('.modal-body #services_tabs form').each(function () {
                                if ($(this).attr('action') !== undefined && $(this).attr('enctype') != "multipart/form-data") {
                                    submits[index] = {'url': $(this).attr('action'), 'data': $(this).serialize()};
                                    index++;
                                }
                            });
                            submits[index] = 'end';
                            gui.submitAll({
                                submits: submits,
                                index: 0,
                                endFunction: "loadServices()",
                                loader: true,
                                loaderElement: $('#webgfc_content'),
                                loaderMessage: gui.loaderMessage
                            });
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }

            });
        });
    }
    function itemDelete() {
        $('#services_table .itemDelete').click(function () {
            var itemid = $(this).attr('itemid');
            swal({
                showCloseButton: true,
                title: '<?php echo  __d('default', 'Confirmation de suppression'); ?>',
                text: '<?php echo __d('default', 'Voulez-vous supprimer cet élément ?'); ?>',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#3085d6",
                confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> <?php echo  __d('default', 'Button.delete') ; ?>',
                cancelButtonText: "<?php echo  __d('default', 'Button.cancel'); ?>",
            }).then(function (data) {
                if (data) {
                    gui.request({
                        url: "/services/delete/" + itemid,
                        updateElement: $('#webgfc_content'),
                        loader: true,
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        getJsonResponse(data);
                        loadStructure();
                    });
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n\'avez pas supprimé.",
                        type: "error",

                    });
                }
            });
            $('.swal2-cancel').css('margin-left', '-320px');
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
            $('.swal2-cancel').css('border', 'solid 1px');

            $('.swal2-cancel').hover(function () {
                $('.swal2-cancel').css('background-color', '#5397a7');
                $('.swal2-cancel').css('color', 'white');
                $('.swal2-cancel').css('border-color', '#5397a7');
            }, function () {
                $('.swal2-cancel').css('background-color', 'transparent');
                $('.swal2-cancel').css('color', '#5397a7');
                $('.swal2-cancel').css('border-color', '#3C7582');
            });
        });
    }
    itemEdit();
    itemDelete();

    $('.treeCollapseButton').css('cursor', 'pointer');

    $('.treeCollapseButton').toggle(function () {
        $(this).attr('src', '<i class="fa fa-minus-square-o" aria-hidden="true"></i>');
        $(this).removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
        var tempThis = $(this).parents('tr').find('td:first-child').html();
        var countThis = (tempThis.match(/&nbsp;/g) || []).length;
        var tempNext = $(this).parents('tr').next().find('td:first-child').html();
        var countNext = (tempNext.match(/&nbsp;/g) || []).length;
        var indexThisTr = $('#services_table').find('tr').index($(this).parents('tr'));
        for (var i = indexThisTr + 1; i < $('#services_table').find('tr').length; i++) {
            var tr = $('#services_table').find('tr').eq(i);
            if (tr.find('td:first-child').html() != undefined && (tr.find('td:first-child').html().match(/&nbsp;/g) || []).length != countThis) {
                if ((tr.find('td:first-child').html().match(/&nbsp;/g) || []).length >= countNext) {
                    tr.hide();
                }
            } else {
                return false;
            }
        }
    }
    , function () {
        var temp = $(this).parents('tr').next().find('td:first-child').html();
        var count = (temp.match(/&nbsp;/g) || []).length;
        $('#services_table').find('td').each(function () {
            if ($(this).html().match(/&nbsp;/g) != undefined && ($(this).html().match(/&nbsp;/g) || []).length >= count) {
                $(this).parent('tr').show();
            }
        });
        $(this).removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
    });

    //Pouvoir tout déplier/replier d'un clic
    $(".toggle-pile").click(function () {
        var btn = $(this).find('i');
        var link = $(this);
        if (btn.hasClass('fa-minus-square-o')) {
            $('.treeCollapseButton').each(function () {
                $(this).click();
            });
            btn.removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
            link.prop('title', 'Tout déplier');
        } else if (btn.hasClass('fa-plus-square-o')) {
            $('.treeCollapseButton').each(function () {
                $(this).click();
            });
            btn.removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
            link.prop('title', 'Tout plier');
        }
    });
</script>

<?php

} else {
	echo $this->Html->div('alert alert-warning',__d('service', 'Service.void'));
}
?>
