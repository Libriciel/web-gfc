<?php

/**
 *
 * Services/set_droits_meta.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
$options = array(
	'no' => $this->Html->image('/DataAcl/img/test-fail-icon.png', array('style' => 'width: 16px;', 'alt' => 'no')),
	'yes' => $this->Html->image('/DataAcl/img/test-pass-icon.png', array('style' => 'width: 16px;', 'alt' => 'yes')),
    'parentNodeMarkClose' => '<i class="fa fa-minus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
    'parentNodeMarkOpen' => '<i class="fa fa-plus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
//	'parentNodeMarkOpen' => $this->Html->image('/DataAcl/img/parent_node_mark_close.png', array('style' => 'width: 16px;vertical-align: middle;', 'alt' => 'closed')),
//	'parentNodeMarkClose' => $this->Html->image('/DataAcl/img/parent_node_mark_open.png', array('style' => 'width: 16px;vertical-align: middle;', 'alt' => 'opened')),
	'edit' => true,
	'tableRootClass' => '',/*'ui-widget ui-widget-content ui-corner-all',*/
	'legendClass' => 'ui-widget ui-widget-content ui-corner-all ui-state-focus',
	'fieldsetClass' => /*'ui-widget ui-widget-content ui-corner-all'*/'',
	'fieldsetStyle' => 'margin-top: 15px;',
	'legendStyle' => 'padding-left: 55px;padding-right: 55px;padding-top: 3px;padding-bottom: 3px;',
	'requester' => array(
		'model' => 'Service',
		'foreign_key' => $id
	),
	'aclType' => 'DataAcl',
	'fields' => array(
		'ressource' => 'Metadonnees',
		'read' => 'Lecture',
		'update' => 'Edition'
	)
);
$this->DataAcl->drawTable($dataRights, false, $options);
?>

<script type="text/javascript">
    initTables(true);
    $('.submit').each(function () {
        $('input[type=submit]', this).remove();
    });

    $('#ServiceSetDroitsMetaForm table tr').each(function () {
        var firstChk = $('td:nth-child(2) input[type="checkbox"]', this);
        var secondChk = $('td:nth-child(3) input[type="checkbox"]', this);

        if (secondChk.prop("checked")) {
            firstChk.prop("checked", true);
        }
        if (typeof firstChk.attr('checked') == "undefined") {
            secondChk.prop("checked", false);
        }
        secondChk.change(function () {
            if ($(this).prop("checked")) {
                firstChk.prop("checked", true);
            }
        });
        firstChk.change(function () {
            if (typeof $(this).attr('checked') == "undefined") {
                secondChk.prop("checked", false);
            }
        });
    });


    // On coche/décoche d'un coup
    $('#ServiceSetDroitsMetaForm table th').css({cursor: 'pointer'});
    $('#ServiceSetDroitsMetaForm table th').click(function () {
                <?php $foreignKey = 0;
                        foreach( $dataRights as $key => $value ):?>
                <?php $foreignKey = $value['foreign_key'];?>
        if ($('#ServiceSetDroitsMetaForm table th')[1].innerHTML == 'lecture') {
            if ($("#RightsMetadonnee<?php echo $foreignKey;?>Read").attr("state") == "checked") {
                $("#RightsMetadonnee<?php echo $foreignKey;?>Read").prop("checked", false);
                $("#RightsMetadonnee<?php echo $foreignKey;?>Read").attr("state", "unchecked");
            }
            else {
                $("#RightsMetadonnee<?php echo $foreignKey;?>Read").prop("checked", true);
                $("#RightsMetadonnee<?php echo $foreignKey;?>Read").attr("state", "checked");
            }
        }
        if ($('#ServiceSetDroitsMetaForm table th')[2].innerHTML == 'édition') {

            if ($("#RightsMetadonnee<?php echo $foreignKey;?>Update").attr("state") == "checked") {
                $("#RightsMetadonnee<?php echo $foreignKey;?>Update").prop("checked", false);
                $("#RightsMetadonnee<?php echo $foreignKey;?>Update").attr("state", "unchecked");
            }
            else {
                $("#RightsMetadonnee<?php echo $foreignKey;?>Update").prop("checked", true);
                $("#RightsMetadonnee<?php echo $foreignKey;?>Update").attr("state", "checked");
            }
        }
            <?php endforeach;?>
    });
</script>
