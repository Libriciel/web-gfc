<?php

/**
 *
 * Services/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div id="services_tabs">
    <ul  class="nav nav-tabs titre" role="tablist">
        <li class="active" ><a href="#services_tabs_1" role="tab" data-toggle="tab"><?php echo __d('default', 'InformationsGenerales'); ?></a></li>
        <li><a role="tab" data-toggle="tab" href="#services_tabs_2"><?php echo __d('service', 'Service.droitsData'); ?></a></li>
        <li><a  role="tab" data-toggle="tab" href="#services_tabs_3"><?php echo __d('service', 'Service.droitsMeta'); ?></a></li>
    </ul>

    <div class="tab-content">
        <div id="services_tabs_1" class="tab-pane fade active in">
        </div>
        <div  class="tab-pane fade"  id="services_tabs_2">
        </div>
        <div  class="tab-pane fade" id="services_tabs_3">
        </div>
    </div>

</div>



<script type="text/javascript">

    function affichageTab() {
        if ($('#services_tabs_1').length > 0 && $('#services_tabs_1').hasClass('active')) {
            gui.request({
                url: "<?php echo Configure::read('BaseUrl') . "/services/setInfos/" . $id ?>",
                updateElement: $('#services_tabs_1'),
            });
        }
        if ($('#services_tabs_2').length > 0 && $('#services_tabs_2').hasClass('active')) {
            gui.request({
                url: "<?php echo Configure::read('BaseUrl') . "/services/setDroitsData/" . $id ?>",
                updateElement: $('#services_tabs_2'),
            });
        }
        if ($('#services_tabs_3').length > 0 && $('#services_tabs_3').hasClass('active')) {
            gui.request({
                url: "<?php echo Configure::read('BaseUrl') . "/services/setDroitsMeta/" . $id ?>",
                updateElement: $('#services_tabs_3'),
            });
        }
    }

    $('#services_tabs_1').empty();
    $(document).ready(function () {
        // Change hash for page-reload
        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            affichageTab();
        })

        affichageTab();

    });
</script>
