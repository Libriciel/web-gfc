<?php
/**
 * @var \AsalaeCore\View\AppView $this
 */
foreach ($logs as $logname => $content) {
    $id = preg_replace('/[\W]+/', '_', basename($logname));
    $button = $this->Html->tag('button', '<i class="fa fa-play fa-fw" aria-hidden="true"></i>', array(
        'class' => 'btn btn-link tail',
        'onclick' => 'tailF("'.$this->Html->url( '/consoles/tail/'.$logname).'", "'.$id.'", this)',
    ));
    echo $this->Html->tag('div',
        $this->Html->tag('label', $logname)
        . ' '.$button
        . $this->Html->tag('textarea', $content, array('class' => 'form-control console', 'rows' => 10, 'id' => $id) ),
        array('class' => 'minifiable-target form-group' ) );
}
?>
<script>
$('textarea').each(function() {
    var e = $(this).get(0);
    e.scrollTop = e.scrollHeight - e.getBoundingClientRect().height;
});

    
    $('textarea').css( 'background', '#3c0d29' );
    $('textarea').css( 'color', 'white' );
var tailXhr;
function tailF(url, id, button) {
    var textarea = $('#'+id),
        changed = false,
        btn = $(button),
        modal = btn.closest('.modal');
    if (tailXhr) {
        tailXhr.abort();
    }
    btn.blur()
        .attr('onclick', '')
        .off('click.abort')
        .one('click.abort', function() {
            $('button.tail:not(.active)')
                .find('i.fa').removeClass('fa-lock').addClass('fa-play')
                .closest('.minifiable-target').show();
            textarea.attr('rows', 10);
            $(this).one('click.abort', function() {
                $(this).data('abortAjax', false);
                tailF(url, id, this);
            });
            $(this).data('abortAjax', true)
                .removeClass('active')
                .find('i.fa')
                .addClass('fa-play')
                .removeClass('fa-spinner')
                .removeClass('faa-spin')
                .removeClass('animated');
            modal.off('hide.bs.modal');
            tailXhr.abort();
        })
        .addClass('active')
        .find('i.fa')
        .removeClass('fa-play')
        .addClass('fa-spinner')
        .addClass('faa-spin')
        .addClass('animated');
    modal.on('hide.bs.modal', function () {
        return false;
    });
    if (typeof btn.data('abortAjax') === 'undefined') {
        btn.data('abortAjax', false);
    }
    $('button.tail:not(.active)')
        .find('i.fa').addClass('fa-lock').removeClass('fa-play')
        .closest('.minifiable-target').hide();
    textarea.attr('rows', 40);
    tailXhr = $.ajax({
        url: url,
        xhr: function() {
            var xhr = new XMLHttpRequest();
            var loaded = false;
            xhr.addEventListener("readystatechange", function() {
                if (this.readyState === 3 && !btn.data('abortAjax')) {// === LOADING

                    if (loaded) {
                        textarea.html(this.response.trim());
                        var e = textarea.get(0);
                        e.scrollTop = e.scrollHeight - e.getBoundingClientRect().height;
                        changed = true;
                    }
                    loaded = true;
                }
            }, false);
            return xhr;
        },
        success: function(content) {
            if (!btn.data('abortAjax')) {
                tailF(url, id);
            }
        }
    });
}
</script>


