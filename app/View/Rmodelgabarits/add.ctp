<?php

/**
 *
 * Rmodels/addgabarit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (empty($message)) {
?>
<div class="panel panel-default" id="rmodelgabarits_add">
    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
        Ajouter un gabarit
    </div>
    <div class="panel-body">
<?php
        $formRmodelgabarit = array(
            'name' => 'Rmodelgabarit',
            'label_w' => 'col-sm-5',
            'input_w' => 'col-sm-5',
			'form_url' => array( 'controller' => 'rmodelgabarits', 'action' => 'add' ),
            'form_target' => "modelUploadFrame",
            'form_type' => 'file',
            'input' => array(
                'Rmodelgabarit.soustype_id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden',
                        'value'=>$soustype_id
                    )
                ),
                'Rmodelgabarit.name' => array(
                    'labelText' =>__d('rmodel', 'Rmodelgabarit.name'),
                    'inputType' => 'text',
                    'items'=>array(
						'required' => true,
                        'type'=>'text'
                    )
                ),
                'Rmodelgabarit.file' => array(
                    'labelText' =>__d('rmodel', 'Rmodelgabarit.file'),
                    'inputType' => 'file',
					'items'=>array(
						'required' => true,
                        'type' => 'file',
                        'name' => 'myfile',
                        'class' => 'fileField'
                    )
                )
            )
        );
        echo $this->Formulaire->createForm($formRmodelgabarit);
        echo $this->Form->end();
	?>
    </div>
    <div class="panel-footer controls " role="group">

    </div>
</div>
<script type="text/javascript">

    gui.addbuttons({
        element: $('#rmodelgabarits_add .panel-footer'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    $('#rmodelgabarits_add').remove();
                    gui.request({
                        url: "<?php echo "/Soustypes/setRmodels/" . $soustype_id; ?>",
                        updateElement: $('#stype_tabs_3'),
                    });
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success ",
                action: function () {
                    var formgabarit = $('#RmodelgabaritAddForm');
                    if (form_validate(formgabarit)) {
                        formgabarit.submit();
                        $('#rmodelgabarits_add').remove();
                        gui.request({
                            url: "<?php echo "/Soustypes/setRmodels/" . $soustype_id; ?>",
                            updateElement: $('#stype_tabs_3'),
                        });
						layer.msg("<div class='info'><?php echo $message; ?></div>", {offset: '70px'});
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        ]
    });


    $("#modelUploadFrame").remove();
    $('body').append('<iframe name="modelUploadFrame" id="modelUploadFrame" src="#" style="width:0;height:0;border:none;"></iframe>');




    //var ulfile = gui.createupload({
    //			button: "<?php echo __d('default', 'Button.uploadFile'); ?>",
    //			label: "<?php echo __d('relement', 'Relement.add'); ?>",
    //			url: "/rmodelgabarits/add",
    //			fileInputName: "myfile",
    //			hiddenItem: {
    //				name: "soustypeId",
    //				value: <?php echo $soustype_id; ?>
    //			}
    //		});
    //		$('.relement_list fieldset').append(ulfile);
</script>
<?php } else { ?>
<script type="text/javascript">




    //		if (window.top.window.jQuery.jGrowl){
    //			window.top.window.jQuery.jGrowl.defaults.theme = 'ui-state-focus';
<?php if (!$create) { ?>
    //				window.top.window.jQuery.jGrowl.defaults.theme = 'ui-state-error';
<?php } ?>
    layer.msg("<div class='info'><?php echo $message; ?></div>", {offset: '70px'});
    window.top.window.stype_tabs_reload();
    //		}
</script>
<?php } ?>

