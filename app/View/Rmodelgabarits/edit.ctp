<?php

/**
 *
 * Modeldocs/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$formRmodelgabarit = array(
	'name' => 'Rmodelgabarit',
	'label_w' => 'col-sm-5',
	'input_w' => 'col-sm-5',
	'form_url' => array( 'controller' => 'rmodelgabarits', 'action' => 'edit', $rmodelgabarit['Rmodelgabarit']['id'] ),
	'form_target' =>'modelUploadFrame',
	'form_type' =>'file',
	'input' => array(
		'Rmodelgabarit.id' => array(
			'inputType' => 'hidden',
			'items'=>array(
				'type'=>'hidden',
				'value'=> $rmodelgabarit['Rmodelgabarit']['id']
			)
		),
		'Rmodelgabarit.soustype_id' => array(
			'inputType' => 'hidden',
			'items'=>array(
				'type'=>'hidden',
				'value'=> $rmodelgabarit['Rmodelgabarit']['soustype_id']
			)
		),
		'Rmodelgabarit.path' => array(
			'inputType' => 'hidden',
			'items'=>array(
				'type'=>'hidden',
				'value'=> $rmodelgabarit['Rmodelgabarit']['path']
			)
		),
		'Rmodelgabarit.name' => array(
			'labelText' =>__d('rmodelgabarit', 'Rmodelgabarit.name'),
			'inputType' => 'text',
			'items'=>array(
				'type'=>'text',
				'value'=>$rmodelgabarit['Rmodelgabarit']['name']
			)
		),
		'Rmodelgabarit.file' => array(
			'labelText' =>__d('rmodelgabarit', 'Rmodelgabarit.file'),
			'inputType' => 'file',
			'items'=>array(
				'type' => 'file',
				'name' => 'myfile',
				'class' => 'fileField'
			)
		)
	)
);
echo $this->Formulaire->createForm($formRmodelgabarit);
echo $this->Form->end();
?>
<script type="text/javascript">
	$("#modelUploadFrame").remove();
	$('body').append('<iframe name="modelUploadFrame" id="modelUploadFrame" src="#" style="width:0;height:0;border:none;"></iframe>');
	<?php if (isset($uploadComplete) && $uploadComplete == "ok") { ?>
	if (window.top.window.updateModelTab) {
		window.top.window.updateModelTab();
	}
	<?php } ?>
</script>
