<?php

/**
 *
 * Layouts/growl.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script type="text/javascript">
<?php if (isset($type) && $type == 'important') : ?>
    var settings = {header: "Important :", glue: 'before'};
<?php elseif (isset($type) && $type == 'error') : ?>
    var settings = {header: "Erreur :", sticky: true, theme: 'ui-state-error'};
<?php else : ?>
    var settings = {};
<?php endif ?>
    layer.msg("<?php echo $this->fetch('content'); ?>", settings);
</script>
<div id="flashMessage" class="message"><?php echo $this->fetch('content'); ?></div>
