<?php

/**
 *
 * Layouts/default.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 *
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo __d('default', 'titre'); ?>
        </title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        $cssFiles = array(
                JQUERYCSS . 'jquery-ui-' . JQUERYUIVERSION . '.custom',
                JQUERYCSSBASE . 'jquery.jgrowl',
                JQUERYCSSBASE . 'jquery.treeview',
                'select2', // Arnaud
                'font-awesome/css/font-awesome.min.css',
                'bootstrap_table/bootstrap-table.min.css',
                'webgfc',
                'bootstrap_datepicker/bootstrap-datepicker.min.css',
                'pdf_viewer.css',
                'sweetalert2.min.css'
//                'ls-composants-bootstrap-3.css'
        );
        echo $this->Html->css($cssFiles);
        echo $this->fetch('css');

        $jsFiles = array(
                JQUERYJSBASE . 'jquery-' . JQUERYVERSION,
                JQUERYJSBASE . 'jquery-ui-' . JQUERYUIVERSION . '.custom.min',
                JQUERYJSBASE . 'jquery.validate',
                JQUERYJSBASE . 'jquery.validate.messages_fr',
                JQUERYJSBASE . 'jquery.simpleclass',
                JQUERYJSBASE . 'jquery.treeview',
                JQUERYJSBASE . 'jquery.tablesorter.min',
                JQUERYJSBASE . 'jquery-migrate-1.2.1.min',
                JQUERYJSBASE . 'jquery.caret.1.02.js',
                'bootstrap-table.min.js',
                'gui',
                'flexpaper_flash',
                'default',
                '/data_acl/js/jquery.acl',
                'select2', //Arnaud
                'bootstrap.min.js',
                'bootstrap-datepicker.min.js',
                'bootstrap-datepicker.fr.min.js',
                'moment.min.js',
                'layer/layer.js',
                'compatibility.js',
                'validator.min.js',
                'sweetalert2.min.js',
                'bootstrap-table-fr-FR.min',
                'ls-elements.js'
        );
        if (Configure::read('debug') > 0) {
            $jsFiles[] = 'debugTools';
        }
        echo $this->Html->script($jsFiles);
        echo $this->fetch('script');
        ?>
    </head>
    <body>
        <div id="header"><?php echo $this->element('menu'); ?></div>
        <div id="webgfc_content" class="center-block">
            <!--<div class="col-md-offset-1 col-md-10 center-block" >-->
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>

        </div>
        <ls-lib-footer application_name="web-GFC v(<?php echo WEBGFCVERSION;?>)" style="position:fixed;width:100%;bottom:0;left:0;"></ls-lib-footer>

        <noscript>
            <div id="noJS">
                <br /><br /><br /><br /><br /><br />
                Impossible d'utiliser JavaScript !!!
                <br /><br />
                La prise en charge de JavaScript &agrave; &eacute;t&eacute; d&eacute;sactiv&eacute;e ou votre navigateur ne supporte pas JavaScript.
                <br />
                Afin de pouvoir utiliser cette application web, veuillez activer la prise en charge de JavaScript ou utiliser un navigateur diff&eacute;rent.
                <br /><br />
            </div>
        </noscript>
        <script type="text/javascript">
            $('#arianneEnv').button().click(function () {
                window.location.href = "<?php echo Router::url("/environnement"); ?>";
            });

            $(document).ready(function () {
                 <?php if( $this->params['controller'] == 'environnement' && $this->params['action'] != 'historique' ) { ?>
                    loadNotifications();
                    initNotifications();
                    loadTaches();
                    initTaches();
                    loadDeleg();
                    initDeleg();
                <?php } ?>
                <?php if (Configure::read('debug') > 0) { ?>
                    initDebugTools();
                <?php } ?>
            });

        </script>
    </body>


</html>
