<?php

/**
 *
 * Templatenotifications/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="col-sm-12">
<?php
    $formTemplatenotification = array(
        'name' => 'Templatenotification',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-8',
        'input' => array(
            'Templatenotification.name' => array(
                'labelText' =>__d('templatenotification', 'Templatenotification.name'),
                'inputType' => 'select',
                'labelPlaceholder' =>__d('templatenotification', 'Templatenotification.placeholderNameAdd'),
                'items'=>array(
                    'required'=>true,
                    'type'=>'select',
                    'options' => $templates,
                    'empty' => true
                )
            ),
            'Templatenotification.subject' => array(
                'labelText' =>__d('templatenotification', 'Templatenotification.subject'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required' => true
                )
            ),
            'Templatenotification.object' => array(
                'labelText' =>__d('templatenotification', 'Templatenotification.object'),
                'inputType' => 'textarea',
                'items'=>array(
                    'type'=>'textarea',
                    'required' => true
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formTemplatenotification);
    echo $this->Form->end();
?>

<div>
<legend><b>Liste des variables disponibles renseignables dans la partie "Objet du mail" du formulaire : </b></legend>
<pre class="templatenotifications">
<b>#PRENOM#</b> = reprend le Prénom de lutilisateur à qui le mail est adressé <br/>
<b>#NOM#</b> = reprend le Nom de lutilisateur à qui le mail est adressé <br/>
<b>#IDENTIFIANT_FLUX# </b>= reprend lID du flux stocké en BDD <br/>
<b>#REFERENCE_FLUX# </b>= reprend le numéro de référence unique du flux <br/>
<b>#NOM_FLUX# </b>= reprend le nom du flux renseigné par lagent<br/>
<b>#OBJET_FLUX# </b>= reprend lobjet du flux renseigné par lagent<br/>
<b>#LIBELLE_CIRCUIT# </b>= reprend lintitulé du circuit dans lequel le flux est en cours de traitement<br/>
<b>#ADRESSE_A_VISUALISER# </b>= reprend lURL de lapplication vers laquelle lutilisateur sera redirigée sil clique dessus<br/>
<b>#NOM_BUREAU# </b>= reprend le nom du bureau qui est délégué<br />
<b>#DEBUT_DELEGATION# </b>= reprend la date de début de délégation en cas de planification<br />
<b>#FIN_DELEGATION# </b>= reprend la date de fin de délégation en cas de planification<br />
<b>#MOTIF_REFUS# </b>= reprend le motif du refus<br />
</pre>
</div>
<script type="text/javascript">

    $('#TemplatenotificationName').select2({allowClear: true, placeholder: "Sélectionner un type de notification"});

</script>
