<?php

/**
 *
 * Relements/get_preview.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
echo $this->Element('flexPaper', array('height' => '350px', 'width' => '440px', 'filename' => '/files/previews/' . $tmpPreviewFileBasename));
?>


