<?php

/**
 *
 * Relements/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php if (!empty($message)) { ?>
<script type="text/javascript">
    if (window.top.window.$('#relements').length != 0) {
        gui.request({
            url: '/courriers/getRelements/<?php echo $flux_id; ?>',
            updateElement: window.top.window.$('#relements'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
//		if (window.top.window.jQuery.jGrowl){
//			window.top.window.jQuery.jGrowl.defaults.theme = 'ui-state-focus';
<?php if (!$create) { ?>
//				window.top.window.jQuery.jGrowl.defaults.theme = 'ui-state-error';
<?php } ?>
    layer.msg("<div class='info'><?php echo $message; ?></div>", {offset: '70px'});
//			window.top.window.loadActiveContextTab();
//		}
</script>
<?php } ?>
