<?php

/**
 *
 * Demarchessimplifiees/search.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel sCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php
echo $this->Html->script('appAttendable.js', array('inline' => true));
?>
<?php
if (!empty($data['demarche']->data->demarche)) {
	?>
	<div class="zone col-sm-12">
		<div  id="proceduredata" class="panel-body fieldsetView">
			<dl>
				<h4><img border="0" src="/img/demarchessimplifiees.png" width="" title="DS" > Démarche</h4>
				<div class="card">
					<table class="table col-sm-8">
						<tbody>
						<tr>
							<th class="libelle">N° de démarche :</th>
							<td class="col-sm-8"><?php echo $this->request->data['Demarchesimplifiee']['procedureId'];?></td>
						</tr>
						<tr>
							<th class="libelle">Description :</th>
							<td class="col-sm-8"><?php echo $data['demarche']->data->demarche->description;?></td>
						</tr>
						<tr>
							<th class="libelle">Statut :</th>
							<td class="col-sm-8"><?php echo ucfirst( $data['demarche']->data->demarche->state );?></td>
						</tr>

							<tr>
								<th class="libelle">Instructeurs :</th>
								<td class="col-sm-8"><?php echo nl2br($emailsInstructeurs);?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<h4>Service</h4>
				<div class="card">
					<table class="table col-sm-8">
						<tbody>
						<tr>
							<th class="libelle">Cette démarche est gérée par :</th>
							<td class="col-sm-8"><?php echo $data['demarche']->data->demarche->service->nom;?></td>
						</tr>
						<tr>
							<th class="libelle">Organisation :</th>
							<td class="col-sm-8"><?php echo $data['demarche']->data->demarche->service->organisme;?></td>
						</tr>
						<tr>
							<th class="libelle">Type d'organisation :</th>
							<td class="col-sm-8"><?php echo $data['demarche']->data->demarche->service->typeOrganisme;?></td>
						</tr>
						<!-- <tr>
							<th class="libelle">Adresse :</th>
							<td class="col-sm-8"><?php echo $data['demarche']->data->demarche->service->address;?></td>
						</tr>
						<tr>
							<th class="libelle">Mail :</th>
							<td class="col-sm-8"><?php echo $data['demarche']->data->demarche->service->email;?></td>
						</tr>
						<tr>
							<th class="libelle">Téléphone :</th>
							<td class="col-sm-8"><?php echo $data['demarche']->data->demarche->service->phone;?></td>
						</tr>
						<tr>
							<th class="libelle">Horaires :</th>
							<td class="col-sm-8"><?php echo $data['demarche']->data->demarche->service->schedule;?></td>
						</tr> -->
						</tbody>
					</table>
				</div>
			</dl>
		</div>
	</div>

	<div class="zone col-sm-12">
	<!-- FIXME: MEttre une condition pour afficher n° dossier uniquement si n° de démarche renseignée -->
	<?php if( isset($data['demarche']->data->demarche->dossiers->nodes[0]) && !empty($data['demarche']->data->demarche->dossiers->nodes[0]) ) { ?>
		<?php foreach ($data['demarche']->data->demarche->dossiers->nodes as $key => $dossier) {
			if( $this->request->data['Demarchesimplifiee']['hideiscreated'] == 1  && !isset($dossier->alreadyCreated) ) {
			?>
			<div class="zone col-sm-12">
				<div id="dossierdata" class="panel-body fieldsetView">
					<div id="dossier-show">
						<div class="container">

							<h4>Dossier</h4>
							<div class="card">
								<table class="table col-sm-8">
									<tbody>
									<tr>
										<th class="libelle">N° de dossier :</th>
										<td class="col-sm-8"><?php echo $dossier->number; ?></td>
									</tr>
									<tr>
										<th class="libelle">Dossier créé le :</th>
										<td class="col-sm-8"><?php echo !empty($dossier->datePassageEnConstruction) ? $this->Html2->ukToFrenchDateWithHourAndSlashes($dossier->datePassageEnConstruction) : null; ?></td>
									</tr>
									<tr>
										<th class="libelle">Dernière modification le :</th>
										<td class="col-sm-8"><?php echo !empty($dossier->dateDerniereModification) ? $this->Html2->ukToFrenchDateWithHourAndSlashes($dossier->dateDerniereModification) : null; ?></td>
									</tr>
									<tr>
										<th class="libelle">Dossier instruit le :</th>
										<td class="col-sm-8"><?php echo !empty($dossier->datePassageEnInstruction) ? $this->Html2->ukToFrenchDateWithHourAndSlashes($dossier->datePassageEnInstruction) : null; ?></td>
									</tr>
									<tr>
										<th class="libelle">Email&nbsp;:</th>
										<td class="col-sm-8"><?php echo $dossier->usager->email; ?></td>
									</tr>
									</tbody>
								</table>
							</div>
							<h4>Identité du demandeur</h4>
							<div class="card">
								<table class="table col-sm-8">
									<tbody>
									<tr>
										<th class="libelle">Civilité&nbsp;:</th>
										<td class="col-sm-8"><?php echo $dossier->demandeur->civilite; ?></td>
									</tr>
									<tr>
										<th class="libelle">Prénom&nbsp;:</th>
										<td class="col-sm-8"><?php echo $dossier->demandeur->prenom; ?></td>
									</tr>
									<tr>
										<th class="libelle">Nom&nbsp;:</th>
										<td class="col-sm-8"><?php echo $dossier->demandeur->nom; ?></td>
									</tr>
									</tbody>
								</table>
							</div>
							<h4>Votre demande</h4>
							<div class="card">
								<table class="table vertical dossier-champs">
									<tbody>
									<?php
										$boolean = [ 1 => 'Oui', 0 => 'Non'];
									?>
									<?php foreach ($dossier->champs as $d => $value) :?>
										<?php if( !empty( $value->value) || !empty($value->file) ) :?>
										<tr>
											<th class="libelle"><?php echo trim($value->label); ?>
												&nbsp:
											</th>
											<td class="col-sm-8">
												<div>
													<?php if( isset( $value->file ) ) :?>
														<p><?php echo $this->Html->link( $value->file->filename, $value->file->url, array('target' => '_blank'));?></p>
													<?php elseif( $data['demarche']->data->demarche->champDescriptors[$d]->type == 'date') :?>
														<p><?php  echo $this->Html2->ukToFrenchDateWithHourAndSlashes($value->value);?></p>
													<?php elseif( in_array($data['demarche']->data->demarche->champDescriptors[$d]->type, array('yes_no', 'engagement') ) ):?>
														<p><?php  echo $boolean[$value->value];?></p>
													<?php else:?>
														<p><?php echo $value->value; ?></p>
													<?php endif;?>
												</div>
											</td>
										</tr>
										<?php endif;?>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<div id="results">
						<div class="buttonDossier panel-footer" id="<?php echo $dossier->number ?>"
							 role="group">
						</div>
					</div>
				</div>
			</div>
		<?php }?>

			<?php
			if( !$this->request->data['Demarchesimplifiee']['hideiscreated'] && isset( $dossier->alreadyCreated ) ) {?>
				<div class="zone col-sm-12">
					<div id="dossierdata" class="panel-body fieldsetView">
						<div id="dossier-show">
							<div class="container">

								<h4>Dossier</h4>
								<div class="card">
									<table class="table col-sm-8">
										<tbody>
										<tr>
											<th class="libelle">N° de dossier :</th>
											<td class="col-sm-8"><?php echo $dossier->number; ?></td>
										</tr>
										<tr>
											<th class="libelle">Dossier créé le :</th>
											<td class="col-sm-8"><?php echo !empty($dossier->datePassageEnConstruction) ? $this->Html2->ukToFrenchDateWithHourAndSlashes($dossier->datePassageEnConstruction) : null; ?></td>
										</tr>
										<tr>
											<th class="libelle">Dernière modification le :</th>
											<td class="col-sm-8"><?php echo !empty($dossier->dateDerniereModification) ? $this->Html2->ukToFrenchDateWithHourAndSlashes($dossier->dateDerniereModification) : null; ?></td>
										</tr>
										<tr>
											<th class="libelle">Dossier instruit le :</th>
											<td class="col-sm-8"><?php echo !empty($dossier->datePassageEnInstruction) ? $this->Html2->ukToFrenchDateWithHourAndSlashes($dossier->datePassageEnInstruction) : null; ?></td>
										</tr>
										<tr>
											<th class="libelle">Email&nbsp;:</th>
											<td class="col-sm-8"><?php echo $dossier->usager->email; ?></td>
										</tr>
										</tbody>
									</table>
								</div>
								<h4>Identité du demandeur</h4>
								<div class="card">
									<table class="table col-sm-8">
										<tbody>
										<tr>
											<th class="libelle">Civilité&nbsp;:</th>
											<td class="col-sm-8"><?php echo $dossier->demandeur->civilite; ?></td>
										</tr>
										<tr>
											<th class="libelle">Prénom&nbsp;:</th>
											<td class="col-sm-8"><?php echo $dossier->demandeur->prenom; ?></td>
										</tr>
										<tr>
											<th class="libelle">Nom&nbsp;:</th>
											<td class="col-sm-8"><?php echo $dossier->demandeur->nom; ?></td>
										</tr>
										</tbody>
									</table>
								</div>
								<h4>Votre demande</h4>
								<div class="card">
									<table class="table vertical dossier-champs">
										<tbody>
										<?php
										$boolean = [ 1 => 'Oui', 0 => 'Non'];
										?>
										<?php foreach ($dossier->champs as $d => $value) :?>
											<?php if( !empty( $value->value) || !empty($value->file) ) :?>
												<tr>
													<th class="libelle"><?php echo trim($value->label); ?>
														&nbsp:
													</th>
													<td class="col-sm-8">
														<div>
															<?php if( isset( $value->file ) ) :?>
																<p><?php echo $this->Html->link( $value->file->filename, $value->file->url, array('target' => '_blank'));?></p>
															<?php elseif( $data['demarche']->data->demarche->champDescriptors[$d]->type == 'date') :?>
																<p><?php  echo $this->Html2->ukToFrenchDateWithHourAndSlashes($value->value);?></p>
															<?php elseif( in_array($data['demarche']->data->demarche->champDescriptors[$d]->type, array('yes_no', 'engagement') ) ):?>
																<p><?php  echo $boolean[$value->value];?></p>
															<?php else:?>
																<p><?php echo $value->value; ?></p>
															<?php endif;?>
														</div>
													</td>
												</tr>
											<?php endif;?>
										<?php endforeach;?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div id="results">
							<div class="buttonDossierAlreadyGenerated panel-footer"
								 reference="<?php echo $referenceGenerated[$dossier->number] ?>"
								 id="<?php echo $courrierGeneratedId[$dossier->number] ?>" role="group"></div>
						</div>
					</div>
				</div>
			<?php }
			?>
			<?php
		}
	}
	else {
		echo '<br/>';
		echo $this->Html->tag('div', 'Aucun dossier portant ce numéro n\'a été trouvé pour cette démarche', array('class' => 'alert alert-warning'));
	}
	?>
	</div>
<?php
}
else if (isset( $data['error'] ) && !empty( $data['error'] )) {
	echo '<br/>';
	echo $this->Html->tag('div', $data['error'], array('class' => 'alert alert-danger'));
}
else {
	echo '<br/>';
	echo $this->Html->tag('div', 'Aucune démarche, ni dossier, portant ce numéro n\'a été trouvé sur Démarches-Simplifiées', array('class' => 'alert alert-warning'));
}
?>
<script type="text/javascript">

	if ($('#infos .controls .btn').length == 0) {
		gui.buttonbox({
			element: $('#infos .controls'),
			align: "center"
		});

		// permet de masquer le formulaire de recherche, lorsqu'on le réouvre
		$('#liste h3').css('cursor', 'pointer');
		$('#liste h3').click(function () {
			$('#liste').hide();
		});

		// Bouton pour vider le formulaire de recherche
		gui.addbutton({
			element: $('#infos .controls'),
			button: {
				content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
				class: 'btn-info-webgfc btn-inverse',
				title: "<?php echo __d('default', 'Button.reset'); ?>",
				action: function () {
					if (!$(this).hasClass('ui-state-disabled')) {
						$('#infos .content').empty();
						$('#infos .controls').empty();
					}
					$('#liste').show();
					$('#infos').hide();
					$('.hide-search-formulaire').remove();
				}
			}
		});

		// Bouton pour réouvrir le formulaire de recherche
		gui.addbutton({
			element: $('#infos .controls'),
			button: {
				content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
				title: "<?php echo __d('default', 'Button.search'); ?>",
				class: 'btn-info-webgfc',
				action: function () {
					$('#liste').show();
					$('.hide-search-formulaire').show();
				}
			}
		});




	}

	// bouton pour créer le flux depuis le dossier
	gui.addbutton({
		element: $('#results .buttonDossier'),
		button: {
			content: '<i class="fa fa-plus" aria-hidden="true" > Créer un flux à partir de ce dossier</i>',
			title: ' Créer un flux depuis ce dossier',
			class: 'btn btn-info-webgfc btn-success',
			action: function () {
				var procedureId = '<?php echo $this->request->data['Demarchesimplifiee']['procedureId']?>';
				var dossierId = $(this).parent().attr('id');
				var url = '/demarchessimplifiees/createCourrier/' + procedureId + '/' + dossierId;
				gui.request({
					url: url,
					updateElement: $('#liste .content'),
					loader: true,
					loaderMessage: gui.loaderMessage
				}, function (data) {
					getJsonResponse(data);

					var courrierId = getJsonResponse(data)['newCourrierId'];
					window.open('/courriers/historiqueGlobal/' + courrierId, '_blank');
					window.location.reload();
				});
			}
		}
	});

	// bouton pour consulter le flux créé à partir du dossier
	gui.addbutton({
		element: $('#results .buttonDossierAlreadyGenerated'),
		button: {
			content: '<i class="fa fa-check" aria-hidden="true" > Flux déjà généré à partir de ce dossier</i>',
			title: ' Flux déjà généré à partir de ce dossier',
			class: 'btn btn-success',
			data_toggle: 'tab',
			data_target: '_blank',
			action: function () {
				var courrierId =  $(this).parent().attr('id');
				window.open('/courriers/historiqueGlobal/' + courrierId, '_blank');
			}
		}
	});
</script>
<?php  echo $this->Js->writeBuffer(); ?>
