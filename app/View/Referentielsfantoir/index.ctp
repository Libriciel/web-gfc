<?php

/**
 *
 * Addressbooks/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//echo $this->Html->css(array('administration'), null, array('inline' => false));
echo $this->Html->script('formValidate.js', array('inline' => true));
?>

<script type="text/javascript">

    function loadReferentielsfantoir() {
        $('#infos .content').empty();
        $('#liste table tr').removeClass('ui-state-focus');
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/Referentielsfantoir/getReferentiel"; ?>",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
</script>

<div class="container">
    <div id="liste" class="row">
        <div  class="table-list">
            <h3><?php echo __d('referentielfantoir', 'Referentielfantoir.liste'); ?></h3>
            <div class="content">
            </div>
        </div>
        <div class="controls panel-footer " role="group">
        </div>
    </div>
</div>

<div id="infos" class="modal fade " role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                        <h4 class="panel-title"><?php echo __d('referentielfantoir', 'Referentielfantoir.infos'); ?></h4>
                    </div>
                    <div class="panel-body content">
                    </div>
                    <div class="panel-footer controls " role="group">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    gui.buttonbox({
        element: $('#liste .controls')
    });

    gui.addbutton({
        element: $('#liste .controls'),
        class: 'btn  btn-info-webgfc',
        button: {
            content: '<i class="fa fa-upload" aria-hidden="true"></i>',
            title: "<?php echo __d('referentielfantoir', 'Referentielfantoir.import'); ?>",
            action: function () {
                $('#infos').modal('show');
                gui.request({
                    url: "/referentielsfantoir/import",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }
        }
    });


    gui.buttonbox({
        element: $('#infos .controls'),
    });

    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    $('#infos').modal('hide');
                }
            },
            {
                content: "<?php echo __d('referentielfantoir', 'Referentielfantoir.import'); ?>",
                class: "btn-success ",
                action: function () {
                    $('#ImportImportForm').submit();
                    $('#infos').modal('hide');
                }
            }
        ]
    });


    loadReferentielsfantoir();
</script>
