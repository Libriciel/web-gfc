<?php

/**
 *
 * Soustypes/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>

<div id="stype_tabs">
    <ul  class="nav nav-tabs titre" role="tablist">
        <li class="active" ><a href="#stype_tabs_1" role="tab" data-toggle="tab"><?php echo __d('default', 'Informations'); ?></a></li>
        <li><a href="#stype_tabs_2" role="tab" data-toggle="tab"><?php echo __d('soustype', 'Soustype.Armodels'); ?></a></li>
        <li><a href="#stype_tabs_3" role="tab" data-toggle="tab"><?php echo __d('soustype', 'Soustype.Rmodels'); ?></a></li>
    </ul>
    <div class="tab-content">
        <div id="stype_tabs_1" class="tab-pane fade active in">
                        <?php
                            $formSoustype = array(
                                'name' => 'Soustype',
                                'label_w' => 'col-sm-5',
                                'input_w' => 'col-sm-6',
                                'form_url' => array('controller' => 'Soustypes', 'action' => 'add', $type_id),
                                'input' => array(
                                    'Soustype.name' => array(
                                        'labelText' =>__d('soustype', 'Soustype.name'),
                                        'labelPlaceholder' => __d('soustype', 'Soustype.placeholderNameAdd'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'required'=>true,
                                            'type'=>'text'
                                        )
                                    ),
                                    'Soustype.type_id' => array(
                                        'labelText' =>__d('soustype', 'Soustype.type_id'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type'=>'select',
                                            'options' => $types,
                                            'empty' => true,
                                            'required'=>true
                                        )
                                    ),
                                    'Soustype.circuit_id' => array(
                                        'labelText' =>__d('soustype', 'Soustype.circuit_id'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type'=>'select',
                                            'options' => $circuits,
                                            'empty' => true
                                        )
                                    ),
                                    'Soustype.information' => array(
                                        'labelText' =>__d('soustype', 'Soustype.information'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text'
                                        )
                                    ),
                                    'Metadonnee.Metadonnee' => array(
                                        'labelText' =>__d('soustype', 'Metadonnee.Metadonnee'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type'=>'select',
                                            'multiple' => true,
                                            'options' => $listMeta
                                        )
                                    ),
                                )
                            );


						if (Configure::read('Soustype.MailForService')) {
							$formSoustype['input']['Soustype.mailservice'] =array(
									'labelText' => 'Mail repris dans la réponse',
									'inputType' => 'select',
									'items'=>array(
										'type'=>'text'
									)
							);
						}

						echo $this->Formulaire->createForm($formSoustype);
                        ?>

			<?php if( !empty($hasPastellActif) ) :?>
				<div class="col-sm-6">
					<legend>Cheminement personnalisé dans Pastell</legend>
					<?php
					$formSoustypeCheminementPastell = array(
							'name' => 'Soustype',
							'label_w' => 'col-sm-6',
							'input_w' => 'col-sm-6',
							'form_url' => array('controller' => 'Soustypes', 'action' => 'add', $type_id),
							'input' => array(
									'Soustype.envoi_signature' => array(
										'labelText' =>__d('soustype', 'Soustype.envoi_signature'),
										'inputType' => 'checkbox',
										'items'=>array(
											'type'=>'checkbox'
										)
									)
							)
					);
					echo $this->Formulaire->createForm($formSoustypeCheminementPastell);
					?>
					<div id="selectinsoustypeparapheur">
						<?php
							$formSoustypeCheminementPastellSelectionSoustypeIP = array(
									'name' => 'Soustype',
									'label_w' => 'col-sm-6',
									'input_w' => 'col-sm-6',
									'form_url' => array('controller' => 'Soustypes', 'action' => 'add', $type_id),
									'input' => array(
										'Soustype.soustype_parapheur' => array(
											'labelText' => 'Choix du sous-type du parapheur',
											'inputType' => 'select',
											'items'=>array(
												'type'=>'select',
												'options' => $listSoustypeIp,
												'empty' => true
											)
										)
									)
							);
							echo $this->Formulaire->createForm($formSoustypeCheminementPastellSelectionSoustypeIP);
						?>
					</div>
					<?php
					$formSoustypeCheminementPastell2 = array(
							'name' => 'Soustype',
							'label_w' => 'col-sm-6',
							'input_w' => 'col-sm-6',
							'form_url' => array('controller' => 'Soustypes', 'action' => 'add', $type_id),
							'input' => array(
									'Soustype.envoi_mailsec' => array(
											'labelText' =>__d('soustype', 'Soustype.envoi_mailsec'),
											'inputType' => 'checkbox',
											'items'=>array(
													'type'=>'checkbox'
											)
									),
									'Soustype.envoi_ged' => array(
											'labelText' =>__d('soustype', 'Soustype.envoi_ged'),
											'inputType' => 'checkbox',
											'items'=>array(
													'type'=>'checkbox'
											)
									),
									'Soustype.envoi_sae' => array(
										'labelText' =>__d('soustype', 'Soustype.envoi_sae'),
										'inputType' => 'checkbox',
										'items'=>array(
											'type'=>'checkbox'
										)
									),
							)
					);
					echo $this->Formulaire->createForm($formSoustypeCheminementPastell2);
					?>
				</div>
			<?php endif;?>
            <div class="col-sm-6">
                <legend><?php echo __d('soustype', 'Soustype.delai'); ?></legend>
                    <?php
                    $formSoustypeDelai = array(
                                'name' => 'Soustype',
                                'label_w' => 'col-sm-5',
                                'input_w' => 'col-sm-6',
                                'form_url' => array('controller' => 'Soustypes', 'action' => 'add', $type_id),
                                'input' => array(
                                    'Soustype.delai_nb' => array(
                                        'labelText' =>__d('soustype', 'Soustype.delai_nb'),
                                        'inputType' => 'number',
                                        'items'=>array(
                                            'type'=>'number'
                                        )
                                    ),
                                    'Soustype.delai_unite' => array(
                                        'labelText' =>__d('soustype', 'Soustype.delai_unite'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type'=>'select',
                                            'options' =>array('0' => 'jour', '1' => 'semaine', '2' => 'mois'),
                                            'empty' => true
                                        )
                                    )
                                )
                            );
                        echo $this->Formulaire->createForm($formSoustypeDelai);
                    ?>
            </div>
            <div class="col-sm-6">
                <legend><?php echo __d('soustype', 'Soustype.entrant'); ?></legend>
                                <?php
									$formSoustypeEntrant = array(
                                    'name' => 'Soustype',
                                    'label_w' => 'col-sm-6',
                                    'input_w' => 'col-sm-6',
                                    'form_url' => array('controller' => 'Soustypes', 'action' => 'add', $type_id),
                                    'input' => array(
                                        'Soustype.entrant' => array(
//                                            'labelText' =>__d('soustype', 'Soustype.name'),
                                            'labelPlaceholder' => __d('soustype', 'Soustype.delai_nb'),
                                            'inputType' => 'radio',
                                            'items'=>array(
                                                'legend' => false,
                                                'separator'=> '</div><div  class="radioUserGedLabel">',
                                                'before' => '<div class="radioUserGedLabel">',
                                                'after' => '</div>',
                                                'label' => true,
                                                'type'=>'radio',
                                                'required'=>true,
                                                'options'=> array(
                                                    1 => __d('soustype', 'Soustype.entrant.entrant'),
                                                    0 => __d('soustype', 'Soustype.entrant.sortant'),
                                                    2 => __d('soustype', 'Soustype.entrant.interne')
                                                ),
                                            )
                                        ),
                                        'Soustype.Soustypecible' => array(
                                            'labelText' =>__d('soustype', 'Soustype.parent_id'),
                                            'inputType' => 'select',
                                            'items'=>array(
                                                'type'=>'select',
                                                'multiple' => true,
                                                'options' => $soustypesList
                                            )
                                        )
                                    )
                                );
                                echo $this->Formulaire->createForm($formSoustypeEntrant);
                                ?>
            </div>
        </div>
        <div  class="tab-pane fade"  id="stype_tabs_2">
            <div class="alert alert-warning"><?php echo __d('soustype', 'Soustype.Armodels.undefined'); ?></div>
        </div>
        <div  class="tab-pane fade" id="stype_tabs_3">
            <div class="alert alert-warning"><?php echo __d('soustype', 'Soustype.Rmodels.undefined'); ?></div>
        </div>
    </div>
</div>

<script type="text/javascript">

	$('.radioLabel').css('margin', '5px');
	$('.radioLabel label').css('cursor', 'pointer');

    $('label[for="SoustypeSoustypecible"]').parent().hide();
    $('#SoustypeEntrant0').change(function () {
        $('#SoustypeParentId').select2({allowClear: true, placeholder: "Sélectionner un type parent"});
        $('#SoustypeSoustypecible').select2({allowClear: true, placeholder: "Sélectionner un circuit"});
        $('label[for="SoustypeSoustypecible"]').parent().show();
        $('#SoustypeSoustypecible').removeAttr('disabled');

    });
	$('#SoustypeEntrant1').change(function () {
		$('label[for="SoustypeSoustypecible"]').parent().hide();
	});
	$('#SoustypeEntrant2').change(function () {
		$('label[for="SoustypeSoustypecible"]').parent().hide();
	});

    $(document).ready(function () {
        $('#MetadonneeMetadonnee').select2();
        $('#SoustypeSoustypecible').select2();

        $('#SoustypeTypeId').select2({allowClear: true, placeholder: "Sélectionner un type"});
        $('#SoustypeCircuitId').select2({allowClear: true, placeholder: "Sélectionner un circuit"});
        $('#SoustypeDelaiUnite').select2({allowClear: true, placeholder: "Sélectionner un délai"});
    });

	// cas à cocher Envoi signature
	$('#SoustypeEnvoiSignature').change(function () {
		if ($('#SoustypeEnvoiSignature').attr('checked') == 'checked') {
			$('#selectinsoustypeparapheur').show();
		}
		else {
			$('#selectinsoustypeparapheur').hide();
		}
	});
	if ($('#SoustypeEnvoiSignature').attr('checked') == 'checked') {
		$('#selectinsoustypeparapheur').show();
	}
	else {
		$('#selectinsoustypeparapheur').hide();
	}
	$('#SoustypeSoustypeParapheur').select2({allowClear: true, placeholder: "Sélectionner un sous-type"});

</script>
