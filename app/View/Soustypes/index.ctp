<?php

/**
 *
 * Soustypes/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//echo $this->Html->css(array('administration'), null, array('inline' => false));
?>

<script type="text/javascript">
    function loadTypesSoutypes() {
        $('#infos .content').empty();
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/soustypes/getTypesSoustypes"; ?>",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
</script>


<div class="container">
    <div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
    <div id="liste" class="row">
        <div  class="table-list">
            <h3><?php echo __d('soustype', 'Soustype.liste'); ?><span class="action-title " role="group"></span></h3>
            <div  class="bannette_panel" style="padding:5px; margin-bottom: 35px;margin-right: 12px; ">
                <span class="bouton-search action-title pull-right"  role="group" style="margin-top: 10px;">
                    <a href="#" class="btn btn-info-webgfc btn-inverse" data-target="#SoustypeGetSoustypesFormModal" data-toggle="modal" title="Recherche avancée"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
                    <a href="/soustypes" class="btn btn-info-webgfc btn-inverse" id="searchCancel" data-target="#searchModal" title="Réinitialisation" style="margin-left: 15px;"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>
                </span>
            </div>
            <div class="content">

            </div>

        </div>
        <div class="controls panel-footer " role="group">

        </div>
    </div>
</div>

<div id="infos" class="modal fade" role="dialog">
    <div class="modal-dialog  modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                        <h4 class="panel-title"><?php echo __d('soustype', 'Soustype.infos'); ?></h4>
                    </div>
                    <div class="panel-body content">

                    </div>
                    <div class="panel-footer controls " role="group">

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



<script type="text/javascript">
    gui.buttonbox({
        element: $('#liste .controls')
    });

    gui.addbutton({
        element: $('#SoustypeGetSoustypesFormButton'),
        button: {
            content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
            class: 'btn btn-success searchBtn ',
            action: function () {
                var form = $(this).parents('.modal').find('form');
                gui.request({
                    url: form.attr('action'),
                    data: form.serialize(),
                    loader: true,
                    updateElement: $('#get_users_contents'),
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                        $('#infos .content').empty();
                        $('#infos .content').html(data);
                });
                $(this).parents('.modal').modal('hide');
                $(this).parents('.modal').empty();

            }

        }
    });
    gui.addbutton({
        element: $('#liste .table-list h3 .action-title'),
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> <?php echo __d("type", "New Type"); ?>',
            "class": "btn btn-primary ",
            action: function () {
                $('#infos').modal('show');
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/Types/add"; ?>",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }
        }
    });

    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> <?php echo __d("type", "New Type"); ?>',
            class: "btn-info-webgfc  ",
            action: function () {
                $('#infos').modal('show');
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/Types/add"; ?>",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }
        }
    });

    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> <?php echo __d("soustype", "New Soustype"); ?>',
            class: "btn-info-webgfc  ",
            action: function () {
                $('#infos').modal('show');
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/Soustypes/add"; ?>",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }
        }
    });

    gui.addbutton({
        element: $('#liste .table-list h3 .action-title'),
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> <?php echo __d("soustype", "New Soustype"); ?>',
            'class': "btn btn-primary ",
            action: function () {
                $('#infos').modal('show');
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/Soustypes/add"; ?>",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }
        }
    });

    gui.buttonbox({
        element: $('#infos .controls'),
        align: "center"
    });

    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    $('#infos').modal('hide');
                    // $('#infos').modal.empty();
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success ",
                action: function () {
                    var forms = $(this).parents('.modal').find('form');
                    $.each(forms, function (index, form) {
                        var url = form.action;
                        var id = form.id;
                        if (form_validate($('#' + id))) {
                            var radioPass = false;
                            $('input[id^="SoustypeEntrant"]').each(function () {
                                if ($(this).is(':checked')) {
                                    radioPass = true;
                                }
                            });
                            if (id != "SoustypeAddForm" && id != "SoustypeEditForm") {
                                radioPass = true;
                            }
                            if (radioPass) {
                                gui.request({
                                    url: url,
                                    data: $('#' + id).serialize()
                                }, function (data) {
                                    getJsonResponse(data);
                                    loadTypesSoutypes();
                                });
                                $(this).parents('.modal').modal('hide');
								// $(this).parents('.modal').empty();
                            } else {
                                swal({
                    showCloseButton: true,
                                    title: "Oops...",
                                    text: "Veuillez choisir le sens du flux!",
                                    type: "error",

                                });
                            }
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    });
                }
            }
        ]
    });

    loadTypesSoutypes();

    $(document).ready(function () {
        gui.addbutton({
            element: $('#SoustypeGetSoustypesFormButton'),
            button: {
                content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
                class: 'btn btn-info-webgfc btn-inverse cancelSearch ',
                action: function () {
                    $(this).parents('.modal').modal('hide');
					// $(this).parents('.modal').empty();
                    loadTypesSoutypes();
                }

            }
        });

        gui.addbutton({
            element: $('#SoustypeGetSoustypesFormButton'),
            button: {
                content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
                class: 'btn btn-success searchBtn',
                action: function () {
                    var form = $(this).parents('.modal').find('form');
                    gui.request({
                        url: form.attr('action'),
                        data: form.serialize(),
                        loader: true,
                        updateElement: $('#browser .filetree.typestype'),
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        $('#browser').empty();
                        $('.content').html(data);
                    });
                    $(this).parents('.modal').modal('hide');
					// $(this).parents('.modal').empty();

                }

            }
        });
    });
</script>

<script type="text/javascript">
    $('#TypeName').select2({allowClear: true, placeholder: "Sélectionner un type"});

    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: "<i class='fa fa-download' aria-hidden='true'></i> <?php echo __d('soustype', 'Button.exporttype'); ?>",
            title: "<?php echo __d('soustype', 'Button.exporttype'); ?>",
            class: 'btn  btn-info-webgfc export_types',
            action: function () {
                window.location.href = "<?php echo Router::url( array( 'controller' => 'soustypes', 'action' => 'export' ) + Hash::flatten( $this->request->data, '__' ) );?>";
            }
        }
    });
    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/admin#donnes"; ?>";
                }
            }
        ]
    });


	$('.close').click( function() {
		$(this).parents('.modal').modal('hide');
		// $(this).parents('.modal').empty();
	});
</script>


<div id="SoustypeGetSoustypesFormModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content zone-form">
            <div class="modal-header">
                <button data-dismiss="modal" class="close">×</button>
                <h4 class="modal-title">Recherche de type/sous-type</h4>
            </div>
            <div class="modal-body">
                            <?php
                    $formTypeSoustype = array(
                        'name' => 'Soustype',
                        'label_w' => 'col-sm-5',
                        'input_w' => 'col-sm-5',
						'form_url' => array( 'controller' => 'soustypes', 'action' => 'getTypesSoustypes' ),
                        'input' => array(
                            'Type.name' => array(
                                'labelText' =>__d('type', 'Type.name'),
                                'inputType' => 'text',
                                'items'=>array(
                                    'type' => 'text',
                                    'required' => false
                                )
                            ),
                            'Type.active' => array(
                                'labelText' => 'Type actif ?',
                                'inputType' => 'checkbox',
                                'items'=>array(
                                    'type' => 'checkbox',
                                    'checked' => true
                                )
                            ),
                            'Soustype.name' => array(
                                'labelText' => 'Sous-type',
                                'inputType' => 'text',
                                'items'=>array(
                                    'type' => 'text',
                                    'required' => false
                                )
                            ),
                            'Soustype.active' => array(
                                'labelText' => 'Sous-type actif ?',
                                'inputType' => 'checkbox',
                                'items'=>array(
                                    'type' => 'checkbox',
                                    'checked' => true
                                )
                            )
                        )
                    );
                        echo $this->Formulaire->createForm($formTypeSoustype);
                        echo $this->Form->end();
                        ?>
            </div>
            <div id="SoustypeGetSoustypesFormButton" class="modal-footer controls " role="group">

            </div>
                        <?php echo $this->Form->end();?>
        </div>
    </div>
</div>
