	<?php

	/**
	 *
	 * Soustypes/set_rmodels.ctp View File
	 *
	 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
	 *
	 * PHP version 7
	 * @author Stéphane Sampaio
	 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
	 * @link http://adullact.org/
	 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
	 */
	?>
	<div id="rmodel">
		<fieldset>
			<legend><?php echo __d('rmodel', 'Rmodel.list'); ?></legend>
			<?php
			$ulContent = '';
			if (empty($rmodels)) {
				$ulContent = $this->Html->tag('li', __d('soustype', 'Soustype.Rmodel.void'), array('class' => 'alert alert-warning'));
			} else {
				foreach ($rmodels as $rmodel) {
					$liContent = $this->Element('ctxdoc', array('gfcDocType' => 'Rmodel', 'ctxdoc' => $rmodel['Rmodel'], 'adminMode' => true,'sendCourrier'=>false,'sendGED'=>false));
					$ulContent .= $this->Html->tag('li', $liContent, array('class' => 'ctxdoc'));
				}
			}
			echo $this->Html->tag('ul', $ulContent, array('id' => 'rmodelListNew'));
			?>

		</fieldset>
		<fieldset>
			<legend><?php echo __d('rmodel', 'Rmodel.gabarit'); ?></legend>

			<?php
			$ul2Content = '';
			if (empty($rmodelsgabarits)) {
				$ul2Content = $this->Html->tag('li', __d('rmodel', 'Rmodel.Rmodelgabarit.void'), array('class' => 'alert alert-warning'));
			} else {
				foreach ($rmodelsgabarits as $rmodelgabarit) {
					$li2Content = $this->Element('ctxdoc', array('gfcDocType' => 'Rmodelgabarit', 'ctxdoc' => $rmodelgabarit['Rmodelgabarit'], 'adminMode' => true,'sendCourrier'=>false,'sendGED'=>false));
					$ul2Content .= $this->Html->tag('li', $li2Content, array('class' => 'ctxdoc'));
				}
			}
			echo $this->Html->tag('ul', $ul2Content, array('id' => 'rmodelListGabarit'));
			?>

		</fieldset>
		<?php

		echo $this->Html->tag('span', '<i class="fa fa-plus-circle" aria-hidden="true"></i> '.__d('rmodel', 'Rmodel.bttn.add'), array('class' => 'rmodel_bttn_add btn btn-info-webgfc'));
		echo $this->Html->tag('span', '<i class="fa fa-plus-circle" aria-hidden="true"></i> '.__d('rmodel', 'Rmodelgabarit.bttn.add'), array('class' => 'rmodel_gabarit_bttn_add btn btn-info-webgfc'));
		?>
	</div>
	<script type="text/javascript">

		$('div.modal-footer').show();
		$('.rmodel_bttn_add').button().click(function () {

			$('#rmodel').hide();
			$('div.modal-footer').hide();


			gui.request({
				url: '/rmodels/add/' + "<?php echo $soustypeId; ?>",
				updateElement: $('#stype_tabs_3'),
				loader: true,
				loaderMessage: gui.loaderMessage
			});
		});

		$('.rmodel_gabarit_bttn_add').button().click(function () {

			$('#rmodel').hide();
			$('div.modal-footer').hide();

			gui.request({
				url: '/rmodelgabarits/add/' + "<?php echo $soustypeId; ?>",
				updateElement: $('#stype_tabs_3'),
				loader: true,
				loaderMessage: gui.loaderMessage
			});
		});


		<?php if( !empty($rmodel) ) :?>
			$('.ctxdocBttnDelete').click(function () {
				var docId = $(this).parent().parent().parent().attr('itemId');
				swal({
							showCloseButton: true,
					title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
					text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#d33',
					cancelButtonColor: '#3085d6',
					confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
					cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
				}).then(function (data) {
					if (data) {
						gui.request({
							url: '/rmodels/delete/' + docId,
							loader: true,
							loaderMessage: gui.loaderMessage,
						}, function (data) {
							getJsonResponse(data);
							stype_tabs_reload();
						});
					} else {
						swal({
							showCloseButton: true,
							title: "Annulé!",
							text: "Vous n'avez pas supprimé, ;) .",
							type: "error",

						});
					}
				});
				$('.swal2-cancel').css('margin-left', '-320px');
				$('.swal2-cancel').css('background-color', 'transparent');
				$('.swal2-cancel').css('color', '#5397a7');
				$('.swal2-cancel').css('border-color', '#3C7582');
				$('.swal2-cancel').css('border', 'solid 1px');

				$('.swal2-cancel').hover(function () {
					$('.swal2-cancel').css('background-color', '#5397a7');
					$('.swal2-cancel').css('color', 'white');
					$('.swal2-cancel').css('border-color', '#5397a7');
				}, function () {
					$('.swal2-cancel').css('background-color', 'transparent');
					$('.swal2-cancel').css('color', '#5397a7');
					$('.swal2-cancel').css('border-color', '#3C7582');
				});

			});
			  // bouton edit
			$('.ctxdocBttnEditRmodel').click(function () {
				var rmodelId = $(this).parent().parent().parent().attr('itemId');
				gui.formMessage({
					updateElement: $('body'),
					loader: true,
					width: '550px',
					loaderMessage: gui.loaderMessage,
					title: 'Modèle de réponse',
					url: " /rmodels/edit/" + rmodelId,
					buttons: {
						'<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
							$(this).parents('.modal').modal('hide');
							$(this).parents('.modal').empty();
						},
						'<i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer': function () {
							var form = $('#RmodelEditForm');
							if (form_validate(form)) {
								form.submit();
								$('#RmodelEditForm').remove();
								gui.request({
									url: "<?php echo "/Soustypes/setRmodels/" . $rmodel['Rmodel']['soustype_id']; ?>",
									updateElement: $('#stype_tabs_3')
								});
								stype_tabs_reload();
							}
							else {
								swal({
							showCloseButton: true,
									title: "Oops...",
									text: "Veuillez vérifier votre formulaire!",
									type: "error",

								});
							}
							$(this).parents('.modal').modal('hide');
							$(this).parents('.modal').empty();
						}
					}
				});

			});
		<?php endif;?>

		// bouton edit
		$('.ctxdocBttnEditRmodelgabarit').click(function () {
			var rmodelgabaritId = $(this).parent().parent().parent().attr('itemId');
			gui.formMessage({
				updateElement: $('body'),
				loader: true,
				width: '550px',
				loaderMessage: gui.loaderMessage,
				title: 'Modèle de réponse',
				url: " /rmodelgabarits/edit/" + rmodelgabaritId,
				buttons: {
					'<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
						$(this).parents('.modal').modal('hide');
						$(this).parents('.modal').empty();
					},
					'<i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer': function () {
						var form = $('#RmodelgabaritEditForm');
						if (form_validate(form)) {
							form.submit();
							$('#RmodelgabaritEditForm').remove();
							gui.request({
								url: "<?php echo "/Soustypes/setRmodels/" . $rmodel['Rmodel']['soustype_id']; ?>",
								updateElement: $('#stype_tabs_3')
							});
							stype_tabs_reload();
						} else {
							swal({
								showCloseButton: true,
								title: "Oops...",
								text: "Veuillez vérifier votre formulaire!",
								type: "error",

							});
						}
						$(this).parents('.modal').modal('hide');
						$(this).parents('.modal').empty();
					}
				}
			});
		});

		$('.ctxdocBttnDeleteGabarit').click(function () {
			var ligneGabarit = $(this).parent().parent().parent();
			var docId = ligneGabarit.attr('itemId');
			var filename = ligneGabarit.find('.ctxdoc_name span.filename').attr('title');
			swal({
				showCloseButton: true,
				title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
				text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
				cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
			}).then(function (data) {
				if (data) {
					gui.request({
						url: '/rmodelgabarits/delete/' + docId + '/' + "<?php echo $soustypeId; ?>" + '/' + filename,
						loader: true,
						loaderMessage: gui.loaderMessage,
					}, function (data) {
						getJsonResponse(data);
						stype_tabs_reload();
					});
				} else {
					swal({
						showCloseButton: true,
						title: "Annulé!",
						text: "Vous n'avez pas supprimé, ;) .",
						type: "error",

					});
				}
			});
			$('.swal2-cancel').css('margin-left', '-320px');
			$('.swal2-cancel').css('background-color', 'transparent');
			$('.swal2-cancel').css('color', '#5397a7');
			$('.swal2-cancel').css('border-color', '#3C7582');
			$('.swal2-cancel').css('border', 'solid 1px');

			$('.swal2-cancel').hover(function () {
				$('.swal2-cancel').css('background-color', '#5397a7');
				$('.swal2-cancel').css('color', 'white');
				$('.swal2-cancel').css('border-color', '#5397a7');
			}, function () {
				$('.swal2-cancel').css('background-color', 'transparent');
				$('.swal2-cancel').css('color', '#5397a7');
				$('.swal2-cancel').css('border-color', '#3C7582');
			});

		});
	</script>
