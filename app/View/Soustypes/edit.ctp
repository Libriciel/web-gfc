<?php

/**
 *
 * Soustypes/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>

<div id="stype_tabs">
    <ul  class="nav nav-tabs titre" role="tablist">
        <li class="active" ><a href="#stype_tabs_1" role="tab" data-toggle="tab"><?php echo __d('default', 'Informations'); ?></a></li>
		<li><a href="#stype_tabs_2" role="tab" data-toggle="tab"><?php echo __d('soustype', 'Soustype.Armodels'); ?></a></li>
		<li><a href="#stype_tabs_3" role="tab" data-toggle="tab"><?php echo __d('soustype', 'Soustype.Rmodels'); ?></a></li>
		<li><a href="#stype_tabs_4" role="tab" data-toggle="tab"><?php echo __d('soustype', 'Soustype.getCircuit'); ?></a></li>
    </ul>
    <div class="tab-content">
		<div id="stype_tabs_1" class="tab-pane fade active in">
            <?php
            $formSoustype = array(
                'name' => 'Soustype',
				'label_w' => 'col-sm-5',
				'input_w' => 'col-sm-6',
                'input' => array(
					'Soustype.id' => array(
                        'inputType' => 'hidden',
                        'items'=>array(
                            'type'=>'hidden'
                        )
                    ),
                    'Soustype.name' => array(
                        'labelText' =>__d('soustype', 'Soustype.name'),
                        'labelPlaceholder' => __d('soustype', 'Soustype.placeholderNameAdd'),
                        'inputType' => 'text',
                        'items'=>array(
                            'required'=>true,
                            'type'=>'text'
                        )
                    ),
                    'Soustype.type_id' => array(
                        'labelText' =>__d('soustype', 'Soustype.type_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'options' => $types,
                            'empty' => true,
                            'required'=>true
                        )
                    ),
                    'Soustype.circuit_id' => array(
                        'labelText' =>__d('soustype', 'Soustype.circuit_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'options' => $circuits,
                            'empty' => true,
                            'selected' =>$this->request->data['Soustype']['circuit_id']
                        )
                    ),
                    'Soustype.information' => array(
                        'labelText' =>__d('soustype', 'Soustype.information'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Metadonnee.Metadonnee' => array(
                        'labelText' =>__d('soustype', 'Metadonnee.Metadonnee'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'multiple' => true,
                            'options' => $listMeta
                        )
                    ),
                    'Soustype.active' => array(
                        'labelText' =>__d('soustype', 'Soustype.active'),
                        'inputType' => 'checkbox',
                        'items'=>array(
                            'type'=>'checkbox',
                            'checked'=>$this->request->data['Soustype']['active']
                        )
                    ),
                )
            );
			if (Configure::read('Soustype.MailForService')) {
				$formSoustype['input']['Soustype.mailservice'] =array(
						'labelText' => 'Mail repris dans la réponse',
						'inputType' => 'select',
						'items'=>array(
								'type'=>'text'
						)
				);
			}
            echo $this->Formulaire->createForm($formSoustype);
            ?>
				<legend>Quelles sont les métadonnées obligatoires ?</legend>
				<?php
				if( !empty($listMetaSelected) ) {
					echo '<div class="col-sm-3"></div>';
					echo '<div class="col-sm-7" style="margin-top:-20px">';
					foreach ($listMetaSelected as $key => $value) {
						if(isset($this->request->data['Metadonnee']['obligatoire'][$key]) && $this->request->data['Metadonnee']['obligatoire'][$key]){
							echo '<label class="checkbox-inline"><input type="checkbox" name="data[Metadonnee][obligatoire][]" value ="'.$key.'" checked ="'.$key.'" id="MetadonneeObligatoire'.$key.'">'.$value.'</label>';
						}else{
							echo '<label class="checkbox-inline"><input type="checkbox" name="data[Metadonnee][obligatoire][]" value ="'.$key.'" id="MetadonneeObligatoire'.$key.'">'.$value.'</label>';
						}
					}
					echo '</div>';
				}
				else {
					echo $this->Html->tag('div', __d('soustype', 'Soustype.Metadonnee.void'), array('class' => 'alert alert-warning col-sm-12'));
				}
				?>

			<hr>
			<br /><?php if( count($listMetaSelected) > 4 && count($listMetaSelected) < 6 ) :?>
				<br /><br /><br /><br />
			<?php elseif(count($listMetaSelected) >= 6) :?>
				<br /><br /><br /><br /><br /><br />
			<?php endif;?>
			<?php if( !empty($hasPastellActif) ) :?>


					<legend>Cheminement personnalisé dans Pastell </legend>
					<?php
					$formSoustypeCheminementPastell = array(
							'name' => 'Soustype',
							'label_w' => 'col-sm-5',
							'input_w' => 'col-sm-6',
							'input' => array(
									'Soustype.envoi_signature' => array(
											'labelText' =>__d('soustype', 'Soustype.envoi_signature'),
											'inputType' => 'checkbox',
											'items'=>array(
													'type'=>'checkbox',
													'checked'=>$this->request->data['Soustype']['envoi_signature']
											)
									)
							)
					);
					echo $this->Formulaire->createForm($formSoustypeCheminementPastell);
					?>
					<div id="selectinsoustypeparapheur">
						<?php

						$formSoustypeCheminementPastellSelectionSoustypeIP = array(
								'name' => 'Soustype',
								'label_w' => 'col-sm-6',
								'input_w' => 'col-sm-6',
								'input' => array(
										'Soustype.soustype_parapheur' => array(
												'labelText' => 'Choix du sous-type du parapheur',
												'inputType' => 'select',
												'items'=>array(
														'type'=>'select',
														'options' => $listSoustypeIp,
														'empty' => true
												)
										)
								)
						);
						echo $this->Formulaire->createForm($formSoustypeCheminementPastellSelectionSoustypeIP);
						?>
					</div>
					<?php
						$formSoustypeCheminementPastell2 = array(
								'name' => 'Soustype',
								'label_w' => 'col-sm-5',
								'input_w' => 'col-sm-6',
								'input' => array(
										'Soustype.envoi_mailsec' => array(
												'labelText' =>__d('soustype', 'Soustype.envoi_mailsec'),
												'inputType' => 'checkbox',
												'items'=>array(
														'type'=>'checkbox',
														'checked'=>$this->request->data['Soustype']['envoi_mailsec']
												)
										),
										'Soustype.envoi_ged' => array(
												'labelText' =>__d('soustype', 'Soustype.envoi_ged'),
												'inputType' => 'checkbox',
												'items'=>array(
														'type'=>'checkbox',
														'checked'=>$this->request->data['Soustype']['envoi_ged']
												)
										),
										'Soustype.envoi_sae' => array(
												'labelText' =>__d('soustype', 'Soustype.envoi_sae'),
												'inputType' => 'checkbox',
												'items'=>array(
														'type'=>'checkbox',
														'checked'=>$this->request->data['Soustype']['envoi_sae']
												)
										),
								)
						);
						echo $this->Formulaire->createForm($formSoustypeCheminementPastell2);
					?>
			<?php endif;?>
			<hr>

                <legend><?php echo __d('soustype', 'Soustype.delai'); ?></legend>
                <?php
					$formSoustypeDelai = array(
						'name' => 'Soustype',
						'label_w' => 'col-sm-5',
						'input_w' => 'col-sm-6',
						'input' => array(
							'Soustype.delai_nb' => array(
								'labelText' =>__d('soustype', 'Soustype.delai_nb'),
								'inputType' => 'number',
								'items'=>array(
									'type'=>'number'
								)
							),
							'Soustype.delai_unite' => array(
								'labelText' =>__d('soustype', 'Soustype.delai_unite'),
								'inputType' => 'select',
								'items'=>array(
									'type'=>'select',
									'options' =>array('0' => 'jour', '1' => 'semaine', '2' => 'mois'),
									'empty' => true
								)
							)
						)
					);
					echo $this->Formulaire->createForm($formSoustypeDelai);
                ?>

                <legend><?php echo __d('soustype', 'Soustype.entrant'); ?></legend>
                <?php
                $formSoustypeEntrant = array(
                    'name' => 'Soustype',
                    'label_w' => 'col-sm-6',
                    'input_w' => 'col-sm-6',
                    'input' => array(
                        'Soustype.entrant' => array(
                            'inputType' => 'radio',
                            'items'=>array(
                                'legend' => false,
                                'separator'=> '</div><div  class="radioUserGedLabel">',
                                'before' => '<div class="radioUserGedLabel">',
                                'after' => '</div>',
                                'label' => true,
                                'type'=>'radio',
                                'options'=> array(
                                    1 => __d('soustype', 'Soustype.entrant.entrant'),
                                    0 => __d('soustype', 'Soustype.entrant.sortant'),
                                    2 => __d('soustype', 'Soustype.entrant.interne')
                                )
                            )
                        ),
                        'Soustype.Soustypecible' => array(
                            'labelText' =>__d('soustype', 'Soustype.parent_id'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'multiple' => true,
                                'options' => $soustypesList,
                                'selected' => $listSoustype
                            )
                        )
                    )
                );
                echo $this->Formulaire->createForm($formSoustypeEntrant);
            ?>
        </div>
        <div  class="tab-pane fade"  id="stype_tabs_2">
        </div>
        <div  class="tab-pane fade" id="stype_tabs_3">
        </div>
        <div  class="tab-pane fade" id="stype_tabs_4">
        </div>
    </div>


</div>
<script type="text/javascript">
function InsertSelectedValueIntoInput(el) {
        var input = $('input[type=text]:first', $(el).parent());
        var caretPos = $(input).caret().start;

        var newVal = $("option:selected", el).val();
        var beginVal = input.val().substr(0, caretPos);
        var endVal = input.val().substr(caretPos, input.val().length);

        input.val(beginVal + newVal + endVal);
    }

    function defRefInOut() {
        var inRef = "E_";
        var outRef = "S_";
        var internalRef = "I_";
        if ($('#SoustypeEntrant1').prop("checked")) {
            $('#refInOut').html(inRef);
        } else if ($('#SoustypeEntrant0').prop("checked")) {
            $('#refInOut').html(outRef);
        } else if ($('#SoustypeEntrant2').prop("checked")) {
            $('#refInOut').html(internalRef);
        }
    }

    function stype_tabs_reload() {
        affichageTab();
    }
    var soustypeName = $('#SoustypeName').val();
    var soustypeValue = "<?php echo $this->request->data['Soustype']['entrant'];?>";


    if (soustypeValue == '1' ) {
        $("input[name='data[Soustype][entrant]'][value=1]").prop("checked", true);
        $('label[for="SoustypeSoustypecible"]').parent().hide();
    } else if (soustypeValue == '2'  ) {
        $("input[name='data[Soustype][entrant]'][value=2]").prop("checked", true);
        $('label[for="SoustypeSoustypecible"]').parent().hide();
    } else if (soustypeValue = '0'  ) {
        $("input[name='data[Soustype][entrant]'][value=0]").prop("checked", true);
    }


    if ($('#SoustypeEntrant0').prop("checked")) {
        $('#SoustypeSoustypecible').removeAttr('disabled');
    }
    if ($('#SoustypeEntrant1').prop("checked") || $('#SoustypeEntrant2').prop("checked")) {
        $('#SoustypeSoustypecible').attr('disabled', "disabled");
    }

    function 	affichageTab() {
        if ($('#stype_tabs_2').length > 0 && $('#stype_tabs_2').is(':visible')) {
            gui.request({
                url: "<?php echo "/Soustypes/setArmodels/" . $id; ?>",
                updateElement: $('#stype_tabs_2'),
            });
        }
        if ($('#stype_tabs_3').length > 0 && $('#stype_tabs_3').is(':visible')) {
            gui.request({
                url: "<?php echo "/Soustypes/setRmodels/" . $id; ?>",
                updateElement: $('#stype_tabs_3'),
            });
        }
        if ($('#stype_tabs_4').length > 0 && $('#stype_tabs_4').is(':visible')) {
            gui.request({
                url: "<?php echo "/Soustypes/getCircuit/" . $id; ?>",
                updateElement: $('#stype_tabs_4'),
            });
        }
    }
    $(document).ready(function () {
        $('#MetadonneeMetadonnee').select2({allowClear: true, placeholder: "Sélectionner une métadonnée"});
        $('#SoustypeTypeId').select2({allowClear: true, placeholder: "Sélectionner un type"});
        $('#SoustypeCircuitId').select2({allowClear: true, placeholder: "Sélectionner un circuit"});
        $('#SoustypeDelaiUnite').select2({allowClear: true, placeholder: "Sélectionner un délai"});


//        $('#SoustypeSoustypecible').select2({allowClear: true, placeholder: "Sélectionner un sous-type"});
        $('#SoustypeSoustypecible').select2({allowClear: true, placeholder: "Sélectionner un sous-type"});
//        $('#ServiceService').select2({allowClear: true, placeholder: "Sélectionner un ou plusieurs services"});


        // Change hash for page-reload
        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            affichageTab();
        })



    });

    $('hr').css('border-top', '0');



	$('.radioLabel label').css('cursor', 'pointer');
	$('label[for="SoustypeSoustypecible"]').parent().hide();
	$('#SoustypeEntrant0').change(function () {
		$('#SoustypeParentId').select2({allowClear: true, placeholder: "Sélectionner un type parent"});
		$('#SoustypeSoustypecible').select2({allowClear: true, placeholder: "Sélectionner un circuit"});
		$('label[for="SoustypeSoustypecible"]').parent().show();
		$('#SoustypeSoustypecible').removeAttr('disabled');

	});
	$('#SoustypeEntrant1').change(function () {
		$('label[for="SoustypeSoustypecible"]').parent().hide();
	});
	$('#SoustypeEntrant2').change(function () {
		$('label[for="SoustypeSoustypecible"]').parent().hide();
	});

	if ($('#SoustypeEntrant0').is(":checked")) {
		$('#SoustypeParentId').select2({allowClear: true, placeholder: "Sélectionner un type parent"});
		$('#SoustypeSoustypecible').select2({allowClear: true, placeholder: "Sélectionner un circuit"});
		$('label[for="SoustypeSoustypecible"]').parent().show();
		$('#SoustypeSoustypecible').removeAttr('disabled');
	}


	$(document).ready(function () {
		$('#MetadonneeMetadonnee').select2();
		$('#SoustypeSoustypecible').select2();

		$('#SoustypeTypeId').select2({allowClear: true, placeholder: "Sélectionner un type"});
		$('#SoustypeCircuitId').select2({allowClear: true, placeholder: "Sélectionner un circuit"});
		$('#SoustypeDelaiUnite').select2({allowClear: true, placeholder: "Sélectionner un délai"});

		$('#SoustypeSoustypecible').select2();
	});

 $('#SoustypeEditForm').css('margin-top', '10px' );

 $('.modal-content legend').css('margin-left', '0px');

// cas à cocher Envoi signature
$('#SoustypeEnvoiSignature').change(function () {
	if ($('#SoustypeEnvoiSignature').attr('checked') == 'checked') {
		$('#selectinsoustypeparapheur').show();
	}
	else {
		$('#selectinsoustypeparapheur').hide();
	}
});
if ($('#SoustypeEnvoiSignature').attr('checked') == 'checked') {
	$('#selectinsoustypeparapheur').show();
}
else {
	$('#selectinsoustypeparapheur').hide();
}
$('#SoustypeSoustypeParapheur').select2({allowClear: true, placeholder: "Sélectionner un sous-type"});

</script>

<?php
    // Gestion des cases à cocher pour les métadonnées saisies comme obligatoires dans le cas de l'uilisation du sous-type en question
if( !empty($this->request->data['Metadonnee']) ){
    foreach( $this->request->data['Metadonnee']['obligatoire'] as $key => $metaValue ) {
        if( $metaValue == true ) {
            ?>
				<script>
					$("input[name='data[Metadonnee][obligatoire][]'][value=<?php echo $key;?>]").prop("checked", true);
				</script>
            <?php
        }
        else {
            ?>
				<script>
					$("input[name='data[Metadonnee][obligatoire][]'][value=<?php echo $key;?>]").prop("checked", false);
				</script>
            <?php
        }
    }
}


?>
