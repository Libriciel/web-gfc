<?php

/**
 *
 * Soustypes/set_rmodels.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
//echo $this->Html->css(array('zones/administration/index'), null, array('inline' => false));
//echo $this->Html->css(array('zones/circuit/index'), null, array('inline' => false));
?>
<script>
    function set_comment_description_height(etapid, heightComment) {
        $("#comment_description_" + etapid).css('height', heightComment);
        $("#test_" + etapid).css('height', heightComment);
    }
</script>
<?php
if (empty($soustype['Soustype']['circuit_id'])) {
    echo $this->Html->tag('div', __d('circuit', 'Circuit.void'), array('class' => 'alert alert-warning'));
} else {
    if(!empty($circuit)){
    $typesEtape = array(
        ETAPESIMPLE => 'Simple',
        ETAPEET => 'Collaborative',
        ETAPEOU => 'Concurrente'
    );
?>
<legend><?php echo $circuit['Circuit']['nom']; ?></legend>
<table class="show-circuit">
    <thead><th class="etapeInfo"></th><th class="arrow"></th><th class="etapeDescription"></th></thead>
<tbody>
<?php
    foreach ($etapes as $etape) {
        $ulCompoContent = array();
        foreach ($etape['Composition'] as $composition) {
            $compositionName = '';
            if( $composition['trigger_id'] == '-1') {
                if( !empty($soustypes) ) {
                    $sousTypeSelectionne = $soustypes[$composition['soustype']];
                    if( !empty( $sousTypeSelectionne )) {
                        $compositionName = 'Etape Parapheur : '.$sousTypeSelectionne;
                    }
                    else {
						$compositionName = "<i style='color:red;'>Etape Parapheur : La valeur du sous-type sélectionné n'existe pas dans le Parapheur cible</i>";
                    }
                }
                else {
                    $compositionName = '<i style="color:red;">Etape Parapheur : Connecteur Parapheur en erreur</i>';
                }
            }
			else if( $composition['trigger_id'] == '-3') {
				if( !empty($documents) ) {
					$listeDocumentSelectionne = $documents[$composition['type_document']];
					if( !empty( $listeDocumentSelectionne )) {
						$compositionName = 'Etape Pastell : '.$listeDocumentSelectionne;
						if( $composition['type_document'] == 'document-a-signer' ) {
							$typeSelectionne = $composition['inforequired_type_document'];
							$compositionName = 'Etape Pastell : '.$listeDocumentSelectionne.' : '.$typeSelectionne;
						}
					}
					else {
						$compositionName = 'Etape Pastell';
					}
				}
				else {
					$compositionName = '<i style="color:red;">Etape Pastell : Connecteur Pastell en erreur</i>';
				}
			}
			else {
                $compositionName = $composition[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS];
            }
            $ulCompoContent[] = $this->Html->tag('div', $compositionName, array('class' => 'desktop_name'));
        }
        $userEnough=array();
        $userEnough=$this->requestAction('Workflows/userEnough/'.$etape['Etape']['id']);
        if($userEnough[0]!=true){
            foreach($userEnough[1] as $falseEtapeId){
                if($falseEtapeId==$etape['Etape']['id']){
                    $userNotEnought =true;
                    $panelClassEtape = ' panel-danger';
                    $descriptionEtapeTitle =  __d('circuit', 'Not Enough User').$this->Html->tag('input','',array('type' => 'hidden','id'=>'noenoughUser','value'=>'noenoughUser'));
                    $arrowDirectionColor = '#A94464';
                }
            }
        }else{
            $userNotEnought =false;
            $panelClassEtape = ' panel-default';
            $descriptionEtapeTitle = '';
            $arrowDirectionColor = '#b2b2b2';
        }
?>
    <tr>
        <td class="etapeInfo">
            <div class="panel <?php echo $panelClassEtape; ?>" id="comment_<?php echo $etape['Etape']['id']; ?>">
                <div class="panel-heading">
                    <h4 class="panel-title"><?php echo $etape['Etape']['ordre']; ?><span><?php echo $typesEtape[$etape['Etape']['type']]; ?></span></h4>
                </div>
                <div class="panel-body">
                    <legend><?php echo $etape['Etape']['nom']; ?></legend>
                    <ul >
                        <?php foreach ($ulCompoContent as $composition): ?>
                        <li><?php echo $composition; ?></li>
                        <?php endforeach;?>
                        <?php if($userNotEnought): ?>
                        <li class="alert alert-danger"><?php echo $descriptionEtapeTitle; ?></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </td>
        <td class="arrow" style="color:<?php echo $arrowDirectionColor; ?>;"><div><div style="background: <?php echo $arrowDirectionColor; ?>;" id="test_<?php echo $etape['Etape']['id']; ?>"></div><i class="fa fa-long-arrow-down" aria-hidden="true" ></i></div></td>
        <td class="etapeDescription">
            <div class="panel panel-default " style="border-color:<?php echo $arrowDirectionColor; ?>;" id="comment_description_<?php echo $etape['Etape']['id']; ?>">
                <div class="panel-body">
                    <legend>Description: </legend>
                    <p><?php echo $etape['Etape']['description']; ?></p>
                </div>
            </div>
        </td>
    </tr>
<script>
    var etapid = "<?php echo $etape['Etape']['id']; ?>";
    $(window).resize(function () {
        var heightComment = $("#comment_" + etapid).height();
        set_comment_description_height(etapid, heightComment);
    });
    var heightComment = $("#comment_" + etapid).height();
    set_comment_description_height(etapid, heightComment);
</script>
<?php
    }
?>
</tbody>
</table>
<?php
    }else{
        echo $this->Html->tag('div', __d('circuit', 'Etape.voidDefinie'), array('class' => 'alert alert-warning'));
    }
}
?>
