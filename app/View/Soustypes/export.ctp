<?php

    $this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow(array('Informations sur les types/Sous-types'));

    $this->Csv->addRow(
            array(
                'Type',
                'Soustype',
                'Circuit associé'
            )
    );

    foreach ($results as $m => $type) {
        $nomType = $type['Type']['name'];
        $rowType[$m] = array_merge(
                array($nomType)
        );
        if (!empty($type['Soustype'])) {
            foreach ($type['Soustype'] as $s => $sstype) {
                $nameSoustypes = $sstype['name'];
				$nameCircuit = $sstype['Circuit']['nom'];

                $rowSoustype[$m] = array_merge(
                        array($nameSoustypes),
                        array($nameCircuit)
                );
                $this->Csv->addRow(
                        array_merge($rowType[$m], $rowSoustype[$m])
                );
            }
        } else {
            $this->Csv->addRow(
                    array_merge(
                            $rowType[$m], array('')
                    )
            );
        }
    }
    Configure::write('debug', 0);
    echo $this->Csv->render("{$this->request->params['controller']}_{$this->request->params['action']}_" . date('Ymd-His') . '.csv');
?>
