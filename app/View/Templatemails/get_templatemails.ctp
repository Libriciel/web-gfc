<?php

/**
 *
 * Scanemail/get_scanemails.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script>
</script>
<?php
if (!empty($templatemails)) {
    $fields = array(
        'name',
        'subject',
        'object'
    );


    $actions = array(
        'edit' => array(
            "url" => Configure::read('BaseUrl') . '/templatemails/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        'delete' => array(
            "url" => Configure::read('BaseUrl') . '/templatemails/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadTemplatemail();"
        )
    );
    $options = array();
    $data = $this->Liste->drawTbody($templatemails, 'Templatemail', $fields, $actions, $options);
?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_templatemails"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-pagination = "true"
           >
    </table>
</div>

<script>
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($templatemails,$options); ?>');
    $('#table_templatemails')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d("templatemail","Templatemail.name"); ?>",
                        class: "name"
                    },
                    {
                        field: "subject",
                        title: "<?php echo __d("templatemail","Templatemail.subject"); ?>",
                        class: "subject"
                    },
                    {
                        field: "object",
                        title: "<?php echo __d("templatemail","Templatemail.object"); ?>",
                        class: "object"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            });
    $(document).ready(function () {
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.49);
    }

</script>
<?php
    echo $this->LIste->drawScript($templatemails, 'Templatemail', $fields, $actions, $options);
} else {

    echo $this->Html->tag('div', __d('templatemail', 'Templatemail.void'), array('class' => 'alert alert-warning'));
}
?>

