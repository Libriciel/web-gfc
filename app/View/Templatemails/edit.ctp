<?php

/**
 *
 * Templatemails/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formTemplatemail = array(
    'name' => 'Templatemail',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Templatemail.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Templatemail.name' => array(
            'labelText' =>__d('templatemail', 'Templatemail.name'),
            'inputType' => 'text',
            'labelPlaceholder' => __d('templatemail', 'Templatemail.placeholderNameAdd'),
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Templatemail.subject' => array(
            'labelText' =>__d('templatemail', 'Templatemail.subject'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text',
                'required'=>true
            )
        ),
        'Templatemail.object' => array(
            'labelText' =>__d('templatemail', 'Templatemail.object'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea',
                'required'=>true
            )
        )
    )
);
    echo $this->Formulaire->createForm($formTemplatemail);
    echo $this->Form->end();
?>

<legend><b>Liste des variables disponibles renseignables dans les parties "Objet et Sujet du mail" du formulaire : </b></legend>
<pre class="templatenotifications">
<b>#PRENOM#</b> = reprend le Prénom de l'utilisateur qui envoie le mail <br/>
<b>#NOM#</b> = reprend le Nom de l'utilisateur qui envoie le mail <br/>
<b>#CIVILITE_CONTACT#</b> = reprend la civilité du contact présent dans le flux <br/>
<b>#NOM_CONTACT#</b> = reprend le Nom du contact présent dans le flux <br/>
<b>#PRENOM_CONTACT#</b> = reprend le Prénom du contact présent dans le flux <br/>
<b>#IDENTIFIANT_FLUX# </b>= reprend lID du flux stocké en BDD <br/>
<b>#REFERENCE_FLUX# </b>= reprend le numéro de référence unique du flux <br/>
<b>#NOM_FLUX# </b>= reprend le nom du flux renseigné par l'agent<br/>
<b>#OBJET_FLUX# </b>= reprend l'objet du flux renseigné par l'agent<br/>
<b>#LIBELLE_CIRCUIT# </b>= reprend l'intitulé du circuit dans lequel le flux est en cours de traitement<br/>
<b>#ADRESSE_A_VISUALISER# </b>= reprend lURL de l'application vers laquelle l'utilisateur sera redirigée sil clique dessus <br/>
<b>#NOM_BUREAU# </b>= reprend le nom du bureau qui est délégué<br />
<b>#DEBUT_DELEGATION# </b>= reprend la date de début de délégation en cas de planification<br />
<b>#FIN_DELEGATION# </b>= reprend la date de fin de délégation en cas de planification <br />
<b>#MOTIF_REFUS# </b>= reprend le motif du refus<br />
</pre>
