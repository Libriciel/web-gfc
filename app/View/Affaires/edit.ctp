<?php

/**
 *
 * Affaires/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formAffaire = array(
    'name' => 'Affaire',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-7',
    'input' => array(
        'id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'name' => array(
            'labelText' =>__d('affaire', 'Affaire.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'comment' => array(
            'labelText' =>__d('affaire', 'Affaire.commentaire'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea'
            )
        ),
        'dossier_id' => array(
            'labelText' =>__d('affaire', 'Affaire.dossier'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $dossiers,
                'empty' => true
            )
        )
    )
);

echo $this->Formulaire->createForm($formAffaire);
echo $this->Form->end();
?>
<script type="text/javascript">
    $('#AffaireDossierId').select2();
</script>
