<?php

/**
 *
 * Contractants/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$formContractant = array(
    'name' => 'Contractant',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Contractant.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Contractant.name' => array(
            'labelText' =>__d('contractant', 'Contractant.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Contractant.description' => array(
            'labelText' =>__d('contractant', 'Contractant.description'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'Contractant.active' => array(
            'labelText' =>__d('contractant', 'Contractant.active'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['Contractant']['active']
            )
        )
    )
);
echo $this->Formulaire->createForm($formContractant);
echo $this->Form->end();
?>
