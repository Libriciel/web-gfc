<?php

/**
 *
 * Gabaritsdocuments/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php if( !empty($gabaritsdocuments) ) {
    $fields = array(
        'name'
    );
    $actions = array(
        "download" => array(
            "url" => Configure::read('BaseUrl') . '/gabaritsdocuments/download',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        "edit" => array(
//            "url" => Configure::read('BaseUrl') . '/gabaritsdocuments/edit',
            "url" => "",
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/gabaritsdocuments/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadGabaritdocument();"
        )
    );
    $options = array();
    $data = $this->Liste->drawTbody($gabaritsdocuments, 'Gabaritdocument', $fields, $actions, $options);

?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_gabaritdocument"
           data-toggle="table"
           data-search="true"
           data-sortable ="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-pagination = "true"
           >
    </table>
</div>


<script type="text/javascript">
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($gabaritsdocuments,$options); ?>');
    $('#table_gabaritdocument')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d('gabaritdocument', 'Gabaritdocument.name'); ?>",
                        class: "name",
                        sortable: true
                    },
                    {
                        field: "download",
                        title: "Télécharger",
                        class: "actions thDownload",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            });

    $(document).ready(function () {
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }



    $('.thEdit').click(function () {
        var docId = $(this).children().attr('itemId');
        var url = "/gabaritsdocuments/edit/" + docId;
        gui.formMessage({
            updateElement: $('body'),
            loader: true,
            width: '550px',
            loaderMessage: gui.loaderMessage,
            title: "Modèle de documents",
            url: url,
            buttons: {
                '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                },
                '<i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer': function () {
                    var form = $('#GabaritdocumentEditForm');
                    form.submit();
                    loadGabaritdocument();
                    location.reload();
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                }
            }
        });

    });
</script>
<?php
    echo $this->Liste->drawScript($gabaritsdocuments, 'Gabaritdocument', $fields, $actions, $options);
    echo $this->Js->writeBuffer();
}else{
    echo $this->Html->tag('div', __d('gabaritdocument', 'Gabaritdocument.void'), array('class' => 'alert alert-warning'));
}
?>

