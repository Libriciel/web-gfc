

<?php

/**
 *
 * Gabaritsdocuments/set_gabaritdocuments.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    $formGabaritdocument = array(
        'name' => 'Gabaritdocument',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-5',
		'form_url' => array( 'controller' => 'gabaritsdocuments', 'action' => 'edit', $gabaritdocument['Gabaritdocument']['id'] ),
        'form_target' => 'modelUploadFrame',
        'form_type' => 'file',
        'enctype' =>'multipart/form-data',
        'input' => array(
            'Gabaritdocument.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value'=> $gabaritdocument['Gabaritdocument']['id']
                )
            ),
            'Gabaritdocument.name' => array(
                'labelText' =>__d('gabaritdocument', 'Gabaritdocument.name'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'value'=>$gabaritdocument['Gabaritdocument']['name']
                )
            ),
            'Gabaritdocument.file' => array(
                'labelText' =>__d('gabaritdocument', 'Gabaritdocument.file'),
                'inputType' => 'file',
                'items'=>array(
                    'type' => 'file',
                    'name' => 'myfile',
                    'class' => 'fileField'
                )
            ),
            'Gabaritdocument.format' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type' => 'hidden',
                    'value' => 'ott'
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formGabaritdocument);
    echo $this->Form->end();
?>
<script type="text/javascript">
    $("#modelUploadFrame").remove();
    $('body').append('<iframe name="modelUploadFrame" id="modelUploadFrame" src="#" style="width:0;height:0;border:none;"></iframe>');
<?php if (isset($uploadComplete) && $uploadComplete == "ok") { ?>
    if (window.top.window.updateModelTab) {
        window.top.window.updateModelTab();
    }
<?php } ?>
<?php if (!empty($gabaritdocument['Gabaritdocument']['ext']) ) : ?>
    $('#GabaritdocumentFile').parent().append("<?php echo 'Nom du fichier actuellement présent :<br /> '.$gabaritdocument['Gabaritdocument']['name'].'.'.$gabaritdocument['Gabaritdocument']['ext'];?>");
<?php endif;?>
</script>
