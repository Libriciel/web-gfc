<?php

/**
 *
 * Elements/dktpEditBttn.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 *
 * @param $desktopId integer
 * @param $userId integer
 * @param deletable boolean
 *
 * @param $rightsImg string
 * @param $delegImg string
 * @param $deleteImg string
 *
 */
$spanContent = '';

if ($deletable && !$delegated ) {
    $spanContent = $this->Html->tag('span', $this->Html->image(!empty($delegImg) ? '' : '/img/go-home.png', array('url' => '/desktops/setMain/' . $desktopId . '/' . $userId, 'alt' => 'principal', 'title' => __d('desktop', 'Desktop.setMain'))), array('class' => 'desktopSetMain btn btn-default '));
}

if (!isset($editable) || $editable ) {
    $spanContent .= $this->Html->tag('span', $this->Html->image(!empty($rigthsImg) ? $rightsImg : '/img/tool.png', array('url' => '/desktops/edit/' . $desktopId, 'alt' => 'édition', 'title' => __d('desktop', 'Desktop.edit'))), array('class' => 'desktopEdit btn btn-default '));
}

$spanContent .= $this->Html->tag('span', $this->Html->image(!empty($rigthsImg) ? $rightsImg : '/img/setRights.png', array('url' => '/desktops/rights/' . $desktopId, 'alt' => 'droits', 'title' => __d('desktop', 'Desktop.setRights'))), array('class' => 'setRights btn btn-default '));
$spanContent .= $this->Html->tag('span', $this->Html->image(!empty($delegImg) ? $delegImg : '/img/deleg.png', array('url' => '/desktops/setDeleg/' . $desktopId, 'alt' => 'délégations', 'title' => __d('desktop', 'Desktop.setDeleg'))), array('class' => 'setDeleg btn btn-default '));
if ($deletable) {
	$spanContent .= $this->Html->tag('span', $this->Html->image(!empty($deleteImg) ? $deleteImg : '/img/trash.png', array('url' => '/desktops/delete/' . $desktopId . '/' . $userId, 'alt' => 'suppression', 'title' => __d('desktop', 'Desktop.delete'))), array('class' => 'desktopDelete btn btn-default '));
}
echo $this->Html->tag('span', $spanContent, array('class' => 'desktopCtrls ','role'=>'group'));
?>
