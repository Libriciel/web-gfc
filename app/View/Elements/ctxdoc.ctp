<?php

/**
 *
 * Elements/ctxdoc.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$typeMime = array(
    'application/vnd.ms-office' => array(
        'ico' => '/img/document.png',
        'name' => 'Document MS Office'
    ),
    'application/vnd.oasis.opendocument.text' => array(
        'ico' => '/img/document.png',
        'name' => 'Document ODT'
    ),
    'VND.OASIS.OPENDOCUMENT.TEXT' => array(
        'ico' => '/img/document.png',
        'name' => 'Document ODT'
    ),
    'OCTET-STREAM' => array(
        'ico' => '/img/document.png',
        'name' => 'Document ODT'
    ),
    'application/rtf' => array(
        'ico' => '/img/document.png',
        'name' => 'Document RTF'
    ),
    'application/msword' => array(
        'ico' => '/img/document.png',
        'name' => 'Document MS Word'
    ),
    'text/plain' => array(
        'ico' => '/img/document.png',
        'name' => 'Document texte'
    ),
    'application/pdf' => array(
        'ico' => '/img/pdf.png',
        'name' => 'Document PDF'
    ),
    'PDF' => array(
        'ico' => '/img/pdf.png',
        'name' => 'Document PDF'
    ),
    'image/gif' => array(
        'ico' => '/img/image.png',
        'name' => 'Image GIF'
    ),
    'image/png' => array(
        'ico' => '/img/image.png',
        'name' => 'Image PNG'
    ),
    'image/jpeg' => array(
        'ico' => '/img/image.png',
        'name' => 'Image JPEG'
    ),
    'audio/x-wav' => array(
        'ico' => '/img/audio.png',
        'name' => 'Fichier audio WAV'
    ),
    'audio/wav' => array(
        'ico' => '/img/audio.png',
        'name' => 'Fichier audio WAV'
    ),
    'audio/mp3' => array(
        'ico' => '/img/audio.png',
        'name' => 'Fichier audio MP3'
    ),
    'audio/ogg' => array(
        'ico' => '/img/audio.png',
        'name' => 'Fichier audio OGG'
    ),
    'audio/mpeg' => array(
        'ico' => '/img/audio.png',
        'name' => 'Fichier audio MPEG/MP3'
    ),
    'video/mp4' => array(
        'ico' => '/img/video.png',
        'name' => 'Fichier video MPEG 4'
    ),
    'application/ogg' => array(
        'ico' => '/img/video.png',
        'name' => 'Fichier video OGG'
    ),
    'video/ogg' => array(
        'ico' => '/img/video.png',
        'name' => 'Fichier video OGG'
    ),
    'video/mpg' => array(
        'ico' => '/img/video.png',
        'name' => 'Fichier video MPEG'
    ),
    'video/mpeg' => array(
        'ico' => '/img/video.png',
        'name' => 'Fichier video MPEG'
    ),
    'unknown' => array(
        'ico' => '/img/unknown-mime.png',
        'name' => 'Type de fichier inconnu'
    ),
    'application/vnd.openxmlformats-officedocument.presentationml.presentation' => array(
        'ico' => '/img/ooo_writer_22.png',
        'name' => 'Fichier PPTX'
    ),
    'application/vnd.oasis.opendocument.spreadsheet' => array(
        'ico' => '/img/ooo_calc.png',
        'name' => 'Fichier Calc/Excel'
    ),
    'application/force-download' => array(
        'ico' => '/img/document.png',
        'name' => 'Document ODT'
    ),
);

$aricons = array(
    'audio' => '/img/audio.png',
    'video' => '/img/video.png',
    'ott' => '/img/document.png',
    'email' => '/img/email.png',
    'sms' => '/img/phone.png'
);

if (!isset($adminMode)) {
    $adminMode = false;
}
//var_dump( $ctxdoc) ;
if (!isset($selectableForMainDoc)) {
    $selectableForMainDoc = false;
}

if( !isset( $rightToDelete ) ) {
    $rightToDelete = false;
}

if (count($ctxdoc) > 1) {
?>
<div model="<?php echo $gfcDocType; ?>" controllerName="<?php echo $gfcDocType . "s"; ?>" itemId="<?php echo $ctxdoc['id']; ?>" class="<?php echo!empty($cssClass) ? $cssClass : ''; ?>" style="line-height:35px;" id="<?php echo $gfcDocType; ?>_<?php echo $ctxdoc['id']; ?>">
    <span class="ctxdoc_name">
        <?php
        if ($gfcDocType == "Armodel") {
            echo $this->Html->image($aricons[$ctxdoc['format']], array('title' => !empty($ctxdoc['mime']) ? $typeMime[$ctxdoc['mime']]['name'] : '')) . ' ';
        } else {
            if (!empty($ctxdoc['mime'])) {
                if (!in_array($ctxdoc['mime'], array_keys($typeMime))) {
                    echo $this->Html->image($typeMime['unknown']['ico'], array('title' => $typeMime['unknown']['name'])) . ' ';
                } else {
                    echo $this->Html->image($typeMime[$ctxdoc['mime']]['ico'], array('title' => $typeMime[$ctxdoc['mime']]['name'])) . ' ';
                }
            } else {
                echo $this->Html->image('/img/document.png') . ' ';
//                echo '<i class="fa fa-file-pdf-o table" title="Documents"></i>';
            }
        }
//        echo $this->Html->tag('span', $ctxdoc['name'], array('class' => 'filename')) . (!empty($ctxdoc['size']) ? ' (' . $this->Html2->formatsize($ctxdoc['size']) . ')' : '');
        if( Configure::read('debug') == 2 ) {
            echo $this->Html->tag('span', mb_strimwidth($ctxdoc['name'], 0, 35, "..."), array('class' => 'filename', 'title' => $ctxdoc['name']) ) . (!empty($ctxdoc['size']) ? ' (' . $this->Html2->formatsize($ctxdoc['size']) . ')' : '');
        }
        else {
            echo $this->Html->tag('span', mb_strimwidth($ctxdoc['name'], 0, 35, "..."), array('class' => 'filename', 'title' => $ctxdoc['name']));// . (!empty($ctxdoc['size']) ? ' (' . $this->Html2->formatsize($ctxdoc['size']) . ')' : '');
//            echo $this->Html->tag('span', $ctxdoc['name'], array('class' => 'filename', 'title' => $ctxdoc['name']));// . (!empty($ctxdoc['size']) ? ' (' . $this->Html2->formatsize($ctxdoc['size']) . ')' : '');
        }

        ?>
    </span>
    <span class="ctxdoc_actions " role="group" style="float:right;">
        <?php
//$this->log($ctxdoc);
		if($ctxdoc['size'] >= 0 ) {
			if( isset( $ctxdoc['path']) && !file_exists($ctxdoc['path'])) {
				$img = $this->Html->tag('i','', array( 'class' => 'ctxdocBttnDelete fa fa-trash fa-fw','aria-hidden'=>true));?>
				<b class="alert-error">Le fichier n'a pas été trouvé</b>
				<?php echo $this->Html->link($img, '#', array('escape' => false,'class'=>' btn btn-default ', 'title' => 'Supprimer le fichier'));?>
				<?php
			}
			else {

				// Définir en doc principal
				if (
					$selectableForMainDoc
					&& $right_setAsDefault
					&& ($this->action != 'getParent')
					&& file_exists($ctxdoc['path'])
					&& in_array($ctxdoc['mime'], array('application/pdf', 'application/vnd.oasis.opendocument.text', 'application/msword'))
					) {
					$img = $this->Html->tag('i', '', array('class' => 'fa fa-file fa-fw ctxdocBttnSetMainDoc', 'aria-hidden' => true));
					echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Définir comme document principal'));
				}

				// Convertir en PDF
				if (
					isset($ctxdoc['path'])
					&& file_exists($ctxdoc['path'])
					&& $gfcDocType == 'Document'
					&& $ctxdoc['mime'] != 'application/pdf'
					&& $ctxdoc['ext'] != 'zip'
					&& in_array($ctxdoc['mime'], array('application/pdf', 'application/vnd.oasis.opendocument.text', 'application/msword'))
				) {
					$img = $this->Html->tag('i', '', array('class' => 'fa fa-file-pdf-o ctxdocBttnConversionPDF', 'aria-hidden' => true));
					echo $this->Html->link($img, '/Documents/convertToPdf/' . $ctxdoc['id'], array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Convertir en PDF'));
				} else if (
					$gfcDocType == 'Ar'
					&& in_array($ctxdoc['mime'], array('application/pdf', 'application/vnd.oasis.opendocument.text', 'application/msword'))
				) {
					// Convertir en PDF
					$img = $this->Html->tag('i', '', array('class' => 'fa fa-file-pdf-o fa-fw ctxdocBttnConversionPDF', 'aria-hidden' => true));
					echo $this->Html->link($img, '/Ars/convertToPdf/' . $ctxdoc['id'], array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Convertir en PDF'));
				}

				// Envoyer à la signature en + du doc principal
				if (isset($ctxdoc['path']) && file_exists($ctxdoc['path']) && isset($hasParapheurActif) && $hasParapheurActif && $hasEtapeParapheur) {
					if ($selectableForMainDoc && empty($ctxdoc['signature'])) {
						if (empty($ctxdoc['parapheur_bordereau']) && strpos($ctxdoc['name'], 'Bordereau') === false) {
							if (!$ctxdoc['asigner']) {
								$img = $this->Html->tag('i', '', array('class' => 'fa fa-certificate ctxdocBttnASigner', 'aria-hidden' => true));
								echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Ajouter à la signature'));
							} else {
								$img = $this->Html->tag('i', '', array('class' => 'fa fa-minus ctxdocBttnDesigner', 'aria-hidden' => true));
								echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Enlever pour la signature'));
							}
						}
					}
				}

				// Télécharger le fichier
				if (!empty($ctxdoc['size']) && $ctxdoc['size'] > 0) {
					$img = $this->Html->tag('i', '', array('class' => 'fa fa-download fa-fw ctxdocBttnDownload', 'aria-hidden' => true));
					if ($gfcDocType == "Gabaritdocument") {
						echo $this->Html->link($img, '/gabaritsdocuments/download/' . $ctxdoc['id'], array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Télécharger le fichier'));
					} else {
						if( !empty($ctxdoc['id']) ) {
							echo $this->Html->link($img, '/' . $gfcDocType . 's/download/' . $ctxdoc['id'], array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Télécharger le fichier'));
						}
						else {
							echo $this->Html->link($img, '/courriers/download_relement/' . $ctxdoc['name'] . '/' . $ctxdoc['courrier_id'], array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Télécharger le fichier'));
						}
					}
				}

				// Voir le document
				if (!$adminMode /*&& ($this->action != 'getParent')*/) {
					if(in_array($ctxdoc['mime'], array('application/pdf', 'application/vnd.oasis.opendocument.text','image/jpeg', 'image/gif', 'image/png', 'application/msword'))){

						if ($gfcDocType == 'Document') {
							if (
								isset($ctxdoc['path'])
								&& file_exists($ctxdoc['path'])
								) {
								$fileName = substr($ctxdoc['name'], 0, 7) . '...';
								$img = $this->Html->tag('i', '', array('class' => 'fa fa-eye fa-fw ctxdocBttnPreview', 'aria-hidden' => true, 'fileName' => $fileName));
								echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Visualiser le fichier'));
							} else {
								$img = $this->Html->tag('i', '', array('class' => 'fa fa-eye fa-fw ctxdocBttnPreview', 'aria-hidden' => true));
								echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Visualiser le fichier'));
							}
						}
						if ($gfcDocType == 'Armodel') {
							$img = $this->Html->tag('i', '', array('class' => 'fa fa-eye fa-fw ctxdocBttnPreviewArmodel', 'aria-hidden' => true));
							echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Visualiser le fichier'));
						}
						if ($gfcDocType == 'Ar') {
							$img = $this->Html->tag('i', '', array('class' => 'fa fa-eye fa-fw ctxdocBttnPreviewAr', 'aria-hidden' => true));
							echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Visualiser le fichier'));
						}
						if ($gfcDocType == 'Rmodel') {
							$img = $this->Html->tag('i', '', array('class' => 'fa fa-eye fa-fw ctxdocBttnPreviewRmodel', 'aria-hidden' => true));
							echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Visualiser le fichier'));
						}
					}
				}

				// Editer via webdav
				if ($gfcDocType == 'Relement' || $gfcDocType == 'Document' && in_array($ctxdoc['mime'], array('application/vnd.oasis.opendocument.text')) && $ctxdoc['isgenerated'] == true) {
					if (empty($ctxdoc['id']) && !empty($ctxdoc['name'])) {
						$img = $this->Html->tag('i', '', array('class' => 'fa fa-plus-circle fa-fw ctxdocBttnWebdavAdd', 'itemName' => $ctxdoc['name']));
						echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default btn-resize', 'title' => 'Utiliser ce fichier (webdav)'));
					} else {
						$img = $this->Html->tag('i', '', array('class' => 'fa fa-pencil fa-fw ctxdocBttnWebdav', 'aria-hidden' => true, 'itemName' => $ctxdoc['name']));
						echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default btn-resize', 'title' => 'Editer le fichier (webdav)'));
					}
				}
				if (
						($adminMode && $gfcDocType == "Rmodel") ||
						($adminMode && $gfcDocType == "Armodel") ||
						(!$adminMode && $gfcDocType != "Armodel" && $gfcDocType != "Rmodel")
				) {

					if ($gfcDocType == 'Rmodel' || $gfcDocType == 'Armodel') {
						// Editer via webdav
						$img = $this->Html->tag('i', '', array('class' => 'fa fa-pencil ctxdocBttnEditRmodel', 'aria-hidden' => true));
						echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Editer le document'));
					}

					if ($gfcDocType == "Ar" && in_array($ctxdoc['mime'], array('application/vnd.oasis.opendocument.text'))) {
						// Editer via webdav
						$img = $this->Html->tag('i', '', array('class' => 'fa fa-pencil fa-fw ctxdocBttnWebdav', 'aria-hidden' => true, 'itemName' => $ctxdoc['name']));
						echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Editer le fichier (webdav)'));
					}
					$isAR = strpos($ctxdoc['name'], 'AR');
					$isAROdt = @$ctxdoc['ext'];
					// Envoyer par mail
					// Ajout de l'envoi individuel d'un document par mail
					if ((isset($sendCourrier) && $sendCourrier) || !isset($sendCourrier)) {
						if (!empty($right_sendmail) || $adminMode) {
							if ($gfcDocType != 'Relement' && ($gfcDocType != 'Ar')) {
								$img = $this->Html->tag('i', '', array('class' => 'ctxdocBttnSendmail fa fa-paper-plane fa-fw', 'aria-hidden' => true));
								echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Envoyer le document par mail',));
							} else if ($gfcDocType == 'Ar' && $isAROdt == 'pdf') {
								$img = $this->Html->tag('i', '', array('class' => 'ctxdocBttnSendmail fa fa-paper-plane fa-fw', 'aria-hidden' => true));
								echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Envoyer le document par mail',));
							}
						}
					}

					// Envoyer à la GED
					// Ajout de l'envoi individuel d'un document par mail
					if ((isset($sendGED) && $sendGED) || !isset($sendGED)) {
						if (!empty($right_sendged) || $adminMode) {
							//                $img = $this->Html->image('/img/folder_sent_mail_16.png', array('alt' => 'Verser en GED', 'title' => 'Versement du document en GED', 'class' => 'ctxdocBttnSendDocged'));
							$img = $this->Html->tag('i', '', array('class' => 'ctxdocBttnSendDocged fa fa-share-square-o', 'aria-hidden' => true));
							echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Versement du document en GED'));
						}
					}
					// Supprimer le fichier
					if (!empty($right_delete) || $adminMode || $isAdmin || $rightToDelete) {
						$img = $this->Html->tag('i', '', array('class' => 'ctxdocBttnDelete fa fa-trash fa-fw', 'aria-hidden' => true));
						echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Supprimer le fichier'));
					}
				}

				if (($adminMode && $gfcDocType == "Rmodelgabarit")) {
					// Editer le rmodelgabarit
					$img = $this->Html->tag('i', '', array('class' => 'fa fa-pencil ctxdocBttnEditRmodelgabarit', 'aria-hidden' => true));
					echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Editer le document'));
					// Supprimer le gabarit
					if (!empty($right_delete) || $adminMode || $rightToDelete) {
						$img = $this->Html->tag('i', '', array('class' => 'ctxdocBttnDeleteGabarit fa fa-trash fa-fw', 'aria-hidden' => true));
						echo $this->Html->link($img, '#', array('escape' => false, 'class' => ' btn btn-default ', 'title' => 'Supprimer le fichier'));
					}

				}
				if ($adminMode && $gfcDocType == "Gabaritdocument") {
					// Supprimer le gabarit
					if (!empty($right_delete) || $adminMode || $rightToDelete) {
						echo $this->Html->tag('i', '', array('class' => 'ctxdocBttnDeleteGabaritdocument fa fa-trash fa-fw', 'aria-hidden' => true, 'title' => 'Supprimer le fichier'));
					}
				}
			}
		}
		else {
			$img = $this->Html->tag('i','', array( 'class' => 'ctxdocBttnDelete fa fa-trash fa-fw','aria-hidden'=>true));
			?>
			<b class="alert-error">Le fichier n'a pas été trouvé</b>
			<?php echo $this->Html->link($img, '#', array('escape' => false,'class'=>' btn btn-default ', 'title' => 'Supprimer le fichier'));?>
			<?php

		}
?>
    </span>


</div>
<?php
} else {
    echo substr( $ctxdoc['name'], 0, 7 ).'...';
}
?>

<?php if( isset( $ctxdoc['courrier_id'] ) && !empty($ctxdoc['courrier_id']) ):?>
<script type="text/javascript">
    $('.document_list .ctxdocBttnWebdav').unbind('click').click(function () {
        window.location.href = "<?php echo WEBDAV_URL; ?>" + "/" + "<?php echo $conn; ?>" + "/" + "<?php echo $ctxdoc['courrier_id']; ?>" + "/" + $(this).attr('itemname');
    });
    $('.main_document .ctxdocBttnWebdav').unbind('click').click(function () {
        window.location.href = "<?php echo WEBDAV_URL; ?>" + "/" + "<?php echo $conn; ?>" + "/" + "<?php echo $ctxdoc['courrier_id']; ?>" + "/" + $(this).attr('itemname');
    });
    $('.panel-body .ctxdocBttnWebdav').unbind('click').click(function () {
        window.location.href = "<?php echo WEBDAV_URL; ?>" + "/" + "<?php echo $conn; ?>" + "/" + "<?php echo $ctxdoc['courrier_id']; ?>" + "/" + $(this).attr('itemname');
    });
    $('#arElement .ctxdocBttnWebdav').unbind('click').click(function () {
        window.location.href = "<?php echo WEBDAV_URL; ?>" + "/" + "<?php echo $conn; ?>" + "/" + "<?php echo $ctxdoc['courrier_id']; ?>" + "/" + $(this).attr('itemname');
    });
</script>
<?php endif;?>
