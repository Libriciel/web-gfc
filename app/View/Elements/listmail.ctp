<div>
    <span>
        <?php
            foreach($mailsSent as $mail) {
				$documentName = isset( $mail['Document']['name'] ) ? $mail['Document']['name'] : '<i>Aucun document associé</i>';
				if( $mail['Desktop']['id'] == '-3' || empty($mail['Desktop']['id']) ) {
					echo '<dl>'
							. '<dt>Mail sécurisé adressé à : </dt><dd>'.$mail['Sendmail']['email']. '</dd>'
							. '<dt>Envoyé le : </dt><dd>'.  $this->Html2->ukToFrenchDateWithHourAndSlashes( $mail['Sendmail']['created'] ). '</dd>'
							. '<dt>Informations : </dt><dd>'.  $mail['Sendmail']['message']. '</dd>'
							. '</dl>';
				}
				else {
					if(Configure::read('Sendmail.DisplayContent')){
						echo '<dl>'
							. '<dt>Mail envoyé par : </dt><dd>' . $mail['Desktop']['name'] . '</dd>'
							. '<dt>Adressé à : </dt><dd>' . $mail['Sendmail']['email'] . '</dd>'
							. '<dt>En copie pour : </dt><dd>' . $mail['Sendmail']['ccmail'] . '</dd>'
							. '<dt>Concernant le document : </dt><dd>' . $documentName . '</dd>'
							. '<dt>Envoyé le : </dt><dd>' . $this->Html2->ukToFrenchDateWithHourAndSlashes($mail['Sendmail']['created']) . '</dd>'
							. '<dt>Contenu du mail : </dt><dd>'.  $mail['Sendmail']['message']. '</dd>'
							. '</dl>';
					}
					else {
						echo '<dl>'
							. '<dt>Mail envoyé par : </dt><dd>' . $mail['Desktop']['name'] . '</dd>'
							. '<dt>Adressé à : </dt><dd>' . $mail['Sendmail']['email'] . '</dd>'
							. '<dt>En copie pour : </dt><dd>' . $mail['Sendmail']['ccmail'] . '</dd>'
							. '<dt>Concernant le document : </dt><dd>' . $documentName . '</dd>'
							. '<dt>Envoyé le : </dt><dd>' . $this->Html2->ukToFrenchDateWithHourAndSlashes($mail['Sendmail']['created']) . '</dd>'
							. '</dl>';
					}
				}

            }
        ?>
    </span>
</div>
