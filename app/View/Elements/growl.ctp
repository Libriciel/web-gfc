<?php
/**
 *
 * Elements/growl.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$pos = @strripos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6');
if ($pos === false) {
	?>

	<script type="text/javascript">
	<?php if (isset($type) && $type == 'important') : ?>
			layer.msg("<?php echo $message; ?>", {});
	<?php elseif (isset($type) && $type == 'erreur') : ?>
			layer.msg("<?php echo $message; ?>", {});
	<?php else : ?>
			layer.msg("<?php echo $message; ?>", {});
	<?php endif; ?>
	</script>
	<?php
}
?>


