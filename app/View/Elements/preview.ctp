<?php

/**
 *
 * Elements/preview.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
echo '<div id="viewerContainer" style="overflow: auto;">
                        <div id="viewer" class="pdfViewer" >
                    </div> ';
if ($previewType == 'audio') {
	$audioContent = $this->Html->tag('source', '', array('src' => $filename, 'type' => $mime));
	$audioContent .= __d('default', 'Your browser does not support the audio tag.');
	$audio = $this->Html->tag('audio', $audioContent, array('controls' => 'controls'));
	echo $this->Html->tag('div', $audio, array('style' => 'text-align:center;padding:150px 0;'));
} else if ($previewType == 'video') {
	$videoContent = $this->Html->tag('source', '', array('src' => $filename, 'type' => $mime));
	$videoContent .= __d('default', 'Your browser does not support the video tag.');
	echo $this->Html->tag('video', $videoContent, array('controls' => 'controls', 'width' => $width, 'height' => $height));
} else if ($previewType == 'image') {
	$img = $this->Html->image($filename, array('style' => 'max-width:' . $width .';max-height:' . $height . ';vertical-align:middle;'));
	echo $this->Html->tag('div', $img, array('style' => 'text-align:center;padding:10px 0;'));
}
?>
