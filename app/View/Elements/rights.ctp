<?php

$yes = '<img src="/DataAcl/img/test-pass-icon.png" alt="yes" />';
$no = '<img src="/DataAcl/img/test-fail-icon.png" alt="no"/>';
if (!empty($edit)) {
	echo $this->Form->create('Right', array('url' => $url));
	echo $this->Form->input('Right.id', array('type' => 'hidden', 'value' => !empty($rights['Right']['id']) ? $rights['Right']['id'] : ''));
	echo $this->Form->input('Right.aro_id', array('type' => 'hidden', 'value' => $rights['Right']['aro_id']));
}
?>

<fieldset class="" id="tabs">
    <legend  ><?php echo __d('right', 'tabs'); ?></legend>
    <table class="acl_table">
        <tr>
            <th><?php echo __d('right', 'tab'); ?></th>
            <th class="action"><?php echo __d('right', 'right.create'); ?></th>
            <th class="action"><?php echo __d('right', 'right.get'); ?></th>
            <th class="action"><?php echo __d('right', 'right.set'); ?></th>
        </tr>
        <tr>
            <td><?php echo __d('courrier', 'Informations'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.create_infos', array('type' => 'checkbox', 'class' => 'infos create', 'label' => false, 'checked' => $rights['Right']['create_infos'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['create_infos'] ? $yes : $no;
                }
                ?>
            </td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.get_infos', array('type' => 'checkbox', 'class' => 'infos get', 'label' => false, 'checked' => $rights['Right']['get_infos'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['get_infos'] ? $yes : $no;
                }
                ?>
            </td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.set_infos', array('type' => 'checkbox', 'class' => 'infos set', 'label' => false, 'checked' => $rights['Right']['set_infos'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['set_infos'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('courrier', 'Metadonnees'); ?></td>
            <td class="action"><span class="ui-state-inactive"> - </span></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.get_meta', array('type' => 'checkbox', 'class' => 'meta get', 'label' => false, 'checked' => $rights['Right']['get_meta'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['get_meta'] ? $yes : $no;
                }
                ?>
            </td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.set_meta', array('type' => 'checkbox', 'class' => 'meta set', 'label' => false, 'checked' => $rights['Right']['set_meta'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['set_meta'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('courrier', 'Taches'); ?></td>
            <td class="action"><span class="ui-state-inactive"> - </span></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.get_taches', array('type' => 'checkbox', 'class' => 'taches get', 'label' => false, 'checked' => $rights['Right']['get_taches'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['get_taches'] ? $yes : $no;
                }
                ?>
            </td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.set_taches', array('type' => 'checkbox', 'class' => 'taches set', 'label' => false, 'checked' => $rights['Right']['set_taches'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['set_taches'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('courrier', 'Affaire'); ?></td>
            <td class="action"><span class="ui-state-inactive"> - </span></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.get_affaire', array('type' => 'checkbox', 'class' => 'affaire get', 'label' => false, 'checked' => $rights['Right']['get_affaire'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['get_affaire'] ? $yes : $no;
                }
                ?>
            </td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.set_affaire', array('type' => 'checkbox', 'class' => 'affaire set', 'label' => false, 'checked' => $rights['Right']['set_affaire'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['set_affaire'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
    </table>
</fieldset>


<fieldset class="" id="rightsSets">
    <legend  ><?php echo __d('right', 'rightSets'); ?></legend>
    <table class="acl_table">
        <tr>
            <th><?php echo __d('right', 'rightSet'); ?></th>
            <th class="action"><?php echo __d('right', 'right.auth'); ?></th>
        </tr>
        <tr>
            <td <?php echo !empty($edit) ? 'class="ui-state-inactive"' : ''; ?>><?php echo __d('right', 'rightSet.administration'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo "<span class='ui-state-inactive'> - </span>";
                        //echo $this->Form->input('Right.administration', array('type' => 'checkbox', 'class' => 'administration', 'label' => false, 'disabled'=> 'disabled', 'checked' => $rights['Right']['administration'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['administration'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td <?php echo !empty($edit) ? 'class="ui-state-inactive"' : ''; ?>><?php echo __d('right', 'rightSet.aiguillage'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo "<span class='ui-state-inactive'> - </span>";
                        //echo $this->Form->input('Right.aiguillage', array('type' => 'checkbox', 'class' => 'aiguillage', 'label' => false, 'disabled' => 'disabled', 'checked' => $rights['Right']['aiguillage'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['aiguillage'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <?php
                if(!empty($edit) && !isset($rolePers)){
                    echo '<td class ="ui-state-inactive">'.__d('right', 'rightSet.creation').'</td>
                         <td class="action"><span class="ui-state-inactive"> - </span></td>';
                }else{
                    echo '<td >'.__d('right', 'rightSet.creation').'</td><td class="action">';
                    if (!empty($edit)) {

                            echo $this->Form->input('Right.creation', array('type' => 'checkbox', 'class' => 'creation', 'label' => false, 'checked' => $rights['Right']['creation'] ? 'checked' : ''));

                    } else {
                            echo $rights['Right']['creation'] ? $yes : $no;
                    }
                    echo '</td>';
                }
            ?>
        </tr>
        <tr>
            <td><?php echo __d('right', 'rightSet.validation'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.validation', array('type' => 'checkbox', 'class' => 'validation', 'label' => false, 'checked' => $rights['Right']['validation'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['validation'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'rightSet.edition'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.edition', array('type' => 'checkbox', 'class' => 'edition', 'label' => false, 'checked' => $rights['Right']['edition'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['edition'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'rightSet.documentation'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.documentation', array('type' => 'checkbox', 'class' => 'documentation', 'label' => false, 'checked' => $rights['Right']['documentation'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['documentation'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
    </table>
</fieldset>

<fieldset class="" id="ctx">
    <legend  ><?php echo __d('right', 'ctx'); ?></legend>
    <table class="acl_table">
        <tr>
            <th><?php echo __d('right', 'right.action'); ?></th>
            <th class="action"><?php echo __d('right', 'right.auth'); ?></th>
        </tr>
        <tr>
            <td class="ctx_subtitle" colspan="2">
                <?php echo __d('right', 'ctx.document'); ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'ctx.document_upload'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.document_upload', array('type' => 'checkbox', 'class' => 'document_upload', 'label' => false, 'checked' => $rights['Right']['document_upload'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['document_upload'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'ctx.document_suppr'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.document_suppr', array('type' => 'checkbox', 'class' => 'document_suppr', 'label' => false, 'checked' => $rights['Right']['document_suppr'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['document_suppr'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'ctx.document_setasdefault'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.document_setasdefault', array('type' => 'checkbox', 'class' => 'document_setasdefault', 'label' => false, 'checked' => $rights['Right']['document_setasdefault'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['document_setasdefault'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>

        <tr>
            <td class="ctx_subtitle" colspan="2">
                <?php echo __d('right', 'ctx.relement'); ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'ctx.relements'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.relements', array('type' => 'checkbox', 'class' => 'relements', 'label' => false, 'checked' => $rights['Right']['relements'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['relements'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'ctx.relements_upload'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.relements_upload', array('type' => 'checkbox', 'class' => 'relements_upload', 'label' => false, 'checked' => $rights['Right']['relements_upload'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['relements_upload'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'ctx.relements_suppr'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.relements_suppr', array('type' => 'checkbox', 'class' => 'relements_suppr', 'label' => false, 'checked' => $rights['Right']['relements_suppr'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['relements_suppr'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>

        <tr>
            <td class="ctx_subtitle" colspan="2">
                <?php echo __d('right', 'ctx.ar'); ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'ctx.ar_gen'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.ar_gen', array('type' => 'checkbox', 'class' => 'ar_gen', 'label' => false, 'checked' => $rights['Right']['ar_gen'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['ar_gen'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
    </table>
</fieldset>

<fieldset class="" id="env">
    <legend ><?php echo __d('right', 'env'); ?></legend>
    <table class="acl_table">
        <tr>
            <th><?php echo __d('right', 'right.action'); ?></th>
            <th class="action"><?php echo __d('right', 'right.auth'); ?></th>
        </tr>
        <tr>
            <td><?php echo __d('right', 'env.mes_services'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.mes_services', array('type' => 'checkbox', 'class' => 'mes_services', 'label' => false, 'checked' => $rights['Right']['mes_services'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['mes_services'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'env.recherches'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.recherches', array('type' => 'checkbox', 'class' => 'recherches', 'label' => false, 'checked' => $rights['Right']['recherches'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['recherches'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'env.suppression_flux'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.suppression_flux', array('type' => 'checkbox', 'class' => 'suppression_flux', 'label' => false, 'checked' => $rights['Right']['suppression_flux'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['suppression_flux'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'env.statistiques'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.statistiques', array('type' => 'checkbox', 'class' => 'statistiques', 'label' => false, 'checked' => $rights['Right']['statistiques'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['statistiques'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'env.carnet_adresse'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.carnet_adresse', array('type' => 'checkbox', 'class' => 'carnet_adresse', 'label' => false, 'checked' => $rights['Right']['carnet_adresse'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['carnet_adresse'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
		<tr>
			<td>Créer/éditer des contacts depuis le flux ?</td>
			<td class="action">
				<?php
				if (!empty($edit)) {
					echo $this->Form->input('Right.action_carnet_adresse_flux', array('type' => 'checkbox', 'class' => 'action_carnet_adresse_flux', 'label' => false, 'checked' => $rights['Right']['action_carnet_adresse_flux'] ? 'checked' : ''));
				} else {
					echo $rights['Right']['action_carnet_adresse_flux'] ? $yes : $no;
				}
				?>
			</td>
		</tr>
    </table>
</fieldset>

<fieldset class="" id="wkf">
    <legend ><?php echo __d('right', 'wkf'); ?></legend>
    <table class="acl_table">
        <tr>
            <th><?php echo __d('right', 'right.action'); ?></th>
            <th class="action"><?php echo __d('right', 'right.auth'); ?></th>
        </tr>
        <tr>
            <td><?php echo __d('right', 'wkf.envoi_cpy'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.envoi_cpy', array('type' => 'checkbox', 'class' => 'envoi_cpy', 'label' => false, 'checked' => $rights['Right']['envoi_cpy'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['envoi_cpy'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'wkf.envoi_wkf'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.envoi_wkf', array('type' => 'checkbox', 'class' => 'envoi_wkf', 'label' => false, 'checked' => $rights['Right']['envoi_wkf'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['envoi_wkf'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'wkf.envoi_ged'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.envoi_ged', array('type' => 'checkbox', 'class' => 'envoi_ged', 'label' => false, 'checked' => $rights['Right']['envoi_ged'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['envoi_ged'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo __d('right', 'wkf.consult_scan'); ?></td>
            <td class="action">
                <?php
                if (!empty($edit)) {
                        echo $this->Form->input('Right.consult_scan', array('type' => 'checkbox', 'class' => 'consult_scan', 'label' => false, 'checked' => $rights['Right']['consult_scan'] ? 'checked' : ''));
                } else {
                        echo $rights['Right']['consult_scan'] ? $yes : $no;
                }
                ?>
            </td>
        </tr>
    </table>
</fieldset>

<?php
if (!empty($edit) && empty($noSubmit)) {
//	echo $this->Form->end(__d('default', 'submit'));
    echo $this->Form->end('<a class="btn btn-info-webgfc" style="color:#fff;float:right;margin-top:-20px;">'.__d('default', 'submit').'</a>');
}
?>

<script type="text/javascript">
    function setAll(sets) {
        var setAll = new Array();
        var test = new Array();
        $.each(sets, function (key, val) {
            if ($('.' + key).is(':checked')) {
                $.merge(test, val);
            }
            delete test["length"];
            setAll = test;
        });
        return setAll;
    }
    function setTabsAll(sets) {
        var setAll = new Object();
        var infos = new Array();
        var meta = new Array();
        var taches = new Array();
        var affaire = new Array();
        $.each(sets, function (key, val) {
            if ($('.' + key).is(':checked')) {
                $.each(val, function (k, v) {
                    if (k == 'infos') {
                        infos = jQuery.unique($.merge(infos, v));
                    }
                    if (k == 'meta') {
                        meta = jQuery.unique($.merge(meta, v));
                    }
                    if (k == 'taches') {
                        taches = jQuery.unique($.merge(taches, v));
                    }
                    if (k == 'affaire') {
                        affaire = jQuery.unique($.merge(affaire, v));
                    }
                });
            }
        });
        setAll['infos'] = infos;
        setAll['meta'] = meta;
        setAll['taches'] = taches;
        setAll['affaire'] = affaire;
        return setAll;
    }
    //bloquer les droit vient de son profil
    <?php if(isset($profil)){ ?>
    var profil = '<?php echo $profil ?>';

    $('.' + profil).attr('checked', 'checked');
    $('.' + profil).attr("disabled", true);
    <?php } ?>
<?php if (!empty($tabsForSets)) { ?>
    var tabsForSets = jQuery.parseJSON('<?php echo $tabsForSets; ?>');
    $.each(tabsForSets, function (key, val) {
        $('.' + key).hover(function () {
            $.each(val, function (tab, chks) {
                $.each(chks, function (chkKey, chkVal) {
                    $('.' + tab + '.' + chkVal).parents('td').addClass('alert alert-warning');
                });
            });
        }, function () {
            $.each(val, function (tab, chks) {
                $.each(chks, function (chkKey, chkVal) {
                    $('.' + tab + '.' + chkVal).parents('td').removeClass('alert alert-warning');
                });
            });
        }).change(function () {
            if ($(this).prop("checked")) {
                $.each(setTabsAll(tabsForSets), function (tab, chks) {
                    $.each(chks, function (chkKey, chkVal) {
                        $('.' + tab + '.' + chkVal).attr('checked', 'checked');
                    });
                });
            } else {
                $.each(val, function (k, vs) {
                    $.each(vs, function (index, v) {
                        if ($.inArray(v, setTabsAll(tabsForSets)[k]) == -1) {
                            $('.' + k + '.' + v).removeAttr('checked');
                        }
                    });
                });
            }
        });
    });
<?php } ?>

<?php if (!empty($envsForSets)) { ?>
    var envsForSets = jQuery.parseJSON('<?php echo $envsForSets; ?>');
    $.each(envsForSets, function (key, val) {
        $('.' + key).hover(function () {
            $.each(val, function (key, chkVal) {
                $('.' + chkVal).parents('td').addClass('alert alert-warning');
            });
        }, function () {
            $.each(val, function (key, chkVal) {
                $('.' + chkVal).parents('td').removeClass('alert alert-warning');
            });
        }).change(function () {
            if ($(this).prop("checked")) {

                $.each(setAll(envsForSets), function (tab, chks) {
                    $('.' + chks).attr('checked', 'checked');
                });

            } else {
                $.each(val, function (k, vs) {
                    if ($.inArray(vs, setAll(envsForSets)) == -1) {
                        $('.' + vs).removeAttr('checked');
                    }
                });
            }
        });
    });
<?php } ?>

<?php if (!empty($ctxsForSets)) { ?>
    var ctxsForSets = jQuery.parseJSON('<?php echo $ctxsForSets; ?>');
    $.each(ctxsForSets, function (key, val) {
        $('.' + key).hover(function () {
            $.each(val, function (key, chkVal) {
                $('.' + chkVal).parents('td').addClass('alert alert-warning');
            });
        }, function () {
            $.each(val, function (key, chkVal) {
                $('.' + chkVal).parents('td').removeClass('alert alert-warning');
            });
        }).change(function () {
            if ($(this).prop("checked")) {
                $.each(setAll(ctxsForSets), function (tab, chks) {
                    $('.' + chks).attr('checked', 'checked');
                });
            } else {
                $.each(val, function (k, vs) {
                    if ($.inArray(vs, setAll(ctxsForSets)) == -1) {
                        $('.' + vs).removeAttr('checked');
                    }
                });
            }
        });
    });
<?php } ?>

<?php if (!empty($wkfsForSets)) { ?>
    var wkfsForSets = jQuery.parseJSON('<?php echo $wkfsForSets; ?>');
    $.each(wkfsForSets, function (key, val) {
        $('.' + key).hover(function () {
            $.each(val, function (key, chkVal) {
                $('.' + chkVal).parents('td').addClass('alert alert-warning');
            });
        }, function () {
            $.each(val, function (key, chkVal) {
                $('.' + chkVal).parents('td').removeClass('alert alert-warning');
            });
        }).change(function () {
            if ($(this).prop("checked")) {
                $.each(setAll(wkfsForSets), function (tab, chks) {
                    $('.' + chks).attr('checked', 'checked');
                });
            } else {
                $.each(val, function (k, vs) {
                    if ($.inArray(vs, setAll(wkfsForSets)) == -1) {
                        $('.' + vs).removeAttr('checked');
                    }
                });
            }
        });
    });
    $('#RightCreateInfos').change(function () {
        if ($('#RightCreateInfos').is(':checked')) {
            $('#RightGetInfos').attr('checked', true);
            $('#RightSetInfos').attr('checked', true);
        }
    });
    $('#RightGetInfos').change(function () {
        if (!$('#RightGetInfos').is(':checked')) {
            $('#RightSetInfos').attr('checked', false);
        }
    });
    $('#RightSetInfos').change(function () {
        if ($('#RightSetInfos').is(':checked')) {
            $('#RightGetInfos').attr('checked', true);
        }
    });
    $('#RightGetMeta').change(function () {
        if (!$('#RightGetMeta').is(':checked')) {
            $('#RightSetMeta').attr('checked', false);
        }
    });
    $('#RightSetMeta').change(function () {
        if ($('#RightSetMeta').is(':checked')) {
            $('#RightGetMeta').attr('checked', true);
        }
    });
    $('#RightGetTaches').change(function () {
        if (!$('#RightGetTaches').is(':checked')) {
            $('#RightSetTaches').attr('checked', false);
        }
    });
    $('#RightSetTaches').change(function () {
        if ($('#RightSetTaches').is(':checked')) {
            $('#RightGetTaches').attr('checked', true);
        }
    });
    $('#RightGetAffaire').change(function () {
        if (!$('#RightGetAffaire').is(':checked')) {
            $('#RightSetAffaire').attr('checked', false);
        }
    });
    $('#RightSetAffaire').change(function () {
        if ($('#RightSetAffaire').is(':checked')) {
            $('#RightGetAffaire').attr('checked', true);
        }
    });
<?php } ?>
</script>
