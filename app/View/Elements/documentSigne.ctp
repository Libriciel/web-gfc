<?php
/**
 *
 * Elements/documentList.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
foreach ($documentSigne as $document) {
	?>
	<li class="ctxdoc">
		<?php
		echo $this->Element('ctxdoc', array(
			'ctxdoc' => $document['Document'],
			'gfcDocType' => 'Document',
			'cssClass' => '',
			'selectableForMainDoc' => true,
			'hasParapheurActif' => $hasParapheurActif,
			'hasEtapeParapheur' => $hasEtapeParapheur
		));
		?>
	</li>
<?php } ?>
