<div class="col-sm-12 zone-form">
    <legend class="panel-title"><?php echo __d('operation', 'Operation.newSousoperations'); ?></legend>
    <div class="panel-body">
            <?php
                $formSousoperation = array(
                    'name' => 'Sousoperation',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-5',
                    'form_url' => array( 'controller' => 'sousoperations', 'action' => 'add'),
                    'input' => array(
                        'Sousoperation.operation_id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden',
                                'value' => $operationId
                            )
                        ),
                        'Sousoperation.name' => array(
                            'labelText' =>__d('operation', 'Sousoperation.name'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        )
                    )
                );
                echo $this->Formulaire->createForm($formSousoperation);
                echo $this->Form->end();
            ?>
    </div>
    <div class="controls text-center" role="group">
        <a id="cancel" class="btn btn-danger-webgfc btn-inverse "><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a id="valid" class="btn btn-success "><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script type="text/javascript">

    $('#valid').click(function () {
        if (form_validate($('#SousoperationAddForm'))) {
            gui.request({
                url: '<?php echo Router::url(array('controller' => 'sousoperations', 'action' => 'add', $operationId)); ?>',
                data: $('#SousoperationAddForm').serialize(),
                loaderElement: $('#infos .content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                getJsonResponse(data);
                loadSousOperation();
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",
                
            });
        }

    });
    $('#cancel').click(function () {
        loadSousOperation();
    });
</script>
