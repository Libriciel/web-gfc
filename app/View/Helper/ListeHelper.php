<?php

/**
 *
 * Liste helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		cake.app
 * @subpackage	cake.app.view.helper
 */
class listeHelper extends Helper {

    /**
     *
     * @var type
     */
    var $helpers = array('String', 'Html', 'Paginator');

    /**
     *
     * @var type
     */
    var $treeTrId = 0;

    public function drawPanelHeading($tab, $options = array()) {
        $settings = array(
            'filter' => true,
            'title' => '',
            'noTitle' => false,
            'paginator' => false
        );
        foreach ($options as $key => $val) {
            $settings[$key] = $val;
        }
        $nbItem = count($tab);
        if (!$settings['paginator'] && !$settings['noTitle']) {
            if ($settings['title'] != '') {
                $settings['title'] .= ' (total : ' . $nbItem . ')';
            } else {
                $settings['title'] = 'total :  ' . $nbItem;
            }
        }

//        $h4 = $this->Html->tag('h4', $settings['title'], array('class' => 'bannette'));
//        $data = $this->Html->div('panel-heading', $h4);
        $data = $this->Html->tag('span', ' - ' . $settings['title'], array('class' => 'bannette', 'style' => 'border-bottom: none', 'id' => 'title_nb'));
//        $data .= "<script>if ($('.table-list h3 span').length < 1) { $('.table-list h3').append($('#title_nb')); }</script>";
        return $data;
    }

    public function drawTbody($tab, $model, $fields = array(), $actions = array(), $options = array()) {
        $data = array();

		$userIdConnected = CakeSession::read('Auth.User.id');
        $i = 0;
        foreach ($tab as $item) {

//       }
//        for ($i = 0; $i < count($tab); $i++) {
//            $item = $tab[$i];
            foreach ($fields as $field) {
                $data[$i][$field] = $item[$model][$field];
            }
            if (isset($item['right_add']) && $item['right_add']) {
                $applyAddButton = true;
                $data[$i]['add'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.add'),
                    'alt' => __d('default', 'Button.add'),
                    'class' => "fa fa-plus-circle itemAdd",
                    'itemId' => $item[$model]['id'],
                    'onclick' => "return addElement(" . $item[$model]['id'] . ")",
//                    'style' => 'color: #5397a7;cursor:pointer'
                        )
                );
            }

            if (isset($item['right_view']) && $item['right_view']) {
                $applyViewButton = true;
                $data[$i]['view'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.view'),
                    'alt' => __d('default', 'Button.view'),
                    'class' => "fa fa-eye itemView",
                    'itemId' => $item[$model]['id'],
                    'onclick' => "return viewElement(" . $item[$model]['id'] . ")",
//                    'style' => 'color: #5397a7;cursor:pointer'
                        )
                );
            }

            if (isset($item['right_edit']) && $item['right_edit']) {
                if (isset($item['Desktopmanager'])  && in_array( $item['Desktopmanager']['id'], ['-1', '-2', '-3'] ) ) {
                    $data[$i]['edit'] = '';
                } else {
                    $applyEditButton = true;
                    $data[$i]['edit'] = $this->Html->tag('i', '', array(
                        'title' => __d('default', 'Button.edit'),
                        'alt' => __d('default', 'Button.edit'),
                        'class' => "fa fa-pencil itemEdit",
                        'aria-hidden' => true,
                        'itemId' => $item[$model]['id'],
                        'onclick' => "return editElement(" . $item[$model]['id'] . ");",
    //                    'style' => 'color: #5397a7;cursor:pointer'
                            )
                    );
                }
            }

            $applyDownloadButton = true;
            $data[$i]['download'] = $this->Html->tag('i', '', array(
                'title' => __d('default', 'Button.download'),
                'alt' => __d('default', 'Button.download'),
                'class' => "fa fa-download itemDownload",
                'aria-hidden' => true,
                'itemId' => $item[$model]['id'],
                'onclick' => "return downloadElement(" . $item[$model]['id'] . ");"
                    )
            );

            $isAdmin = false;
            if (isset($item['Desktop']['name'])) {
// FIXME: trouver une meilleure identification du bureau admin
                if ($item['Desktop']['name'] === 'Bureau administrateur') {
                    $isAdmin = true;
                }
            }
            if (isset($item['right_delete']) && !$isAdmin & $item['right_delete']) {
                $applyDeleteButton = true;
				$isSuperadminConnected = false;
				if( $model == 'User' ) {
					if( $userIdConnected == $item[$model]['id'] ) {
						$isSuperadminConnected = true;
					}
				}
                if (isset($item['Traitement']) && !empty($item['Traitement']) || (isset($item['Desktopmanager']) && in_array( $item['Desktopmanager']['id'], ['-1', '-2', '-3'] ) ) || $isSuperadminConnected) {
                    $data[$i]['delete'] = '';
                } else {
                    $data[$i]['delete'] = $this->Html->tag('i', '', array(
                        'title' => __d('default', 'Button.delete'),
                        'alt' => __d('default', 'Button.delete'),
                        'class' => "fa fa-trash itemDelete",
                        'itemId' => $item[$model]['id'],
                        'onclick' => "return deleteElement(" . $item[$model]['id'] . ")",
//                        'style' => 'color: #FF0000;cursor:pointer'
                            )
                    );
                }
            }
            // Ajout pour la vérification du connecteur parapheur
            if (isset($item['right_check']) && $item['right_check']) {
                $applyCheckButton = true;
                $data[$i]['check'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.check'),
                    'alt' => __d('default', 'Button.check'),
                    'class' => "fa fa-check itemCheck",
                    'itemId' => $item[$model]['id'],
                    'onclick' => "return checkElement(" . $item[$model]['id'] . ")",
//                    'style' => 'color: #5397a7;cursor:pointer'
                        )
                );
            }
            // Ajout pour l'icône de copie de circuit
            if (isset($item['right_copieCircuit']) && $item['right_copieCircuit']) {
                $applyCopieCircuitButton = true;
                $data[$i]['copieCircuit'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.copieCircuit'),
                    'alt' => __d('default', 'Button.copieCircuit'),
                    'class' => "fa fa-files-o itemCopieCircuit",
                    'onclick' => "return copieElement(" . $item[$model]['id'] . ")",
//                    'style' => 'color: #5397a7;cursor:pointer'
                        )
                );
            }

            if (isset($item['right_export']) && $item['right_export']) {
                $applyExportButton = true;
                $data[$i]['export'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.exportcsv'),
                    'alt' => __d('default', 'Button.exportcsv'),
                    'class' => "fa fa-download itemExport",
                    'onclick' => "return exportElement(" . $item[$model]['id'] . ")",
//                    'style' => 'color: #5397a7;cursor:pointer'
                        )
                );
            }
            if (isset($item['Document']['id']) && $item['Document']['id'] != null && $item['Document']['ishistorique'] == false ) {
                $data[$i]['apercu'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.apercu'),
                    'alt' => __d('default', 'Button.apercu'),
                    'class' => "fa fa-file itemApercu",
                    'onclick' => "return apercuElement(" . $item[$model]['reference'] . ")",
//                    'style' => 'color: #5397a7;cursor:pointer'
                        )
                );
            } else {
                $data[$i]['apercu'] = "";
            }

            // Ajout pour la vérification du connecteur parapheur
            if (isset($item['right_sync']) && $item['right_sync']) {
                $applySyncButton = true;
                $data[$i]['sync'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.sync'),
                    'alt' => __d('default', 'Button.sync'),
                    'class' => "fa fa-refresh itemSync",
                    'itemId' => $item[$model]['id'],
                    'onclick' => "return syncElement(" . $item[$model]['id'] . ")"
                        )
                );
            }

             // Ajout pour le téléchargement du bordereau
            if( isset( $item['Document']['ishistorique'] ) &&  $item['Document']['ishistorique'] ) {
                $img = $this->Html->tag('i','',array('class'=>'fa fa-download fa-fw ctxdocBttnDownload','aria-hidden'=>true));
                $data[$i]['download'] = $this->Html->link($img, '/documents/download/' . $item[$model]['id'], array('escape' => false, 'class' => 'treeviewBttn'));
            }

            // Ajout de l'icône de check pour les connecteurs actifs ou non
			if( isset( $item['Connecteur'] ) ) {
				if( isset( $item['active'] ) && $item['active'] ) {
					$data[$i]['active'] = $this->Html->tag('i','',array('class'=>'fa fa-check', 'style' => 'color:green;', 'aria-hidden'=>true));
				}
				else {
					$data[$i]['active'] = $this->Html->tag('i','',array('class'=>'fa fa-times', 'style' => 'color:red;', 'aria-hidden'=>true));
				}
			}

			// Ajout pour la vérification du connecteur parapheur
			if (isset($item['right_lock']) && $item['right_lock']) {
				$applyLockButton = true;
				$data[$i]['lock'] = $this->Html->tag('i', '', array(
						'title' => __d('default', 'Button.lock'),
						'alt' => __d('default', 'Button.lock'),
						'class' => "fa fa-lock itemLock",
						'itemId' => $item[$model]['id'],
						'onclick' => "return lockElement(" . $item[$model]['id'] . ")"
					)
				);
			}

            $i++;
        }
        return json_encode($data);
    }

    public function drawScript($tab, $model, $fields = array(), $actions = array(), $options = array()) {
        echo '<script>
        var actionsBtnCancel = "' . __d("default", "Button.cancel") . '";
        var actionsBtnValide = "' . __d("default", "Button.valid") . '";
        var editElementSpecial = false;
        ';

        if (isset($actions["view"])) {
            echo
            'var actionsViewFormMessage = "' . $actions["view"]["formMessage"] . '";
            var actionsViewUpdateElement = ' . $actions["view"]["updateElement"] . ';
            var actionsViewTitle =" ' . __d(strtolower($model), "View" . $model, true) . '";
            var actionsViewUrl = "' . $actions["view"]["url"] . '";';
            if (isset($actions["view"]["formMessageWidth"])) {
                echo '
                var actionsViewFormMessageWidth ="' . $actions["view"]["formMessageWidth"] . '";';
            }
            if (isset($actions["view"]["formMessageHeight"])) {
                echo '
                var actionsViewFormMessageHeight ="' . $actions["view"]["formMessageHeight"] . '";';
            }
        }

        if (isset($actions["edit"])) {
            if (isset($options['editSpecial']) && $options['editSpecial'] != "") {
                echo
                'var editElementSpecial = true;';
            }
//            debug($actions["edit"]);
            if (isset($actions["edit"]["formMessage"])) {
                echo
                'var actionsEditFormMessage = "' . $actions["edit"]["formMessage"] . '";';
            }
            if (isset($actions["edit"]["updateElement"])) {
                echo
                'var actionsEditUpdateElement = ' . $actions["edit"]["updateElement"] . ';';
            }
            echo
            'var actionsEditTitle = "' . __d(strtolower($model), "Edit" . $model, true) . '";
            var actionsEditUrl = "' . $actions["edit"]["url"] . '";';
            if (isset($actions["edit"]["divshow"])) {
                echo'
                var actionsEditDivshow = "' . $actions["edit"]["divshow"] . '";';
            }
            if (isset($actions["edit"]["divhide"])) {
                echo'
                var actionsEditDivhide = "' . $actions["edit"]["divhide"] . '";';
            }
            if (isset($actions["edit"]["formMessageWidth"])) {
                echo '
                var actionsEditFormMessageWidth ="' . $actions["edit"]["formMessageWidth"] . '";';
            }
            if (isset($actions["edit"]["formMessageHeight"])) {
                echo '
                var actionsEditFormMessageHeight ="' . $actions["edit"]["formMessageHeight"] . '";';
            }
            if (isset($actions["edit"]["message"])) {
                echo '
                var actionsEditMessage = "' . $actions["edit"]["message"] . '";';
            }
            if (isset($actions["edit"]["refreshAction"])) {
                echo '
                var actionsEditRefreshAction = "' . $actions["edit"]["refreshAction"] . '";';
            }
        }


        if (isset($actions["delete"])) {
            echo
            'var actionsDeleteUpdateElement = ' . $actions["delete"]["updateElement"] . ';
            var actionsDeleteTitle =" ' . __d("default", "Modal.suppressionTitle") . '";
            var actionsDeleteText = "' . __d("default", "Modal.suppressionContents") . '";
            var actionsDeleteUrl = "' . $actions["delete"]["url"] . '";';
            if (isset($actions["delete"]["refreshAction"])) {
                echo '
                var actionsDeleteRefreshAction = "' . $actions["delete"]["refreshAction"] . '";';
            }
        }

        if (isset($actions["check"])) {
            echo
            'var actionsCheckUpdateElement = ' . $actions["check"]["updateElement"] . ';
            var actionsCheckUrl = "' . $actions["check"]["url"] . '";';
        }

		if (isset($actions["lock"])) {
			if (isset($actions["lock"]["formMessage"])) {
				echo
					'var actionsLockFormMessage = "' . $actions["lock"]["formMessage"] . '";';
			}
			if (isset($actions["lock"]["updateElement"])) {
				echo
					'var actionsLopckUpdateElement = ' . $actions["lock"]["updateElement"] . ';';
			}
			if (isset($actions["lock"]["divshow"])) {
				echo'
                var actionsLockDivshow = "' . $actions["lock"]["divshow"] . '";';
			}
			if (isset($actions["lock"]["divhide"])) {
				echo'
                var actionsLockDivhide = "' . $actions["lock"]["divhide"] . '";';
			}
			if (isset($actions["lock"]["formMessageWidth"])) {
				echo '
                var actionsLockFormMessageWidth ="' . $actions["lock"]["formMessageWidth"] . '";';
			}
			if (isset($actions["lock"]["formMessageHeight"])) {
				echo '
                var actionsLockFormMessageHeight ="' . $actions["lock"]["formMessageHeight"] . '";';
			}
			if (isset($actions["lock"]["message"])) {
				echo '
                var actionsLockMessage = "' . $actions["lock"]["message"] . '";';
			}
			if (isset($actions["lock"]["refreshAction"])) {
				echo '
                var actionsLockRefreshAction = "' . $actions["lock"]["refreshAction"] . '";';
			}
			echo
				'var actionsLockUpdateElement = ' . $actions["lock"]["updateElement"] . ';
            var actionsLockUrl = "' . $actions["lock"]["url"] . '";';
		}

        if (isset($actions["sync"])) {
            echo
            'var actionsSyncUpdateElement = ' . $actions["sync"]["updateElement"] . ';
            var actionsSyncUrl = "' . $actions["sync"]["url"] . '";';
        }
        if (isset($actions["copieCircuit"])) {
            echo
            'var actionsCopieCircuitUpdateElement = ' . $actions["copieCircuit"]["updateElement"] . ';
            var actionsCopieCircuitUrl = "' . $actions["copieCircuit"]["url"] . '";';
            if (isset($actions["copieCircuit"]["refreshAction"])) {
                echo '
                var actionsCopieCircuitRefreshAction = "' . $actions["copieCircuit"]["refreshAction"] . '";

                ';
            }
        }

        if (isset($actions["export"])) {
            echo '
                var actionsExposrtUrl = "' . $actions["export"]["url"] . '";
            ';
        }

        if (isset($actions["download"])) {
            echo '
                var actionsDownloadUpdateElement = ' . $actions["download"]["updateElement"] . ';
                var actionsDownloadUrl = "' . $actions["download"]["url"] . '";
            ';
        }

        echo'</script>';

        echo $this->Html->script('liste.js');
    }

    /**
     *
     * @param type $tab
     * @param type $model
     * @param type $labels
     * @param type $actions
     * @param type $updateElements
     * @param type $rights
     * @param type $treeTrParentsId
     * @param type $lvl
     */
    public function drawTree($tab, $model, $labels, $actions, $updateElements, $rights, $treeTrParentsId = array(), $lvl = 0) {
        $retval = '';
        foreach ($tab as $item) {
            $hasChildren = isset($item['children'][0]);
            if ($hasChildren) {
                $treeTrParentsId[] = $this->treeTrId;
            }
            $retval .= $this->drawTreeLine($item, $model, $labels, count($actions), $updateElements, $rights, $lvl, $hasChildren, $treeTrParentsId) . ',';
            if ($hasChildren) {
                if( $model == 'Type' ) {
                    $model2 = 'Soustype';
                }
                else if( $model == 'Dossier' ) {
                    $model2 = 'Affaire';
                }
                else {
                    $model2 = $model;
                }
                $lvl++;
                $retval .= $this->drawTree($item['children'], $model2, $labels, $actions, $updateElements, $rights, $treeTrParentsId, $lvl);
                $lvl--;
                array_pop($treeTrParentsId);
            }
        }
        return $retval;
    }

    /**
     *
     * @param type $item
     * @param type $model
     * @param type $labels
     * @param type $nbAction
     * @param type $updateElements
     * @param type $rights
     * @param type $lvl
     * @param type $hasChildren
     * @param type $treeTrParentsId
     */
    public function drawTreeLine($item, $model, $labels, $nbAction, $updateElements, $rights, $lvl, $hasChildren, $treeTrParentsId) {

        $data = array();
        foreach ($labels as $index => $label) {
            $indentItem = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            $indentation = '';
            for ($i = 0; $i < $lvl; $i++) {
                $indentation .= $indentItem;
            }
            if ($hasChildren) {
//                $indentation .= $this->Html->image('/img/arrow_down.png', array('class' => 'treeCollapseButton'));
                $indentation .= '<i class="fa fa-minus-square-o treeCollapseButton" aria-hidden="true" style="padding-right: 0.6em;"></i>';
            }
            if (isset($label) && !empty($label)) {
                if ($label == 'active') {
                    $data[$label] = $item[$model][$label];
                } else {
                    $data[$label] = $indentation . '  ' . $item[$model][$label];
                }
            }
        }
        if ($nbAction != 0) {
            if (isset($rights['view'])) {
                $data['view'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.view'),
                    'alt' => __d('default', 'Button.view'),
                    'class' => "fa fa-eye itemView",
                    'itemId' => $item[$model]['id'],
//                    'style' => 'color: #5397a7;cursor:pointer'
                        )
                );
            }
            if (isset($rights['read'])) {
                $parent = @$item[$model]['hasParent'];
                if( $parent ) {
                    $dataId = '1';
                }
                else if(!$parent) {
                    $dataId = '0';
                }

                $data['edit'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.edit'),
                    'alt' => __d('default', 'Button.edit'),
                    'class' => "fa fa-pencil itemEdit",
                    'itemId' => $item[$model]['id'],
                    'id' => $item[$model]['id'],
                    'parentId' => $dataId
                        )
                );
				if( $model == 'Affaire') {
                    $data['attach'] = $this->Html->tag('i', '', array(
                        'title' => __d('default', 'Button.attach'),
                        'alt' => __d('default', 'Button.attach'),
                        'class' => "fa fa-link itemAttach",
                        'itemId' => $item[$model]['id'],
                        'id' => $item[$model]['id'],
                        'parentId' => $dataId
                            )
                    );
                }
                $data['info'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.info'),
                    'alt' => __d('default', 'Button.info'),
                    'class' => "fa fa-info itemInfo",
                    'itemId' => $item[$model]['id'],
                    'id' => $item[$model]['id'],
                    'parentId' => $dataId
                        )
                );
            }
            if (isset($rights['delete'])) {
                $parent = @$item[$model]['hasParent'];
                if( $parent ) {
                    $dataId = '1';
                }
                else if(!$parent) {
                    $dataId = '0';
                }

                $data['delete'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.delete'),
                    'alt' => __d('default', 'Button.delete'),
                    'class' => "fa fa-trash itemDelete",
                    'itemId' => $item[$model]['id'],
                    'parentId' => $dataId
//                    'style' => 'color: #FF0000;cursor:pointer'
                        )
                );

                // Cas particulier pour le Soustype Fantôme que l'on ne peut modifier/supprimer
				if( $model == 'Type' ) {
					if( $item['Type']['id'] == '-1' ) {
						$data['edit'] = '';
						$data['delete'] = '';
					}
				}
				if( $model == 'Soustype' ) {
					if( $item['Soustype']['id'] == '-1' ) {
						$data['edit'] = '';
						$data['delete'] = '';
					}
				}
				if( $model == 'Dossier' ) {
					if( $item['Dossier']['id'] == '' ) {
						$data['edit'] = '';
						$data['delete'] = '';
					}
				}
            }
        }
        return json_encode($data);
    }

    /**
     *
     * @param type $items
     * @param type $options
     * @return type
     */
    public function jqueryListView($items = array(), $options) {
        $retval = "";
        if (!empty($items) && !empty($options)) {
//application des choix par défaut
            $settings = array_merge(array('listId' => 'browser', 'listClass' => 'filetree'), $options);

//recuperation des
            $listId = $settings['listId'];
            $listClass = $settings['listClass'];

            $parentNodeSettings = $settings['nodes'][0];
//      $childNodeSettings = $settings['nodes'][1];

            $nestedListItems = array();
            foreach ($items as $item) {
                $parentNodeSettings['actions']['id'] = $item[$parentNodeSettings['model']]['id'];
                $parentNodeSettings['actions']['model'] = $parentNodeSettings['model'];

                $noIdParent = $item[$parentNodeSettings['model']]['id'] == '' ? ' ui-state-focus' : '';
                $specialClass = "";
                if (isset($item[$parentNodeSettings['model']]['active']) && !$item[$parentNodeSettings['model']]['active']) {
                    $specialClass .=' list-group-item-warning';
                }
                $nestedListName = $this->Html->tag('div', $item[$parentNodeSettings['model']][$parentNodeSettings['field']], array('class' => 'col-sm-7', 'style' => 'white-space: nowrap;overflow: hidden;', 'title' => $item[$parentNodeSettings['model']][$parentNodeSettings['field']]));
                $nestedListParent = $this->Html->tag('span', $nestedListName . /* ' (' . (count($item[$childNodeSettings['model']])) . ')' . */ $this->_genActions($parentNodeSettings['actions']), array('id' => $parentNodeSettings['actions']['model'] . '_' . $parentNodeSettings['actions']['id'], 'class' => 'folder col-sm-12' . $noIdParent . $specialClass));

                $nestedListItems[] = $nestedListParent;
            }
            $retval = $this->Html->nestedList($nestedListItems, array('id' => $listId, 'class' => $listClass));
        }
        return $retval;
    }

    /**
     *
     * @param type $items
     * @param type $options
     * @return type
     */
    public function jqueryTreeView($items = array(), $options) {
        $retval = "";
        if (!empty($items) && !empty($options)) {
//application des choix par défaut
            $settings = array_merge(array('listId' => 'browser', 'listClass' => 'filetree'), $options);

//recuperation des
            $listId = $settings['listId'];
            $listClass = $settings['listClass'];

            $parentNodeSettings = $settings['nodes'][0];
            $childNodeSettings = $settings['nodes'][1];
//debug($items);
//debug($options);
//debug($settings);
            $nestedListItems = array();
            foreach ($items as $item) {
                $parentNodeSettings['actions']['id'] = $item[$parentNodeSettings['model']]['id'];
                $parentNodeSettings['actions']['model'] = $parentNodeSettings['model'];

                $selectedFolder = false;
                foreach ($item[$childNodeSettings['model']] as $child) {
                    $selectedFolder = isset($settings['selectedFolder']) && $settings['selectedFolder'] == $child['dossier_id'];
                }
                $noIdParent = $item[$parentNodeSettings['model']]['id'] == '' ? ' ui-state-focus' : '';

                $specialClass = '';
                if (!empty($parentNodeSettings['classField'])) {
                    $specialClassItemValue = $item[$parentNodeSettings['model']][key($parentNodeSettings['classField'])];
                    $specialClass = ' ' . $parentNodeSettings['classField'][key($parentNodeSettings['classField'])][$specialClassItemValue];
                }
                if (isset($item[$parentNodeSettings['model']]['active']) && !$item[$parentNodeSettings['model']]['active']) {
                    $specialClass .=' list-group-item-warning';
                }
//debug($selectedDossier);
//si dossier
                $id = '';
                if ($parentNodeSettings['model'] == 'Dossier') {
                    $specialClass = ' ' . 'Dossier';
                    $id = '<input type = "hidden" value = "' . $parentNodeSettings['actions']['id'] . '">';
                }
                $nestedListParent = $this->Html->tag('span', $item[$parentNodeSettings['model']][$parentNodeSettings['field']] . ' (' . (count($item[$childNodeSettings['model']])) . ')' . $id . $this->_genActions($parentNodeSettings['actions']), array('class' => 'folder' . /* $noIdParent . */ $specialClass . (($selectedFolder) ? ' ui-state-valid' : '')));

                $nestedListItems[$nestedListParent] = array();
                foreach ($item[$childNodeSettings['model']] as $child) {

                    $specialClassChild = '';
                    if (isset($child['active']) && !$child['active']) {
                        $specialClassChild .= ' list-group-item-warning';
                    }
                    $selected = isset($settings['selectedChild']) && $settings['selectedChild'] == $child['id'];

                    $childNodeSettings['actions']['id'] = $child['id'];
                    $childNodeSettings['actions']['model'] = $childNodeSettings['model'];

//si affaire
                    $idAffaire = '';
                    $specialClassAffaire = '';
                    if ($childNodeSettings['model'] == 'Affaire') {
                        $specialClassAffaire = ' ' . 'Affaire';
                        $idAffaire = '<input type = "hidden" value = "' . $childNodeSettings['actions']['id'] . '" >';
                    }

                    if ($childNodeSettings['model'] == 'Organisme') {
                        $nestedListItems[$nestedListParent][] = $this->Html->tag('span', $child[$childNodeSettings['field']] . ' (' . (count($child['Contact'])) . ')' . $idAffaire . $this->_genActions($childNodeSettings['actions'], $selected), array('class' => $specialClassAffaire . ' file' . (($selected) ? ' ui-state-valid' : '') . $specialClassChild));
                    } else {
                        $nestedListItems[$nestedListParent][] = $this->Html->tag('span', $child[$childNodeSettings['field']] . $idAffaire . $this->_genActions($childNodeSettings['actions'], $selected), array('class' => $specialClassAffaire . ' file' . (($selected) ? ' ui-state-valid' : '') . $specialClassChild, 'id' => $childNodeSettings['actions']['id']));
                    }
                }
            }
            $retval = $this->Html->nestedList($nestedListItems, array('id' => $listId, 'class' => $listClass));
        }
        return $retval;
    }

    public function jqueryThreadedView($items = array(), $options) {
        $retval = "";
        if (!empty($items) && !empty($options)) {
//application des choix par défaut
            $settings = array_merge(array('listId' => 'browser', 'listClass' => 'filetree'), $options);

//recuperation des
            $listId = $settings['listId'];
            $listClass = $settings['listClass'];

            $parentNodeSettings = $settings['nodes'][0];
            $nestedListItems = array();
            foreach ($items as $item) {
                $parentNodeSettings['actions']['id'] = $item[$parentNodeSettings['model']]['id'];
                $parentNodeSettings['actions']['model'] = $parentNodeSettings['model'];

                $noIdParent = $item[$parentNodeSettings['model']]['id'] == '' ? ' ui-state-focus' : '';

                if (!empty($item['children'])) {
                    $text = $this->Html->div("span_text", $item[$parentNodeSettings['model']][$parentNodeSettings['field']] . ' (' . (count($item['children'])) . ' ' . __d('repertoire', 'Repertoire.SousRepertoires') . ')', array("title" => $item[$parentNodeSettings['model']][$parentNodeSettings['field']] . ' (' . (count($item['children'])) . ' ' . __d('repertoire', 'Repertoire.SousRepertoires') . ')'));
                    $nestedListItem = $this->Html->tag('span', $text . $this->_genActions($parentNodeSettings['actions']), array('class' => 'folder' . $noIdParent));
                    $nestedListItem .= $this->jqueryThreadedView($item['children'], $options);
                } else {
                    $text = $this->Html->div("span_text", $item[$parentNodeSettings['model']][$parentNodeSettings['field']] . ' (' . (count($item['Courrier'])) . ' ' . __d('repertoire', 'Repertoire.Courriers') . ')', array("title" => $item[$parentNodeSettings['model']][$parentNodeSettings['field']] . ' (' . (count($item['Courrier'])) . ' ' . __d('repertoire', 'Repertoire.Courriers') . ')'));
                    $nestedListItem = $this->Html->tag('span', $text . $this->_genActions($parentNodeSettings['actions']), array('class' => 'folder' . $noIdParent));
                }
                $nestedListItems[] = $nestedListItem;
            }
            $retval = $this->Html->nestedList($nestedListItems, array('id' => $listId, 'class' => $listClass));
        }
        return $retval;
    }

    /**
     *
     * @param type $actions
     * @param type $selected
     * @return type
     */
    private function _genActions($actions = array(), $selected = false) {
        $retval = "";
        if (!empty($actions) && $actions['id'] != '') {
            $spans = '';
            foreach ($actions as $kAction => $action) {
                if ($kAction != 'id' && $kAction != 'model') {

                    $statusImg = null;
                    /* if (!empty($action['icoAlt']) && $selected) {
                      $statusImg = $this->Html->tag('i', '', array('class' => $action['ico'] . ' statusImg', 'style' => 'color:#FF0000;cursor:pointer;margin-left:10px; font-size:16px'));
                      } */
                    if (!empty($action['icoAlt']) && $selected) {
                        $imgClass = $action['icoAlt'] . ' actionImg ' . $kAction . 'Bttn ' . $actions['model'];
//                        $imgColor = 'color: #FF0000';
                    } else {
                        $imgClass = $action['ico'] . ' actionImg ' . $kAction . 'Bttn ' . $actions['model'];
//                        $imgColor = 'color:' . $action['icoColor'];
                    }
                    $img = $this->Html->tag('i', '', array('class' => $imgClass, 'title' => __d('default', 'Button.' . $kAction . ($selected ? '_alt' : '')), 'alt' => __d('default', 'Button.' . $kAction . ($selected ? '_alt' : '')), 'id' => $actions['model'] . '_' . $actions['id']));

                    $spans .= ($statusImg != null ? $statusImg : '') . $this->Html->link($img, $action['url'] . '/' . $actions['id'], array('escape' => false, 'class' => 'treeviewBttn'));
                }
            }
            $retval = $this->Html->tag('span', $spans, array('style' => 'float:right;margin-right:15px;'));
        }
        return $retval;
    }

}

?>
