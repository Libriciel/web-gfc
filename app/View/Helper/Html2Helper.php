<?php

/**
 *
 * Liste helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		cake.app
 * @subpackage	cake.app.view.helper
 *
 *
 * Création : 13 févr. 2006
 * @author Christophe Espiau
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class Html2Helper extends HtmlHelper {

    /**
     * Retourne deux élements SELECT pour la saisie des heures et des minutes.
     *
     * Cette fonction est une surcharge de la fonction dateTimeOptionTag.
     * Elle est faite :
     * -  pour afficher seulement les heures et les minutes
     * -  pour que les deux champs SELECT n'affichent rien s'il n'y a pas encore eu de pointage enregistré
     * (alors que la fonction dateTimeOptionTag sélectionne l'heure courante par défaut).
     *
     * @param string $tagName Prefix name for the SELECT element
     * @param string $selected Option which is selected. Must be a timestamp. See also getTimestamp().
     * @param array $optionAttr Attribute array for the option elements
     *
     * @return string The HTML formatted SELECT elements
     *
     */
    public function timeOptionTag($tagName, $selected = null, $optionAttr = null) {
        $output = $this->dateTimeOptionTag($tagName, 'NONE', '24', $selected, $optionAttr);

        if ($selected == null) {
            return $this->selectBlankOption($output);
        }
        return $output;
    }

    /**
     *
     * @param type $tagName
     * @param type $selected
     * @param type $optionAttr
     * @return type
     */
    public function dateOptionTag($tagName, $selected = null, $optionAttr = null) {
        $output = $this->dateTimeOptionTagFr($tagName, 'DMY', 'NONE', $selected, $optionAttr);
        if ($selected == null) {
            return $this->selectBlankOption($output);
        }
        return $output;
    }

    /**
     *
     * @param type $output
     * @return type
     */
    public function selectBlankOption($output) {
        $output = str_replace(" selected=\"selected\"", "", $output);
        $output = str_replace(" value=\"\"", " value=\"\" selected=\"selected\"", $output);
        return $output;
    }

    /**
     * Retourne un timestamp Unix à partir d'une date et d'une heure.
     *
     * Un pointage est enregistré en base avec la date dans un champ,
     * l'heure de début dans un autre et l'heure de fin dans un troisième.
     *
     * Dans l'écran de pointage, pour pouvoir afficher les heure déjà enregistrées,
     * il faut passer à la fonction timeOptionTag() (voir ci-dessus) le paramètre $selected
     * sous forme de timestamp, qu'il faut créer à partir du champ date et du champ heureDebut
     * (ou bien date et heureFin).
     *
     * @param string $date Une date au format aaaa-mm-jj
     * @param string $time Une heure au format hh:mm:ss
     *
     * @return  int A Unix timestamp, or null if $time is null.
     */
    public function getTimestamp($date, $time) {
        // Test pour l'heure de fin.
        // Si celle-ci n'est pas encore enregistrée, elle est à NULL dans la base.
        // Et si $time = null, la fonction doit retourner null.
        if ($time == null)
            return null;

        $amj = explode("-", $date);
        $hms = explode(":", $time);

        return mktime($hms[0], $hms[1], $hms[2], $amj[1], $amj[2], $amj[0]);
    }

    /**
     *
     * @param type $tagName
     * @param type $value
     * @param type $minYear
     * @param type $maxYear
     * @param type $selected
     * @param type $selectAttr
     * @param type $optionAttr
     * @param type $showEmpty
     * @return type
     */
    public function yearOptionTagFr($tagName, $value = null, $minYear = null, $maxYear = null, $selected = null, $selectAttr = null, $optionAttr = null, $showEmpty = true) {
        $yearValue = empty($selected) ? null : $selected;
        $currentYear = date('Y');
        $maxYear = is_null($maxYear) ? $currentYear + 11 : $maxYear + 1;
        $minYear = is_null($minYear) ? $currentYear - 42 : $minYear;

        if ($minYear > $maxYear) {
            $tmpYear = $minYear;
            $minYear = $maxYear;
            $maxYear = $tmpYear;
        }
        $minYear = $currentYear < $minYear ? $currentYear : $minYear;
        $maxYear = $currentYear > $maxYear ? $currentYear : $maxYear;

        for ($yearCounter = $minYear; $yearCounter < $maxYear; $yearCounter++) {
            $years[$yearCounter] = $yearCounter;
        }
        $option = $this->selectTag($tagName . "_year", $years, $yearValue, $selectAttr, $optionAttr, $showEmpty);
        return $option;
    }

    /**
     *
     * @param type $tagName
     * @param type $dateFormat
     * @param type $timeFormat
     * @param type $selected
     * @param type $selectAttr
     * @param type $optionAttr
     * @param type $showEmpty
     * @return string
     */
    public function dateTimeOptionTagFr($tagName, $dateFormat = 'DMY', $timeFormat = '12', $selected = null, $selectAttr = null, $optionAttr = null, $showEmpty = false) {
        $day = null;
        $month = null;
        $year = null;
        $hour = null;
        $min = null;
        $meridian = null;

        if (!empty($selected)) {
            if (is_int($selected)) {
                $selected = strftime('%Y-%m-%d  %H:%M', $selected);
            }
            $date = explode('-', $selected);
            $days = explode(' ', $date[2]);

            $day = $days[0];
            $month = $date[1];
            $year = $date[0];

            if ($timeFormat != 'NONE' && !empty($timeFormat)) {
                $time = explode(':', $days[2]);
                if (($time[0] > 12) && $timeFormat == '12') {
                    $time[0] = $time[0] - 12;
                    $meridian = 'pm';
                } elseif ($time[0] > 12) {
                    $meridian = 'pm';
                }
                $hour = $time[0];
                $min = $time[1];
            }
        }

        switch ($dateFormat) {
            case 'DMY' :
                $opt = $this->dayOptionTag($tagName, null, $day, $selectAttr, $optionAttr, $showEmpty) . '-' . $this->monthOptionTagFr($tagName, null, $month, $selectAttr, $optionAttr, $showEmpty) . '-' . $this->yearOptionTagFr($tagName, null, null, null, $year, $selectAttr, $optionAttr, $showEmpty);
                break;
            case 'MDY' :
                $opt = $this->monthOptionTagFr($tagName, null, $month, $selectAttr, $optionAttr, $showEmpty) . '-' . $this->dayOptionTag($tagName, null, $day, $selectAttr, $optionAttr, $showEmpty) . '-' . $this->yearOptionTagFr($tagName, null, null, null, $year, $optionAttr, $selectAttr, $showEmpty);
                break;
            case 'YMD' :
                $opt = $this->yearOptionTagFr($tagName, null, null, null, $year, $selectAttr, $optionAttr, $showEmpty) . '-' . $this->monthOptionTagFr($tagName, null, $month, $selectAttr, $optionAttr, $showEmpty) . '-' . $this->dayOptionTag($tagName, null, $day, $optionAttr, $selectAttr, $showEmpty);
                break;
            case 'NONE':
                $opt = '';
                break;
            default:
                $opt = '';
                break;
        }
        switch ($timeFormat) {
            case '24':
                $opt .= $this->hourOptionTag($tagName, null, true, $hour, $selectAttr, $optionAttr, $showEmpty) . ':' . $this->minuteOptionTag($tagName, null, $min, $selectAttr, $optionAttr, $showEmpty);
                break;
            case '12':
                $opt .= $this->hourOptionTag($tagName, null, false, $hour, $selectAttr, $optionAttr, $showEmpty) . ':' . $this->minuteOptionTag($tagName, null, $min, $selectAttr, $optionAttr, $showEmpty) . ' ' . $this->meridianOptionTag($tagName, null, $meridian, $selectAttr, $optionAttr, $showEmpty);
                break;
            case 'NONE':
                $opt .='';
                break;
            default :
                $opt .='';
                break;
        }
        return $opt;
    }

    /**
     * Returns a SELECT element for years
     *
     * @param string $tagName Prefix name for the SELECT element
     * @deprecated  string $value
     * @param integer $minYear First year in sequence
     * @param integer $maxYear Last year in sequence
     * @param string $selected Option which is selected.
     * @param array $optionAttr Attribute array for the option elements.
     * @param boolean $showEmpty Show/hide the empty select option
     * @return mixed
     * @access public
     */
    public function yearOptionTag($tagName, $value = null, $minYear = null, $maxYear = null, $selected = null, $selectAttr = null, $optionAttr = null, $showEmpty = true) {
        if (empty($selected) && ($this->tagValue($tagName))) {
            $selected = date('Y', strtotime($this->tagValue($tagName)));
        }

        $yearValue = empty($selected) ? ($showEmpty ? NULL : date('Y')) : $selected;
        $currentYear = date('Y');
        $maxYear = is_null($maxYear) ? $currentYear + 11 : $maxYear + 1;
        $minYear = is_null($minYear) ? $currentYear - 60 : $minYear;

        if ($minYear > $maxYear) {
            $tmpYear = $minYear;
            $minYear = $maxYear;
            $maxYear = $tmpYear;
        }

        for ($yearCounter = $minYear; $yearCounter < $maxYear; $yearCounter++) {
            $years[$yearCounter] = $yearCounter;
        }

        return $this->selectTag($tagName . "_year", $years, $yearValue, $selectAttr, $optionAttr, $showEmpty);
    }

    /**
     *
     * @param type $date
     * @return type
     */
    public function ukToFrenchDate($date) {
        return date("d-m-Y", strtotime($date));
    }

    /**
     *
     * @param type $date
     * @return type
     */
    public function ukToFrenchDateWithHour($date) {
        return date("d-m-Y \a H:i", strtotime($date));
    }

    /**
     * affiche une flèche vers le bas et une flèche vers le bas pour le tri asc et desc de $urlChampTri
     *
     * @param type $urlChampTri
     * @param type $inverse
     */
    public function boutonsTri($urlChampTri = null, $inverse = false) {
        echo $this->link(SHY, $urlChampTri . '/' . ($inverse ? 'DESC' : 'ASC'), array('class' => 'link_triasc', 'title' => 'Trier par ordre croissant'), false, false);
        echo $this->link(SHY, $urlChampTri . '/' . ($inverse ? 'ASC' : 'DESC'), array('class' => 'link_tridesc', 'title' => 'Trier par ordre décroissant'), false, false);
    }

    /*
     *  WebGFC Edit
     */

    /**
     *
     * @param type $date
     * @return type
     */
    public function ukToFrenchDateWithHourAndSlashes($date) {
        return date("d/m/Y \à H:i", strtotime($date));
    }

    /**
     *
     * @param type $date
     * @return type
     */
    public function ukToFrenchDateWithSlashes($date) {
        return date("d/m/Y", strtotime($date));
    }

    /**
     *
     * @param type $name
     * @param type $ext
     * @param type $options
     * @param type $size
     * @return string
     */
    public function downloadLink($filename, $options = array('controller' => 'courriers', 'action' => 'download'), $size = 16) {
        $tmp = explode('.', $filename);
        $name = $tmp[0];
        $ext = $tmp[1];
        $imgOptions = array(
            'alt' => 'Télécharger le fichier ' . $name . ' ' . $ext,
            'style' => 'vertical-align: middle;height: ' . $size . 'px;width: ' . $size . 'px;'
        );
        $img = $this->image('/img/download_' . $size . '.png', $imgOptions);
        $span = $this->tag('span', $img . "&nbsp;" . __d('default', 'File.goDownload'));
        $anchorUrl = array(
            'controller' => $options['controller'],
            'action' => $options['action'],
            $name,
            $ext
        );
        $linkOptions = array(
            'id' => date('YmdHis') . '_downloadLink',
            'class' => 'downloadLink bttn',
            'title' => 'Télécharger le fichier ' . $name . ' ' . $ext,
            'escape' => false
        );
        return $this->link($span, $anchorUrl, $linkOptions);
    }

    /**
     *
     * @param type $filename
     * @param type $dlOptions
     * @param type $download
     * @return type
     */
    public function fileInfo($filename, $dlOptions = array()) {

        $defaultDlOptions = array(
            'class' => 'fileinfo ui-widget-content ui-corner-all',
            'download' => false
        );
        $dlOpts = array_merge($defaultDlOptions, $dlOptions);

        $tmp = explode('.', $filename);
        $name = $tmp[0];
        $ext = $tmp[1];
        if (is_file(ASSOCIATED_DIR . $name . '.' . $ext)) {
            $path = ASSOCIATED_DIR;
        } else if (is_file(GENERATED_DIR . $name . '.' . $ext)) {
            $path = GENERATED_DIR;
        } else if (is_file(VIDEO_DIR . $name . '.' . $ext)) {
            $path = VIDEO_DIR;
        } else if (is_file(WAV_DIR . $name . '.' . $ext)) {
            $path = WAV_DIR;
        } else if (is_file(MODELS_DIR . $name . '.' . $ext)) {
            $path = MODELS_DIR;
        }

        $dlContent = $this->tag('dt', __d('default', 'File.filename'));
        $dlContent .= $this->tag('dd', $name . '.' . $ext);
        $dlContent .= $this->tag('dt', __d('default', 'File.size'));
        $dlContent .= $this->tag('dd', $this->formatSize(filesize($path . $name . '.' . $ext)));
        if ($dlOpts['download']) {
            $dlContent .= $this->tag('dt', __d('default', 'Button.download'));
            $dlContent .= $this->tag('dd', $this->downloadLink($name . '.' . $ext));
        }


        return $this->tag('dl', $dlContent, $dlOpts);
    }

    /**
     *
     * @param type $size
     * @return type
     */
    public function formatSize($size) {
        $sizes = array(" o", " Ko", " Mo", " Go", " To", " Po", " Eo", " Zo", " Yo");
        if ($size == 0) {
            return('n/a');
        } else {
            return (round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]);
        }
    }

    /**
     *
     * @param type $size
     * @return type
     */
    public function monthYearDate($date) {
        if (empty($date)) {
            return '';
        } else {
            return date("mY", strtotime($date));
        }
    }


	/**
	 * @param $string
	 * @param $start
	 * @param $end
	 * @return false|string
	 */
	public function get_string_between($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

}

?>
