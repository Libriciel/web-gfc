<?php

/**
 *
 * Bannette helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		cake.app
 * @subpackage	cake.app.view.helper
 */
class DroitsHelper extends Helper {

	/**
	 *
	 * @var type
	 */
	var $helpers = array('Html', 'Form');

	/**
	 *
	 * @param type $tabCRUD
	 * @param type $firstLabel
	 * @param type $secondLabel
	 * @param type $CRUD
	 */
	function printTabCRUD($tabCRUD, $firstLabel, $secondLabel, $CRUD = array('create' => true)) {

		$idSuffix = str_replace(array(' ', '.'), '', microtime());
		$id = "tabCRUD_" . $idSuffix;

		echo "<table id='" . $id . "'>";
		echo "<thead>";
		if ($firstLabel != "")
			echo "<th>" . $firstLabel . "</th>";
		echo "<th>" . $secondLabel . "</th>";
		if (isset($CRUD['create']) && $CRUD['create'] == true) {
			echo "<th style='padding: 0; width: 60px; text-align: left;'>" . __d('default', 'right.create') . "</th>";
		}
		if (isset($CRUD['read']) && $CRUD['read'] == true) {
			echo "<th style='padding: 0; width: 60px; text-align: left;'>" . __d('default', 'right.read') . "</th>";
		}
		if (isset($CRUD['update']) && $CRUD['update'] == true) {
			echo "<th style='padding: 0; width: 60px; text-align: left;'>" . __d('default', 'right.update') . "</th>";
		}
		if (isset($CRUD['delete']) && $CRUD['delete'] == true) {
			echo "<th style='padding: 0; width: 60px; text-align: left;'>" . __d('default', 'right.delete') . "</th>";
		}
		echo "</thead>";
		echo "<tbody>";
		foreach ($tabCRUD as $name => $item) {
			foreach ($item as $subItemName => $subItem) {
				echo "<tr onmouseover='$(this).addClass(\"alert alert-warning\");' onmouseout='$(this).removeClass(\"alert alert-warning\");'>";
				if ($firstLabel != "")
					echo "<td>" . $name . "</td>";
				echo "<td>" . $subItemName . "</td>";
				if (isset($CRUD['create']) && $CRUD['create'] == true) {
					echo "<td style='padding: 0; width: 60px; text-align: center;'>" . $this->Form->checkbox($subItemName . "_create", array('checked' => $subItem["create"] == 1 ? 'checked' : '')) . "</td>";
				}
				if (isset($CRUD['read']) && $CRUD['read'] == true) {
					echo "<td style='padding: 0; width: 60px; text-align: center;'>" . $this->Form->checkbox($subItemName . "_read", array('checked' => $subItem["read"] == 1 ? 'checked' : '')) . "</td>";
				}
				if (isset($CRUD['update']) && $CRUD['update'] == true) {
					echo "<td style='padding: 0; width: 60px; text-align: center;'>" . $this->Form->checkbox($subItemName . "_update", array('checked' => $subItem["update"] == 1 ? 'checked' : '')) . "</td>";
				}
				if (isset($CRUD['delete']) && $CRUD['delete'] == true) {
					echo "<td style='padding: 0; width: 60px; text-align: center;'>" . $this->Form->checkbox($subItemName . "_delete", array('checked' => $subItem["delete"] == 1 ? 'checked' : '')) . "</td>";
				}
				echo "</tr>";
			}
		}
		echo '</tbody>
            </table>
            ';
	}

	/**
	 *
	 * @param type $tabAcces
	 * @param type $label
	 */
	function printTabAcces($tabAcces, $label) {

		$idSuffix = str_replace(array(' ', '.'), '', microtime());
		$id = "tabACCES_" . $idSuffix;


		echo "<table id='" . $id . "'>";
		echo "<thead>";
		echo "<th  style='padding: 0; width: 60px; text-align: left;'>" . $label . "</th>";
		echo "<th  style='padding: 0; width: 60px; text-align: left;'>" . __d('default', 'right.acces') . "</th>";
		echo "</thead>";
		echo "<tbody>";
		foreach ($tabAcces as $name => $item) {
			echo "<tr onmouseover='$(this).addClass(\"alert alert-warning\");' onmouseout='$(this).removeClass(\"alert alert-warning\");'>";
			echo "<td>" . __d('fonctionnalites', $name) . "</td>";
			echo "<td style='padding: 0; width: 60px; text-align: left;'>" . $this->Form->checkbox($name, array('checked' => $item)) . "</td>";
			echo "</tr>";
		}
		echo "</tbody>";
		echo "</table>";
	}

}

?>
