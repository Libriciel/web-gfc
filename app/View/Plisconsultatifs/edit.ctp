<?php

/**
 *
 * Pliconsultatifs/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
            <h4 class="panel-title"><?php echo __d('ordreservice', 'Ordreservice.edit'); ?></h4>
        </div>
        <div class="panel-body">
            <?php
                $formPlisconsultatifs = array(
                    'name' => 'Pliconsultatif',
                    'label_w' => 'col-sm-4',
                    'input_w' => 'col-sm-5',
                    'input' => array(
                        'Pliconsultatif.id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden',
                                'value' => $pliconsultatifId
                            )
                        ),
                        'Pliconsultatif.origineflux_id' => array(
                            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.origineflux_id'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type' => 'select',
                                'empty' => true,
                                'options' => $origineflux
                            )
                        ),
                        'Pliconsultatif.numero' => array(
                            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.numero'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text',
                                'required'=>true,
                            )
                        ),
                        'Pliconsultatif.objet' => array(
                            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.objet'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Pliconsultatif.date' => array(
                            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.date'),
                            'inputType' => 'text',
                            'dateInput' =>true,
                            'items'=>array(
                                'type'=>'text',
                                'readonly' => true,,
                                'required'=>true,
                                'class' => 'datepicker',
                                'data-format'=>'dd/MM/yyyy'
                            )
                        ),
                        'Pliconsultatif.heure' => array(
                            'labelText' =>__d('pliconsultatif', 'Pliconsultatif.heure'),
                            'inputType' => 'time',
                            'items'=>array(
                                'type' => 'time',
                                'timeFormat' => '24',
                                'interval' => 05,
                                'empty' => true,
                                'required'=>true
                            )
                        ),
                        'Pliconsultatif.lot' => array(
                            'labelText' =>__d( 'pliconsultatif', 'Pliconsultatif.lot'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Pliconsultatif.consultation_id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden',
                                'value' => $consultationId
                            )
                        )
                    )
                );

                echo $this->Formulaire->createForm($formPlisconsultatifs);

                echo $this->Form->end();
            ?>
        </div>
        <div class="panel-footer controler " role="group">
            <a id="cancel" class="btn btn-danger-webgfc btn-inverse "><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
            <a id="valid" class="btn btn-success "><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $('.form_datetime input').datepicker({
            language: 'fr-FR',
            format: "dd/mm/yyyy",
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked'
        });
    });

    $('#PliconsultatifNumero').change(function () {
        var marcheid = "<?php echo $this->request->params['pass'][0];?>";
        gui.request({
            url: "/plisconsultatifs/ajaxformdata/numero/" + marcheid,
            data: $('#PliconsultatifEditForm').serialize()
        }, function (data) {
            processJsonFormCheck(data);
        });
    });

    $('#PliconsultatifOriginefluxId').select2({allowClear: true, placeholder: "Sélectionner un mode de réception"});
    $('#PliconsultatifHeureHour').select2({allowClear: true});
    $('#PliconsultatifHeureMin').select2({allowClear: true});

    $('#s2id_PliconsultatifHeureHour').css({
        width: '48%',
        float: 'left'
    });

    $('#s2id_PliconsultatifHeureMin').css({
        width: '48%',
        float: 'right'
    });

    $('#valid').click(function () {
        var form = $('#PliconsultatifEditForm');
        if (form_validate(form)) {
            gui.request({
                url: $('#PliconsultatifEditForm').attr('action'),
                data: $('#PliconsultatifEditForm').serialize()
            }, function (data) {
                loadValues();
                affichageTab();
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }

    });
    $('#cancel').click(function () {
        loadValues();
        affichageTab();
    });
</script>





