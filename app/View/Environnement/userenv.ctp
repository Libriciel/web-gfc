<?php

/**
 *
 * Environnement/userenv.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script>
	var supModalTitle = "<?php echo __d('default', 'Modal.suppressionTitle');?>";
	var supModalContent = "<?php echo __d('default', 'Modal.suppressionContents'); ?>";
	var btnConfirm = "<?php  echo __d('default', 'Button.delete');?>";
	var btnCancel = "<?php  echo __d('default', 'Button.cancel');?>";

	$('.btn-ds-webgfc').css('margin-left', '-320px');
	$('.btn-ds-webgfc').css('background-color', 'transparent');
	$('.btn-ds-webgfc').css('color', '#5397a7');
	$('.btn-ds-webgfc').css('border-color', '#3C7582');
	$('.btn-ds-webgfc').css('border', 'solid 1px');

	$('.btn-ds-webgfc').hover(function () {
		$('.btn-ds-webgfc').css('background-color', '#5397a7');
		$('.btn-ds-webgfc').css('color', 'white');
		$('.btn-ds-webgfc').css('border-color', '#5397a7');
	}, function () {
		$('.btn-ds-webgfc').css('background-color', 'transparent');
		$('.btn-ds-webgfc').css('color', '#5397a7');
		$('.btn-ds-webgfc').css('border-color', '#3C7582');
	});

	function messageHideShow() {
		$('#message_compos').toggle("slow");
	}
</script>
<?php
echo $this->Bannette->importScript();
?>
<div class="">
	<div class="row bannette-content">
		<div class="table-list">
			<div class="infoParentDiv"></div>
			<!-- nav de banette-->
			<ul class="nav nav-tabs titre" role="tablist">
				<?php
				$j = 1;
				foreach ($bannettes as $bannetteName => $bannetteContent) {
					$alias = Inflector::camelize("bannette_" . Inflector::camelize(Inflector::slug($bannetteName)));
					$path = "paging.{$alias}.count";
					$countFlux = Hash::get($this->request->params, $path);
					if (($tab == null && $j == 1) || ($tab != null && $bannetteName == $tab)) {
						echo ' <li class="active">'
								. '<a href="#' . $bannetteName . '" role="tab" data-toggle="tab">'
								. __d('bannette', 'Bannette.' . $bannetteName) . '   ' . $this->Html->tag('span', ' : ' . $countFlux)
								. '</a>'
								. '</li>';
					} else {
						echo ' <li class="">'
								. '<a href="#' . $bannetteName . '" role="tab" data-toggle="tab">'
								. __d('bannette', 'Bannette.' . $bannetteName) . '   ' . $this->Html->tag('span', ' : ' . $countFlux)
								. '</a>'
								. '</li>';
					}
					$j++;
				}
				?>
			</ul>
			<!-- fin de banette-->
			<!-- Tab panes -->
			<div class="tab-content container">
				<?php
				$i = 1;
				foreach ($bannettes

				as $bannetteName => $bannetteContent) {
				$bannetteOptions = array(
						'sortable' => false,
						'checkable' => true,
						'filter' => true,
						'translateBannetteName' => true,
						'date' => 'datereception'
				);
				?>

				<?php
				if (!$tab == null && $tab == $bannetteName){
				?>
				<div class="tab-pane fade active in" id="<?php echo $bannetteName; ?>">
					<?php
					echo $this->Bannette->bannettePaginator($bannetteName);
					?>
					<?php
					}else if ($i == 1 && $tab == null){
					?>
					<div class="tab-pane fade active in" id="<?php echo $bannetteName; ?>">
						<?php
						echo $this->Bannette->bannettePaginator($bannetteName);
						?>
						<?php
						}else{
						?>
						<div class="tab-pane fade" id="<?php echo $bannetteName; ?>">
							<?php
							echo $this->Bannette->bannettePaginator($bannetteName);
							?>

							<?php
							}
							?>

							<script type="text/javascript">
								var screenHeight = $(window).height() * 0.5;
							</script>
							<table id="Table_<?php echo $bannetteName; ?>"
								   data-toggle="table"
								   data-height=screenHeight
								   data-search="true"
								   data-locale="fr-CA"
								   data-show-refresh="false"
								   data-show-toggle="false"
								   data-show-columns="true"
								   data-toolbar="#toolbar">
								<!--					<thead style="position:sticky;top:0;background:white;border-left:1px solid #ddd;border-bottom:1px blue solid #ddd;">-->
								<thead>
								<tr>
									<?php
									echo $this->Bannette->bannetteHead($bannetteContent, $bannetteName, $bannetteOptions);
									?>
								</tr>
								</thead>
							</table>
						</div>
						<?php
						$i++;
						}
						?>
					</div>

				</div>
				<!-- FIN Tab panes -->
				<!-- button box -->
				<div id="bannetteControls" class="controls" role="group">

				</div>
				<!-- fin button box -->
			</div>

		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function () {
			//charger les contenus de tableaux
			var fluxChoix = [];
			var desktopChoix = [];
			<?php
			foreach ($bannettes as $bannetteName => $bannetteContent) {
			$bannetteOptions = array(
					'sortable' => false,
					'checkable' => true,
					'filter' => true,
					'translateBannetteName' => true,
					'date' => 'datereception'
			);
			if(!empty($bannetteContent)){
			?>
			var data = <?php echo $this->Bannette->bannetteTableTd($bannetteContent, $bannetteName, $bannetteOptions); ?>;
			$("#Table_<?php echo $bannetteName; ?>")
				.bootstrapTable('load', data)
				.on('all.bs.table', function (e, data) {
					$('.unread').each(function () {
						if ($(this).val() == '-1') {
							$(this).parents('tr').addClass('unread');
						}
					});
					$('.thName').find('i').each(function () {
						var fluxId = $(this).attr('id').substring(5);
						$(this).hover(function () {
							$('#flux_' + fluxId).show();
						}, function () {
							$('#flux_' + fluxId).hide();
						});
					});
				})
				.on('click-row.bs.table', function (e, row, element) {
					if ((!$(element.context).hasClass('thActions') && !$(element.context).hasClass('bs-checkbox')) || $(element.context).hasClass('thEdit') || $(element.context).hasClass('thView')) {
						var href = element.find('.thEdit').find('a').attr('href');
						if (href == undefined) {
							var href = element.find('.thView').find('a').attr('href');
						}
						window.location.href = href;
					}
				})
				.on('check.bs.table', function (e, row) {
					var fluxId = row['id'];
					fluxChoix.push(fluxId);
					var fluxChoixNew = fluxChoix;
					nbInCircuit(fluxChoixNew);

					isCopy(fluxChoixNew);

					var desktopId = row['desktopId'];
					desktopChoix.push(desktopId);
				})
				.on('uncheck.bs.table', function (e, row) {
					var fluxId = row['id'];
					fluxChoix = jQuery.grep(fluxChoix, function (value) {
						return value != fluxId;
					});
					var fluxChoixNew = fluxChoix;
					nbInCircuit(fluxChoixNew);
					isCopy(fluxChoixNew);

					var desktopId = row['desktopId'];
					desktopChoix = jQuery.grep(desktopChoix, function (valueDesktop) {
						return valueDesktop != desktopId;
					});
				})
				.on('check-all.bs.table', function (e, rows) {
					$.each(rows, function (i, row) {

						var fluxId = row['id'];
						if (jQuery.inArray(fluxId, fluxChoix)) {
							fluxChoix.push(fluxId);
						}
						var desktopId = row['desktopId'];
						if (jQuery.inArray(desktopId, desktopChoix)) {
							desktopChoix.push(desktopId);
						}
					});
					var fluxChoixNew = fluxChoix;
					nbInCircuit(fluxChoixNew);
					isCopy(fluxChoixNew);
				})
				.on('uncheck-all.bs.table', function (e, rows) {
					$.each(rows, function (i, row) {
						var fluxId = row['id'];
						fluxChoix = jQuery.grep(fluxChoix, function (value) {
							return value != fluxId;
						});

						var desktopId = row['desktopId'];
						desktopChoix = jQuery.grep(desktopChoix, function (valueDesktop) {
							return valueDesktop != desktopId;
						});
					});
					var fluxChoixNew = fluxChoix;
					nbInCircuit(fluxChoixNew);
					isCopy(fluxChoixNew);
				})
				.on('sort.bs.table', function (e, name, order) {
					var bannetteName = $(this).parents('.tab-pane.fade.active.in').attr('id');
					var nb = parseInt($('.nav.nav-tabs.titre li.active a span').html().substr(3));
					var colonne;
					switch (name) {
						case 'priorite':
							colonne = 'Courrier.priorite';
							break;
						case 'retard':
							colonne = 'Courrier.mail_retard_envoye';
							break;
						case 'reference':
							colonne = 'Courrier.reference';
							break;
						case 'contact':
							colonne = 'Organisme.name';
							break;
						case 'date':
							colonne = 'Courrier.datereception';
							break;
						case 'typesoustype':
							colonne = 'Type.name';
							break;
						case 'bureau':
							colonne = 'Desktop.name';
							break;
						case 'commentaire':
							colonne = 'Courrier.commentaire';
							break;
						case 'modified':
							colonne = 'Courrier.modified';
							break;
					}
					if (nb > 20) {
						gui.request({
							url: "/environnement/setSessionPagination/" + bannetteName + '/' + colonne,
							loader: true,
							loaderMessage: gui.loaderMessage,
							updateElement: $('.tab-content ')
						}, function (data) {
//                                if (data) {
//                                    window.location.href = window.location.href;
//                                }
						});
					}
				});

			$(window).resize(function () {
				$("#Table_<?php echo $bannetteName; ?>").bootstrapTable('resetView', {height: screenHeight});
			});

			$("#Table_<?php echo $bannetteName; ?>").bootstrapTable('resetView', {showHeader: false});
			<?php
			};
			};
			?>


			// On met en rouge les retards
			$("th.thRetard.thState").css('color', 'black');
			$("td.thRetard.thState").css('color', 'blue');
			var hasimg = $("td.thRetard.thState").find('img');
			if (hasimg) {
				$("td.thRetard.thState").find('img').parent('td').css('color', 'red');
			}


			changeTableBannetteHeight();
			$(window).resize(function () {
				changeTableBannetteHeight();
			});
			//if le total de flux est plus que 20, il est besoin de Pagination, il faut changement le url avec le nom de bannette
			$('.nav-tabs a').click(function () {
				if (parseInt($(this).find('span').text().replace(':', '')) > 20) {
					var bannette = $(this).attr('href').replace('#', '');
					$(this).removeAttr("href");
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/userenv/"; ?>" + bannette;
				}
			});
			$('.pagination').each(function () {
				var tab = $(this).attr('class').split('_')[0];
				$(this).insertAfter("#" + tab + " .bootstrap-table").show();
			});
			$('.pagination li').click(function (e) {
				if (!$(e.target).is('a')) {
					window.location.href = $('a', this).attr('href');
				}
			});
			<?php if (!empty($add)) { ?>
			var bannetteSelected = document.querySelector('#webgfc_content > div > div > div.table-list > ul > li.active > a');
			var hrefToLook = bannetteSelected.href;
			var bannetteType = hrefToLook.substr(hrefToLook.length - 5);
			if (bannetteType == '#docs' || bannetteType == '#refus' || "<?php echo Configure::read('Reaiguillage.affiche');?>") {
				// Ajout d'un bouton permettant le renvoi vers un aiguilleur ou un autre initiateur
				gui.addbutton({
					element: $('#bannetteControls'),
					button: {
						content: '<i class="fa fa-backward" aria-hidden="true" ></i>',
						title: ' Ré-aiguillage',
						action: function () {
							if (fluxChoix.length == 0) {
								swal({
									showCloseButton: true,
									title: 'Ré-aiguillage impossible',
									text: 'Veuillez choisir au moins un flux',
									type: 'error'
								});
							}
						}
					}
				});
			}
			if (bannetteType == '#copy') {
				gui.disablebutton({
					element: $('#bannetteControls'),
					button: '<i class="fa fa-backward" aria-hidden="true" ></i>',
				});
			}
			<?php } ?>
			// Traitement par lot
			gui.addbutton({
				element: $('#bannetteControls'),
				button: {
					content: '<i class="fa fa-th-large" aria-hidden="true" ></i>',
					title: '<?php echo __d("courrier", "Courrier.traitementlot"); ?>',
					action: function () {
						<?php if(!Configure::read('Conf.SAERP') && Configure::read('Traitementlot.Unread')):?>
						if ($('.selected').hasClass('unread')) {
							swal({
								showCloseButton: true,
								title: 'Oops...',
								html: '<p>Un flux parmi ceux sélectionnés n\'a pas été consulté. </p><p>Veuillez le consulter avant de pouvoir traiter les flux par lot</p>',
								type: 'error'
							});
						} else {
							<?php endif;?>
							if (fluxChoix.length == 0) {
								swal({
									showCloseButton: true,
									type: 'error',
									title: 'Traitement par lots',
									text: 'Veuillez choisir au moins un flux'
								})
							} else {
								var form = "<form id='formChecked'>";
								for (i in fluxChoix) {
									form += '<input type="hidden" name="data[checkItem][]" value="' + fluxChoix[i] + '">';
									form += '<input type="hidden" name="data[checkDesktop][]" value="' + desktopChoix[i] + '">';
								}
								form += "</form>";
								gui.formMessage({
									title: 'Traitement par lots',
									url: "/courriers/traitementlot",
									data: $(form).serialize(),
									loader: true,
									loaderMessage: gui.loaderMessage,
									updateElement: $('.table-list'),
									width: 800,
									buttons: {
										'<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
											$(this).parents(".modal").modal('hide');
											$(this).parents(".modal").empty();
										}
									}
								});
							}
							<?php if(!Configure::read('Conf.SAERP') && Configure::read('Traitementlot.Unread')):?>
						}
						<?php endif;?>
					}
				}
			});


			gui.disablebutton({
				element: $('#bannetteControls'),
				button: '<i class="fa fa-th-large" aria-hidden="true" ></i>',
			});

			// On clique sur la colonne Référence pour trier et réorganiser les colonnes (Ralenti quand même le traitement)
//            $("th.thRef span").click();

		});

		function changeTableBannetteHeight() {
			$(".fixed-table-container").css('height', $(window).height() * 0.59);
			$(".fixed-table-container").css('overflow', 'hidden');
		}


		gui.buttonbox({element: $("#bannetteControls")});
		gui.buttonbox({element: $("#notificationControls")});

		function nbInCircuit(fluxChoix) {
			var form = "<form id='formChecked'>";
			for (i in fluxChoix) {
				form += '<input type="hidden" name="data[InCircuit][]" value="' + fluxChoix[i] + '">';
			}
			form += "</form>";
			gui.request({
				url: '/Courriers/isInTraite',
				data: $(form).serialize(),
				loader: false,
			}, function (data) {
				if (data) {
					isButtonAiguillage(1);
				} else {
					isButtonAiguillage(0);
				}
			});
		}


		function isCopy(fluxChoix) {
			var form = "<form id='formChecked'>";
			for (i in fluxChoix) {
				form += '<input type="hidden" name="data[InCopy][]" value="' + fluxChoix[i] + '">';
			}
			form += "</form>";
			gui.request({
				url: '/Courriers/isInCopy',
				data: $(form).serialize(),
				loader: false,
			}, function (data) {
				if (data == '{"incopy":true}') {
					isButtonAiguillage(1);
					gui.disablebutton({
						element: $('#bannetteControls'),
						button: '<i class="fa fa-backward" aria-hidden="true" ></i>',
					});
				} else {
					isButtonAiguillage(0);
				}
			});
		}

		//new flux, rappelle Modal de formulaire(code dans menu.ctp) + AppController.php
		<?php
		if (!empty($add) && !empty($addservice)) {?>
		<!-- Ajout du bouton pour accéder à Démarches-Simplifiées -->
		<?php if (Configure::read('Webservice.DS') && $isDsActif):?>
		gui.addbutton({
			element: $('#bannetteControls'),
			button: {
				content: '<i class="fa fa-plus demarchessimplifiees" aria-hidden="true" ></i> <img src="/img/demarchessimplifiees16.png" style="height:16px;">',
				title: ' Créer depuis un dossier issu de Démarches Simplifiées',
				class: 'btn-ds-webgfc'
			}
		});

		$('.btn-ds-webgfc').css('margin-left', '-320px');
		$('.btn-ds-webgfc').css('background-color', 'transparent');
		$('.btn-ds-webgfc').css('color', '#5397a7');
		$('.btn-ds-webgfc').css('border-color', '#3C7582');
		$('.btn-ds-webgfc').css('border', 'solid 1px');
		$('.btn-ds-webgfc').hover(function () {
			$('.btn-ds-webgfc').css('background-color', '#5397a7');
			$('.btn-ds-webgfc').css('color', 'white');
			$('.btn-ds-webgfc').css('border-color', '#5397a7');
		}, function () {
			$('.btn-ds-webgfc').css('background-color', 'transparent');
			$('.btn-ds-webgfc').css('color', '#5397a7');
			$('.btn-ds-webgfc').css('border-color', '#3C7582');
		});
		$('.btn-ds-webgfc').css('height', '34px');


		<?php if( !empty($this->Session->read('Auth.DesktopmanagerInit')) ) :?>
		$('.btn-ds-webgfc').click(function () {
			window.location.href = "<?php echo Configure::read('BaseUrl') . "/demarchessimplifiees/index"; ?>";
		});
		<?php endif;?>

		<?php endif;?>

		<!-- Ajout du bouton pour accéderà Direct-Mairie -->
		<?php if (Configure::read('Webservice.Directmairie') && $isDmActif):?>
		gui.addbutton({
			element: $('#bannetteControls'),
			button: {
				content: '<i class="fa fa-plus directmairies" aria-hidden="true" ></i> <img src="/img/directmairie.svg" style="height:16px;">',
				title: ' Créer depuis un dossier issu de Direct-Mairie',
				class: 'btn-dm-webgfc'
			}
		});

		$('.btn-dm-webgfc').css('background-color', 'transparent');
		$('.btn-dm-webgfc').css('color', '#5397a7');
		$('.btn-dm-webgfc').css('border-color', '#3C7582');
		$('.btn-dm-webgfc').css('border', 'solid 1px');
		$('.btn-dm-webgfc').hover(function () {
			$('.btn-dm-webgfc').css('background-color', '#5397a7');
			$('.btn-dm-webgfc').css('color', 'white');
			$('.btn-dm-webgfc').css('border-color', '#5397a7');
		}, function () {
			$('.btn-dm-webgfc').css('background-color', 'transparent');
			$('.btn-dm-webgfc').css('color', '#5397a7');
			$('.btn-dm-webgfc').css('border-color', '#3C7582');
		});
		$('.btn-dm-webgfc').css('height', '34px');

		<?php if( !empty($this->Session->read('Auth.DesktopmanagerInit')) ) :?>
		$('.btn-dm-webgfc').click(function () {
			window.location.href = "<?php echo Configure::read('BaseUrl') . "/directmairies/index"; ?>";
		});
		<?php endif;?>
		<?php endif;?>


		gui.addbutton({
			element: $('#bannetteControls'),
			button: {
				content: '<i class="fa fa-plus" aria-hidden="true" ></i>',
				title: ' <?php echo __d("courrier", "New Courrier"); ?>',
				class: 'btn btn-info-webgfc',
				data_toggle: "modal",
				data_target: "#myModal"
			}
		});

		<?php } ?>

	</script>

	<?php

	if (!empty($this->Session->read('Auth.DesktopmanagerInit'))) {
		// l'agent connecé possèd eplusieurs rôles ou est lié )à plkusierus services
		// on affiche donc la modal de sélection du rôle et du service avant création d'un flux
	if ($displayForm) {
	if (!empty($add) && !empty($addservice)) {
		?>
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<form id="desktopCreateForm" name="desktopCreateForm" action="#" class="panel-body form-horizontal">
						<div class="modal-header">
							<button data-dismiss="modal" class="close">×</button>
							<h4 class="modal-title"><?php echo __d('courrier', 'New Courrier'); ?></h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="control-label col-sm-6"
									   for="desktopCreateFlux"><?php echo __d('desktop', 'Desktop.choice') ?></label>
								<div class="controls col-sm-5">
									<select class="selectpicker form-control" type="text" id="desktopCreateFlux"
											name="desktopCreateFlux" onchange="selectService(this)">
										<option class="void" value=""></option>
										<?php
										$addSelectRole = '';
										foreach ($add as $desktopId => $desktopName) {
											$addSelectRole .= '<option value="' . $desktopId . '">' . $desktopName . '</option>';
										}
										echo $addSelectRole;
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-6 " for="inputEmail">Veuillez sélectionner un
									service</label>
								<?php
								$formSend = array(
										'name' => 'Courrier',
										'label_w' => 'col-sm-6',
										'input_w' => 'col-sm-5',
										'input' => array(
												'serviceCreateFlux' => array(
													//                                            'labelText' =>__d('service', 'Soustype'),
														'inputType' => 'select',
														'items' => array(
																'type' => 'select',
																"options" => array(),
																'empty' => true
														)
												)
										)
								);
								echo $this->Formulaire->createForm($formSend);
								?>
							</div>
						</div>
						<div id="newFluxButton" class="modal-footer " role="group" style="width: 100%">
						</div>
					</form>
				</div>

			</div>
		</div>
		<script type="text/javascript">
			$('#desktopCreateFlux').select2({allowClear: true, placeholder: "Sélectionner un profil"});
			$('#CourrierServiceCreateFlux').select2({allowClear: true, placeholder: "Sélectionner un service"});
			gui.addbutton({
					element: $('#newFluxButton'),
					buttons: [
						{
							content: "<?php echo __d('default', 'Button.cancel'); ?>",
							class: 'btn btn-danger-webgfc btn-inverse ',
							data_dismiss: 'modal'
						},
						{
							content: "<?php echo __d('default', 'Button.submit'); ?>",
							class: 'btn btn-success btn-sencondary',
							action: function () {
								if ($('#desktopCreateFlux option:selected').val() != '' && $('#serviceCreateFlux option:selected').val() != '') {
									window.location.href = '<?php echo $this->Html->url(array('controller' => 'courriers', 'action' => 'add')); ?>/' + $('#desktopCreateFlux option:selected').val() + '/' + $('#CourrierServiceCreateFlux option:selected').val();
								} else {
									$("#desktopCreateForm label.ui-state-error").hide();
									$("#desktopCreateForm select").removeClass('alert alert-warning');
									// Pour la liste des bureaux
									if ($('#desktopCreateFlux option:selected').val() == '') {
										$("#desktopCreateFlux").addClass('alert alert-warning');
										$("#desktopCreateFlux").parent('div.input').find('label.ui-state-error').show();
									}

									// Pour la liste des services
									if ($('#CourrierServiceCreateFlux option:selected').val() == '') {
										$("#CourrierServiceCreateFlux").addClass('alert alert-warning');
										$("#CourrierServiceCreateFlux").parent('div.input').find('label.ui-state-error').show();
									}
								}
							}
						}
					]
				}
			);


			$('#desktopCreateFlux').change(function () {
				var desktop_id = $(this).val();
				var serviceLists = <?php echo json_encode($addservice); ?>;
				$('#CourrierServiceCreateFlux option').remove();

				// On est obligés de passer par un array car on ne peut trier les clés d'un objet
				var options = [];
				for (var value in serviceLists[desktop_id]) {
					options.push([value, serviceLists[desktop_id][value]]);
				}
				options.sort(function (a, b) {
					if (a[1] < b[1]) return -1;
					else if (a[1] > b[1]) return 1;
					return 0;
				});
				$.each(options, function (index, value) {
					$('#CourrierServiceCreateFlux').append($("<option value='" + value[0] + "'>" + value[1] + "</option>"));
				});
			});
		</script>
	<?php
	}
	}
	// sinon on accède directement au formulaire de création du flux
	else {
	?>
		<script type="text/javascript">
			$('.fa-plus').click(function () {
				window.location.href = "<?php echo Configure::read('BaseUrl') . "/courriers/add/$desktopId/$serviceId"; ?>";
			});
		</script>
	<?php
	}
	}
	else {
	?>
		<script type="text/javascript">
			$('#bannetteControls > a ').children('.fa-plus').parent().addClass('ui-state-disabled');
			$('.fa-plus').click(function () {
				swal({
					showCloseButton: true,
					title: 'Ajout impossible',
					text: 'Veuillez contacter votre administrateur, <br />aucun bureau initiateur trouvé',
					type: 'error'
				});
			});
			$('.btn-ds-webgfc').click(function () {
				swal({
					showCloseButton: true,
					title: 'Ajout impossible',
					text: 'Veuillez contacter votre administrateur, <br />aucun bureau initiateur trouvé',
					type: 'error'
				});
			});
			$('.btn-dm-webgfc').click(function () {
				swal({
					showCloseButton: true,
					title: 'Ajout impossible',
					text: 'Veuillez contacter votre administrateur, <br />aucun bureau initiateur trouvé',
					type: 'error'
				});
			});
		</script>
		<?php
	}
	?>

	<?php echo $this->Js->writeBuffer(); ?>


