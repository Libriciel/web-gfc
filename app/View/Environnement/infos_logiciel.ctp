<?php

/**
 *
 * Layouts/default.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 *
 *
 * PHP versions 7
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="">
    <div class="row">
        <div class="table-list" id="infos-logiciel" >
            <h3>A Propos de web-GFC</h3>
            <div class="content">
                <table data-toggle="table",
                       data-height="810">
                    <thead>
                        <tr>
                            <th>Outils</th>
                            <th  data-width = '400px'>Licence</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr class="outils-bootstrap">
                            <td>
                                <a href="http://getbootstrap.com/" target="view_window"><img src="/img/bootstrap3.png"  />
                                </a><a href="http://getbootstrap.com/" target="view_window"> Bootstrap 3</a>
                            </td>
                            <td><a href="https://opensource.org/licenses/mit-license.php" target="view_window" >Licence MIT</a></td>
                        </tr>
                        <tr class="outils-bootstrap">
                            <td>
                                <a href="https://github.com/eternicode/bootstrap-datepicker" target="view_window" >Bootstrap 3 Datepicker v4</a></td>
                            <td><a href="https://opensource.org/licenses/mit-license.php" target="view_window" >Licence MIT</a></td>
                        </tr>
                        <tr class="outils-bootstrap">
                            <td>
                                <a href="http://bootstrap-table.wenzhixin.net.cn/" target="view_window" ><img src="/img/bootstraptable-logo.png"  /></a>
                                <a href="http://bootstrap-table.wenzhixin.net.cn/" target="view_window" >Bootstrap Table v1.10.1</a>
                            </td>
                            <td><a href="https://opensource.org/licenses/mit-license.php" target="view_window" >Licence MIT</a></td>
                        </tr>
                        <tr class="outils-bootstrap">
                            <td>
                                <a href="http://1000hz.github.io/bootstrap-validator/" target="view_window" >Bootstrap Validator </a>
                            </td>
                            <td><a href="https://opensource.org/licenses/mit-license.php" target="view_window" >Licence MIT</a></td>
                        </tr>
                        <tr class="outils-cakephp">
                            <td>
                                <a href="http://cakephp.org/" target="view_window" ><img src="/img/logo-cake.png"  class="bg-cakephp"/> </a>
                                <a href="http://cakephp.org/" target="view_window" >2.9.0</a>
                            </td>
                            <td><a href="https://opensource.org/licenses/mit-license.php" target="view_window" >Licence MIT</a></td>
                        </tr>
                        <tr class="outils-css3">
                            <td>
                                <a href="http://www.w3schools.com/css/css3_intro.asp" target="view_window"><img src="/img/w3-logo.png"  /> </a>
                                <a href="http://www.w3schools.com/css/css3_intro.asp" target="view_window">CSS 3  </a>
                            </td>
                            <td><a href="https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document" target="view_window">Licence  </a></td>
                        </tr>
                        <tr class="outils-fontawesome">
                            <td>
                                <a href="http://fontawesome.io/" target="view_window"><img src="/img/font-awesome.png"  /></a>
                                <a href="http://fontawesome.io/" target="view_window">Font Awesome</a>
                            </td>
                            <td><a href ="http://scripts.sil.org/OFL" target="view_window" > Licence SIL OFL 1.1</a></td>

                        </tr>
                        <tr class="outils-flaticon">
                            <td>
                                <a href="http://www.flaticon.com/" target="view_window" ><img src="/img/flaticon-logo.svg"   class="bg-flaticon"> </a>
                                <a href="http://www.flaticon.com/" target="view_window" > designed by freepik  </a>
                            </td>
                            <td><a href="/img/license.pdf" target="view_window"  > Free Licence</a></td>
                        </tr>
                        <tr class="outils-jquery">
                            <td>
                                <a href="https://jquery.com/" target="view_window"><img src="/img/icon-jquery.png" /> </a>
                                <a href="https://jquery.com/" target="view_window">JQuery  v1.11.3 </a>
                            </td>
                            <td><a href="https://jquery.org/license/"> Licence</a></td>
                        </tr>
                        <tr class="outils-layer">
                            <td>
                                <a href="https://github.com/sentsin/layer/" target="view_window"> <img src="/img/layer-logo.png "  /> </a>
                                <a href="https://github.com/sentsin/layer/" target="view_window"> v2.3 </a>
                            </td>
                            <td><a href="https://github.com/sentsin/layer/blob/2.x/LICENSE"  target="view_window">GNU GENERAL PUBLIC LICENCE</a></td>
                        </tr>
                        <tr class="outils-php">
                            <td>
                                <a href="https://secure.php.net/downloads.php#v7.0.7" target="view_window"><img src="/img/php7.png"  /> </a>
                                <a href="https://secure.php.net/downloads.php#v7.0.7" target="view_window">php 7.0 </a>
                            </td>
                            <td><a href="http://www.php.net/license/3_01.txt"  target="view_window" >  Licence 3.01</a></td>
                        </tr>
                        <tr class="outils-postgresql">
                            <td>
                                <a href="https://www.postgresql.org/" target="view_window" ><img src="/img/postgresql.gif" /> </a>
                                <a href="https://www.postgresql.org/" target="view_window" >PostgreSQL  v9.2.16 </a>
                            </td>
                            <td><a href="https://opensource.org/licenses/PostgreSQL"> Licence</a></td>
                        </tr>
                        <tr class="outils-pdf">
                            <td>
                                <a href="https://mozilla.github.io/pdf.js/" target="view_window"><img src="/img/pdfjs-logo.svg "  /> </a>
                                <a href="https://mozilla.github.io/pdf.js/" target="view_window"> PDF.js </a>
                            </td>
                            <td><a href ="https://github.com/mozilla/pdf.js/blob/master/LICENSE" target="view_window"> Apache License</a></td>
                        </tr>
                        <tr class="outils-sass">
                            <td>
                                <a href="http://sass-lang.com/" target="view_window" ><img src="/img/sass.svg"  /> </a>
                                <a href="http://sass-lang.com/" target="view_window" >v3.4.22 </a>
                            </td>
                            <td><a href="https://opensource.org/licenses/mit-license.php" target="view_window" >Licence MIT</a></td>
                        </tr>
                        <tr class="outils-select2">
                            <td>
                                <a href="https://select2.github.io/" target="view_window"><img src="/img/select2-logo.png "  /></a>
                                <a href="https://select2.github.io/" target="view_window">Select2</a>
                            </td>
                            <td><a href="https://opensource.org/licenses/mit-license.php" target="view_window" >Licence MIT</a></td>
                        </tr>
                        <tr class="outils-sweetalert2">
                            <td>
                                <a href="https://limonte.github.io/sweetalert2/" target="view_window"> <img src="/img/sweet-alert-logo.png "  /> </a>
                                <a href="https://limonte.github.io/sweetalert2/" target="view_window"> v3.2.3</a>
                            </td>
                            <td><a href="https://opensource.org/licenses/mit-license.php" target="view_window" >Licence MIT</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
