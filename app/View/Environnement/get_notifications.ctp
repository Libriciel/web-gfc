<?php

/**
 *
 * Notifications/get_notifications.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<a class="notification_fullwrapper">
<?php
    $nbNotifications = count($notifications);
    if (!empty($notifications) && $nbNotifications > 0) {
        $span_notif = $this->Html->tag('div', $nbNotifications, array('class' => 'badge'));
        $span_notif .= $this->Html->tag('div','<i class="fa fa-bell" style="color:#fff;" aria-hidden="true"></i>', array('id' => 'viewNotifications', 'title' => __d('notification', 'Notification.new'),'escape' => false));
        echo $this->Html->tag('span', $span_notif, array('class' => 'notification_wrapper'));
?>
    <div id="divNotif" class="ui-widget ui-widget-content ui-corner-all notification_fullwrapper_div">
<?php
            if (!empty($notifications) && $nbNotifications > 0) {
                echo '<div class="arrow"></div>  ';
                echo '<div class="content_notification">';
                echo '<button type="button" class="btn btn-default" id="notifMarkAllReadBttn" title="'. __d('notification', 'Notification.mark_all_read').'" ><i style="color:#5397a7;cursor:pointer;" class="fa fa-check-square-o" aria-hidden="true"></i></button>';
                foreach ($notifications as $notification) {
?>
        <div class="notification_div" notification_id="<?php echo $notification['Notification']['id']; ?>">
            <h4 class="panel-title"><?php echo $notification['Notification']['name']; ?></h4>
            <p class="message"><span class="notification_title">Message: </span><?php echo $notification['Notification']['description']; ?></p>
            <p class="infos_sender"><span class="notification_title">de</span> : <?php echo $notification['Desktop']['name']; ?>
                <span class="notification_title">à</span> : <?php echo date_format(date_create($notification['Notification']['created']),'d/m/y H:i:s'); ?></p>
            <div class="notif_actions" id="chNotif">
                            <?php
//                            echo $this->Html->image("/img/eye.png", array( 'title' => 'Accéder au flux', 'class' => 'notifView', 'notificationId' => $notification['Notification']['id'], 'desktopId' => $notification['Notifieddesktop']['desktop_id'], 'fluxId' => $notification['Courrier']['id']));
                            echo $this->Html->tag('i', '', array('style' => 'margin-left:10px;color:#5397a7;cursor:pointer;',  'title' => 'Accéder au flux', 'class' => 'notifView fa fa-eye', 'notificationId' => $notification['Notification']['id'], 'desktopId' => $notification['Notifieddesktop']['desktop_id'], 'fluxId' => $notification['Courrier']['id']));
                            echo '&nbsp;';
//                            echo $this->Html->image("/img/valid_16.png", array('title' => 'Marquer la notification comme lue', 'class' => 'notifRead', 'notificationId' => $notification['Notification']['id'], 'desktopId' => $notification['Notifieddesktop']['desktop_id'], 'fluxId' => $notification['Courrier']['id']));
                            echo $this->Html->tag('i', '', array('style' => 'margin-left:10px;color:#5397a7;cursor:pointer;', 'title' => 'Marquer la notification comme lue', 'class' => 'notifRead fa fa-check', 'notificationId' => $notification['Notification']['id'], 'desktopId' => $notification['Notifieddesktop']['desktop_id'], 'fluxId' => $notification['Courrier']['id']));
                            ?>
            </div>
        </div>
<?php
                }
                echo '</div>';
                echo '</div>';
?>
        <script type="text/javascript">
<?php
            if (!CakeSession::read('Auth.User.notifyNotif')) {
?>
            layer.msg("<?php echo __d('notification', 'Notification.new'); ?>", {});
<?php
                CakeSession::write('Auth.User.notifyNotif', true);
            }
?>
        </script>
<?php
            } else {
?>
        <br />
        <div class="alert alert-warning"><?php echo __d('notification', 'Notification.void'); ?></div>
        <br />
<?php
            }
?>
        <div id="newNotif"></div>
        <script type="text/javascript">
            $('#notifMarkAllReadBttn').button().click(function () {
                gui.request({
                    url: '/environnement/setAllNotificationRead'
                }, function (data) {
                    getJsonResponse(data);
                    gui.request({
                        url: "/environnement/getNotifications",
                        updateElement: $('#nbNotifications')
                    });
                })
            });

$(".notifRead").css('cursor', 'pointer');
            $(".notifRead").button().click(function () {
                var notificationId = $(this).attr('notificationId');
                var desktopId = $(this).attr('desktopId');
                var fluxId = $(this).attr('fluxId');

                $('#nbNotifications').html("<img src='/img/ajax-loader-mini.gif' alt='' />");
                gui.request({
                    url: "/environnement/setNotificationRead/" + notificationId + "/" + desktopId + "/" + fluxId
                }, function () {
                    gui.request({
                        url: "/environnement/getNotifications",
                        updateElement: $('#nbNotifications')
                    });
                });
            });
$(".notifView").css('cursor', 'pointer');
            $(".notifView").button().click(function () {
                var desktopId = $(this).attr('desktopId');
                var fluxId = $(this).attr('fluxId');
                window.location.href = "/courriers/view/" + fluxId + "/" + desktopId;
            });
        </script>
    </div>
</a>
<?php
} else {
    echo '<i class="fa fa-bell" aria-hidden="true"></i>';
}
?>
