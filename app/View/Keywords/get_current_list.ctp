<?php

/**
 *
 * Keywords/get_current_list.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php

echo $form->input('info.keywordId', array(
	'label' => __('Choisissez le mot clé', true),
	'options' => $options));
?>
