<?php
/**
 *
 * Keywords/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<h1><?php echo __('Liste des mots-clés'); ?></h1>
<div class='Version'>
    <?php
    echo $form->create(array('controllers' => 'keywords', 'action' => "index/$keywordlist_id"));
    echo $form->input('versions', array('div' => false, 'selected' => $lastVersion));
    echo $form->submit('Afficher', array('div' => false));
    echo $form->end();
    echo $form->label('Version de travail');
    echo ('<ul><li>');
    if ($isWorkingVersion) {
        echo $html->link(__('Valider la version de travail', true), array('action' => 'tagVersion', $keywordlist_id), array(), 'Etes vous sur de vouloir valider la version de travail ?', false);
        echo ('</li><li>');
        echo $html->link(__('Supprimer une version de travail', true), array('action' => 'suppWorkVersion', $keywordlist_id), array(), 'Etes vous sur de vouloir supprimer la version de travail ?', false);
        echo ('</li>');
    } else {
        echo $html->link(__('Créer une version de travail', true), array('action' => 'createWorkVersion', $keywordlist_id), array(), 'Etes vous sur de vouloir créer une nouvelle version de travail ?', false);
    }
    echo ('</li></ul>');
    ?>
</div>
<?php echo $this->element('indexPageCourante'); ?>
<table cellpadding="0" cellspacing="0">
    <?php
    $listeColonnes = array();
    $listeColonnes[] = $paginator->sort(__('Code', true), 'code', array('url' => array($keywordlist_id)));
    $listeColonnes[] = $paginator->sort(__('Libellé', true), 'libelle', array('url' => array($keywordlist_id)));
    $listeColonnes[] = __('Actions', true);
    ?>
    <?php echo $html->tableHeaders($listeColonnes); ?>
    <?php foreach ($this->data as $rownum => $rowElement) : ?>
        <tr <?php echo ($rownum & 1) ? "" : " class='altrow'"; ?>>
            <td><?php echo $rowElement['Keyword']['code']; ?></td>
            <td><?php echo $rowElement['Keyword']['libelle']; ?></td>
            <td class='actions'>
                <?php
                if ($rowElement['Keyword']['version'] == 0) {
                    if ($rowElement['ListeActions']['edit'])
                        echo $this->element('boutonAction', array('url' => array('action' => 'edit', $rowElement['Keyword']['id'], $keywordlist_id)));
                    if ($rowElement['ListeActions']['delete'])
                        echo $this->element('boutonAction', array('url' => array('action' => 'delete', $rowElement['Keyword']['id'], $keywordlist_id), 'confirmMessage' => __('Voulez-vous supprimer le mot-clé : ', true) . $rowElement['Keyword']['libelle']));
                }
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<?php echo $this->element('indexPageNavigation', array('url' => array($keywordlist_id))); ?>

<div class="actions">
    <ul>
        <li>
            <?php
            if ($isWorkingVersion)
                echo $html->link(__('Ajouter un mot-clé', true), array('action' => 'add', $keywordlist_id), array(), false, false);
            ?>
        </li>
        <li>
            <?php
                echo $html->link(__('Retour à la liste des mots-clés', true), array('controller' => 'keywordlists', 'action' => 'index'), array(), false, false);
            ?>
        </li>
    </ul>
</div>
