<?php
	$this->Csv->preserveLeadingZerosInExcel = true;

    if( !empty($origine)) {
        $this->Csv->addRow( array( 'Nombre de flux possédant les métadonnées suivantes renseignées pour l\'année '.$annee.' dont l\'origine est "'.$origineName.'"' ));
    }
    else {
        $this->Csv->addRow( array( 'Nombre de flux possédant les métadonnées suivantes renseignées pour l\'année '.$annee ));
    }
    
    $this->Csv->addRow(
        array(
            '',
            'Total'
        )
	);
    ksort($results['Indicateurmeta']);
	foreach( array_keys( $results['Indicateurmeta'] ) as $indicateur ){

        $nomEtat = $indicateur;
        $val = Hash::get( $results, "Indicateurmeta.{$indicateur}" );
        
        $value = $this->Locale->number( $val );

            
        $this->Csv->addRow(
            array(
                $nomEtat,
                $value
            )
        );
	}

    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>