<?php

$this->Csv->preserveLeadingZerosInExcel = true;
    
    if( !empty($origine)) {
        $this->Csv->addRow( array( 'Taux de réponses closes pour l\'année '.$annee.' dont l\'origine est "'.$origineName.'"' ));
    }
    else {
        $this->Csv->addRow( array( 'Taux de réponses closes pour l\'année '.$annee ));
    }
    

    $this->Csv->addRow(
        array_merge(
            array(
                '',
                ''
            ),
            $mois,
            array('Total')
        )
        
	);
    
    $etats = array( 'Nb total de flux', 'En cours de traitement', 'Clos sans suite', 'Clos en attente d\'une réponse','Clos dans les délais', 'Courriers en retard');
    $etat = array( 'total', 'ect', 'ss', 'ra', 'delai', 'retard' );
	foreach( $results['service'] as $serviceName => $values ) {
        
        $this->Csv->addRow(
            array_merge(
                array($serviceName),
                array(),
                array(),
                array()

            )
        );
        foreach( $values as $i => $val) {
            foreach($etat as $et) {
                $valuesPrintable[$serviceName][$et][] = $this->Locale->number( $val["count_$et"] );
            }
        }

        foreach($etat as $et) {
            $values[$et] = '0';
            for( $i =0; $i <12; $i++) {
                $values[$et] += $results['service'][$serviceName][$i]["count_$et"];
            }
            $valueTotal[$et] = $values[$et];
            $nbTotal = Hash::get($results, 'Indicateurpourcent.nbtotal');
            $valueTotal[$et] = $this->Locale->number( $valueTotal[$et] ).' ('.$this->Locale->number( $valueTotal[$et] * 100 / $nbTotal, 2 ).'%)';
        }      

        foreach($etat as $et) {
            $this->Csv->addRow(
                array_merge(
                    array( '' ),
                    array(__d( 'statistique', $et )),
                    $valuesPrintable[$serviceName][$et],
                    array($valueTotal[$et])
                )
            );
        }
	}

    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>