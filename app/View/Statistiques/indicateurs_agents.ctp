<?php

/**
 *
 * Statistiques/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->css(array('SimpleComment.comment.css'), null, array('inline' => false));
echo $this->Html->script(array('jsapi'),array('inline'=>false));
echo $this->Html->script(array('google_charts_loader.js'),array('inline'=>false));
?>
<div class="container">
    <div id="page_content" class="row">
        <div id ="" class="table-list statistiques">
            <h3><?php echo __d('statistique','IndicateurAgent.title'); ?></h3>
            <div class="formule-recherche row">
    <?php
        $formStatistique = array(
            'name' => 'Statistique',
            'label_w' => 'col-sm-5',
            'input_w' => 'col-sm-3',
            'form_url' => array('controller' => 'statistiques', 'action' => 'indicateursAgents' ),
            'input' => array(
                'Statistique.annee' => array(
                    'labelText' =>__d('statistique','IndicateurAgent.annee'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'options' => array_combine( range( date( 'Y' ), 2012, -1 ), range( date( 'Y' ), 2012, -1 ) ),
                    )
                ),
                'Agent.Agent' => array(
                    'labelText' =>__d('statistique','IndicateurAgent.Agent'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'class' =>'JSelectMultiple',
                        'options' => $listagents,
                        'empty'=>true
                    )
                )
            )
        );
    echo $this->Formulaire->createForm($formStatistique);
    echo $this->Form->end();
    ?>
            </div>
            <div id="formule-recherche-controls">
                <a class="btn btn-info-webgfc" id="searchButton" title="<?php echo __d('statistique', 'btn.recherche'); ?>"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
            </div>


    <?php $annee = Hash::get( $this->request->data, 'Statistique.annee' );?>
    <?php if( isset($results)): ?>
            <div class="panel-body">
                <legend class="legend-resultat"><?php echo __d('statistique', 'IndicateurAgent.legend.Resultat').$annee;?></legend>
                <!-- Etat statistique du nombre de flux par agent -->
                <div id= "gauche" class="resultsTableAgentpanel col-sm-12">
                    <div class="panel-body  form-horizontal">

                <?php foreach( $results as $agentId => $valueSelected ) :?>
                <?php foreach( $valueSelected as $idSelected => $indicateurs):?>
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                                    <h4 class="panel-title"><?php echo isset( $agentName[$idSelected] ) ? $agentName[$idSelected] : 'Tout agent confondu';?></h4>
                                </div>
                                <div class="panel-body">

                                    <div id="myPieChart_<?php echo $idSelected ? $idSelected : 0 ;?>">
                                    </div>
                            <?php foreach( $indicateurs as $title => $value ):?>
                                    <div class="results control-group">
                                        <label class="control-label  col-sm-9"><?php echo __d( 'statistique', "Indicateurquant.$title" );?></label>
                                        <div class="controls  col-sm-3 statistiquesResultatNb">
                                            <span class="number">
                                        <?php
                                        if( is_null( $value ) ) {
                                            echo 'ND';
                                        }
                                        else {
                                            echo $this->Locale->number( $value );
                                        }
                                    ?>
                                            </span>
                                        </div>
                                    </div>
                            <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                  <?php endforeach;?>
                <?php endforeach;?>
                    </div>




                    <div class="panel-footer controls" style="min-height: 50px;">

                    </div>
                </div>
        <?php endif;?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#RechercheFormulaireForm select.JSelectMultiple').each(function () {
        setJselectNew($(this));
    });

<?php if( isset($results)): ?>
    $('.formule-recherche').hide();
    $('#formule-recherche-controls').html('<a class="btn btn-info-webgfc btn-inverse" id="reset" title="<?php echo __d('statistique', 'btn.reset'); ?>"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>');
    $('#reset').click(function () {
        location.reload();
    });
<?php endif ?>
    //chart pie
<?php if( isset($results)): ?>
    <?php if(!isset($alreadyLoad)):?>
    google.charts.load('41', {packages: ['corechart', 'bar']});
    <?php endif?>
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        <?php foreach( $results as $agentId => $valueSelected ) :?>
                <?php foreach( $valueSelected as $idSelected => $indicateurs):?>
        // Define the chart to be drawn.
        var data = google.visualization.arrayToDataTable([
            ['Variable de flux', 'Nombre', ],
            <?php foreach( $indicateurs as $title => $value ):?>
                <?php if($title!='total'): ?>
            ['<?php echo __d( 'statistique', "Indicateurquant.$title" );?>', parseInt("<?php
                                                                                                                            if( is_null( $value ) ) {
                                                                                                                                echo 0;
                                                                                                                            }
                                                                                                                            else {
                                                                                                                                echo $this->Locale->number( $value );
                                                                                                                            }
                                                                                                                        ?>")],
                <?php endif;?>
            <?php endforeach;?>
        ]);
        var options = {
            title: '',
            is3D: true,
        };

        var id = <?php echo $idSelected ? $idSelected : 0 ;?>
        // Instantiate and draw the chart.
        var chart = new google.visualization.PieChart(document.getElementById('myPieChart_' + id));
        chart.draw(data, options);
          <?php endforeach;?>
        <?php endforeach;?>
    }

<?php endif;?>

    $('#formulaireButton').button();
    $('#searchButton').button();

    $('#StatistiqueAnnee').select2({allowClear: true, placeholder: "Sélectionner une année"});


    $('#AgentAgent').select2({allowClear: true, placeholder: "Sélectionner un agent"});

    $('#StatistiqueIndicateursAgentsForm select.JSelectMultiple').each(function () {
        setJselectNew($(this));
    });


    $('#searchButton').click(function () {
        var dataSerialize = $("#StatistiqueIndicateursAgentsForm").serialize();
        if (!$('.legend-resultat').size() < 1) {
            dataSerialize += '&alreadyLoad=true';
        }
        gui.request({
            url: $("#StatistiqueIndicateursAgentsForm").attr('action'),
            data: dataSerialize,
            loader: true,
            updateElement: $('#page_content'),
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $('#page_content').html(data);
        });
    });

    // Ajout de l'action de recherche via la touche Entrée
    $('#StatistiqueIndicateursAgentsForm').keypress(function (e) {
        var dataSerialize = $("#StatistiqueIndicateursAgentsForm").serialize();
        if (!$('.legend-resultat').size() < 1) {
            dataSerialize += '&alreadyLoad=true';
        }
        if (e.keyCode == 13) {
            gui.request({
                url: $("#StatistiqueIndicateursAgentsForm").attr('action'),
                data: dataSerialize,
                loader: true,
                updateElement: $('#page-content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#page-content').html(data);
            });
        }
    });

    $('#StatistiqueIndicateursAgentsForm.folded').hide();

    $('#s2id_AgentAgent').css('width', '104%');

    // Bouton pour le tableau de nb de flux
    gui.addbutton({
        element: $('#gauche .panel-footer'),
        button: {
            content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
            title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
            class: 'btn btn-info-webgfc pull-right',
            action: function () {
                window.location.href = "<?php echo Router::url( array( 'controller' => 'statistiques', 'action' => 'exportagent' ) + Hash::flatten( $this->request->data, '__' ) );?>";
            }
        }

    });

    gui.disablebutton({
        element: $('#gauche'),
        button: '<i class="fa fa-download" aria-hidden="true"></i>'
    });


    <?php if( !empty( $results ) ) { ?>
    gui.enablebutton({
        element: $('#gauche'),
        button: '<i class="fa fa-download" aria-hidden="true"></i>',
        class: 'btn btn-info-webgfc pull-right',
        action: function () {
            window.location.href = "<?php echo Router::url( array( 'controller' => 'statistiques', 'action' => 'exportagent' ) + Hash::flatten( $this->request->data, '__' ) );?>";
        }
    });
        <?php } ?>
</script>

<script type="text/javascript">
    /*
     <?php if( !empty($this->request->data['Agent']['Agent']) ):?>
     $('#AgentAgent').parent().append('<ul id="AgentAgentList" class="jSelectList">');
     <?php foreach( $this->request->data['Agent']['Agent'] as $i => $agentId ):?>


     $('.duplicatedId<?php echo $agentId;?> span.delBttn').click(function () {
     delItem($(this));
     });

     <?php endforeach;?>
     $('#AgentAgent').parent().append('</ul>');
     $('#AgentAgent').parent().append('<a class="delAllBttn btn btn-info-webgfc" listid="AgentAgentList" role="button">Tout enlever</a>');




     $('.delAllBttn').click(function () {
     $('#AgentAgentList').text('');
     $('#s2id_AgentAgent .select2-search-choice').each(function () {
     $(this).remove();
     });
     $('#AgentAgent option:selected').each(function () {
     $(this).prop("selected", false);
     });
     $(this).remove();

     });
     <?php endif;?>
     */


</script>
