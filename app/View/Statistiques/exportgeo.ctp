<?php
	$this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow(
        array(
            'Nombre de flux par commune pour la commune '.$nameCommune.' pour l\'année '.$annee
        )
    );
    $this->Csv->addRow(
        array(
            'Adresse',
            'Nombre de flux'
        )
	);
    
    if( $resultatnonvide == '1' ) {
        foreach( $results['Indicateurgeo']['adresse']['nonvide'] as $j => $stat ){
            $nom = Hash::get( $stat, "Statistique.name" );
            $val = Hash::get( $stat, "Statistique.count" );
            
            $value = $this->Locale->number( $val );

            $this->Csv->addRow(
                array(
                    $nom,
                    $value
                )
            );
        }
    }
    else {
        foreach( $results['Indicateurgeo']['adresse'] as $j => $stat ){
            $nom = Hash::get( $stat, "Statistique.name" );
            $val = Hash::get( $stat, "Statistique.count" );
            
            $value = $this->Locale->number( $val );

            $this->Csv->addRow(
                array(
                    $nom,
                    $value
                )
            );
        }
    }

    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>