<?php

/**
 *
 * Statistiques/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->css(array('SimpleComment.comment.css'), null, array('inline' => false));
echo $this->Html->script(array('jsapi'),array('inline'=>false));
echo $this->Html->script(array('google_charts_loader.js'),array('inline'=>false));
?>
<div class="container">
    <div id="content_page" class="row">
        <div id ="" class="table-list statistiques">

            <h3><?php echo __d('statistique', 'Indicateurgeo.title'); ?></h3>
            <!--formulaire recherche-->
            <div class="formule-recherche  form-horizontal" id="searchIndicateurGeo">
            <?php
            $valueDept = isset($this->request->data['Statistique']['dept']) ? $this->request->data['Statistique']['dept'] : null;
            $valueCommune = isset($this->request->data['Statistique']['commune']) ? $this->request->data['Statistique']['commune'] : null;

            $formStatistique = array(
                'name' => 'Statistique',
                'label_w' => 'col-sm-5',
                'input_w' => 'col-sm-3',
				'form_url' => array('controller' => 'statistiques', 'action' => 'indicateursGeo' ),
                'input' => array(
                    'Statistique.annee' => array(
                        'labelText' =>__d('statistique', 'Indicateurgeo.annee'),
                        'inputType' => 'select',
                        'items'=>array(
                            'options' => array_combine( range( date( 'Y' ), 2012, -1 ), range( date( 'Y' ), 2012, -1 ) ),
                            'type'=>'select'
                        )
                    ),
                    'Statistique.dept' => array(
                        'labelText' =>__d('recherche', 'Recherche.contactdepartement'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'options' => array(),
                            'required'=>true
                        )
                    ),
                    'Statistique.commune' => array(
                        'labelText' =>__d('recherche', 'Recherche.contactcommune'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'options' => array(),
                            'required' => true
                        )
                    ),
                    'Statistique.adresse' => array(
                        'labelText' =>__d('statistique', 'Statistique.adresse'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'options' => array(),
//                            'required'=>true,
                            'multiple' => true
                        )
                    ),
                    'Statistique.origineflux_id' => array(
                        'labelText' =>__d('statistique', 'Indicateurgeo.origineflux_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'options' => $origines,
                            'empty' => true
                        )
                    ),
                    'Statistique.resultatNonvide' => array(
                        'labelText' =>__d('statistique', 'Statistique.resultatNonvide'),
                        'inputType' => 'checkbox',
                        'items'=>array(
                            'type'=>'checkbox'
                        )
                    )
                )
            );
            /*if( Configure::read('DOC_DEFAULT') ) {
                if( !empty($metas) ){
                    foreach( $metas as $meta ) {
                        $formStatistique['input']["Metadonnee.{$meta['Metadonnee']['id']}"] = array(
                            'labelText' =>$meta['Metadonnee']['name'],
                            'inputType' => 'checkbox',
                            'items'=>array(
                                'type'=>'checkbox'
                            )
                        );
                    }
                }
            }*/
            echo $this->Formulaire->createForm($formStatistique);
            echo $this->Form->end();
            ?>
            </div>
            <div id="formule-recherche-controls">
                <a class="btn btn-info-webgfc" id="searchButton" title="<?php echo __d('statistique', 'btn.recherche'); ?>"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
            </div>

            <!-- zone resultat -->
        <?php $annee = Hash::get( $this->request->data, 'Statistique.annee' );?>
        <?php if( isset($results)): ?>
        <?php $valueTotal = ' - (Total = ' .Hash::get( $results, "Indicateurgeo.total" ).' ) '; ?>
        <?php
            $origine = '';
            if(!empty($origineSelected)) {
                $origine = " - dont l'origine est ".'"'.Hash::get( $origines, $origineSelected ).'"';
            }
        ?>
            <legend class="legend-resultat" style="margin-left:200px; width: 70%"><?php echo __d('statistique', 'Indicateurgeo.legend.Resultat') . $annee .$valueTotal.$origine; ?></legend>
            <div id="mygeoChart">

            </div>
            <div id= "gauche" class="resultsTableGeo" style="padding-bottom: 50px;">
                <table class="resultsGeo" data-toggle='table' data-height ='599' data-show-refresh= 'true' data-show-toggle = 'true'
                       data-show-columns="true"
                       data-search="true"
                       data-locale = "fr-CA"
                       data-select-item-name="toolbar1">
                    <thead>
                        <tr class="results bold">
                            <th><?php echo __d('statistique', 'Indicateurgeo.Resultat.commune'); ?></th>
                            <th><?php echo __d('statistique', 'Indicateurgeo.Resultat.nb'); ?></th>
                            <th class="number bold"><?php echo __d('statistique', 'Indicateurgeo.Resultat.total'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- Données pour la commune (au sens global)-->
                    <?php foreach( $results['Indicateurgeo']['commune'] as $i => $commune):?>
                        <tr class="resultscommune">
                            <td> <?php echo $commune;?></td>
                            <td>
                            <?php
                                $value = Hash::get( $results, "Indicateurgeo.{$commune}" );
                                if( is_null( $value ) ) {
                                    echo 'ND';
                                }
                                else {
                                    echo $this->Locale->number( $value );
                                }
                            ?>
                            </td>
                            <td class="bold">
                            <?php
                                $valueTotal = Hash::get( $results, "Indicateurgeo.total" );
                                if( $valueTotal == 0 ) {
                                    echo $this->Locale->number( 0 ).'%';
                                }
                                else {
                                    if( is_null( $value ) ) {
                                        echo 'ND';
                                    }
                                    else {
                                        echo $this->Locale->number( $value * 100 / $valueTotal ).'%';
                                    }
                                }
                            ?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                        <!-- Données par adresses de la commune quelque soit le nombre de flux -->
                <?php if( $this->request->data['Statistique']['resultatNonvide'] != '1' && !isset( $results['Indicateurgeo']['adresse']['selected'] )) :?>
                    <?php foreach( $results['Indicateurgeo']['adresse'] as $j => $stat):?>
                        <tr class="resultsadresse">
                            <td class="resultsadresse"> <?php echo $stat['Statistique']['name'];?></td>
                            <td class="resultsadresse">
                            <?php
                                $value = Hash::get( $results, "Indicateurgeo.{$commune}" );
                                if( is_null( $value ) ) {
                                    echo 'ND';
                                }
                                else {
                                    echo $this->Locale->number( $stat['Statistique']['count'] );
                                }
                            ?>
                            </td>
                            <td class="bold resultsadresse">
                            <?php
                                $valueTotal = Hash::get( $results, "Indicateurgeo.total" );
                                if( $valueTotal == 0 ) {
                                    echo $this->Locale->number( 0 ).'%';
                                }
                                else {
                                    if( is_null( $value ) ) {
                                        echo 'ND';
                                    }
                                    else {
                                        echo $this->Locale->number( $stat['Statistique']['count'] * 100 / $valueTotal ).'%';
                                    }
                                }
                            ?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                        <?php else:?>
                        <!-- Données par adresses de la commune uniquement pour les rues possédant un flux -->
                        <?php if( isset( $results['Indicateurgeo']['adresse']['nonvide'] ) && !isset( $results['Indicateurgeo']['adresse']['selected'] )):?>
                            <?php foreach( $results['Indicateurgeo']['adresse']['nonvide'] as $k => $stat):?>
                        <tr class="resultsadresse">
                            <td class="resultsadresse"> <?php echo $stat['Statistique']['name'];?></td>
                            <td class="resultsadresse">
                            <?php
                                $value = Hash::get( $results, "Indicateurgeo.{$commune}" );
                                if( is_null( $value ) ) {
                                    echo 'ND';
                                }
                                else {
                                    echo $this->Locale->number( $stat['Statistique']['count'] );
                                }
                            ?>
                            </td>
                            <td class="bold resultsadresse">
                            <?php
                                $valueTotal = Hash::get( $results, "Indicateurgeo.total" );
                                if( $valueTotal == 0 ) {
                                    echo $this->Locale->number( 0 ).'%';
                                }
                                else {
                                    if( is_null( $value ) ) {
                                        echo 'ND';
                                    }
                                    else {
                                        echo $this->Locale->number( $stat['Statistique']['count'] * 100 / $valueTotal ).'%';
                                    }
                                }
                            ?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        <?php endif;?>

                <?php if( isset( $results['Indicateurgeo']['adresse']['selected'] )):?>
                    <?php foreach( $results['Indicateurgeo']['adresse']['selected'] as $k => $stat):?>
                        <tr class="resultsadresse">
                            <td class="resultsadresse"> <?php echo $stat['Statistique']['name'];?></td>
                            <td class="resultsadresse">
                            <?php
                                $value = Hash::get( $results, "Indicateurgeo.{$commune}" );
                                if( is_null( $value ) ) {
                                    echo 'ND';
                                }
                                else {
                                    echo $this->Locale->number( $stat['Statistique']['count'] );
                                }
                            ?>
                            </td>
                            <td class="bold resultsadresse">
                            <?php
                                $valueTotal = Hash::get( $results, "Indicateurgeo.total" );
                                if( $valueTotal == 0 ) {
                                    echo $this->Locale->number( 0 ).'%';
                                }
                                else {
                                    if( is_null( $value ) ) {
                                        echo 'ND';
                                    }
                                    else {
                                        echo $this->Locale->number( $stat['Statistique']['count'] * 100 / $valueTotal ).'%';
                                    }
                                }
                            ?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                <?php endif;?>
            <?php endif;?>
                    <?php if( !isset( $results['Indicateurgeo']['adresse']['selected'] )):?>
                        <tr class="resultsadresse">
                            <td class="resultsadresse">  <?php echo 'SANS ADRESSE';?></td>
                            <td class="resultsadresse">
                                <?php
                                    $valueSansAdresse = Hash::get( $results, "Indicateurgeo.sansadresse" );
                                    if( is_null( $valueSansAdresse ) ) {
                                        echo 'ND';
                                    }
                                    else {
                                        echo $this->Locale->number( $valueSansAdresse );
                                    }
                                ?>
                            </td>
                            <td class="bold resultsadresse"><?php
                                if( $valueTotal == 0 ) {
                                    echo $this->Locale->number( 0 ).'%';
                                }
                                else {
                                    if( is_null( $valueSansAdresse ) ) {
                                        echo 'ND';
                                    }
                                    else {
                                        echo $this->Locale->number( $valueSansAdresse * 100 / $valueTotal, 2 ).'%';
                                    }
                                }
                            ?></td>
                        </tr>
                       <?php endif;?>
                    </tbody>
                </table>
            </div>
        <?php endif;?>
        </div>
    </div>
</div>
<!-- Affichage des états des flux-->


<script type="text/javascript">
    var end_formgroup = $('#StatistiqueResultatNonvide').parents('.form-group ');
    $("<legend style='margin-left:200px; width: 70%'>Métadonnées</legend>").insertAfter(end_formgroup);
    $('.test').bootstrapTable({});

    $('#formulaireButton').button();
    $('#searchButton').button();

    $('#StatistiqueAnnee').select2({allowClear: true, placeholder: "Sélectionner une année"});

    /*Javascript dédié aux informations de départment et communes*/
    $('#StatistiqueDept').select2({allowClear: true, placeholder: "Sélectionner un département"});
    $('#StatistiqueCommune').select2({allowClear: true, placeholder: "Sélectionner une commune"});
    $('#StatistiqueAdresse').select2({allowClear: true, placeholder: "Sélectionner une adresse"});

    $('#StatistiqueOriginefluxId').select2({allowClear: true, placeholder: "Sélectionner une origine"});

    <?php if( isset($results)): ?>
    $('.formule-recherche').hide();
    $('#formule-recherche-controls').html('<a class="btn btn-info-webgfc btn-inverse" id="reset" ><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>');
    $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyCxUs1S5ixuKPIvolDjdXv35dw3s3UyLjM&callback=drawMap");
    $('#reset').click(function () {
        location.reload();
    });
    <?php endif ?>

    //**************************************************************//
    //statisitique

     <?php if( isset($results)): ?>
     <?php if(!isset($alreadyLoad)):?>
    google.charts.load('current', {'packages': ['map']});
     <?php endif?>

    google.charts.setOnLoadCallback(drawMap);

    function drawMap() {
        $('#mygeoChart').html();
        var data = google.visualization.arrayToDataTable([
            ['Lat', 'Long', 'Name'],
            <?php
            foreach( $results['Indicateurgeo']['adresse'] as $j => $stat){
                $value = Hash::get( $results, "Indicateurgeo.{$commune}" );
                if( is_null( $value ) || $value == 0 ) {
                    $nbFlux =  0;
                }
                else {
                    if(isset($stat['Statistique']['count'])){
                        $nbFlux = (int)$this->Locale->number( $stat['Statistique']['count'] );
                    }
                }
                if($nbFlux){
                    echo'['. $stat['Statistique']['lat'].','. $stat['Statistique']['lon'].',"Nombre de flux : '.$nbFlux.'"],';
                }
            }
            ?>
        ]);

        var options = {
//            mapType: 'styledMap',
            showTip: true,
            useMapTypeControl: true,
            icons: {
                default: {
                    normal: '/img/Map-Marker-Ball-Azure-ico.png',
                    selected: '/img/Map-Marker-Ball-Right-Azure-icon.png'
                }
            },
//            mapTypeIds: ['styledMap', 'redEverything', 'imBlue'],
            maps: {
                styledMap: {
                    name: 'Styled Map',
                    styles: [
                        {featureType: 'poi.attraction',
                            stylers: [{color: '#fce8b2'}]},
                        {featureType: 'road.highway',
                            stylers: [{hue: '#0277bd'}, {saturation: -50}]},
                        {featureType: 'road.highway', elementType: 'labels.icon',
                            stylers: [{hue: '#000'}, {saturation: 100}, {lightness: 50}]},
                        {featureType: 'landscape',
                            stylers: [{hue: '#259b24'}, {saturation: 10}, {lightness: -22}]}
                    ]},
                redEverything: {
                    name: 'Redden All The Things',
                    styles: [
                        {featureType: 'landscape',
                            stylers: [{color: '#fde0dd'}]},
                        {featureType: 'road.highway',
                            stylers: [{color: '#67000d'}]},
                        {featureType: 'road.highway', elementType: 'labels',
                            stylers: [{visibility: 'off'}]},
                        {featureType: 'poi',
                            stylers: [{hue: '#ff0000'}, {saturation: 50}, {lightness: 0}]},
                        {featureType: 'water',
                            stylers: [{color: '#67000d'}]},
                        {featureType: 'transit.station.airport',
                            stylers: [{color: '#ff0000'}, {saturation: 50}, {lightness: -50}]}
                    ]},
                imBlue: {
                    name: 'All Your Blues are Belong to Us',
                    styles: [
                        {featureType: 'landscape',
                            stylers: [{color: '#c5cae9'}]},
                        {featureType: 'road.highway',
                            stylers: [{color: '#023858'}]},
                        {featureType: 'road.highway', elementType: 'labels',
                            stylers: [{visibility: 'off'}]},
                        {featureType: 'poi',
                            stylers: [{hue: '#0000ff'}, {saturation: 50}, {lightness: 0}]},
                        {featureType: 'water',
                            stylers: [{color: '#0288d1'}]},
                        {featureType: 'transit.station.airport',
                            stylers: [{color: '#0000ff'}, {saturation: 50}, {lightness: -50}]}
                    ]}
            }
        };

        var map = new google.visualization.Map(document.getElementById('mygeoChart'));

        if (!jQuery.isEmptyObject(data['Lf'])) {

            map.draw(data, options);
        }
    }
     <?php endif ?>

    //*******************************************************************************//


    var bans = [];
        <?php foreach ($bans as $ban) { ?>
    var ban = {
        id: "<?php echo $ban['Ban']['id']; ?>",
        name: "<?php echo $ban['Ban']['name']; ?>",
        banscommunes: []
    };
                <?php foreach ($ban['Bancommune'] as $bancommune) { ?>
    var bancommune = {
        id: "<?php echo $bancommune['id']; ?>",
        name: "<?php echo $bancommune['name']; ?>",
    };
    ban.banscommunes.push(bancommune);
                <?php } ?>
    bans.push(ban);
        <?php } ?>

    function fillBanscommunes(ban_id, bancommune_id) {
        $("#StatistiqueCommune").empty();
        $("#StatistiqueCommune").append($("<option value=''> --- </option>"));

        for (i in bans) {
            if (bans[i].id == ban_id) {
                for (j in bans[i].banscommunes) {
                    if (bans[i].banscommunes[j].id == bancommune_id) {
                        $("#StatistiqueCommune").append($("<option value='" + bans[i].banscommunes[j].id + "' selected='selected'>" + bans[i].banscommunes[j].name + "</option>"));
                    } else {

                        $("#StatistiqueCommune").append($("<option value='" + bans[i].banscommunes[j].id + "'>" + bans[i].banscommunes[j].name + "</option>"));
                    }
                }
            }
        }
    }


    $("#StatistiqueDept").append($("<option value=''> --- </option>"));
    //remplissage de la liste des bans
    for (i in bans) {
        $("#StatistiqueDept").append($("<option value='" + bans[i].id + "'>" + bans[i].name + "</option>"));
    }
    //definition de l action sur bancommune
    $("#StatistiqueDept").bind('change', function () {
        var ban_id = $('option:selected', this).attr('value');
        fillBanscommunes(ban_id);
    });


    var checkAdresses = function (bancommune_id) {
        $("#StatistiqueAdresse").empty();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Statistiques/getAdresses/' + bancommune_id,
            data: $('#StatistiqueIndicateursGeoForm').serialize(),
            success: function (data) {
                if (data.length > 0) {
                    $("#StatistiqueAdresse").append($("<option value=''> --- </option>"));
                    // remplissage de la partie adresse
                    for (var i in data) {
                        $("#StatistiqueAdresse").append($("<option value='" + data[i].Banadresse.nom_afnor + "'>" + data[i].Banadresse.nom_afnor + "</option>"));
                    }
                }
            }
        });
    }

    $("#StatistiqueCommune").bind('change', function () {
        var bancommune_id = $('option:selected', this).attr('value');
        checkAdresses(bancommune_id);
    });


    // Une fois la recherche lancée, on repeuple les listes déroulantes avec les valeurs sélectionnées
    <?php if( !empty($this->request->data['Statistique']['dept'])) :?>
    $("#StatistiqueDept").select2('val', "<?php echo $this->request->data['Statistique']['dept']; ?>");
    $("#StatistiqueDept").trigger('change');

    $("#StatistiqueCommune").select2('val', "<?php echo $this->request->data['Statistique']['commune']; ?>");
    $("#StatistiqueCommune").trigger('change');

        <?php if( !empty($this->request->data['Statistique']['adresse']) && isset( $this->request->data['Statistique']['adresse'])):?>
    var adresses = [];
            <?php foreach($this->request->data['Statistique']['adresse'] as $p => $ad):?>
    adresse = {
        id: "<?php echo $p;?>",
        text: "<?php echo $ad;?>"
    }
    adresses.push(adresse);
            <?php endforeach;?>
    $("#StatistiqueAdresse").select2('data', adresses); // GOOD
        <?php endif;?>
    <?php endif;?>

    /**
     * Bouton pour la recherche
     * @param {type} param
     */
    $('#searchButton').click(function () {
        if (form_validate($("#StatistiqueIndicateursGeoForm"))) {
            var dataSerialize = $("#StatistiqueIndicateursGeoForm").serialize();
            if (!$('.legend-resultat').size() < 1) {
                dataSerialize += '&alreadyLoad=true';
            }
            gui.request({
                url: $("#StatistiqueIndicateursGeoForm").attr('action'),
                data: dataSerialize,
                loader: true,
                updateElement: $('.table-list'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#content_page').empty();
                $('#content_page').html(data);
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }
    });

    // Ajout de l'action de recherche via la touche Entrée
    $('#searchIndicateurGeo').keypress(function (e) {
        if (e.keyCode == 13) {
            if (form_validate($("#StatistiqueIndicateursGeoForm"))) {
                var dataSerialize = $("#StatistiqueIndicateursGeoForm").serialize();
                if (!$('.legend-resultat').size() < 1) {
                    dataSerialize += '&alreadyLoad=true';
                }
                gui.request({
                    url: $("#StatistiqueIndicateursGeoForm").attr('action'),
                    data: dataSerialize,
                    loader: true,
                    updateElement: $('#page-content'),
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    $('#page-content').html(data);
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Oops...",
                    text: "Veuillez vérifier votre formulaire!",
                    type: "error",

                });
            }
        }
    });

//    $('#StatistiqueIndicateursGeoForm.folded').hide();

    /**
     * Bouton pour l'export des résultats
     */
    gui.addbutton({
        element: $('#content_page .resultsTableGeo'),
        button: {
            content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
            title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
            class: 'btn btn-info-webgfc pull-right',
            action: function () {
                window.location.href = "<?php echo Router::url( array( 'controller' => 'statistiques', 'action' => 'exportgeo' ) + Hash::flatten( $this->request->data, '__' ) );?>";
            }
        }

    });

    gui.disablebutton({
        element: $('#content_page .resultsTableGeo'),
        button: '<i class="fa fa-download" aria-hidden="true"></i>'
    });


    <?php if( !empty( $results ) ) { ?>
    $('.resultsGeo').bootstrapTable({});
    gui.enablebutton({
        element: $('#content_page .resultsTableGeo'),
        button: '<i class="fa fa-download" aria-hidden="true"></i>',
        class: 'btn btn-info-webgfc pull-right',
        action: function () {
            window.location.href = "<?php echo Router::url( array( 'controller' => 'statistiques', 'action' => 'exportgeo' ) + Hash::flatten( $this->request->data, '__' ) );?>";
        }
    });
        <?php } ?>


</script>
