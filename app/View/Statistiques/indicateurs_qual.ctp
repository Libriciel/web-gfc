<?php

/**
 *
 * Statistiques/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->css(array('SimpleComment.comment.css'), null, array('inline' => false));
echo $this->Html->script(array('jsapi'),array('inline'=>false));
echo $this->Html->script(array('google_charts_loader.js'),array('inline'=>false));
?>
<div class="container">
    <div id="content_page" class="row">
        <div  id ="" class="table-list statistiques">
            <h3><?php echo __d('statistique','Indicateurqual.title'); ?></h3>
            <div class="formule-recherche  panel-body" id="searchIndicateurQual">
            <?php
            $formStatistique = array(
                'name' => 'Statistique',
                'label_w' => 'col-sm-5',
                'input_w' => 'col-sm-3',
				'form_url' => array('controller' => 'statistiques', 'action' => 'indicateursQual' ),
                'input' => array(
                    'Statistique.annee' => array(
                        'labelText' =>__d('statistique', 'Indicateurqual.annee'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'options' => array_combine( range( date( 'Y' ), 2012, -1 ), range( date( 'Y' ), 2012, -1 ) ),
                        )
                    ),
                    'Statistique.origineflux_id' => array(
                        'labelText' =>__d('statistique', 'Indicateurqual.origineflux_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'class' => 'JSelectMultiple',
                            'options' => $origines,
                            'empty' => true
                        )
                    )
                )
            );
            if(Configure::read('CD') == 81 ) {
                $formStatistique['input']['Statistique.service'] = array(
                    'labelText' =>__d('statistique', 'Indicateurqual.service'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'multiple' => true,
//                        'class' => 'JSelectMultiple',
                        'options' => $services,
                        'required' => true
                    )
                );
            }
            else {
                $formStatistique['input']['Statistique.service'] = array(
                    'labelText' =>__d('statistique', 'Indicateurqual.service'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'multiple' => true,
                        'options' => $services,
						'required' => true
                    )
                );
            }
            echo $this->Formulaire->createForm($formStatistique);
            echo $this->Form->end();
            ?>
            </div>
            <div id="formule-recherche-controls">
                <a class="btn btn-info-webgfc" id="searchButton" title="<?php echo __d('statistique', 'btn.recherche'); ?>"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
            </div>

            <!-- zone resultat -->
        <?php $annee = Hash::get( $this->request->data, 'Statistique.annee' );?>
        <?php if( isset($results)): ?>
        <?php
        $services = array_keys($results['service']);
        if(count($services)>1){
            $count_ss=array();
            $count_ra=array();
            $count_delai=array();
            foreach($results['service'] as $serviceName => $service){
                $count_ss[$serviceName]=0;
                $count_ra[$serviceName]=0;
                $count_delai[$serviceName]=0;
                foreach ($service as $m => $donne){
                    $count_ss[$serviceName] += (int)$donne['count_ss'];
                    $count_ra[$serviceName] += (int)$donne['count_ra'];
                    $count_delai[$serviceName] += (int)$donne['count_delai'];
                }
            }
        }
        ?>
        <?php $nbFluxTotal = ' - (Total = '.Hash::get($results, 'Indicateurpourcent.nbtotal').' flux) ' ; ?>
        <?php
            $origine = '';
            if(!empty($origineSelected)) {
                $origine = " dont l'origine est ".'"'.Hash::get( $origines, $origineSelected ).'"';
            }
        ?>
            <div class="results panel-body">
                <legend class="legend-resultat"><?php echo __d('statistique', 'Indicateurqual.legend.Resultat') . $annee.$nbFluxTotal.$origine; ?></legend>
                <div class="resultsTableEtat">
                    <div id="mybarChart">

                    </div>
                    <table class="pourcentage" data-toggle='table' data-height ='599' data-show-refresh= 'true' data-show-toggle = 'true'
                           data-show-columns="true"
                           data-search="true"
                           data-locale = "fr-CA"
                           data-select-item-name="toolbar1">
                        <!--<h2>Taux de réponses cloturées sans suite</h2>-->
                        <thead>
                            <tr class="results">
                                <th></th>
                                <th></th>
                        <?php foreach( $mois as $valueMonth => $name ):?>
                                <th class="number bold"><?php echo $name.'<br />'.$annee;?></th>
                        <?php endforeach;?>
                                <th class="number bold">TOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- 1 ou plusieurs services sélectionnés -->
                <?php if( isset($results) ):?>
                    <?php /*$count = count( $results['service'] );*/?>
                    <?php foreach( $results['service'] as $serviceName => $values):?>
                            <tr class="titleindicateur">
                                <td class="bold servicename" rowspan="3"><?php echo $serviceName;?></td>
                                <td class="italical"><?php echo __d('statistique','Indicateurqual.Resultat.cloture');?></td>
                        <?php foreach( $values as $key => $val):?>
                                <td class="number">
                            <?php
                                $valueSanssuite = $val['count_ss'];
                                echo $this->Locale->number( $valueSanssuite );
                            ?>
                                </td>
                        <?php endforeach; ?>
                                <td class="number bold">
                            <?php
                                $valuesSs = '0';
                                for( $i =0; $i <12; $i++) {
                                    $valuesSs += $results['service'][$serviceName][$i]['count_ss'];
                                }
                                $valueTotal = $valuesSs;
                                $nbTotal = Hash::get($results, 'Indicateurpourcent.nbtotal');
                                echo $this->Locale->number( $valueTotal ).' ('.$this->Locale->number( $valueTotal * 100 / $nbTotal ).'%)';
                            ?>
                                </td>
                            </tr>

                            <tr class="titleindicateur">
                                <td class="italical"><?php echo __d('statistique','Indicateurqual.Resultat.enattente');?></td>
                        <?php foreach( $values as $key => $val):?>
                                <td class="number">
                        <?php
                            $valueReponse = $val['count_ra'];
                            echo $this->Locale->number( $valueReponse );
                        ?>
                                </td>
                        <?php endforeach; ?>
                                <td class="number bold">
                            <?php
                                $valuesRa = '0';
                                for( $i =0; $i <12; $i++) {
                                    $valuesRa += $results['service'][$serviceName][$i]['count_ra'];
                                }
                                $valueTotalRa = $valuesRa;
                                $nbTotalRa = Hash::get($results, 'Indicateurpourcent.nbtotal');
                                echo $this->Locale->number( $valueTotalRa ).' ('.$this->Locale->number( $valueTotalRa * 100 / $nbTotalRa ).'%)';
                            ?>
                                </td>
                            </tr>
                            <tr class="titleindicateur">
                                <td class="italical"><?php echo __d('statistique','Indicateurqual.Resultat.delais');?></td>
                        <?php foreach( $values as $key => $val):?>
                                <td class="number">
                            <?php
                                $valueReponse = $val['count_delai'];
                                echo $this->Locale->number( $valueReponse );
                            ?>
                                </td>
                        <?php endforeach; ?>
                                <td class="number bold">
                            <?php
                                $valuesRa = '0';
                                for( $i =0; $i <12; $i++) {
                                    $valuesRa += $results['service'][$serviceName][$i]['count_delai'];
                                }
                                $valueTotalRa = $valuesRa;
                                $nbTotalRa = Hash::get($results, 'Indicateurpourcent.nbtotal');
                                echo $this->Locale->number( $valueTotalRa ).' ('.$this->Locale->number( $valueTotalRa * 100 / $nbTotalRa ).'%)';
                            ?>
                                </td>
                            </tr>
                    <?php endforeach;?>
                    <?php endif;?>
                        </tbody>
                    </table>
                </div>
        <?php endif;?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#formulaireButton').button();
    $('#searchButton').button();

    $('#StatistiqueAnnee').select2({allowClear: true, placeholder: "Sélectionner une année"});
//    $( '#StatistiquePole' ).select2();
//    $( '#StatistiqueDirection' ).select2();
    $('#StatistiqueService').select2({allowClear: true, placeholder: "Sélectionner un service"});

    $('#StatistiqueOriginefluxId').select2({allowClear: true, placeholder: "Sélectionner une origine"});

    <?php if( isset($results)): ?>
    $('.formule-recherche').hide();
    $('#formule-recherche-controls').html('<a class="btn btn-info-webgfc btn-inverse" id="reset" title="<?php echo __d('statistique', 'btn.reset'); ?>"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>');
    $('#reset').click(function () {
        location.reload();
    });
    <?php endif ?>


    //**************************************************************//
    //statisitique
    <?php if( isset($results)): ?>
        <?php if(!isset($alreadyLoad)):?>
    google.charts.load('41', {packages: ['corechart', 'bar']});
        <?php endif?>
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        // Define the chart to be drawn.
        var data = google.visualization.arrayToDataTable([
            //plusieur service
           <?php
           if(count($services)>1){
                echo "['Service', 'Clos sans suite', 'En attente d\'une réponse', 'Réponse dans les délais'],";
                foreach($services as $service){
                        echo '["'.$service.'",'. $count_ss[$service]. ', '.$count_ra[$service] .','. $count_delai[$service].'],';
                }
            }else{
                echo "['Mois', 'Clos sans suite', 'Clos en attente d\'une réponse', 'Réponse dans les délais'],";
                foreach($results['service'] as $serviceN => $donneeMoiss){
                    foreach($donneeMoiss as $donneeMois){
                        echo '["'.$mois[$donneeMois["mois"]].'",'.(int)$donneeMois["count_ss"] . ', '.(int)$donneeMois["count_ra"].','.(int)$donneeMois["count_delai"].'],';

                    }
                }
            }
           ?>
        ]);

        var options = {
            chart: {
            },
            bars: 'horizontal', // Required for Material Bar Charts.
            vAxis: {format: 'decimal'},
            height: 400,
            colors: ['#1b9e77', '#d95f02', '#7570b3']
        };
        // Instantiate and draw the chart.
        var chart = new google.charts.Bar(document.getElementById('mybarChart'));
        chart.draw(data, options);
    }
    <?php endif ?>
    //*******************************************************************************//

    $('#searchButton').click(function () {
        var dataSerialize = $("#StatistiqueIndicateursQualForm").serialize();
        if (!$('.legend-resultat').size() < 1) {
            dataSerialize += '&alreadyLoad=true';
        }

		if (form_validate($("#StatistiqueIndicateursQualForm"))) {
			gui.request({
				url: $("#StatistiqueIndicateursQualForm").attr('action'),
				data: dataSerialize,
				loader: true,
				updateElement: $('.formule-recherche'),
				loaderMessage: gui.loaderMessage
			}, function (data) {
				$('#content_page').empty();
				$('#content_page').html(data);
			});
		}
    });

    // Ajout de l'action de recherche via la touche Entrée
    $('#StatistiqueIndicateursQualForm').keypress(function (e) {
        var dataSerialize = $("#StatistiqueIndicateursQualForm").serialize();
        if (!$('.legend-resultat').size() < 1) {
            dataSerialize += '&alreadyLoad=true';
        }
        if (e.keyCode == 13) {
            gui.request({
                url: $("#StatistiqueIndicateursQualForm").attr('action'),
                data: dataSerialize,
                loader: true,
                updateElement: $('#page-content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#page-content').html(data);
            });
        }
    });

//    $('#StatistiqueIndicateursQualForm.folded').hide();


    // Bouton pour le tableau du taux de cloturés sans suite
    gui.addbutton({
        element: $('#content_page .resultsTableEtat'),
        button: {
            content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
            title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
            class: 'btn btn-info-webgfc pull-right',
            action: function () {
                window.location.href = "<?php echo Router::url( array( 'controller' => 'statistiques', 'action' => 'exportqual' ) + Hash::flatten( $this->request->data, '__' ) );?>";
            }
        }

    });

    gui.disablebutton({
        element: $('#content_page .resultsTableEtat'),
        button: '<i class="fa fa-download" aria-hidden="true"></i>'
    });


    <?php if( !empty( $results ) ) { ?>
    $('.pourcentage').bootstrapTable({});
    gui.enablebutton({
        element: $('#content_page .resultsTableEtat'),
        button: '<i class="fa fa-download" aria-hidden="true"></i>',
        class: 'btn btn-info-webgfc pull-right',
        action: function () {
            window.location.href = "<?php echo Router::url( array( 'controller' => 'statistiques', 'action' => 'exportqual' ) + Hash::flatten( $this->request->data, '__' ) );?>";
        }
    });
        <?php } ?>

</script>
