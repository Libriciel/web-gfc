    <?php
	$this->Csv->preserveLeadingZerosInExcel = true;
	$this->Csv->delimiter = ';';
    $datasPassed = Hash::expand( $this->request->params['named'], '__' );
	$this->Csv->addRow(
		array_merge(
			array(
				'Statut',
				'Retard',
				'Référence',
				'Nom',
				'Objet',
				'Origine',
				'Date de création du flux',
				'Date d\'entrée dans la collectivité',
				'Direction concernée',
				'Service concerné',
				'Agent possédant le flux',
				'Affaire suivie par',
				'Organisme',
				'Contact',
				'Type',
				'Sous-type',
				'Dossier',
				'Affaire',
				'Nom de la tâche associée',
				'Méta-données',
				'Date de validation',
				'Date de clôture',
				'Date de refus'
			),
			$metasName
		)
	);

	foreach( $flux as $i => $result ) {

        $metasValue = array();
        foreach( $metasName as $key  => $name ) {
            $metasValue[] = $result['Courrier'][$name];
        }

        if( !empty($datasPassed['Recherche']['cretard']) ) {
            $retard = $datasPassed['Recherche']['cretard'];
        }
        else {
            $retard = ( Hash::get( $result, 'Courrier.mail_retard_envoye' ) ? 'Oui' : 'Non');
        }
        $statut = Hash::get( $etat, Hash::get( $result, 'Bancontenu.etat' ) );

		$row = array_merge(
			array(
				$statut,
				$retard,
				Hash::get( $result, 'Courrier.reference' ),
				Hash::get( $result, 'Courrier.name' ),
				preg_replace( "/[\r\n]+/", " ", Hash::get( $result, 'Courrier.objet' ) ),
				Hash::get( $result, 'Origineflux.name' ),
				$this->Html2->ukToFrenchDateWithSlashes( Hash::get( $result, 'Courrier.date' ) ),
				$this->Html2->ukToFrenchDateWithSlashes( Hash::get( $result, 'Courrier.datereception' ) ),
				isset($result['Service']['parent_id']) ? Hash::get( $services, Hash::get( $result, 'Service.parent_id' ) ) : '',
				isset($result['Service']['name']) ? Hash::get( $result, 'Service.name' ) : '',
				isset($result['Courrier']['agentstraitants']) ? $result['Courrier']['agentstraitants'] : '',
				isset($result['Courrier']['affairesuiviepar']) ? $result['Courrier']['affairesuiviepar'] : '',
				isset($result['Organisme']['name']) ? preg_replace( "/[\r\n,]+/", " - ", Hash::get( $result, 'Organisme.name' ) ) : '',
				isset($result['Contact']['name']) ? preg_replace( "/[\r\n,]+/", " - ", Hash::get( $result, 'Contact.name' ) ) : '',
				isset($result['Type']['name']) ? Hash::get( $result, 'Type.name' ) : '',
				isset($result['Soustype']['name']) ? Hash::get( $result, 'Soustype.name' ) : '',
				isset($result['Dossier']['name']) ? Hash::get( $result, 'Dossier.name' ) : '',
				isset($result['Affaire']['name']) ? Hash::get( $result, 'Affaire.name' ) : '',
				isset($result['Tache']['name']) ? Hash::get( $result, 'Tache.name' ) : '',
				isset($result['Courrier']['metadonnees']) ? Hash::get( $result, 'Courrier.metadonnees' ) : '',
				( isset( $result['Courrier']['datevalidation']) && !empty( $result['Courrier']['datevalidation'] ) ) ? $this->Html2->ukToFrenchDateWithSlashes( Hash::get( $result, 'Courrier.datevalidation' ) ) : '',
				( isset( $result['Courrier']['datecloture']) && !empty( $result['Courrier']['datecloture'] ) ) ? $this->Html2->ukToFrenchDateWithSlashes( Hash::get( $result, 'Courrier.datecloture' ) ) : '',
				( isset( $result['Courrier']['daterefus']) && !empty( $result['Courrier']['daterefus'] ) ) ? $this->Html2->ukToFrenchDateWithSlashes( Hash::get( $result, 'Courrier.daterefus' ) ) : ''
			),
			$metasValue
		);
		$this->Csv->addRow( $row );
	}

	Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>
