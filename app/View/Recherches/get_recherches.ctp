<?php

/**
 *
 * Recherches/get_recherches.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (!empty($recherches)) {

    $options = array(
        'nodes' => array(
            array(
                'model' => 'Recherche',
                'field' => 'name',
                'actions' => array(
                    "view" => array(
                        "ico" => 'fa fa-eye',
                        "url" => Configure::read('BaseUrl') . '/recherches/recherche',
//                        "icoColor" => "#5397a7"
                    ),
                    "edit" => array(
                        "ico" => 'fa fa-pencil',
                        "url" => Configure::read('BaseUrl') . '/recherches/formulaire',
//                        "icoColor" => "#5397a7"
                    ),
                    "delete" => array(
                        "ico" => 'fa fa-trash',
                        "url" => Configure::read('BaseUrl') . '/recherches/delete',
//                        "icoColor" => "#FF0000"
                    )
                )
            )
        )
        ,
        'listId' => 'browserRecherche',
        'listClass' => 'filetree'
    );

    $val = $this->Liste->jqueryListView($recherches, $options);
    echo $val;
} else {
	echo __d('recherche', 'Recherche.void');
}
?>


<script type="text/javascript">

    $("#browserRecherche").treeview({collapsed: true});

    $('.treeview span.folder, .treeview span.file').addClass('ui-corner-all').hover(function () {
        $(this).addClass('alert alert-warning').css('border', 'none').css('margin', '0');
    }, function () {
        $(this).removeClass('alert alert-warning').css('border', 'none');
    });


    $('.treeviewBttn i').each(function () {
        if ($(this).hasClass('actionImg')) {
            $(this).button();
        }
        var parentLine = $(this).parent().parent().parent();
        var parentSpan = $(this).parent().parent();
        var parentLink = $(this).parent();
        var href = $(this).parent().attr('href');
        var img = $(this).detach();
        img.appendTo(parentSpan).click(function () {
            if (img.hasClass('deleteBttn')) {
                swal({
                    showCloseButton: true,
                    title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
                    text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
                    cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
                }).then(function (data) {
//                    $(this).parents(".modal").modal('hide');
                    if (data) {
                        gui.request({
                            url: href,
                            loaderElement: $('#repertoires_skel_dyn .content'),
                            loaderMessage: gui.loaderMessage
                        }, function (data) {
                            getJsonResponse(data);
                            loadRecherches();
                        });
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Annulé!",
                            text: "Vous n'avez pas supprimé, ;) .",
                            type: "error",

                        });
                    }
                });
                $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
            } else if ($(this).hasClass('viewBttn')) {
                selectLineElement(parentLine);
                gui.request({
                    url: href,
                    loader: true,
                    updateElement: $('#recherche_skel_infos .content'),
                    loaderMessage: gui.loaderMessage
                });
                backToList();
            } else {
                $('#recherche_skel_infos .content').empty();
                gui.request({
                    url: href,
                    loader: true,
                    updateElement: $('#recherche_skel_infos .content'),
                    loaderMessage: gui.loaderMessage
                });
                gui.removebutton({
                    element: $('#recherche_skel_infos .panel-footer'),
                    button: "<?php echo __d('default', 'Button.submit'); ?>"
                });
                gui.removebutton({
                    element: $('#recherche_skel_infos .panel-footer'),
                    button: "<?php echo __d('default', 'Button.cancel'); ?>"
                });
                gui.addbuttons({
                    element: $('#recherche_skel_infos .panel-footer'),
                    buttons: [
                        {
                            content: "<?php echo __d('default', 'Button.cancel'); ?>",
                            class: 'btn-danger-webgfc btn-inverse ',
                            action: function () {
                                $('#recherche_skel_infos .content').empty();
                                $('#recherche_skel_infos .panel-footer').empty();
                            }
                        },
                        {
                            content: "<?php echo __d('default', 'Button.submit'); ?>",
                            class: 'btn-info-webgfc ',
                            action: function () {
                                var form = $('#RechercheFormulaireForm');
                                if (form_validate(form)) {
                                    gui.request({
                                        url: form.attr('action'),
                                        data: form.serialize()
                                    }, function (data) {
                                        getJsonResponse(data);
                                        loadRecherches();
                                        $('.jSelectList').each(function () {
                                            $(this).empty();
                                        });
                                        if (parentLine.hasClass('ui-state-focus')) {
                                            $('#recherche_skel_infos .content').empty();
                                        }
                                    });
                                } else {
                                    swal({
                    showCloseButton: true,
                                        title: "Oops...",
                                        text: "Veuillez vérifier votre formulaire!",
                                        type: "error",

                                    });
                                }
                            }
                        }
                    ]
                });
                backToList();
            }
        });
        parentLink.remove();

        /*pour gagner plus de place , une fois que modifier/view un recherche enregistrée,
         * liste de recherches va cacher, ajouter un bouton de décacher la liste*/
        function backToList() {
            $('#repertoires_skel_dyn').hide();
            var bouton_back = '<a class="btn btn-info-webgfc" id="back_to_list" title="Affiche list des recherches enrregistrées." style="float:left"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>'
            $(bouton_back).insertBefore($('#recherche_skel_infos >.panel-default > .panel-heading > h4'));
            $('#recherche_skel_infos').removeClass('col-sm-8').addClass('col-sm-12');
            $("#back_to_list").click(function () {
                $('#recherche_skel_infos').removeClass('col-sm-12').addClass('col-sm-8');
                $('#repertoires_skel_dyn').show();
                $("#back_to_list").remove();
            });
        }

    });
</script>
