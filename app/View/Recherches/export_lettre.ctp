<?php
	$this->Csv->preserveLeadingZerosInExcel = true;
    
    
    $os = Hash::extract($flux,'{n}.Ordreservice' );
    $plis = Hash::extract($flux,'{n}.Consultation' );
    
    if( !empty($flux[0]['Ordreservice']['id']) ) {
        $this->Csv->addRow(
            array(
                'N° OPE',
                'Nom OP',
                'Ville',
                'RO / AO',
                'Date',
                'N° Chrono',
                'N° Marché',
                'Titulaire',
                'N° OS',
                'Objet',
                'Montant H.T.',
                'Destinataire',
                'N° de voie',
                'Nom de voie',
                'Compl.',
                'Code postal',
                'Ville',
                'Méta-données'
            )
        );
    }
    else if( !empty($plis) ) {
        $this->Csv->addRow(
            array(
                'N° OP',
                'Nom OP',
                'N° consultation',
                'N° d\'ordre d\'arrivée',
                'Lot',
                'Date et Heure de réception',
                'Mode de réception',
                'Observations',
                'Destinataire',
                'N° de voie',
                'Nom de voie',
                'Compl.',
                'Code postal',
                'Ville',
                'Méta-données'
            )
        );
    }
    else {
        $this->Csv->addRow(
            array(
                'N° OPE',
                'Nom OP',
                'RO / AO',
                'Date',
                'N° Chrono',
                'Intitulé du flux',
                'Objet',
                'N° de marché',
                'Montant H.T.',
                'Destinataire',
                'SIRET',
                'Code postal',
                'Ville',
                'Méta-données'
            )
        );
    }
	foreach( $flux as $flx ) {
        $types = Hash::get($this->request->params['named'], 'Recherche__ctypedocument');
		if($types == 'OS') {
            $row = array(
                $flx['Operation']['name'],
                $flx['Operation']['nomoperation'],
                $flx['Operation']['ville'],
                isset( $flx['Operation']['ro'] ) ? $flx['Operation']['ro'] . ' / ' . $flx['Operation']['ao'] : '',
                isset($flx['Ordreservice']['created']) ? $this->Html2->ukToFrenchDateWithSlashes($flx['Ordreservice']['created']) : '',
                isset( $flx['Courrier']['reference'] ) ? $flx['Courrier']['reference'] : '',
                isset( $flx['Marche']['numero'] ) ? $flx['Marche']['numero'] : '',
                isset( $flx['Marche']['titulaire'] ) ?  $flx['Marche']['titulaire'] : '',
                isset( $flx['Ordreservice']['numero'] ) ? $flx['Ordreservice']['numero'] : '',
                ( isset( $flx['Ordreservice']['objet'] ) && !empty($flx['Ordreservice']['objet']) ) ? preg_replace( "/[\r\n,]+/", " ", $flx['Ordreservice']['objet'] ): preg_replace( "/[\r\n,]+/", " ", $flx['Courrier']['objet'] ),
                isset( $flx['Ordreservice']['montant'] ) ? preg_replace( "/[,]+/", ".", $flx['Ordreservice']['montant'] ) : '',
                isset( $flx['Destinataire']['name'] ) ? $flx['Destinataire']['name'] : '',
                isset( $flx['Destinataire']['numvoie'] ) ? $flx['Destinataire']['numvoie'] : '',
                isset( $flx['Destinataire']['nomvoie'] ) ? $flx['Destinataire']['nomvoie'] : '',
                isset( $flx['Destinataire']['compl'] ) ? $flx['Destinataire']['compl'] : '',
                isset( $flx['Destinataire']['cp'] ) ? $flx['Destinataire']['cp'] : '',
                isset( $flx['Destinataire']['ville'] ) ? $flx['Destinataire']['ville'] : '',
                Hash::get( $flx, 'Metadonnee.valeur' )

            );
        }
        else if($types == 'PLI') {
            $row = array(
                $flx['Operation']['name'],
                $flx['Operation']['nomoperation'],
                $flx['Consultation']['name'],
                isset( $flx['Pliconsultatif']['numero'] ) ? $flx['Pliconsultatif']['numero'] : '',
                isset( $flx['Pliconsultatif']['lot'] ) ? $flx['Pliconsultatif']['lot'] : '',
                isset($flx['Pliconsultatif']['date']) ? $this->Html2->ukToFrenchDateWithSlashes($flx['Pliconsultatif']['date']).' à '.$flx['Pliconsultatif']['heure'] : '',
                isset( $flx['Origineflux']['name'] ) ? $flx['Origineflux']['name'] : '',
                isset( $flx['Pliconsultatif']['objet'] ) ? preg_replace( "/[\r\n,]+/", " ", $flx['Pliconsultatif']['objet']) : '',
                isset( $flx['Destinataire']['name'] ) ? $flx['Destinataire']['name'] : '',
                isset( $flx['Destinataire']['numvoie'] ) ? $flx['Destinataire']['numvoie'] : '',
                isset( $flx['Destinataire']['nomvoie'] ) ? $flx['Destinataire']['nomvoie'] : '',
                isset( $flx['Destinataire']['compl'] ) ? $flx['Destinataire']['compl'] : '',
                isset( $flx['Destinataire']['cp'] ) ? $flx['Destinataire']['cp'] : '',
                isset( $flx['Destinataire']['ville'] ) ? $flx['Destinataire']['ville'] : '',
                Hash::get( $flx, 'Metadonnee.valeur' )

            );
        }
        else {
            $row = array(
                $flx['Operation']['name'],
                $flx['Operation']['nomoperation'],
                isset( $flx['Operation']['ro'] ) ?  $flx['Operation']['ro']. ' / ' . @$flx['Operation']['ao'] : '',
                isset($flx['Courrier']['created']) ? $this->Html2->ukToFrenchDateWithSlashes($flx['Courrier']['created']) : '',
                isset( $flx['Courrier']['reference'] ) ? $flx['Courrier']['reference'] : '',
                isset( $flx['Courrier']['name'] ) ? $flx['Courrier']['name'] : '',
                isset( $flx['Marche']['objet'] ) ? preg_replace( "/[\r\n,]+/", " ", $flx['Marche']['objet'] ) : '',
                isset( $flx['Marche']['numero'] ) ? $flx['Marche']['numero'] : '',
                isset($flx['Lettre']['montant']) ? preg_replace( "/[,]+/", ".", $flx['Lettre']['montant'] ) : '',
                isset( $flx['Destinataire']['name'] ) ? $flx['Destinataire']['name'] : '',
                isset( $flx['Destinataire']['siret'] ) ? $flx['Destinataire']['siret'] : '',
                isset( $flx['Destinataire']['cp'] ) ? $flx['Destinataire']['cp'] : '',
                isset( $flx['Destinataire']['ville'] ) ? $flx['Destinataire']['ville'] : '',
                Hash::get( $flx, 'Metadonnee.valeur' )

            );
        }
		$this->Csv->addRow( $row );
	}
    
	Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>