<?php

/**
 *
 * Recherches/recherche.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php
echo $this->Html->script('appAttendable.js', array('inline' => true));
?>
<?php
//Debugger::log($flux);
if (empty($flux)) {
        echo $this->Html->tag('div', __d('recherche', 'RechercheFlux.void'), array('class' => 'alert alert-warning'));
}else{
    $nbItem = Hash::get($this->request->params, 'paging.Courrier.count');
?>
<div class="bannette_panel panel-body">

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
    <table class="bannette-table"
           data-toggle="table"
           data-height=screenHeight
           data-show-refresh="false"
           data-show-toggle="false"
           data-show-columns="true"
           data-search="true"
           data-locale = "fr-CA"
           data-select-item-name="toolbar1"
           id="table_res">
        <thead>
            <tr>
                <?php
                    echo $this->Bannette->drawTableHead($flux, array('view' => 'historiqueGlobal' , 'delete' => 'delete'));
                ?>
            </tr>
        </thead>
    </table>
    <?php
    echo $this->Bannette->drawPaginator($flux,array('view' => 'historiqueGlobal' , 'delete' => 'delete'));
    ?>
</div>



<?php

}
?>
<script type="text/javascript">
    $(document).ready(function () {
        //title legend (nombre de données)
        if ($('.table-list h3 span').length < 1) {
            <?php if(isset($nbItem) && !empty($nbItem)): ?>
            $('.table-list h3').append(' <span> - total: <?php echo $nbItem;?></span>');
            <?php endif; ?>
        } else {
            $('.table-list h3 span').remove();
            <?php if(isset($nbItem) && !empty($nbItem)): ?>
            $('.table-list h3').append(' <span> - total: <?php echo $nbItem;?></span>');
            <?php endif; ?>
        }
        var fluxChoix = [];
        <?php $data =!empty($this->Bannette->drawTableTd($flux, array('view' => 'historiqueGlobal' , 'delete' => 'delete'))) ?  $this->Bannette->drawTableTd($flux, array('view' => 'historiqueGlobal' , 'delete' => 'delete')) : '[]' ?>;
        data = <?php echo $data; ?>;
        <?php if( !empty( $nbItem ) ) : ?>
        $('.bannette-table').bootstrapTable();
        $("#table_res").bootstrapTable('load', data);
        $("#table_res")
                .on("click-row.bs.table", function (e, row, element) {
                    if (!$(element.context).hasClass("action")) {
//                        var href = element.find('.thView').find('a').attr('href');
//                        window.open(href);
                        var itemId = element.find('.checkItem').attr('itemId');
                        window.open('/courriers/historiqueGlobal/' + itemId);
                    } else if ($(element.context).hasClass("thView")) {
                    }
                })
                // .on('all.bs.table', function (e, data) {
                //     $('.tri').find('i').each(function () {
                //         var fluxId = $(this).attr('id').substring(5);
                //         $(this).hover(function () {
                //             $('#flux_' + fluxId).show();
                //         }, function () {
                //             $('#flux_' + fluxId).hide();
                //         });
                //     });
                // })
                .on('sort.bs.table', function (e, name, order) {
                    var nb = <?php echo $nbItem; ?>;
                    if (nb > 20) {
                        var tri;
                        <?php if(isset($ancien_order) && isset($ancien_name)): ?>
                        var ancien_order = '<?php echo $ancien_order; ?>';
                        <?php if($ancien_name!=''): ?>
                        var ancien_name = '<?php echo $ancien_name; ?>';
                        <?php endif; ?>
                        if (ancien_order != '' && ancien_name == name) {
                            if (ancien_order == 'desc') {
                                order = 'asc';
                            } else {
                                order = 'desc';
                            }
                        }
                        <?php endif; ?>

                        switch (name) {
                            case 'reference':
                                tri = 'Courrier.reference ' + order;
                                break;
                            case 'etat':
                                tri = 'Courrier.etat ' + order;
                                break;
                            case 'nom':
                                tri = 'Courrier.name ' + order;
                                break;
                            case 'objet':
                                tri = 'Courrier.objet ' + order;
                                break;
                            case 'expediteur':
                                tri = 'Organisme.name ' + order; // à voir c'est avec organisme.name ou contact.name
                                break;
							case 'agenttraitant':
                                tri = 'Desktop.name ' + order;
                                break;
                            case 'datereception':
                                tri = 'Courrier.datereception ' + order;
                                break;
                            case 'retard':
                                tri = '';
                                break;
                        }
                        var data = $('#RechercheFormulaireForm').serialize() + '&tri=' + tri;
                        gui.request({
                            url: "<?php echo Configure::read('BaseUrl') . "/Recherches/recherche/"; ?>",
                            data: data,
                            updateElement: $('#infos .content'),
                            loader: true,
                            loaderMessage: gui.loaderMessage
                        });
                    }
                });

        <?php endif;?>

        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });

        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.59);
        $(".fixed-table-container").css('overflow', 'hidden');
    }

	// On met en rouge les retards
	$("th.retard.tri.delairetard").css('color', 'black');
	$("td.retard.tri.delairetard").css('color', 'blue');
	var hasimg = $(".delairetard").find('img');
    if( hasimg ) {
		$(".delairetard").find('img').parent('td').css('color', 'red');
	}


    if ($('#infos .controls .btn').length == 0) {
        gui.buttonbox({
            element: $('#infos .controls'),
            align: "center"
        });
        gui.addbutton({
            element: $('#infos .controls'),
            button: {
                id: "download",
                content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
                class: 'btn-info-webgfc btn-inverse',
                title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
                action: function () {
                    window.location.href = "<?php echo Router::url( array( 'controller' => 'recherches', 'action' => 'exportcsv' ) + Hash::flatten( $this->request->data, '__' ) );?>";
                }
            }
        });
//
//        gui.addbutton({
//            element: $('#liste .panel-footer'),
//            button: {
//                content: '<i class="fa fa-eye-slash" aria-hidden="true"></i> Masquer',
//                class: "hide-search-formulaire btn-inverse btn-info-webgfc ",
//                title: "<?php echo __d('default', 'Button.hide'); ?>",
//                action: function () {
//                    $('#liste').hide();
//                }
//            }
//        });
         // permet de masquer le formulaire de recherche, lorsqu'on le réouvre
        $('#liste h3').css('cursor', 'pointer');
        $('#liste h3').click(function () {
            $('#liste').hide();
        });

        // Bouton pour vider le formulaire de recherche
        gui.addbutton({
            element: $('#infos .controls'),
            button: {
                content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
                class: 'btn-info-webgfc btn-inverse',
                title: "<?php echo __d('default', 'Button.reset'); ?>",
                action: function () {
                    if (!$(this).hasClass('ui-state-disabled')) {
                        $('#infos .content').empty();
                        $('#infos .controls').empty();
                    }
                    resetJSelect('#RechercheFormulaireForm');
                    $('#RechercheFormulaireForm .select2-search-choice').remove();
                    var formCheckbox = $('.formSearch input');
                    formCheckbox.each(function (index, item) {
                        var idInput = item.id;
                        if ($('#' + idInput + '').prop('checked')) {
                            $('.' + idInput + '').show();
                        } else {
                            $('.' + idInput + '').hide();
                        }
                    });
                    if ($('.searcheSaveTrue').is(':visible')) {
                        $('#searchSavaBool').click();
                    }
                    $('#liste').show();
                    $('#infos').hide();
                    $('.hide-search-formulaire').remove();
                }
            }
        });

        // Bouton pour réouvrir le formulaire de recherche
        gui.addbutton({
            element: $('#infos .controls'),
            button: {
                content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
                title: "<?php echo __d('default', 'Button.search'); ?>",
                class: 'btn-info-webgfc',
                action: function () {
//                    resetJSelect('#RechercheFormulaireForm');
                    var formCheckbox = $('.formSearch input');
                    formCheckbox.each(function (index, item) {
                        var idInput = item.id;
                        if ($('#' + idInput + '').prop('checked')) {
                            $('.' + idInput + '').show();
                        } else {
                            $('.' + idInput + '').hide();
                        }
                    });
                    $('#liste').show();
                    $('.hide-search-formulaire').show();
                }
            }
        });






        gui.disablebutton({
            element: $('#infos .controls'),
            button: "<?php echo __d('default', 'Button.exportcsv'); ?>"
        });
    }

<?php if( !empty( $flux ) ) { ?>
    gui.enablebutton({
        element: $('#infos .controls'),
        button: "<?php echo __d('default', 'Button.exportcsv'); ?>",
        action: function () {
            window.location.href = "<?php echo Router::url( array( 'controller' => 'recherches', 'action' => 'exportcsv' ) + Hash::flatten( $this->request->data, '__' ) );?>";

            if ($(this).hasClass('waiter-default')) {
                $('#waiter').empty();
                $('#waiter').hide();
                $('#waiter').remove();
                $('.action').remove();
            }
        }
    });
<?php } ?>

    $('#download').click(function () {
        $('#download').attr('data-target', '#waiter');
        $('#download').attr('data-toggle', 'modal');
        $('#download').attr('data-waiter-send', 'XHR');
        $('#download').attr('data-waiter-callback', 'getProgress');
        $('#download').attr('data-waiter-type', 'progress-bar');
        $('#download').attr('data-waiter-title', 'Test');
    });

    gui.addbutton({
        element: $('#waiter .action'),
        button: {
            content: '<i class="fa fa-times" aria-hidden="true"></i> Fermer',
            class: "btn-danger-webgfc btn-inverse ",
            action: function () {
                $(this).parents(".modal").modal('hide');
                $(this).parents(".modal").empty();
            }
        }
    });

    // fonction pour clore la modale une fois l'export retourné
	$('#download').click(function () {
		//Start the long running process
		$.ajax({
			url: 'long_process',
			success: function (data) {
			}
		});

		//Start receiving progress
		function getProgress() {
			$.ajax({
				url: 'progress',
				success: function (data) {
					// on ferme la modale
					$('#waiter').css('display', 'none');

					// on enlève la classe modal-open
					$('body').removeClass('modal-open');

					// on supprime la div nécessair epour, l'ouverture de la modale
					$('.modal-backdrop.fade.in').remove();
					if (data < 10) {
						getProgress();
					}
				}
			});
		}

		getProgress();
	});
</script>

<?php  echo $this->Js->writeBuffer(); ?>

<div id="waiter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="waiterLabel"><?php echo __('Téléchargement des résultats en cours'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="waiter-progress">
                    <!--<div class="progress">-->
                      <!--<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>-->
                    <!--</div>-->
                    <div class="spacer"></div>
                    <center><p  class="waiter-comment">Veuillez patienter...</p></center>
                </div>

                <div class="waiter-default" >
                    <center><?php echo $this->Html->image('/img/ajax-loader.gif', array('alt' => __('Loading'), 'id' => 'waiter-image')); ?></center>
                    <br />
                    <center><p  class="waiter-comment alert alert-info" style="margin-bottom: 50px;">L'export est en cours de téléchargement ...</p></center>
                    <div class="action" style="text-align: right;"></div>
                </div>
              </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
