<?php

/**
 *
 * Recherches/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php echo $this->Form->create('Recherche'); ?>
<div id="recherche_tabs">
    <ul>
        <li><a href="#recherche_tabs_1">Champs</a></li>
        <li><a href="#recherche_tabs_2">Metadonnées</a></li>
    </ul>
    <div id="recherche_tabs_1">
        <?php
        echo $this->Form->input('Recherche.user_id', array('type' => 'hidden', 'value' => $this->Session->read('Auth.User.id')));
        echo "<fieldset><legend>" . __d('recherche', 'Recherche') . "</legend>";
        echo $this->Form->input('Recherche.name', array('label' => __d('recherche', 'Recherche.name')));
        echo "</fieldset><br />";
        echo "<fieldset><legend>" . __d('recherche', 'Flux') . "</legend>";
        echo $this->Form->input('Recherche.cname', array('label' => __d('recherche', 'Recherche.cname')));
        echo $this->Form->input('Recherche.cobjet', array('label' => __d('recherche', 'Recherche.cobjet')));
        echo $this->Form->input('Recherche.qtype_id', array('label' => __d('recherche', 'Recherche.qtype_id'), 'options' => $types, 'empty' => '', 'type' => 'select', 'escape' => false));
        echo $this->Form->input('Recherche.qsoustype_id', array('label' => __d('recherche', 'Recherche.qsoustype_id'), 'options' => $soustypes, 'empty' => '', 'type' => 'select', 'escape' => false));
        echo $this->Form->input('Recherche.qinout', array('label' => __d('recherche', 'Recherche.qinout')));
        echo "</fieldset>";
        ?>
    </div>
    <div id="recherche_tabs_2">
        <?php
        echo "<fieldset><legend>" . __d('recherche', 'Metadonnees') . "</legend>";
        echo $this->Form->input('Metadonnee.Metadonnee', array('label' => '', 'style' => 'height: 150px;width: 400px;', 'options' => $metadonnees, 'default' => false, 'multiple' => 'multiple', 'class' => 'selectMultiple', 'escape' => false));
        echo "</fieldset>";
        ?>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<script type="text/javascript">
    gui.tabs({
        element: $('#recherche_tabs'),
        noErrorMsg: true,
        loaderMessage: gui.loaderMessage
    });
</script>




