 <?php
/**
 *
 * Recherches/export.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//echo $this->Html->script('zone/Recherches/recherchesFunction.js', array('inline' => true));
    //echo $this->Html->css(array('administration'), null, array('inline' => false));
?>

<div class="container">
    <div class="row">
        <div id="liste" class="table-list rechercheCondition">
            <h3>Recherche de type de flux</h3>
            <div class="content">
                <div class="zoneRechercheExportForm" style="margin-top: 50px">
                <?php
                $formRecherche = array(
                    'name' => 'Recherche',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-4',
					'form_url' => array( 'controller' => 'recherches', 'action' => 'export' ),
                    'input' => array(
                        'Recherche.ctypedocument' => array(
                            'labelText' =>__d('courrier', 'Courrier.typedocument'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type' => 'select',
                                'required' => true,
                                'options' => $typedocument,
                                'empty' => true
                            )
                        ),
                        'Recherche.cdatefilter' =>array(
                            'labelText' =>'Debut',
                            'inputType' => 'text',
                            'dateInput' =>true,
                            'items'=>array(
                                'type' => 'text',
                                'readonly' => true,
                                'class' => 'datepicker',
                                'data-format'=>'dd/MM/yyyy',
                                'empty' => true
                            )
                        ),
                        'Recherche.cdatefilterfin' =>array(
                            'labelText' =>'Fin',
                            'inputType' => 'text',
                            'dateInput' =>true,
                            'items'=>array(
                                'type' => 'text',
                                'readonly' => true,
                                'class' => 'datepicker',
                                'data-format'=>'dd/MM/yyyy',
                                'empty' => true
                            )
                        ),
                        'Recherche.cnumeroop' =>array(
                            'labelText' =>'N° OP',
                            'inputType' => 'select',
                            'items'=>array(
                                'type' => 'select',
                                'readonly' => true,
                                'options' => $operations,
                                'empty' => true
                            )
                        ),
                        'Recherche.cnumerorar' =>array(
                            'labelText' =>'N° RAR',
                            'inputType' => 'select',
                            'items'=>array(
                                'type' => 'select',
                                'readonly' => true,
                                'options' => $rars,
                                'empty' => true
                            )
                        )
                    )
                );
                echo $this->Formulaire->createForm($formRecherche);
                echo $this->Form->end();
                ?>
                </div>
            </div>
            <div class="controls panel-footer " role="group" style="width: 100%;">
                <a id="searchButton" class="btn btn-info-webgfc" title="<?php echo __d('recherche','Recherche.search'); ?>"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
            </div>
        </div>

    <?php if( isset($flux) && !empty($flux) ) {?>
	 <?php $plis = Hash::extract($flux,'{n}.Consultation' ) ;?>
        <?php
             if( isset($plis ) && !empty($plis) ) {
                 $Model = 'Consultation';
                 $update = '#webgfc_content';
                 $before = '$("#webgfc_content")';
             }
             else if(isset($flux[0]['Ordreservice'])){
                 $Model = 'Ordreservice';
                 $update = '#webgfc_content';
                 $before = '$("#webgfc_content")';

             }
             if( !isset($flux[0]['Ordreservice']) && !isset($flux[0]['Pliconsultatif']) ) {
                 $Model = 'Courrier';
                 $update = '#webgfc_content';
                 $before = '$("#webgfc_content")';
             }
             $pagination = '';
             $this->Paginator->options(array(
                 'update' => "$update",
                 'evalScripts' => true,
                 'before' => 'gui.loader({ element: $("' . $update . ' "), message: gui.loaderMessage });',
                 'complete' => '$(".loader").remove()',
                 'url' => $this->request->params['pass'],
                 'data' => http_build_query($this->request->data),
                 'method' => 'POST'
             ));
             if (Set::classicExtract($this->params, "paging.{$Model}.pageCount") > 1) {
                 $countFlux = $this->params['paging'][$Model]['count'];
                 $optionsNumber['separator'] = false;
                 $pagination = '<ul class="pagination">' . implode('', Set::filter(array(
                                        $this->Html->tag('li', $this->Paginator->first('<<', array(), null, array('class' => 'disabled'))),
                                        $this->Html->tag('li', $this->Paginator->prev('<', array(), null, array('class' => 'disabled')), array('class' => 'previous')),
                                        $this->Html->tag('li', $this->Paginator->numbers($optionsNumber)),
                                        $this->Html->tag('li', $this->Paginator->next('>', array(), null, array('class' => 'disabled')), array('class' => 'previous')),
                                        $this->Html->tag('li', $this->Paginator->last('>>', array(), null, array('class' => 'disabled')))))) . '
                                    </ul>';
             }
         ?>
        <div class="table-list rechercheCondition" id="infos_resultat">
            <h3><?php echo 'Total : ' . $countFlux;?></h3>
            <div class="content">
            <?php $plis = Hash::extract($flux,'{n}.Consultation' ) ;?>
	        <?php if( isset($plis ) && !empty($plis) ) {?>
                <table id="listePlis" class="plis" data-toggle="table"
                       data-height="599"
                       data-search="true"
                       data-locale = "fr-CA"
                       data-select-item-name="toolbar1">
                    <thead>
                        <tr>
                            <th>N° OP</th>
                            <th>Nom OP</th>
                            <th>N° Consultation</th>
                            <th>Nombre de plis</th>
                            <th>Date et Heure limite</th>
                            <th class="actions"><?php echo __d('default', 'Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $i = 0;
                            foreach ($flux as $flx):
                                $i++;
                                ?>
                        <tr class="<?php echo $flx['Consultation']['id']; ?>">
                            <td><?php echo $flx['Operation']['name']; ?>&nbsp;</td>
                            <td><?php echo $flx['Operation']['nomoperation']; ?>&nbsp;</td>
                            <td><?php echo $flx['Consultation']['name']; ?>&nbsp;</td>
                            <td><?php echo $flx['Pliconsultatif']['numero']; ?>&nbsp;</td>
                            <td><?php echo $this->Html2->ukToFrenchDateWithSlashes($flx['Consultation']['datelimite']).' à '.$flx['Consultation']['heurelimite']; ?>&nbsp;</td>
                            <td class="actions"><?php echo !empty($flx['Consultation']['id']) ?  $this->Html->link('<i class="fa fa-eye" aria-hidden="true" ></i>',array('controller' => 'consultations', 'action' => 'index'), array('escape'=>false,'title' => __d('default', 'Button.view'),'target'=>'_blank')): '';?></td>
                        </tr>
                            <?php endforeach; ?>
                    </tbody>
                </table>
                <?php } else if( isset($flux[0]['Ordreservice'])) {?>
                <table id="listeOS" class="os" data-toggle="table"
                       data-height="599"
                       data-search="true"
                       data-locale = "fr-CA"
                       data-select-item-name="toolbar1">
                    <thead>
                        <tr>
                            <th>N° OPE</th>
                            <th>Nom OP</th>
                            <th>Ville</th>
                            <th>RO / AO</th>
                            <th>Date</th>
                            <th>N° Chrono</th>
                            <th>N° du Marché</th>
                            <th>Titulaire</th>
                            <th>N° OS</th>
                            <th>Objet</th>
                            <th>Montant H.T.</th>
                            <th class="actions"><?php echo __d('default', 'Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody class="bodyOS">
                        <?php
                            $i = 0;
                            foreach ($flux as $flx):
                                $i++;
                                ?>
                        <tr class="<?php echo $flx['Ordreservice']['id']; ?>">
                            <td><?php echo isset($flx['Operation']['name']) ? $flx['Operation']['name'] : ''; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Operation']['nomoperation'] ) ? $flx['Operation']['nomoperation'] : ''; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Operation']['ville'] ) ? $flx['Operation']['ville'] : ''; ?>&nbsp;</td>
                            <td><?php echo $flx['Operation']['ro'] . ' / ' . $flx['Operation']['ao']; ?>&nbsp;</td>
                            <td><?php echo $this->Html2->ukToFrenchDateWithSlashes($flx['Ordreservice']['created']); ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Courrier']['reference'] ) ? $flx['Courrier']['reference'] : ''; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Marche']['numero'] ) ? $flx['Marche']['numero'] : ''; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Marche']['titulaire'] ) ? $flx['Marche']['titulaire'] : ''; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Ordreservice']['numero'] ) ? $flx['Ordreservice']['numero'] : '' ; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Ordreservice']['objet'] ) ? $flx['Ordreservice']['objet'] : '' ; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Ordreservice']['montant'] ) ? $flx['Ordreservice']['montant'] : ''; ?>&nbsp;</td>
                            <td class="actions"><?php echo !empty($flx['Courrier']['id']) ? $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>',array('controller' => 'courriers', 'action' => 'historiqueGlobal', $flx['Courrier']['id']), array('escape'=>false,'title' => __d('default', 'Button.view'),'target'=>'_blank')): '';?></td>
                        </tr>
                            <?php endforeach; ?>
                    </tbody>
                </table>
                <?php } else  {?>
                <table id="listeOS" class="os" data-toggle="table"
                       data-height="599"
                       data-search="true"
                       data-locale = "fr-CA"
                       data-select-item-name="toolbar1">
                    <thead>
                        <tr>
                            <th>N° OPE</th>
                            <th>Nom OP</th>
                            <th>RO / AO</th>
                            <th>Date</th>
                            <th>N° Chrono</th>
                            <th>Intitulé du flux</th>
                            <th>Identité du contact</th>
                            <th>Objet</th>
                            <th>Montant H.T.</th>
                            <th class="actions"><?php echo __d('default', 'Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody class="bodyOS">
                        <?php
                            $i = 0;
                            foreach ($flux as $flx):
                                $i++;
                                ?>
                        <tr class="<?php echo $flx['Courrier']['id']; ?>">
                            <td><?php echo isset($flx['Operation']['name']) ? $flx['Operation']['name'] : ''; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Operation']['nomoperation'] ) ? $flx['Operation']['nomoperation'] : ''; ?>&nbsp;</td>
                            <td><?php echo $flx['Operation']['ro'] . ' / ' . $flx['Operation']['ao']; ?>&nbsp;</td>
                            <td><?php echo $this->Html2->ukToFrenchDateWithSlashes($flx['Courrier']['created']); ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Courrier']['reference'] ) ? $flx['Courrier']['reference'] : ''; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Courrier']['name'] ) ? $flx['Courrier']['name'] : ''; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Contact']['name'] ) ? $flx['Contact']['name'] : ''; ?>&nbsp;</td>
                            <td><?php echo $flx['Marche']['objet']; ?>&nbsp;</td>
                            <td><?php echo isset( $flx['Lettre']['montant'] ) ? $flx['Lettre']['montant'] : ''; ?>&nbsp;</td>
                            <td class="actions"><?php echo !empty($flx['Courrier']['id']) ? $this->Html->link('<i class="fa fa-eye" aria-hidden="true"></i>',array('controller' => 'courriers', 'action' => 'historiqueGlobal', $flx['Courrier']['id']), array('escape'=>false,'title' => __d('default', 'Button.view'),'target'=>'_blank')): '';?></td>
                        </tr>
                            <?php endforeach; ?>
                    </tbody>
                </table>
                <?php }?>
                <p class='aere'><?php echo $pagination; ?></p>
            </div>
            <div class="controls panel-footer  " role="group" style="width: 100%;"></div>
        </div>
    <?php }else{ ?>
        <div class="row" id="infos_void">
            <div class="table-list" id="infos">
                <h3>Résultats</h3>
                <div class="content">
                    <div class="alert alert-warning" style="margin-top: 30px"> <?php echo __d('recherche', 'Recherche.exportvoid'); ?></div>
                </div>
                <div class="controls panel-footer  " role="group" style="width: 100%;"></div>
            </div>
        </div>
    <?php } ?>

    </div>
</div>
<?php echo $this->Js->writeBuffer(); ?>
<script type="text/javascript">

    $(document).ready(function () {
            $('.form_datetime input').datepicker({
                language: 'fr-FR',
                format: "dd/mm/yyyy",
                weekStart: 1,
                autoclose: true,
                todayBtn: 'linked'
            });
        });


    $('#searchButton').button();

    $("#RechercheCtypedocument").select2({allowClear: true, placeholder: "Sélectionner un type de document"});
    $("#RechercheCnumeroop").select2({allowClear: true, placeholder: "Sélectionner un N° OP"});
    $("#RechercheCnumerorar").select2({allowClear: true, placeholder: "Sélectionner un N° RAR"});


    <?php if( isset($flux) && !empty($flux) ) {?>
    $('#infos_void').show();
    $('#infos_resultat').show();
    $('#liste').hide();
    <?php }else{ ?>
    $('#infos_void').hide();
    $('#infos_resultat').hide();
    $('#liste').show();
    <?php } ?>


    if ($('#listePlis').length > 0) {
        $('#listePlis').bootstrapTable();
    }
    if ($('#listeOS').length > 0) {
        $('#listeOS').bootstrapTable();
    }

    $(document).ready(function () {
        $('td.actions').css({'width': '80px', 'text-align': 'center'});
        $('td.actions').css({'width': '80px', 'text-align': 'center'});
    });


    $('#searchButton').click(function () {
         if ($("#RechercheCtypedocument").val() != "" ){
            gui.request({
                url: $("#RechercheExportForm").attr('action'),
                data: $("#RechercheExportForm").serialize(),
                loader: true,
                updateElement: $('#webgfc_content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#webgfc_content').html(data);
                $('#infos_void').show();
                $('#infos_resultat').show();
                $('#liste').hide();
            });
        }
        else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez renseigner au moins un type de document !",
                type: "error",

            });
        }

    });

    // Bouton pour le tableau de nb de flux
    gui.addbuttons({
        element: $('#infos_resultat .controls'),
        buttons: [,
            {
                content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
                title: "",
                class: "btn-info-webgfc btn-inverse ",
                action: function () {
                    $('#infos_void').hide();
                    $('#infos_resultat').hide();
                    $('#liste').show();
                }
            },
            {
                content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
                title: "",
                class: "btn-info-webgfc ",
                action: function () {
                    window.location.href = "<?php echo Router::url( array( 'controller' => 'recherches', 'action' => 'exportLettre' ) + Hash::flatten( $this->request->data, '__' ) );?>";
                }
            }
        ]
    });

    gui.addbutton({
        element: $('#infos_void .controls'),
        button: {
            content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
            title: "",
            class: "btn-info-webgfc btn-inverse ",
            action: function () {
                $('#infos_void').hide();
                $('#infos_resultat').hide();
                $('#liste').show();
            }
        }
    });

    // Ajout de l'action de recherche via la touche Entrée
    $('#liste .rechercheCondition').keypress(function (e) {
        if (e.keyCode == 13) {
            gui.request({
                url: $("#RechercheExportForm").attr('action'),
                data: $("#RechercheExportForm").serialize(),
                loader: true,
                updateElement: $('#webgfc_content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#webgfc_content').html(data);
            });
        }
    });


    $('.fa .fa-eye').click(function (e) {
        $('#infos').modal('show');
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/consultations/edit"; ?>/" + item,
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    });

</script>
