<?php

/**
 *
 * Addressbooks/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php //echo $this->Html->css(array('administration'), null, array('inline' => false)); ?>

<script type="text/javascript">

    function loadBanscommunes() {
        $('#infos .content').empty();
        $('#liste table tr').removeClass('ui-state-focus');
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/bans/getBan"; ?>",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
</script>

<div class="container">
    <div id="liste" class="row">
        <div  class="table-list">
            <h3><?php echo __d('bancommune', 'Bancommune.liste'); ?></h3>
            <div class="content">
            </div>
        </div>
        <div class="controls panel-footer">
        </div>
    </div>
</div>

<div id="infos" class="modal fade" role="dialog">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                        <h4 class="panel-title"><?php echo __d('bancommune', 'Bancommune.infos');?></h4>
                    </div>
                    <div class="panel-body content">
                    </div>
                    <div class="panel-footer controls " role="group">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    gui.buttonbox({
        element: $('#liste .controls')
    });

    gui.buttonbox({
        element: $('#infos .controls'),
        align: "center"
    });

    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn btn-danger-webgfc btn-inverse ",
                action: function () {
                    $(this).parents('.modal').modal('hide');
                    if (!$(this).hasClass('ui-state-disabled')) {
                        $('#infos .content').empty();
                        $('#liste table tr').removeClass('ui-state-focus');
                    }
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn btn-success ",
                action: function () {
                    var forms = $(this).parents('.modal').find('form');
                    $.each(forms, function (index, form) {
                        var url = form.action;
                        var id = form.id;
                        if (form_validate($('#' + id))) {
                            gui.request({
                                url: url,
                                data: $('#' + id).serialize()
                            }, function (data) {
                                getJsonResponse(data);
                                loadBanscommunes();
                            });
                            $(this).parents('.modal').modal('hide');
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    });
                }
            }
        ]
    });
    loadBanscommunes();
</script>
