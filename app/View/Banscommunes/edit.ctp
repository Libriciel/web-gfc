<?php

/**
 *
 * Metadonnees/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<!--<div class="col-sm-12">-->
<?php
$this->Paginator->options(array(
	'update' => '#banscommunesvalues',
	'evalScripts' => true,
    'before' => 'gui.loader({ element: $("#banscommunesvalues").parent(), message: gui.loaderMessage });',
	'complete' => '$(".loader").remove()',
));

$pagination = '';
if (Set::classicExtract($this->params, 'paging.Banadresse.pageCount') > 1) {
    $pagination = '<div class="pagination">
                            ' . implode(
                                '', Set::filter(
                                    array(
                                        $this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
                                        $this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
                                        $this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
                                        $this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
                                        $this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
                                    )
                                )
                            ) . '
                        </div>';
}
?>
<?php if(!empty($bansadresses)) :?>
    <div id="banscommunesvalues">
        <?php
            $formBancommune = array(
                'name' => 'Bancommune',
                'label_w' => 'col-sm-5',
                'input_w' => 'col-sm-5',
                'input' => array(
                    'Bancommune.id' => array(
                        'inputType' => 'hidden',
                        'items'=>array(
                            'type'=>'hidden'
                        )
                    ),
                     'Bancommune.name' => array(
                        'labelText' =>__d('bancommune', 'Bancommune.name'),
                        'inputType' => 'text',
                        'items'=>array(
                            'required'=>true,
                            'type'=>'text'
                        )
                    )
                )
            );
            echo $this->Formulaire->createForm($formBancommune);
        echo $this->Form->end();
        ?>
        <legend class="bannette" style="width:97%;">Liste des adresses de la commune</legend>
        <?php
            $fields = array(
                'name',
                'numero',
                'rep',
                'nom_voie',
                'nom_afnor',
                'code_insee',
                'code_post',
                'nom_ld',
                'alias',
                'id_fantoir',
                'ident',
                'x',
                'y',
                'lon',
                'lat',
                'active'
            );

            $actions = array();
            $options = array();

            $panelBodyId ="";
            if(!empty($options['panel_id'])){
                $panelBodyId = "id='".$options['panel_id']."'";
            }
            ?>
    <?php
    $data = $this->Liste->drawTbody($bansadresses, 'Banadresse', $fields, $actions, $options);
    ?>
        <!--</div>-->
        <div  class="bannette_panel panel-body" <?php echo $panelBodyId; ?>>
            <table id="table_banscommunes"
                   data-toggle="table"
                   data-search="true"
                   data-locale = "fr-CA"
                   data-height="400"
                   data-show-refresh="false"
                   data-show-toggle="false"
                   data-show-columns="true"
                   data-toolbar="#toolbar">
            </table>
        </div>
        <?php echo $pagination; ?>
    </div>
<!--</div>-->
<script>
    $('#infos h4 span').remove();
    $('#infos h4').append('<span> - Total <?php echo $nbBanadresse; ?></span>');

    $('#table_banscommunes')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d("banadresse","Banadresse.name"); ?>",
                        class: "name"
                    },
                    {
                        field: "numero",
                        title: "<?php echo __d("banadresse","Banadresse.numero"); ?>",
                        class: "numero"
                    },
                    {
                        field: "rep",
                        title: "<?php echo __d("banadresse","Banadresse.rep"); ?>",
                        visible: false,
                        class: "rep"
                    },
                    {
                        field: "nom_voie",
                        title: "<?php echo __d("banadresse","Banadresse.nom_voie"); ?>",
                        class: "nom_voie"
                    },
                    {
                        field: "nom_afnor",
                        title: "<?php echo __d("banadresse","Banadresse.nom_afnor"); ?>",
                        visible: false,
                        class: "nom_afnor"
                    },
                    {
                        field: "code_insee",
                        title: "<?php echo __d("banadresse","Banadresse.code_insee"); ?>",
                        visible: false,
                        class: "code_insee"
                    },
                    {
                        field: "code_post",
                        title: "<?php echo __d("banadresse","Banadresse.code_post"); ?>",
                        class: "code_post"
                    },
                    {
                        field: "nom_ld",
                        title: "<?php echo __d("banadresse","Banadresse.nom_ld"); ?>",
                        visible: false,
                        class: "nom_ld"
                    },
                    {
                        field: "alias",
                        title: "<?php echo __d("banadresse","Banadresse.alias"); ?>",
                        visible: false,
                        class: "alias"
                    },
                    {
                        field: "id_fantoir",
                        title: "<?php echo __d("banadresse","Banadresse.id_fantoir"); ?>",
                        visible: false,
                        class: "id_fantoir"
                    },
                    {
                        field: "ident",
                        title: "<?php echo __d("banadresse","Banadresse.ident"); ?>",
                        visible: false,
                        class: "ident"
                    },
                    {
                        field: "x",
                        title: "<?php echo __d("banadresse","Banadresse.x"); ?>",
                        visible: false,
                        class: "x"
                    },
                    {
                        field: "y",
                        title: "<?php echo __d("banadresse","Banadresse.y"); ?>",
                        visible: false,
                        class: "y"
                    },
                    {
                        field: "lon",
                        title: "<?php echo __d("banadresse","Banadresse.lon"); ?>",
                        visible: false,
                        class: "lon"
                    },
                    {
                        field: "lat",
                        title: "<?php echo __d("banadresse","Banadresse.lat"); ?>",
                        visible: false,
                        class: "lat"
                    },
                    {
                        field: "active",
                        title: "<?php echo __d("banadresse","Banadresse.active"); ?>",
                        class: "active"
                    },
                ]
            });


            $(".fixed-table-container").css('height', $(window).height() * 0.50);
            $(".fixed-table-body").css('height', '90%');
</script>
    <?php echo $this->LIste->drawScript($bansadresses, 'Banadresse', $fields, $actions, $options); ?>
    <?php echo $this->Js->writeBuffer();?>
<?php endif;?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
    });
</script>
