<ls-lib-login-config configuration='<?php echo $entreeExistante['Configuration']['config'];?>'></ls-lib-login-config>

<script type='text/javascript'>
    $('#webgfc_content').css( 'background-color', '#f7f7f7' );
    $('#webgfc_content').css( 'padding', '30px' );
    $('#webgfc_content').css( 'box-shadow', '0 0 5px rgba(0,0,0,0.5)' );



    document.getElementsByTagName('ls-lib-login-config')[0].addEventListener('save', function(e) {
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Configurations/config',
            data: e['detail'],
            success: function (data) {
                if (data.length > 0) {
                    layer.msg("Les informations ont bien été enregistrées");
                }
            }
        });
        layer.msg("Les informations ont bien été enregistrées");
    });

    document.getElementsByTagName('ls-lib-login-config')[0].addEventListener('back', function(e) {
        window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index"; ?>";
    });
</script>
