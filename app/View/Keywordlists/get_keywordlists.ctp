<?php

/**
 *
 * Keywordlists/get_keywordlists.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//Debugger::log($keywordlist);
if (!empty($keywordlists)) {
    $options = array(
        'nodes' => array(
            array(
                'model' => 'Keywordlist',
                'field' => 'nom',
                'actions' => array(
                    "view" => array(
                        "ico" => 'fa fa-eye',
                        "url" => '/keywordlists/view',
                        "updateElement" => "$('#infos .content')",
                        "formMessage" => false,
//                        "icoColor" => "#5397a7"
                    ),
                    "edit" => array(
                        "ico" => 'fa fa-pencil',
                        "url" => '/keywordlists/edit',
                        "updateElement" => "$('#infos .content')",
                        "formMessage" => false,
//                        "icoColor" => "#5397a7"
                    )
                )
            )
        ),
        'listId' => 'browser',
        'listClass' => 'filetree thesaurusList'
    );
    echo $this->Liste->jqueryListView($keywordlists, $options);
} else {
    echo $this->Html->div('alert alert-warning',__d('keyword', 'Keywordlist.void'));
}
?>

<script type="text/javascript">
    $("#browser").treeview({collapsed: true});
    $('.treeviewBttn i').each(function () {
        var parentSpan = $(this).parent().parent();
        var parentLink = $(this).parent();
        var href = $(this).parent().attr('href');
        var img = $(this).detach();
        img.appendTo(parentSpan).click(function () {
            if ($(this).hasClass('editBttn')) {
                $('#infos').modal('show');
                gui.request({
                    url: href,
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }
            if ($(this).hasClass('viewBttn')) {
                gui.formMessage({
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                    url: href,
                    buttons: {
                        "<i class='fa fa-times' aria-hidden='true'></i> Fermer": function () {
                            $(this).parents('.modal').modal('hide');
                            $(this).parents('.modal').empty();
                        }
                    }
                });
            }
        });
        parentLink.remove();
    });

    $('.treeview span.folder, .treeview span.file').addClass('ui-corner-all').hover(function () {
        $(this).addClass('well').css('border', 'none').css('margin-bottom', '0px');
    }, function () {
        $(this).removeClass('well').css('border', 'none');
    })
</script>
