<?php

/**
 *
 * Keywordlists/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$content = '';
foreach ($this->data['Keywordlist'] as $key => $val) {
    if (!preg_match('/.*_id$/', $key) && $key != 'id' && $key != 'active') {
        $dtcontent = __d('keyword', 'Keywordlist.' . $key);
        if ($key == 'nom' && $this->data['Keywordlist']['active']) {
            $dtcontent .= $this->Html->tag('span', $this->Html->image('/img/link.png', array('alt' => 'lié à la collectivité', 'title' => 'lié à la collectivité')), array('style' => 'float:right;'));
        }
        $content .='<tr>';
        $content .= $this->Html->tag('td', $dtcontent,array('style'=>'font-weight: bold;color:#000;'));
        $printval = $val;
        if ($key == 'modified' || $key == 'created') {
            $tmp = explode(' ', $val);
            $tmp2 = explode('-', $tmp[0]);
            $month = $tmp2[1];
            $day = $tmp2[2];
            $year = $tmp2[0];
            $printval = date('d/m/Y', mktime(0, 0, 0, $month, $day, $year));
        }
        if ($key == 'scheme_data_uri' || $key == 'scheme_uri') {
            $printval = $this->Html->link($val, $val, array('target' => '_blank'));
        }
        $content .= $this->Html->tag('td', $printval);
        $content .='</tr>';
    }
}
$tbody = $this->Html->tag('tbody', $content);
echo $this->Html->tag('table',$tbody,array('class'=>'table table-striped table-bordered table-hover'));
?>
