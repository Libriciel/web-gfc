<?php

/**
 *
 * Keywordlists/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$form = $this->Form->create('Keywordlist');
$form .= $this->Form->input('Keywordlist.id');
$form .= $this->Form->input('Keywordlist.active', array('label' => false, 'div' => false));
$form .= $this->Form->end();

$content = '';
foreach ($keywords as $item) {
    $content .= $this->Html->tag('li', $item);
}
$ul = $this->Html->tag('ul', $content,array('style'=>'overflow: auto;max-height: 200px;'));
?>

<table class="table table-striped table-bordered">
    <tbody>
        <tr>
            <td style="font-weight: bold;color:#000"><?php echo __d('keyword', 'Keywordlist.nom'); ?></td>
            <td><?php echo $this->data['Keywordlist']['nom']; ?></td>
        </tr>
        <tr>
            <td style="font-weight: bold;color:#000">Lié à la collectivité</td>
            <td><?php echo $form; ?></td>
        </tr>
        <tr>
            <td style="font-weight: bold;color:#000">Mot-clès associés</td>
            <td><?php echo $ul; ?></td>
        </tr>
    </tbody>
</table>

<script type="text/javascript">
    $('#KeywordlistActive').css('margin-top', '-8px')
</script>
