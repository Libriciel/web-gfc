<?php

/**
 *
 * Organismes/get_organismes.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$this->Paginator->options(array(
   'update' => '#zone_contact .panel-body',
   'evalScripts' => true,
   'before' => 'gui.loader({ element: $("#zone_contact .panel-body"), message: gui.loaderMessage });',
   'complete' => '$(".loader").remove()'
   ));
$pagination = '';
if (Set::classicExtract($this->params, 'paging.Contact.pageCount') > 1) {
   $pagination = '<div class="pagination">
   ' . implode(
     '', Set::filter(
       array(
          $this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
          $this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
          )
       )
     ) . '
   </div>';
}
?>

<table id="table_contact"
       data-toggle="table"
       data-locale = "fr-CA"
       data-height="499"
       >
</table>
<div>
    <?php echo $pagination; ?>
    <!-- fonction export recherche résultat -->
    <?php if(!empty($contacts)):
        $formSearch = array(
            'name' => 'exportContact',
            'label_w' => 'col-sm-4',
            'input_w' => 'col-sm-6',
            'form_url' =>array('controller' => 'contacts', 'action' => 'export'),
            'form_class'=>'pull-right',
            'input' => array(
                'SearchContact.organisme_id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'SearchContact.name' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'SearchContact.nom' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'SearchContact.prenom' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'SearchContact.active' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
            )
        );
		$formSearch['input']['SearchContact.Fonction.id']=array(
			'inputType' => 'hidden',
			'items'=>array(
				'type'=>'hidden'
			)
		);
    if( Configure::read('Conf.SAERP')) {
         $formSearch['input']['SearchContact.Operation.id']=array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
         );
         $formSearch['input']['SearchContact.Event.id']=array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
         );
    }
    echo $this->Formulaire->createForm($formSearch);
    ?>

    <button type="submit" class="btn btn-info-webgfc pull-right" id="exportResultat" style="margin:20px auto;"><i class="fa fa-download" aria-hidden="true"></i></button>
    <?php
    $this->Form->end();
    endif; ?>
    <!-- fin fonction export recherche résultat-->
</div>
<script>
    $('#table_contact')
            .bootstrapTable({
                data: <?php echo json_encode($contacts); ?>,
                columns: [
                    {
                        field: "name",
                        title: "Nom",
                        class: "name"
                    },
                    {
                        field: "view",
                        title: "",
                        align: "center"

                    },
                    {
                        field: "edit",
                        title: "",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "",
                        align: "center"
                    },
                    {
                        field: "exportcsv",
                        title: "",
                        align: "center"
                    }
                ]
            }).on('search.bs.table', function (e, text) {
        chargerFunctionJquery();
    });
    $(document).ready(function () {
        chargerFunctionJquery();
        $('#exportContactExportForm input').each(function () {
            var idInput = $(this).attr('id');
            if ($('#searchModal form #' + idInput).val().length != 0) {
                $(this).val($('#searchModal form #' + idInput).val());
            }
        });

        // export des contacts
        $('.exportContact').click(function () {
            var id = $(this).attr('id').substring(8);
            var href = '/contacts/export/' + id;
            exportItem(href);
        });
        function exportItem(href) {
            window.location.href = href;
        }

    });
    function chargerFunctionJquery() {
        $.getScript("/js/addressbook.js");
    }
</script>
<?php echo $this->Js->writeBuffer();?>
