
<?php
/**
 *
 * Contacts/get_contactsform.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->link(
    $this->Html->image(
        'icons/application_form_magnify.png',
        array( 'alt' => '' )
    ).' Formulaire',
    '#',
    array( 'escape' => false, 'title' => 'Visibilité formulaire', 'onclick' => "$( '#zoneContactGetContactformForm' ).toggle(); return false;", 'id' => 'formulaireButton', 'class' => 'aere' )
);

$formGetContactForm = array(
        'name' => 'Contact',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-6',
        'form_type' => 'post',
		'form_url' => array( 'controller' => 'contacts', 'action' => 'get_contactform' ),
        'input' =>array(
            'SearchContact.name' => array(
                'labelText' =>__d('addressbook', 'Contact.name'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required' => false
                )
            ),
            'SearchContact.nom' => array(
                'labelText' =>__d('addressbook', 'Contact.nom'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required' => false
                )
            ),
            'SearchContact.prenom' => array(
                'labelText' =>__d('addressbook', 'Contact.prenom'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            )
        )
    );
    if( Configure::read('Conf.SAERP')) {
        $formGetContactForm['input']['SearchContact.fonction_id'] = array(
            'labelText' =>'Fonction',
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $fonctions,
                'required' =>false,
                'empyt'=>true
            )
        );
        $formGetContactForm['input']['SearchContact.Event'] = array(
            'labelText' =>'Evénements',
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $events,
                'required' =>false,
                'empyt'=>true,
                'multiple' => true
            )
        );
    }
    $formGetContactForm['input']['SearchContact.active'] = array(
        'labelText' =>__d('contact', 'Contact.active'),
        'inputType' => 'checkbox',
        'items'=>array(
            'type'=>'checkbox',
            'checked'=>true,
        )
    );


?>
<fieldset class="aere zone-form" id = "zoneContactGetContactformForm">
    <legend>Recherche de contact</legend>
    <?php
        echo $this->Formulaire->createForm($formGetContactForm);
    ?>
    <div class="controls text-center" role="group" >
        <a id="searchButton" class="btn btn-info-webgfc" title="Rechercher"><i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
    </div>
    <?php echo $this->Form->end();?>
</fieldset>




<script type="text/javascript">
    $('#ContactGetContactformForm div.required').removeClass('required');
    $('#formulaireButton').button();
    $('#searchButton').button();
    $('#formulaireButton').css('margin-bottom', '1em');
    $('.paging').css('margin-bottom', '1em');


    $('#searchButton').click(function () {
        gui.request({
            url: $("#ContactGetContactformForm").attr('action'),
            data: $("#ContactGetContactformForm").serialize(),
            loader: true,
            updateElement: $('#infos .content'),
            loaderMessage: gui.loaderMessage
        }, function (data) {

            $('#infos .content').html(data);
        });
    });

    // Ajout de l'action de recherche via la touche Entrée
    $('#ContactGetContactformForm').keyup(function (e) {
        if (e.keyCode == 13) {
            gui.request({
                url: $("#ContactGetContactformForm").attr('action'),
                data: $("#ContactGetContactformForm").serialize(),
                loader: true,
                updateElement: $('#infos .content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#infos .content').html(data);
            });
        }
    });

    $('#ContactGetContactformForm.folded').hide();

<?php if( Configure::read('Conf.SAERP') ) :?>
    $('#SearchContactFonctionId').select2({allowClear: true, placeholder: "Sélectionner une fonction"});
    $('#SearchContactEvent').select2({allowClear: true, placeholder: "Sélectionner un évenement"});
<?php endif;?>
</script>
<?php
$this->Paginator->options(array(
	'update' => '#infos .content',
	'evalScripts' => true,
                'before' => 'gui.loader({ element: $("#infos .content"), message: gui.loaderMessage });',
	'complete' => '$(".loader").remove()',
));

$pagination = '';
if (Set::classicExtract($this->params, 'paging.Contact.pageCount') > 1) {
    $pagination = '<div class="paging">
                                    ' . implode(
                                        ' | ', Set::filter(
                                            array(
                                                $this->Paginator->first('<<', array(), null, array('class' => 'disabled')),
                                                $this->Paginator->prev('<', array(), null, array('class' => 'disabled')),
                                                $this->Paginator->numbers(),
                                                $this->Paginator->next('>', array(), null, array('class' => 'disabled')),
                                                $this->Paginator->last('>>', array(), null, array('class' => 'disabled')),
                                            )
                                        )
                                    ) . '
                            </div>';
}
echo $pagination;



if (!empty($contacts)) {
    $fields = array(
        'name',
        'organisme_name',
        'carnet_name'
    );
    $actions = array(
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/contacts/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false,
            "refreshAction" => "loadContactsForm();"
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/contacts/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadContactsForm();"
        ),
        "export" => array(
            "url" => Configure::read('BaseUrl') . '/contacts/export',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        )
    );
    $options = array();
    $panelBodyId ="";
    if(!empty($options['panel_id'])){
        $panelBodyId = "id='".$options['panel_id']."'";
    }
?>

<div class="panel">
    <div class="panel panel-default">
        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
            <?php
            echo $this->Liste->drawPanelHeading($contacts,$options);
            $data = $this->Liste->drawTbody($contacts, 'Contact', $fields, $actions, $options);
            ?>
        </div>
        <div  class="bannette_panel panel-body" <?php echo $panelBodyId; ?>>
            <table id="table_contactfrom"
                   data-toggle="table"
                   data-search="true"
                   data-locale = "fr-CA"
                   data-height="400"
                   data-show-refresh="false"
                   data-show-toggle="false"
                   data-show-columns="true"
                   data-toolbar="#toolbar">
            </table>
        </div>
    </div>
</div>
<script>
    $('#table_contactfrom')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d("contact","Contact.name"); ?>",
                        class: "name"
                    },
                    {
                        field: "organisme_name",
                        title: "<?php echo __d("contact","Contact.organisme_name"); ?>",
                        class: "organisme_name"
                    },
                    {
                        field: "carnet_name",
                        title: "<?php echo __d("contact","Contact.carnet_name"); ?>",
                        class: "carnet_name"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        align: "center",
                        width: "80px"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        align: "center",
                        width: "80px"
                    },
                    {
                        field: "export",
                        title: "Exporter",
                        class: "actions thExport",
                        align: "center",
                        width: "80px"
                    }
                ]
            });
</script>
<?php
    echo $this->LIste->drawScript($contacts, 'Contact', $fields, $actions, $options);
    echo $this->Js->writeBuffer();
} else {
    echo $this->Html->tag('div', __d('contact', 'Contact.void'), array('class' => 'voidMessage'));
}
?>

<script type="text/javascript">
    $('#infos .title').empty();
    $('#infos .title').append('Contacts');
    $('#infos .controls .ui-button').show();
    $('#infos .controls .subFormImport').remove();

    $("#browserContact").treeview({collapsed: true});
    $('#addressbooks_skel #infos div.content div.bannette_panel table img.itemEdit').unbind('click');

    $('#addressbooks_skel #infos div.content div.bannette_panel table img.itemEdit').bind('click', function () {
        var itemId = $(this).attr('itemId');
        gui.formMessage({
            updateElement: $("#infos .content"),
            loader: true,
            width: 700,
            loaderMessage: gui.loaderMessage,
            title: '<?php echo __d('contact', 'Contact.edit'); ?>',
            url: "/contacts/edit/" + itemId,
            buttons: {
                "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                    $(this).parents('.modal').modal('hide');
                },
                "<?php echo __d('default', 'Button.submit'); ?>": function () {
                    var form = $('#ContactEditForm');
                    if (form_validate(form)) {
                        gui.request({
                            url: form.attr('action'),
                            data: form.serialize()
                        }, function (data) {
                            getJsonResponse(data);
                            loadContactsForm();
                        });
                        $(this).parents('.modal').modal('hide');
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        });
    });


    $('.treeview span.folder, .treeview span.file').addClass('ui-corner-all').hover(function () {
        $(this).addClass('alert alert-warning').css('border', 'none');
    }, function () {
        $(this).removeClass('alert alert-warning').css('border', 'none');
    })


    var checkHomonyms = function () {
        $('.homonyms').remove();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Contacts/getHomonyms',
            data: $('#ContactGetContactformForm').serialize(),
            success: function (data) {

                if (data.length > 0) {
                    var homonyms = $('<div></div>').html('<?php echo __d('contact', 'Contact.HomonymsListTitle'); ?>').addClass('homonyms').addClass('alert alert-warning');
                    var homonymList = $('<ul></ul>');

                    for (var i in data) {
                        homonymList.append($('<li></li>').html(data[i].Contact.name + ' (' + data[i].Addressbook.name + ' / ' + data[i].Organisme.name + ')'));
                    }
                    homonyms.append(homonymList);

                    $('#ContactName').parent().after(homonyms);

                }
            }
        });

    }
    $('#ContactNom, #ContactPrenom').blur(function () {
        if ($('#ContactPrenom').val() != "" || $('#ContactNom').val() != "") {
            $('#ContactName').val($('#ContactPrenom').val() + " " + $('#ContactNom').val());
            checkHomonyms();
        }
    });

    $('#ContactName').change(function () {
        checkHomonyms();
    });
    $('#ContactGetContactformForm').addClass('checkHomonyms');

    $(document).ready(function () {
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
    });
</script>
