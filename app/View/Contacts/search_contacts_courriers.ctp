<?php

    $formSearch = array(
        'name' => 'searchContactsCourriers',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-3',
        'form_url' =>array('controller' => 'contacts', 'action' => 'search_contacts_courriers', $orgId),
//        'form_class'=>'pull-right',
        'input' => array(
            'Contact.name' => array(
                'labelText' =>__d('contact', 'Contact.name'),
                'inputType' => 'text',
                'items'=>array(
                    'type' => 'text',
                    'required' => true,
                    'empty' => false
                )
            )
        )
    );
?>
<div  class="zone-form">
    <legend>Recherche de contacts</legend>
        <?php
            echo $this->Formulaire->createForm($formSearch);
        ?>
    <div class="controls  text-center" role="group" >
        <a id="searchButton" class="aere btn btn-success " ><i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
        <a id="closeButton" class="aere btn btn-info-webgfc " ><i class="fa fa-times" aria-hidden="true"></i> Fermer</a>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<div id="contactsvalues" >
    <legend class="bannette">Liste des contacts</legend>
    <div  class="bannette_panel panel-body">
        <table id="table_contacts_edit"
               data-toggle="table"
               data-locale = "fr-CA"
               data-pagination="true"
               data-page-size =" 100"
               >
        </table>
    </div>
</div>
    <script type="text/javascript">
        //title legend (nombre de données)
        if ($('#contactsvalues .bannette span').length < 1) {
            $('#contactsvalues .bannette ').append('<span> - total : <?php echo count($contacts);?></span>');
        }
        $('#table_contacts_edit')
            .bootstrapTable({
                data:<?php echo json_encode($contacts);?>,
                columns: [
                    {
                        field: 'id',
                        title: "Identifiant",
                        class: 'hidden'
                    },
                    {
                        field: 'name',
                        title: "<?php echo __d('contact','Contact.name'); ?>",
                        class: 'name'
                    },
					{
						field: 'fonction',
						title: "Fonction",
						class: 'fonction'
					},
					{
						field: 'titre',
						title: "Titre",
						class: 'titre'
					},
                    {
                        field: 'ville',
                        title: "<?php echo __d('contact','Contact.ville'); ?>",
                        class: 'ville'
                    }
                ]
            });
    </script>
<?php
    echo $this->Js->writeBuffer();
?>


<script type="text/javascript">
    $('#searchButton').button();
	$('#closeButton').button();
    $('.zone-form').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#searchButton').trigger('click');
            return false;
        }
    });
    $('#searchButton').click(function () {
        gui.request({
            url: '/contacts/search_contacts_courriers/' + "<?php echo $orgId;?>",
            data: $("#searchContactsCourriersSearchContactsCourriersForm").serialize(),
            loader: true,
            updateElement: $('#table_contacts_edit'),
            loaderMessage: gui.loaderMessage,
        }, function (data) {
            $('.zone-form').remove();
            $('#contactsvalues').html(data);
        });
    });

	$('#closeButton').click(function () {
		$(this).parents(".modal").modal('hide');
		$(this).parents(".modal").empty();
	});

    $('#table_contacts_edit tbody tr').css('cursor', 'pointer');
    $('#table_contacts_edit tbody tr').click(function () {
        var contactId = $(this).children('.hidden').text();
        var contactName = $(this).children('.name').text();
        $(this).css('background', 'grey');

        $(".modal").modal('hide');
        $(".modal").empty();
        refreshContactvalue(contactId, contactName);
    });


    function refreshContactvalue(contactId, contactName) {
        $("#CourrierContactId").append($("<option value='" + contactId + "' selected='selected'>" + contactName + "</option>"));
        $('#CourrierContactId').val(contactId).change();
        $( '#select2-chosen-2' ).html(contactName);

        $('#HiddenCourrierContactId').val(contactId);
    }
</script>
