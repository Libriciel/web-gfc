<?php

/**
 *
 * Contacts/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('jquery/jquery.ui.autocomplete.html.js', array('inline' => true));
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div id="edit_contact">
    <div class="row">
    <?php
    $formContact = array(
        'name' => 'Contact',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-8',
        'input' =>array()
    );
    echo $this->Formulaire->createForm($formContact);
    asort( $civilite );
    ?>
        <div class="col-sm-6">
            <legend><?php echo  __d('addressbook', 'Contact.fieldset.identity') ?></legend>
        <?php
         $formAddContactIdent = array(
            'name' => 'Courrier',
            'label_w' => 'col-sm-5',
            'input_w' => 'col-sm-7',
            'input' => array(
                'Contact.id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'Contact.addressbook_id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden',
                        'value' => $addressbook_id
                    )
                ),
                'Contact.civilite' => array(
                    'labelText' =>__d('addressbook', 'Contact.civilite'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'options' => $civilite
//                        'value' => $this->request->data['Contact']['civilite']
                    )
                ),
                'Contact.nom' => array(
                    'labelText' =>__d('contact', 'Contact.nom'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text',
                        'required' => true
                    )
                ),
                'Contact.prenom' => array(
                    'labelText' =>__d('contact', 'Contact.prenom'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Contact.role' => array(
                    'labelText' =>__d('contact', 'Contact.role'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Contact.titre_id' => array(
                    'labelText' =>__d('contact', 'Contact.titre_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'options'=> $titres,
                        'empty' => true
                    )
                ),
                'Contact.name' => array(
                    'labelText' =>__d('contact', 'Contact.name'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                )
            )
        );
         echo $this->Formulaire->createForm($formAddContactIdent);
        ?>
            <legend><?php echo  __d('addressbook', 'Contactinfo.fieldset.work') ?></legend>
        <?php
           $formAddContactLien = array(
            'name' => 'Courrier',
            'label_w' => 'col-sm-5',
            'input_w' => 'col-sm-7',
            'input' => array(
                'Contact.organisme_id' => array(
                    'labelText' =>__d('contact', 'Contact.organisme_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty'=>true,
                        'options'=>$organismes,
                        'required'=>true
                    )
                ),
                 'Contact.email' => array(
                    'labelText' =>__d('contact', 'Contact.email'),
                    'inputType' => 'email',
                    'items'=>array(
                        'type'=>'email'
                    )
                ),
                'Contact.tel' => array(
                    'labelText' =>__d('contact', 'Contact.tel'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Contact.portable' => array(
                    'labelText' =>__d('contact', 'Contact.portable'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Contact.lignedirecte' => array(
                    'labelText' =>__d('contact', 'Contact.lignedirecte'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                )
            )
        );
		$formAddContactLien['input']['Contact.fonction_id'] = array(
			'labelText' =>__d('contact', 'Contact.fonction_id'),
			'inputType' => 'select',
			'items'=>array(
				'type'=>'select',
				'empty' => true,
				'options' => $fonctions
			)
		);
        if(Configure::read('Conf.SAERP')){
            $formAddContactLien['input']['Operation.Operation'] = array(
                'labelText' =>__d('contact', 'Contact.operation_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'empty' => true,
                    'multiple' => true,
                    'options' => $operations
                )
            );
            $formAddContactLien['input']['Event.Event'] = array(
                'labelText' =>__d('contact', 'Contact.event_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'empty' => true,
                    'multiple' => true,
                    'options' => $events
                )
            );
            $formAddContactLien['input']['Contact.observations'] = array(
                'labelText' =>__d('contact', 'Contact.observations'),
                'inputType' => 'textarea',
                'items'=>array(
                    'type'=>'textarea'
                )
            );
        }
         echo $this->Formulaire->createForm($formAddContactLien);
        ?>
        </div>
        <div class="panel col-sm-6" >
            <legend><?php echo __d('contact', 'Contact.adresses') ?></legend>
        <?php
        $formAddContactAdd = array(
            'name' => 'Courrier',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-6',
            'input' => array(
                'Contact.adressecomplete' => array(
                    'labelText' =>__d('contact', 'Contact.adressecompleteLable'),
                    'inputType' => 'text',
                    'items'=>array(
                        'title'=>__d('contact', 'Contact.adressecompleteTitle'),
                        'type'=>'text'
                    )
                ),
                'Contact.ban_id' => array(
                    'labelText' =>__d('contact', 'Contact.ban_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'options' => array()
                    )
                ),
                'Contact.bancommune_id' => array(
                    'labelText' =>__d('contact', 'Contact.bancommune_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'options' => array()
                    )
                ),
                'Contact.banadresse' => array(
                    'labelText' =>__d('contact', 'Contact.banadresse'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'options' => array()
                    )
                ),
                'Contact.numvoie' => array(
                    'labelText' =>__d('contact', 'Contact.numvoie'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Contact.nomvoie' => array(
                    'labelText' =>__d('contact', 'Contact.nomvoie'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Contact.compl' => array(
                    'labelText' =>__d('contact', 'Contact.compl'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
				'Contact.compl2' => array(
					'labelText' =>__d('contact', 'Contact.compl2'),
					'inputType' => 'text',
					'items'=>array(
						'type'=>'text'
					)
				),
                'Contact.cp' => array(
                    'labelText' =>__d('contact', 'Contact.cp'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Contact.ville' => array(
                    'labelText' =>__d('contact', 'Contact.ville'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
				'Contact.cedex' => array(
					'labelText' =>__d('contact', 'Contact.cedex'),
					'inputType' => 'text',
					'items'=>array(
						'type'=>'text'
					)
				),
                'Contact.adresse' => array(
                    'labelText' =>__d('contact', 'Contact.adresse'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text',
                        'readonly' => true
                    )
                ),
                'Contact.pays' => array(
                    'labelText' =>__d('contact', 'Contact.pays'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Contact.active' => array(
                    'labelText' =>__d('contact', 'Contact.active'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['Contact']['active']
                    )
                ),
//                'Contact.organisme_id' => array(
//                    'inputType' => 'hidden',
//                    'items'=>array(
//                        'type'=>'hidden',
//                        'value' => $orgId
//                    )
//                ),
                'Contact.addressbook_id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden',
                        'value' => $addressbook_id
                    )
                ),
            )
        );
        if( Configure::read('CD') == 81 ) {
            $formAddContactAdd['input']['Contact.canton'] = array(
                'labelText' =>__d('contact', 'Contact.canton'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            );
        }
        echo $this->Formulaire->createForm($formAddContactAdd);
        ?>
        </div>
        <div class="col-sm-7"></div>
        <div class="contact-creation-infos col-sm-5 information_update">
            <p><b>Fiche créée le : </b><?php echo $this->Html2->ukToFrenchDateWithHourAndSlashes($this->request->data['Contact']['created']); ?></br>
                <b>Dernière modification le : </b> <?php echo $this->Html2->ukToFrenchDateWithHourAndSlashes($this->request->data['Contact']['modified']); ?></p>
        </div>
    </div>
<?php if(isset($panel_footer) && $panel_footer):?>
    <div class="modal-footer " style="width: 100%">
        <a class="btn btn-danger-webgfc btn-inverse " id="btn_cancel"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a class="btn btn-success " id="btn_ok"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btn_ok').click(function () {
                var form = $('#ContactEditForm');
                if (form_validate(form)) {
                    gui.request({
                        url: form.attr('action'),
                        data: form.serialize()
                    }, function (data) {
                        getJsonResponse(data);
                    });
                    $('#edit_contact').remove();
                    if ($('#panel_get_organismes').length != 0) {
                        $('#panel_get_organismes').show();
                        loadOrganismes(orgId);
                    }
                    if ($('#panel_get_contacts').length != 0) {
                        $("#panel_get_contacts").show();
                        loadContacts(<?php echo $addressbook_id ?>);
                    }
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Oops...",
                        text: "Veuillez vérifier votre formulaire!",
                        type: "error",

                    });
                }
            });
            $('#btn_cancel').click(function () {
                $('#edit_contact').remove();
                if ($('#panel_get_organismes').length != 0) {
                    $('#panel_get_organismes').show();
                }
                if ($('#panel_get_contacts').length != 0) {
                    $("#panel_get_contacts").show();
                }

            });
        });
    </script>
<?php else: ?>
    <script>
        $('#infos .panel-title').empty();
        $('#infos .panel-title').append('Modification : <?php echo $this->request->data['Contact']['name']; ?>');
    </script>
<?php endif; ?>
</div>
<?php
    echo $this->Form->end();
?>
<script type="text/javascript">
    $('#ContactCivilite').select2({allowClear: true, placeholder: "Sélectionner une civilité"});
    $('#ContactBanId').select2({allowClear: true, placeholder: "Sélectionner un département"});
    $('#ContactBancommuneId').select2({allowClear: true, placeholder: "Sélectionner une commune"});
    $('#ContactBanadresse').select2({allowClear: true, placeholder: "Sélectionner une adresse"});
    $('#ContactOrganismeId').select2({allowClear: true, placeholder: "Sélectionner un organisme"});

    gui.enablebutton({
        element: $('#infos .controls'),
        button: "Valider"
    });

    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this,
                    currentCategory = "";

            $.each(items, function (index, item) {
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                self._renderItem(ul, item);
            });

        }

    });
    //contruction du tableau de types / soustypes
    var bans = [];
    <?php foreach ($bans as $ban) { ?>
    var ban = {
        id: "<?php echo $ban['Ban']['id']; ?>",
        name: "<?php echo $ban['Ban']['name']; ?>",
        banscommunes: []
    };
        <?php foreach ($ban['Bancommune'] as $communeB) { ?>
    var bancommune = {
        id: "<?php echo $communeB['id']; ?>",
        name: "<?php echo $communeB['name']; ?>"
    };
    ban.banscommunes.push(bancommune);
                                                <?php } ?>
    bans.push(ban);
    <?php } ?>

    function fillBanscommunes(ban_id, bancommune_id) {
        $("#ContactBancommuneId").empty();
        $("#ContactBancommuneId").append($("<option value=''> --- </option>"));

        for (i in bans) {
            if (bans[i].id == ban_id) {
                for (j in bans[i].banscommunes) {
                    if (bans[i].banscommunes[j].id == bancommune_id) {
                        $("#ContactBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "' selected='selected'>" + bans[i].banscommunes[j].name + "</option>"));
                    } else {
                        $("#ContactBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "'>" + bans[i].banscommunes[j].name + "</option>"));
                    }
                }
            }
        }
    }

    $("#ContactBanId").append($("<option value=''> --- </option>"));
    //remplissage de la liste des types
    for (i in bans) {
        $("#ContactBanId").append($("<option value='" + bans[i].id + "'>" + bans[i].name + "</option>"));
    }
    //definition de l action sur type
    $("#ContactBanId").bind('change', function () {
        var ban_id = $('option:selected', this).attr('value');
        fillBanscommunes(ban_id);
    });


    var checkAdresses = function (bancommune_id) {
        $("#ContactBanadresse").empty();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Organismes/getAdresses/' + bancommune_id,
            data: $('#ContactEditForm').serialize(),
            success: function (data) {
                if (data.length > 0) {
                    $("#ContactBanadresse").append($("<option value=''> --- </option>"));
                    // remplissage de la partie adresse
                    for (var i in data) {
                        if (data[i].Banadresse.nom_afnor == "<?php echo $this->request->data['Contact']['banadresse'];?>") {
                            $("#ContactBanadresse").append($("<option value='" + data[i].Banadresse.nom_afnor + "' selected='selected'>" + data[i].Banadresse.nom_afnor + "</option>"));
                            $("#ContactBanadresse").val(data[i].Banadresse.nom_afnor).change();
                        } else {
                            $("#ContactBanadresse").append($("<option value='" + data[i].Banadresse.nom_afnor + "'>" + data[i].Banadresse.nom_afnor + "</option>"));
                        }
                    }
                }
            }
        });
    }
	if( $("#ContactBancommuneId").val() != null ) {
		$("#ContactBancommuneId").bind('change', function () {
			var bancommune_id = $("#ContactBancommuneId").val();
			if( bancommune_id != '') {
				checkAdresses(bancommune_id);
			}
		});
	}


    // On remplit les listes déroulantes ban_id, banommune_id et banadresse à jour si les infos sont présentes
    <?php if( !empty( $this->request->data['Contact']['ban_id'] ) ) :?>
    $("#ContactBanId").val("<?php echo $this->request->data['Contact']['ban_id'];?>").change();
                <?php else:?>
    <?php if( !Configure::read('Conf.SAERP')) : ?>
    $("#ContactBanId").val("<?php echo isset( $bans[0]['Ban']['id'] ) ? $bans[0]['Ban']['id'] : ''; ?>").change();
    <?php endif;?>
    <?php endif;?>
    <?php if( !empty( $this->request->data['Contact']['bancommune_id'] ) ) :?>
    $("#ContactBancommuneId").val("<?php echo $this->request->data['Contact']['bancommune_id'];?>").change();
    <?php endif;?>

    <?php if( empty( $this->request->data['Contact']['ban_id'] ) ) :?>
        $("#ContactBanId").val("---").change();
    <?php endif;?>

    var checkHomonyms = function () {
        $('.homonyms').remove();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Contacts/getHomonyms',
            data: $('#ContactEditForm').serialize(),
            success: function (data) {

                if (data.length > 0) {
                    var homonyms = $('<div></div>').html('<?php echo __d('contact', 'Contact.HomonymsListTitleEdit'); ?>').addClass('homonyms').addClass('alert alert-warning');
                    var homonymList = $('<ul></ul>');

                    for (var i in data) {
						if( data[i].Contact.adresse == null ) {
							homonymList.append($('<li></li>').html(data[i].Contact.name + ' (' + data[i].Addressbook.name + ' / ' + data[i].Organisme.name + ')'));
						}
						else {
							homonymList.append($('<li></li>').html(data[i].Contact.name + ' (' + data[i].Addressbook.name + ' / ' + data[i].Organisme.name  + ' / <br />' + data[i].Contact.adresse + ' ' + data[i].Contact.cp + ' ' + data[i].Contact.ville + ')'));
						}
                    }
                    homonyms.append(homonymList);

                    $('#ContactName').parent().after(homonyms);

                }
            }
        });

    }
    $('#ContactNom, #ContactPrenom').blur(function () {
		if ($('#ContactPrenom').val() != "" || $('#ContactNom').val() != "") {
			$('#ContactName').val($('#ContactPrenom').val() + " " + $('#ContactNom').val());

			if ($('#ContactId').val() != '<?php echo $id;?>') {
				checkHomonyms();
			}
		}
    });

    $('#ContactName').change(function () {
        checkHomonyms();
    });
    $('#ContactEditForm').addClass('checkHomonyms');

    // Using jQuery UI's autocomplete widget:
    $('#ContactAdressecomplete').bind("keydown", function (event) {
        //keycodes - maj : 16, ctrl : 17, alt : 18
        if (event.keyCode != 16 &&
                event.keyCode != 17 &&
                event.keyCode != 18) {
            $('#ContactBanId').val(0);
            $('#ContactBancommuneId').html('');
        }
        if ($(this).attr('value') != '') {
            if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).data("autocomplete").menu.active) {
                event.preventDefault();
            }
        }
    }).catcomplete({
        minLength: 3,
        source: '/contacts/searchAdresse',
        select: function (event, ui) {
            gui.request({
                url: '/contacts/searchAdresseSpecifique/' + ui.item.id
            }, function (data) {
                $('#ContactBancommuneId').val(ui.item.id);
                var json = jQuery.parseJSON(data);
                var banadresse = json['Banadresse'];
                var banville = json['Bancommune'];
                var ban = json['Ban'];
                var num = banadresse['numero'];
                if (typeof banadresse['rep'] !== 'undefined') {
                    num = banadresse['numero'] + " " + banadresse['rep'];
                }
                if (typeof banadresse['id'] !== 'undefined') {
                    $('#ContactBanId').val(ban['id']).change();
                    $('#ContactBancommuneId').val(banville['id']).change();

                    $("#ContactBanadresse").append($("<option value='" + banadresse['nom_afnor'] + "' selected='selected'>" + banadresse['nom_afnor'] + "</option>"));
                    $("#ContactBanadresse").val(banadresse['nom_afnor']).change();
                    $('#ContactNumvoie').val(num);
                    if( banadresse['nom_afnor'] != "" ) {
                        $('#ContactNomvoie').val(banadresse['nom_afnor']);
                    }
                    else {
                        $('#ContactNomvoie').val(banadresse['nom_voie']);
                    }
                    $('#ContactCp').val(banadresse['code_post']);
                    $('#ContactVille').val(banville['name']);
                     <?php if( Configure::read('CD') == 81 ):?>
                        $('#ContactCanton').val(banadresse['canton']);
                    <?php endif;?>
                }
            });
        }
    });


    // Partie pour mettre à jour le code postal selon le nom de voie sélectionné
    $('#ContactBanadresse').change(function () {
//        refreshCodepostal($("#ContactBancommuneId").val(), $("#ContactBanadresse").val());
    });
    var refreshCodepostal = function (bancommuneId, banadresseName) {
        var params = '';
        if (banadresseName != '') {
            var params = bancommuneId + '/' + banadresseName;
        }
        else {
            params = bancommuneId;
        }
        gui.request({
            url: '/bansadresses/getCodepostal/' + params
        }, function (data) {
            var json = jQuery.parseJSON(data);
            var codepostal = json;
            if( codepostal['nom_afnor'] != "" ) {
                $('#ContactNomvoie').val(codepostal['nom_afnor']);
            }
            else {
                $('#ContactNomvoie').val(codepostal['nom_voie']);
            }
            $('#ContactCp').val(codepostal['codepostal']);
            $('#ContactVille').val(codepostal['ville']);
            <?php if( Configure::read('CD') == 81 ):?>
                $('#ContactCanton').val(codepostal['canton']);
            <?php endif;?>
        });
    }
	$('#ContactFonctionId').select2({allowClear: true, placeholder: "Sélectionner une fonction"});
    <?php if( Configure::read('Conf.SAERP') ) :?>
    $('#OperationOperation').select2({allowClear: true, placeholder: "Sélectionner une OP"});
    $('#EventEvent').select2({allowClear: true, placeholder: "Sélectionner un événement"});
    <?php endif;?>

    $('#ContactTitreId').select2({allowClear: true, placeholder: "Sélectionner un titre"});

	// si on sélectionne l'organisme, on pré-rempli les champs adresses du contact que l'on ajoute
	$('#ContactOrganismeId').change(function () {
		var orgID = $('#ContactOrganismeId').val();
		$.ajax({
			type: 'post',
			dataType: 'json',
			url: '/Contacts/getOrganismeValues/' + orgID,
			success: function (data) {
				$('#ContactBanId').val(data['Organisme']['ban_id']).change();
				$('#ContactBancommuneId').val(data['Organisme']['bancommune_id']).change();
				$("#ContactBanadresse").append($("<option value='" + data['Organisme']['banadresse'] + "' selected='selected'>" + data['Organisme']['banadresse'] + "</option>"));
				$("#ContactBanadresse").val(data['Organisme']['banadresse']).change();

				$('#ContactNumvoie').val(data['Organisme']['numvoie']);
				$('#ContactNomvoie').val(data['Organisme']['nomvoie']);
				$('#ContactCompl').val(data['Organisme']['compl']);
				$('#ContactCompl2').val(data['Organisme']['compl2']);
				$('#ContactCedex').val(data['Organisme']['cedex']);
				$('#ContactCp').val(data['Organisme']['cp']);
				$('#ContactVille').val(data['Organisme']['ville']);
				$('#ContactPays').val(data['Organisme']['pays']);
			}
		});
	});
</script>
