<?php

/**
 *
 * Contacts/get_contacts.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));


$formGetContactForm = array(
        'name' => 'Contact',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-6',
        'form_type' => 'post',
		'form_url' => array( 'controller' => 'contacts', 'action' => 'get_contactform' ),
        'input' =>array(
            'SearchContact.name' => array(
                'labelText' =>__d('addressbook', 'Contact.name'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required' => false
                )
            ),
            'SearchContact.nom' => array(
                'labelText' =>__d('addressbook', 'Contact.nom'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required' => false
                )
            ),
            'SearchContact.prenom' => array(
                'labelText' =>__d('addressbook', 'Contact.prenom'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            )
        )
    );
    if( Configure::read('Conf.SAERP')) {
        $formGetContactForm['input']['SearchContact.fonction_id'] = array(
            'labelText' =>'Fonction',
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $fonctions,
                'required' =>false,
                'empyt'=>true
            )
        );
        $formGetContactForm['input']['SearchContact.Event'] = array(
            'labelText' =>'Evénements',
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $events,
                'required' =>false,
                'empyt'=>true,
                'multiple' => true
            )
        );
    }
    $formGetContactForm['input']['SearchContact.active'] = array(
        'labelText' =>__d('contact', 'Contact.active'),
        'inputType' => 'checkbox',
        'items'=>array(
            'type'=>'checkbox',
            'checked'=>true,
        )
    );
?>
<fieldset class="aere serachContactDiv">
    <legend>Recherche de contact</legend>
    <?php
        echo $this->Formulaire->createForm($formGetContactForm);
    ?>
    <div class="controls text-center" role="group" >
        <a id="searchButton" class="aere btn btn-info-webgfc"  title="Rechercher"><i class="fa fa-search" aria-hidden="true"> Rechercher</a>
    </div>
    <?php echo $this->Form->end();?>
</fieldset>




<script type="text/javascript">
    function loadAddressbooks() {
        $('#infos .content').empty();
        $('#infos .title').empty();
        $('#infos .title').append('Contacts');
        $('#infos .controls .ui-button').show();
        $('#infos .controls .subFormImport').remove();
        gui.request({
            url: "/Addressbooks/get_addressbooks",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
    $('#ContactGetContactsForm div.required').removeClass('required');

    $('#formulaireButton').button();
    $('#searchButton').button();
    $('#formulaireButton').css('margin-bottom', '1em');
    $('.paging').css('margin-bottom', '1em');
    var addressbookId = "<?php echo $addressbook_id;?>";
    var orgId = "<?php echo isset( $this->request->data['SearchContact']['org_id'] ) ? $this->request->data['SearchContact']['org_id'] : '';?>";

    $('#searchButton').click(function () {
        gui.request({
            url: $("#ContactGetContactsForm").attr('action') + '/' + addressbookId + '/' + orgId,
            data: $("#ContactGetContactsForm").serialize(),
            loader: true,
            updateElement: $('#infos .content'),
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $('#infos .content').html(data);
        });
    });

    // Ajout de l'action de recherche via la touche Entrée
    $('#ContactGetContactsForm').keyup(function (e) {
        if (e.keyCode == 13) {
            gui.request({
                url: $("#ContactGetContactsForm").attr('action') + '/' + addressbookId + '/' + orgId,
                data: $("#ContactGetContactsForm").serialize(),
                loader: true,
                updateElement: $('#infos .content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#infos .content').html(data);
            });
        }
    });

    $('#ContactGetContactsForm.folded').hide();

    <?php if( Configure::read('Conf.SAERP') ) :?>
    //        $('#SearchOperationId').select2({allowClear: true, placeholder: "Sélectionner une OP"});
    //        $('#SearchActiviteId').select2({allowClear: true, placeholder: "Sélectionner une activité"});
    $('#SearchContactEvent').select2({allowClear: true, placeholder: "Sélectionner un évenement"});
    $('#SearchContactFonctionId').select2({allowClear: true, placeholder: "Sélectionner une fonction"});
    <?php endif;?>

</script>
<?php
$this->Paginator->options(array(
	'update' => '#infos .content',
	'evalScripts' => true,
                'before' => 'gui.loader({ element: $("#infos .content"), message: gui.loaderMessage });',
	'complete' => '$(".loader").remove()',
));

$pagination = '';
if (Set::classicExtract($this->params, 'paging.Contact.pageCount') > 1) {
        $pagination = '<ul class="pagination">' . implode(
                                    '', Set::filter(
                                        array(
                                            $this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
                                            $this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
                                            $this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
                                            $this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
                                            $this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
                                        )
                                    )
                                ) . '
                                </ul>';
}




if (!empty($contacts)) {
    $fields = array(
        'name',
        'organisme_name'
    );
    $actions = array(
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/contacts/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => true,
            "refreshAction" => "loadAddressbook();"
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/contacts/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadAddressbook();"
        ),
        "export" => array(
            "url" => '/contacts/export',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        )
    );
    $options = array();

    $data = $this->Liste->drawTbody($contacts, 'Contact', $fields, $actions, $options);
?>
<div  class="bannette_panel panel-body"  id="panel_get_contacts">
    <table id="table_contacts"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height="400"
           data-show-refresh="false"
           data-show-toggle="false"
           data-show-columns="true"
           data-toolbar="#toolbar">
    </table>
    <?php echo $pagination;?>
</div>

<script>
    //title legend (nombre de données)
    if ($('.table-list h3 span').length < 1) {
        $('.table-list h3').append('<span> - total:<?php echo $nbContact;?><span>');
    }
    $('#table_contacts')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d("contact","Contact.name"); ?>",
                        class: "name"
                    },
                    {
                        field: "organisme_name",
                        title: "<?php echo __d("contact","Contact.organisme_name"); ?>",
                        class: "organisme_name"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        align: "center",
                        width: "80px"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        align: "center",
                        width: "80px"
                    },
                    {
                        field: "export",
                        title: "Exporter",
                        class: "actions thExport",
                        align: "center",
                        width: "80px"
                    }
                ]
            });

</script>
<?php echo $this->LIste->drawScript($contacts, 'Contact', $fields, $actions, $options);
    // Pour empêcher la redirection de la pagination
    echo $this->Js->writeBuffer();
} else {
        echo $this->Html->div('alert alert-warning',__d('contact', 'Contact.void'));

}
?>

<script type="text/javascript">
    $('#infos .title').empty();
    $('#infos .title').append('Contacts');
    $('#infos .controls .ui-button').show();
    $('#infos .controls .subFormImport').remove();
    $("#browserContact").treeview({collapsed: true});

    $('#addressbooks_skel #infos div.content div.bannette_panel table img.itemEdit').unbind('click');
    $('#infos table i.itemEdit').prop('onclick', null).off('click');
    $('#infos div.content div.bannette_panel table i.itemEdit').bind('click', function () {
        var itemId = $(this).attr('itemId');

        gui.request({
            url: "/contacts/edit/" + itemId + "/panel_footer",
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $("#panel_get_contacts").hide();
            $('#infos .content').append(data);
        });
    });


    $('.treeview span.folder, .treeview span.file').addClass('ui-corner-all').hover(function () {
        $(this).addClass('alert alert-warning').css('border', 'none');
    }, function () {
        $(this).removeClass('alert alert-warning').css('border', 'none');
    });
    var carnetId = <?php echo $addressbook_id;?>;
    if ($('#infos .controls').find('.btn').length == 0) {
        gui.addbutton({
            element: $('#infos .panel-footer'),
            button: {
                content: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
                class: 'afficheOne btn-info-webgfc',
                action: function () {
                    $('#infos').modal('hide');
                    $('#infos .panel-footer').empty();
                }
            }
        });
    }
    $(document).ready(function () {
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });


        <?php
        $searchContact = $this->Html->tag('span','<i class="fa fa-toggle-off" aria-hidden="true"></i>',array('escape'=>false,'id'=>'searchContactSwich','class'=>'btn btn-info-webgfc action-title','title'=>'recherche avancée'));
        $searchContact .= $this->Html->tag('span','<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',array('escape'=>false,'id'=>'clearSearch','class'=>'btn btn-info-webgfc action-title','title'=>'Annuler la recherche'));
        ?>
        var addressbook_name = 'Contact <?php echo $searchContact; ?>';
        <?php if(!empty($contacts)):?>
        addressbook_name = '<?php echo $contacts[0]["Addressbook"]["name"]. $searchContact; ?>';
        <?php endif ?>

        $('#infos .panel-title').html(addressbook_name);
        $('.serachContactDiv').hide();
        $('#searchContactSwich').click(function () {
            $('.serachContactDiv').toggle();
            if ($('.serachContactDiv').css('display') == 'block') {
                $('#searchContactSwich i').removeClass("fa-toggle-off").addClass("fa-toggle-on");
            } else {
                $('#searchContactSwich i').removeClass("fa-toggle-on").addClass("fa-toggle-off");
            }
        });
        $('#clearSearch').click(function () {
            $('#infos .content').empty();
            gui.request({
                url: '/contacts/get_contacts/<?php echo $addressbook_id;?>',
                updateElement: $('#infos .content'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
        });
    });


</script>
