<?php

$formEditDeleg = array(
        'name' => 'Plandelegation',
        'label_w' => 'col-sm-3',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Plandelegation.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value'=>$planId
                )
            ),
            'Plandelegation.desktop_id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value'=>$desktopId
                )
            ),
            'Plandelegation.user_id' => array(
                'labelText' => __d('user', 'Utilisateur'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'options' => $chooseusers,
                    'selected' => $userId,
                    'required' => true
                )
            ),
            'Plandelegation.delegation' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value'=>true
                )
            ),
            'Plandelegation.date_start' => array(
                'labelText' =>__d('desktop', 'Desktop.deleg.datestart'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Plandelegation.date_end' => array(
                'labelText' =>__d('desktop', 'Desktop.deleg.dateend'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text'
                )
            )
        )
    );
?>
<div class="zone-form">
    <fieldset>
        <legend>Modification de la délégation</legend>
    <?php 
        echo $this->Formulaire->createForm($formEditDeleg);
        echo $this->Form->end();
    ?>
    </fieldset>
    <div id='edit_deleg_footer' class="modal-footer controls " role="group">
        <a class="btn btn-danger-webgfc btn-inverse " id="cancelmodifier"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a id="saveModifier" class="btn btn-success "><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script type="text/javascript">
    $('.form_datetime input').datepicker({
        language: 'fr-FR',
        format: "dd/mm/yyyy",
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked'
    });

    //$('#PlandelegationEditDelegForm input[type=submit]').remove();
    $('#PlandelegationEditDelegForm #PlandelegationUserId').select2({allowClear: true, placeholder: "Sélectionner un utilisateur"});
    $('#edit_deleg_footer').hide();
    //$('.modal-footer').hide();
    //$('.modal-content').append($('#edit_deleg_footer').show());

    var userId = <?php echo $this->Session->read('Auth.User.id'); ?>;
    /*$('#edit_deleg_footer #cancelmodifier').click(function () {
        gui.request({
            url: '/desktops/setDeleg/<?php echo $desktopId; ?>',
            updateElement: $('.modal-body'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
        $('.modal-content #edit_deleg_footer').remove();
        $('.modal-footer').show();
    });

    $('#edit_deleg_footer #saveModifier').click(function () {
        var form = $(this).parents('.modal').find('form');
        var url = '/desktops/editDeleg/<?php /*echo $desktopId; ?>/<?php echo $userId; ?>';
        if (form_validate(form)) {
            gui.request({
                url: url,
                data: form.serialize(),
                updateElement: $('.modal-body'),
                loader: true,
            }, function (data) {
                getJsonResponse(data);
                gui.request({
                    url: '/desktops/setDeleg/<?php echo $desktopId;*/ ?>',
                    updateElement: $('.modal-body'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
                $('.modal-content #edit_deleg_footer').remove();
                $('.modal-footer').show();
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",
                
            });
        }
    });*/
</script>