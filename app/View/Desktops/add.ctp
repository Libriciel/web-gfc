<?php

/**
 *
 * Desktops/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="panel panel-default zone-form" id="newRole">
    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
        <h4 class="panel-title"><?php echo __d('desktop', 'Desktop.add'); ?></h4>
    </div>
    <div class="panel-body">
        <?php
        $formDesktops = array(
            'name' => 'Desktop',
            'label_w' => 'col-sm-3',
            'input_w' => 'col-sm-6',
            'input' => array(
                'Desktop.profil_id' => array(
                    'labelText' =>__d('desktop', 'Desktop.profil_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'required'=>true,
                        'type'=>'select',
                        'empty' => true,
                        'options' => $groups
                    )
                ),
                'Desktop.name' => array(
                    'labelText' =>__d('desktop', 'Desktop.name'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text',
                        'required'=>true/*,
                        'pattern'=>"^[_A-z0-9 ]{1,}$",
                        'data-error' => "Les caractères suivants ne sont pas autorisés (/[](){},$\_*+.]/' )"*/
                    )
                ),
                'Service.Service' => array(
                    'labelText' => 'Services',
                    'inputType' => 'select',
                    'items'=>array(
                        'type' => 'select',
                        'required'=>true,
                        'escape' => false,
                        'options' => $services,
                        'multiple' => true,
                        'default' => false,
                        'multiple' => 'multiple',
                        'class' => 'selectMultiple',
                    )
                )
            )
        );
        echo $this->Formulaire->createForm($formDesktops);
        echo $this->Form->end();
        ?>
    </div>
    <div class="panel-footer controls " role="group">
        <a class="btn btn-danger-webgfc btn-inverse " id="cancel"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a class="btn  btn-success " id="saveNewRole"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>

<script>
    $('#ServiceService').select2({allowClear: true, placeholder: "Sélectionner un service"});
    $('#DesktopProfilId').select2({allowClear: true, placeholder: "Sélectionner un profil"});
    $('#DesktopProfilId').change(function () {
        if ($('#DesktopName').val() == '' && $('#DesktopProfilId').val() != '') {
            $('#DesktopName').val($('#DesktopProfilId option:selected').html() + ' ' + $('#UserEditForm #UserPrenom').val() + ' ' + $('#UserEditForm #UserNom').val());
            checkDesktopNameUnique($('#DesktopAddForm'));
        }
    });

    $('#DesktopName').blur(function () {
        checkDesktopNameUnique($('#DesktopAddForm'));
    });

    $('#saveNewRole').click(function () {
        if (form_validate($('#UserChangemdpForm'))) {
            var id = $('#UserId').val();
            if (form_validate($('#DesktopAddForm'))) {
                gui.request({
                    url: '/desktops/add/' + id,
                    data: $('#DesktopAddForm').serialize(),
                    loaderElement: $('.ui-dialog-content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    getJsonResponse(data);
                    gui.request({
                        url: '/users/edit/' + id,
                        loader: false
                    }, function (data) {
                        $('.ui-dialog-content').html(data);
                        $('.loader').remove();
                        loadHabilis();
                    });
                    $("#newRole").remove();
                    $("#habilsPerson").show();
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Oops...",
                    text: "Veuillez vérifier votre formulaire!",
                    type: "error",

                });
            }
        }
    });

    $('#cancel').click(function () {
        $("#newRole").remove();
        $("#habilsPerson").show();
    });
</script>
