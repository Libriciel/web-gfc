<?php

/**
 *
 * Desktops/rights.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="panel panel-default editRD">
    <div class="panel-heading">
        <h4 class="panel-title">
            <?php echo __d('desktop', 'Desktop.Desktop') . ' : ' . $desktopName; ?>
        </h4>
    </div>
    <div class="panel-body">
        <ul class="nav nav-tabs titre" role="tablist">
            <li class="active"><a href="#droits_data" role="tab" data-toggle="tab"><?php echo __d('desktop', 'Tab.droits_data'); ?></a></li>
            <li><a href="#droits_meta" role="tab" data-toggle="tab"><?php echo __d('desktop', 'Tab.droits_meta'); ?></a></li>
            <li><a href="#droits_func" role="tab" data-toggle="tab"><?php echo __d('desktop', 'Tab.droits_func'); ?></a></li>
        </ul>
        <div class="tab-content clearfix">
            <div id="droits_data" class="tab-pane fade active in" ></div>
            <div id="droits_meta" class="tab-pane fade"></div>
            <div id="droits_func" class="tab-pane fade"></div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function affichageTab() {
        if ($('#droits_data').length > 0 && $('#droits_data').is(':visible')) {
            gui.request({
                url: "<?php echo "/desktops/" . $mode.'Rights/'.$id.'/data'; ?>",
                updateElement: $('#droits_data'),
            });
        }
        if ($('#droits_meta').length > 0 && $('#droits_meta').is(':visible')) {
            gui.request({
                url: "<?php echo "/desktops/" . $mode.'Rights/'.$id.'/meta'; ?>",
                updateElement: $('#droits_meta'),
            });
        }
        if ($('#droits_func').length > 0 && $('#droits_func').is(':visible')) {
            gui.request({
                url: "<?php echo "/desktops/" . $mode.'Rights/'.$id.'/func'; ?>",
                updateElement: $('#droits_func'),
            });
        }
    }
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        affichageTab();
    });
    affichageTab();

</script>
