<?php

$formEditDeleg = array(
        'name' => 'Plandelegation',
        'label_w' => 'col-sm-3',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Plandelegation.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value'=>$planId
                )
            ),
            'Plandelegation.desktop_id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value'=>$desktopId
                )
            ),
            'Plandelegation.user_id' => array(
                'labelText' => __d('user', 'Utilisateur'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'options' => $chooseusers,
                    'selected' => $userId,
                    'required' => true
                )
            ),
            'Plandelegation.date_start' => array(
                'labelText' =>__d('desktop', 'Desktop.deleg.datestart'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Plandelegation.delegation' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value'=>true
                )
            ),
            'Plandelegation.date_end' => array(
                'labelText' =>__d('desktop', 'Desktop.deleg.dateend'),
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type'=>'text'
                )
            )
        )
    );
?>
<div class="zone-form">
    <fieldset>
        <legend>Modification de la délégation</legend>
    <?php 
    echo $this->Formulaire->createForm($formEditDeleg);
    echo $this->Form->end();
    ?>
    </fieldset>
    <div id='edit_deleg_admin_footer' class="modal-footer controls " role="group">
        <a id="cancelmodifier" class="btn btn-danger-webgfc btn-inverse "><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a id="saveModifier" class="btn btn-success "><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>

<script type="text/javascript">
    $('.form_datetime input').datepicker({
        language: 'fr-FR',
        format: "dd/mm/yyyy",
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked'
    });

    $('#PlandelegationEditDelegForm input[type=submit]').remove();
    $('#PlandelegationEditDelegForm #PlandelegationUserId').select2({allowClear: true, placeholder: "Sélectionner un utilisateur"});
//    $('#PlandelegationDateStart').before("<input type='text' id='PlandelegationdateStartAltField' />");
//    $('#PlandelegationDateStart').css('display', 'none');
    $('#divSetDelege .delegFooter').hide();
    $('#edit_deleg_admin_footer').hide();
    $('#divSetDelege').append($('#edit_deleg_admin_footer').show());

    var userId = <?php echo $this->Session->read('Auth.User.id'); ?>;
    $('#edit_deleg_admin_footer #cancelmodifier').click(function () {
        loadHabilis();
        $("#habilsPerson").show();
        $('#divSetDelege #edit_deleg_admin_footer').remove();
    });


    $('#edit_deleg_admin_footer #saveModifier').click(function () {
        var form = $('#PlandelegationEditDelegForm');
        var url = '/desktops/editDeleg/<?php echo $desktopId; ?>/<?php echo $userId; ?>';
        //TODO debug valider un modification
        if (form_validate(form)) {
            gui.request({
                url: url,
                data: form.serialize(),
                updateElement: $('#divSetDelege .panel-body'),
                loader: true,
            }, function (data) {
                getJsonResponse(data);
                loadHabilis();
                $("#habilsPerson").show();
                $('#divSetDelege #edit_deleg_admin_footer').remove();
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",
                
            });
        }
    });
</script>