<?php

/**
 *
 * Activites/get_activites.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php if( !empty($activites) ){
    $fields = array(
        'name',
        'description',
        'active'
    );
    $actions = array(
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/activites/edit/',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/activites/delete/',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadActivite();"
        )
    );
    $options = array();

    //preparation des données dans la tableaux
    $data_table = $this->Liste->drawTbody($activites, 'Activite', $fields, $actions, $options);
    $data = (json_decode($data_table,true));
    $i=0;
    foreach( $activites as $activite ) {
        if( !empty($activite['Activite']['sousactivite'] ) && isset($activite['Activite']['sousactivite']) ) {
            $data[$i]['sousactivites']="<ul>";
            foreach( $activite['Activite']['sousactivite'] as $sousactivite ) {
                $data[$i]['sousactivites'].="<li>".$sousactivite."</li>";
            }
            $data[$i]['sousactivites'].="</ul>";
        }
        $i++;
    }
    $data = json_encode($data);

?>
<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_activites"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-pagination = "true"
           >
    </table>
</div>
<script type="text/javascript">
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($activites,$options); ?>');
    //ajouter les donner dans la tableaux
    $('#table_activites')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d('activite', 'Activite.name'); ?>",
                        class: "name"
                    },
                    {
                        field: "sousactivites",
                        title: "<?php echo __d('activite', 'Activite.sousactivites'); ?>",
                        class: "sousactivites"
                    },
                    {
                        field: "active",
                        title: "<?php echo __d('activite', 'Activite.active'); ?>",
                        class: "active_column"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        align: 'center',
                        width: "80px"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        align: 'center',
                        width: "80px"
                    }
                ]
            })
            .on('page-change.bs.table', function (number, size) {
                addClassActive();
            });
    $(document).ready(function () {
        addClassActive();
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }
    function addClassActive() {
        $('#table_activites .active_column').hide();
        $('#table_activites .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }
</script>
<?php
    echo $this->LIste->drawScript($activites, 'Activite', $fields, $actions, $options);
    echo $this->Js->writeBuffer();
}else{
    echo $this->Html->tag('div', __d('activite', 'Activite.void'), array('class' => 'alert alert-warning'));
 }
 ?>


