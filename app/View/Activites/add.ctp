<?php

/**
 *
 * Activites/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php

$formActivite = array(
    'name' => 'Activite',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-5',
    'input' => array(
        'Activite.name' => array(
            'labelText' =>__d('activite', 'Activite.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Activite.description' => array(
            'labelText' =>__d('activite', 'Activite.description'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'Activite.active' => array(
            'labelText' =>__d('activite', 'Activite.active'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>true,
            )
        )
    )
);
echo $this->Formulaire->createForm($formActivite);
echo $this->Form->end();
?>

