<?php

/**
 *
 * Activites/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<!-- affichage (ou masquage) des informations liées à une métadonnée de type select -->
<?php
$formActivite = array(
    'name' => 'Activite',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-5',
    'input' => array(
        'Activite.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Activite.name' => array(
            'labelText' =>__d('activite', 'Activite.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Activite.description' => array(
            'labelText' =>__d('activite', 'Activite.description'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'Activite.active' => array(
            'labelText' =>__d('activite', 'Activite.active'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['Activite']['active']
            )
        )
    )
);
echo $this->Formulaire->createForm($formActivite);
echo $this->Form->end();
?>
<div id="ajouterSousActivite" class="btn btn-info-webgfc ajouterBtnSousEdit"  title="<?php echo __d('sousactivite', 'Sousactivite.add'); ?>">
    <i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter une sous-activité
</div>



<?php if (!empty($sousactivites) ) :?>
<div class="activites index col-sm-12">
    <legend class="bannette">Liste des sous-activités</legend>

    <table id="listeSousactivite" data-toggle="table" data-sortable = "true">
        <thead>
            <tr>
                <th data-sortable="true"><?php echo  __d('sousactivite', 'Sousactivite.name') ?></th>
                <th class="actions" date-cell-style="cellStyle">Modification</th>
                <th class="actions" date-cell-style="cellStyle">Suppression</th>
            </tr>
        </thead>
        <tbody>
    <?php
    $i = 0;
    foreach ($sousactivites as $sousactivite):
        $i++;
        $class="";
        if( $sousactivite['Sousactivite']['active'] == false ) {
            $class = 'inactive';
        }
        $editImage =  $this->Html->tag('i', '', array(
                'title' => __d('default', 'Button.edit'),
                'alt' => __d('default', 'Button.edit'),
                'class' => "fa fa-pencil itemEdit",
                'itemId' => $sousactivite['Sousactivite']['id'],
//                'style' => 'color: #5397a7;cursor:pointer'
                    )
            );
        $deleteImage =  $this->Html->tag('i', '', array(
                'title' => __d('default', 'Button.delete'),
                'alt' => __d('default', 'Button.delete'),
                'class' => "fa fa-trash itemDelete",
                'itemId' => $sousactivite['Sousactivite']['id'],
//                'style' => 'color: #FF0000;cursor:pointer'
                    )
            );
    ?>
            <tr class="<?php echo $class; ?> <?php echo $sousactivite['Sousactivite']['activite_id']; ?>">
                <td><?php echo $sousactivite['Sousactivite']['name']; ?>&nbsp;</td>
                <td class="actions actions_sousactivites"><?php echo $editImage; ?></td>
                <td class="actions actions_sousactivites"><?php echo $deleteImage; ?></td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endif;?>

<script type="text/javascript">
    $('#listeSousactivite').bootstrapTable({});
    $(document).ready(function () {
        $('th.actions').css({
            'width': '80px'
        });
        $('td.actions').css({
            'text-align': 'center'
        });
    });

    function loadValues() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/activites/edit/" . $activiteId; ?>",
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

    $('#ajouterSousActivite').addClass('selectmetadonnees');
    $('#ajouterSousActivite').button().click(function () {
        var url = "<?php echo Router::url(array('controller' => 'sousactivites', 'action' => 'add', $activiteId)); ?>";
        gui.request({
            url: url,
            updateElement: $('#infos .content '),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    });

    // Pour la partie Edit
    $('.actions_sousactivites i.itemEdit').bind('click', function () {
        var itemId = $(this).attr('itemId');
        gui.request({
            url: "/sousactivites/edit/" + itemId,
            updateElement: $('#infos .content '),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    });

    $('.actions_sousactivites i.itemDelete').click(function () {
        var item = $(this);
        var itemId = $(this).attr("itemId");
        swal({
            showCloseButton: true,
            title: "<?php echo __d('default', 'Confirmation de suppression'); ?>",
            text: "<?php echo __d('default', 'Voulez-vous supprimer cet élément ?'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
            cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: "/sousactivites/delete/" + itemId,
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                    updateElement: $('#liste .content')
                }, function (data) {
                    getJsonResponse(data);
                    loadValues();
                    loadActivite();

                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n\'avez pas supprimé.",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');
        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    });



    $('.itemEdit').css('cursor' , 'pointer');
    $('.itemDelete').css('cursor' , 'pointer');
</script>

