<?php
/**
 *
 * Keywordtypes/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<h1><?php echo __('Liste des codes des types de mot clé'); ?></h1>
<?php echo $html->link(__('Retour au référentiel', true), array('controller' => 'referentiels', 'action' => 'index')); ?>
<?php echo $this->element('indexPageCourante'); ?>
<table cellpadding="0" cellspacing="0">
    <?php
    echo $html->tableHeaders(array(
            $paginator->sort(__('Code', true), 'code'),
            __('Documentation', true)));
    ?>
    <?php foreach ($this->data as $rownum => $rowElement) : ?>
        <tr <?php echo ($rownum & 1) ? "" : " class='altrow'"; ?>>
            <td><?php echo $rowElement[$this->params['models'][0]]['code']; ?></td>
            <td><?php echo $rowElement[$this->params['models'][0]]['documentation']; ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<?php echo $this->element('indexPageNavigation'); ?>
