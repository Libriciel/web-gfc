<?php

$true_false = array('true' => 'Oui', 'false' => 'Non');
$formDS = array(
	'name' => 'Connecteur',
	'label_w' => 'col-sm-6',
	'input_w' => 'col-sm-7',
	'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'sms'),
	'input' => array(
		'Connecteur.id' => array(
			'inputType' => 'hidden',
			'items'=>array(
				'type' => 'hidden',
				'value' => $this->params['pass'][0]
			)
		)
	)
);
echo $this->Formulaire->createForm($formDS);
?>
<fieldset>
	<legend>Activation du service LsMessage pour les SMS</legend>
	<?php
	$formSMSRadio = array(
		'name' => 'Connecteur',
		'label_w'=>'col-sm-6',
		'input_w'=>'col-sm-6',
		'form_url' => array('controller' => 'connecteurs', 'action' => 'makeconf', 'sms'),
		'input' =>array(
			'use_sms' => array(
				'labelText' =>"",
				'inputType' => 'radio',
				'required' => true,
				'items'=>array(
					'legend' => false,
					'separator'=> '</div><div  class="radioUseSmsLabel">',
					'before' => '<div class="radioUseSmsLabel">',
					'after' => '</div>',
					'label' => true,
					'type'=>'radio',
					'options' => $true_false,
					'value' => ( isset( $this->request->data['Connecteur']['use_sms'] ) && $this->request->data['Connecteur']['use_sms'] ) ? 'true' : 'false'
				)
			)
		)
	);
	echo $this->Formulaire->createForm($formSMSRadio);
	?>
</fieldset>
<div id='affiche' <?php echo $this->request->data['Connecteur']['use_sms'] === false ? 'style="display: none;"' : ''; ?>>
	<fieldset>
		<legend>Paramètrage de LsMessage</legend>
		<?php
		$formConnecteurSms = array(
			'name' => 'Connecteur',
			'label_w' => 'col-sm-6',
			'input_w' => 'col-sm-6',
			'form_url' => array( 'controller' => 'connecteurs', 'action' => 'sms'),
			'input' => array(
				'Connecteur.host' => array(
					'labelText' => 'URL de LsMessage',
					'inputType' => 'text',
					'items'=>array(
						'required' => true,
						'type'=>'text',
						'class'=>'grcparam'
					)
				),
				'Connecteur.sms_apikey' => array(
					'labelText' =>__d('connecteur', 'Connecteur.sms_apikey'),
					'inputType' => 'text',
					'items'=>array(
						'required' => true,
						'type'=>'text',
						'class'=>'dsparam'
					)
				),
				'Connecteur.sms_expediteur' => array(
					'labelText' =>__d('connecteur', 'Connecteur.sms_expediteur'),
					'inputType' => 'text',
					'items'=>array(
						'required' => true,
						'type'=>'text',
						'min'=>"0",
						'data-minlength'=>"11"
					)
				)
			)
		);
		echo $this->Formulaire->createForm($formConnecteurSms);
		?>
	</fieldset>
</div>
<?php
echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 50%;'));
echo $this->Html->link(' <i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler', array('controller' => 'connecteurs', 'action' => 'index'), array('class' => 'btn btn-danger-webgfc btn-inverse closeModal', 'escape' => false, 'title' => 'Annuler'));
echo $this->Html->tag('/div', null);
echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 1%;'));
echo $this->Form->button(' <i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer', array('type' => 'submit', 'id' => 'boutonValider', 'class' => 'btn btn-success closeModal', 'escape' => false, 'title' => 'Modifier la configuration'));
echo $this->Html->tag('/div', null);
echo $this->Form->end();

?>
<script type="text/javascript">

	$('#ConnecteurMakeconfForm input[type=radio]').change(function () {
		$('#affiche').toggle();
	});

	if ($('#ConnecteurUseSmsTrue').is(":checked")) {
		$('#affiche').show();
	}

	if ($('#ConnecteurUseSmsFalse').is(":checked")) {
		$('#affiche').hide();
	}
</script>
