<?php

$true_false = array('true' => 'Oui', 'false' => 'Non');
$formCMIS = array(
        'name' => 'Connecteur',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-6',
        'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'cmis'),
        'input' => array(
            'Connecteur.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type' => 'hidden',
                    'value' => $this->params['pass'][0]
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formCMIS);
?>
<fieldset>
    <legend>Activation du service GED - Export CMIS</legend>
    <?php
    $formCMISRadio = array(
        'name' => 'Connecteur',
        'label_w'=>'col-sm-5',
        'input_w'=>'col-sm-6',
        'form_url' => array('controller' => 'connecteurs', 'action' => 'makeconf', 'cmis'),
        'input' =>array(
            'use_ged' => array(
                'labelText' =>"",
                'inputType' => 'radio',
                'items'=>array(
                    'legend' => false,
                    'separator'=> '</div><div  class="radioUserGedLabel">',
                    'before' => '<div class="radioUserGedLabel">',
                    'after' => '</div>',
                    'label' => true,
                    'type'=>'radio',
                    'options' => $true_false,
                    'value' => ( isset( $this->request->data['Connecteur']['use_ged'] ) && $this->request->data['Connecteur']['use_ged'] ) ? 'true' : 'false'
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formCMISRadio);
    ?>
</fieldset>
<div id='affiche' <?php echo $this->request->data['Connecteur']['use_ged'] === false ? 'style="display: none;"' : ''; ?>>
    <fieldset>
        <legend>Paramètrage de la GED</legend>
    <?php
    $formCMISParametrage = array(
        'name' => 'Connecteur',
        'label_w'=>'col-sm-5',
        'input_w'=>'col-sm-6',
        'form_url' => array('controller' => 'connecteurs', 'action' => 'makeconf', 'cmis'),
        'input' => array(
            'ged_url' => array(
                'labelText' =>'Serveur de la GED : ',
                'inputType' => 'text',
                'labelPlaceholder' => "Exemple : http://x.x.ma-ville.fr",
                'items'=>array(
					'required' => true,
                    'type' => 'text'
                )
            ),
            'ged_login' => array(
                'labelText' =>'Nom d\'utilisateur ',
                'inputType' => 'text',
                'labelPlaceholder' => "Nom d'utilisateur",
                'items'=>array(
					'required' => true,
                    'type' => 'text'
                )
            ),
            'ged_passwd' => array(
                'labelText' =>'Mot de passe',
                'inputType' => 'password',
                'labelPlaceholder' => "Mot de passe",
                'items'=>array(
					'required' => true,
                    'type' => 'password'
                )
            ),
            'ged_repo' => array(
                'labelText' =>'Répertoire de stockage',
                'inputType' => 'text',
                'labelPlaceholder' => "Exemple : /Sites/Web-delib",
                'items'=>array(
					'required' => true,
                    'type' => 'text'
                )
            ),
            'ged_xml_version' => array(
                'labelText' =>'Version du schéma XML',
                'inputType' => 'select',
                'items'=>array(
                    'type' => 'select',
                    'options' => array(1 => 1, 2 => 2, 3 => 3),
                    'autocomplete' => 'off'
                )
            ),
            'ged_cmis_version' => array(
                'labelText' =>'Version du protocole CMIS',
                'inputType' => 'select',
                'items'=>array(
                    'type' => 'select',
                    'options' => array('1.0' => '1.0', '1.1' => '1.1')
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formCMISParametrage);
    ?>
        <div class="control-group information_update">
            (1.0 si version Alfresco 4.x  ||  1.1 si version Alfresco 5.x)
        </div>
    </fieldset>
</div>
<?php
//echo $this->Form->end();
?>
   <?php
    echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 50%;'));
    echo $this->Html->link(' <i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler', array('controller' => 'connecteurs', 'action' => 'index'), array('class' => 'btn btn-danger-webgfc btn-inverse closeModal', 'escape' => false, 'title' => 'Annuler'));
    echo $this->Html->tag('/div', null);
    echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 1%;'));
    echo $this->Form->button(' <i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer', array('type' => 'submit', 'id' => 'boutonValider', 'class' => 'btn btn-success closeModal', 'escape' => false, 'title' => 'Modifier la configuration'));
    echo $this->Html->tag('/div', null);
    echo $this->Form->end();

    ?>
<script type="text/javascript">

    $('#ConnecteurMakeconfForm input[type=radio]').change(function () {
        $('#affiche').toggle();
    });

    if ($('#ConnecteurUseGedTrue').is(":checked")) {
        $('#affiche').show();
    }

    if ($('#ConnecteurUseGedFalse').is(":checked")) {
        $('#affiche').hide();
    }
</script>
