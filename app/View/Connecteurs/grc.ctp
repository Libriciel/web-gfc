<?php

$true_false = array('true' => 'Oui', 'false' => 'Non');
$formGRC = array(
        'name' => 'Connecteur',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-6',
        'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'grc'),
        'input' => array(
            'Connecteur.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type' => 'hidden',
                    'value' => $this->params['pass'][0]
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formGRC);
?>
<fieldset>
    <legend>Activation de la connexion à la GRC</legend>
    <?php
    $formGrcRadio = array(
        'name' => 'Connecteur',
        'label_w'=>'col-sm-5',
        'input_w'=>'col-sm-6',
        'form_url' => array('controller' => 'connecteurs', 'action' => 'makeconf', 'grc'),
        'input' =>array(
            'use_grc' => array(
                'labelText' =>"",
                'inputType' => 'radio',
                'items'=>array(
                    'legend' => false,
                    'separator'=> '</div><div  class="radioUseGrcLabel">',
                    'before' => '<div class="radioUseGrcLabel">',
                    'after' => '</div>',
                    'label' => true,
                    'type'=>'radio',
                    'options' => $true_false,
                    'value' => ( isset( $this->request->data['Connecteur']['use_grc'] ) && $this->request->data['Connecteur']['use_grc'] ) ? 'true' : 'false'
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formGrcRadio);
    ?>
</fieldset>
<div id='affiche' class="panel-body form-horizontal">
    <div id ="activeGrc">
        <legend><i class="fa fa-sliders" style="font-size:16px;" aria-hidden="true"></i> <?php echo __d('connecteur', 'Connecteur.grc_server'); ?></legend>
     <?php
        $formConnecteurGrcServer = array(
           'name' => 'Connecteur',
           'label_w' => 'col-sm-5',
           'input_w' => 'col-sm-7',
           'form_url' => array( 'controller' => 'connecteurs', 'action' => 'admin'),
           'input' => array(
               'Connecteur.grc_host' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.grc_host'),
                   'inputType' => 'text',
                   'items'=>array(
					   'required' => true,
                       'type'=>'text',
                       'class'=>'grcparam'
                   )
               ),
               'Connecteur.grc_apikey' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.grc_apikey'),
                   'inputType' => 'text',
                   'items'=>array(
					   'required' => true,
                       'type'=>'text',
                       'class'=>'grcparam'
                   )
               ),
               'Connecteur.grc_appname' => array(
                   'labelText' => __d('connecteur', 'Connecteur.grc_appname'),
                   'inputType' => 'text',
                   'items'=>array(
					   'required' => true,
                       'type'=>'text',
                       'class' =>'grcparam'
                   )
               ),
               'Connecteur.grc_cityid' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.grc_cityid'),
                   'inputType' => 'text',
                   'items'=>array(
					   'required' => true,
                       'type'=>'text',
                       'class'=>'grcparam'
                   )
               ),
               'Connecteur.grc_canalid' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.grc_canalid'),
                   'inputType' => 'text',
                   'items'=>array(
					   'required' => true,
                       'type'=>'text',
                       'class'=>'grcparam'
                   )
               ),
               'Connecteur.grc_servid' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.grc_servid'),
                   'inputType' => 'text',
                   'items'=>array(
					   'required' => true,
                       'type'=>'text',
                       'class'=>'grcparam'
                   )
               ),
               'Connecteur.grc_sqrtid' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.grc_sqrtid'),
                   'inputType' => 'text',
                   'items'=>array(
					   'required' => true,
                       'type'=>'text',
                       'class'=>'grcparam'
                   )
               ),
               'Connecteur.grc_qrtid' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.grc_qrtid'),
                   'inputType' => 'text',
                   'items'=>array(
					   'required' => true,
                       'type'=>'text',
                       'class'=>'grcparam'
                   )
               )
           )
        );
        echo $this->Formulaire->createForm($formConnecteurGrcServer);
    ?>


    </div>
</div>
<?php
//echo $this->Form->end();
?>
   <?php

    echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 50%;'));
    echo $this->Html->link(' <i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler', array('controller' => 'connecteurs', 'action' => 'index'), array('class' => 'btn btn-danger-webgfc btn-inverse closeModal', 'escape' => false, 'title' => 'Annuler'));
    echo $this->Html->tag('/div', null);
    echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 1%;'));
    echo $this->Form->button(' <i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer', array('type' => 'submit', 'id' => 'boutonValider', 'class' => 'btn btn-success closeModal', 'escape' => false, 'title' => 'Modifier la configuration'));
    echo $this->Html->tag('/div', null);
    echo $this->Form->end();
    ?>
<script type="text/javascript">

    $('#ConnecteurMakeconfForm input[type=radio]').change(function () {
        $('#affiche').toggle();
    });

    if ($('#ConnecteurUseGrcTrue').is(":checked")) {
        $('#affiche').show();
    }

    if ($('#ConnecteurUseGrcFalse').is(":checked")) {
        $('#affiche').hide();
    }

</script>
