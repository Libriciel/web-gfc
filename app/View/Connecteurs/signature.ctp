<div id="configSignature">
    <?php
    $true_false = array('true' => 'Oui', 'false' => 'Non');
    $formSignature = array(
            'name' => 'Connecteur',
            'label_w' => 'col-sm-5',
            'input_w' => 'col-sm-6',
            'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'signature'),
            'form_type' => 'file',
            'input' => array(
                'Connecteur.id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type' => 'hidden',
                        'value' => $this->params['pass'][0]
                    )
                )
            )
        );
    echo $this->Formulaire->createForm($formSignature);
    ?>
    <fieldset>
        <legend>Activation de la signature électronique</legend>
        <?php
        $formSignatureRadio = array(
            'name' => 'Connecteur',
            'label_w'=>'col-sm-5',
            'input_w'=>'col-sm-6',
            'form_url' => array('controller' => 'connecteurs', 'action' => 'makeconf', 'signature'),
            'input' =>array(
                'use_signature' => array(
                    'labelText' =>"",
                    'inputType' => 'radio',
                    'items'=>array(
                        'legend' => false,
                        'separator'=> '</div><div  class="radioUserGedLabel">',
                        'before' => '<div class="radioUserGedLabel">',
                        'after' => '</div>',
                        'label' => true,
                        'type'=>'radio',
                        'options' => $true_false,
                        'value' => ( isset( $this->request->data['Connecteur']['use_signature'] ) && $this->request->data['Connecteur']['use_signature'] ) ? 'true' : 'false'
                    )
                )
            )
        );
        echo $this->Formulaire->createForm($formSignatureRadio);
        ?>
    </fieldset>
    <div id='config_content'>
        <fieldset>
            <legend>Connecteur de signature</legend>
            <?php
            $formSignatureProtocol = array(
                    'name' => 'Connecteur',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-6',
                    'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'signature'),
                    'input' => array(
                        'signature_protocol' => array(
                            'labelText' =>'Type de connecteur',
                            'inputType' => 'select',
                            'items'=>array(
                                'type' => 'select',
                                'options' => $protocoles
                            )
                        )
                    )
                );
            echo $this->Formulaire->createForm($formSignatureProtocol);
            ?>
        </fieldset>





        <fieldset class="iparapheur-infos">
            <legend>Informations d'authentification</legend>
            <?php
            $formSignatureAuthentification = array(
                    'name' => 'Connecteur',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-6',
                    'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'signature'),
                    'input' => array(
                        'host' => array(
                            'labelText' =>'URL',
                            'labelPlaceholder' => "https://secure-parapheur.x.x.org",
                            'inputType' => 'text',
                            'items'=>array(
                                'type' => 'text'
                            )
                        ),
                        'login' => array(
                            'labelText' =>'Login',
                            'labelPlaceholder' => "Nom d'utilisateur",
                            'inputType' => 'text',
                            'items'=>array(
                                'type' => 'text'
                            )
                        ),
                        'pwd' => array(
                            'labelText' =>'Mot de passe',
                            'labelPlaceholder' => "Mot de passe utilisateur",
                            'inputType' => 'password',
                            'items'=>array(
                                'type' => 'password'
                            )
                        )
                    )
                );
            echo $this->Formulaire->createForm($formSignatureAuthentification);

            ?>
        </fieldset>
        <fieldset class='iparapheur-infos'>
            <legend>Configuration des flux</legend>
            <?php

            $formSignatureFlux = array(
                    'name' => 'Connecteur',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-6',
                    'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'signature'),
                    'input' => array(
                        'type' => array(
                            'labelText' =>'Type technique (Parapheur)',
                            'inputType' => 'text',
                            'items'=>array(
                                'type' => 'text'
                            )
                        ),
                        'visibility' => array(
                            'labelText' =>'Visibilité',
                            'labelPlaceholder' => "CONFIDENTIEL",
                            'inputType' => 'select',
                            'items'=>array(
                                'type' => 'select',
                                'options' => $visibilite,
                                'default' => 'CONFIDENTIEL'
                            )
                        )
                    )
                );
            echo $this->Formulaire->createForm($formSignatureFlux);

            ?>
        </fieldset>
		<?php if( Configure::read('Certificate.ForIparapheur') ) :?>

			<fieldset class='iparapheur-infos'>
				<legend>Certificat d'authentification</legend>
				<?php
				$formSignatureCertificatAuthentification = array(
						'name' => 'Connecteur',
						'label_w' => 'col-sm-5',
						'input_w' => 'col-sm-6',
						'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'signature'),
						'form_type' => 'file',
						'input' => array(
							'clientcert' => array(
								'labelText' =>'Certificat (p12)',
								'inputType' => 'file',
								'items'=>array(
									'type'=>'file',
									'name' => 'myfile',
									'class' => 'fileField'
								)
							),
							'certpwd' => array(
								'labelText' =>'Mot de passe',
								'labelPlaceholder' => "Mot de passe du certificat",
								'inputType' => 'password',
								'items'=>array(
									'type' => 'password'
								)
							)
						)
					);
				echo $this->Formulaire->createForm($formSignatureCertificatAuthentification);

				$value = $this->request->data['Connecteur']['nom_cert'];
				if( !empty( $value )) {
					echo 'Certificat utilisé : '.$value;
				}

				?>
			</fieldset>
		<?php endif;?>
    </div>

    <div class="boutons">    </div>

	<div class="form-group "  id="TypeConnecteurProtocol">
		<label for="SendPastellInforequiredTypeDocument" class="control-label col-sm-4">Type de connecteur</label>
		<div class=" controls-input col-sm-6 " style="margin-top:8px;">
			<?php echo $this->request->data['Connecteur']['signature_protocol'];?>
		</div>
		<br />
		<br />
	</div>

    <?php

    echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 50%;'));
    echo $this->Html->link(' <i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler', array('controller' => 'connecteurs', 'action' => 'index'), array('class' => 'btn btn-danger-webgfc btn-inverse closeModal', 'escape' => false, 'title' => 'Annuler'));
    echo $this->Html->tag('/div', null);
    echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 1%;'));
    echo $this->Form->button(' <i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer', array('type' => 'submit', 'id' => 'boutonValider', 'class' => 'btn btn-success closeModal', 'escape' => false, 'title' => 'Modifier la configuration'));
    echo $this->Html->tag('/div', null);
    echo $this->Form->end();
    ?>


    <?php
//    echo $this->Form->end('Coucou');
    ?>
</div>
<script type="text/javascript">

	$('#TypeConnecteurProtocol').hide();
    $(document).ready(function () {
		<?php if( $this->request->data['Connecteur']['signature_protocol'] == 'PASTELL' ):?>
			$('#ConnecteurSignatureProtocol').val('PASTELL');
			$('#ConnecteurSignatureProtocol').css('display', 'none');
			$('#ConnecteurSignatureProtocol').parent().parent().css('display', 'none');
			$('#TypeConnecteurProtocol').show();

			$('#ConnecteurUseSignatureTrue').attr('disabled', 'disabled');
			$('#ConnecteurUseSignatureFalse').attr('disabled', 'disabled');

		<?php endif;?>

		changeProtocol();

        $('#ConnecteurMakeconfForm input[type=radio]').change(function () {
            $('#config_content').toggle();

			if ($('#ConnecteurUseSignatureTrue').is(":checked")) {
				$('#config_content').show();
				$('#TypeConnecteurProtocol').show();
			}

			if ($('#ConnecteurUseSignatureFalse').is(":checked")) {
				$('#config_content').hide();
				$('#TypeConnecteurProtocol').hide();
			}
        });



		if ($('#ConnecteurUseSignatureTrue').is(":checked")) {
			$('#config_content').show();
			$('#TypeConnecteurProtocol').show();
		}

		if ($('#ConnecteurUseSignatureFalse').is(":checked")) {
			$('#config_content').hide();
			$('#TypeConnecteurProtocol').hide();
		}

        $('#ConnecteurSignatureProtocol').change(function () {
            changeProtocol();
        })
    });

    function changeProtocol() {
        var protocol = $('#ConnecteurSignatureProtocol').val();
        if (protocol == 'PASTELL') {
            $('.pastell-infos').show();
			$('#TypeConnecteurProtocol').show();
        } else {
            $('.pastell-infos').hide();
        }
        if (protocol == 'IPARAPHEUR') {
            $('.iparapheur-infos').show();
			$('#TypeConnecteurProtocol').show();
        } else {
            $('.iparapheur-infos').hide();
        }
    }

    $('#ConnecteurSignatureProtocol').select2();
    $('#ConnecteurVisibility').select2();
</script>
