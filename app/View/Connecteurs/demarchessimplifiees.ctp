<?php

$true_false = array('true' => 'Oui', 'false' => 'Non');
$formDS = array(
	'name' => 'Connecteur',
	'label_w' => 'col-sm-6',
	'input_w' => 'col-sm-6',
	'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'demarchessimplifiees'),
	'input' => array(
		'Connecteur.id' => array(
			'inputType' => 'hidden',
			'items'=>array(
				'type' => 'hidden',
				'value' => $this->params['pass'][0]
			)
		)
	)
);
echo $this->Formulaire->createForm($formDS);
?>
<fieldset>
	<legend>Activation du service Démarches-Simplifiées</legend>
	<?php
	$formDSRadio = array(
		'name' => 'Connecteur',
		'label_w'=>'col-sm-5',
		'input_w'=>'col-sm-6',
		'form_url' => array('controller' => 'connecteurs', 'action' => 'makeconf', 'demarchessimplifiees'),
		'input' =>array(
			'use_ds' => array(
				'labelText' =>"",
				'inputType' => 'radio',
				'items'=>array(
					'legend' => false,
					'separator'=> '</div><div  class="radioUseDemarchessimplifieesLabel">',
					'before' => '<div class="radioUseDemarchessimplifieesLabel">',
					'after' => '</div>',
					'label' => true,
					'type'=>'radio',
					'options' => $true_false,
					'value' => ( isset( $this->request->data['Connecteur']['use_ds'] ) && $this->request->data['Connecteur']['use_ds'] ) ? 'true' : 'false'
				)
			)
		)
	);
	echo $this->Formulaire->createForm($formDSRadio);
	?>
</fieldset>
<div id='affiche' <?php echo $this->request->data['Connecteur']['use_ds'] === false ? 'style="display: none;"' : ''; ?>>
	<fieldset>
		<legend>Paramètrage de Démarches-Simplifiées</legend>
		<?php
		$formConnecteurDs = array(
			'name' => 'Connecteur',
			'label_w' => 'col-sm-5',
			'input_w' => 'col-sm-7',
			'form_url' => array( 'controller' => 'connecteurs', 'action' => 'demarchessimplifiees'),
			'input' => array(
				'Connecteur.host' => array(
					'labelText' => 'URL',
					'inputType' => 'text',
					'items'=>array(
						'required' => true,
						'type'=>'text',
						'class'=>'grcparam'
					)
				),
				'Connecteur.ds_apikey' => array(
					'labelText' =>__d('connecteur', 'Connecteur.ds_apikey'),
					'inputType' => 'text',
					'items'=>array(
						'required' => true,
						'type'=>'text',
						'class'=>'dsparam'
					)
				),
				'Connecteur.ds_desktopmanager_id' => array(
					'labelText' =>'Bureau destinataire',
					'labelPlaceholder' => "Bureau destinataire",
					'inputType' => 'select',
					'items'=>array(
						'required' => true,
                       'type'=>'select',
                       'options'=> $scanDesktops,
                       'empty' => true
					)
				)
			)
		);
		echo $this->Formulaire->createForm($formConnecteurDs);
		?>
	</fieldset>
</div>
<?php
//echo $this->Form->end();
?>
<?php
echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 50%;'));
echo $this->Html->link(' <i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler', array('controller' => 'connecteurs', 'action' => 'index'), array('class' => 'btn btn-danger-webgfc btn-inverse closeModal', 'escape' => false, 'title' => 'Annuler'));
echo $this->Html->tag('/div', null);
echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 1%;'));
echo $this->Form->button(' <i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer', array('type' => 'submit', 'id' => 'boutonValider', 'class' => 'btn btn-success closeModal', 'escape' => false, 'title' => 'Modifier la configuration'));
echo $this->Html->tag('/div', null);
echo $this->Form->end();

?>
<script type="text/javascript">

	$('#ConnecteurMakeconfForm input[type=radio]').change(function () {
		$('#affiche').toggle();
	});

	if ($('#ConnecteurUseDsTrue').is(":checked")) {
		$('#affiche').show();
	}

	if ($('#ConnecteurUseDsFalse').is(":checked")) {
		$('#affiche').hide();
	}
	$('#ConnecteurDsDesktopmanagerId').select2({allowClear: true});
</script>
