<?php

/**
 *
 * Connecteurs/get_connecteurs.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (!empty($connecteurs)) {
    $fields = array(
        'name'
    );
    $actions = array(
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/connecteurs/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        "check" => array(
            "url" => Configure::read('BaseUrl') . '/connecteurs/check',
            "updateElement" => "$('#liste .content')",
        ),
        "sync" => array(
            "url" => Configure::read('BaseUrl') . '/connecteurs/sync',
            "updateElement" => "$('#liste .content')",
            "formMessage" => false
        )
    );
    $options = [];
    $data = $this->Liste->drawTbody($connecteurs, 'Connecteur', $fields, $actions, $options);
?>
<?php echo $this->Liste->drawPanelHeading($connecteurs,$options);?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_connecteurs"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           >
    </table>
</div>
<script>
    //title legend (nombre de données)
    if ($('.table-list h3 span').length < 1) {
        $('.table-list h3').append($('#title_nb'));
    }
    $('#table_connecteurs')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d("connecteur","Connecteur.name"); ?>",
                        class: "name"
                    },
					{
                        field: "active",
                        title: "Actif",
                        class: "active",
						width: "80px",
						align: "center"
                    },
                    {
                        field: "check",
                        title: "Vérifier",
                        class: "actions thCheck",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "sync",
                        title: "Synchroniser",
                        class: "actions thSync",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    }
                ]
            });
    $(document).ready(function () {
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }

</script>
<?php echo $this->Liste->drawScript($connecteurs, 'Connecteur', $fields, $actions, $options);
} else {
    echo $this->Html->div('alert alert-warning',__d('connecteur', 'Connecteur.void'));
}
?>

