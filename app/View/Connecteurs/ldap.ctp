<?php

$true_false = array('true' => 'Oui', 'false' => 'Non');
$formLDAP = array(
        'name' => 'Connecteur',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-6',
        'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'ldap'),
		'form_type' => 'file',
        'input' => array(
            'Connecteur.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type' => 'hidden',
                    'value' => $this->params['pass'][0]
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formLDAP);
?>
<fieldset>
    <legend>Activation de l'identification via LDAP</legend>
    <?php
    $formLdapRadio = array(
        'name' => 'Connecteur',
        'label_w'=>'col-sm-5',
        'input_w'=>'col-sm-6',
        'form_url' => array('controller' => 'connecteurs', 'action' => 'makeconf', 'ldap'),
        'input' =>array(
            'use_ldap' => array(
                'labelText' =>"",
                'inputType' => 'radio',
                'items'=>array(
                    'legend' => false,
                    'separator'=> '</div><div  class="radioUseLdapLabel">',
                    'before' => '<div class="radioUseLdapLabel">',
                    'after' => '</div>',
                    'label' => true,
                    'type'=>'radio',
                    'options' => $true_false,
                    'value' => ( isset( $this->request->data['Connecteur']['use_ldap'] ) && $this->request->data['Connecteur']['use_ldap'] ) ? 'true' : 'false'
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formLdapRadio);
    ?>
</fieldset>
<div id='affiche' class="panel-body form-horizontal">
    <div id ="activeLdap">
        <legend><i class="fa fa-sliders" style="font-size:16px;" aria-hidden="true"></i> <?php echo __d('connecteur', 'Connecteur.ldap_server'); ?></legend>
     <?php
        $formConnecteurLdapServer = array(
           'name' => 'Connecteur',
           'label_w' => 'col-sm-5',
           'input_w' => 'col-sm-7',
           'form_url' => array( 'controller' => 'connecteurs', 'action' => 'admin'),
           'input' => array(
               'Connecteur.ldap_type' => array(
                   'labelText' => __d('connecteur', 'Connecteur.ldap_type'),
                   'inputType' => 'select',
//                   'changeWidth' =>true,
                   'items'=>array(
                       'type'=>'select',
                       'class' =>'ldapparam',
                       'options'=> $type,
                       'empty' => true
                   )
               ),
               'Connecteur.ldap_host' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.ldap_host'),
                   'inputType' => 'text',
                   'labelPlaceholder' => "Exemple : http://ldap.x.x.x",
                   'items'=>array(
                       'type'=>'text',
                       'class'=>'ldapparam'
                   )
               ),
               'Connecteur.ldap_has_fall_over' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.ldap_has_fall_over'),
                   'inputType' => 'text',
                   'labelPlaceholder' => "Exemple : http://ldap.x.x.x",
                   'items'=>array(
                       'type'=>'text',
                       'class'=>'ldapparam'
                   )
               ),
               'Connecteur.ldap_port' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.ldap_port'),
                   'inputType' => 'number',
                   'labelPlaceholder' => "Exemple : 389",
                   'items'=>array(
                       'type'=>'number',
                       'class'=>'ldapparam'
                   )
               ),
               'Connecteur.ldap_login' => array(
                   'labelText' => __d('connecteur', 'Connecteur.ldap_login'),
                   'inputType' => 'text',
                   'items'=>array(
                       'type'=>'text',
                       'class' =>'ldapparam'
                   )
               ),
               'Connecteur.ldap_password' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.ldap_password'),
                   'inputType' => 'password',
                   'items'=>array(
                       'type'=>'password',
                       'class'=>'ldapparam'
                   )
               ),
               'Connecteur.ldap_account_suffix' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.ldap_account_suffix'),
                   'inputType' => 'text',
                   'items'=>array(
                       'type'=>'text',
                       'class'=>'ldapparam'
                   )
               ),
               'Connecteur.ldap_base_dn' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.ldap_base_dn'),
                   'inputType' => 'text',
                   'items'=>array(
                       'type'=>'text',
                       'class'=>'ldapparam'
                   )
               ),
               'Connecteur.ldap_uniqueid' => array(
                   'labelText' => __d('connecteur', 'Connecteur.ldap_uniqueid'),
                   'inputType' => 'text',
                   'items'=>array(
                       'type'=>'text',
                       'class' =>'ldapparam'
                   )
               ),
//               'Connecteur.ldap_dn' => array(
//                   'labelText' =>__d('connecteur', 'Connecteur.ldap_dn'),
//                   'inputType' => 'text',
//                   'items'=>array(
//                       'type'=>'text',
//                       'class'=>'ldapparam'
//                   )
//               ),
               'Connecteur.ldap_tls' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.ldap_tls'),
                   'inputType' => 'select',
                   'items'=>array(
                       'type'=>'select',
                       'options' => array( true => 'Oui', false => 'Non'),
                       'empty' => true,
                       'class'=>'ldapparam'
                   )
               ),
               'Connecteur.ldap_version' => array(
                   'labelText' =>__d('connecteur', 'Connecteur.ldap_version'),
                   'inputType' => 'text',
                   'labelPlaceholder' => "La version 3 du protocole est la plus courante",
                   'items'=>array(
                       'type'=>'text',
                       'class'=>'ldapparam'
                   )
               )

           )
        );
        echo $this->Formulaire->createForm($formConnecteurLdapServer);
    ?>
		<fieldset class='ldap-infos'>
			<legend>Certificat d'authentification</legend>
			<?php
			$formLdapsCertificatAuthentification = array(
					'name' => 'Connecteur',
					'label_w' => 'col-sm-5',
					'input_w' => 'col-sm-6',
					'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'ldap'),
					'form_type' => 'file',
					'input' => array(
						'Connecteur.ldaps_cert' => array(
							'labelText' =>'Certificat (crt)',
							'inputType' => 'file',
							'items'=>array(
								'type'=>'file',
								'name' => 'myfile',
								'class' => 'fileField'
							)
						)
					)
			);
			echo $this->Formulaire->createForm($formLdapsCertificatAuthentification);

			$value = @$this->request->data['Connecteur']['ldaps_nom_cert'];
			if( !empty( $value )) {
				echo 'Certificat utilisé : '.$value;
			}

			?>
		</fieldset>

    </div>
</div>
<?php
//echo $this->Form->end();
?>
   <?php

    echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 50%;'));
    echo $this->Html->link(' <i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler', array('controller' => 'connecteurs', 'action' => 'index'), array('class' => 'btn btn-danger-webgfc btn-inverse closeModal', 'escape' => false, 'title' => 'Annuler'));
    echo $this->Html->tag('/div', null);
    echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 1%;'));
    echo $this->Form->button(' <i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer', array('type' => 'submit', 'id' => 'boutonValider', 'class' => 'btn btn-success closeModal', 'escape' => false, 'title' => 'Modifier la configuration'));
    echo $this->Html->tag('/div', null);
    echo $this->Form->end();
    ?>
<script type="text/javascript">

    $('#ConnecteurMakeconfForm input[type=radio]').change(function () {
        $('#affiche').toggle();
    });

    if ($('#ConnecteurUSeLdapTrue').is(":checked")) {
        $('#affiche').show();
    }

    if ($('#ConnecteurUseLdapFalse').is(":checked")) {
        $('#affiche').hide();
    }

    $('#ConnecteurLdapType').select2({allowClear: true});
    $('#ConnecteurLdapTls').select2({allowClear: true});
</script>
