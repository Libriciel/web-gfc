<?php

/**
 *
 * CourrierMetadonnees/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="courrierMetadonnees view">
    <h2><?php __('Courrier Metadonnee'); ?></h2>
    <ul class="actions">
        <li><?php echo $this->Html->link(__('Edit Courrier Metadonnee', true), array('action' => 'edit', $courrierMetadonnee['CourrierMetadonnee']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('Delete Courrier Metadonnee', true), array('action' => 'delete', $courrierMetadonnee['CourrierMetadonnee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $courrierMetadonnee['CourrierMetadonnee']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('New Courrier Metadonnee', true), array('action' => 'add')); ?> </li>
    </ul>

    <dl><?php $i = 0; $class = ' class="altrow"'; ?>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('courrier_metadonnee', 'CourrierMetadonnee.id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $courrierMetadonnee['CourrierMetadonnee']['id']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('courrier_metadonnee', 'CourrierMetadonnee.valeur'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $courrierMetadonnee['CourrierMetadonnee']['valeur']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('courrier_metadonnee', 'CourrierMetadonnee.created'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $courrierMetadonnee['CourrierMetadonnee']['created']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('courrier_metadonnee', 'CourrierMetadonnee.modified'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $courrierMetadonnee['CourrierMetadonnee']['modified']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('courrier_metadonnee', 'CourrierMetadonnee.courrier_id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $this->Html->link($courrierMetadonnee['Courrier']['name'], array('controller' => 'courriers', 'action' => 'view', $courrierMetadonnee['Courrier']['id'])); ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('courrier_metadonnee', 'CourrierMetadonnee.metadonnee_id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0) echo $class; ?>>
                <?php echo $this->Html->link($courrierMetadonnee['Metadonnee']['name'], array('controller' => 'metadonnees', 'action' => 'view', $courrierMetadonnee['Metadonnee']['id'])); ?>
            &nbsp;
        </dd>
    </dl>

</div>
