<?php

/**
 *
 * CourrierMetadonnees/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="courrierMetadonnees form">
    <fieldset>
        <legend><?php __(sprintf('%s Courrier Metadonnee', Inflector::humanize($this->action))); ?></legend>
        <?php
        $formCourrierMetadonnee = array(
            'name' => 'CourrierMetadonnee',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-6',
            'input' => array(
                'CourrierMetadonnee.valeur' => array(
                    'labelText' =>__d('courrier_metadonnee', 'CourrierMetadonnee.valeur', true),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'CourrierMetadonnee.courrier_id' => array(
                    'labelText' =>__d('courrier_metadonnee', 'CourrierMetadonnee.courrier_id', true),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'options' => $courriers,
                         'empty' => true
                    )
                ),
                 'CourrierMetadonnee.metadonnee_id' => array(
                    'labelText' =>__d('courrier_metadonnee', 'CourrierMetadonnee.metadonnee_id', true),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'options' => $metadonnees,
                         'empty' => true
                    )
                )
            )
        );
        if ($this->action == 'edit') {
                 $formCourrierMetadonnee['input']['CourrierMetadonnee.id'] = array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                );
        }
        echo $this->Formulaire->createForm($formCourrierMetadonnee);
        echo $this->Form->end(__('Submit', true));
        ?>
    </fieldset>
</div>
