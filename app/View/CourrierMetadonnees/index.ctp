<?php

/**
 *
 * CourrierMetadonnees/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$pagination = '<p>' .
    $this->Paginator->counter(
                array(
                    'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
                )
        )
    . '</p>';

if (Set::classicExtract($this->params, 'paging.CourrierMetadonnee.pageCount') > 1) {
    $pagination = '<div class="pagination">
                            ' . implode(
                                '', Set::filter(
                                    array(
                                        $this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
                                        $this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
                                        $this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
                                        $this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
                                        $this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
                                    )
                                )
                            ) . '
                         </div>';
}
?>

<div class="courrierMetadonnees index">
    <h2><?php __('Courrier Metadonnees'); ?></h2>

    <ul class="actions">
        <li><?php echo $this->Html->link(__('New Courrier Metadonnee', true), array('action' => 'add')); ?></li>
    </ul>

<?php echo $pagination; ?>
<table>
    <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('CourrierMetadonnee.id', __d('courrier_metadonnee', 'CourrierMetadonnee.id')); ?></th>
            <th><?php echo $this->Paginator->sort('CourrierMetadonnee.valeur', __d('courrier_metadonnee', 'CourrierMetadonnee.valeur')); ?></th>
            <th><?php echo $this->Paginator->sort('CourrierMetadonnee.created', __d('courrier_metadonnee', 'CourrierMetadonnee.created')); ?></th>
            <th><?php echo $this->Paginator->sort('CourrierMetadonnee.modified', __d('courrier_metadonnee', 'CourrierMetadonnee.modified')); ?></th>
            <th><?php echo $this->Paginator->sort('Courrier.name', __d('courrier_metadonnee', 'Courrier.name')); ?></th>
            <th><?php echo $this->Paginator->sort('Metadonnee.name', __d('courrier_metadonnee', 'Metadonnee.name')); ?></th>
            <th class="actions" colspan="3"><?php echo __d('default', 'Actions'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 0;
        foreach ($courrierMetadonnees as $courrierMetadonnee):
            $i++;
            $class = ( $i % 2 ? 'odd' : 'even' );
        ?>
            <tr class="<?php echo $class; ?>">
                <td><?php echo $courrierMetadonnee['CourrierMetadonnee']['id']; ?>&nbsp;</td>
                <td><?php echo $courrierMetadonnee['CourrierMetadonnee']['valeur']; ?>&nbsp;</td>
                <td><?php echo $courrierMetadonnee['CourrierMetadonnee']['created']; ?>&nbsp;</td>
                <td><?php echo $courrierMetadonnee['CourrierMetadonnee']['modified']; ?>&nbsp;</td>
                <td>
                    <?php
                    echo $this->Html->link($courrierMetadonnee['Courrier']['name'], array('controller' => 'courriers',
                                                                                                                                'action' => 'view',
                                                                                                                                $courrierMetadonnee['Courrier']['id']));
                    ?>
                </td>
                <td>
                    <?php
                    echo $this->Html->link($courrierMetadonnee['Metadonnee']['name'], array('controller' => 'metadonnees',
                                                                                                                                                'action' => 'view',
                                                                                                                                                $courrierMetadonnee['Metadonnee']['id']));
                    ?>
                </td>
                <td class="actions">
                    <?php echo $this->Html->link(__('View', true), array('action' => 'view', $courrierMetadonnee['CourrierMetadonnee']['id'])); ?>
                </td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $courrierMetadonnee['CourrierMetadonnee']['id'])); ?>
                </td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $courrierMetadonnee['CourrierMetadonnee']['id']), null, sprintf(__('Are you sure you want to delete « %s » ?', true), $courrierMetadonnee['CourrierMetadonnee']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php echo $pagination; ?>
</div>
<script>
    $(document).ready(function () {
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
    });
</script>
