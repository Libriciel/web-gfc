<?php

/**
 *
 * Contactinfos/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
asort($civilite);
    echo $this->Form->create('Contactinfo',array(
        'class' => 'form-horizontal',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'control-group'),
            'label' => array('class' => 'control-label  col-sm-3'),
            'between' => '<div class="controls  col-sm-3">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        )));
    echo $this->Form->input('Contact.id');
?>
<?php
    echo $this->Form->input('Contactinfo.name', array('label' =>array('text'=>  __d('addressbook', 'Contactinfo.name'),'class'=>'control-label  col-sm-3')));
    echo $this->Form->input('Contactinfo.id', array('type' => 'hidden'));
?>
<div class="col-sm-8"></div>
<div class="row">
    <div class="panel" id="addContactIdent"  style="margin-top: 45px;" >
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo  __d('addressbook', 'Contact.fieldset.identity') ?></h4>
            </div>
            <div class="panel-body">
                <?php
                    echo $this->Form->input('Contactinfo.civilite', array('type' => 'select', 'options' => $civilite, 'label' =>array('text'=> __d('addressbook', 'Contact.civilite'),'class'=>'control-label  col-sm-3'),'class' => 'form-control'));
                    echo $this->Form->input('Contactinfo.nom', array('label' =>array('text'=>  __d('addressbook', 'Contact.nom'),'class'=>'control-label  col-sm-3')));
                    echo $this->Form->input('Contactinfo.prenom', array('label' =>array('text'=>  __d('addressbook', 'Contact.prenom'),'class'=>'control-label  col-sm-3')));
                ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="panel col-sm-6" id="addContactFicheCoor" >
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo  __d('addressbook', 'Contactinfo.fieldset.com') ?></h4>
            </div>
            <div class="panel-body">
                <?php
                    $fieldsetIdentity = $this->Html->tag('legend', __d('addressbook', 'Contactinfo.fieldset.identity'));
                    $fieldsetIdentity .= $this->Form->input('Contactinfo.civilite', array('type' => 'hidden', 'empty' => true, 'options' => $civilite, 'label' => __d('addressbook', 'Contactinfo.civilite')));
                    $fieldsetIdentity .= $this->Form->input('Contactinfo.nom', array('type' => 'hidden','label' => __d('addressbook', 'Contactinfo.nom')));
                    $fieldsetIdentity .= $this->Form->input('Contactinfo.prenom', array('type' => 'hidden','label' => __d('addressbook', 'Contactinfo.prenom')));
                    $fieldsetIdentity .= $this->Form->input('Contactinfo.contact_id', array('type' => 'hidden'));
                    echo $this->Html->tag('fieldset', $fieldsetIdentity, array('style' => 'display:none;'));

                    echo $this->Form->input('Contactinfo.email', array('label' =>array('text'=> __d('addressbook', 'Contactinfo.email'),'class'=>'control-label  col-sm-5')));
                    echo $this->Form->input('Contactinfo.tel', array('label' =>array('text'=> __d('addressbook', 'Contactinfo.tel'),'class'=>'control-label  col-sm-5')));
                    echo $this->Form->input('Contactinfo.portable', array('label' =>array('text'=> __d('addressbook', 'Contactinfo.portable'),'class'=>'control-label  col-sm-5')));

                ?>
            </div>
        </div>
    </div>
    <div class="panel col-sm-6" id="addContactLien" >
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo  __d('addressbook', 'Contactinfo.fieldset.work') ?></h4>
            </div>
            <div class="panel-body">
                <?php
                    echo $this->Form->input('Contactinfo.role', array('label' =>array('text'=> __d('addressbook', 'Contactinfo.role'),'class'=>'control-label  col-sm-5')));
                    echo $this->Form->input('Contactinfo.organisation', array('label' =>array('text'=> __d('addressbook', 'Contactinfo.organisation'),'class'=>'control-label  col-sm-5')));

                ?>
            </div>
        </div>
    </div>
</div>
<div class="panel" id="addContactAdd" >
    <div class="panel panel-default">
        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
            <h4 class="panel-title"><?php echo  __d('addressbook', 'Contactinfo.fieldset.loc') ?></h4>
        </div>
        <div class="panel-body">
            <?php
                echo $this->Form->input('Contactinfo.adressecomplete', array('label' =>array('text'=>  __d('addressbook', 'Contactinfo.adressecomplete' ),'class'=>'control-label  col-sm-3')));
                echo $this->Form->input('Contactinfo.numvoie', array('label' => array('text'=> __d('addressbook', 'Contactinfo.numvoie'),'class'=>'control-label  col-sm-3')));
                echo $this->Form->input('Contactinfo.typevoie', array('label' =>array('text'=>  __d('addressbook', 'Contactinfo.typevoie'),'class'=>'control-label  col-sm-3'), 'type' => 'select', 'empty' => true, 'options' => $options['Addressbook']['typevoie'],'class' => 'form-control'));
                echo $this->Form->input('Contactinfo.nomvoie', array('label' =>array('text'=>  __d('addressbook', 'Contactinfo.nomvoie'),'class'=>'control-label  col-sm-3')));
                echo $this->Form->input('Contactinfo.adresse', array('type' => 'hidden' ));
                echo $this->Form->input('Contactinfo.compl', array('label' => array('text'=> __d('addressbook', 'Contactinfo.compl'),'class'=>'control-label  col-sm-3')));
                echo $this->Form->input('Contactinfo.cp', array('label' =>array('text'=>  __d('addressbook', 'Contactinfo.cp'),'class'=>'control-label  col-sm-3')));
                echo $this->Form->input('Contactinfo.ville', array('label' => array('text'=> __d('addressbook', 'Contactinfo.ville'),'class'=>'control-label  col-sm-3')));
                echo $this->Form->input('Contactinfo.region', array('label' => array('text'=> __d('addressbook', 'Contactinfo.region'),'class'=>'control-label  col-sm-3')));
                echo $this->Form->input('Contactinfo.pays', array('label' => array('text'=> __d('addressbook', 'Contactinfo.pays'),'class'=>'control-label  col-sm-3'), 'default' => 'FRANCE'));
            ?>
        </div>
    </div>
</div>
    <?php
        echo $this->Form->end();
    ?>


<script type="text/javascript">
    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this,
                    currentCategory = "";

            $.each(items, function (index, item) {
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                self._renderItem(ul, item);
            });

        }

    });

    // Using jQuery UI's autocomplete widget:
    $('#ContactinfoAdressecomplete').bind("keydown", function (event) {
        if (event.keyCode != 16 &&
                event.keyCode != 17 &&
                event.keyCode != 18) {
            $('#ContactinfoBanId').val(0);
            $('#ContactinfoBancommuneId').html('');
        }
        if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    }).catcomplete({
        minLength: 3,
        source: '/contacts/searchAdresse',
        select: function (event, ui) {
            gui.request({
                url: '/contacts/searchAdresseSpecifique/' + ui.item.id
            }, function (data) {
                $('#ContactinfoBancommuneId').val(ui.item.id);
                var json = jQuery.parseJSON(data);
                var banadresse = json['Banadresse'];
                var ban = json['Ban'];
                if (typeof banadresse['id'] !== 'undefined') {
                    $('#ContactinfoNumvoie').val(banadresse['numero']);
                    $('#ContactinfoNomvoie').val(banadresse['nom_voie']);
                    $('#ContactinfoCp').val(banadresse['code_post']);
                    $('#ContactinfoVille').val(banadresse['nom_commune']);
                    $('#ContactinfoRegion').val(ban['name']);
                }
            });
        }
    });
    $('select').css('width', '170px');
</script>
