<?php

/**
 *
 * Contactinfos/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
asort($civilite);
echo $this->Form->create('Contactinfo');
echo $this->Form->input('Contactinfo.name', array('label' => __d('addressbook', 'Contactinfo.name')));

echo $this->Html->tag('br');
$fieldsetIdentity = $this->Html->tag('legend', __d('addressbook', 'Contactinfo.fieldset.identity'));
$fieldsetIdentity .= $this->Form->input('Contactinfo.civilite', array('type' => 'select', 'empty' => true, 'options' => $civilite, 'label' => __d('addressbook', 'Contactinfo.civilite')));
$fieldsetIdentity .= $this->Form->input('Contactinfo.nom', array('label' => __d('addressbook', 'Contactinfo.nom')));
$fieldsetIdentity .= $this->Form->input('Contactinfo.prenom', array('label' => __d('addressbook', 'Contactinfo.prenom')));
echo $this->Html->tag('fieldset', $fieldsetIdentity);

echo $this->Html->tag('br');

$fieldsetCom = $this->Html->tag('legend', __d('addressbook', 'Contactinfo.fieldset.com'));
$fieldsetCom .= $this->Form->input('Contactinfo.email', array('label' => __d('addressbook', 'Contactinfo.email')));
$fieldsetCom .= $this->Form->input('Contactinfo.tel', array('label' => __d('addressbook', 'Contactinfo.tel')));
$fieldsetCom .= $this->Form->input('Contactinfo.portable', array('label' => __d('addressbook', 'Contactinfo.portable')));
echo $this->Html->tag('fieldset', $fieldsetCom);

echo $this->Html->tag('br');
$fieldsetLoc = $this->Html->tag('legend', __d('addressbook', 'Contactinfo.fieldset.loc'));
$fieldsetLoc .= $this->Form->input('Contactinfo.adressecomplete', array('label' => __d('addressbook', 'Contactinfo.adressecomplete' )));
$fieldsetLoc .= $this->Form->input('Contactinfo.numvoie', array('label' => __d('addressbook', 'Contactinfo.numvoie')));
$fieldsetLoc .= $this->Form->input('Contactinfo.typevoie', array('label' => __d('addressbook', 'Contactinfo.typevoie'), 'type' => 'select', 'empty' => true, 'options' => $options['Addressbook']['typevoie']));
$fieldsetLoc .= $this->Form->input('Contactinfo.nomvoie', array('label' => __d('addressbook', 'Contactinfo.nomvoie')));
$fieldsetLoc .= $this->Form->input('Contactinfo.adresse', array('type' => 'hidden' ));
$fieldsetLoc .= $this->Form->input('Contactinfo.compl', array('label' => __d('addressbook', 'Contactinfo.compl')));
$fieldsetLoc .= $this->Form->input('Contactinfo.cp', array('label' => __d('addressbook', 'Contactinfo.cp')));
$fieldsetLoc .= $this->Form->input('Contactinfo.ville', array('label' => __d('addressbook', 'Contactinfo.ville')));
$fieldsetLoc .= $this->Form->input('Contactinfo.region', array('label' => __d('addressbook', 'Contactinfo.region')));
$fieldsetLoc .= $this->Form->input('Contactinfo.pays', array('label' => __d('addressbook', 'Contactinfo.pays'), 'default' => 'FRANCE'));
echo $this->Html->tag('fieldset', $fieldsetLoc);

echo $this->Html->tag('br');

$fieldsetWork = $this->Html->tag('legend', __d('addressbook', 'Contactinfo.fieldset.work'));
$fieldsetWork .= $this->Form->input('Contactinfo.role', array('label' => __d('addressbook', 'Contactinfo.role')));
$fieldsetWork .= $this->Form->input('Contactinfo.organisation', array('label' => __d('addressbook', 'Contactinfo.organisation')));
echo $this->Html->tag('fieldset', $fieldsetWork);
echo $this->Form->end();
?>
<script type="text/javascript">

    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this,
                    currentCategory = "";
            $.each(items, function (index, item) {
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                self._renderItem(ul, item);
            });

        }

    });


    // Using jQuery UI's autocomplete widget:
    $('#ContactinfoAdressecomplete').bind("keydown", function (event) {
        if (event.keyCode != 16 &&
                event.keyCode != 17 &&
                event.keyCode != 18) {
            $('#ContactinfoBanId').val(0);
            $('#ContactinfoBancommuneId').html('');
        }
        if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    }).catcomplete({
        minLength: 3,
        source: '/contacts/searchAdresse',
        select: function (event, ui) {
            gui.request({
                url: '/contacts/searchAdresseSpecifique/' + ui.item.id
            }, function (data) {
                $('#ContactinfoBancommuneId').val(ui.item.id);
                var json = jQuery.parseJSON(data);
                var banadresse = json['Banadresse'];
                var ban = json['Ban'];
                if (typeof banadresse['id'] !== 'undefined') {
                    $('#ContactinfoNumvoie').val(banadresse['numero']);
                    $('#ContactinfoNomvoie').val(banadresse['nom_voie']);
                    $('#ContactinfoCp').val(banadresse['code_post']);
                    $('#ContactinfoVille').val(banadresse['nom_commune']);
                    $('#ContactinfoRegion').val(ban['name']);
                }
            });
        }
    });
    $('#ContactinfoTypevoie').select2({allowClear: true, placeholder: "Sélectionner un type de voie"});
    $('#ContactinfoCivilite').select2({allowClear: true, placeholder: "Sélectionner une civilité"});
    $('#ContactinfoBanId').select2({allowClear: true, placeholder: "Sélectionner un département"});
    $('#ContactinfoBancommuneId').select2({allowClear: true, placeholder: "Sélectionner une commune"});
    $('#ContactinfoBanadresse').select2({allowClear: true, placeholder: "Sélectionner une adresse"});
    var bans = [];
        <?php foreach ($bans as $ban) { ?>
    var ban = {
        id: "<?php echo $ban['Ban']['id']; ?>",
        name: "<?php echo $ban['Ban']['name']; ?>",
        banscommunes: []
    };
                <?php foreach ($ban['Bancommune'] as $communeB) { ?>
    var bancommune = {
        id: "<?php echo $communeB['id']; ?>",
        name: "<?php echo $communeB['name']; ?>"
    };
    ban.banscommunes.push(bancommune);
                <?php } ?>
    bans.push(ban);
        <?php } ?>

    function fillBanscommunes(ban_id, bancommune_id) {
        $("#ContactinfoBancommuneId").empty();
        $("#ContactinfoBancommuneId").append($("<option value=''> --- </option>"));

        for (i in bans) {
            if (bans[i].id == ban_id) {
                for (j in bans[i].banscommunes) {
                    if (bans[i].banscommunes[j].id == bancommune_id) {
                        $("#ContactinfoBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "' selected='selected'>" + bans[i].banscommunes[j].name + "</option>"));
                    } else {
                        $("#ContactinfoBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "'>" + bans[i].banscommunes[j].name + "</option>"));
                    }
                }
            }
        }
    }


    $("#ContactinfoBanId").append($("<option value=''> --- </option>"));
    //remplissage de la liste des types
    for (i in bans) {
        $("#ContactinfoBanId").append($("<option value='" + bans[i].id + "'>" + bans[i].name + "</option>"));
    }
    //definition de l action sur type
    $("#ContactinfoBanId").bind('change', function () {
        var ban_id = $('option:selected', this).attr('value');
        fillBanscommunes(ban_id);
    });

    var checkAdresses = function (bancommune_id) {
        $("#ContactinfoBanadresse").empty();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Organismes/getAdresses/' + bancommune_id,
            data: $('#ContactinfoAddFromCourrierForm').serialize(),
            success: function (data) {
                if (data.length > 0) {
                    $("#ContactinfoBanadresse").append($("<option value=''> --- </option>"));
                    // remplissage de la partie adresse
                    for (var i in data) {
                        $("#ContactinfoBanadresse").append($("<option value='" + data[i].Banadresse.nom_afnor + "'>" + data[i].Banadresse.nom_afnor + "</option>"));
                    }
                }
            }
        });
    }

    $("#ContactinfoBancommuneId").bind('change', function () {
        var bancommune_id = $('option:selected', this).attr('value');
        checkAdresses(bancommune_id);
    });
</script>
