
<div class="bannette-content">
<div class="container" style="width:70%">
	<div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>

	<h1 class="mt-5">Page de politique de confidentialité</h1>

	<div class="row mt-4">
		<div class="col-10 offset-1 form-white">

			La création de votre compte utilisateur sur web-GFC demande la
			saisie de données personnelles vous concernant. Vous trouverez ci-dessous le
			détail des informations collectées ainsi que les traitements effectués. Ces
			données sont collectées uniquement dans le cadre de votre activité
			professionnelle et pour l'utilisation exclusive de
			l'application web-GFC.
			Le fonctionnement dans les conditions nominales et recommandées de
			l'application, ne permet pas de partager ou de diffuser à un tiers vos
			informations personnelles. Vous pouvez demander à tout moment à l'administrateur
			de la plateforme de vous communiquer l'ensemble des informations collectées pour
			la création de votre compte.

			<h2 class="mt-3">Article 1 - Mentions légales</h2>
			<h3 class="mt-3">1.1 Logiciel (ci-après « le logiciel ») :</h3>
			web-GFC
			<h3 class="mt-3">1.2 Éditeur (ci-après « l'éditeur ») :</h3>
			LIBRICIEL SCOP<br>

			836 Rue du Mas de Verchant - 34000 Montpellier<br>

			numéro SIRET : 491 011 698 00025 <br>

			<h2 class="mt-3">Article 2 - Hébergement</h2>


			<h3 class="mt-3">2.1 Hébergeur (ci-après « l'hébergeur ») :</h3>
			Vos données sont hébergées chez plusieurs de nos sous-traitants, uniquement sur le territoire Français.
			web-GFC est hébergée par : <br>

			<?php echo isset($values['Rgpd']['nomhebergeur'] ) ? $values['Rgpd']['nomhebergeur'] : null;?><br>

			<?php echo isset($values['Rgpd']['adressecompletehebergeur']) ? $values['Rgpd']['adressecompletehebergeur'] : null;?><br>

			représentée par : <?php echo isset($values['Rgpd']['representanthebergeur']) ? $values['Rgpd']['representanthebergeur'] : null;?>, en sa qualité de :
			<?php echo isset($values['Rgpd']['qualiterepresentanthebergeur']) ? $values['Rgpd']['qualiterepresentanthebergeur'] : null;?> <br>

			numéro SIRET : <?php echo isset($values['Rgpd']['sirethebergeur']) ? $values['Rgpd']['sirethebergeur'] : null;?> <br>

			code APE : <?php echo isset($values['Rgpd']['codeapehebergeur']) ? $values['Rgpd']['codeapehebergeur'] : null;?>  <br>

			n° de téléphone : <?php echo isset($values['Rgpd']['numtelhebergeur'] ) ? $values['Rgpd']['numtelhebergeur'] : null;?> <br>

			e-mail : <?php echo isset($values['Rgpd']['mailhebergeur']) ? $values['Rgpd']['mailhebergeur'] : null;?>  <br>
			<h3 class="mt-3">2.2 Délégué à la protection des données (DPO) :</h3>
			Vous pouvez contacter notre Délégué à la Protection des Données à l'adresse mail suivante : <br>

			<?php echo isset($values['Rgpd']['maildpo']) ? $values['Rgpd']['maildpo'] : null;?>


			<h2 class="mt-3">Article 3 - Collecte</h2>
			Création d'un utilisateur : <br>
			Lors de la création de votre compte utilisateur, les informations suivantes sont collectées obligatoirement
			: <br>
				<ul>
					<li>Nom d'utilisateur </li>
					<li>Prénom d'utilisateur </li>
					<li>Adresse de messagerie </li>
				</ul>


			<h2 class="mt-3">Article 4 - Traitements</h2>
			Vos informations personnelles sont utilisées pour le bon fonctionnement de web-GFC dans les cas listés
			ci-après :<br>

			Lister les utilisateurs de l'application : <br>
			<ul><li>votre identifiant unique, votre prénom, votre nom et votre adresse de messagerie apparaissent dans la liste. </li></ul>

			Journal d'événement d'un flux : <br>
			<ul><li>votre identifiant unique et votre adresse IP apparaissent pour toutes les actions réalisées dans l'application</li></ul>

			Pour les notifications : <br>
			<ul><li>votre adresse de messagerie </li></ul>

			<h2 class="mt-3">Article 5 - Droit à l'oubli</h2>
			Sur votre demande ou à la demande de l’administrateur de web-GFC, votre compte utilisateur peut être
			supprimé définitivement. <br>

			La suppression de votre compte laissera les traces de votre activité dans le journal des événements pendant une durée de 6 mois après la suppression de l'utilisateur <br>

			<h2 class="mt-3">Article 6 - Vos droits sur les données vous concernant</h2>
			Vous pouvez accéder et obtenir une copie des données vous concernant, vous opposer au traitement de ces
			données,
			les faire rectifier ou les faire effacer. Vous disposez également d'un droit à la limitation du traitement
			de vos données.
			<a href="https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles"> Comprendre vos droits
				informatique et libertés</a>
			<br>

			Pour exercer vos droits sur les données personnelles vous concernant, vous
			pouvez contacter le Délégué à la Protection des Données (DPO), à l’adresse
			suivante : <br>

			<ul><li>dpo@libriciel.coop </li></ul>

			Réclamation (plainte) auprès de la CNIL
			Si vous estimez, après nous avoir contacté, que vos droits sur vos données ne
			sont pas respectés, vous pouvez <a href="https://www.cnil.fr/fr/plaintes">adresser une réclamation (plainte)
				à la CNIL</a>.
		</div>
	</div>
</div>
</div>


<script type="text/javascript">
	gui.addbuttons({
		element: $('#backButton'),
		buttons: [
			{
				content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
				class: "btn-info-webgfc",
				action: function () {
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/admin"; ?>";
				}
			}
		]
	});
</script>
