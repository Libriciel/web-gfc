<?php

/**
 *
 * Tests/userinfo.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
if (isset($chooseUser)) {
	echo $this->Html->tag('h4', 'Please choose an user :');
	foreach ($chooseUser as $kUser => $user) {
		echo $this->Html->tag('div', $this->Html->link($user, array($kUser)));
	}
	echo $this->Html->tag('div', $this->Html->link('Add User', array('controller' => 'users', 'action' => 'add')));
} else {
	echo $this->Html->tag('h3', 'User');
	$dlContent = $this->Html->tag('dt', 'Username');
	$dlContent .= $this->Html->tag('dd', $user['User']['username']);
	$dlContent .= $this->Html->tag('dt', 'Desktop');
	$dlDesktopContent = $this->Html->link($user['Desktop']['name'], array('action' => 'desktopinfo', $user['Desktop']['id']));
	$dlDesktopContent .= $this->Html->tag('dt', 'Profil');
	$dlDesktopContent .= $this->Html->tag('dd', $user['Desktop']['Profil']['name']);
	$dlDesktopContent .= $this->Html->tag('dt', 'Agents');
	$ddDesktopAgents = '';
	foreach ($user['Desktop']['Agent'] as $agent) {
		$ddDesktopAgents .= $this->Html->tag('li', $agent['name']);
	}
	$dlDesktopContent .= $this->Html->tag('dd', $this->Html->tag('ul', $ddDesktopAgents));
	$dlContent .= $this->Html->tag('dd', $this->Html->tag('dl', $dlDesktopContent));
	$dlContent .= $this->Html->tag('dt', 'Secondary Desktops');
	$ulSecondaryDesktopsContent = '';
	foreach ($user['SecondaryDesktop'] as $secDesktop) {
		$liSecDesktopContent = $this->Html->link($secDesktop['name'], array('action' => 'desktopinfo', $secDesktop['id']));
		$dlSecDesktopContent = $this->Html->tag('dt', 'Profil');
		$dlSecDesktopContent .= $this->Html->tag('dd', $secDesktop['Profil']['name']);
		$dlSecDesktopContent .= $this->Html->tag('dt', 'Agents');
		$ddSecDesktopAgents = '';
		foreach ($secDesktop['Agent'] as $agent) {
			$ddSecDesktopAgents .= $this->Html->tag('li', $agent['name']);
		}
		$dlSecDesktopContent .= $this->Html->tag('dd', $this->Html->tag('ul', $ddSecDesktopAgents));
		$liSecDesktopContent .= $this->Html->tag('dl', $dlSecDesktopContent);
		$ulSecondaryDesktopsContent .= $this->Html->tag('li', $liSecDesktopContent);
	}
	$dlContent .= $this->Html->tag('dd', $this->Html->tag('ul', $ulSecondaryDesktopsContent));
	echo $this->Html->tag('dl', $dlContent);
}
?>
