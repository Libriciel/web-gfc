<?php
/**
 *
 * Tessts/desktopinfo.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
echo $this->Html->css('/DataAcl/css/acl');
echo $this->Html->script('/DataAcl/js/jquery');
echo $this->Html->script('/DataAcl/js/jquery.acl');

if (!empty($this->request->data)) {

} else {
	if (isset($chooseDesktop)) {
		echo $this->Html->tag('h4', 'Please choose a desktop:');
		foreach ($chooseDesktop as $kDesktop => $desktop) {
			echo $this->Html->tag('div', $this->Html->link($desktop, array($kDesktop)));
		}
		echo $this->Html->tag('div', $this->Html->link('Add User', array('controller' => 'users', 'action' => 'add')));
	} else {
		//init options
		$options = array(
			'no' => $this->Html->image('/DataAcl/img/test-fail-icon.png', array('style' => 'width: 16px;', 'alt' => 'no')),
			'yes' => $this->Html->image('/DataAcl/img/test-pass-icon.png', array('style' => 'width: 16px;', 'alt' => 'yes')),
            'parentNodeMarkClose' => '<i class="fa fa-minus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
            'parentNodeMarkOpen' => '<i class="fa fa-plus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
			'edit' => true,
			'requester' => $requester
		);

		//show func rigths
		$options['fields'] = array(
			'read' => 'accès'
		);
		$options['fieldsetLegend'] = 'Acl: Controllers/Actions';
		$options['aclType'] = 'Acl';
		$options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_rights';
		$this->DataAcl->drawTable($rights[0], true, $options);

		//show data rigths
		$options['aclType'] = 'DataAcl';
		foreach ($dataRights as $desktopAgent => $rights) {
			$requester = array('model' => 'DesktopsAgent', 'foreign_key' => $desktopAgent);
			$options['requester'] = $requester;
			$options['fieldsetLegend'] = 'DataAcl: Types/Soustypes - DesktopsAgent ' . $desktopAgent;
			$options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_data';
			$this->DataAcl->drawTable($rights, false, $options);
		}

		//show meta rigths
		$options['fields'] = array(
			'create' => 'création',
			'read' => 'lecture',
			'update' => 'édition',
			'delete' => 'suppression'
		);
		$options['aclType'] = 'DataAcl';
		foreach ($metaRights as $desktopAgent => $rights) {
			$requester = array('model' => 'DesktopsAgent', 'foreign_key' => $desktopAgent);
			$options['requester'] = $requester;
			$options['fieldsetLegend'] = 'DataAcl: Metadonnees - DesktopsAgent ' . $desktopAgent;
			$options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_meta';
			$this->DataAcl->drawTable($rights, false, $options);
		}
	}
}
?>

<script type="text/javascript">
    initTables(true);
</script>
