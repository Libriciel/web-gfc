<?php
/**
 *
 * Tests/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
echo $this->Html->script('/DataAcl/js/jquery');

echo $this->Html->tag('h3', 'desktops');
if (!empty($desktops)) {
	$desktopsHeaders = array_keys($desktops[0]['Desktop']);

	$desktopsCells = array();
	foreach ($desktops as $item) {
		$item['Desktop']['id'] .= '<span id="Desktop_' . $item['Desktop']['id'] . '"';
		$desktopsCells[] = $item['Desktop'];
	}

	echo "<div><table id='desktopsTable'>";
	echo $this->Html->tableHeaders($desktopsHeaders);
	echo $this->Html->tableCells($desktopsCells);
	echo "</table></div>";
}

echo $this->Html->tag('h3', 'desktops_agents');
if (!empty($desktops_agents)) {
	$desktopsAgentsHeaders = array_keys($desktops_agents[0]['DesktopsAgent']);

	$desktopsAgentsCells = array();
	foreach ($desktops_agents as $item) {
		$item['DesktopsAgent']['id'] .= '<span id="DesktopsAgent_' . $item['DesktopsAgent']['id'] . '"';
		$desktopsAgentsCells[] = $item['DesktopsAgent'];
	}
	echo "<div><table id='desktopsAgentsTable'>";
	echo $this->Html->tableHeaders($desktopsAgentsHeaders);
	echo $this->Html->tableCells($desktopsAgentsCells);
	echo "</table></div>";
}

echo $this->Html->tag('h3', 'groups');
if (!empty($groups)) {
	$groupsHeaders = array_keys($groups[0]['Profil']);

	$groupsCells = array();
	foreach ($groups as $item) {
		$item['Profil']['id'] .= '<span id="Profil_' . $item['Profil']['id'] . '"';
		$groupsCells[] = $item['Profil'];
	}

	echo "<div><table id='groupsTable'>";
	echo $this->Html->tableHeaders($groupsHeaders);
	echo $this->Html->tableCells($groupsCells);
	echo "</table></div>";
}

echo $this->Html->tag('h3', 'agents');
if (!empty($agents)) {
	$agentsHeaders = array_keys($agents[0]['Agent']);

	$agentsCells = array();
	foreach ($agents as $item) {
		$item['Agent']['id'] .= '<span id="Agent_' . $item['Agent']['id'] . '"';
		$agentsCells[] = $item['Agent'];
	}

	echo "<div><table id='agentsTable'>";
	echo $this->Html->tableHeaders($agentsHeaders);
	echo $this->Html->tableCells($agentsCells);
	echo "</table></div>";
}

echo $this->Html->tag('h3', 'types');
if (!empty($types)) {
	$typesHeaders = array_keys($types[0]['Type']);

	$typesCells = array();
	foreach ($types as $item) {
		$item['Type']['id'] .= '<span id="Type_' . $item['Type']['id'] . '"';
		$typesCells[] = $item['Type'];
	}

	echo "<div><table id='typesTable'>";
	echo $this->Html->tableHeaders($typesHeaders);
	echo $this->Html->tableCells($typesCells);
	echo "</table></div>";
}

echo $this->Html->tag('h3', 'soustypes');
if (!empty($soustypes)) {
	$soustypesHeaders = array_keys($soustypes[0]['Soustype']);

	$soustypesCells = array();
	foreach ($soustypes as $item) {
		$item['Soustype']['id'] .= '<span id="Soustype_' . $item['Soustype']['id'] . '"';
		$soustypesCells[] = $item['Soustype'];
	}

	echo "<div><table id='soustypesTable'>";
	echo $this->Html->tableHeaders($soustypesHeaders);
	echo $this->Html->tableCells($soustypesCells);
	echo "</table></div>";
}

echo $this->Html->tag('h3', 'metadonnees');
if (!empty($metadonnees)) {
	$metadonneesHeaders = array_keys($metadonnees[0]['Metadonnee']);

	$metadonneesCells = array();
	foreach ($metadonnees as $item) {
		$item['Metadonnee']['id'] .= '<span id="Metadonnee_' . $item['Metadonnee']['id'] . '"';
		$metadonneesCells[] = $item['Metadonnee'];
	}

	echo "<div><table id='metadonneesTable'>";
	echo $this->Html->tableHeaders($metadonneesHeaders);
	echo $this->Html->tableCells($metadonneesCells);
	echo "</table></div>";
}

echo $this->Html->tag('h3', 'daros');
if (!empty($daros)) {
	$darosHeaders = array_keys($daros[0]['Daro']);

	$darosCells = array();
	foreach ($daros as $item) {
		$item['Daro']['id'] .= '<span id="daro_' . $item['Daro']['id'] . '">&nbsp;</span>';
		$item['Daro']['foreign_key'] .= ' <a href="#' . $item['Daro']['model'] . '_' . $item['Daro']['foreign_key'] . '">' . $item['Daro']['model'] . '</a>';
		if (!empty($item['Daro']['parent_id'])) {
			$item['Daro']['parent_id'] .= ' <a href="#daro_' . $item['Daro']['parent_id'] . '">daro</a>';
		}
		$darosCells[] = $item['Daro'];
	}

	echo "<div><table id='darosTable'>";
	echo $this->Html->tableHeaders($darosHeaders);
	echo $this->Html->tableCells($darosCells);
	echo "</table></div>";
}

echo $this->Html->tag('h3', 'dacos');
if (!empty($dacos)) {
	$dacosHeaders = array_keys($dacos[0]['Daco']);

	$dacosCells = array();
	foreach ($dacos as $item) {
		$item['Daco']['id'] .= '<span id="daco_' . $item['Daco']['id'] . '">&nbsp;</span>';
		$item['Daco']['foreign_key'] .= ' <a href="#' . $item['Daco']['model'] . '_' . $item['Daco']['foreign_key'] . '">' . $item['Daco']['model'] . '</a>';
		if (!empty($item['Daco']['parent_id'])) {
			$item['Daco']['parent_id'] .= ' <a href="#daco_' . $item['Daco']['parent_id'] . '">daco</a>';
		}
		$dacosCells[] = $item['Daco'];
	}

	echo "<div><table id='dacosTable'>";
	echo $this->Html->tableHeaders($dacosHeaders);
	echo $this->Html->tableCells($dacosCells);
	echo "</table></div>";
}


echo $this->Html->tag('h3', 'daros_dacos (dpermissions)');
if (!empty($daros_dacos)) {
	$daros_dacosHeaders = array_keys($daros_dacos[0]['DarosDaco']);

	$daros_dacosCells = array();
	foreach ($daros_dacos as $item) {
		$item['DarosDaco']['daro_id'] .= ' <a href="#daro_' . $item['DarosDaco']['daro_id'] . '">daro</a>';
		$item['DarosDaco']['daco_id'] .= ' <a href="#daco_' . $item['DarosDaco']['daco_id'] . '">daco</a>';
		foreach ($item['DarosDaco'] as $key => $val) {
			if ($key == '_create' || $key == '_read' || $key == '_update' || $key == '_delete') {
				$item['DarosDaco'][$key] = '<div style="text-align:center;color: white;padding:5px;width: 25px;font-weight: bold;background-color: ';
				if ($val == 1) {
					$item['DarosDaco'][$key] .= 'green';
				}
				if ($val == -1) {
					$item['DarosDaco'][$key] .= 'red';
				}
				if ($val == 0) {
					$item['DarosDaco'][$key] .= 'blue';
				}
				$item['DarosDaco'][$key] .= ';">' . $val . '</div>';
			}
		}
		$daros_dacosCells[] = $item['DarosDaco'];
	}

	echo "<div><table id='daros_dacosTable'>";
	echo $this->Html->tableHeaders($daros_dacosHeaders);
	echo $this->Html->tableCells($daros_dacosCells);
	echo "</table></div>";
}

echo $this->Html->tag('h3', 'aros');
if (!empty($aros)) {
	$arosHeaders = array_keys($aros[0]['Aro']);

	$arosCells = array();
	foreach ($aros as $item) {
		$item['Aro']['id'] .= '<span id="aro_' . $item['Aro']['id'] . '">&nbsp;</span>';
		$item['Aro']['foreign_key'] .= ' <a href="#' . $item['Aro']['model'] . '_' . $item['Aro']['foreign_key'] . '">' . $item['Aro']['model'] . '</a>';
		if (!empty($item['Aro']['parent_id'])) {
			$item['Aro']['parent_id'] .= ' <a href="#aro_' . $item['Aro']['parent_id'] . '">aro</a>';
		}
		$arosCells[] = $item['Aro'];
	}

	echo "<div><table id='arosTable'>";
	echo $this->Html->tableHeaders($arosHeaders);
	echo $this->Html->tableCells($arosCells);
	echo "</table></div>";
}


echo $this->Html->tag('h3', 'acos');
if (!empty($acos)) {
	$acosHeaders = array_keys($acos[0]['Aco']);

	$acosCells = array();
	foreach ($acos as $item) {
		$item['Aco']['id'] .= '<span id="aco_' . $item['Aco']['id'] . '">&nbsp;</span>';
		if (!empty($item['Aco']['parent_id'])) {
			$item['Aco']['parent_id'] .= ' <a href="#aco_' . $item['Aco']['parent_id'] . '">aco</a>';
		}
		$acosCells[] = $item['Aco'];
	}

	echo "<div><table id='acosTable'>";
	echo $this->Html->tableHeaders($acosHeaders);
	echo $this->Html->tableCells($acosCells);
	echo "</table></div>";
}


echo $this->Html->tag('h3', 'aros_acos (permissions)');
if (!empty($aros_acos)) {
	$aros_acosHeaders = array_keys($aros_acos[0]['Permission']);

	$aros_acosCells = array();
	foreach ($aros_acos as $item) {

		$item['Permission']['aro_id'] .= ' <a href="#aro_' . $item['Permission']['aro_id'] . '">aro</a>';
		$item['Permission']['aco_id'] .= ' <a href="#aco_' . $item['Permission']['aco_id'] . '">aco</a>';

		foreach ($item['Permission'] as $key => $val) {
			if ($key == '_create' || $key == '_read' || $key == '_update' || $key == '_delete') {
				$item['Permission'][$key] = '<div style="text-align:center;color: white;padding:5px;width: 25px;font-weight: bold;background-color: ';
				if ($val == 1) {
					$item['Permission'][$key] .= 'green';
				}
				if ($val == -1) {
					$item['Permission'][$key] .= 'red';
				}
				if ($val == 0) {
					$item['Permission'][$key] .= 'blue';
				}
				$item['Permission'][$key] .= ';">' . $val . '</div>';
			}
		}
		$aros_acosCells[] = $item['Permission'];
	}

	echo "<div><table id='aros_acosTable'>";
	echo $this->Html->tableHeaders($aros_acosHeaders);
	echo $this->Html->tableCells($aros_acosCells);
	echo "</table></div>";
}
?>

<script type="text/javascript">
	$('h3').click(function() {
		$(this).next('div').slideToggle('fast');
	});
</script>
