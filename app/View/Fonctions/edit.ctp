<?php

/**
 *
 * Fonctions/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$formFonction = array(
    'name' => 'Fonction',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Fonction.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Fonction.name' => array(
            'labelText' =>__d('fonction', 'Fonction.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Fonction.description' => array(
            'labelText' =>__d('fonction', 'Fonction.description'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'Fonction.active' => array(
            'labelText' =>__d('fonction', 'Fonction.active'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['Fonction']['active']
            )
        )
    )
);
echo $this->Formulaire->createForm($formFonction);
echo $this->Form->end();
?>

