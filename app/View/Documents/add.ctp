<?php

/**
 *
 * Gabaritsdocuments/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (empty($message)) {
?>

<?php
$formDocument = array(
    'name' => 'Document',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-5',
	'form_url' => array( 'controller' => 'documents', 'action' => 'add' ),
    'form_target' => 'modelUploadFrame',
    'form_type' => 'file',
    'enctype' =>'multipart/form-data',
    'input' => array(
        'Document.name' => array(
            'labelText' =>__d('document', 'Document.name'),
            'inputType' => 'text',
            'items'=>array(
                'value' => 'Bordereau historique.odt',
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Document.format' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value' => 'ott'
            )
        ),
        'Document.ishistorique' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value' => true
            )
        ),
        'Document.file' => array(
            'labelText' =>__d('document', 'Document.file'),
            'inputType' => 'file',
            'items'=>array(
                'type'=>'file',
                'name' => 'myfile',
                'class' => 'fileField'
            )
        )
    )
);
echo $this->Formulaire->createForm($formDocument);
echo $this->Form->end();


if (!empty($warnings)) {
        $warningsContent = '';
        for ($i = 0; $i < count($warnings); $i++) {
                $warningsContent .= __d('document', 'Document.warning.' . $warnings[$i]) . $this->Html->tag('br');
        }
        echo $this->Html->tag('div', $warningsContent, array('class' => 'formatConnWarnings alert alert-warning'));
}
?>
<script type="text/javascript">
    $("#modelUploadFrame").remove();
    $('body').append('<iframe name="modelUploadFrame" id="modelUploadFrame" src="#" style="width:0;height:0;border:none;"></iframe>');

</script>
<?php } else { ?>
<script type="text/javascript">
    if (window.top.window.jQuery.jGrowl) {
        window.top.window.jQuery.jGrowl.defaults.theme = 'ui-state-focus';
<?php if (!$create) { ?>
        window.top.window.jQuery.jGrowl.defaults.theme = 'ui-state-error';
<?php } ?>
        window.top.window.jQuery.jGrowl("<div class='info'><?php echo $message; ?></div>", {});
        window.top.window.stype_tabs_reload();
    }
</script>
<?php } ?>
