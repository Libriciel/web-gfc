<?php
/**
 *
 * Documents/get_document_list.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
echo $this->Element('documentList', array('documentList' => $documentList));
echo $this->Element('documentSigne', array('documentSigne' => $documentSigne));
?>
<script type="text/javascript">
    layer.msg('<?php echo __d('document', 'Document.list.loaded'); ?>', {});
    initContextDocumentBttnActions();
</script>
