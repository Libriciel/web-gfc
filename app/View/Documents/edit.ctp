<?php

/**
 *
 * Collectivites/scanemail_add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (empty($message)) {
?>

<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formDocument = array(
    'name' => 'Document',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-5',
	'form_url' => array( 'controller' => 'documents', 'action' => 'edit', $id ),
    'form_target' => 'modelUploadFrame',
    'form_type' => 'file',
    'enctype' =>'multipart/form-data',
    'input' => array(
        'Document.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value'=> $id
            )
        ),
        'Document.name' => array(
            'labelText' =>__d('document', 'Document.name'),
            'inputType' => 'text',
            'items'=>array(
                'value' => 'Bordereau historique.odt',
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Document.format' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value' => 'ott'
            )
        ),
        'Document.ishistorique' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value' => true
            )
        ),
        'Document.file' => array(
            'labelText' =>__d('document', 'Document.file'),
            'inputType' => 'file',
            'items'=>array(
                'type'=>'file',
                'name' => 'myfile',
                'class' => 'fileField'
            )
        )
    )
);
echo $this->Formulaire->createForm($formDocument);
echo $this->Form->end();

if (!empty($warnings)) {
        $warningsContent = '';
        for ($i = 0; $i < count($warnings); $i++) {
                $warningsContent .= __d('document', 'Document.warning.' . $warnings[$i]) . $this->Html->tag('br');
        }
        echo $this->Html->tag('div', $warningsContent, array('class' => 'formatConnWarnings alert alert-warning'));
}
?>
<script type="text/javascript">
    $("#modelUploadFrame").remove();
    $('body').append('<iframe name="modelUploadFrame" id="modelUploadFrame" src="#" style="width:0;height:0;border:none;"></iframe>');

</script>
<?php } ?>
