<?php

/**
 *
 * Documents/get_documents.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
if (!empty($documents)) {

    $fields = array(
        'name'
    );

    $actions = array(
        'download' => array(
            "url" => Configure::read('BaseUrl') . '/documents/download',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        'edit' => array(
            "url" => "",
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        'delete' => array(
            "url" => Configure::read('BaseUrl') . '/documents/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadDocument();"
        )
    );

    $options = array();
    $data = $this->Liste->drawTbody($documents, 'Document', $fields, $actions, $options);
?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_documents"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-pagination = "true"
           >
    </table>
</div>

<script>
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($documents,$options); ?>');
    $('#table_documents')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d("document","Document.name"); ?>",
                        class: "name"
                    },
                    {
                        field: "download",
                        title: "Télécharger",
                        class: "actions thDownload",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            });
    $(document).ready(function () {
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }



    $('.thEdit').click(function () {
            var docId = $(this).children().attr('itemId');
            var url = "/documents/edit/" + docId;
            gui.formMessage({
                updateElement: $('body'),
                loader: true,
                width: '550px',
                loaderMessage: gui.loaderMessage,
                title: "Modèle de bordereau",
                url: url,
                buttons: {
                    '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    },
                    '<i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer': function () {
                        var form = $('#DocumentEditForm');
                        if (form_validate(form)) {
                            form.submit();
                            $('#DocumentEditForm').remove();
                            loadDocument();
                            window.location.reload();
                        }
                        else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    }
                }
            });

        });
</script>
<?php
    echo $this->Liste->drawScript($documents, 'Document', $fields, $actions, $options);
} else {
    echo $this->Html->div('alert alert-warning',__d('document', 'Document.void'));
}
?>
