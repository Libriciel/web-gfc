<?php

/**
 *
 * Documents/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//echo $this->Html->css(array('administration'), null, array('inline' => false));
?>

<script type="text/javascript">
    function loadDocument() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/documents/getDocuments"; ?>",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
        $('#infos .content').empty();
    }

</script>
<div class="container">
    <div id="liste" class="row">
        <div  class="table-list">
            <h3><?php echo __d('document', 'Document.liste'); ?></h3>
            <div class="content">
            </div>
        </div>
        <div class="controls panel-footer">
        </div>
    </div>
</div>

<div id="infos" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                        <h4 class="panel-title"><?php echo __d('document', 'Document.infos'); ?></h4>
                    </div>
                    <div class="panel-body content">
                    </div>
                    <div class="panel-footer controls " role="group">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    gui.buttonbox({
        element: $('#liste .controls')
    });

    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter un document de type bordereau',
            class: "btn-info-webgfc ",
            title: "<?php echo __d('default', 'Button.add'); ?>",
            action: function () {
                $('#infos').modal('show');
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/Documents/add"; ?>",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });

            }
        }
    });

    gui.buttonbox({
        element: $('#infos .controls'),
        align: "center"
    });

    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    $('#infos').modal('hide');
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success ",
                action: function (data) {
                    var form = $('#DocumentAddForm');
                    if (form_validate(form)) {
                        form.submit();
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                        loadDocument();
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        ]
    });

    loadDocument();
</script>
