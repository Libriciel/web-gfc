<?php

/**
 *
 * Documents/get_preview.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php
if( !empty($viewPreviewFile)) {
?>
    <a href="/Documents/getFusionPreview/<?php echo $document; ?>/true" target="_blank" >
        <?php echo $this->Form->button("Plein écran");?>
    </a>
<?php
    echo $this->Element('flexPaper', array('height' => '600px', 'width' => '100%', 'filename' => '/files/previews/' . $viewPreviewFile));
}
?>


