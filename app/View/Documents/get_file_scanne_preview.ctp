<?php

/**
 *
 * Documents/get_fichier_scane.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Yuzhu JIN
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('/js/pdf.js');
echo $this->Html->script('/js/pdf_viewer.js');
?>
<div id="viewerContainer">
    <div id="viewer" class="pdfViewer" ></div>
</div>

<script>
    <?php if (isset($fileName)) { ?>
    if (!PDFJS.PDFViewer || !PDFJS.getDocument) {
        alert('Please build the library and components using\n' +
                'node make generic components');
    }
	<?php if( !Configure::read('Reverseproxy.pdfjs') ) :?>
		PDFJS.workerSrc = '<?php echo $this->Html->url('/js/pdf.worker.js',true); ?>';
		var DEFAULT_URL = '<?php echo $this->Html->url('/files/previews/'.$fileName,true); ?>';
	<?php else:?>
		PDFJS.workerSrc = '<?php echo Configure::read('WEBGFC_URL');?>/js/pdf.worker.js';
		var DEFAULT_URL = '<?php echo Configure::read('WEBGFC_URL');?>/files/previews/<?php echo $fileName;?>';
	<?php endif;?>
    var container = document.getElementById('viewerContainer');
    var pdfViewer = new PDFJS.PDFViewer({
        container: container,
    });
    container.addEventListener('pagesinit', function () {
        // We can use pdfViewer now, e.g. let's change default scale.
        pdfViewer.currentScaleValue = 'auto';
    });
    PDFJS.getDocument(DEFAULT_URL).then(function (pdfDocument) {
        // Document loaded, specifying document for the viewer
        pdfViewer.setDocument(pdfDocument);
    });
    <?php } ?>
</script>
