<?php

/**
 *
 * Authentifications/get_authentifications.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    //echo $this->Html->css(array('administration'), null, array('inline' => false));
?>
<?php
if (!empty($authentifications)) {
    $fields = array(
        'host'
    );
    $actions = array(
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/authentifications/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        "check" => array(
            "url" => Configure::read('BaseUrl') . '/authentifications/check',
            "updateElement" => "$('#liste')",
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/authentifications/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadConnecteurlist()"
        ),
    );
    $options = array();
    $panelBodyId ="";
    if(!empty($options['panel_id'])){
        $panelBodyId = "id='".$options['panel_id']."'";
    }
?>
<div class="panel">
    <div class="panel panel-default">
        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
        <?php
            echo $this->Liste->drawPanelHeading($authentifications,$options);
            $data = $this->Liste->drawTbody($authentifications, 'Authentification', $fields, $actions, $options);
        ?>
        </div>
        <div  class="bannette_panel panel-body" <?php echo $panelBodyId; ?>>
            <table id="table_authentification"
                   data-toggle="table"
                   data-search="true"
                   data-locale = "fr-CA"
                   data-height="400"
                   data-show-refresh="false"
                   data-show-toggle="false"
                   data-show-columns="true"
                   data-toolbar="#toolbar">
            </table>
        </div>
    </div>
</div>
<script>
    $('#table_authentification')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "host",
                        title: "<?php echo __d("authentification","Authentification.host"); ?>",
                        class: "host"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "check",
                        title: "Check",
                        class: "actions thCheck",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            });
</script>
<?php
    echo $this->LIste->drawScript($authentifications, 'Authentification', $fields, $actions, $options);
    echo $this->Js->writeBuffer();
} else {
    echo $this->Html->tag('div', __d('authentification', 'Authentification.void'), array('class' => 'alert alert-warning'));
}
?>
