<?php

/**
 *
 * Authentifications/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="container">
    <div id="backButton" style="margin-bottom: 10px;"></div>
    <div id="liste" class="">
        <div  class="table-list">

            <h3><?php echo __d('authentification', 'Authentification.infos'); ?></h3>
            <?php
                $true_false = array('true' => 'Oui', 'false' => 'Non');
                $formAuthent = array(
                        'name' => 'Authentification',
                        'label_w' => 'col-sm-5',
                        'input_w' => 'col-sm-6',
                        'form_url' =>array('controller' => 'authentifications', 'action' => 'makeconf'),
                        'form_type' => 'file',
                        'input' => array(
                            'Authentification.id' => array(
                                'inputType' => 'hidden',
                                'items'=>array(
                                    'type' => 'hidden'
                                )
                            )
                        )
                    );
                echo $this->Formulaire->createForm($formAuthent);
            ?>
            <div class="panel-body">
                <fieldset>
                    <legend>Activation de la signature électronique</legend>
                    <?php
                    $formAuthentRadio = array(
                        'name' => 'Authentification',
                        'label_w'=>'col-sm-5',
                        'input_w'=>'col-sm-6',
                        'form_url' => array('controller' => 'authentifications', 'action' => 'makeconf'),
                        'input' =>array(
                            'use_cas' => array(
                                'labelText' =>"",
                                'inputType' => 'radio',
                                'items'=>array(
                                    'legend' => false,
                                    'separator'=> '</div><div  class="radioUserGedLabel">',
                                    'before' => '<div class="radioUserGedLabel">',
                                    'after' => '</div>',
                                    'label' => true,
                                    'type'=>'radio',
                                    'options' => $true_false,
                                    'value' => ( isset( $this->request->data['Authentification']['use_cas'] ) && $this->request->data['Authentification']['use_cas'] ) ? 'true' : 'false'
                                )
                            )
                        )
                    );
                    echo $this->Formulaire->createForm($formAuthentRadio);
                    ?>
                </fieldset>
                <div id='cas_config_content'>
                    <legend>Authentification CAS</legend>
                    <?php
                        $formAuthentificationForm = array(
                                'name' => 'Authentification',
                                'label_w' => 'col-sm-5',
                                'input_w' => 'col-sm-6',
                                'form_url' =>array('controller' => 'authentifications', 'action' => 'makeconf'),
                                'input' => array(
                                    'host' => array(
                                        'labelText' =>'URL *',
                                        'labelPlaceholder' => "xxx.webgfc.fr",
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type' => 'text',
                                            'value' => Configure::read('PASTELL_PARAPHEUR_TYPE')
                                        )
                                    ),
                                    'port' => array(
                                        'labelText' =>'Port *',
                                        'labelPlaceholder' => "443",
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type' => 'text'
                                        )
                                    ),
                                    'contexte' => array(
                                        'labelText' =>'Contexte *',
                                        'labelPlaceholder' => "/cas",
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type' => 'text'
                                        )
                                    )
                                )
                            );
                        echo $this->Formulaire->createForm($formAuthentificationForm);
            ?>
                         <?php
                            $formAuthentCertificatAuthentification = array(
                                'name' => 'Authentification',
                                'label_w' => 'col-sm-5',
                                'input_w' => 'col-sm-6',
                                'form_url' =>array('controller' => 'authentifications', 'action' => 'makeconf' ),
                                'form_type' => 'file',
                                'input' => array(
                                    'autcert' => array(
                                        'labelText' =>'Certificat (pem) *',
                                        'inputType' => 'file',
                                        'items'=>array(
                                            'type'=>'file',
                                            'name' => 'myfile',
                                            'class' => 'fileField'
                                        )
                                    )
                                )
                            );
                            echo $this->Formulaire->createForm($formAuthentCertificatAuthentification);


                            $value = isset( $this->request->data['Authentification']['nom_cert'] ) ? $this->request->data['Authentification']['nom_cert'] : '';
                            if( !empty($value )) {
                                echo 'Certificat utilisé : '.$value;
                            }

                            $formAuthentificationOtherForm = array(
                                'name' => 'Authentification',
                                'label_w' => 'col-sm-5',
                                'input_w' => 'col-sm-6',
                                'form_url' =>array('controller' => 'authentifications', 'action' => 'makeconf'),
                                'input' => array(
                                    'proxy' => array(
                                        'labelText' =>'Proxy',
                                        'labelPlaceholder' =>'https://xxx.webgfc.fr',
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type' => 'text'
                                        )
                                    ),
                                    'logpath' => array(
                                        'labelText' =>'Emplacement du fichier de log *',
                                        'labelPlaceholder' => '/tmp/cas.log',
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type' => 'text'
                                        )
                                    )
                                )
                            );
                            echo $this->Formulaire->createForm($formAuthentificationOtherForm);
                            ?>
                </div>
            </div>
        </div>
        <div class="controls panel-footer " role="group">
            <?php
                echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-right: -10px; margin-left: 8%;display: inline-block;'));
                echo $this->Html->link(' <i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler', array('controller' => 'environnement', 'action' => 'index'), array('class' => 'btn btn-danger-webgfc btn-inverse', 'escape' => false, 'title' => 'Annuler'));
                echo $this->Html->tag('/div', null);
                echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 1%;'));;
                echo $this->Form->button(' <i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer', array('type' => 'submit', 'id' => 'boutonValider', 'class' => 'btn btn-success', 'escape' => false, 'title' => 'Modifier la configuration'));
                echo $this->Html->tag('/div', null);
                echo $this->Form->end();
            ?>
        </div>

    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {

        $('#AuthentificationMakeconfForm input[type=radio]').change(function () {
            $('#cas_config_content').toggle();
        });

        if ($('#AuthentificationUseCasTrue').is(":checked")) {
            $('#cas_config_content').show();
        }

        if ($('#AuthentificationUseCasFalse').is(":checked")) {
            $('#cas_config_content').hide();
        }


        $("input[name='data[Authentification][use_cas]']").change(function () {
            changeProtocol();
        });

    });

        if ($('#AuthentificationUseCasTrue').is(":checked")) {
            $('#cas_config_content').show();
        }

        if ($('#AuthentificationUseCasFalse').is(":checked")) {
            $('#cas_config_content').hide();
        }
    function changeProtocol() {
        if ($("input:checked").val() === 'true') {
            $('#cas_config_content').show();
            $("input[name='data[Authentification][host]']").attr("required", "true");
            $("input[name='data[Authentification][port]']").attr("required", "true");
            $("input[name='data[Authentification][contexte]']").attr("required", "true");
            $("input[name='data[Authentification][autcert]']").attr("required", "true");
            $("input[name='data[Authentification][logpath]']").attr("required", "true");
        }
        if ($("input:checked").val() === 'false') {
            $('#cas_config_content').hide();
            $("input[name='data[Authentification][host]']").removeAttr("required");
            $("input[name='data[Authentification][port]']").removeAttr("required");
            $("input[name='data[Authentification][contexte]']").removeAttr("required");
            $("input[name='data[Authentification][autcert]']").removeAttr("required");
            $("input[name='data[Authentification][logpath]']").removeAttr("required");
        }
    }

    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
                }
            }
        ]
    });
</script>
