<?php

/**
 *
 * Workflows/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div id="circuit_tabs" class="col-sm-12">
    <ul  class="nav nav-tabs titre" role="tablist">
        <li  class="active"><a  role="tab" data-toggle="tab" href="#circuit_tabs_1"><?php echo __d('circuit', 'Circuit.infos'); ?></a></li>
        <li><a   role="tab" data-toggle="tab" href="#circuit_tabs_3" id="set_schema"><?php echo __d('circuit', 'Circuit.schema'); ?></a></li>
    </ul>

    <div class="tab-content">
        <div id="circuit_tabs_1" class="tab-pane fade active in">
                    <?php
                    $formCircuit = array(
                        'name' => 'Circuit',
                        'label_w' => 'col-sm-4',
                        'input_w' => 'col-sm-7',
							'form_url' => array( 'controller' => 'workflows', 'action' => 'add'),
                        'input' => array(
                            'Circuit.nom' => array(
                                'labelText' =>__d('circuit', 'Circuit.nom'),
                                'inputType' => 'text',
                                'items'=>array(
                                    'required'=>true,
                                    'type'=>'text'
                                )
                            ),
                            'Circuit.description' => array(
                                'labelText' =>__d('circuit', 'Circuit.description'),
                                'inputType' => 'textarea',
                                'items'=>array(
                                    'type'=>'textarea'
                                )
                            ),
                            'Circuit.soustype' => array(
                                'labelText' =>__d('circuit', 'Circuit.soustype'),
                                'inputType' => 'selcet',
                                'items'=>array(
                                    'type'=>'select',
                                    'required'=>true,
                                    'multiple' => true,
                                    'options' => $listSoustypes
                                )
                            )
                        )
                    );
                    echo $this->Formulaire->createForm($formCircuit);
                    echo $this->Form->end();
                    ?>
            <div style="text-align: right"><a class="btn btn-info-webgfc" id="saveNewCircuitInfosBase"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer et créer votre schéma</a></div>
        </div>
        <div id="circuit_tabs_3" class="tab-pane fade" >
            <?php
                echo $this->Html->div('alert alert-warning',"Vous devez enregistrer les informations du circuit et y associer au moins un sous-type pour pouvoir modifier le schéma");
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#saveNewCircuitInfosBase').click(function () {
        var form = $('#CircuitAddForm');
        if ($('#CircuitSoustype').val() != null) {
            if (form_validate(form)) {
                gui.request({
                    url: '/workflows/add',
                    data: form.serialize(),
                    loaderElement: $('body'),
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    var json = getJsonResponse(data);
                    circuitId = json.CircuitId;
                    getJsonResponse(data);
                    gui.request({
                        url: "<?php echo "/workflows/setSchema/"; ?>" + circuitId,
                        updateElement: $('#circuit_tabs_3'),
                    });
                    $('#set_schema').click();

                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Oops...",
                    text: "Veuillez vérifier votre formulaire!",
                    type: "error",

                });
            }
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez saisir votre sous-type(s) associé(s)!",
                type: "error",

            });
        }

    });

    $(document).ready(function () {
        $('#CircuitSoustype').select2();
    });

</script>
