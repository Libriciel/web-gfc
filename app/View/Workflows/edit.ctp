<?php

/**
 *
 * Workflows/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div id="circuit_tabs" class="col-sm-12">

    <ul  class="nav nav-tabs titre" role="tablist">
        <li class="active"><a  role="tab" data-toggle="tab" href="#circuit_tabs_1"><?php echo __d('circuit', 'Circuit.infos'); ?></a></li>
        <?php if (empty($this->data['Soustype']) || (!empty($this->data['Soustype']) && count($this->data['Soustype']) == 0)) { ?>
        <li><a  role="tab" data-toggle="tab" href="#circuit_tabs_2"><?php echo __d('circuit', 'Circuit.schema'); ?></a></li>
        <?php } else { ?>
        <li><a  role="tab" data-toggle="tab" href="#circuit_tabs_3"><?php echo __d('circuit', 'Circuit.schema'); ?></a></li>
        <?php } ?>


    </ul>
    <div class="tab-content" >
        <div id="circuit_tabs_1" class="tab-pane fade active in" style="">
        <?php
            $formCircuit = array(
                            'name' => 'Circuit',
                            'label_w' => 'col-sm-4',
                            'input_w' => 'col-sm-7',
                            'form_url' => array( 'controller' => 'workflows', 'action' => 'edit', $this->request->data['Circuit']['id'] ),
                            'input' => array(
                                'Circuit.id' => array(
                                    'inputType' => 'hidden',
                                    'items'=>array(
                                        'type'=>'hidden',
                                        'value' => $this->request->data['Circuit']['id']
                                    )
                                ),
                                'Circuit.nom' => array(
                                    'labelText' =>__d('circuit', 'Circuit.nom'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'required'=>true,
                                        'type'=>'text'
                                    )
                                ),
                                'Circuit.description' => array(
                                    'labelText' =>__d('circuit', 'Circuit.description'),
                                    'inputType' => 'textarea',
                                    'items'=>array(
                                        'type'=>'textarea'
                                    )
                                ),
                                'Circuit.actif' => array(
                                    'labelText' =>'Actif',
                                    'inputType' => 'checkbox',
                                    'items'=>array(
                                        'type'=>'checkbox',
                                        'checked'=>$this->request->data['Circuit']['actif']

                                    )
                                )
                            )
                        );
            echo $this->Formulaire->createForm($formCircuit);
        ?>

            <legend>
          <?php echo __d('circuit', 'Soustype.associated'); ?>
            </legend>
          <?php
          $formCircuit = array(
                        'name' => 'Circuit',
                        'label_w' => 'col-sm-4',
                        'input_w' => 'col-sm-7',
                        'input' => array(
                            'Soustype.Soustype' => array(
                                'labelText' =>__d('circuit', 'Circuit.soustype'),
                                'inputType' => 'selcet',
                                'items'=>array(
                                    'type'=>'select',
                                    'multiple' => true,
                                    'options' => $listSoustypes
                                )
                            )
                        )
                    );
          echo $this->Formulaire->createForm($formCircuit);
          echo $this->Form->end();
          ?>
        </div>
        <div id="circuit_tabs_3"  class="tab-pane fade" style="max-height: 700px;">
        </div>
      <?php if (empty($this->data['Soustype']) || (!empty($this->data['Soustype']) && count($this->data['Soustype']) == 0)) { ?>
        <div id="circuit_tabs_2"  class="tab-pane fade">
        <?php
        echo $this->Html->div('alert alert-warning',"Vous devez associer au moins un sous-type au circuit pour pouvoir modifier le schéma");
        ?>
        </div>

    <?php } ?>
    </div>
</div>

<script type="text/javascript">
    function affichageTab() {
        if ($('#circuit_tabs_3').length > 0 && $('#circuit_tabs_3').is(':visible')) {
            gui.request({
                url: "<?php echo "/workflows/setSchema/" . $this->data['Circuit']['id']; ?>",
                updateElement: $('#circuit_tabs_3'),
            });
        }
    }
    $(document).ready(function () {
        $('#SoustypeSoustype').select2();
        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            affichageTab();
        })
    });
</script>
