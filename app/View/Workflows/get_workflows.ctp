<?php

/**
 *
 * Workflows/get_workflows.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php

if (!empty($workflows)) {
    $fields = array(
        'nom',
        'actif'
     );
    $actions = array(
        "edit" => array(
            "url" => '/workflows/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        "copieCircuit" => array(
            "url" => '/workflows/copieCircuit',
            "updateElement" => "$('#webgfc_content')",
            "refreshAction" => "loadCircuits();$('#infos .content').empty();"
            ),
        "delete" => array(
           "url" => '/workflows/delete',
           "updateElement" => "$('#webgfc_content')",
           "refreshAction" => "loadCircuits();$('#infos .content').empty();"
           )
        );
    $options = array('check_inactive' => true, 'inactive_value' => 0, 'inactive_field' => 'actif', 'inactive_model' => 'Circuit');
    $data = $this->Liste->drawTbody($workflows, 'Circuit', $fields, $actions, $options);

	$panelBodyId ="";
	if(!empty($options['panel_id'])){
		$panelBodyId = "id='".$options['panel_id']."'";
	}
    ?>


<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
	<div  class="bannette_panel panel-body" <?php echo $panelBodyId; ?>>
	<span class="bouton-search action-title pull-right"  role="group"  style="margin-top: 10px; margin-left: 5px; ">
		<a href="#" class="btn btn-info-webgfc btn-inverse" data-target="#zoneWorkflowGetWorkflowsForm" data-toggle="modal" title="Recherche avancée"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
		<a href="/workflows" class="btn btn-info-webgfc btn-inverse" id="cancelModalButton" data-target="#searchModal" title="Réinitialisation" style="margin-left: 15px;"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>
	</span>
		<table id="table_workflows"
			   data-toggle="table"
			   data-search="true"
			   data-locale = "fr-CA"
			   data-height="570"
			   data-show-refresh="false"
			   data-show-toggle="false"
			   data-show-columns="false"
			   data-sortable ="true"
			   data-pagination ="true"
			   data-page-size ="10"
			   data-toolbar="#toolbar">
		</table>
	</div>
<script>
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($workflows,$options); ?>');
    $('#table_workflows')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "nom",
                        title: "<?php echo __d("circuit","Circuit.nom"); ?>",
                        class: "nom"
                    },
                    {
                        field: "actif",
                        title: "<?php echo __d('circuit', 'Circuit.actif'); ?>",
                        class: "active_column"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "copieCircuit",
                        title: "Dupliquer",
                        class: "actions thCopie",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            })
            .on('page-change.bs.table', function (number, size) {
                addClassActive();
            });
    $(document).ready(function () {
        addClassActive();
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }
    function addClassActive() {
        $('#table_workflows .active_column').hide();
        $('#table_workflows .active_column').each(function () {
            if ($(this).html() == 'false' || $(this).html() == 0) {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }

</script>
<?php
    echo $this->Liste->drawScript($workflows, 'Circuit', $fields, $actions, $options);
    echo $this->Js->writeBuffer();
    ?>
	<?php

	$formWorkflow = array(
			'name' => 'Workflow',
			'label_w' => 'col-sm-4',
			'input_w' => 'col-sm-5',
			'input' => array(
				'Circuit.nom' => array(
					'labelText' =>__d('circuit', 'Circuit.nom'),
					'inputType' => 'text',
					'items'=>array(
						'type' => 'text',
						'required' => false
					)
				),
				'Circuit.actif' => array(
					'labelText' => 'Circuit actif ?',
					'inputType' => 'checkbox',
					'items'=>array(
						'type' => 'checkbox',
						'checked' => true
					)
				)
			)
	);
	?>
	<!--<div id="zoneWorkflowGetWorkflowsForm" class="zone-form">-->
	<div id="zoneWorkflowGetWorkflowsForm" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content zone-form">
				<div class="modal-header">
					<button data-dismiss="modal" class="close">×</button>
					<h4 class="modal-title">Recherche de circuits</h4>
				</div>
				<div class="modal-body">
					<?php
					echo $this->Formulaire->createForm($formWorkflow);
					?>
				</div>
				<div id="zoneWorkflowGetWorkflowsFormButton" class="modal-footer controls " role="group">

				</div>
				<?php echo $this->Form->end();?>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		$('#formulaireButton').button();
		$('#searchButton').button();
		$(document).ready(function () {
			$('#zoneWorkflowGetWorkflowsForm').hide();
		});


		$('#zoneWorkflowGetWorkflowsForm').keypress(function (e) {
			if (e.keyCode == 13) {
				$('#zoneWorkflowGetWorkflowsForm #searchButton').trigger('click');
				return false;
			}
			if (e.keyCode === 27) {
				$('#zoneWorkflowGetWorkflowsForm #cancelButton').trigger('click');
				return false;
			}
		});



		$('#searchButton').click(function () {
			gui.request({
				url: $("#WorkflowGetWorkflowsForm").attr('action'),
				data: $("#WorkflowGetWorkflowsForm").serialize(),
				loader: true,
				updateElement: $('#liste .content'),
				loaderMessage: gui.loaderMessage,
			}, function (data) {
				$('#liste .content').html(data);
			});
		});
		// Ajout de l'action de recherche via la touche Entrée
		$(function () {
			$('#WorkflowGetWorkflowsForm').keypress(function (event) {
				if (event.which == 13) {
					gui.request({
								url: $("#WorkflowGetWorkflowsForm").attr('action'),
								data: $("#WorkflowGetWorkflowsForm").serialize(),
								loader: true,
								updateElement: $('#liste .content'),
								loaderMessage: gui.loaderMessage,
							}, function (data) {
								$('#liste .content').html(data);
								$('div.modal-backdrop').hide();
							}
					);
					return false;
				}
			});
		});
		$('#WorkflowGetWorkflowsForm.folded').hide();
		$(document).ready(function () {
			$('.pagination li span').click(function () {
				$(this).find('a').click();
			});

			// Gestion des actions
			gui.addbutton({
				element: $('#zoneWorkflowGetWorkflowsFormButton'),
				button: {
					content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
					class: 'btn btn-info-webgfc btn-inverse cancelSearch ',
					action: function () {
						$(this).parents('.modal').modal('hide');
						loadCircuits();
					}

				}
			});

			gui.addbutton({
				element: $('#zoneWorkflowGetWorkflowsFormButton'),
				button: {
					content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
					class: 'btn btn-success searchBtn',
					action: function () {
						var form = $(this).parents('.modal').find('form');
						gui.request({
							url: form.attr('action'),
							data: form.serialize(),
							loader: true,
							updateElement: $('#browser .filetree.typestype'),
							loaderMessage: gui.loaderMessage
						}, function (data) {
							$('#browser').empty();
							$('.content').html(data);
						});
						$(this).parents('.modal').modal('hide');
						$('div.modal-backdrop').hide();

					}

				}
			});
		});
	</script>



<?php
} else {
    echo '<div class="alert alert-warning">'.__d('circuit', 'Circuit.void').'</div>';
}
?>
