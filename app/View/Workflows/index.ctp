<?php

/**
 *
 * Workflows/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    //echo $this->Html->css(array('administration'), null, array('inline' => false));
?>

<script type="text/javascript">

    function loadCircuits() {

        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/workflows/getWorkflows"; ?>",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

</script>

<div class="container">
    <div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
    <div id="liste" class="row">
        <div  class="table-list">
            <h3><?php echo __d('circuit', 'Circuit.liste'); ?></h3>
            <div class="content">
            </div>
        </div>
        <div class="controls panel-footer">
        </div>
    </div>
</div>

<div id="infos" class="modal fade" role="dialog">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                        <h4 class="panel-title"><?php echo __d('circuit', 'Circuit.infos'); ?></h4>
                    </div>
                    <div class="panel-body content">
                    </div>
                    <div class="panel-footer controls " role="group">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    gui.buttonbox({
        element: $('#liste .controls')
    });

    gui.buttonbox({
        element: $('#infos .controls')
    });

    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter un circuit',
            class: "btn-info-webgfc ",
            title: "<?php echo __d('default', 'Button.add'); ?>",
            action: function () {
                $('#infos').modal('show');
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/Workflows/add"; ?>",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }
        }
    });

    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    $('#infos').modal('hide');
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success ",
                action: function () {
                    var forms = $(this).parents('.modal').find('form');
                    $.each(forms, function (index, form) {
                        var url = form.action;
                        var id = form.id;
                        if (form_validate($('#' + id))) {
                            gui.request({
                                url: url,
                                data: $('#' + id).serialize()
                            }, function (data) {
                                getJsonResponse(data);
                                loadCircuits();
                            });
                            $(this).parents('.modal').modal('hide');
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    });
                }
            }
        ]
    });

    loadCircuits();

    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: "<i class='fa fa-download' aria-hidden='true'></i> Télécharger les circuits",
            title: "Télécharger les circuits",
            class: 'btn  btn-info-webgfc export_wkf',
            action: function () {
                window.location.href = "<?php echo Router::url( array( 'controller' => 'workflows', 'action' => 'export' ) + Hash::flatten( $this->request->data, '__' ) );?>";
            }
        }
    });
    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/admin#donnes"; ?>";
                }
            }
        ]
    });
</script>
