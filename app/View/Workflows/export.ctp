<?php

    $this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow(array('Informations sur les circuits'));

    $this->Csv->addRow(
            array(
                'Circuit',
                'Etape',
                'Composition'
            )
    );

    foreach ($results as $m => $circuit) {
        $nomCircuit = $circuit['Circuit']['nom'];
        $listeEtape = implode( ' ', $circuit['Circuit']['listeetapes'] );
        $listeCompo = implode( ' ', $circuit['Circuit']['listecompos'] );
        $rowCircuit[$m] = array_merge(
                array($nomCircuit),
                array($listeEtape),
                array($listeCompo)
        );
        $this->Csv->addRow(
            array_merge(
                $rowCircuit[$m]
            )
        );
    }
    Configure::write('debug', 0);
    echo $this->Csv->render("{$this->request->params['controller']}_{$this->request->params['action']}_" . date('Ymd-His') . '.csv');
?>