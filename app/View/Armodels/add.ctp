<?php

/**
 *
 * Armodels/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
if (empty($message)) {
?>
<script type="text/javascript">
    function hideAllSpecialFields() {
        $('div.input').each(function () {
            var div = $(this);
            if ($('.emailField', this).length > 0 || $('.smsField', this).length > 0 || $('.fileField', this).length > 0) {
                div.css('display', 'none');
            }
        });
    }

    function showSmsSpecialFields() {
        $('input[type=file]').val('');
        $('div.input').each(function () {
            var div = $(this);
            if ($('.smsField', this).length > 0) {
                div.css('display', 'block');
            }
        });
    }

    function showEmailSpecialFields() {
        $('input[type=file]').val('');
        $('div.input').each(function () {
            var div = $(this);
            if ($('.emailField', this).length > 0) {
                div.css('display', 'block');
            }
        });
    }

    function showFileSpecialFields() {
        $('div.input').each(function () {
            var div = $(this);
            if ($('.fileField', this).length > 0) {
                div.css('display', 'block');
            }
        });
    }
</script>
<div class="panel panel-default zone-form" id="armodel_add">
    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
        <h4>Ajouter un modèle</h4>
    </div>
    <div class="panel-body">

<?php
        $formArmodel = array(
        'name' => 'Armodel',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-5',
        'form_url' => array( 'controller' => 'armodels', 'action' => 'add' ),
        'form_target' => 'modelUploadFrame',
        'form_type' => 'file',
        'input' => array(
            'Armodel.soustype_id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value'=>$soustype_id
                )
            ),
            'Armodel.name' => array(
                'labelText' =>__d('armodel', 'Armodel.name'),
                'inputType' => 'text',
                'items'=>array(
                    'required'=>true,
                    'type'=>'text'
                )
            ),
            'Armodel.format' => array(
                'labelText' =>__d('armodel', 'Armodel.format'),
                'inputType' => 'select',
                'items'=>array(
                    'empty' => true,
                    'options' =>$options,
                    'type' => 'select',
                    'required'=>true
                )
            ),
            'Armodel.file' => array(
                'labelText' =>__d('armodel', 'Armodel.file'),
                'inputType' => 'file',
                'items'=>array(
					'required'=>true,
                    'type'=>'file',
                    'name' => 'myfile',
                    'class' => 'fileField'
                )
            )
        )
    );

    if (!in_array('sms', $warnings)) {
        $formArmodel['input']['Armodel.sms_content'] = array(
                'labelText' =>__d('armodel', 'Armodel.sms_content'),
                'inputType' => 'textarea',
                'items'=>array(
                    'type'=>'textarea',
                    'class'=>'smsField'
                )
        );
    }
    if (!in_array('email', $warnings)) {

        $formArmodel['input']['Armodel.email_title'] = array(
            'labelText' =>__d('armodel', 'Armodel.email_title'),
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text',
                'class'=>'emailField'
            )
        );
        $formArmodel['input']['Armodel.email_content'] = array(
            'labelText' =>__d('armodel', 'Armodel.email_content'),
            'inputType' => 'textarea',
            'items'=>array(
                'type' => 'textarea',
                'class'=>'emailField'
            )
        );
    }


    echo $this->Formulaire->createForm($formArmodel);
    echo $this->Form->end();

    ?>

    </div>
    <div class="panel-footer controls " role="group">

    </div>
</div>

<script type="text/javascript">
    $("#modelUploadFrame").remove();
    $('body').append('<iframe name="modelUploadFrame" id="modelUploadFrame" src="#" style="width:0;height:0;border:none;"></iframe>');

    $('#ArmodelFormat').select2();

    $('#ArmodelFormat').change(function () {
        hideAllSpecialFields();
        if ($('option:selected', this).val() == "sms") {
            showSmsSpecialFields();
        } else if ($('option:selected', this).val() == "email") {
            showEmailSpecialFields();
        } else if ($('option:selected', this).val() != "") {
            showFileSpecialFields();
        }
    });
    hideAllSpecialFields();

    gui.addbuttons({
        element: $('#armodel_add .panel-footer'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    $('#armodel_add').remove();
                    gui.request({
                        url: "<?php echo "/Soustypes/setArmodels/" . $soustype_id; ?>",
                        updateElement: $('#stype_tabs_2'),
                    });
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success ",
                action: function () {
                    var form = $('#ArmodelAddForm');
                    if (form_validate(form)) {
                        form.submit();
                        $('#armodel_add').remove();
                        gui.request({
                            url: "<?php echo "/Soustypes/setArmodels/" . $soustype_id; ?>",
                            updateElement: $('#stype_tabs_2'),
                        });
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        ]
    });
</script>
<?php } else { ?>
<script type="text/javascript">
<?php if (!$create) { ?>
<?php } ?>
    layer.msg("<div class='info'><?php echo $message; ?></div>", {offset: '70px'});
    window.top.window.stype_tabs_reload();
</script>
<?php } ?>

