<?php
/**
 *
 * Activites/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php

$formComment = array(
    'name' => 'Comment',
    'label_w'=>'col-sm-4',
    'input_w'=>'col-sm-5',
    'input' => array(
        'Comment.objet' => array(
            'labelText' =>__d('comment', 'Comment.objet'),
            'inputType' => 'textarea',
            'items'=>array(
				'required' => true,
                'type' => 'textarea'
            )
        )
    )
);
if ($canAddPrivateComment) {
    $formComment['input']['Comment.private'] = array(
        'labelText' =>__d('comment', 'Comment.private'),
        'inputType' => 'checkbox',
        'items'=>array(
            'type'=>'checkbox'
        )
    );
    $formComment['input']['Reader.Reader'] = array(
        'labelText' =>__d('comment', 'Comment.readers'),
        'inputType' => 'select',
        'items'=>array(
            'type'=>'select',
            'multiple' => true,
            'options' => $readers,
            'class' => 'commentCheckbox',
			'required' => true
        )
    );
}

echo $this->Formulaire->createForm($formComment);
echo $this->Form->end();
?>



<script type="text/javascript">
    $('#ReaderReader').select2();
    function switchPrivate() {
        if ($('#CommentPrivate').prop("checked")) {
            $('#ReaderReader').parents('.form-group').show();
        } else {
            $('#ReaderReader').parents('.form-group').hide();
        }
    }
    $('#CommentPrivate').change(function () {
        switchPrivate();
    });
    switchPrivate();
</script>
