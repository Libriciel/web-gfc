<?php

/**
 *
 * Directmairies/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('zone/Recherches/recherchesFunction.js', array('inline' => true));
?>
<?php if( $hasDm ) :?>
	<div class="container">
		<div class="row">
			<div class="table-list rechercheCondition" id="liste">
				<h3>Conditions</h3>
				<div class="content">
					<div class="panel-body form-horizontal">
						<div class="col-sm-12">
							<div class="col-sm-6">
								<legend><img border="0" src="/img/directmairie.svg" width="32px" height="32px" title="Direct-Mairie" > Direct-Mairie</legend>
								<?php

								$formDirect = array(
									'name' => 'Directmairie',
									'label_w' => 'col-sm-5',
									'input_w' => 'col-sm-6',
									'input' => array(
										'Directmairie.issueId' =>array(
											'labelText' => 'N° de demande',
											'inputType' => 'text',
											'items'=>array(
												'type' => 'text'
											)
										),
										'Directmairie.hideiscreated' =>array(
											'labelText' => 'Masquer les demandes déjà créées',
											'inputType' => 'checkbox',
											'items'=>array(
												'type' => 'checkbox',
												'checked' => true
											)
										)
									)
								);
								echo $this->Formulaire->createForm($formDirect);
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="controls panel-footer " role="group"></div>
			</div>
		</div>
		<div class="row">
			<div class="table-list rechercheCondition" id="infos">
				<h3>Résultats</h3>
				<div class="content"></div>
				<div class="controls panel-footer  " role="group"></div>
			</div>
		</div>
	</div>
<?php else :?>
<div class="container">
	<div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
	<?php echo $this->Html->tag('div', "Le connecteur avec Direct-Mairie n'est pas actif sur votre instance. <br /> Merci de contacter votre administrateur afin de l'activer.", array('class' => 'alert alert-danger')); ?>
</div>
<?php endif;?>
<script type="text/javascript">

	// Ajout de l'action de recherche via la touche Entrée
	$('#DirectmairieIndexForm').keypress(function (e) {
		if (e.keyCode == 13) {
			$('#liste .controls .searchBtn').trigger('click');
			return false;
		}
	});



	gui.buttonbox({
		element: $('#liste .controls'),
		align: 'center'
	});

	gui.addbuttons({
		element: $('#liste .controls'),
		buttons: [
			{
				content: '<i class="fa fa-eye-slash" aria-hidden="true"></i> Masquer',
				class: "hide-search-formulaire btn-inverse btn-info-webgfc ",
				title: "<?php echo __d('default', 'Button.hide'); ?>",
				action: function () {
					$('#liste').hide();
				}
			},
			{
				content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
				title: "<?php echo __d('default', 'Button.reset'); ?>",
				class: 'btn-info-webgfc btn-inverse',
				action: function () {
					resetJSelect('#DirectmairieIndexForm');
					var formCheckbox = $('.formSearch input');
					$('#DirectmairieIndexForm .select2-search-choice').remove();
					formCheckbox.each(function (index, item) {
						var idInput = item.id;
						if ($('#' + idInput + '').prop('checked')) {
							$('.' + idInput + '').show();
						} else {
							$('.' + idInput + '').hide();
						}

					});
					if ($('.searcheSaveTrue').is(':visible')) {
						$('#searchSavaBool').click();
					}
				}
			},
			{
				content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
				title: "<?php echo __d('default', 'Button.search'); ?>",
				class: 'btn-info-webgfc searchBtn',
				action: function () {
					var form = $('#DirectmairieIndexForm');
					var data = $('#DirectmairieIndexForm').serialize();
					/*$("#DirectmairieIndexForm").validate({
						'rules': {
							'data[][Directmairie][issueId]': 'required'
						}
					});
					if (form_validate($(form))) {
						gui.request({
							url: "/directmairies/ajaxformvalid/issueId",
							data: $('#DirectmairieIndexForm').serialize(),
							loader: true,
							loaderMessage: gui.loaderMessage,
						}, function (data) {
							processJsonFormCheck(data);
						});
					} else {
						swal({
							showCloseButton: true,
							title: "Oops...",
							text: "Veuillez vérifier votre formulaire!",
							type: "error",

						});
					}*/


					if (form_validate($(form))) {
						gui.request({
							url: "<?php echo Configure::read('BaseUrl') . "/directmairies/search"; ?>",
							data: data,
							updateElement: $('#infos .content'),
							loader: true,
							loaderMessage: gui.loaderMessage
						});
						$('#liste').hide();
						$('#infos').show();
					}
				}
			}
		]
	});

	gui.disablebutton({
		element: $('#liste .controls'),
		button: "<?php echo __d('default', 'Button.hide'); ?>"
	});

	$('#infos').hide();
	$('.hide-search-formulaire').hide();


	gui.addbuttons({
		element: $('#backButton'),
		buttons: [
			{
				content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
				class: "btn-info-webgfc",
				action: function () {
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/user"; ?>";
				}
			}
		]
	});
</script>
