<?php

/**
 *
 * Directmairies/search.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel sCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php
echo $this->Html->script('appAttendable.js', array('inline' => true));
?>
<?php

if (!empty($datas->id) ) {
	?>
	<div class="zone col-sm-12">
		<div  id="issuedata" class="panel-body fieldsetView">
			<dl>
				<h4><img border="0" src="/img/directmairie.svg" width="32px" height="32px" title="Direct-Mairie" > Remontée</h4>
				<div class="card">
					<table class="table col-sm-8">
						<tbody>
						<tr>
							<th class="libelle">N° de remontée :</th>
							<td class="col-sm-8"><?php echo $this->request->data['Directmairie']['issueId'];?></td>
						</tr>
						</tbody>
					</table>
				</div>
				<h4>Informations</h4>
				<div class="card">
					<table class="table col-sm-8">
						<tbody>
						<tr>
							<th class="libelle">Staut de la demande :</th>
							<td class="col-sm-8"><?php echo $translationOfStatus[$datas->status];?></td>
						</tr>
						<tr>
							<th class="libelle">Catégorie de problèmes :</th>
							<td class="col-sm-8"><?php echo $datas->category->label;?></td>
						</tr>
						<tr>
							<th class="libelle">Description du problème:</th>
							<td class="col-sm-8"><?php echo $datas->description;?></td>
						</tr>
						<?php if( isset( $datas->pictures ) && !empty( $datas->pictures[0]->id)) :?>

							<tr>
								<th class="libelle">Image(s) créée(s) le : </th>
								<td class="col-sm-8"><?php echo $this->Html2->ukToFrenchDateWithHourAndSlashes($datas->pictures[0]->creationInstant);?></td>
							</tr>

							<?php foreach($datas->pictures as $p => $picture)  :?>
								<tr>
									<th class="libelle"></th>
									<td class="col-sm-8"><img src="app/webroot/files/previews/image<?php echo $picture->id;?>" height="64px" width="64px" /></td>
								</tr>
							<?php endforeach;?>
						<?php endif;?>

						<tr>
							<th class="libelle">Coordonnées: </th>
							<td class="col-sm-8">Latitude:    <?php echo $datas->coordinates->latitude;?></td>
						</tr>
						<tr>
							<th class="libelle"></th>
							<td class="col-sm-8">Longitude: <?php echo $datas->coordinates->longitude;?></td>
						</tr>
						<tr>
							<th class="libelle">Date de création :</th>
							<td class="col-sm-8"><?php echo $this->Html2->ukToFrenchDateWithHourAndSlashes($datas->creationInstant) ;?></td>
						</tr>
						<tr>
							<th class="libelle">Compte associé :</th>
							<td class="col-sm-8"><?php echo $datas->assignedGovernment->name;?></td>
						</tr>
						<tr>
							<th class="libelle">SIREN :</th>
							<td class="col-sm-8"><?php echo $datas->assignedGovernment->code;?></td>
						</tr>

						</tbody>
					</table>
				</div>

			</dl>
		</div>
	</div>

	<?php if (!$issueAlreadyGenerated): ?>
		<div id="results">
			<div class="buttonDossier panel-footer" id="<?php echo $datas->id ?>"
				 role="group"></div>
		</div>
	<?php else : ?>
		<div id="results">
			<div class="buttonDossierAlreadyGenerated panel-footer"
				 reference="<?php echo $referenceGenerated ?>"
				 id="<?php echo $courrierGeneratedId ?>" role="group"></div>
		</div>
	<?php endif; ?>
	<?php
}
else if ( !empty($datas[0]->id) ) {
	foreach( $datas as $data) :?>
	<?php if( $this->request->data['Directmairie']['hideiscreated'] == 1  && !isset($data->alreadyCreated) ) {?>
		<div class="zone col-sm-12">
			<div  id="issuedata" class="panel-body fieldsetView">
				<dl>
					<h4><img border="0" src="/img/directmairie.svg" width="32px" height="32px" title="Direct-Mairie" > Remontée</h4>
					<div class="card">
						<table class="table col-sm-8">
							<tbody>
							<tr>
								<th class="libelle">N° de remontée :</th>
								<td class="col-sm-8"><?php echo $data->id;?></td>
							</tr>
							</tbody>
						</table>
					</div>
					<h4>Informations</h4>
					<div class="card">
						<table class="table col-sm-8">
							<tbody>
							<tr>
								<th class="libelle">Staut de la demande :</th>
								<td class="col-sm-8"><?php echo $translationOfStatus[$data->status];?></td>
							</tr>
							<tr>
								<th class="libelle">Catégorie de problèmes :</th>
								<td class="col-sm-8"><?php echo $data->category->label;?></td>
							</tr>
							<tr>
								<th class="libelle">Description du problème:</th>
								<td class="col-sm-8"><?php echo $data->description;?></td>
							</tr>
							<?php if( isset( $data->pictures ) && !empty( $data->pictures[0]->id)) :?>

								<tr>
									<th class="libelle">Image(s) créée(s) le : </th>
									<td class="col-sm-8"><?php echo $this->Html2->ukToFrenchDateWithHourAndSlashes($data->pictures[0]->creationInstant);?></td>
								</tr>

								<?php foreach($data->pictures as $p => $picture)  :?>
									<tr>
										<th class="libelle"></th>
										<td class="col-sm-8"><img src="app/webroot/files/previews/image<?php echo $picture->id;?>" height="64px" width="64px" /></td>
									</tr>
								<?php endforeach;?>
							<?php endif;?>

							<tr>
								<th class="libelle">Coordonnées: </th>
								<td class="col-sm-8">Latitude:    <?php echo $data->coordinates->latitude;?></td>
							</tr>
							<tr>
								<th class="libelle"></th>
								<td class="col-sm-8">Longitude: <?php echo $data->coordinates->longitude;?></td>
							</tr>
							<tr>
								<th class="libelle">Date de création :</th>
								<td class="col-sm-8"><?php echo $this->Html2->ukToFrenchDateWithHourAndSlashes($data->creationInstant) ;?></td>
							</tr>
							<tr>
								<th class="libelle">Compte associé :</th>
								<td class="col-sm-8"><?php echo $data->assignedGovernment->name;?></td>
							</tr>
							<tr>
								<th class="libelle">SIREN :</th>
								<td class="col-sm-8"><?php echo $data->assignedGovernment->code;?></td>
							</tr>

							</tbody>
						</table>
					</div>

				</dl>
			</div>
		</div>
		<div id="results">
			<div class="buttonDossier panel-footer" id="<?php echo $data->id ?>" name="<?php echo $data->id ?>"
				 role="group"></div>
		</div>
	<?php } ?>



		<?php if( !$this->request->data['Directmairie']['hideiscreated'] && isset( $data->alreadyCreated ) ) {?>
			<div class="zone col-sm-12">
				<div  id="issuedata" class="panel-body fieldsetView">
					<dl>
						<h4><img border="0" src="/img/directmairie.svg" width="32px" height="32px" title="Direct-Mairie" > Remontée</h4>
						<div class="card">
							<table class="table col-sm-8">
								<tbody>
								<tr>
									<th class="libelle">N° de remontée :</th>
									<td class="col-sm-8"><?php echo $data->id;?></td>
								</tr>
								</tbody>
							</table>
						</div>
						<h4>Informations</h4>
						<div class="card">
							<table class="table col-sm-8">
								<tbody>
								<tr>
									<th class="libelle">Statut de la demande :</th>
									<td class="col-sm-8"><?php echo $translationOfStatus[$data->status];?></td>
								</tr>
								<tr>
									<th class="libelle">Catégorie de problèmes :</th>
									<td class="col-sm-8"><?php echo $data->category->label;?></td>
								</tr>
								<tr>
									<th class="libelle">Description du problème:</th>
									<td class="col-sm-8"><?php echo $data->description;?></td>
								</tr>
								<?php if( isset( $data->pictures ) && !empty( $data->pictures[0]->id)) :?>

									<tr>
										<th class="libelle">Image(s) créée(s) le : </th>
										<td class="col-sm-8"><?php echo $this->Html2->ukToFrenchDateWithHourAndSlashes($data->pictures[0]->creationInstant);?></td>
									</tr>

									<?php foreach($data->pictures as $p => $picture)  :?>
										<tr>
											<th class="libelle"></th>
											<td class="col-sm-8"><img src="app/webroot/files/previews/image<?php echo $picture->id;?>" height="64px" width="64px" /></td>
										</tr>
									<?php endforeach;?>
								<?php endif;?>

								<tr>
									<th class="libelle">Coordonnées: </th>
									<td class="col-sm-8">Latitude:    <?php echo $data->coordinates->latitude;?></td>
								</tr>
								<tr>
									<th class="libelle"></th>
									<td class="col-sm-8">Longitude: <?php echo $data->coordinates->longitude;?></td>
								</tr>
								<tr>
									<th class="libelle">Date de création :</th>
									<td class="col-sm-8"><?php echo $this->Html2->ukToFrenchDateWithHourAndSlashes($data->creationInstant) ;?></td>
								</tr>
								<tr>
									<th class="libelle">Compte associé :</th>
									<td class="col-sm-8"><?php echo $data->assignedGovernment->name;?></td>
								</tr>
								<tr>
									<th class="libelle">SIREN :</th>
									<td class="col-sm-8"><?php echo $data->assignedGovernment->code;?></td>
								</tr>

								</tbody>
							</table>
						</div>

					</dl>
				</div>
			</div>
			<div id="results">
				<div class="buttonDossierAlreadyGenerated panel-footer"
					 reference="<?php echo $referenceGenerated[$data->id] ?>"
					 id="<?php echo $courrierGeneratedId[$data->id] ?>" role="group"></div>
			</div>
		<?php } ?>


	<?php endforeach;?>
<?php }
else if (!empty($errorMessage) && !isset($errorMessage->body)) {
	echo $this->Html->tag('div', 'Impossible de se connecter à Direct-Mairie. <br />Le connecteur retourne : <b>'.$errorMessage.'</b> <br />Veuillez contacter votre administrateur.', array('class' => 'alert alert-danger'));
}else {
	echo $this->Html->tag('div', 'Aucune remontée, portant ce numéro n\'a été trouvé sur Direct-Mairie', array('class' => 'alert alert-warning'));
}
?>
<script type="text/javascript">

	if ($('#infos .controls .btn').length == 0) {
		gui.buttonbox({
			element: $('#infos .controls'),
			align: "center"
		});

		// permet de masquer le formulaire de recherche, lorsqu'on le réouvre
		$('#liste h3').css('cursor', 'pointer');
		$('#liste h3').click(function () {
			$('#liste').hide();
		});

		// Bouton pour vider le formulaire de recherche
		gui.addbutton({
			element: $('#infos .controls'),
			button: {
				content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
				class: 'btn-info-webgfc btn-inverse',
				title: "<?php echo __d('default', 'Button.reset'); ?>",
				action: function () {
					if (!$(this).hasClass('ui-state-disabled')) {
						$('#infos .content').empty();
						$('#infos .controls').empty();
					}
					$('#liste').show();
					$('#infos').hide();
					$('.hide-search-formulaire').remove();
				}
			}
		});

		// Bouton pour réouvrir le formulaire de recherche
		gui.addbutton({
			element: $('#infos .controls'),
			button: {
				content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
				title: "<?php echo __d('default', 'Button.search'); ?>",
				class: 'btn-info-webgfc',
				action: function () {
					$('#liste').show();
					$('.hide-search-formulaire').show();
				}
			}
		});




	}

	// bouton pour créer le flux depuis le dossier
	gui.addbutton({
		element: $('#results .buttonDossier'),
		button: {
			content: '<i class="fa fa-plus" aria-hidden="true" > Créer un flux à partir de cette remontée</i>',
			title: ' Créer un flux depuis ce dossier',
			class: 'btn btn-info-webgfc btn-success',
			action: function () {
				<?php if( isset( $this->request->data['Directmairie']['issueId'] ) && !empty($this->request->data['Directmairie']['issueId'])) :?>
					var issueId = '<?php echo $this->request->data['Directmairie']['issueId'];?>';
				<?php else:?>
					var issueId = $(this).parents().attr('id');
				<?php endif;?>

				var url = '/directmairies/createCourrier/' + issueId;
				gui.request({
					url: url,
					updateElement: $('#liste .content'),
					loader: true,
					loaderMessage: gui.loaderMessage
				}, function (data) {
					getJsonResponse(data);

					var courrierId = getJsonResponse(data)['newCourrierId'];
					window.open('/courriers/historiqueGlobal/' + courrierId, '_blank');
					window.location.reload();
				});
			}
		}
	});

	// bouton pour consulter le flux créé à partir du dossier
	gui.addbutton({
		element: $('#results .buttonDossierAlreadyGenerated'),
		button: {
			content: '<i class="fa fa-check" aria-hidden="true" > Flux déjà généré à partir de cette remontée</i>',
			title: ' Flux déjà généré à partir de ce dossier',
			class: 'btn btn-success',
			data_toggle: 'tab',
			data_target: '_blank',
			action: function () {
				var courrierId =  $(this).parent().attr('id');
				window.open('/courriers/historiqueGlobal/' + courrierId, '_blank');
			}
		}
	});

</script>
<?php  echo $this->Js->writeBuffer(); ?>
