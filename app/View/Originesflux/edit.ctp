<?php

/**
 *
 * Collectivites/scanemail_add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formOrigineflux = array(
    'name' => 'Origineflux',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Origineflux.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Origineflux.name' => array(
            'labelText' =>__d('origineflux', 'Origineflux.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        )
	)
);
if( Configure::read('Display.RatioOrigineflux' ) ) {
	$formOrigineflux['input']['Origineflux.ratio'] =array(
		'labelText' => 'Ratio pour le délai de retard',
		'inputType' => 'select',
		'items'=>array(
			'empty' => true,
			'options' => $ratio,
			'type' => 'select'
		)
	);
}
$formOrigineflux['input']['Origineflux.active'] =array(
	'labelText' => __d('origineflux', 'Origineflux.active'),
		'inputType' => 'checkbox',
		'items'=>array(
			'type'=>'checkbox',
			'checked'=>$this->request->data['Origineflux']['active']
		)
);
echo $this->Formulaire->createForm($formOrigineflux);
echo $this->Form->end();
?>

<script type="text/javascript">

    $('#OriginefluxEditForm').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

	<?php if( Configure::read('Display.RatioOrigineflux' ) ):?>
		$('#OriginefluxRatio').select2({allowClear: true, placeholder: "Sélectionner un ratio à appliquer"});
	<?php endif;?>

</script>
