<?php

/**
 * Référentiel des voies FANTOIR
 *
 *
 * Referentielsfantoir controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller
 */
class ReferentielsfantoirController extends AppController {

	/**
	 * Controller uses
	 *
	 * @var array
	 * @access public
	 */
	public $uses = array('Referentielfantoir');

	/**
	 * Gestion des référentiels de voie
	 *
	 * @logical-group Carnet d'adresse
	 * @user-profile Admin
	 *
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
                    '<a href="environnement/index/0/admin">'.__d('menu', 'Administration', true).'</a>',
                    __d('menu', 'gestionReferentiels', true)
                ));
	}

		/**
	 * Récupération de la liste des circuits de traitements (ajax)
	 *
	 * @logical-group Circuits de traitements
	 * @logical-group CakeFlow
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function getReferentiel() {
        $referentielsfantoirdir_tmp = $this->Referentielfantoir->find(
			"all",
			array(
                'contain' => array(
                    'Referentielfantoirdir' => array(
                        'order' => array( 'Referentielfantoirdir.name')
                    ),
                    'Referentielfantoircom' => array(
                        'order' => array( 'Referentielfantoircom.name')
                    ),
                    'Referentielfantoirvoie' => array(
                        'order' => array( 'Referentielfantoirvoie.name')
                    )
                ),
				'order' => 'Referentielfantoir.name'
			)
		);
		$referentielsfantoirdir = array();
		foreach ($referentielsfantoirdir_tmp as $i => $item) {
			$item['right_edit'] = true;
			$item['right_delete'] = true;
			$referentielsfantoirdir[] = $item;
		}

		$this->set('referentielsfantoir', $referentielsfantoirdir);
	}

	/**
	 * Importation de référentiels à partir d'un fichier FANTOIR
	 *
	 * @logical-group Referentielfantoir
	 * @user-profile Admin
	 *
	 *	Modifier le php.ini /etc/php5/apache2/php.ini avec :
	 *		- post_max_size = 20M (au lieu de 8M)
	 *		- upload_max_filesize = 20M (au lieu de 2M)
	 * @access public
	 * @return void
	 */
	public function import() {
		if (!empty($this->request->data)) {
			if (!empty($this->request->data['Import']['file'])) {
				$tmpName = $this->request->data['Import']['file']['tmp_name'];
				$filename = TMP . $this->request->data['Import']['file']['name'];
				if (move_uploaded_file($tmpName, $filename)) {
					if ($this->Referentielfantoir->importReferentiel($filename, $this->request->data['Import']['departement_id'])) {
						$this->Session->setFlash(__d('default', 'save.ok'), 'growl', array('type' => 'default'));
					} else {
						$this->Session->setFlash(__d('default', 'save.error'), 'growl', array('type' => 'error'));
					}
				}

				if (is_file($filename)) {
					unlink($filename);
				}
				$this->redirect(array('controller' => 'referentielsfantoir', 'action' => 'index'));
			}
		} else {
			$this->set('referentielsfantoir', $this->Referentielfantoir->find(
                    'list',
                    array(
                        'conditions' => array('Referentielfantoir.active' => 1)
                    )
                )
            );
		}

// 		$regions = Configure::read( 'Referentiel.region' );
		$this->set('departements', $this->Referentielfantoir->listeDepartements());
	}

}
