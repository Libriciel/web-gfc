<?php

/**
 * web-GFC : Application de gestion des FLux citoyens
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The AGPL v3 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     https://choosealicense.com/licenses/agpl-3.0/ AGPL v3 License
 */

App::uses('Component', 'Controller');
App::uses('ComponentCollection', 'Controller');
App::uses('SessionComponent', 'Controller/Component');
App::uses('AppTools', 'Lib');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('HttpSocket', 'Network/Http');
/**
 *[PastellComponent description]
 * @package app.Controller.Component
 * @version 5.1.1
 * @since 4.3.0
 */

class PastellComponent extends Component
{


    /**
     * [public description]
     * @var [type]
     */
    private $host;
    /**
     * [public description]
     * @var [type]
     */
    private $login;
    /**
     * [public description]
     * @var [type]
     */
    private $pwd;
    /**
     * [public description]
     * @var [type]
     */
    private $parapheur_type;
    /**
     * [public description]
     * @var [type]
     */
    private $pastell_type;
    /**
     * [public description]
     * @var [type]
     */
    private $Session;
    /**
     * [public description]
     * @var [type]
     */
    private $config;

    /**
     * [__construct description]
     */
    public function __construct()
    {
//        $collection = new ComponentCollection();
//        $this->Session = new SessionComponent($collection);
//        $this->host = Configure::read('PASTELL_HOST');
//        $this->login = Configure::read('PASTELL_LOGIN');
//        $this->pwd = Configure::read('PASTELL_PWD');
//        $this->pastell_type = Configure::read('PASTELL_TYPE');
//        $this->parapheur_type = Configure::read('PASTELL_PARAPHEUR_TYPE');
//        //clés de champs
//        $pastell_config = Configure::read('Pastell');
//        if (isset($pastell_config[$this->pastell_type])) {
//            $this->config = $pastell_config[$this->pastell_type];
//        }

    }


    /**
     * Retourne la liste des circuits du PASTELL
     * La liste est enregistrée en Session
     * pour économiser du traffic réseau entre WD et Pastell lors des appels suivants
     *
     * @param int $id_e identifiant de la collectivité
     * @return array
     */
    public function getCircuits($id_e, $type)
    {
		CakeSession::write('user.Pastell.circuits', null);
        if (empty( CakeSession::read('user.Pastell.circuits') ) ) {
            $tmp_id_d = $this->createDocument($id_e, $type);
            $circuits = $this->getInfosField($id_e, $tmp_id_d, 'iparapheur_sous_type');
            if (!empty($circuits)) {
                CakeSession::write('user.Pastell.circuits', $circuits);
            }
            $this->action($id_e, $tmp_id_d, 'supression');
        } else {
            $circuits = CakeSession::read('user.Pastell.circuits');
        }
        return $circuits;
    }

    /**
     * @param string $page script php
     * @param array $data
     * @param bool $file_transfert attente d'un fichier en retour ?
     * @return bool|mixed|string retour du webservice
     */
    public function execute($page, $data = [], $file_transfert = false)
    {
		$conn = CakeSession::read('Auth.User.connName');
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$this->Connecteur ->setDataSource($conn);
        $pastell = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%PASTELL%'
                ),
                'contain' => false
            )
        );

        if (!empty($pastell) && $pastell['Connecteur']['use_pastell']) {
            $this->host = $pastell['Connecteur']['host'];
            $this->login = $pastell['Connecteur']['login'];
            $this->pwd = $pastell['Connecteur']['pwd'];
            $this->pastell_type = $pastell['Connecteur']['type'];
            $this->parapheur_type = @$pastell['Connecteur']['pastell_parapheur_type'];

            $pastell_config = Configure::read('Pastell');
            if (isset($pastell_config[$this->pastell_type])) {
                $this->config = $pastell_config[$this->pastell_type];
            }
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, $this->login . ":" . $this->pwd);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $api = $this->host . "/api/v2/$page";


        curl_setopt($curl, CURLOPT_URL, $api);
        //Hack Pastell pour champ multiple en api V1
        if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (is_array($value)) {
					$data[$key] = json_encode($value);
				}
			}
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			// pour gérer le cas de la modification
			if( !isset( $data['type']) ) {
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
			}
        }
//        if ($file_transfert) {
//            $folder = new Folder(AppTools::newTmpDir(WEBROOT_DIR . 'files' . DS . 'Pastell'), true, 0777);
//            $file = new File($folder->path . DS . 'WG_PASTELL_DOC', true, 0777);
//            $fp = fopen($file->path, 'w');
//            curl_setopt($curl, CURLOPT_FILE, $fp);
//        }

        $response = curl_exec($curl);

        if ($response === false) {
            CakeLog::error(curl_error($curl), 'Pastell');
            throw new Exception(curl_error($curl));
        }
        curl_close($curl);

//        if ($file_transfert) {
//            fclose($fp);
//            $content = $file->read();
//            $folder->delete();
//        } else {
            $content = null;
//        }

        return $this->parseRetour($page, $data, $response, $content);
    }

    /**
     * Fonction de parse des réponses de pastell
     * @param string $page script ws
     * @param array $data paramètres
     * @param array $retourWS reponse ws
     * @return bool false si en erreur
     */
    private function parseRetour($page, $data, $retourWS, $content)
    {
        //CakeLog::info(var_export($retourWS, true), 'pastell');
        $result = json_decode($retourWS, true);

        // Test si le retour est un fichier ou un message json (erreur)
        //CakeLog::info(print_r(['Request' => $page, 'Data' => $data], true), 'pastell');

        if (!empty($result['message'])) {
            return $result['message'];
            //$this->log('Message : ' . $result['message'], 'pastell');
        }
        if (!empty($result['error-message'])) {
            CakeLog::error('Error : ' . $result['error-message'], 'pastell');
            return false;
        }

        if (!empty($result) && is_array($result)) {
            CakeLog::info('Récupération du json Ok', 'pastell');
            return $result;
        }
        if (!empty($content)) {
            CakeLog::info('Récupération du json Ok', 'pastell');
            return $content;
        }
        if ($page==='action.php' && empty($result)) {
            CakeLog::info('Action effectuée', 'pastell');
            return true;
        }
        if (@simplexml_load_string($retourWS)!==false  && empty($result)) {
            CakeLog::info('Classification récupérée', 'pastell');
            return $retourWS;
        }

        CakeLog::error(print_r(array(
          'Request' => $page,
          'Data' => $data ,
          'reponse' => var_export($retourWS, true),
          'reponseData' => $content), true), 'pastell');

        throw new Exception('Aucune réponse du serveur distant', 500);
    }

    /**
     * Converti un tableau d'arguments en une chaine pour envoi en méthode $_GET (args par url)
     *
     * @param array $params tableau de paramètres
     * @return string chaine de paramètres
     */
    private function paramsArray2String($params = [])
    {
        $target = '';
        foreach ($params as $opt => $val) {
            if (!empty($val)) {
                $target .= $opt . '=' . urlencode($val) . '&';
            }
        }
        return substr($target, 0, strlen($target) - 1);
    }

    /**
     * Permet d'obtenir la version de la plateforme. Pastell assure une compatibilité ascendante entre les différents numéro de révision.
     * @return array(
     *      version => Numéro de version commerciale,
     *      revision => Numéro de révision du dépôt de source officiel de Pastell  (https://adullact.net/scm/viewvc.php/?root=pastell),
     *      version_complete => Version affiché sur la l'interface web de la plateforme
     * )
     */
//    public function getVersion()
//    {
//        return $this->execute('version');
//    }

    /**
     * Types de document supportés par la plateforme
     *
     * Liste l'ensemble des flux (types de documents) disponible sur Pastell
     * comme par exemple, les actes, les mails sécurisées, les flux citoyen, etc...
     *
     * @return array(type* => array(id => nom))
     * type : Groupe de type de document (exemple : Flux généraux)
     * id : Identifiant du type
     * nom : Nom à afficher pour l'utilisateur (exemple : Actes, Message du centre de gestion)
     */
    public function getDocumentTypes()
    {
        $documents = [];
        $flux = $this->execute("flux");
        foreach ($flux as $name => $type) {
            $documents = $type['nom'];
        }
        return $documents;
    }

    /**
     * Information sur un type de document
     *
     * Liste l'ensemble des champs d'un type document ainsi que les informations sur chaque champs
     * (type de champs, valeur par défaut, script de choix, ...)
     *
     * @param string $type Type de document (retourné par document-type.php). Exemple : actes, mailsec, ...
     * @return array propriete * : Couple clé/valeur de la propriété
     */
    public function getDocumentTypeInfos($type)
    {
        return $this->execute("flux/$type");
    }

    /**
     * Actions possible sur un type de document
     *
     * Ramène la liste des statuts/actions possibles sur ce type de document ainsi que des infos relatives à ce type de document
     *
     * @param string $type Type de document (retourné par document-type.php)
     * @return array propriete * : Couple clé/valeur de la propriété
     */
    public function getDocumentTypeActions($type)
    {
        return $this->execute("flux/$type/action");
    }

//    /**
//     * Listes des entités
//     *
//     * Liste l'ensemble des entités sur lesquelles l'utilisateur a des droits. Liste également les entités filles.
//     *
//     * @param bool $details si on veut afficher toutes les infos des entités
//     * si $details :
//     * @return array( array*(
//     *  id_e => Identifiant numérique de l'entité
//     *  denomination => Libellé de l'entité (ex : Saint-Amand-les-Eaux)
//     *  siren => Numéro SIREN de l'entité (si c'est une collectivité ou un centre de gestion)
//     *  centre_de_gestion => Identifiant numérique (id_e) du CDG de la collectivité
//     *  entite_mere => Identifiant numérique (id_e) de l'entité mère de l'entité (par exemple pour un service)
//     * ))
//     * sinon
//     * @return array( id_e => denomination )
//     */
//    public function listEntities($details = false)
//    {
//        $entities = $this->execute('entite');
//
//        if ($details) {
//            return $entities;
//        } else {
//            $collectivites = [];
//            foreach ($entities as $entity) {
//                $collectivites[$entity['id_e']] = $entity['denomination'];
//            }
//            return $collectivites;
//        }
//    }

    /**
     * Listes de documents d'une collectivité
     *
     * Liste l'ensemble des documents d'une entité Liste également les entités filles.
     *
     * @param int $id_e Identifiant de l'entité (retourné par list-entite)
     * @param string $type Type de document (retourné par document-type)
     * @param integer $offset (facultatif, 0 par défaut) Numéro de la première ligne à retourner
     * @param integer $limit (facultatif, 100 par défaut) Nombre maximum de lignes à retourner
     * @return array (
     *                  id_e => Identifiant numérique de l'entité (identique à l'entrée),
     *                  id_d => Identifiant unique du document,
     *                  role => Rôle de l'entité sur le document (exemple : éditeur),
     *                  last-action => Dernière action effectuée sur le document,
     *                  last_action_date => Date de la dernière action,
     *                  type => Type de document (identique à l'entrée),
     *                  creation => Date de création du document,
     *                  modification => Date de dernière modification du document,
     *                  entite * => Liste des identifiant (id_e) des autres entités qui ont des droits sur ce document
     * )
     */
    public function listDocuments($id_e, $type, $offset = 0, $limit = 100)
    {
        return $this->execute("entite/$id_e/document/$type/$offset/$limit");
    }

    /**
     * Recherche multi-critère dans la liste des documents
     *
     * @param array $options
     * @return array
     * Format de sortie :
     * [] => {
     *  id_e =>             Identifiant numérique de l'entité
     *  id_d =>             Identifiant unique du document
     *  role =>             Rôle de l'entité sur le document (exemple : éditeur)
     *  last-action =>      Dernière action effectuée sur le document
     *  last_action_date => Date de la dernière action
     *  type =>             Type de document (identique à l'entrée)
     *  creation =>         Date de création du document
     *  modification =>     Date de dernière modification du document
     *  entite * =>         Liste des identifiant (id_e) des autres entités qui ont des droits sur ce document.
     * }
     */
    public function rechercheDocument($options = [])
    {
		$id_e = $options['id_e'];
        $default_options = [
            'id_e' => null, //Identifiant de l'entité (retourné par list-entite)
            'type' => null, //Type de document (retourné par document-type.php)
            'offset' => null, //numéro de la première ligne à retourner
            'lastetat' => null, //Dernier état du document
            'last_state_begin' => null, //date du passage au dernier état du document le plus ancien(date iso)
            'last_state_end' => null, //date du passage au dernier état du document le plus récent(date iso)
            'etatTransit' => null, //le document doit être passé dans cet état
            'state_begin' => null, //date d'entrée la plus ancienne de l'état etatTransit
            'state_end' => null, //date d'entrée la plus récente de l'état etatTransit
            'tri' => null, //critère de tri parmi last_action_date, title et entite
            'search' => null, //l'objet du document doit contenir la chaine indiquée
        ];
        $options = array_merge($default_options, $options);
//        return $this->execute('list-document.php?' . $this->paramsArray2String($options));
        return $this->execute("entite/$id_e/document/" . $this->paramsArray2String($options));
    }

    /**
     * Détail sur un document
     *
     * Récupère l'ensemble des informations sur un document Liste également les entités filles.
     *
     * @param int $id_e Identifiant de l'entité (retourné par list-entite)
     * @param int $id_d Identifiant unique du document (retourné par list-document)
     * @return array
     * Format de sortie :
     * [] => {
     *  info => Reprend les informations disponible sur list-document.php
     *  data => Données issue du formulaire (voir document-type-info.php pour savoir ce qu'il est possible de récupérer)
     *  action_possible * => Liste des actions possible (exemple : modification, envoi-tdt, ...)
     * }
     */
//    public function detailDocument($id_e, $id_d)
//    {
//		$this->Connecteur = ClassRegistry::init('Connecteur');
//		$pastell = $this->Connecteur->find(
//			'first',
//			array(
//				'conditions' => array(
//					'Connecteur.name ILIKE' => '%PASTELL%'
//				),
//				'contain' => false
//			)
//		);
//
//		if (!empty($pastell) && $pastell['Connecteur']['use_pastell']) {
//			$this->host = $pastell['Connecteur']['host'];
//			$this->login = $pastell['Connecteur']['login'];
//			$this->pwd = $pastell['Connecteur']['pwd'];
//		}
//
//		$httpSocket = new HttpSocket();
//		$request = array(
//			'method' => 'GET',
//			'auth' => array(
//				'method' => 'Basic',
//				'user' => $this->login,
//				'pass' => $this->pwd
//			)
//		);
//		return $httpSocket->get($this->host  . '/api/v2/entite/' . $id_e . '/document/'. $id_d, [], $request );
//
//    }

    /**
     * Détail sur plusieurs documents
     *
     * Récupère l'ensemble des informations sur un document Liste également les entités filles.
     *
     * @param int $id_e Identifiant de l'entité (retourné par list-entite)
     * @param array $id_d Tableau d'identifiants uniques de documents (retourné par list-document)
     * @return array
     * Format de sortie :
     * [] => {
     *  info => Reprend les informations disponible sur list-document.php
     *  data => Données issue du formulaire (voir document-type-info.php pour savoir ce qu'il est possible de récupérer)
     *  action_possible * => Liste des actions possible (exemple : modification, envoi-tdt, ...)
     * }
     */
//    public function detailDocuments($id_e, $id_d)
//    {
//        $args = "id_e=$id_e";
//        foreach ($id_d as $d) {
//            $args .= "&id_d=$d";
//        }
//        return $this->execute('detail-several-document.php?' . $args);
//    }

    /**
     * @param int $id_e identifiant de la collectivité
     * @param string $type type de flux (pastell)
     * @return integer id_d Identifiant unique du document crée.
     * @throws Exception Si erreur lors de la création
     */
//    public function createDocument($id_e, $type = null)
//    {
//        if ($type == null) {
//            $type = $this->pastell_type;
//        }
////        $infos = $this->execute('entite/' . $id_e . '/document', ['type'=> $type] );
//
//		$this->Connecteur = ClassRegistry::init('Connecteur');
//		$pastell = $this->Connecteur->find(
//			'first',
//			array(
//				'conditions' => array(
//					'Connecteur.name ILIKE' => '%PASTELL%'
//				),
//				'contain' => false
//			)
//		);
//
//		if (!empty($pastell) && $pastell['Connecteur']['use_pastell']) {
//			$this->host = $pastell['Connecteur']['host'];
//			$this->login = $pastell['Connecteur']['login'];
//			$this->pwd = $pastell['Connecteur']['pwd'];
//		}
//
//		$httpSocket = new HttpSocket();
//		$request = array(
//			'method' => 'POST',
//			'auth' => array(
//				'method' => 'Basic',
//				'user' => $this->login,
//				'pass' => $this->pwd
//			)
//		);
//
//		$infos = $httpSocket->post($this->host  . '/api/v2/entite/' . $id_e . '/document', ['type'=> $type], $request );
//
//        if (empty($infos)) {
//            throw new Exception("Echec de connexion avec le serveur Pastell");
//        }
//		return json_decode($infos->body)->id_d;
//
//        /*if (array_key_exists('id_d', $infos)) {
//            return ($infos['id_d']);
//        } elseif (array_key_exists('error-message', $infos)) {
//            throw new Exception($infos['error-message']);
//        } else {
//            throw new Exception("Une erreur inconnue est survenue");
//        }*/
//    }

    /**
     * FIXME : 'error-message' => 'Type action-possible introuvable'
     *
     * Récupération des choix possibles pour un champs spécial du document : external-data
     *
     * Récupère les valeurs possible d'un champs.
     * En effet, certaine valeur sont « externe » a Pastell : classification Actes, classification CDG, etc..
     * Ce script permet de récupérer l'ensemble de ces valeurs.
     * Ce script est utilisable sur tous les champs qui dispose d'une propriétés « controler »
     *
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @param string $field le nom d'un champ du document
     * @return array
     * valeur_possible * => Information supplémentaire sur la valeur possible (éventuellement sous forme de tableau associatif)
     */
    public function getInfosField($id_e, $id_d, $field)
    {

		$this->Connecteur = ClassRegistry::init('Connecteur');
		$pastell = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%PASTELL%'
				),
				'contain' => false
			)
		);

		if (!empty($pastell) && $pastell['Connecteur']['use_pastell']) {
			$this->host = $pastell['Connecteur']['host'];
			$this->login = $pastell['Connecteur']['login'];
			$this->pwd = $pastell['Connecteur']['pwd'];
		}

		$httpSocket = new HttpSocket();
		$request = array(
			'method' => 'GET',
			'auth' => array(
				'method' => 'Basic',
				'user' => $this->login,
				'pass' => $this->pwd
			)
		);

		return $httpSocket->get($this->host . '/api/v2/entite/'.$id_e.'/document/'.$id_d . '/externalData/' . $field, [], $request);
//		return $this->execute("external-data.php?id_e=$id_e&id_d=$id_d&field=$field");
    }

    /**
     * Modification d'un document
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @param array $delib
     * @param string $document (Pdf)
     * @param array $annexes
     * @return bool|string
     * @since 4.2
     * @version 5.0
     * result : ok - si l'enregistrement s'est bien déroulé
     * formulaire_ok : 1 si le formulaire est valide, 0 sinon
     * message : Message complémentaire
     *
     * A noter que pour connaître la liste et les intitulés exacts des champs modifiables,
     * il convient d'utiliser la fonction document-type-info.php, en lui précisant le type concerné.
     */
//    public function modifDocument($id_e, $id_d, $data, $document = [], $pjs = [])
//    {
//		$this->Connecteur = ClassRegistry::init('Connecteur');
//		$pastell = $this->Connecteur->find(
//			'first',
//			array(
//				'conditions' => array(
//					'Connecteur.name ILIKE' => '%PASTELL%'
//				),
//				'contain' => false
//			)
//		);
//
//		if (!empty($pastell) && $pastell['Connecteur']['use_pastell']) {
//			$this->host = $pastell['Connecteur']['host'];
//			$this->login = $pastell['Connecteur']['login'];
//			$this->pwd = $pastell['Connecteur']['pwd'];
//			$this->pastell_type = $pastell['Connecteur']['type'];
//			$this->parapheur_type = $pastell['Connecteur']['pastell_parapheur_type'];
//
//			$pastell_config = Configure::read('Pastell');
//			if (isset($pastell_config[$this->pastell_type])) {
//				$this->config = $pastell_config[$this->pastell_type];
//			}
//		}
//		$success = true;
//		$dataForPastell = array(
//            'id_e' => $id_e,
//            'id_d' => $id_d,
//            'to' => $data['Sendmail']['email'],
//            'cc' => !empty($data['Sendmail']['ccmail']) ? $data['Sendmail']['ccmail'] : null,
//            'bcc' => !empty($data['Sendmail']['ccimail']) ? $data['Sendmail']['ccimail']: null,
////            'password' => !empty($data['Deliberation']['typologiepiece_code']) ? $data['Deliberation']['typologiepiece_code']: null,
////            'password2' => !empty($data['annexe_typologiepiece_code']) ? $data['annexe_typologiepiece_code']: null,
//            'objet' => $data['Sendmail']['sujet'],
//            'message' => $data['Sendmail']['message']
//		);
//        //filtre les eléments vides
//		$dataForPastell = array_filter($dataForPastell);
//
//		$httpSocket = new HttpSocket();
//		$request = array(
//			'method' => 'PATCH',
//			'auth' => array(
//				'method' => 'Basic',
//				'user' => $this->login,
//				'pass' => $this->pwd
//			)
//		);
//
//		$httpSocket->patch($this->host . '/api/v2/entite/'.$id_e.'/document/'.$id_d, $dataForPastell, $request);
//
//		if( isset( $document ) && !empty( $document ) ) {
//			$options = array(
//				'field_name' => 'document_attache',
//				'file_name' => $document['Document']['name'],
//				'file_num' => 0,
//				'file_content' => file_get_contents($document['Document']['path'])
//			);
//			$this->sendFile($id_e, $id_d, $options);
//		}
//		/*if (!empty($pjs)) {
//			foreach ($pjs as $key => $pj) {
//				$pjoptions = array(
//					'field_name' => 'document_attache',
//					'file_name' => $pj['Document']['name'],
//					'file_num' => $key + 1,
//					'file_content' => file_get_contents($pj['Document']['path'])
//				);
//				$pjoptions = array_merge( $options, $pjoptions );
//				$this->sendFile($id_e, $id_d, $pjoptions);
//			}
//		}*/
//
//		return $success;
////$this->log($mail);
////        if ($this->execute('/entite/'.$id_e.'/document/'.$id_d, $mail) == false) {
////            $success = false;
////        }
//
//
////        $folder->delete();
//
////        return $success;
//    }

    /**
     * Envoi d'un fichier pour modifier un document
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @param array $options
     * [] =>
     *  string field_name le nom du champs
     *  string file_name le nom du fichier
     *  integer file_number le numéro du fichier (pour les fichier multiple)
     *  string file_content le contenu du fichier
     *
     * @return array
     * [] =>
     *  result : ok - si l'enregistrement s'est bien déroulé
     *  formulaire_ok : 1 si le formulaire est valide, 0 sinon
     *  message : Message complémentaire
     */
//    public function sendFile($id_e, $id_d, $options = [])
//    {
//		$this->Connecteur = ClassRegistry::init('Connecteur');
//		$pastell = $this->Connecteur->find(
//			'first',
//			array(
//				'conditions' => array(
//					'Connecteur.name ILIKE' => '%PASTELL%'
//				),
//				'contain' => false
//			)
//		);
//
//		if (!empty($pastell) && $pastell['Connecteur']['use_pastell']) {
//			$this->host = $pastell['Connecteur']['host'];
//			$this->login = $pastell['Connecteur']['login'];
//			$this->pwd = $pastell['Connecteur']['pwd'];
//		}
//        $default_options = [
//            'id_e' => $id_e,
//            'id_d' => $id_d,
//            'field_name' => null, //le nom du champs
//            'file_name' => null, //le nom du fichier
//            'file_num' => null, //le numéro du fichier (pour les fichier multiple)
//            'file_content' => null, //le contenu du fichier
//        ];
//        $options = array_merge($default_options, $options);
//		$httpSocket = new HttpSocket();
//		$request = array(
//			'method' => 'POST',
//			'auth' => array(
//				'method' => 'Basic',
//				'user' => $this->login,
//				'pass' => $this->pwd
//			)
//		);
//        return $httpSocket->post($this->host . '/api/v2/entite/'.$id_e.'/document/'.$id_d.'/file/'.$options['field_name'], $this->paramsArray2String($options), $request);
//    }

    /**
     * Réception d'un fichier
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @param string $field_name le nom du champs
     * @param integer $file_number le numéro du fichier (pour les fichiers multiple)
     * @return array
     * [] =>
     *  file_name : le nom du fichier
     *  file_content : le contenu du fichier
     */
    public function receiveFile($id_e, $id_d, $field_name = null, $file_number = null)
    {
        $options = [
            'id_e' => $id_e,
            'id_d' => $id_d,
            'field_name' => $field_name, //le nom du champs
            'file_number' => $file_number, //le numéro du fichier (pour les fichier multiple)
        ];
        return $this->execute('receive-file.php?' . $this->paramsArray2String($options));
    }

    /**
     * Execute une action sur un document
     *
     * @param int $id_e Identifiant de l'entité (retourné par list-entite)
     * @param int $id_d Identifiant unique du document (retourné par list-document)
     * @param string $action Nom de l'action (retourné par detail-document, champs action-possible)
     * @param array $options
     * @return array
     * [] =>
     *  result : 1 si l'action a été correctement exécute. Sinon, une erreur est envoyé
     *  message : Message complémentaire en cas de réussite
     *
     */
    public function action($id_e, $id_d, $action, $options = [])
    {
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$pastell = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%PASTELL%'
				),
				'contain' => false
			)
		);

		if (!empty($pastell) && $pastell['Connecteur']['use_pastell']) {
			$this->host = $pastell['Connecteur']['host'];
			$this->login = $pastell['Connecteur']['login'];
			$this->pwd = $pastell['Connecteur']['pwd'];
		}

		$httpSocket = new HttpSocket();
		$request = array(
			'method' => 'POST',
			'auth' => array(
				'method' => 'Basic',
				'user' => $this->login,
				'pass' => $this->pwd
			)
		);

		return $httpSocket->post($this->host . '/api/v2/entite/'.$id_e.'/document/'.$id_d . '/action/' . $action, [], $request);
    }

    /**
     * Supprime un dossier dans Pastell
     * Attention: l'exécution de cette commande peut necessiter beaucoup de temps
     * @param int $id_e identifiant d'entité (collectivité)
     * @param int $id_d identifiant de dossier
     * @return bool|array
     */
    public function delete($id_e, $id_d)
    {
        return $this->action($id_e, $id_d, $this->config['action']['supression']);
    }

    /**
     * Récupère le contenu d'un fichier
     * @param int $id_e Identifiant de l'entité (retourné par list-entite)
     * @param int $id_d Identifiant unique du document (retourné par list-document)
     * @param string $field le nom d'un champ du document
     * @param int $num le numéro du fichier, s'il s'agit d'un champ fichier multiple
     * @return string fichier c'est le fichier qui est renvoyé directement
     */
    public function getFile($id_e, $id_d, $field, $num = 0)
    {
        $data = [
            'id_e' => $id_e,
            'id_d' => $id_d,
            'field' => $field,
        ];

        $page = 'recuperation-fichier.php';
        if ($num) {
            $data['num'] = $num;
        }
        return $this->execute($page, $data, true);
    }

    /**
     * TODO ajouter les autres paramètres d'entrée
     * Récupérer le journal
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @return bool|array résultat
     */
    public function journal($id_e, $id_d)
    {
        $acte = [
            'id_e' => $id_e,
            'id_d' => $id_d
        ];
        return $this->execute('journal.php', $acte);
    }

    /**
     * Déclare à Pastell que l'acte doit être envoyé en signature
     * Attention: le document doit être inséré dans un circuit avant !
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @param string $classification
     * @param bool $value
     * @return bool|array résultat
     */
    public function envoiSignature($id_e, $id_d, $classification = null, $value = true)
    {
        $data = [
            'id_e' => $id_e,
            'id_d' => $id_d,
            $this->config['field']['envoi_signature'] => $value,
        ];
        if (!empty($classification)) {
            $data[$this->config['field']['classification']] = $classification;
        }
        return $this->execute('modif-document.php', $data);
    }

    /**
     * Mise à jour des informations de télétransmission à Pastell
     *
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @param array $acte
     *
     * @return bool|array résultat
     *
     * @version 5.1.1
     */
    public function updateTdt($entiteId, $documentId, $acte)
    {
        $fields = $this->config['field'];
        $data = [
            'id_e' => $entiteId,
            'id_d' => $documentId,
            $fields['envoi_tdt'] => true,
            $fields['classification'] => $acte['Deliberation']['num_pref'],
            $fields['document_papier'] => !empty($acte['Deliberation']['tdt_document_papier']) ? $acte['Deliberation']['tdt_document_papier']: '',
            $fields['type_acte'] => $acte['Deliberation']['typologiepiece_code'],
            $fields['type_pj'] => !empty($acte['annexe_typologiepiece_code']) ? $acte['annexe_typologiepiece_code']: '',
        ];
        //filtre les eléments vide
        $data = array_filter($data);

        return $this->execute('modif-document.php', $data);
    }

    /**
     * Déclare à Pastell que l'acte doit être envoyé au sae
     * Attention: le document doit être inséré dans un circuit avant !
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @param bool $value
     * @return bool|array résultat
     */
    public function envoiSae($id_e, $id_d, $value = true)
    {
        $data = [
            'id_e' => $id_e,
            'id_d' => $id_d,
            $this->config['field']['envoi_sae'] => $value
        ];
        return $this->execute('modif-document.php', $data);
    }

    /**
     * Déclare à Pastell que l'acte doit être envoyé à la GED
     * Attention: le document doit être inséré dans un circuit avant !
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @param bool $value
     * @return bool|array résultat
     */
    public function envoiGed($id_e, $id_d, $value = true)
    {
        $data = [
            'id_e' => $id_e,
            'id_d' => $id_d,
            $this->config['field']['envoi_ged'] => $value
        ];
        return $this->execute('modif-document.php', $data);
    }

    /**
     * Envoi à Pastell l'information sur le circuit du parapheur à emprunter
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @param string $sous_type
     * @return bool|array résultat
     */
    public function selectCircuit($id_e, $id_d, $sous_type)
    {
        $infos = [
            'id_e' => $id_e,
            'id_d' => $id_d,
            $this->config['field']['iparapheur_type'] => $this->parapheur_type,
            $this->config['field']['iparapheur_sous_type'] => utf8_decode($sous_type),
            $this->config['field']['envoi_signature'] => true
        ];
        return $this->execute('modif-document.php', $infos);
    }

    /**
     * Joint ses annexes à un document dans Pastell
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @param string $annex chemin de l'annexe à attacher
     * @return bool|array résultat
     */
    public function sendAnnex($id_e, $id_d, $path, $name, $mimetype)
    {
        /*$acte = [
            'id_e' => $id_e,
            'id_d' => $id_d,
            $this->config['field']['autre_document_attache'] => curl_file_create($path, $mimetype, $name)
        ];
        return $this->execute('modif-document.php', $acte);*/

		$this->Connecteur = ClassRegistry::init('Connecteur');
		$pastell = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%PASTELL%'
				),
				'contain' => false
			)
		);

		if (!empty($pastell) && $pastell['Connecteur']['use_pastell']) {
			$this->host = $pastell['Connecteur']['host'];
			$this->login = $pastell['Connecteur']['login'];
			$this->pwd = $pastell['Connecteur']['pwd'];
		}

		$httpSocket = new HttpSocket();
		$request = array(
			'method' => 'POST',
			'auth' => array(
				'method' => 'Basic',
				'user' => $this->login,
				'pass' => $this->pwd
			)
		);

		$data = [
			'id_e' => $id_e,
			'id_d' => $id_d,
			'autre_document_attache' => curl_file_create($path, $mimetype, $name)
		];

		return $httpSocket->post($this->host . '/api/v2/entite/'.$id_e.'/document/'.$id_d, $data, $request);
    }

    /**
     * Récupère le tableau de classifications
     * @param int $id_e identifiant de la collectivité
     * @param int $id_d identifiant du dossier pastell
     * @return array
     *
     * @version 5.1.1
     */
    public function getClassification($idEntite)
    {
        $idCe = $this->getIdCeByEntiteAndConnecteur($idEntite, 'TdT');
        $classification = $this->execute('entite/' . $idEntite . '/connecteur/' . $idCe . '/file/classification_file');

        return $classification;
    }

    /**
     * [getIdCeByEntiteAndConnecteur description]
     * @param  [type] $id_e       [description]
     * @param  [type] $connecteur [description]
     * @return [type]             [description]
     *
     * @version 5.1.1
     */
    public function getIdCeByEntiteAndConnecteur($idEntite, $connecteurType)
    {
        $flux = Configure::read('PASTELL_TYPE');
        $connecteur = $this->execute('entite/' . $idEntite . '/flux?flux=' . $flux . '&type=' . $connecteurType);

        // Il ne doit y avoir qu'un seul connecteur
        if (count($connecteur) === 1) {
            return $connecteur[0]['id_ce'];
        }

        return null;
    }

	/**
	 * Listes des types du i-Parapheur
	 *
	 * Liste l'ensemble des entités sur lesquelles l'utilisateur a des droits. Liste également les entités filles.
	 *
	 * @param bool $details si on veut afficher toutes les infos des entités
	 * si $details :
	 * @return array( array*(
	 *  id_e => Identifiant numérique de l'entité
	 *  denomination => Libellé de l'entité (ex : Saint-Amand-les-Eaux)
	 *  siren => Numéro SIREN de l'entité (si c'est une collectivité ou un centre de gestion)
	 *  centre_de_gestion => Identifiant numérique (id_e) du CDG de la collectivité
	 *  entite_mere => Identifiant numérique (id_e) de l'entité mère de l'entité (par exemple pour un service)
	 * ))
	 * sinon
	 * @return array( id_e => denomination )
	 */
	public function listTypesIParapheur($idEntite)
	{
		$idCe = $this->getIdCeByEntiteAndConnecteur($idEntite, 'signature');

		$typesIP = $this->execute('entite/' . $idEntite . '/connecteur/' . $idCe . '/recupFile/iparapheur_sous_type');

		return $typesIP;

	}
}

