<?php
/**
 * Class LsMessageComponent
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT <arnaud.auzolat@libriciel.coop>
 * @editor Libriciel SCOP
 *
 * @created 19 sept 2018
 * @copyright  Développé par Libriciel SCOP
 * @link http://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
use Buzz\Client\Curl;
use Libriciel\LsMessageWrapper\LsMessageWrapper;
use Libriciel\LsMessageWrapper\Sms;
use Nyholm\Psr7\Factory\Psr17Factory;

/**
 *
 * LsMessageComponent class.
 *
 * component pour le wrapper d'api de lsmessage
 *
 * composer require libriciel/ls-message-wrapper
 * composer require nyholm/psr7
 * composer require kriswallsmith/buzz
 *
 */
class LsMessageComponent extends Component {

	/**
	 * Controller
	 *
	 * @var type
	 */
	public $controller;

	/**
	 * @var LsMessageWrapper
	 */
	public $messageWrapper;

	/**
	 * Component initialization
	 *
	 * @access public
	 * @param type $controller
	 * @return void
	 */
	public function initialize(Controller $controller) {
		$this->controller = $controller;
		$psr17Factory = new Psr17Factory();
		$client = new Curl($psr17Factory);
		$this->messageWrapper = new LsMessageWrapper($client, $psr17Factory);
		$this->setParams();
	}

	private function setParams()
	{
		$this->controller->loadModel('Connecteur');
		$connector = $this->controller->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%LsMessage%',
					'Connecteur.use_sms' => true
				),
				'contain' => false
			)
		);
		if(!empty($connector)){
			$this->messageWrapper->setApiKey($connector['Connecteur']['sms_apikey']);
			$this->messageWrapper->setUrl($connector['Connecteur']['host']);
		}
	}


	public function setApiKey(string $apiKey)
	{
		$this->messageWrapper->setApiKey($apiKey);
	}

	public function setUrl(string $url)
	{
		$this->messageWrapper->setUrl($url);
	}

	/**
	 * @return array
	 * @throws \Libriciel\LsMessageWrapper\LsMessageException
	 */
	public function info(){
		return $this->messageWrapper->info();
	}

	/**
	 * @param Sms $sms
	 * @return array
	 * @throws \Libriciel\LsMessageWrapper\LsMessageException
	 */
	public function sendOne(Sms $sms) {
		return $this->messageWrapper->sendOne($sms);
	}

	/**
	 * @param Sms[] $messages
	 * @return array
	 * @throws \Libriciel\LsMessageWrapper\LsMessageException
	 */
	public function sendMultiple(array $messages) {
		return $this->messageWrapper->sendMultiple($messages);
	}

}
