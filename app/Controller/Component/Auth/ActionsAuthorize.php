<?php

App::uses('BaseAuthorize', 'Controller/Component/Auth');

/**
 * Actions Authorize class
 *
 * @see AuthComponent::$authenticate
 * @see AclComponent::check()
 * An authorization adapter for AuthComponent.  Provides the ability to authorize using the AclComponent,
 * If AclComponent is not already loaded it will be loaded using the Controller's ComponentCollection.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component.Auth
 */
class ActionsAuthorize extends BaseAuthorize {

    /**
     * Authorize a user using the AclComponent.
     *
     * @param array $user The user to authorize
     * @param CakeRequest $request The request needing authorization.
     * @return boolean
     */
    public function authorize($user, CakeRequest $request) {
        $Acl = $this->_Collection->load('Acl');
        $user = array($this->settings['userModel'] => $user);
        $userModel = ClassRegistry::init('User');

        if ($user['User']['Env']['main'] == 'superadmin') {
            return true;
        }
        $mainDesktop = $userModel->field('desktop_id', array('User.id' => $user['User']['id']));
        $desktops = $userModel->Desktop->DesktopsUser->find('all', array('conditions' => array('DesktopsUser.user_id' => $user['User']['id'])));
        $checks = array();
        $checks[$mainDesktop] = $Acl->check(array('model' => 'Desktop', 'foreign_key' => $mainDesktop), $this->action($request));
        foreach ($desktops as $desktop) {
            $checks[$desktop['DesktopsUser']['desktop_id']] = $Acl->check(array('model' => 'Desktop', 'foreign_key' => $desktop['DesktopsUser']['desktop_id']), $this->action($request));
        }
        return in_array(true, $checks, true);
    }

}
