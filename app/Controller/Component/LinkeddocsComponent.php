<?php

/**
 *
 * Linkeddocs component class.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
class LinkeddocsComponent extends Component {

	/**
	 * Controller
	 *
	 * @access public
	 * @var Controller
	 */
	public $controller;

	/**
	 ** Component initialization
	 *
	 * @access public
	 * @param Controller $controller
	 * @return void
	 */
	public function initialize(Controller $controller) {
		$this->controller = $controller;
	}

	/**
	 * Suppression des fichiers de pré-visualisation générés
	 *
	 * @access public
	 * @return void
	 */
	public function purge() {
		App::uses('Folder', 'Utility');
		$folder = new Folder(APP . WEBROOT_DIR . DS . 'files/previews');
		$files = $folder->find($this->controller->Session->read('Auth.User.username') . '.*');
		foreach ($files as $file) {
			unlink($folder->path . DS . $file);
		}
	}


	/**
	 * Suppression des images récupérées depuis Direct-Mairie
	 *
	 * @access public
	 * @return void
	 */
	public function purgeImage() {
		App::uses('Folder', 'Utility');
		$folder = new Folder(APP . WEBROOT_DIR . DS . 'files/previews');
		$files = $folder->find('image*');
		foreach ($files as $file) {
			unlink($folder->path . DS . $file);
		}
	}
}

?>
