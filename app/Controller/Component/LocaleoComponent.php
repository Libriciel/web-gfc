<?php
/**
 * Class LocaleoComponent
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT <arnaud.auzolat@libriciel.coop>
 * @editor Libriciel SCOP
 *
 * @created 19 sept 2018
 * @copyright  Développé par Libriciel SCOP
 * @link http://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
class LocaleoComponent extends Component {

    public $responseMessage;
    public $apiKey;
    public $appName;
    public $responseMessageStr;
    public $wsto;
    public $wsdl;
    public $clientcert;
    public $passphrase;
    public $userpwd;
    public $boundary;

    function LocaleoComponent() {
        $this->apiKey = "7a1182abe571c90584c2541ecs3a0f54";
        $this->appName = "Webgfc";
    }

    /**
     * Focntion reprenant les paramètres du connecteur GRC
     */
    private function _dataFixed() {
        $this->Connecteur = ClassRegistry::init('Connecteur');
        $hasGrcActif = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%GRC%',
                    'Connecteur.use_grc' => true
                ),
                'contain' => false
            )
        );
        if( !empty( $hasGrcActif )) {
            $dataFixed = array(
                'cityId' => $hasGrcActif['Connecteur']['grc_cityid'],
                'canalId' => $hasGrcActif['Connecteur']['grc_canalid'],
                'servId' => $hasGrcActif['Connecteur']['grc_servid'],
                'sqrtId' => $hasGrcActif['Connecteur']['grc_sqrtid'],
                'qrtId' => $hasGrcActif['Connecteur']['grc_qrtid']
            );
        }
        else {
            $dataFixed = array(
                'cityId' => Configure::read('GRC.cityId'),
                'canalId' => Configure::read('GRC.canalId'),
                'servId' => Configure::read('GRC.servId'),
                'sqrtId' => Configure::read('GRC.sqrtId'),
                'qrtId' => Configure::read('GRC.qrtId')
            );
        }
        return $dataFixed;
    }


    function LancerRequeteCurl($request, $attachments = null, $datas = array(), $etat = null, $note = null) {

        // selon la requête adressée, on défini l'action derrière
        $this->wsto = 'https://grc28.localeo.fr/public/webgfc/'.$request;

        if( $request == 'createquery' ) {
            $dataFixed = $this->_dataFixed();
            // le flux existe dans la GRC, on le met à jour
            if( !empty($datas['Courrier']['courriergrc_id']) ) {
                $data = array(
                    'requete' => json_encode(
                        array_merge(
                            $dataFixed,
                            array(
                                'subject' => $datas['Courrier']['reference'] . ' - ' .$datas['Courrier']['name'],
                                'content' => $datas['Courrier']['objet'],
                                'geo' => false,
                                'externalId' => $datas['Courrier']['id'],
                                'parentQueryId' => $datas['Courrier']['courriergrc_id']
                            )
                        )
                    ),
                    'citoyen' => json_encode(
                        array(
                            'id' => $datas['Courrier']['contactgrc_id'],
                            'externalId' => $datas['Contact']['id']
                        )
                    )
                );
                if( !empty($attachments) ) {
                    $data = array_merge(
                        $data,
                        array(
                            'filename' => $this->makeCurlFile( $attachments )
                        )
                    );
                }
                $dataToGRC = $data;
            }
            else {
                $data = array(
                    'requete' => json_encode(
                        array_merge(
                            $dataFixed,
                            array(
                                'subject' => $datas['Courrier']['reference'] . ' - ' .$datas['Courrier']['name'],
                                'content' => $datas['Courrier']['objet'],
                                'geo' => false,
                                'externalId' => $datas['Courrier']['id']
                            )
                        )
                    ),
                    'citoyen' => json_encode(
                        array(
                            'id' => $datas['Courrier']['contactgrc_id'],
                            'externalId' => $datas['Contact']['id']
                        )
                    )

                );
                if( !empty($attachments) ) {
                    $data = array_merge(
                        $data,
                        array(
                            'filename' => $this->makeCurlFile( $attachments )
                        )
                    );
                }
                $dataToGRC = $data;
            }
        }
        else if( $request == 'createaccount' ) {
            $datasToSend = $this->table_correspondance($datas, true, true);

            $dataToGRC = array(
                'citoyen' => json_encode(
                    $datasToSend
                )
            );
        }
        else if( $request == 'getstatusquery' ) {
            $this->wsto = 'https://grc28.localeo.fr/public/webgfc/'.$request;
            $dataToGRC = $datas;
        }
        else if( $request == 'updatestatusquery' ) {
            $dataFixed = $this->_dataFixed();
            $data = array(
                'requete' => json_encode(
                    array(
                        'id' => $datas,
                        'statusId' => $etat,
                        'note' => $note
                    )
                )
            );

            $datasToSend = array_merge( $dataFixed, $data);
            $this->wsto = 'https://grc28.localeo.fr/public/webgfc/'.$request;
            $dataToGRC = $datasToSend;
        }
        else {
            $dataToGRC = $datas;
        }

        $ch = curl_init ();
        curl_setopt($ch, CURLOPT_URL, $this->wsto);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array("Api-Key: {$this->apiKey}", "appName: {$this->appName}", "content-type: multipart/form-data"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataToGRC);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//$this->log($dataToGRC);
        $reply = curl_exec($ch);
        curl_close($ch);
        $this->responseMessageStr = json_decode ( $reply );
        return $this->responseMessageStr;
    }


    function echoTest() {
        $request = 'getstatusquery';
        $dataFixed = $this->_dataFixed();

        $data = array(
            'requete' => json_encode(
                array(
                    'id' => 1633315
                )
            )
        );
        $datasToSend = array_merge( $dataFixed, $data);
        $this->lancerRequeteCurl($request, null, $datasToSend);
        return $this->responseMessageStr;
    }

    /**
     * Création d'une requête dans la GRC
     * @param type $data
     * @return type
     */
    function createRequestGRC( $data ) {
        $request = 'createquery';
        $this->lancerRequeteCurl($request, null, $data);
        return $this->responseMessageStr;
    }

    /**
     *  Création d'un contact (de type citoyen)
     * @param type $data
     * @return type
     */
    function createContactGRC($data) {
        $request = 'createaccount';
        $this->lancerRequeteCurl($request, null, $data);
        return $this->responseMessageStr;
    }

    /**
     *  Création d'un contact (de type organisme)
     * @param type $data
     * @return type
     */
    function createOrganismeGRC($data) {
        $request = 'createaccount';
        $this->lancerRequeteCurl($request, null, $data);
        return $this->responseMessageStr;
    }

    /**
     * Lorsque le flux est clos dans web-GFC, on envoie le doc à la GRC
     * @param type $data
     * @param type $doc
     * @return type
     */
    function sendDocGRC($data, $doc, $etat, $note) {
        $request = 'createquery';
        $this->lancerRequeteCurl($request, $doc, $data, $etat, $note);
//$this->log($this->responseMessageStr);
        return $this->responseMessageStr;
    }

    /**
     * Fonction permettant de transformer les données de la GRC Localeo vers la BDD de web-GFC
     * @param type $data
     * @param type $collectiviteId
     * @return type
     */
    function table_correspondance($data, $forContact,  $fromGFC) {
        $dataFixed = $this->_dataFixed();
//$this->log($data);
        // Enregistrement du contact TOUT SEUL Courriers/add_contact.ctp)
        if( $forContact && isset($data['Contact']['externalId']) && !empty( $data['Contact']['externalId'] ) ) {
$this->log('Création du contact depuis web-GFC vers la GRC');
            if( !$fromGFC ) {
                if(!empty($data['ext'])) {
                    $numvoie = $data['num'].' '.$data['ext'];
                }
                else {
                    $numvoie = $data['num'];
                }
                if( !empty($data['additionalAddress2'])) {
                    $complAddress = $data['additionalAddress'].' '.$data['additionalAddress2'];
                }
                else {
                    $complAddress = $data['additionalAddress'];
                }

                if( !empty($data['usualName']) ) {
                    $usualName = $data['usualName'];
                }
                else {
                    $usualName = $data['firstname'].' ' .$data['familyName'];
                }

                $dataForGRC = array(
                    'civilite' => isset( $data['civility'] ) ? $data['civility'] : null,
                    'name' => $usualName,
                    'nom' => isset( $data['familyName'] ) ? $data['familyName'] : null,
                    'prenom' => isset( $data['firstname'] ) ? $data['firstname'] : null,
                    'email' => isset( $data['email'] ) ? $data['email'] : null,
                    'tel' => isset($data['phone']) ? $data['phone'] : null,
                    'portable' => isset( $data['gsm'] ) ? $data['gsm'] : null,
                    'numvoie' => $numvoie, // n° de voie + extension de n°
                    'adresse' => isset( $data['street'] ) ? $data['street'] : null,
                    'adressecomplete' => isset( $data['street'] ) ? $data['street'] : null,
                    'compl' => $complAddress,
                    'cp' => isset( $data['ZC'] ) ? $data['ZC'] : null,
                    'ville' => isset( $data['city'] ) ? $data['city'] : null,
                    'pays' => isset( $data['country'] ) ? $data['country'] : null,
                    'lignedirecte' => isset( $data['phonePro'] ) ? $data['phonePro'] : null,
                    'contactgrc_id' => $data['externalId'],
                    'organisme_id' => $data['organismeId']
                );
            }
            else {
                $this->Contact = ClassRegistry::init('Contact');
                $org = $this->Contact->Organisme->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Organisme.id' => $data['Contact']['organisme_id']
                        ),
                        'recursive' => -1,
                        'contain' => false
                    )
                );
                $contact = $this->Contact->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Contact.id' => $data['Contact']['id']
                        ),
                        'recursive' => -1,
                        'contain' => false
                    )
                );


                // Si le contact n'existe pas encore dans la GRC, on le crée
                if( empty($data['Contact']['contactgrc_id']) && !empty($contact) ) {
$this->log('Création du contact dans la GRC');
                    $dataForGRC = array_merge(
                        $dataFixed,
                        array(
                            'externalId' => $contact['Contact']['id'],
                            //'usualName' => $contact['Contact']['name'],
                            'familyName' => $contact['Contact']['nom'],
                            'firstname' => $contact['Contact']['prenom'],
                            'civility' => $contact['Contact']['civilite'],
                            'company' => $org['Organisme']['name'],
                            'num' => $contact['Contact']['numvoie'],
                            'email' => $contact['Contact']['email'],
                            'ZC' => $contact['Contact']['cp'],
                            'city' => $contact['Contact']['ville'],
                            'country' => $contact['Contact']['pays'],
                            'gsm' => $contact['Contact']['portable'],
                            'phone' => $contact['Contact']['tel'],
                            'phonePro' => $contact['Contact']['lignedirecte'],
                            'street' => $contact['Contact']['nomvoie'],
                            'additionalAddress' => $contact['Contact']['compl'],
                            'ext' => ''
                        )
                    );
                }
                // Si le contact existe déjà dans la GRC, on met ses informations à jour
                else {
$this->log('Mise à jour du contact dans la GRC');
                    $dataForGRC = array_merge(
                        $dataFixed,
                        array(
                            'id' => $data['Contact']['contactgrc_id'],
                            'externalId' => $data['Contact']['externalId'],
                            //'usualName' => $data['Contact']['name'],
                            'familyName' => $data['Contact']['nom'],
                            'firstname' => $data['Contact']['prenom'],
                            'civility' => $data['Contact']['civilite'],
                            'company' => $org['Organisme']['name'],
                            'num' => $data['Contact']['numvoie'],
                            'email' => $data['Contact']['email'],
                            'ZC' => $data['Contact']['cp'],
                            'city' => $data['Contact']['ville'],
                            'gsm' => $data['Contact']['portable'],
                            'phone' => $data['Contact']['tel'],
                            'phonePro' => $data['Contact']['lignedirecte'],
                            'country' => $data['Contact']['pays'],
                            'street' => $data['Contact']['nomvoie'],
                            'additionalAddress' => $data['Contact']['compl'],
                            'ext' => ''
                        )
                    );
                }
            }
        }
        // enregistrement du flux avec contact déjà ajouté (edit_contact)
        else if( $forContact && isset($data['Courrier']['contact_id']) && !empty($data['Courrier']['contact_id'])) {
$this->log('Création du contact / organisme et association à une requête depuis web-GFC vers la GRC');
            $this->Contact = ClassRegistry::init('Contact');
            $contact = $this->Contact->find(
                'first',
                array(
                    'conditions' => array(
                        'Contact.id' => $data['Courrier']['contact_id']
                    ),
                    'recursive' => -1,
                    'contain' => false
                )
            );

            $this->Organisme = ClassRegistry::init('Organisme');
            $org = $this->Organisme->find(
                'first',
                array(
                    'conditions' => array(
                        'Organisme.id' => $data['Organisme']['organisme_id']
                    ),
                    'recursive' => -1,
                    'contain' => false
                )
            );
            if(!empty($contact)) {
$this->log('Contact déjà existant dans web-GFC');
                // Le contact existe déjà dans la GRC , on l'assacie donc à la requête dans la GRC
                if( !empty( $contact['Contact']['contactgrc_id'] ) ) {
$this->log('Contact déjà existant dans web-GFC et la GRC');
                    $dataForGRC = array(
                        'requete' => json_encode(
                            array_merge(
                                $dataFixed,
                                array(
                                    'subject' => $data['Courrier']['name'],
                                    'content' => $data['Courrier']['objet'],
                                    'geo' => '',
                                    'externalId' => $data['Courrier']['id']
                                )
                            )
                        ),
                        'citoyen' => json_encode(
                            array(
                                'id' => $contact['Contact']['contactgrc_id'],
                                'externalId' => $contact['Contact']['externalId']
                            )
                        )
                    );
                }
                else {
$this->log('Contact déjà existant dans web-GFC mais pas dans la GRC, on le crée dans la GRC');
                    // il n'existe pas, on ne le prend pas en compte
                    $dataForGRC = array(
                        'requete' => json_encode(
                            array_merge(
                                $dataFixed,
                                array(
                                    'subject' => $data['Courrier']['name'],
                                    'content' => $data['Courrier']['objet'],
                                    'geo' => false,
                                    'externalId' => $data['Courrier']['id'],
                                    'civility' => $contact['Contact']['civilite'],
                                    //'usualName' => $contact['Contact']['name'],
                                    'familyName' => $contact['Contact']['nom'],
                                    'firstname' => $contact['Contact']['prenom'],
                                    'company' => $org['Organisme']['name'],
                                    'email' => $contact['Contact']['email'],
                                    'gsm' => '',
                                    'phone' => '',
                                    'num' => $contact['Contact']['numvoie'],
                                    'additionalAddress' => $contact['Contact']['compl'],
                                    'ext' => '',
                                    'street' => $contact['Contact']['nomvoie'],
                                    'ZC' => $contact['Contact']['cp'],
                                    'city' => $contact['Contact']['ville'],
                                    'country',
                                    'geoType' => '',
                                    'numberIdAddr' => '',
                                    'streetIdAddr' => '',
                                    'cityIdAddr' => ''
                                )
                            )
                        )
                    );
                }
            }
        }
        else if( $forContact && isset($data['Organisme']['externalId']) && !empty($data['Organisme']['externalId'])) {
$this->log('Création dun organisme depuis web-GFC vers la GRC');
            $this->Organisme = ClassRegistry::init('Organisme');
            $org = $this->Organisme->find(
                'first',
                array(
                    'conditions' => array(
                        'Organisme.id' => $data['Organisme']['externalId']
                    ),
                    'recursive' => -1,
                    'contain' => false
                )
            );
            // L'organisme existe déjà dans web-GFC ET la gRC
            if( !empty($org) ) {
$this->log('Création dun organisme existant dans web-GFC vers la GRC');
                // L'organisme existe déjà dans la GRC ET on crée une requête dans la GRC, on l'associe donc à la requête dans la GRC
                if( !empty( $org['Organisme']['contactgrc_id'] ) && !empty($data['Courrier']['id']) ) {
$this->log('Organisme existant dans web-GFC ET dans la GRC et association avec une requête');
                    $dataForGRC = array(
                        'requete' => json_encode(
                            array_merge(
                                $dataFixed,
                                array(
                                    'subject' => $data['Courrier']['name'],
                                    'content' => $data['Courrier']['objet'],
                                    'geo' => '',
                                    'externalId' => $data['Courrier']['id'],
                                    'company' => $data['Organisme']['name']
                                )
                            )
                        ),
                        'citoyen' => json_encode(
                            array(
                                'id' => $org['Organisme']['contactgrc_id'],
                                'externalId' => $org['Organisme']['externalId']
                            )
                        )
                    );
                }
                else {
                    // L'organisme existe déjà dans la GRC ET existe dans web-GFC,  on met simplement ses infos à jour dans la GRC
                    if( !empty( $data['Organisme']['contactgrc_id'] ) ) {
$this->log('Organisme existant dans web-GFC ET dans la GRC');
                        $dataForGRC = array_merge(
                            $dataFixed,
                            array(
                                'id' => $org['Organisme']['contactgrc_id'],
                                'externalId' => $data['Organisme']['externalId'],
                                'num' => $data['Organisme']['numvoie'],
                                'company' => $data['Organisme']['name'],
                                'email' => $data['Organisme']['email'],
                                'ZC' => $data['Organisme']['cp'],
                                'city' => $data['Organisme']['ville'],
                                //'usualName' => $data['Organisme']['name'],
                                'familyName' => $data['Organisme']['name'],
                                'firstname' => $data['Organisme']['name'],
                                'street' => $data['Organisme']['nomvoie'],
                                'additionalAddress' => $data['Contact']['compl'],
                                'ext' => '',
                                'gsm' => $data['Organisme']['portable'],
                                'phone' => $data['Organisme']['tel'],
                                'country' => $data['Organisme']['pays' ]
                            )
                        );
                    }
                    else {
$this->log('Organisme existant dans web-GFC mais pas dans la GRC');
                        // L'organisme n'existe pas dans la GRC ET existe dans web-GFC,  on le crée dans la gRC
                        $dataForGRC = array(
                            'requete' => json_encode(
                                array_merge(
                                    $dataFixed,
                                    array(
                                        'subject' => $data['Courrier']['name'],
                                        'content' => $data['Courrier']['objet'],
                                        'geo' => false,
                                        'externalId' => $data['Courrier']['id'],
                                        'civility' => 1,
                                        //'usualName' => $org['Organisme']['name'],
                                        'familyName' => $org['Organisme']['name'],
                                        'firstname' => '',
                                        'company' => $org['Organisme']['name'],
                                        'email' => $org['Organisme']['email'],
                                        'gsm' => '',
                                        'phone' => '',
                                        'num' => $org['Organisme']['numvoie'],
                                        'additionalAddress' => $contact['Contact']['compl'],
                                        'ext' => '',
                                        'street' => $org['Organisme']['nomvoie'],
                                        'ZC' => $org['Organisme']['cp'],
                                        'city' => $org['Organisme']['ville'],
                                        'country',
                                        'geoType' => '',
                                        'numberIdAddr' => '',
                                        'streetIdAddr' => '',
                                        'cityIdAddr' => ''
                                    )
                                )
                            )
                        );
                    }
                }
            }
        }

        return $dataForGRC;
    }


    /**
     * Pour obtenir l'état de la demande dans la GRC
     * @param type $data
     *  $data = array(
            'requete' => json_encode(
                array(
                    'id' => 1633315
                )
            )
        );
     * @return type
     */
    function getStatusGRC( $data ) {
        $dataToLook = array(
            'requete' => json_encode(
                array(
                    'id' => $data
                )
            )
        );
        $this->lancerRequeteCurl('getstatusquery', null, $dataToLook);
        return $this->responseMessageStr;
    }


    /**
     * Pour mettre àjour l'état de la demande dans la GRC
     * @param type $data
            $data = array(
                'requete' => json_encode(
                    array(
                       'id' => 1606408,
                       'statusId' => 5
                    )
                )
            );
     * @return type
     */
    function updateStatusGRC( $data, $etat, $note ) {
        $this->lancerRequeteCurl('updatestatusquery', null, $data, $etat, $note);
        return $this->responseMessageStr;
    }

    function makeCurlFile($file){
        $mime = mime_content_type($file);
        $info = pathinfo($file);
        $name = $info['basename'];
        $output = new CURLFile($file, $mime, $name);
        return $output;
    }
}

