<?php

/**
 *
 * Ajaxformvalid component class.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
class AjaxformvalidComponent extends Component {

	/**
	 * Controller
	 *
	 * @var type
	 */
	public $controller;

	/**
	 * Component initialization
	 *
	 * @access public
	 * @param type $controller
	 * @return void
	 */
	public function initialize(Controller $controller) {
		$this->controller = $controller;
	}

	/**
	 *
	 */
	public function valid($models, $requestData, $collectiviteId = 0) {
		$conn = '';
		if ($collectiviteId > 0) {
			$conn = ClassRegistry::init('Collectivite')->field('conn', array('Collectivite.id' => $collectiviteId));
		}

		$this->controller->autoRender = false;
		$allErrors = array();
		$withFields = true;

		foreach ($models as $model => $fields) {
			if (!is_array($fields)) {
				$model = $fields;
				$withFields = false;
			}
			$this->controller->loadModel($model);

			if ($conn != '') {
				$this->controller->{$model}->setDataSource($conn);
			}

			$this->controller->{$model}->create($requestData);
//			$this->controller->{$model}->set($requestData);

			if (!$this->controller->{$model}->validates()) {
				$errors = $this->controller->{$model}->validationErrors;

				if ($withFields) {
					foreach (array_keys($errors) as $field) {
						if (!in_array($field, $fields)) {
							unset($errors[$field]);
						}
					}
				}
				$allErrors[$model] = $errors;
			}
		}
		$content = json_encode($allErrors);
		header('Pragma: no-cache');
		header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
		header('Content-Type: text/x-json');
		header("X-JSON: {$content}");
		Configure::write('debug', 0);
		echo $content;
		return;
	}

}

?>
