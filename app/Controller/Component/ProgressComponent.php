<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The AGPL v3 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     https://choosealicense.com/licenses/agpl-3.0/ AGPL v3 License
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppTools', 'Lib');

/**
 * Composant de barre de progression
 * @package app.Controller.Component
 * @version  5.1.0
 * @since 4.4.0
 */
class ProgressComponent extends Component
{

    /**
     * Other components utilized by AuthComponent
     *
     * @var array
     */
    public $components = array('Session', 'RequestHandler');

    /**
     * The session key name where the record of the current generation is stored. Default
     * key is "Generation.Tokens". If you are using only stateless authenticators set this
     * to false to ensure session is not started.
     *
     * @var string
     */
    public static $sessionKey = 'Progress.Tokens';
    /**
     * [public description]
     * @var string
     */
    public $tokenKey = null;

    /**
     * [STATUS_WAITING description]
     * @var integer
     */
    const STATUS_WAITING = 1;
    /**
     * [STATUS_RUNNING description]
     * @var integer
     */
    const STATUS_RUNNING = 2;
    /**
     * [STATUS_FAILED description]
     * @var integer
     */
    const STATUS_FAILED = 3;
    /**
     * [STATUS_COMPLETE description]
     * @var integer
     */
    const STATUS_COMPLETE = 4;
    /**
     * [STATUS_WAITING_PURGE description]
     * @var integer
     */
    const STATUS_WAITING_PURGE = 5;

    /**
     * Initializes AuthComponent for use in the controller.
     *
     * @param Controller $controller A reference to the instantiating controller object
     * @return void
     */
    public function initialize(Controller $controller)
    {
        $this->request = $controller->request;
        $this->response = $controller->response;
        $this->_methods = $controller->methods;
    }

    /**
     * Called automatically after controller beforeFilter
     * @param Controller $controller A reference to the controller object
     */
    public function startup(Controller $controller)
    {
    }

    /**
     * [start description]
     * @param  [type] $cookieTokenKey [description]
     * @return [type]                 [description]
     */
    public function start($cookieTokenKey)
    {
        $tokens = $this->Session->read(self::$sessionKey);
        //Purge des tokens
        if (!empty($tokens)) {
            foreach ($tokens as $key => $value) {
                // Limite d'une heure pour une progression
                if (!empty($tokens[$key]['timestamp_start']) && $tokens[$key]['timestamp_start'] < (time() - (60 * 60))) {
                    $this->Session->delete(self::$sessionKey . '.' . $key);
                }
            }
        }

        $this->tokenKey = $cookieTokenKey;
        session_write_close();
        $this->sessionWrite(array(
            'progress' => 0,
            'timestamp_start' => time(),
            'status' => self::STATUS_WAITING,
            'error' => array(),
            'progress_comment' => __(''),
        ));
    }

    /**
     * [at description]
     * @param  [type] $progression      [description]
     * @param  [type] $progress_comment [description]
     * @return [type]                   [description]
     */
    public function at($progression, $progress_comment = null)
    {
        $this->sessionWrite('status', self::STATUS_RUNNING);
        if (!empty($progress_comment)) {
            $this->sessionWrite('progress_comment', $progress_comment);
        }
        $this->sessionWrite('progress', round($progression));
    }

    /**
     * [stop description]
     * @param  [type] $progress_comment [description]
     * @return [type]                   [description]
     */
    public function stop($progress_comment = null)
    {
        if (!empty($progress_comment)) {
            $this->sessionWrite('progress_comment', $progress_comment);
        }
        $this->sessionWrite('timestamp_stop', time());
        $this->sessionWrite('progress', 100);
        $this->sessionWrite('status', self::STATUS_COMPLETE);
    }

    /**
     * [redirect description]
     * @param  [type] $redirect [description]
     * @return [type]           [description]
     */
    public function redirect($redirect)
    {
        $this->sessionWrite('redirect', Router::normalize($redirect));
    }

    /**
     * [error description]
     * @param  Exception $exception [description]
     * @return [type]               [description]
     */
    public function error(Exception $exception)
    {
        $this->sessionWrite('progress', 100);
        $this->sessionWrite('status', self::STATUS_FAILED);
        $this->sessionWrite('error', array(
            'code' => $exception->getCode(),
            'message' => $exception->getMessage()));
    }

    /**
     * [getToken description]
     * @param  [type] $cookieTokenKey [description]
     * @return [type]                 [description]
     */
    public function getToken($cookieTokenKey)
    {
    }

    /**
     * [sessionWrite description]
     * @param  string $session_key   [description]
     * @param  string $session_value [description]
     * @version 5.1.0
     * @since 4.4.0
     */
    private function sessionWrite($session_key, $session_value = null)
    {
        @session_start();
        $values = array();
        if (!is_array($session_key)) {
            $values = array($session_key => $session_value);
        } else {
            $values = $session_key;
        }
        foreach ($values as $key => $value) {
            $this->Session->write(self::$sessionKey . '.' . $this->tokenKey . '.' . $key, $value);
        }

        session_write_close();
    }

    /**
     * [setFile description]
     * @param [type] $filename [description]
     * @param [type] $typemime [description]
     * @param [type] $content  [description]
     */
    public function setFile($filename, $typemime, $content)
    {
        // fusion du document
        $folderTmp = new Folder(TMP . 'files' . DS . 'progress' . DS . $this->tokenKey, true, 0777);
        $file_generation = new File($folderTmp->pwd() . DS . $filename, true);
        $file_generation->write($content);
        $file_generation->close();
        unset($content);

        $this->sessionWrite('download', true);
        $this->sessionWrite('filename', $filename);
        $this->sessionWrite('typemime', $typemime);
    }

    /**
     * [clear description]
     * @param  [type] $session_key [description]
     * @return [type]              [description]
     */
    public function clear($session_key)
    {
        $this->tokenKey = $session_key;
        $this->sessionWrite('status', self::STATUS_WAITING_PURGE);
    }
}

