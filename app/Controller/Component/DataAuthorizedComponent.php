<?php

App::uses('DataAclComponent', 'SessionDataAcl.Controller/Component');

/**
 *
 * DataAuthorized component class.
 * Set of tools to get authorized object against DataAcl plugin (use SessionDataAcl Component)
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par Libriciel SCOP
 * @link http://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
class DataAuthorizedComponent extends Component {

	/**
	 * Component components
	 *
	 * @access public
	 * @var array
	 */
	public $components = array('SessionDataAcl');

	/**
	 * Récupère les DAROs correspondant à un utilisateur ou à un rôle
	 *
	 * @access private
	 * @param array $options
	 * @throws BadMethodCallException
	 * @return array
	 */
	private function _getDaros($options) {
		$baseSettings = array(
			'desktopId' => '',
			'userId' => CakeSession::read('Auth.User.id'),
			'mode' => 'user'
		);
		$settings = array_merge($options, $baseSettings);

		if (($settings['mode'] != 'user' && $settings['mode'] != 'desktop' ) || ( $settings['mode'] == 'desktop' && empty($settings['desktopId']))) {
			throw new BadMethodCallException();
		}

		if ($settings['mode'] == 'user') {
			$this->User = ClassRegistry::init('User');
			$daros = $this->User->getDaros($settings['userId']);
		} else if ($settings['mode'] == 'desktop') {
			$this->Desktop = ClassRegistry::init('Desktop');
			$daros = $this->Desktop->getDaros($settings['desktopId']);
		}
		return $daros;
	}

	/**
	 * Vérifie l'autorisation d'accès d'un DARO à un DACO
	 *
	 * @access private
	 * @param array $daros
	 * @param array $daco
	 * @param array $action
	 * @return boolean
	 */
	private function _chk($daros, $daco, $action = 'read') {
		$chk = false;
		foreach ($daros as $daro) {
			if ($this->SessionDataAcl->check($daro, $daco, $action)) {
				$chk = true;
			}
		}
		return $chk;
	}

	/**
	 * Vérifie l'autorisation d'accès d'un DARO à un DACO (Type)
	 *
	 * @access public
	 * @param array $options
	 * @throws BadMethodCallException
	 * @return boolean
	 */
	public function checkType($options = array()) {
		if (!is_array($options) || is_array($options) && empty($options['typeId'])) {
			throw new BadMethodCallException();
		}
		$daros = $this->_getDaros($options);
		$this->Type = ClassRegistry::init('Type');
		$daco = $this->Type->getDaco($options['typeId']);
		return $this->_chk($daros, $daco);
	}

	/**
	 * Vérifie l'autorisation d'accès d'un DARO à un DACO (Sous-type)
	 *
	 * @access public
	 * @param array $options
	 * @throws BadMethodCallException
	 * @return boolean
	 */
	public function checkSoustype($options = array()) {
		if (!is_array($options) || is_array($options) && empty($options['soustypeId'])) {
			throw new BadMethodCallException();
		}
		$daros = $this->_getDaros($options);
		$this->Soustype = ClassRegistry::init('Soustype');
		$daco = $this->Soustype->getDaco($options['soustypeId']);
		return $this->_chk($daros, $daco);
	}

	/**
	 * Vérifie l'autorisation d'accès d'un DARO à un DACO (Méta-donnée)
	 *
	 * @access public
	 * @param array $options
	 * @throws BadMethodCallException
	 * @return boolean
	 */
	public function checkMetadonnee($options = array()) {
		if (!is_array($options) || is_array($options) && empty($options['metadonneeId'])) {
			throw new BadMethodCallException();
		}
		$daros = $this->_getDaros($options);
		$this->Metadonnee = ClassRegistry::init('Metadonnee');
		$daco = $this->Metadonnee->getDaco($options['metadonneeId']);
		return $this->_chk($daros, $daco);
	}

	/**
	 * Récupère les sous-types auxquels peut accèder l'utilisateur connecté
	 *
	 * @access public
	 * @param array $options
	 * @return array
	 * @throws BadMethodCallException
	 */
	public function getAuthSoustypes($options = array()) {
		if (!is_array($options)) {
			throw new BadMethodCallException();
		}
		$baseSettings = array('find' => 'all');
        if(CakeSession::read('Auth.User.Desktop.Profil.name') == 'Admin' || CakeSession::read('Auth.ProfilAdmin') == 'Oui') {
            $settings = array( 'find' => 'list');
        }
        else {
            $settings = array_merge($baseSettings, $options);
        }
		$this->User = ClassRegistry::init('User');
		$this->Soustype = ClassRegistry::init('Soustype');
		$querydata = array(
			'recursive' => -1,
			'order' => array(
				'Soustype.name'
			)
		);
		$stypes = $this->Soustype->find($settings['find'], $querydata);
		$authStypes = array();
		$daros = $this->User->getDaros(CakeSession::read('Auth.User.id'));
//debug();
        if(CakeSession::read('Auth.User.Desktop.Profil.name') == 'Admin' || CakeSession::read('Auth.ProfilAdmin') == 'Oui') {
            $authStypes = $stypes;
        }
        else {
            foreach ($stypes as $kstype => $stype) {
                $value = Hash::combine( $stype, 'Soustype.id', 'Soustype.name');

                $chk = false;
                if ($settings['find'] == 'list') {
                    $daco = $this->Soustype->getDaco($kstype);
                } else {
                    $daco = $this->Soustype->getDaco($stype['Soustype']['id']);
                }
                foreach ($daros as $daro) {
                    if ($this->SessionDataAcl->check($daro, $daco)) {
                        $chk = true;
                    }
                }
                if ($chk) {
                    $authStypes[$stype['Soustype']['id']] = $stype['Soustype']['name'];
                }
            }
        }

		return $authStypes;
	}

	/**
	 * Récupère les types auxquels peut accèder l'utilisateur connecté
	 *
	 * @access public
	 * @param array $options
	 * @return array
	 * @throws BadMethodCallException
	 */
	public function getAuthTypes($options = array()) {
		if (!is_array($options)) {
			throw new BadMethodCallException();
		}
		$baseSettings = array(
			'find' => 'all',
			'withSoustypes' => false,
			'onlyActif' => true
		);
		$settings = array_merge($baseSettings, $options);
		$this->User = ClassRegistry::init('User');
		$this->Type = ClassRegistry::init('Type');
        $conditions = array();
        if($settings['onlyActif'] == true) {
            $conditions = array( 'Type.active' => 1 );
        }
        $querydata = array(
            'recursive' => -1,
            'conditions' => $conditions,
            'order' => array(
                'Type.name'
            ),
            'fields' => array(
                'Type.id',
                'Type.name'
            )
        );
		if ($settings['withSoustypes']) {
			$querydata['recursive'] = -1;
            $conditionsST = array();
            if($settings['onlyActif']) {
                $conditionsST = array( 'Soustype.active' => 1 );
            }
			$querydata['contain'] = array(
				$this->Type->Soustype->alias => array(
					'conditions' => $conditionsST,
					'order' => array(
						'Soustype.name'
					),
                    'fields' => array(
                        'Soustype.id',
                        'Soustype.name',
                        'Soustype.entrant'
                    )
				)
			);
		}
		$types = $this->Type->find($settings['find'], $querydata);
		$authTypes = array();
		$daros = $this->User->getDaros(CakeSession::read('Auth.User.id'));

        if(CakeSession::read('Auth.User.Desktop.Profil.name') == 'Admin' || CakeSession::read('Auth.ProfilAdmin') == 'Oui') {
            $authTypes = $types;
        }
        else {
            foreach ($types as $ktype => $type) {
                $chk = false;
                if ($settings['find'] == 'list') {
                    $daco = $this->Type->getDaco($ktype);
                } else {
                    $daco = $this->Type->getDaco($type['Type']['id']);
                }
                foreach ($daros as $daro) {
                    if ($this->SessionDataAcl->check($daro, $daco)) {
                        $chk = true;
                    }
                }
                if ($chk) {
                    $authStypes = array();
                    if (!empty($type['Soustype']) && $settings['find'] != 'list') {
                        foreach ($type['Soustype'] as $kstype => $stype) {
                            $chkStype = false;
                            $sdaco = $this->Type->Soustype->getDaco($stype['id']);
                            foreach ($daros as $daro) {
                                if ($this->SessionDataAcl->check($daro, $sdaco)) {
                                    $chkStype = true;
                                }
                            }
                            if ($chkStype) {
                                $authStypes[$kstype] = $stype;
                            }
                        }
                        $type['Soustype'] = $authStypes;
                    }
                    $authTypes[$ktype] = $type;
                }
            }
        }
		return $authTypes;
	}

	/**
	 * Récupère les méta-données auxquelles peut accèder l'utilisateur connecté
	 *
	 * @access public
	 * @param array $options
	 * @return array
	 * @throws BadMethodCallException
	 */
	public function getAuthMetadonnees($options = array()) {
		if (!is_array($options)) {
			throw new BadMethodCallException();
		}
		$baseSettings = array(
			'find' => 'all',
			'action' => 'read',
			'desktopId' => null,
			'fluxId' => null
		);
		$settings = array_merge($baseSettings, $options);
		if (empty($settings['desktopId'])) {
			$this->User = ClassRegistry::init('User');
		} else {
			$this->Desktop = ClassRegistry::init('Desktop');
		}
		$this->Metadonnee = ClassRegistry::init('Metadonnee');
		$querydata = array(
			'recursive' => -1,
			'conditions' => array(
                'Metadonnee.active' => true
            ),
			'order' => array(
				'Metadonnee.name'
			)
		);

		if(!Configure::read('Courrier.SaveDateCloture')){
			$querydata['conditions'][] = array('Metadonnee.id <>' => '-1' );
		}

		if (!empty($settings['fluxId'])) {
			$querydata['fields'] = array_merge($this->Metadonnee->fields(), $this->Metadonnee->CourrierMetadonnee->fields());
			$querydata['joins'] = array(
				$this->Metadonnee->join('CourrierMetadonnee', array('type' => 'LEFT OUTER'))
			);
			$querydata['contain'] = false;
			$querydata['conditions'] = array(
				'CourrierMetadonnee.courrier_id' => $settings['fluxId']
			);
		}
		$metas = $this->Metadonnee->find($settings['find'], $querydata);

        // Ajout des valeurs de select d'une métadonnée
        if (!empty($metas)) {
            $selectvaluesmetadonnees = array();
            $this->Selectvaluemetadonnee = ClassRegistry::init('Selectvaluemetadonnee');
            foreach( $metas as $key => $meta ) {
//                debug($this);
//                debug($metas);
//                die();
                $querydata['fields'] = array_merge($this->Metadonnee->fields(), $this->Selectvaluemetadonnee->fields());
                $querydata['joins'] = array(
                    $this->Selectvaluemetadonnee->join('Metadonnee', array('type' => 'LEFT OUTER'))
                );
                $querydata['contain'] = false;

                $querydata['conditions'] = array(
                    'Selectvaluemetadonnee.metadonnee_id' => isset( $meta['Metadonnee']['id'] ) ? $meta['Metadonnee']['id'] : $key,
                    'Selectvaluemetadonnee.active' => true
                );

                $querydata['order'] = array( 'Selectvaluemetadonnee.name ASC');
                $selectvaluesmetadonnees = $this->Selectvaluemetadonnee->find('all', $querydata);
                if( isset( $meta['Metadonnee']['id'] ) ){
                    $metas[$key]['Selectvaluemetadonnee'] = Hash::combine( $selectvaluesmetadonnees, '{n}.Selectvaluemetadonnee.id', '{n}.Selectvaluemetadonnee.name' );
                }
            }

		}


		$authMetas = array();
		if (empty($settings['desktopId'])) {
			$daros = $this->User->getDaros(CakeSession::read('Auth.User.id'));
		} else {
			$daros = $this->Desktop->getDaros($settings['desktopId']);
		}

        if(CakeSession::read('Auth.User.Desktop.Profil.name') == 'Admin' || CakeSession::read('Auth.ProfilAdmin') == 'Oui') {
            $authMetas = $metas;
        }
        else {
            foreach ($metas as $kmeta => $meta) {
                $chk = false;
                if ($settings['find'] == 'list') {
                    $daco = $this->Metadonnee->getDaco($kmeta);
                } else {
                    $daco = $this->Metadonnee->getDaco($meta['Metadonnee']['id']);
                }
                foreach ($daros as $daro) {
                    if ($this->SessionDataAcl->check($daro, $daco, $settings['action'])) {
                        $chk = true;
                    }
                }
                if ($chk) {
                    $authMetas[$kmeta] = $meta;
                }
            }
        }
		return $authMetas;
	}

	/**
	 * Récupère tous les rôles autorisés à accéder à un DACO
	 *
	 * @access public
	 * @param array $options
	 * @throws BadMethodCallException
	 * @return array
	 */
	public function getAuthDesktops($options = array()) {
		if (!is_array($options)) {
			throw new BadMethodCallException();
		}
		$soustypes = array();
		$baseSettings = array(
			'etapeId' => '',
			'circuitId' => '',
			'soustypeId' => ''
		);
		$settings = array_merge($baseSettings, $options);
		if (!empty($settings['etapeId'])) {
			$this->Etape = ClassRegistry::init('Cakeflow.Etape');
			$settings['circuitId'] = $this->Etape->field('circuit_id', array('Etape.id' => $settings['etapeId']));
		}
		if (!empty($settings['circuitId'])) {
			$this->Soustype = ClassRegistry::init('Soustype');
			$soustypes = array_keys($this->Soustype->find('list', array('conditions' => array('Soustype.circuit_id' => $settings['circuitId']))));
		}
		if (!empty($settings['soustypeId'])) {
			$this->Soustype = ClassRegistry::init('Soustype');
			$soustypes = array_keys($this->Soustype->find('list', array('conditions' => array('Soustype.id' => $settings['soustypeId']))));
		}
		if (empty($soustypes)) {
			throw new BadMethodCallException();
		}
		$authDesktops = array();
		$this->Desktop = ClassRegistry::init('Desktop');
		$desktops = $this->Desktop->find('list', array('conditions' => array('Desktop.active' => true, 'NOT' => array('Desktop.profil_id' => array(ADMIN_GID, INIT_GID, DISP_GID))), 'order' => 'Desktop.name ASC'));

        // 20150817 :FIXME: nouvelle gestion des bureaux et circuits
        $this->Desktopmanager = ClassRegistry::init('Desktopmanager');
//        $desktops = $this->Desktopmanager->find('list', array('order' => 'Desktopmanager.name ASC'));
        $desktops = $this->Desktopmanager->getAllDesktopsByProfils( array( VALEDIT_GID, VAL_GID, ARCH_GID ) );
//        $desktopsIds = array_keys($desktops);
//        debug($desktopsIds);

//        $desktopsWithRole = $this->Desktopmanager->DesktopDesktopmanager->find(
//            'all',
//            array(
//                'conditions' => array(
//                    'DesktopDesktopmanager.desktop_id' => $desktopsIds
//                )
//            )
//        );
//        foreach( $desktopsWithRole as $i => $desktopProfil) {
//            if(in_array( $desktopProfil['Desktop']['profil_id'], array(ADMIN_GID, DISP_GID, INIT_GID) )) {
//                $desktops[] = $desktopsWithRole;
//            }
//        }

//debug($desktopsWithRole);


        $isAdminDelegated = in_array( 'Admin', Hash::extract( CakeSession::read('Auth.User.SecondaryDesktops'), '{n}.Profil.name' ) );
        if(CakeSession::read('Auth.User.Desktop.Profil.name') == 'Admin' || $isAdminDelegated || CakeSession::read('Auth.ProfilAdmin') == 'Oui') {
            $authDesktops = $desktops;
        }
        else {
            foreach ($soustypes as $soustypeId) {
                $daco = $this->Soustype->getDaco($soustypeId);
                foreach ($desktops as $desktopId => $desktopName) {
                    $daros = $this->Desktop->getDaros($desktopId);
                    $chk = false;
                    foreach ($daros as $daro) {
                        if ($this->SessionDataAcl->check($daro['Daro'], $daco)) {
                            $chk = true;
                        }
                    }
                    if ($chk) {
                        $authDesktops[$desktopId] = $desktopName;
                    }
                }
            }
        }
		return $authDesktops;
	}

}

?>
