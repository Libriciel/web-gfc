<?php
/**
 * Class DsComponent
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT <arnaud.auzolat@libriciel.coop>
 * @editor Libriciel SCOP
 *
 * @created 19 sept 2018
 * @copyright  Développé par Libriciel SCOP
 * @link http://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
class DsComponent extends Component {

	public $apiKey;
	public $responseMessageStr;
	public $wsto;
	public $wsdl;
	public $boundary;

	// sudo apt-get install jq
	function DsComponent() {

		$conn = CakeSession::read('Auth.User.connName');
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$this->Connecteur ->setDataSource($conn);
		$hasDsActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Démarches%',
					'Connecteur.use_ds' => true
				),
				'contain' => false
			)
		);
		$this->apiKey = @$hasDsActif['Connecteur']['ds_apikey'];
		$this->wsto = @$hasDsActif['Connecteur']['host'];
	}

	/**
	 * Fonction cUrl pour récupérer les donneées de DS
	 * @param $request
	 */
	function LancerRequeteCurl($request) {
		// get cURL resource
		$ch = curl_init();
		// set url
		curl_setopt($ch, CURLOPT_URL, $this->wsto);
		// set method
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		// return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// set headers
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: Bearer '.$this->apiKey,
			'Content-Type: application/json; charset=utf-8',
		]);
		// json body
		$json_array = [
			'query' => $request
		];
		$body = json_encode($json_array);
		// set body
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		if( Configure::read('Curl.UseProxy' ) ) {
			curl_setopt($ch, CURLOPT_PROXY, Configure::read('Curl.ProxyHost' ) );
		}
		// send the request and save response to $response
		$response = curl_exec($ch);
		$this->responseMessageStr = json_decode ( $response );
		// stop if fails
		if (!$response) {
			die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
		}
		// close curl resource to free up system resources
		curl_close($ch);
	}

	/**
	 * Fonction permettant de récupérer la structure des demandes
	 * @param $procedureId
	 * @return string
	 *
	 */
	public  function _getDemarcheInfos($procedureId) {
		$request = "{
			demarche(number: $procedureId) {
				id
				number
				title
				description
				state
				service {
					id
					nom
					organisme
					typeOrganisme
				}
				champDescriptors {
				  id
				  type
				  label
				}
				groupeInstructeurs {
					label
					instructeurs {
						email
					}
				}
			}
		  }";
		return $request;
	}



	/**
	 * Fonction permettant de récupérer la structure des demandes
	 * @param $procedureId
	 * @return string
	 *
	 */
	public  function _getAllDemarcheInfos($procedureId, $dossierId = null) {
		$request = "{
			demarche(number: $procedureId) {
				id
				number
				title
				description
				state
				service {
					id
					nom
					organisme
					typeOrganisme
				}
				champDescriptors {
				  id
				  type
				  label
				}
				groupeInstructeurs {
					label
					instructeurs {
						email
					}
				}
				dossiers(first: 10) {
				  nodes {
					id
					number
					datePassageEnConstruction
					datePassageEnInstruction
					dateTraitement
					dateDerniereModification
					usager {
					  email
					}
					demandeur {
					  ... on PersonnePhysique {
						civilite
						nom
						prenom
					  }
					  ... on PersonneMorale {
						siret
						naf
						entreprise {
						  siren
						  dateCreation
						}
						association {
						  rna
						  dateCreation
						}
					  }
					}
					messages {
					  attachment {
						filename
						url
					  }
					  body
					  createdAt
					}
					avis {
					  attachment {
						url
						filename
					  }
						dateQuestion
						dateReponse
						expert {
						  email
						}
						instructeur {
						  email
						}
						question
						reponse
					}
					champs {
					  id
					  label
					  ... on TextChamp {
						value
					  }
					  ... on DecimalNumberChamp {
						value
					  }
					  ... on IntegerNumberChamp {
						value
					  }
					  ... on CheckboxChamp {
						value
					  }
					  ... on DateChamp {
						value
					  }
					  ... on DossierLinkChamp {
						dossier {
						  id
						}
					  }
					  ... on MultipleDropDownListChamp {
						values
					  }
					  ... on LinkedDropDownListChamp {
						primaryValue
						secondaryValue
					  }
					  ... on PieceJustificativeChamp {
						file {
						  byteSize
						  checksum
						  contentType
						  filename
						  url
						}
					  }
					  ... on CarteChamp {
						geoAreas {
						  source
						  geometry {
							type
							coordinates
						  }
						}
					  }
					  ... on SiretChamp {
						etablissement {
						  siret
						  naf
						  entreprise {
							siren
							dateCreation
						  }
						  association {
							rna
							dateCreation
						  }
						}
					  }
					}
				  }
				  pageInfo {
					hasNextPage
					endCursor
				  }
				}
				}
		  }";
		return $request;
	}


	/**
	 * Fonction permettant de récupérer la structure des demandes
	 * @param $procedureId
	 * @return string
	 *
	 */
	public  function _getDossierInfos($dossierId) {
		$request = "{
				dossier(number: $dossierId) {
					id
					number
					datePassageEnConstruction
					datePassageEnInstruction
					dateTraitement
					dateDerniereModification
					usager {
					  email
					}
					demandeur {
					  ... on PersonnePhysique {
						civilite
						nom
						prenom
					  }
					  ... on PersonneMorale {
						siret
						naf
						entreprise {
						  siren
						  dateCreation
						}
						association {
						  rna
						  dateCreation
						}
					  }
					}
					messages {
					  attachment {
						filename
						url
					  }
					  body
					  createdAt
					}
					avis {
					  attachment {
						url
						filename
					  }
						dateQuestion
						dateReponse
						expert {
						  email
						}
						instructeur {
						  email
						}
						question
						reponse
					}
					champs {
					  id
					  label
					  ... on TextChamp {
						value
					  }
					  ... on DecimalNumberChamp {
						value
					  }
					  ... on IntegerNumberChamp {
						value
					  }
					  ... on CheckboxChamp {
						value
					  }
					  ... on DateChamp {
						value
					  }
					  ... on DossierLinkChamp {
						dossier {
						  id
						}
					  }
					  ... on MultipleDropDownListChamp {
						values
					  }
					  ... on LinkedDropDownListChamp {
						primaryValue
						secondaryValue
					  }
					  ... on PieceJustificativeChamp {
						file {
						  byteSize
						  checksum
						  contentType
						  filename
						  url
						}
					  }
					  ... on CarteChamp {
						geoAreas {
						  source
						  geometry {
							type
							coordinates
						  }
						}
					  }
					  ... on SiretChamp {
						etablissement {
						  siret
						  naf
						  entreprise {
							siren
							dateCreation
						  }
						  association {
							rna
							dateCreation
						  }
						}
					  }
					}
				}
		  }";
		return $request;
	}

	// Requête en Graphql
	function echoTest() {
		$procedureId = '28277';
		$dossierId = '1575849';

		// graphql
		$request = $this->_getDemarcheInfos($procedureId);
		$this->lancerRequeteCurl($request);
		return $this->responseMessageStr;
	}

	/**
	 * Fonction permettant de remonter les informations d'un dossier d'une procédure
	 * @param $procedureId
	 * @param $dossierId
	 * @return mixed
	 */
	public function getDemarche($procedureId) {
		// graphql
		$request = $this->_getDemarcheInfos($procedureId);
		$this->lancerRequeteCurl($request);
		return $this->responseMessageStr;
	}



	/**
	 * Fonction permettant de remonter les informations de la procédure
	 * @param $procedureId
	 */
	public function getAllDemarcheInfos($procedureId) {
		// graphql
		$request = $this->_getAllDemarcheInfos($procedureId);
		$this->lancerRequeteCurl($request);
		return $this->responseMessageStr;
	}

	/**
	 * Fonction permettant de remonter les informations d'un dossier d'une procédure
	 * @param $procedureId
	 * @param $dossierId
	 * @return mixed
	 */
	public function getDossier($dossierId) {
		// graphql
		$request = $this->_getDossierInfos($dossierId);
		$this->lancerRequeteCurl($request);
		return $this->responseMessageStr;
	}


	function makeCurlFile($file){
		$mime = mime_content_type($file);
		$info = pathinfo($file);
		$name = $info['basename'];
		$output = new CURLFile($file, $mime, $name);
		return $output;
	}
}

