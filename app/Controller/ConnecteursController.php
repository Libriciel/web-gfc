<?php

App::uses('File', 'Utility');


class ConnecteursController extends AppController {

    const NS_CMIS_RA = "http://docs.oasis-open.org/ns/cmis/restatom/200908/";
    const NS_CMIS = "http://docs.oasis-open.org/ns/cmis/core/200908/";

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Connecteurs';
    public $components = array('Iparapheur', 'Cmis', 'Localeo', 'Pastell', 'Ds', 'LsMessage', 'Directmairie', 'NewPastell');

    /**
     * Gestion des connecteurs interface graphique)
     *
     * @logical-group Connecteurs
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'gestionConnecteurs', true)
        ));
    }


    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('sync');
    }
    /**
     * Récupération de la liste des connecteurs
     *
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function getConnecteurs() {

        $connecteurs_tmp = $this->Connecteur->find('all', array('order' => 'Connecteur.name'));
        $connecteurs = array();
        $isGed = false;
        foreach ($connecteurs_tmp as $item) {
            $item['right_edit'] = true;
            $item['right_check'] = true;
            $item['right_sync'] = false;
            if ($item['Connecteur']['name'] == 'Annuaire LDAP') {
                $item['right_sync'] = true;
            }
            $item['right_delete'] = true;

            // On affiche une icône indiquant si le connecteur est actif ou non
			$item['active'] = false;
			$fields = $this->Connecteur->schema();
			foreach( $fields as $databaseAttr => $values ) {
				if (strpos($databaseAttr, 'use_') !== false && $databaseAttr != 'use_mailsec' ) {
					$item[$databaseAttr] = $item['Connecteur'][$databaseAttr];
				}
				if( !empty($item[$databaseAttr] ) ) {
					$item['active'] = true;
				}
			}
            $connecteurs[] = $item;
        }

        $this->set('connecteurs', $connecteurs);

        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    public function edit($id) {
        $type = array(
            'ActiveDirectory' => 'Active Directory',
            'OpenLDAP' => 'Open LDAP'
        );
        $this->set( 'type', $type);

        $this->request->data = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.id' => $id
				),
				'contain' => false
			)
        );

        if (stristr($this->request->data['Connecteur']['name'], 'Parapheur')) {
			// Configuration signature
			$protocoles = array('PASTELL' => 'Pastell', 'IPARAPHEUR' => 'i-Parapheur');
			$visibilite = array(
				'CONFIDENTIEL' => 'CONFIDENTIEL',
				'PUBLIC' => 'PUBLIC',
				'SERVICE' => 'GROUPE'
			);
			$this->set('visibilite', $visibilite);
			$usePastell = $this->Connecteur->find(
				'first', array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Pastell%',
						'Connecteur.use_pastell' => true
					),
					'contain' => false
				)
			);
			$idEntity = '';
			if( !empty($usePastell) ) {
				$idEntity = $usePastell['Connecteur']['id_entity'];
				$this->request->data['Connecteur']['signature_protocol'] = 'PASTELL';
			}
			else {
				$this->request->data['Connecteur']['signature_protocol'] = 'IPARAPHEUR';
				unset($protocoles['PASTELL']);
			}
			$this->set( 'idEntity', $idEntity);
			$this->set( 'usePastell', $usePastell);
			$this->set('protocoles', $protocoles);
            $this->render('signature');
        } else if (stristr($this->request->data['Connecteur']['name'], 'GED')) {
            $this->render('cmis');
        } else if (stristr($this->request->data['Connecteur']['name'], 'LDAP')) {
            $this->render('ldap');
        } else if (stristr($this->request->data['Connecteur']['name'], 'GRC')) {
			$this->render('grc');
		} else if (stristr($this->request->data['Connecteur']['name'], 'Démarches-Simplifiées')) {
			// Gestion par bureau
			$conn = $this->Session->read('Auth.Collectivite.conn');
			$this->loadModel('Desktopmanager');
			$this->Desktopmanager->setDataSource($conn);
			$scanDesktops = $this->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.isdispatch' => true)));
			$this->set('scanDesktops', $scanDesktops);
			$this->render('demarchessimplifiees');
		} else if (stristr($this->request->data['Connecteur']['name'], 'Direct-Mairie')) {
			// Gestion par bureau
			$conn = $this->Session->read('Auth.Collectivite.conn');
			$this->loadModel('Desktopmanager');
			$this->Desktopmanager->setDataSource($conn);
			$scanDesktops = $this->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.isdispatch' => true)));
			$this->set('scanDesktops', $scanDesktops);
			$this->render('directmairie');
        } else if (stristr($this->request->data['Connecteur']['name'], 'PASTELL')) {
			$selectedName = '';
			$id_entity = '';
			$entities = [];
			$selected = '';
			if (!empty($this->request->data['Connecteur']['id_entity'])) {
				$id_entity = $this->request->data['Connecteur']['id_entity'];
				$selected = $this->request->data['Connecteur']['id_entity'];
			}
			// Pour récupérer le nom de la collectivité dans le select
			if( !empty($this->request->data['Connecteur']['json_pastell']) ) {
				$jsonPastell = json_decode($this->request->data['Connecteur']['json_pastell']);
				foreach($jsonPastell as $values) {
					$entities[$values->id] = $values->text;
					$selectedName = $values->text;
				}
			}
			$this->set('entities', $entities );
			$this->set('id_entity', $id_entity);
			$this->set('selectedName', $selectedName);
			$this->set('selected', $selected);

			$this->render('pastell');
        } else if (stristr($this->request->data['Connecteur']['name'], 'LsMessage')) {
			$this->render('sms');
		} else {
            $this->Session->setFlash('Ce connecteur n\'est pas valide', 'growl', array('type' => 'erreur'));
            return $this->redirect(array('action' => 'index'));
        }
    }

    public function _replaceValue($content, $param, $new_value) {
        if (is_bool(Configure::read($param)))
            $valeur = Configure::read($param) === true ? 'true' : 'false';
        else
            $valeur = Configure::read($param);

        $host_b = "Configure::write('$param', '" . $valeur . "');";
        $host_a = "Configure::write('$param', '$new_value');";
        $return = str_replace($host_b, $host_a, $content, $count);
        if ($count === 0) {
            $host_b = "Configure::write('$param', " . $valeur . ");";
            $host_a = "Configure::write('$param', $new_value);";

            $return = str_replace($host_b, $host_a, $content, $count);
        }

        return $return;
    }

    public function makeconf($type) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
        $this->autoRender = false;
        $this->Jsonmsg->init();
        switch ($type) {
            case 'signature' :
                $this->Jsonmsg->init();
                if (!empty($this->request->data)) {
                    $this->Connecteur->begin();
                    $this->Connecteur->create();
                    if (!empty($this->request->params['form']['myfile']['tmp_name'])) {
                        $certs = array();
                        $this->request->data['Connecteur']['nom_cert'] = $this->request->params['form']['myfile']['name'];
                        $pkcs12 = file_get_contents($this->request->params['form']['myfile']['tmp_name']);
                        if (openssl_pkcs12_read($pkcs12, $certs, $this->data['Connecteur']['certpwd'])) {
                            $this->request->data['Connecteur']['cacert'] = $certs['pkey'] . $certs['cert'];
                            $this->request->data['Connecteur']['clientcert'] = $certs['extracerts'][0];
                        } else
                            $this->Jsonmsg->error('Le mot de passe du certificat est erroné');
                    }
                    else {
                        unset($this->request->params['form']['myfile']);
                    }

                    if ($this->Connecteur->save($this->request->data)) {
                        $this->Connecteur->commit();
                        $this->Jsonmsg->valid();
                    } else {
                        $this->Jsonmsg->error('Erreur lors de l\'enregistrement');
                        $this->Connecteur->rollback();
                    }
                    $this->Jsonmsg->send();
                }
                break;
			case 'ldap':
				$this->Jsonmsg->init();
				if (!empty($this->request->data)) {
					$this->Connecteur->begin();
//					$this->Connecteur->create();
					if (!empty($this->request->params['form']['myfile']['tmp_name'])) {
						$this->request->data['Connecteur']['ldaps_nom_cert'] = $this->request->params['form']['myfile']['name'];
						$crt = file_get_contents($this->request->params['form']['myfile']['tmp_name']);
						$this->request->data['Connecteur']['ldaps_cert'] = $crt;

						$fileConfigCrt = new Folder(APP . DS . 'Config' . DS . 'cert_ldaps' . DS . 'ldaps_'.$conn.'_cert.crt', true, 0777);
						$file = APP . DS . 'Config' . DS . 'cert_ldaps' . DS . 'ldaps_'.$conn.'_cert.crt';
						file_put_contents( $file, $crt );
					}
					else {
						unset($this->request->params['form']['myfile']);
					}

					if ($this->Connecteur->save($this->request->data)) {
						$this->Connecteur->commit();
						$this->Jsonmsg->valid();
					} else {
						$this->Jsonmsg->error('Erreur lors de l\'enregistrement');
						$this->Connecteur->rollback();
					}
					$this->Jsonmsg->send();
				}
				break;
			case 'grc':
			case 'cmis':
			case 'demarchessimplifiees':
			case 'directmairie':
			case 'sms':
                $this->Jsonmsg->init();
                if (!empty($this->request->data)) {
                    $this->Connecteur->begin();
                    $this->Connecteur->create();
                    if ($this->Connecteur->save($this->request->data)) {
                        $this->Connecteur->commit();
                        $this->Jsonmsg->valid();
                    } else {
                        $this->Jsonmsg->error('Erreur lors de l\'enregistrement');
                        $this->Connecteur->rollback();
                    }
                    $this->Jsonmsg->send();
                }
                break;
			case 'pastell' :
                $this->Jsonmsg->init();

                if (!empty($this->request->data)) {
                    $this->Connecteur->begin();
                    $this->Connecteur->create();

                    if (!empty($this->request->params['form']['myfile']['tmp_name'])) {
                        $certs = array();
                        $this->request->data['Connecteur']['nom_cert'] = $this->request->params['form']['myfile']['name'];
                        $pkcs12 = file_get_contents($this->request->params['form']['myfile']['tmp_name']);
                        if (openssl_pkcs12_read($pkcs12, $certs, $this->data['Connecteur']['certpwd'])) {
                            $this->request->data['Connecteur']['cacert'] = $certs['pkey'] . $certs['cert'];
                            $this->request->data['Connecteur']['clientcert'] = $certs['extracerts'][0];
                        } else
                            $this->Jsonmsg->error('Le mot de passe du certificat est erroné');
                    }
                    else {
                        unset($this->request->params['form']['myfile']);
                    }

                    if( !empty($this->request->data['Connecteur']['json_pastell']) ) {
                        $jsonPastell = $this->request->data['Connecteur']['json_pastell'];
                    }
                    if ($this->Connecteur->save($this->request->data)) {
                        $this->Connecteur->commit();

						$useSignature = false;
						$protocole = "'".'IPARAPHEUR'."'";
						if( $this->request->data['Connecteur']['use_pastell'] == 'true' ) {
							$useSignature = true;
							$protocole = "'".'PASTELL'."'";
						}
						$this->Connecteur->updateAll(
							array(
								'Connecteur.use_signature' => $useSignature,
								'Connecteur.signature_protocol' => $protocole
							),
							array(
								'Connecteur.name ILIKE' => '%Parapheur%'
							)
						);
                        $this->Jsonmsg->valid();

                    } else {
                        $this->Jsonmsg->error('Erreur lors de l\'enregistrement');
                        $this->Connecteur->rollback();
                    }
                    $this->Jsonmsg->send();
                }
                break;
            case 'all' :
                $content = $this->data['Connecteur']['all'];
                break;
            default :
                $this->Jsonmsg->error('Ce connecteur n\'est pas valide');
        }

// debug($this->request->data);
// die();
        return $this->redirect(array('action' => 'index'));
    }

    public function getCircuitsParapheur() {
        $this->Jsonmsg->init();
        $time_start = microtime(true);
        $circuits = '';
		$parapheur = $this->Connecteur->find(
				'first', array(
			'conditions' => array(
				'Connecteur.name ILIKE' => '%Parapheur%',
				'Connecteur.use_signature'=> true,
				'Connecteur.signature_protocol'=> 'IPARAPHEUR'
			),
			'contain' => false
				)
		);
		$this->set('parapheur', $parapheur );
        if( !empty($parapheur) ) {
			// Passage
			App::uses('Signature', 'Lib');
			$Signature = new Signature();
			$circuits = $Signature->listCircuits($parapheur);
			$time_end = microtime(true);

			if ($circuits === 'Connexion impossible') {
				$messageError = "Impossible de se connecter à l'hôte renseigné";
				$this->Jsonmsg->error($messageError);
				$this->Jsonmsg->send();
				return;
			}
			if (!empty($circuits["soustype"])) {
				// On stocke la liste des soustypes récupérée depuis l'i-Parapheur car lorsqu'ils changent
				// les identifiants associés aux circuits/soustypes changent également.
				$conn = $this->Session->read('Auth.Collectivite.conn');
				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn, true, 0777);
				$file = WORKSPACE_PATH . DS . $conn . DS . "get_liste_soustype_" . date('Y-m-d') . ".json";
				file_put_contents($file, json_encode($circuits["soustype"], JSON_FORCE_OBJECT));

				$hostparapheur = "Parapheur contacté : " . $parapheur['Connecteur']['host'];
				$time = round($time_end - $time_start, 2);
				$nbCircuits = "Réponse du i-Parapheur : " . count($circuits['soustype']) . " sous-types(s) du i-parapheur récupéré(s) en $time secondes";

				$messageSuccess = $hostparapheur . "<br />" . $nbCircuits;
				$this->Jsonmsg->valid($messageSuccess);
			} else
				$this->Jsonmsg->error('La liste de circuit du i-parapheur est vide');
			$this->Jsonmsg->send();
		}
    }


    public function check($id) {
        $this->request->data = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.id' => $id
                ),
                'contain' => false
            )
        );

        if (stristr($this->request->data['Connecteur']['name'], 'Parapheur')) {
            $parapheur = $this->request->data;

            // Configuration signature
            $this->Jsonmsg->init();
            $time_start = microtime(true);
            $circuits = '';
            if (!$parapheur['Connecteur']['use_signature']) {
                $this->Jsonmsg->valid("Utilisation du parapheur désactivée");
                $this->Jsonmsg->send();
                return;
            }

			// Passage
			App::uses('Signature', 'Lib');
			$Signature = new Signature();
			$circuits = $Signature->listCircuits($this->request->data);

			$time_end = microtime(true);
            if ($circuits === 'Connexion impossible') {
				$messageError = "Impossible de se connecter à l'hôte renseigné";
				$this->Jsonmsg->error($messageError);
                $this->Jsonmsg->send();
                return;
            }

            if (!empty($circuits["soustype"])) {
            	// On stocke la liste des soustypes récupérée depuis l'i-Parapheur car lorsqu'ils changent
				// les identifiants associés aux circuits/soustypes changent également.
				$conn = $this->Session->read('Auth.Collectivite.conn');
				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn, true, 0777);
				$file = WORKSPACE_PATH . DS . $conn . DS . "check_liste_soustype_ip.json";
				file_put_contents( $file, json_encode($circuits["soustype"], JSON_FORCE_OBJECT));

				$usePastell = $this->Connecteur->find(
					'first', array(
						'conditions' => array(
							'Connecteur.name ILIKE' => '%Pastell%',
							'Connecteur.use_pastell' => true
						),
						'contain' => false
					)
				);
				if( empty($usePastell) ) {
					$file = WORKSPACE_PATH . DS . $conn . DS . "old_liste_soustype_ip.json";
					file_put_contents($file, json_encode($circuits["soustypeold"], JSON_FORCE_OBJECT));
				}

				$hostparapheur = "Parapheur contacté : " . $parapheur['Connecteur']['host'];
				if( $parapheur['Connecteur']['signature_protocol'] == 'PASTELL' ) {
					$PastellComponent = new NewPastellComponent();
					$idCe = $PastellComponent->getIdCeByEntiteAndConnecteur($usePastell['Connecteur']['id_entity'], 'signature');
					$infos = $PastellComponent->getOneConnector($usePastell['Connecteur']['id_entity'], $idCe);
					$hostparapheur = "Parapheur contacté : " . $infos->data->data['iparapheur_wsdl'];
				}

                $time = round($time_end - $time_start, 2);
                $nbCircuits = "Réponse du i-Parapheur : " . count($circuits['soustype']) . " sous-type(s) du i-parapheur récupéré(s) en $time secondes";

                $messageSuccess = $hostparapheur . "<br />" . $nbCircuits;
                $this->Jsonmsg->valid($messageSuccess);
            } else {
				if (!empty($circuits) ) {
					$messageError = $circuits;
					$this->Jsonmsg->error($messageError);
					$this->Jsonmsg->send();
					return;
				}
				else {
					$this->Jsonmsg->error('La liste de circuit du i-parapheur est vide');
				}
			}
            $this->Jsonmsg->send();
        }
        else if (stristr($this->request->data['Connecteur']['name'], 'GED')) {
            // Connecteur CMIS/GED
            $this->Jsonmsg->init();
            $time_start = microtime(true);

            // Le connecteur est-il actif
            if (!$this->request->data['Connecteur']['use_ged']) {
                $this->Jsonmsg->error('Utilisation de la GED désactivée', 'growl', array('type' => 'info'));
                $this->Jsonmsg->send();
                return;
            }

            require_once( ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'cmis.php' );

            // Le couple login/mdp est-il le bon
            $configured = Cmis::configured();
            if (!$configured) {
                $this->Jsonmsg->error("La connexion avec la GED a échoué : couple login/mot de passe erroné.", 'growl', array('type' => 'info'));
                $this->Jsonmsg->send();
                return;
            }


            // Existence ou non du répertoire renseigné
            if ($this->request->data['Connecteur']['ged_cmis_version'] == '1.1') {
                $cmis = new CmisComponent;
                $cmis->CmisComponent_Service();

                $folderUsed = $cmis->client->getObjectByPath($this->request->data['Connecteur']['ged_repo']);

                if ($folderUsed == 'emptyRepo') {
                    $this->Jsonmsg->error("La connexion avec la GED a échoué : le dossier " . $this->request->data['Connecteur']['ged_repo'] . " n'a pas été trouvé</ul>", 'growl', array('type' => 'info'));
                    $this->Jsonmsg->send();
                    return;
                } else {
                    $messageSuccess = "La connexion est réussie - web-GFC a récupéré les informations suivantes :<ul>";
                    foreach ($this->getFolderRetrieveInfo11() as $properties) {
                        $messageSuccess .= "<li> $properties  : " . $folderUsed->properties['cmis:' . $properties] . "</li>";
                    }
                    $messageSuccess .="</ul>";
                }

                $this->Jsonmsg->valid($messageSuccess);
                $this->Jsonmsg->send();
            } else {
                $folderUsed = $this->testObject($this->request->data['Connecteur']['ged_repo']);
                if ($folderUsed === false) {
                    $this->Jsonmsg->error("La connexion avec la GED a échoué : le dossier " . $this->request->data['Connecteur']['ged_repo'] . " n'a pas été trouvé</ul>", 'growl', array('type' => 'info'));
                    $this->Jsonmsg->send();
                    return;
                }


                // si tout ok
                $messageSuccess = "La connexion est réussie - web-GFC a récupéré les informations suivantes :<ul>";
                foreach ($this->getFolderRetrieveInfo() as $repoInfo) {
                    $messageSuccess .= "<li> $repoInfo  : " . $folderUsed[$repoInfo] . "</li>";
                }
                $messageSuccess .="</ul>";


                $info = $this->getRepositoryInfo();

                $this->Jsonmsg->valid($messageSuccess);
                $this->Jsonmsg->send();
            }
        }
        else if (stristr($this->request->data['Connecteur']['name'], 'Annuaire LDAP')) {
            $this->loadModel('User');
             // Connecteur CMIS/GED
            $this->Jsonmsg->init();
            $time_start = microtime(true);
            $ldap = $this->request->data;
            // Le connecteur est-il actif
            if (!$ldap['Connecteur']['use_ldap']) {
                $this->Jsonmsg->error('Utilisation du LDAP désactivé', 'growl', array('type' => 'info'));
                $this->Jsonmsg->send();
                return;
            }


            if($ldap['Connecteur']['ldap_port'] === '636') {
				$conn = $this->Session->read('Auth.Collectivite.conn');
				// édition du fichier /etc/ldap/ldap.conf et modification de la valeur TLS_CACERT par celle du nouveau /var/www/html/gitlab/webgfc/webgfc/app/Config/cert_ldaps/ls-bundle.crt
				$this->clientcertdir = APP . DS . 'Config' . DS . 'cert_ldaps' ;
				$this->clientcert = APP . DS . 'Config' . DS . 'cert_ldaps' . DS . 'ldaps_'.$conn.'_cert.crt'; // si le multi-co marche
				ldap_set_option($this->database, LDAP_OPT_X_TLS_REQUIRE_CERT, LDAP_OPT_X_TLS_HARD);
				ldap_set_option($this->database, LDAP_OPT_X_TLS_CACERTDIR, $this->clientcertdir);
				ldap_set_option($this->database, LDAP_OPT_X_TLS_CACERTFILE, $this->clientcert); // required for AD

			}

            // On véreifie la connexion à l'annuaire LDAP défini
            $conn = ldap_connect($ldap['Connecteur']['ldap_host'], $ldap['Connecteur']['ldap_port']) or die("connexion impossible au serveur LDAP");
            ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($conn, LDAP_OPT_REFERRALS, 0); // required for AD
            $bind = @ldap_bind($conn, $ldap['Connecteur']['ldap_login'] . $ldap['Connecteur']['ldap_account_suffix'], $ldap['Connecteur']['ldap_password']);
            if( $bind ) {
                $message = "La connexion à l'annuaire LDAP a réussi";
                $this->Jsonmsg->valid($message);
            }
            else {
                $message = "Echec de la connexion à l'annuaire LDAP";
                $this->Jsonmsg->error( $message);
            }
            $this->Jsonmsg->send();
        }
        else if (stristr($this->request->data['Connecteur']['name'], 'GRC')) {
//            $this->autoRender = false;
            $this->Jsonmsg->init();
//            $time_start = microtime(true);
            $grc = $this->request->data;
            // Le connecteur est-il actif ?
            if (!$grc['Connecteur']['use_grc']) {
                $this->Jsonmsg->error('Utilisation de la GRC désactivée', 'growl', array('type' => 'info'));
                $this->Jsonmsg->send();
                return;
            }

            // On vérifie la connexion à la GRC
            $GRCwebservice = new LocaleoComponent();
            $echoTest = $GRCwebservice->echoTest();
            if( !empty($echoTest) ) {
                if(isset( $echoTest->err_msg)) {
                    $message = "La connexion à la GRC a réussi. <br /> retour de la GRC : ".$echoTest->err_msg;
                }
                else {
                    $message = "La connexion à la GRC a réussi. <br /> Retour de la GRC : ".$echoTest->statut->label;
                }
                $this->Jsonmsg->valid($message);
            }
            else {
                $message = "Echec de la connexion à la GRC";
                $this->Jsonmsg->error($message);
            }
            $this->Jsonmsg->send();
        }
        else if (stristr($this->request->data['Connecteur']['name'], 'PASTELL')) {
            $pastell = $this->request->data;

            // Configuration signature
            $this->Jsonmsg->init();
            $time_start = microtime(true);
            if (!$pastell['Connecteur']['use_pastell']) {
                $this->Jsonmsg->valid("Utilisation de PASTELL désactivée");
                $this->Jsonmsg->send();
                return;
            }

			// Nouvelle version du client PASTELL
            $version = $this->NewPastell->getVersion();
            if ( empty( $version )) {
                $this->Jsonmsg->error("Impossible de se connecter à l'hôte");
                $this->Jsonmsg->send();
                return;
            }

			if( !isset($version['version_complete'] ) ) {
				$this->Jsonmsg->error("Impossible de se connecter à l'hôte: <br/>".$version);
				$this->Jsonmsg->send();
				return;
			}
            $time_end = microtime(true);
            if (!empty($version)) {
                $hostpastell = "PASTELL contacté : " . $pastell['Connecteur']['host'];
                $time = round($time_end - $time_start, 2);

                $checkmessage = "Réponse de PASTELL en $time secondes : la version est ".$version['version_complete'];
                $this->Jsonmsg->valid($checkmessage);
            }
            $this->Jsonmsg->send();
        }
		else if (stristr($this->request->data['Connecteur']['name'], 'Démarches-Simplifiées')) {

			$this->Jsonmsg->init();
			$ds = $this->request->data;
			// Le connecteur est-il actif ?
			if (!$ds['Connecteur']['use_ds']) {
				$this->Jsonmsg->error('Utilisation de Démarches-Simplifiées', 'growl', array('type' => 'info'));
				$this->Jsonmsg->send();
				return;
			}

			// On vérifie la connexion à la GRC
			$DSwebservice = new DSComponent();
			$echoTest = $DSwebservice->echoTest();
			if( !empty($echoTest) ) {
				if(isset( $echoTest->err_msg)) {
					$message = "La connexion à Démarches-Simplifiées a réussi. <br /> retour de DS : ".$echoTest->err_msg;
				}
				else {
					$message = "La connexion à Démarches-Simplifiées a réussi. <br /> Retour de DS : <br />Identifiant de la démarche = ".$echoTest->data->demarche->id;
				}
				$this->Jsonmsg->valid($message);
			}
			else {
				$message = "Echec de la connexion à Démarches-Simplifiées";
				$this->Jsonmsg->error($message);
			}
			$this->Jsonmsg->send();
		}
		else if (stristr($this->request->data['Connecteur']['name'], 'LsMessage')) {
			$this->Jsonmsg->init();
			$sms = $this->request->data;
			// Le connecteur est-il actif ?
			if (!$sms['Connecteur']['use_sms']) {
				$this->Jsonmsg->error('Utilisation de LsMessage', 'growl', array('type' => 'info'));
				$this->Jsonmsg->send();
				return;
			}

			// On vérifie la connexion à LSMessage
			$echoTest = $this->LsMessage->info();
			if( !empty($echoTest) ) {
				if(isset( $echoTest['balance'] ) && !empty($echoTest['balance'])) {
					if( $echoTest['balance'] == 0 ) {
						$message = "La connexion à LsMessage a réussi: <br />LsMessage retourne que vous n'avez plus de crédits";
						$this->Jsonmsg->valid($message);
					}
					else {
						$message = "La connexion à LsMessage a réussi: <br />Il vous reste ".$echoTest['balance']." SMS";
						$this->Jsonmsg->valid($message);
					}
				}
				else {
					$message = "Echec de la connexion à LsMessage <br/> LsMessage a retourné : $echoTest";
					$this->Jsonmsg->error($message);
				}
			}
			else {
				$message = "Echec de la connexion à LsMessage";
				$this->Jsonmsg->error($message);
			}
			$this->Jsonmsg->send();
		}
		else if (stristr($this->request->data['Connecteur']['name'], 'Direct-Mairie')) {

			$this->Jsonmsg->init();
			$dm = $this->request->data;
			// Le connecteur est-il actif ?
			if (!$dm['Connecteur']['use_dm']) {
				$this->Jsonmsg->error('Utilisation de Direct-Mairie', 'growl', array('type' => 'info'));
				$this->Jsonmsg->send();
				return;
			}

			// On vérifie la connexion à la GRC
			$DMwebservice = new DirectmairieComponent();
			$echoTest = $DMwebservice->echoTest();
			if( isset($echoTest->code) && $echoTest->code == '200' ) {
				$message = "La connexion à Direct-Mairie a réussi.<br />";
				// On récupère le token lros de la première vérification du connecteur
				if( empty($this->request->data['Connecteur']['dm_apikey']) ) {
					$token = $DMwebservice->getToken($this->request->data['Connecteur']['login'], $this->request->data['Connecteur']['pwd']);
					if (!empty($token->body)) {
						$valueOfToken = json_decode($token->body);
						$tokenToAdd = $valueOfToken->token;
						$this->Connecteur->id = $this->request->data['Connecteur']['id'];
						$this->Connecteur->begin();
						$tokenAdded = $this->Connecteur->saveField('dm_apikey', $tokenToAdd);
						if ($tokenAdded) {
							$message .= 'Le token est ' . $tokenToAdd;
							$this->Jsonmsg->valid($message);
							$this->Connecteur->commit();
						} else {
							$this->Connecteur->rollback();
						}
					}
				}
				$this->Jsonmsg->valid($message);
			}
			else {
				$message = "Echec de la connexion à Direct-Mairie. <br /> Le connecteur retourne : ".$echoTest;
				$this->Jsonmsg->error($message);
			}
			$this->Jsonmsg->send();
		}
        else {
            $this->Session->setFlash('Ce connecteur n\'est pas valide', 'growl', array('type' => 'erreur'));
            return $this->redirect(array('action' => 'index'));
        }
    }

    public function getRepositoryInfo() {
        $cmis = $this->Connecteur->find(
                'first', array(
            'conditions' => array(
                'Connecteur.name ILIKE' => '%GED%'
            ),
            'contain' => false
                )
        );
        $xmldata = $this->get($cmis['Connecteur']['ged_url']);

        if (!$xmldata) {
            return false;
        }

        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($xmldata);

        if (!$xml) {
            $errors = libxml_get_errors();
            $this->lastError = "Erreur XML : " . $errors[0]->message;
            return false;
        }

        $repInfo = $xml->workspace->children(self::NS_CMIS_RA)->repositoryInfo;

        $result = array();
        foreach ($this->getRepositoryRetrieveInfo() as $infoName) {
            $result[$infoName] = strval($repInfo->children(self::NS_CMIS)->$infoName);
        }

        $uriTemplate = $xml->workspace->children(self::NS_CMIS_RA)->uritemplate;

        foreach ($uriTemplate as $template) {
            $type = strval($template->children(self::NS_CMIS_RA)->type);
            $result['template'][$type] = strval($template->children(self::NS_CMIS_RA)->template);
        }

        return $result;
    }

    private function get($url, $content = false) {
        $cmis = $this->Connecteur->find(
                'first', array(
            'conditions' => array(
                'Connecteur.name ILIKE' => '%GED%'
            ),
            'contain' => false
                )
        );
        $session = curl_init($url);

        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($session, CURLOPT_USERPWD, Configure::read('CMIS_LOGIN') . ":" . Configure::read('CMIS_PWD'));
        curl_setopt($session, CURLOPT_USERPWD, $cmis['Connecteur']['ged_login'] . ":" . $cmis['Connecteur']['ged_passwd']);

        if ($content) {
            curl_setopt($session, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($session, CURLOPT_POSTFIELDS, $content);
            curl_setopt($session, CURLOPT_HTTPHEADER, array("Content-Type: application/atom+xml;type=entry"));
        } else {
            curl_setopt($session, CURLOPT_CUSTOMREQUEST, "GET");
        }
        $result = curl_exec($session);
        $codeResponse = curl_getinfo($session, CURLINFO_HTTP_CODE);

        if (!in_array($codeResponse, array('200', '201'))) {
            $this->lastError = curl_error($session);
            if (!$this->lastError) {
                $this->lastError = "Erreur $codeResponse (la GED a retourné : $result)";
            }
            return false;
        }

        return $result;
    }

    public function getRepositoryRetrieveInfo() {
        return array('repositoryId', 'repositoryName', 'repositoryDescription', 'vendorName', 'productName', 'productVersion', 'rootFolderId');
    }

    public function getRootFolder() {
        $cmis = $this->Connecteur->find(
                'first', array(
            'conditions' => array(
                'Connecteur.name ILIKE' => '%GED%'
            ),
            'contain' => false
                )
        );
//		return Configure::read('CMIS_REPO');
        return $cmis['Connecteur']['ged_url'];
    }

    public function testObject($repoPath) {
        return $this->getObjectByPath($repoPath);
    }

    public function getObjectByPath($path) {

        $repositoryInfo = $this->getRepositoryInfo();

        if (!$repositoryInfo) {
            return false;
        }

        $url_template = $repositoryInfo['template']['objectbypath'];

        $url = str_replace("{path}", $path, $url_template);
        $url = preg_replace("/{[a-zA-Z0-9_]+}/", "", $url);

        $xmldata = $this->get($url);

        if (!$xmldata) {
            return false;
        }

        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($xmldata);
        if (!$xml) {
            $errors = libxml_get_errors();
            $this->lastError = "Erreur XML : " . $errors[0]->message;
            return false;
        }


        $result = array('author' => strval($xml->author->name));
        foreach ($this->getFolderRetrieveInfo() as $infoName) {
            $result[$infoName] = strval($xml->$infoName);
        }

        foreach ($xml->link as $link) {
            if (!isset($result['link'][strval($link['rel'])])) {
                $result['link'][strval($link['rel'])] = strval($link['href']);
            }
        }

        return $result;
    }

    public function getFolderRetrieveInfo() {
        return array('content', 'id', 'summary', 'title', 'published', 'updated');
    }

    /**
     * Fonction de suppression d'un connecteur
     * @param type $id
     */
    public function delete($id) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        if ($this->Connecteur->delete($id)) {
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        }
        $this->Jsonmsg->send();
    }

    /**
     * Fonction remontant les infos en version 1.1 de CMIS
     * @return type
     */
    public function getFolderRetrieveInfo11() {
        return array('path', 'lastModifiedBy', 'name', 'creationDate', 'description', 'createdBy');
    }


    /**
     * Fonctin permettant la synchronisation LDAP manuelle
     * @param type $id
     * @return type
     */
    public function sync($id) {
        $this->request->data = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.id' => $id
                ),
                'contain' => false
            )
        );
		$conn = $this->Session->read('Auth.Collectivite.conn');
        if (stristr($this->request->data['Connecteur']['name'], 'Annuaire LDAP')) {
            $this->loadModel('User');
            $this->Jsonmsg->init();
            $time_start = microtime(true);
            $ldap = $this->request->data;

            // Le connecteur est-il actif
            if (!$ldap['Connecteur']['use_ldap']) {
                $this->Jsonmsg->error('Utilisation du LDAP désactivé', 'growl', array('type' => 'info'));
                $this->Jsonmsg->send();
                return;
            }

            // On vérifie la connexion à l'annuaire LDAP défini
            // si tout ok
            $messageSuccess = "La connexion est réussie - web-GFC a récupéré les informations suivantes :<br /><ul>";
            $cmdUser = APP . 'Console/cake ldap_manager user_update -c '.$conn;
            $commandShell = shell_exec($cmdUser);
            $userIsCreated = strpos( $commandShell, 'a été créé'  );
            $userIsUpdated = strpos( $commandShell, 'a été mis à jour'  );
            $userAlreadyExists = strpos( $commandShell, 'est déjà présent'  );
            if( Configure::read('debug') == 2 ) {
                if(  $userIsCreated !== false ) {
                    $messageSuccess .=  substr($commandShell, 0, strpos($commandShell, '1. search'));
                    $messageSuccess = str_replace( " a été créé", " a été créé " , nl2br( $messageSuccess ) );
                }
                else if(  $userIsUpdated !== false ) {
                    $messageSuccess .=  substr($commandShell, 0, strpos($commandShell, '1. search'));
                    $messageSuccess = str_replace( " a été mis à jour", " a été mis à jour " , nl2br( $messageSuccess ) );
                }
                else if(  $userAlreadyExists !== false ) {
                    $messageSuccess .=  substr($commandShell, 0, strpos($commandShell, '1. search'));
                    $messageSuccess = str_replace( " existe déjà", " existe déjà" , nl2br( $messageSuccess ) );
                }
            }
            else {
                $messageSuccess .= $commandShell;
            }

            $this->set( compact( 'messageSuccess' ) );

//            $this->Jsonmsg->send();
            $this->render('sync');
        }
        else {
            $this->Session->setFlash('Ce connecteur n\'est pas valide', 'growl', array('type' => 'erreur'));
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * Retourne la liste des entités Pastell
     * @return [type] [description]
     * @since 5.1.0
     */
    public function listEntities()
    {
        $conn = $this->Session->read('Auth.Collectivite.conn');
        $this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = $conn;

        $this->autoRender = false;
        try {
            $entities = $this->NewPastell->listEntities();
			$this->response->type('json', 'text/x-json');
			$this->RequestHandler->respondAs('json');
			if( is_array($entities) ) {
				$this->response->body(json_encode($entities));
			}
			else {
				$message = json_decode($entities, true);
				if( !isset($message['error-message']) && empty($message['error-message'])) {
					$message['error-message'] = $entities;
				}
				$errorMessage = [
					'error' => $message['error-message']
				];
				$this->response->body(json_encode($errorMessage) );

			}
        } catch (CakeException $e) {
            $this->response->statusCode(404);
        }

        return $this->response;
    }

	/**
	 * Retourne la liste des entités Pastell
	 * @return [type] [description]
	 * @since 5.1.0
	 */
	public function listTypesIParapheur( $idEntite )
	{
		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = $conn;

		$this->autoRender = false;
		try {
//			$types = $this->Pastell->listTypesIParapheur($idEntite);
			$types = $this->NewPastell->listTypesIParapheur($idEntite);

			$typesIP = $types['data']['iparapheur_type'];

			$this->	response->type('json', 'text/x-json');
			$this->RequestHandler->respondAs('json');
			$this->response->body(json_encode($typesIP));
		} catch (Exception $e) {
			$this->response->statusCode(404);
		}

		return $this->response;
	}

}
