<?php

App::uses('ShellDispatcher', 'Console');

App::uses('Folder', 'Utility');
/**
 * Collectivités
 *
 * Collectivites controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class CollectivitesController extends AppController {

    /**
     *
     * Controller name
     *
     * @var array
     */
    public $name = 'Collectivites';

    /**
     *
     * Controller uses
     *
     * @var array
     */
    public $uses = array('Collectivite');

    /**
     * Fonction de rappel beforeFilter
     *
     * @description  Paramétrage spécifique du composant Auth. Si on est connecté en tant que superadmin, on peut accèder à toutes les pages sans utiliser les Acl de CakePHP.
     *
     * @access public
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        if ($this->Auth->User('id') && Configure::read('conn') == "default") {
            $this->Auth->allow('*');
        }
    }

    /**
     * Gestion des collectivités (interface graphique)
     *
     * @logical-group Collectivité
     * @user-profile Superadmin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement">' . __d('menu', 'Administration') . '</a>',
            __d('menu', 'gestionCollectivites')));
    }

    /**
     * Récupération de la liste des collectivités (ajax)
     *
     * @logical-group Collectivité
     * @user-profile Superadmin
     *
     * @access public
     * @return void
     */
    public function getCollectivites() {
        $collectivites_tmp = $this->Collectivite->find("all", array('order' => 'Collectivite.name'));
        $collectivites = array();
        foreach ($collectivites_tmp as $item) {

        	$conn = $item['Collectivite']['conn'];
			$this->loadModel('User');
			$this->User->setDataSource($conn);

			$admin = $this->User->find(
				'first',
				array(
					'conditions' => [
						'User.desktop_id IN (SELECT desktops.id FROM desktops WHERE profil_id= '.ADMIN_GID.' )'
					]
				)
			);
            $item['Collectivite']['adminname'] = $admin['User']['username'];

            $item['right_edit'] = true;
            $item['right_delete'] = $this->Collectivite->isDeletable($item['Collectivite']['id']);
            $collectivites[] = $item;
        }

        $this->set('collectivites', $collectivites);
    }


    /**
     * Validation d'unicité du nom d'une collectivité (création / édition)
     *
     * @param type $field
     */
    public function ajaxformvalid($field = null) {
        $models = array();
        if ($field != null) {
            $models['Collectivite'] = array($field);
        } else {
            $models[] = 'Collectivite';
        }
        $this->Ajaxformvalid->valid($models, $this->request->data);
    }

    /**
     * Récupération de la liste des connexions disponible via le fichier APP/Config/database.php
     *
     * @logical-group Collectivité
     * @logical-group Ajout d'une collectivité
     *
     * @logical-used-by Collectivités > Ajout d'une collectivité
     * @user-profile Superadmin
     *
     * @access public
     * @return void
     */
    public function getConnList() {
        $conns = ConnectionManager::enumConnectionObjects();
        unset($conns['default']);

        //suppression des connexion dèjà paramétrées pour une collectivite
        $listCollectiviteConn = $this->Collectivite->find('list', array('fields' => array('Collectivite.id', 'Collectivite.conn')));
        foreach ($listCollectiviteConn as $key => $val) {
            unset($conns[$val]);
        }

        $connsToSend = array();
        foreach (array_keys($conns) as $conn) {
            $connsToSend[$conn] = $conn;
        }

        $this->Jsonmsg->sendJson($connsToSend);
    }

    /**
     * Ajout d'une collectivité
     * 	- informations
     * 	- paramétrages
     *  - administrateur de la collectivité
     *  - création des tables dans la base de données
     *  - ajout des données de base
     *  - création des séquences / compteurs pour les dossiers et affaires
     *
     * @logical-group Collectivité
     * @user-profile Superadmin
     *
     * @access public
     * @return void
     */
    public function add() {
        //recuperation de la liste des connexions existantes dans le fichier database.php
        $conns = ConnectionManager::enumConnectionObjects();
        if (!empty($this->request->data)) {
            $valid = array(
                'collectivite' => false,
                'import' => false,
                'admin' => false,
                'success' => false,
                'msg' => __d('collectivite', 'Collectivite.add.error'),
                'class' => 'alert-danger'
            );

// 			$this->Collectivite->begin();
            $this->Jsonmsg->init();


            //creation de la collectivite dans la base admin
            $this->Collectivite->create();

            try {
                // Création de la BDD collectivité s'appuyant sur le "modèle" préalablement créé
                $this->Collectivite->query('CREATE DATABASE ' . $conns[$this->request->data['Collectivite']['conn']]['database'] . ' TEMPLATE ' . $conns['template']['database'] . ';');
                $this->Collectivite->begin();

                //on force la collectivité à être active par défaut
                $this->request->data['Collectivite']['active'] = true;
                if( !empty($this->request->data['Collectivite']['telephone']) ) {
                    $this->request->data['Collectivite']['telephone'] = preg_replace('/\s+/', '', $this->request->data['Collectivite']['telephone'] );
                }


                $colls = $this->Collectivite->find('all', array('contain' => false, 'recursive' => -1));
                $connexions = array();
                foreach( $colls as $coll) {
                    $connexions[] = $coll['Collectivite']['login_suffix'];
                }

//$this->log( in_array( $this->request->data['Collectivite']['login_suffix'], $connexions ), LOG_ERROR );
                if( in_array( $this->request->data['Collectivite']['login_suffix'], $connexions ) ) {
                    $valid['collectivite'] = false;
                    $valid['import'] = false;
                    $valid['cpt'] = false;
                    $valid['valid'] = false;
                }
//                $this->request->data['Collectivite']['telephone'] = trim($this->request->data['Collectivite']['telephone'] );

                if ($this->Collectivite->save($this->request->data)) {
                    $valid['collectivite'] = true;
                    $valid['import'] = true;
                    $valid['cpt'] = true;
                    //creation de l'administrateur dans la base collectivite
                    $this->loadModel('Desktop');
                    $this->Desktop->setDataSource($this->request->data['Collectivite']['conn']);
                    $this->loadModel('User');
                    $this->User->setDataSource($this->request->data['Collectivite']['conn']);
                    $this->loadModel('Aco');
                    $this->Aco->setDataSource($this->request->data['Collectivite']['conn']);
                    $this->loadModel('Aro');
                    $this->Aro->setDataSource($this->request->data['Collectivite']['conn']);
                    $this->loadModel('Permission');
                    $this->Permission->setDataSource($this->request->data['Collectivite']['conn']);
                    $this->User->begin();

                    $desktop_admin = $this->Desktop->create();
                    $desktop_admin['Desktop']['name'] = 'Profil administrateur';
                    $desktop_admin['Desktop']['profil_id'] = ADMIN_GID;

                    if ($this->Desktop->save($desktop_admin)) {

                        $this->loadModel('Desktopmanager');
                        $this->Desktopmanager->setDataSource($this->request->data['Collectivite']['conn']);
                        $bureau_admin = $this->Desktopmanager->create();
                        $bureau_admin['Desktopmanager']['name'] = 'Bureau '.$desktop_admin['Desktop']['name'];
                        $bureau_admin['Desktopmanager']['active'] = true;
                        $bureau_admin['Desktopmanager']['isautocreated'] = true;

                        if($this->Desktopmanager->save($bureau_admin)) {
                            $this->loadModel('DesktopDesktopmanager');
                            $this->DesktopDesktopmanager->setDataSource($this->request->data['Collectivite']['conn']);
                            $desktopDesktopmanager = $this->DesktopDesktopmanager->create();
                            $desktopDesktopmanager['DesktopDesktopmanager']['desktop_id'] = $this->Desktop->id;
                            $desktopDesktopmanager['DesktopDesktopmanager']['desktopmanager_id'] = $this->Desktopmanager->id;
                            $this->DesktopDesktopmanager->save($desktopDesktopmanager);
                        }


                        $user = array('User' => $this->request->data['User']);
                        $user['User']['desktop_id'] = $this->Desktop->id;
                        $user['User']['password'] = Security::hash($this->request->data['User']['password'], 'sha256', true);
                        $this->User->create($user);
                        if ($this->User->save()) {
                            $valid['admin'] = true;
                            $valid['success'] = true;
                        }

                    }

                    // A al création d'une nouvelle collectivité, on regarde si l'annuaire LDAP doit être paramétré
                    if( !empty( $this->request->data['Connecteur'] ) ) {
                        $this->loadModel('Connecteur');
                        $this->Connecteur->setDataSource($this->request->data['Collectivite']['conn']);
                        $this->Connecteur = ClassRegistry::init('Connecteur');
                        $annuaireLdap = $this->Connecteur->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Connecteur.name ILIKE' => '%Annuaire LDAP%'
                                ),
                                'contain' => false
                            )
                        );
                        if( !empty($annuaireLdap) ) {
							if (!empty($this->request->params['form']['myfile']['tmp_name'])) {
								$this->request->data['Connecteur']['ldaps_nom_cert'] = $this->request->params['form']['myfile']['name'];
								$crt = file_get_contents($this->request->params['form']['myfile']['tmp_name']);
								$this->request->data['Connecteur']['ldaps_cert'] = $crt;
							}
							else {
								unset($this->request->params['form']['myfile']);
							}
                            $connecteur = $this->request->data['Connecteur'] + $annuaireLdap['Connecteur'];
                            $this->Connecteur->save($connecteur);
                        }
                    }

					// Partie pour la politique de confidentialité
//					if(isset($this->request->data['Rgpd']) && !empty($this->request->data['Rgpd']) ) {
//						$this->loadModel('Rgpd');
//						$this->Rgpd->useDbConfig = $this->request->data['Collectivite']['conn'];
//						$rgpd = $this->Rgpd->create();
//						$this->request->data['Rgpd']['collectivite_id'] = $this->Collectivite->id;
//						$this->request->data['Rgpd']['conn'] = $this->request->data['Collectivite']['conn'];
////						$rgpd['Rgpd']['nomhebergeur'] =$this->request->data['Collectivite']['nomhebrgeur'];
////						$rgpd['Rgpd']['adressecompletehebergeur'] =$this->request->data['Collectivite']['adressecompletehebergeur'];
////						$rgpd['Rgpd']['active'] = true;
//						if($this->Rgpd->save($this->request->data['Rgpd'])) {
//							$valid['rgpd'] = true;
//						}
//					}

                    //creation des compteurs dossiers / affaires dans la base collectivite
                    $this->loadModel('Compteur');
                    $this->Compteur->useDbConfig = $this->request->data['Collectivite']['conn'];
                    $this->loadModel('Sequence');
                    $this->Sequence->useDbConfig = $this->request->data['Collectivite']['conn'];
                    //sequence affaire
                    $sequence_affaire = $this->Sequence->create();
                    $sequence_affaire['Sequence']['nom'] = 'sequence_affaire';
                    $sequence_affaire['Sequence']['commentaire'] = 'sequence_affaire';
                    $sequence_affaire['Sequence']['num_sequence'] = 1;
                    if ($this->Sequence->save($sequence_affaire)) {
                        //compteur affaire
                        $compteur_affaire = $this->Compteur->create();
                        $compteur_affaire['Compteur']['nom'] = 'compteur_affaire';
                        $compteur_affaire['Compteur']['commentaire'] = 'compteur_affaire';
                        $compteur_affaire['Compteur']['def_compteur'] = "AFF_#AAAA##S#";
                        $compteur_affaire['Compteur']['def_reinit'] = "#AAAA#";
                        $compteur_affaire['Compteur']['sequence_id'] = $this->Sequence->id;
                        if ($this->Compteur->save($compteur_affaire)) {
                            //collectivite compteur affaire
                            //sequence dossier
                            $sequence_dossier = $this->Sequence->create();
                            $sequence_dossier['Sequence']['nom'] = 'sequence_dossier';
                            $sequence_dossier['Sequence']['commentaire'] = 'sequence_dossier';
                            $sequence_dossier['Sequence']['num_sequence'] = 1;
                            if ($this->Sequence->save($sequence_dossier)) {
                                //compteur dossier
                                $compteur_dossier = $this->Compteur->create();
                                $compteur_dossier['Compteur']['nom'] = 'compteur_dossier';
                                $compteur_dossier['Compteur']['commentaire'] = 'compteur_dossier';
                                $compteur_dossier['Compteur']['def_compteur'] = "DOS_#AAAA##S#";
                                $compteur_dossier['Compteur']['def_reinit'] = "#AAAA#";
                                $compteur_dossier['Compteur']['sequence_id'] = $this->Sequence->id;
                                if ($this->Compteur->save($compteur_dossier)) {
                                    //collectivite compteur dossier
                                    $valid['cpt'] = true;
                                }
                            }
                        }
                    }
//                     				}
                }
            } catch (Exception $exc) {
                $valid['msg'] .= '<br /><br />Le suffixe de connexion est déjà utilisé par une autre collectivité ' ;
                $valid['msg'] .= '<br /><br />' . __d('collectivite', 'Collectivite.add.error.db') . '<hr />' . (Configure::read('debug') > 0 ? $exc->getTraceAsString() : '');
            }
//$this->log( $this->request->data, 'debug') ;
//$this->log( $valid, 'debug') ;
//die();
//$this->log( $exc, 'debug') ;
//$this->log( $exc->getTraceAsString(), 'debug') ;
            if (!in_array(false, $valid, true)) {
                $this->Jsonmsg->valid();
                $this->Collectivite->commit();
                if ($valid['import'] == true) {
                    $this->User->commit();
                }

                // Création des répertoires de stockage sur disque par collectivité pour le workspace et le webdav
				$workspaceFolder = new Folder(WORKSPACE_PATH . DS . $this->request->data['Collectivite']['conn'] , true, 0777);
				$webdavFolder = new Folder(WEBDAV_DIR . DS . $this->request->data['Collectivite']['conn'] . DS . 'modeles' , true, 0777);

            } else {
                $this->Jsonmsg->error(__d('collectivite', 'Collectivite.add.error'));
                $this->Collectivite->rollback();
                if ($valid['import'] == true) {
                    $this->User->rollback();
                }
            }
            $this->Jsonmsg->send();
        } else {
            //suppression de la base admin (default)
            unset($conns['default']);
            //suppression de la base stats (uniquement en dev)
            if (isset($conns['stats'])) {
                unset($conns['stats']);
            }

            //suppression des connexion dèjà paramétrées pour une collectivite
            $listCollectiviteConn = $this->Collectivite->find('list', array('fields' => array('Collectivite.id', 'Collectivite.conn')));
            foreach ($listCollectiviteConn as $key => $val) {
                unset($conns[$val]);
            }

            $connsToSend = array();
            foreach (array_keys($conns) as $conn) {
                $connsToSend[$conn] = $conn;
            }
            $this->set('conns', $connsToSend);
        }
        $type = array(
            'ActiveDirectory' => 'Active Directory',
            'OpenLDAP' => 'Open LDAP'
        );
        $this->set( 'type', $type);
    }

    /**
     * Edition d'une collectivité
     *
     * @logical-group Collectivité
     * @user-profile Superadmin
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->Init();
            $conn = $this->Collectivite->field('conn', array('Collectivite.id' => $this->request->data['Collectivite']['id']));
            if (!empty($conn)) {
                $valid = array(
                    'collectivite' => false,
                    'user' => false,
                    'rgpd' => false,
//					'compteur_affaire' => false,
//					'compteur_dossier' => false
                );

                $this->Collectivite->begin();
                $this->Collectivite->create($this->request->data);
                if ($this->Collectivite->save()) {
                    $valid['collectivite'] = true;
                }

                if( !empty( $this->request->data['Connecteur'] ) ) {
                    $this->loadModel('Connecteur');
                    $this->Connecteur->setDataSource($conn);
					if (!empty($this->request->params['form']['myfile']['tmp_name'])) {
						$this->request->data['Connecteur']['ldaps_nom_cert'] = $this->request->params['form']['myfile']['name'];
						$crt = file_get_contents($this->request->params['form']['myfile']['tmp_name']);
						$this->request->data['Connecteur']['ldaps_cert'] = $crt;
					}
					else {
						unset($this->request->params['form']['myfile']);
					}
                    $this->Connecteur->save($this->request->data['Connecteur']);
                }


                $this->loadModel('User');
                $this->User->setDataSource($conn);
                $this->User->begin();

                if (!empty($this->request->data['User']['Newpassword'])) {
					$this->request->data['User']['password'] = Security::hash($this->request->data['User']['Newpassword'], 'sha256', true);
                }

                $this->User->create(array('User' => $this->request->data['User']));
                if ($this->User->save()) {
                    $valid['user'] = true;
                }

                // Partie pour la politique de confidentialité
                if(isset($this->request->data['Rgpd']) && !empty($this->request->data['Rgpd']) ) {
					$this->loadModel('Rgpd');
					$this->Rgpd->setDataSource($conn);

					$this->request->data['Rgpd']['collectivite_id'] = $this->request->data['Collectivite']['id'];
					$this->request->data['Rgpd']['conn'] = $this->request->data['Collectivite']['conn'];
					$this->Rgpd->create(array('Rgpd' => $this->request->data['Rgpd']));
					if ($this->Rgpd->save()) {
						$valid['rgpd'] = true;
					}
				}
                if (!in_array(false, $valid, true)) {
                    $this->Jsonmsg->valid();
                    $this->Collectivite->commit();
                    $this->User->commit();
                } else {
                    $this->Jsonmsg->error(var_export($valid, true));
                    $this->Collectivite->rollback();
                    $this->User->rollback();
                }
                $this->Jsonmsg->send();
            } else {
                throw new NotFoundException();
            }
        } else {
            $this->request->data = $this->Collectivite->find('first', array('conditions' => array('Collectivite.id' => $id)));

            $conn = $this->request->data['Collectivite']['conn'];

            $this->loadModel('User');
            $this->User->setDataSource($conn);
            $this->loadModel('Desktop');
            $this->Desktop->setDataSource($conn);


            $desktopId = $this->Desktop->field('id', array('Desktop.profil_id' => ADMIN_GID));
            $admin = $this->User->find('first', array('conditions' => array('User.desktop_id' => $desktopId)));
            unset($admin['User']['password']);
            $this->request->data['User'] = $admin['User'];

            // Gestion par bureau
            $this->loadModel('Desktopmanager');
            $this->Desktopmanager->setDataSource($conn);
            $optionsScanDesktopId = $this->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.isdispatch' => true)));
            $this->set('optionsScanDesktopId', $optionsScanDesktopId);

            // Gestion du connecteur LDAP
            $this->loadModel('Connecteur');
            $this->Connecteur->setDataSource($conn);
            $this->Connecteur = ClassRegistry::init('Connecteur');
            $annuaireLdap = $this->Connecteur->find(
                'first',
                array(
                    'conditions' => array(
                        'Connecteur.name ILIKE' => '%Annuaire LDAP%'
                    ),
                    'contain' => false
                )
            );
            $connecteurId = null;
            if( !empty($annuaireLdap) ) {
                $this->request->data['Connecteur'] = $annuaireLdap['Connecteur'];
                $connecteurId = $annuaireLdap['Connecteur']['id'];
            }
            $this->set( 'connecteurId', $connecteurId);


			$this->loadModel('Rgpd');
			$this->Rgpd->setDataSource($conn);
			$this->Rgpd = ClassRegistry::init('Rgpd');
			$rgpd = $this->Rgpd->find(
				'first',
				array(
					'conditions' => array(
						'Rgpd.collectivite_id' => $id,
						'Rgpd.conn' => $conn
					),
					'contain' => false
				)
			);
			$rgpdId = null;
			if( !empty($rgpd) ) {
				$this->request->data['Rgpd'] = $rgpd['Rgpd'];
				$rgpdId = $rgpd['Rgpd']['id'];
			}
			$this->set( 'rgpdId', $rgpdId);

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
        $type = array(
            'ActiveDirectory' => 'Active Directory',
            'OpenLDAP' => 'Open LDAP'
        );
        $this->set( 'type', $type);
    }

    /**
     * Suppression d'une collectivité (soft delete)
     *
     * @logical-group Collectivité
     * @user-profile Superadmin
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Collectivite->begin();
        if ($this->Collectivite->delete($id, true)) {
            $this->Collectivite->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Collectivite->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Envoyer le logo d'une collectivité
     *
     * @logical-group Collectivité
     * @logical-group Edition d'une collectivité
     *
     * @logical-used-by Collectivité > Edition d'une collectivité
     * @user-profile Superadmin
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function upload() {
        if (!empty($_FILES['myfile'])) {
            $filename = basename(strtr($_FILES['myfile']['name'], " ", "_"));
            $target = LOGO_DIR . $filename;
            $message = '';
            if (@move_uploaded_file($_FILES['myfile']['tmp_name'], $target)) {
                $message = 'Le fichier a été envoyé.';
                //enregistrer le lienlogo dans la collectivite
                $this->Collectivite->id = $_POST['collectiviteId'];
                //			$collectivite = $this->Collectivite->read();
                $collectivite = $this->Collectivite->find(
                        'first', array(
                    'fields' => array(
                        'Collectivite.id',
                        'Collectivite.logo'
                    ),
                    'conditions' => array(
                        'Collectivite.id' => $this->Collectivite->id
                    ),
                    'contain' => false
                        )
                );
                //debug($collectivite);
                //die();
                $collectivite['Collectivite']['logo'] = $filename;
                $this->Collectivite->save($collectivite);
            } else {
                $message = 'Le fichier n\'a pas été envoyé';
            }
            //affichage du message d erreur
            //on utilisera ici jgrowl directement dans la vue, etant donne que l upload se fait via une iframe et que l utilisation
            //classique de jgrowl afficherai le message dans cette iframe invisible
            $this->set('message', $message);
        }
    }

    /**
     * Récupération du logo d'une collectivité
     *
     * @logical-group Collectivité
     *
     * @logical-used-by Collectivité > Edition d'une collectivité
     * @user-profile Superadmin
     * @user-profile Admin
     *
     * @logical-used-by Environnement > Environnement administrateur
     * @user-profile Admin
     *
     * @access public
     * @param integer $collectiviteId
     * @return void
     */
    public function getLogo($collectiviteId = null) {
        if ($collectiviteId != null) {
            $this->Collectivite->id = $collectiviteId;
//			$collectivite = $this->Collectivite->read();
            $collectivite = $this->Collectivite->find(
                    'first', array(
                'fields' => array(
                    'Collectivite.id',
                    'Collectivite.logo'
                ),
                'conditions' => array(
                    'Collectivite.id' => $collectiviteId
                ),
                'contain' => false
                    )
            );

            $this->set('col_id', $collectiviteId);
            $this->set('logo_url', $collectivite['Collectivite']['logo']);
        }
    }

    /**
     * Edition d'une collectivité
     *
     * @logical-group Collectivité
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function admin() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('default', 'Informations', true)
        ));
        $conn = $this->Session->read('Auth.User.connName');

        if (!empty($this->request->data)) {
            $valid = array(
                'collectivite' => false
            );
            $this->Jsonmsg->init();
            $this->Collectivite->begin();

            $this->request->data['Collectivite']['conn'] = $conn;
            $this->Collectivite->create($this->request->data);
            if ($this->Collectivite->save()) {
                $valid['collectivite'] = true;
            }



            if( !empty( $this->request->data['Connecteur'] ) ) {
                $this->loadModel('Connecteur');
                $this->Connecteur->setDataSource($conn);
                $this->Connecteur->save($this->request->data['Connecteur']);
            }

			if( !empty( $this->request->data['Rgpd'] ) ) {
				$this->loadModel('Rgpd');
				$this->Rgpd->setDataSource($conn);
				$this->request->data['Rgpd']['collectivite_id'] = $this->request->data['Collectivite']['id'];
				$this->request->data['Rgpd']['conn'] = $this->request->data['Collectivite']['conn'];
				$this->Rgpd->save($this->request->data['Rgpd']);
			}


            if (!in_array(false, $valid, true)) {
                $this->Jsonmsg->valid();
                $this->Collectivite->commit();
            } else {
                $this->Collectivite->rollback();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn)));


            $this->loadModel('User');
            $this->User->setDataSource($conn);
            $this->loadModel('Desktop');
            $this->Desktop->setDataSource($conn);


            $scanDestDispRoles = $this->Desktop->find('list', array('conditions' => array('Desktop.profil_id' => array(DISP_GID))));
            $this->set('scanDestDispRoles', $scanDestDispRoles);
            $scanDestInitRoles = $this->Desktop->find('list', array('conditions' => array('Desktop.profil_id' => array(INIT_GID))));
            $this->set('scanDestInitRoles', $scanDestInitRoles);

            // Gestion par bureau
            $this->loadModel('Desktopmanager');
            $this->Desktopmanager->setDataSource($conn);
            $scanDesktops = $this->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.isdispatch' => true)));
            $this->set('scanDesktops', $scanDesktops);

            // Gestion du connecteur LDAP
           $this->loadModel('Connecteur');
           $this->Connecteur->setDataSource($conn);
           $this->Connecteur = ClassRegistry::init('Connecteur');
           $annuaireLdap = $this->Connecteur->find(
               'first',
               array(
                   'conditions' => array(
                       'Connecteur.name ILIKE' => '%Annuaire LDAP%'
                   ),
                   'contain' => false
               )
           );
           $connecteurId = null;
           if( !empty($annuaireLdap) ) {
               $this->request->data['Connecteur'] = $annuaireLdap['Connecteur'];
               $connecteurId = $annuaireLdap['Connecteur']['id'];
           }
           $this->set( 'connecteurId', $connecteurId);

           // Politique de confidentialité
			$this->Rgpd = ClassRegistry::init('Rgpd');
			$this->Rgpd->useDbConfig = $conn;
			$this->loadModel('Rgpd');
			$this->Rgpd->setDataSource($conn);
			$this->Rgpd = ClassRegistry::init('Rgpd');
			$rgpd = $this->Rgpd->find(
				'first',
				array(
					'conditions' => array(
						'Rgpd.collectivite_id' => $this->request->data['Collectivite']['id'],
						'Rgpd.conn' => $this->request->data['Collectivite']['conn']
					),
					'contain' => false
				)
			);
			$rgpdId = null;
			if( !empty($rgpd) ) {
				$this->request->data['Rgpd'] = $rgpd['Rgpd'];
				$rgpdId = $rgpd['Rgpd']['id'];
			}
			$this->set( 'rgpdId', $rgpdId);
        }


        $type = array(
            'ActiveDirectory' => 'Active Directory',
            'OpenLDAP' => 'Open LDAP'
        );
        $this->set( 'type', $type);

        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     *
     */
    /* public function synchronize() {
      $ldapconn = ldap_connect(LDAP_Configure::read('HOST'), PORT)
      or die("Impossible de se connecter au serveur LDAP {LDAP_Configure::read('HOST')}");
      if ($ldapconn) {
      // bind with appropriate dn to give update access
      $r = ldap_bind($ldapconn, MANAGER, LDAP_PASS);
      if (!$r)
      die("ldap_bind failed<br>");

      $dn = "ou=users,dc=adullact,dc=org";
      $filter = "(|(sn=*))";
      $justthese = array(MAIL, COMMON_NAME, UNIQUE_ID, PASSWORD_USER);
      $sr = ldap_search($ldapconn, $dn, $filter, $justthese);
      $users = ldap_get_entries($ldapconn, $sr);

      foreach ($users as $user) {
      if (isset($user[UNIQUE_ID][0]))
      $login = $user[UNIQUE_ID][0];

      if (isset($user[PASSWORD_USER][0]))
      $password = $user[PASSWORD_USER][0];
      else
      unset($password);

      if (isset($login) && isset($password)) {
      if (isset($user[MAIL][0]))
      $mail = $user[MAIL][0];
      else
      $mail = "";

      if (isset($user[COMMON_NAME][0])) {
      $cn = $user[COMMON_NAME][0];
      $prenom = substr($cn, 0, strpos($cn, ' '));
      $nom = substr($cn, strpos($cn, ' '), strlen($cn));
      }
      else
      $cn = "";

      $data = $this->User->findAll("User.login = '$login'");
      if (empty($data)) {
      $this->User->create();
      $this->data['User']['id'] = '';
      $this->data['User']['nom'] = $nom;
      $this->data['User']['prenom'] = $prenom;
      $this->data['User']['login'] = $login;
      $this->data['User']['mail'] = $mail;
      $pwd = substr($password, 5, strlen($password));
      $mdp = unpack("H*", $pwd);
      $this->data['User']['password'] = $mdp[1];
      $this->User->save($this->data);
      }
      }
      }
      ldap_close($ldapconn);
      } else {
      echo "Unable to connect to LDAP server";
      }
      }
     *
     */

    public function deleteLogo($collectiviteId = null) {
        if ($collectiviteId != null) {
            $this->Collectivite->id = $collectiviteId;
            $collectivite = $this->Collectivite->find(
                    'first', array(
                'fields' => array(
                    'Collectivite.id',
                    'Collectivite.logo'
                ),
                'conditions' => array(
                    'Collectivite.id' => $collectiviteId
                ),
                'contain' => false
                    )
            );
            $this->autoRender = false;

            $this->Collectivite->updateAll(
                    array('Collectivite.logo' => null), array('Collectivite.id' => $collectiviteId)
            );
//            debug($collectivite);
//			$this->set('logo_url', $collectivite['Collectivite']['logo']);
        }
    }


	/**
	 * Fonction permettant de générer la clé d'API
	 * @throws Exception
	 */
	public function genApiKey($collectiviteId) {

		$this->Jsonmsg->init();
		$this->autoRender = false;
		$this->Collectivite->id = $collectiviteId;
		$res = $this->Collectivite->saveField('apitoken', bin2hex(random_bytes(30)));
		if($res){
			$jsonMessage = $res['Collectivite']['apitoken'] . "<hr /> La clé d'API a bien été modifiée " ;
			$this->Jsonmsg->valid( $jsonMessage );
		}else{
			$this->Jsonmsg->error();
		}
		$this->Jsonmsg->send();
	}

}

?>
