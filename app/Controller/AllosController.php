<?php

    App::uses('AppController', 'Controller');
    /**
     * Flux
     *
     * Courriers controller class
     *
     * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
     *
     * PHP version 7
     * @author Arnaud AUZOLAT
     * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
     * @link http://adullact.org/
     * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
     *
     * @package		app
     * @subpackage		Controller
     */

    /**
     * La classe AllosController permet des dialogues REST avec Allo.
     *
     * @package app.Controller
     */
    class AllosController extends AppController
    {
        /**
         * Nom du contrôleur.
         *
         * @var string
         */
        public $name = 'Allos';

        /**
         * Components utilisés.
         *
         * @var array
         */
        public $components = array();

        /**
         * Modèles utilisés.
         *
         * @var array
         */
        public $uses = false;

        /**
         * Actions non soumises aux droits.
         *
         * @var array
         */
        public $aucunDroit = array(
            'version'
        );

        /**
         * Numéro de version de l'application en cours (cf contenu de app/Config/version)
         * @return type
         */
        public function app_version() {
            $versionData = explode("\n", file_get_contents(ROOT . DS . 'app' . DS . 'Config' . DS . 'version'));
            $version = explode('.', $versionData[0]);
            return implode('.', $version);
        }

        /**
         * Retourne la base des informations concernant le produit, la version
         * et le client utilisant l'application.
         */
        public function version() {
            $json = array(
                'produit' => Configure::read( 'PRODUIT' ),
                'version' => $this->app_version(),
//                 'refClient' => Configure::read( 'WEBGFCCLIENT' )
            );

            $this->set( compact( 'json' ) );

            $this->layout = false;
            $this->render( '/Elements/json' );
        }
    }
?>
