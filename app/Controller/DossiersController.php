<?php

/**
 * Dossiers
 *
 * Dossiers controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class DossiersController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var array
     */
    public $name = 'Dossiers';

	protected $_lastIdNameDossier = null;

	public function beforeFilter() {
		$this->Auth->allow('getComment');
		parent::beforeFilter();
	}

    /**
     * Gestion des dossiers/affaires
     *
     * @logical-group Dossier
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'gestionDossiersAffaires', true)));
    }

    /**
     * Ajout d'un dossier (avec incrémentation du compteur)
     *
     * @logical-group Dossier
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $this->Dossier->begin();

            $this->Dossier->create($this->request->data);
            if ($this->Dossier->save()) {
                $this->Dossier->commit();
                $this->Jsonmsg->valid();
            } else {
                $this->Dossier->rollback();
                $this->Jsonmsg->error('Erreur de sauvegarde');
            }
            $this->Jsonmsg->send();
        } else {
			$sql = "SELECT count(*) FROM dossiers WHERE date_part('year', created) = date_part('year', CURRENT_DATE);";
			$numSeq = $this->Dossier->query($sql);

			if ($numSeq === false) {
				return null;
			}
			$numDossierName = 'Dossier_'.date('Y').$numSeq[0][0]['count'];
            $this->set('newDossierName', $numDossierName);
        }
    }

    /**
     * Edition d'un dossier
     *
     * @logical-group Dossier
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du dossier
     * @return void
     */
    public function edit($id = null) {
        if (!empty($this->request->data)) {
            $this->Dossier->begin();
            $success = $this->Dossier->save($this->request->data);
            if ( $success ) {
                $this->Dossier->commit();
                $this->Jsonmsg->valid();
            } else {
                $this->Dossier->rollback();
                $this->Jsonmsg->error('Erreur de sauvegarde');
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Dossier->read(null, $id);
        }
    }

    /**
     * Suppression d'un dossier
     *
     * @logical-group Dossier
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du dossier
     * @return void
     */
    public function delete($id = null) {
        if ($id != null) {
            $this->Jsonmsg->init(__d('default', 'delete.error'));
            $this->Dossier->begin();
            if ($this->Dossier->delete($id)) {
                $this->Dossier->commit();
                $this->Jsonmsg->valid(__d('default', 'delete.ok'));
            } else {
                $this->Dossier->rollback();
            }
            $this->Jsonmsg->send();
        }
		$this->autoRender = false;
    }

    /**
     * Récupération de la liste des dossiers
     *
     * @logical-group Dossier
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function getDossiers() {
//        $this->Dossier->recursive = 1;
        $this->set('dossiers', $this->Dossier->getDossiersAffairesTree());
        $querydata = array(
            'contain' => array(
                $this->Dossier->Affaire->alias => array(
                    'order' => 'Affaire.name'
                )
            ),
            'order' => 'Dossier.name',
            'recursive' => -1
        );
        $dossiers_tmp = $this->Dossier->find("threaded", $querydata );

        $dossiers_tmp  = array_merge( $this->Dossier->getDossiersAffairesTree(), $dossiers_tmp  );

        foreach($dossiers_tmp as $t => $dossier) {
            foreach( $dossier['Affaire'] as $s => $affaire ) {
                $dossiers_tmp[$t]['children'][$s]['Dossier'] = $dossier['Dossier'];
                $dossiers_tmp[$t]['children'][$s]['Affaire'] = $affaire;
                $dossiers_tmp[$t]['children'][$s]['Affaire']['hasParent'] = isset( $dossier['Affaire'][0] ) ?  true : false;
            }
            $dossiers_tmp[$t]['Dossier']['hasParent'] = isset( $dossier['Affaire'][0] ) ?  true : false;
        }

        $dossiers = array();
        foreach ($dossiers_tmp as $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $dossiers[] = $item;
        }

        $this->set('dossiers', $dossiers);
    }
    /**
     * Récupération de la liste des affaires hors dossier
     *
     * @access private
     * @return void
     */
    private function _getAffairesHorsDossier() {
        $affaires_tmp = $this->Dossier->Affaire->find("all", array(
            'order' => 'Affaire.name',
            'conditions' => array(
                array(
                    'Affaire.dossier_id' => null
                )
            )
        ));
        $affaires = array();
        foreach ($affaires_tmp as $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $affaires[] = $item;
        }
        $this->set('affaires', $affaires);
    }

    /**
     * obtenir d'un commentaire d'une dossier
     *
     * @logical-group Affaires
     * @user-profile Admin
     * @user-profile User
     *
     */
    public function getComment(){
        $this->autoRender = false;
        if( !empty($this->request->data) ) {
            $id =   array_keys( $this->request->data);
            if($id != null){
                    $comment = $this->Dossier->field('comment', array('Dossier.id' => $id));
            }
        }
        else if( !empty($this->request->params['pass']) ) {
            $id = $this->request->params['pass'][0];
            $comment = $this->Dossier->field('comment', array('Dossier.id' => $id));
        }

        return $comment;
    }

}

?>
