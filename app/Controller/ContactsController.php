<?php

/**
 * Contacts
 *
 * Contacts controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller
 */
class ContactsController extends AppController {

    /**
     * Controller uses
     *
     * @var array
     */
    public $uses = array('Contact', 'Ban');

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('import', 'import_contacts', 'import_contacts_index', 'export', 'getOrganismeValues', 'search_contacts_courriers', 'searchOrganismeinfo', 'searchSansContact');
    }

    protected function _setOptions() {
        $options = array();
        $options = $this->Contact->Addressbook->listTypesVoie();
        $this->set(compact('options'));

        $this->set( 'bans', $this->Session->read('Auth.Ban') );
        $civilite = $this->Session->read( 'Auth.Civilite');
        $this->set('civilite', $civilite);

        $this->set('orgs', $this->Contact->Organisme->find('list', array('order' => array('Organisme.name ASC'))));
        if (Configure::read('Conf.SAERP')) {
            $this->set('operations', $this->Contact->Operation->find('list', array('order' => array('Operation.name ASC'), 'conditions' => array('Operation.active' => true))));
            $this->set('events', $this->Contact->Event->find('list', array('order' => array('Event.name ASC'), 'conditions' => array('Event.active' => true))));

        }
		$this->set('fonctions', $this->Contact->Fonction->find('list', array('order' => array('Fonction.name ASC'), 'conditions' => array('Fonction.active' => true))));
        $this->set('titres', $this->Contact->Titre->find('list',array('order' => array('Titre.name ASC'), 'contain' => false ) ) );
    }

    /**
     * Récupération de la liste des contacts
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @access public
     * @param integer $addressbook_id identifiant du carnet d'adresse
     * @return void
     */
    public function get_contacts($addressbook_id = null, $orgId = null) {
        if (!empty($this->params['named']['page'])) {
            $this->request->data = $this->Session->read('paginationModel.rechercheContact');
        }
        $this->Session->write('paginationModel.rechercheContact', $this->request->data);
        //          On doit pouvoir obtenir les résultats dès le premier accès à la page
        if ($addressbook_id != null) {
            $contacts = array();
            if (empty($this->request->data)) {
                $this->Contact->recursive = -1;
                $querydata = array(
                    'conditions' => array(
                        'Contact.addressbook_id' => $addressbook_id,
                        'Contact.active' => true
                    ),
                    'order' => array(
                        'Contact.nom ASC',
                        'Contact.prenom ASC'
                    ),
                    'contain' => array(
                        'Organisme',
                        'Addressbook'
                    ),
                    'limit' => 20
                );
            } else {
                if (!empty($orgId) || isset($this->request->data['SearchContact']['org_id'])) {
                    if (isset($this->request->data['SearchContact']['org_id'])) {
                        $orgValueId = $this->request->data['SearchContact']['org_id'];
                    } else {
                        $orgValueId = $orgId;
                    }
                    $querydata = $this->Contact->search($this->request->data, $addressbook_id, $orgValueId);
                } else {
                    $querydata = $this->Contact->search($this->request->data, $addressbook_id, null);
                }
            }

            $nbContact = $this->Contact->find('count', $querydata);
            $this->paginate = $querydata;
            $contacts = $this->paginate($this->Contact);
            foreach ($contacts as $i => $contact) {

                // Droits d'accès aux actions edit et delete
                $contact['right_edit'] = true;
                $contact['right_delete'] = true;
                if (Configure::read('Conf.SAERP')) {
                    $contact['right_export'] = true;
                }
                $contacts[$i] = $contact;

                // Ajout du nom de l'organisme dans le tableau reprenant les contacts
                if (!empty($contact['Organisme']['name'])) {
                    $contacts[$i]['Contact']['organisme_name'] = $contact['Organisme']['name'];
                } else {
                    $contacts[$i]['Contact']['organisme_name'] = '';
                }
            }

            $this->set('contacts', $contacts);
            $this->set('orgId', $orgId);
            $this->set('addressbook_id', $addressbook_id);
            $this->set('nbContact', $nbContact);
            $this->_setOptions();
        }
    }

    /**
     * Ajout d'un contact
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @logical-used-by Flux > Ajout d'un flux
     * @user-profile User
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function add($orgId = null, $carnetId = null) {
//        $this->autoRender = false;
        $this->Contact->recursive = -1;
        $this->Jsonmsg->init();
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            if (!empty($this->request->data['Contact']['banadresse'])) {
                $adressecomplete = Hash::get($this->request->data, 'Contact.numvoie') . ' ' . Hash::get($this->request->data, 'Contact.banadresse');
            } else {
                $adressecomplete = Hash::get($this->request->data, 'Contact.numvoie') . ' ' . Hash::get($this->request->data, 'Contact.nomvoie');
            }
            if (!empty($this->request->data['Contact']['observations'])) {
                $this->request->data['Contact']['observations'] = str_replace(array("<p>", "</p>", "<br />"), "\t", quoted_printable_decode($this->request->data['Contact']['observations']));
            }
            $this->request->data['Contact']['adresse'] = $adressecomplete;
            $this->Contact->begin();

			if( strpos($this->request->data['Contact']['nomvoie'], "'") !== false ) {
				$this->request->data['Contact']['nomvoie'] = Sanitize::clean($this->request->data['Contact']['nomvoie'], array('encode', false));
			}
			if( strpos($this->request->data['Contact']['adressecomplete'], "'") !== false ) {
				$this->request->data['Contact']['adressecomplete'] = Sanitize::clean($this->request->data['Contact']['adressecomplete'], array('encode', false));
			}
			$this->request->data['Contact']['ville'] = strtoupper($this->request->data['Contact']['ville'] );
			$success = $this->Contact->save($this->request->data);
            // Cas de l'ajout depuis la gestion du flux
            if ($_SERVER['HTTP_REFERER'] != Configure::read('WEBGFC_URL') . '/addressbooks') {

                // on stocke qui à fait quoi quand
                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a créé le contact ".$this->request->data['Contact']['name']." (organisme_id = ".$this->request->data['Contact']['organisme_id'].") le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info');
                if ($success) {
                    $this->Contact->MiseajourData();
                    $this->autoRender = false;
                    $this->Contact->commit();
                    $this->Jsonmsg->valid();
                } else {
                    $this->Contact->rollback();
                }
                $this->Jsonmsg->send();
            } else {
                // Cas de l'ajout depuis l'administration du carnet d'adresse
                if ($success) {
                    $this->Contact->commit();
                    $this->Jsonmsg->valid();
                } else {
                    $this->Contact->rollback();
                }
                $this->Jsonmsg->send();
            }
        }

        if (!empty($orgId)) {
            if ($orgId != 'null') {
                $organisme = $this->Contact->Organisme->find(
                        'first', array(
                    'conditions' => array(
                        'Organisme.id' => $orgId
                    ),
                    'recursive' => -1
                        )
                );
                $this->set('orgId', $orgId);
                $this->set('addressbook_id', $organisme['Organisme']['addressbook_id']);
            } else {
                if (!empty($carnetId)) {

                    $organisme = $this->Contact->Organisme->find(
                            'first', array(
                        'conditions' => array(
                            'Organisme.name' => 'Sans organisme'
                        ),
                        'recursive' => -1
                            )
                    );

                    $this->set('orgId', $organisme['Organisme']['id']);
                    $this->set('addressbook_id', $carnetId);
                }
            }
        } else {
            $organisme = $this->Contact->Organisme->listOrganismes();
        }

        $this->set('organisme', $organisme);
        if ($orgId != 'null') {
            $this->set('orgId', $orgId);
        }
        $this->_setOptions();
    }

    /**
     * Edition d'un contact
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function edit($id, $panel_footer = null) {
        $this->Contact->recursive = -1;
        if (!empty($this->request->data)) {
			$this->request->data['Contact']['name'] = str_replace( "\'", "'", $this->request->data['Contact']['name']);
            $this->Jsonmsg->init();
            $this->Contact->begin();
            if (!empty($this->request->data['Contact']['banadresse'])) {
                $adressecomplete = Hash::get($this->request->data, 'Contact.numvoie') . ' ' . Hash::get($this->request->data, 'Contact.banadresse');
            } else {
                $adressecomplete = Hash::get($this->request->data, 'Contact.numvoie') . ' ' . Hash::get($this->request->data, 'Contact.nomvoie');
            }
            if (!empty($this->request->data['Contact']['observations'])) {
                $this->request->data['Contact']['observations'] = str_replace(array("<p>", "</p>", "<br />"), "\t", quoted_printable_decode($this->request->data['Contact']['observations']));
            }

			if( strpos($this->request->data['Contact']['nomvoie'], "'") !== false ) {
				$this->request->data['Contact']['nomvoie'] = Sanitize::clean($this->request->data['Contact']['nomvoie'], array('encode', false));
			}
			if( strpos($this->request->data['Contact']['adressecomplete'], "'") !== false ) {
				$this->request->data['Contact']['adressecomplete'] = str_replace( "'", "''", $this->request->data['Contact']['adressecomplete'] );
			}

			$this->request->data['Contact']['adresse'] = $adressecomplete;
			$addressbook_id = $this->request->data['Contact']['addressbook_id'];

			$this->request->data['Contact']['ville'] = strtoupper($this->request->data['Contact']['ville'] );
            $success = $this->Contact->save($this->request->data);

            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le contact ".$this->request->data['Contact']['name']." (organisme_id = ".$this->request->data['Contact']['organisme_id'].") le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info');
            if ($success) {
                $this->Contact->MiseajourData();
                $this->Contact->commit();
                $this->Jsonmsg->valid();
				CakeSession::write('Auth.User.addressbook_private', false);
            } else {
                $this->Contact->rollback();
            }
            $this->Jsonmsg->send();
        } else if ($id != null) {
            $querydata = array(
                'conditions' => array(
                    'Contact.id' => $id
                ),
                'contain' => array(
                    'Addressbook',
                    'Organisme',
                    'Ban' => array(
                        'Bancommune'
                    ),
					'Fonction'
                ),
                'recursive' => -1
            );
            if (Configure::read('Conf.SAERP')) {
                $querydata['contain'] = array_merge(
                        $querydata['contain'], array(
                    'Event',
                    'Operation',
                    'Fonction'
                        )
                );
            }
            if (isset($panel_footer) && $panel_footer == 'panel_footer') {
                $this->set('panel_footer', true);
            }
            $this->request->data = $this->Contact->find('first', $querydata);

            $orgId = $this->request->data['Contact']['organisme_id'];
            $addressbook_id = $this->request->data['Contact']['addressbook_id'];
			$this->request->data['Contact']['name'] = str_replace( "'", "\'", $this->request->data['Contact']['name']);
            $this->set('orgId', $orgId);
            $this->set('addressbook_id', $addressbook_id);
			CakeSession::write('Auth.User.addressbook_private', true);
        }
        $this->set('id', $id);
        $organismes = $this->Contact->Organisme->listOrganismes($addressbook_id);
        $this->set(compact('organismes'));
//debug($this->request->data);
        $this->_setOptions();
    }

    /**
     * Suppression d'un contact
     *
     * @logical-group Contact
     * @logical-group Fiche de contact
     * @user-profile Admin
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function delete($id = null, $real = true) {
        if ($id != null) {
            if ($real) {
                $this->Jsonmsg->init(__d('default', 'delete.error'));
                $contact = $this->Contact->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Contact.id' => $id
                        ),
                        'contain' => false,
                        'recursive' => -1
                    )
                );
                if ($this->Contact->delete($id, true)) {
//                    $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé le contact ".$contact['Contact']['name']." le ".date('d/m/Y à H:i:s'), LOG_WARNING );
                    $this->loadModel('Journalevent');
                    $datasSession = $this->Session->read('Auth.User');
                    $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé le contact ".$contact['Contact']['name']." (organisme_id = ".$contact['Contact']['organisme_id'].") le ".date('d/m/Y à H:i:s');
                    $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info');
                    $this->Jsonmsg->valid(__d('default', 'delete.ok'));
                }
            } else {
                $this->Jsonmsg->init();
                $this->Contact->id = $id;

                if ($this->Contact->saveField('active', false)) {
                    $this->Jsonmsg->valid();
                }
            }
            $this->Jsonmsg->send();
        }
    }

    /*
     * Téléchager des model CSV
     *
     * @logical-group Contact
     * @logical-group Fiche de contact
     * @user-profile Admin
     *
     * @access public
     * @param integer $id
     * @return void
     */

    public function download_model($langue) {
        $this->autoRender = false;
		$file = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'export' . DS . 'modelCSV_fr.csv';
        if (is_file($file) && file_exists($file)) {
            set_time_limit(0);
            if (isset($_SERVER['HTTP_RANGE']) && ($_SERVER['HTTP_RANGE'] != "") &&
                    preg_match("/^bytes=([0-9]+)-$/i", $_SERVER['HTTP_RANGE'], $match) ) {
                $start = $match[1];
            } else {
                $start = 0;
            }
            $size = abs(sprintf("%u", filesize($file)));
            header("Cache-Control: public");
            header("X-Powered-By: kodExplorer.");

            header("Content-Type: application/octet-stream");
            $path = str_replace('\\', '/', rtrim(trim($file), '/'));
            $filename = substr($path, strrpos($path, '/') + 1);
            if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) {
                $filename = str_replace('+', '%20', urlencode($filename));
            }
            header("Content-Disposition: attachment;filename=" . $filename);

            if ($start > 0) {
                header("HTTP/1.1 206 Partial Content");
                header("Content-Ranges: bytes" . $start . "-" . ($size - 1) . "/" . $size);
                header("Content-Length: " . ($size - $start));
            } else {
                header("Accept-Ranges: bytes");
                header("Content-Length: $size");
            }
            $fp = fopen($file, "rb");
            fseek($fp, $start);
            while (!feof($fp)) {
                print (fread($fp, 1024 * 8));
                flush();
                ob_flush();
            }
            fclose($fp);
        }
    }

    /**
     * Analyse le fichier importé, et importation de contact et de fiches de contact à partir d'un fichier vcard/csv
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function import($addressbook_id = null, $organisme_id = null) {
        $this->autoRender = false;
        $return = array();
//var_dump($this->request->data['Import']);
        if (!empty($this->request->data)) {
            $ex = $this->request->data['Import']['format'];
//var_dump($this->request->data);
            //import fichier
            if (!empty($this->request->data['Import']['file']) && empty($this->request->data['Import']['addressbook_id'])) {
// var_dump($this->request->data['Import']);
                $tmpName = $this->request->data['Import']['file']['tmp_name'];
                $filename = TMP . $this->request->data['Import']['file']['name'];
                $extension = pathinfo($filename, PATHINFO_EXTENSION);
                if (move_uploaded_file($tmpName, $filename) && $extension == $ex) {
                    $return['situation'] = true;
                    switch ($extension) {
                        case 'vcf':
                            $return['message'] = $this->Contact->Addressbook->importContactVcards($filename);
                            $return['filename'] = $filename;
                            break;
                        case 'csv':
                            $return['filename'] = $filename;
                            $return['message'] = $this->Contact->Addressbook->importContactCsv($filename);
                            break;
                    }
                } else {
                    $return['situation'] = false;
                    $return['message'] = "L'extension de votre fichier est incorrect. Veuillez sélectionner un autre fichier.";
                }
                //enregistrer dans un carnet exist
            } else if (!empty($this->request->data['Import']['file']) && !empty($this->request->data['Import']['addressbook_id'])) {
//var_dump($this->request->data['Import']);
                $tmpName = $this->request->data['Import']['file']['tmp_name'];
                $filename = TMP . $this->request->data['Import']['file']['name'];
                $extension = pathinfo($filename, PATHINFO_EXTENSION);
                if (move_uploaded_file($tmpName, $filename) && $extension == $ex) {
                    $return['situation'] = true;
                    switch ($extension) {
                        case 'vcf':
                            $return['message'] = $this->Contact->Addressbook->importContactVcards($filename);
                            $return['filename'] = $filename;
                            break;
                        case 'csv':
                            $return['filename'] = $filename;
                            $return['message'] = $this->Contact->Addressbook->importContactCsv($filename);
                            break;
                    }
                } else {
                    $return['situation'] = false;
                    $return['message'] = "L'extension de votre fichier est incorrect. Veuillez sélectionner un autre fichier.";
                }
                //enregistrer dans un carnet exist
            } else if (!empty($this->request->data['Import']['update_file']) && (!empty($this->request->data['Import']['addressbook_id']) || !empty($addressbook_id) )) {
//var_dump(2);
//var_dump($this->request->data['Import']);die();
                $filename = TMP . $this->request->data['Import']['update_file'];
                $return['filename'] = $filename;
                $return['message'] = $this->Contact->Addressbook->importContactCsv($filename);
                switch ($ex) {
                    case 'vcf':
                        if ($this->Contact->Addressbook->importContactVcards($this->request->data['Import']['update_file'], $this->request->data['Import']['addressbook_id'])) {
                            $return['message'] = __d('default', 'save.ok');
                        } else {
                            $return['message'] = __d('default', 'save.error');
                        }
                        break;
                    case 'csv':
                        if ($this->Contact->Addressbook->importContactCsv($this->request->data['Import']['update_file'], $this->request->data['Import']['addressbook_id'], null, $this->request->data['Import']['organisme_id'])) {
                            $return['message'] = __d('default', 'save.ok');
                        } else {
                            $return['message'] = __d('default', 'save.error');
                        }
                        break;
                }
                if (is_file($this->request->data['Import']['update_file'])) {
                    unlink($this->request->data['Import']['update_file']);
                }
                //nouveau carnet
            } else if (!empty($this->request->data['Import']['update_file']) && !empty($this->request->data['Import']['carnet_nom'])) {

//var_dump(3);
                $addressbook = array();
                $addressbook = array(
                    'Addressbook' => array(
                        'name' => $this->request->data['Import']['carnet_nom'],
                        'description' => $this->request->data['Import']['carnet_description']
                    )
                );
                $nb = $this->Contact->Addressbook->find('count', array(
                    'conditions' => array(
                        'Addressbook.name' => $this->request->data['Import']['carnet_nom']
                    )
                ));
                if (!$nb > 0) {
                    $this->Contact->Addressbook->begin();
                    $this->Contact->Addressbook->create();
                    $savedAddressbook = $this->Contact->Addressbook->save($addressbook);
                    $saved = !empty($savedAddressbook);
                    if ($saved) {
                        $this->Contact->Addressbook->commit();
                        switch ($ex) {
                            case 'vcf':
                                if ($this->Contact->Addressbook->importContactVcards($this->request->data['Import']['update_file'], $savedAddressbook['Addressbook']['id'])) {
                                    $return['message'] = __d('default', 'save.ok');
                                } else {
                                    $return['message'] = __d('default', 'save.error');
                                }
                                break;
                            case 'csv':
                                if ($this->Contact->Addressbook->importContactCsv($this->request->data['Import']['update_file'], $savedAddressbook['Addressbook']['id'], null, $this->request->data['Import']['organisme_id'])) {
                                    $return['message'] = __d('default', 'save.ok');
                                } else {
                                    $return['message'] = __d('default', 'save.error');
                                }
                                break;
                        }
                        if (is_file($this->request->data['Import']['update_file'])) {
                            unlink($this->request->data['Import']['update_file']);
                        }
                    } else {
                        $this->Contact->Addressbook->rollback();
                        $return['message'] = __d('default', 'save.error');
                    }
                }
            }
        }
        return json_encode($return);
    }

    /**
     * chargement la page import_contacts
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function import_contacts($addressbookId, $orgId, $viewOriganisme = null) {
        $this->set('addressbookId', $addressbookId);
        $this->set('orgId', $orgId);
        if ($viewOriganisme != null) {
            $this->Contact->Addressbook->Organisme->id = $orgId;
            $origanismeName = $this->Contact->Addressbook->Organisme->field('name');
            $this->set('origanismeName', $origanismeName);

            $this->Contact->Addressbook->id = $addressbookId;
            $addressbookName = $this->Contact->Addressbook->field('name');
            $this->set('addressbookName', $addressbookName);
            $this->set('viewOriganisme', true);
        }

        $this->set('addressbooks', $this->Session->read('Auth.Addressbook') );
        $this->set('organismes', $this->Contact->Addressbook->Organisme->find('list', array('conditions' => array('Organisme.active' => 1), 'order' => array('Organisme.name'))));
    }

    /**
     * chargement la page import_contacts
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function import_contacts_index() {

        $addressbooks = $this->Session->read('Auth.Addressbook');
        $addressbooks2 = $this->Contact->Addressbook->find(
            'all',
            array(
                'conditions' => array(
                    'Addressbook.active' => 1
                ),
                'order' => array('Addressbook.name'),
                'contain' => array(
                    'Organisme' => array(
                        'order' => array('Organisme.name ASC')
                    )
                )
            )
        );
        $this->set('addressbooks', $addressbooks);


        foreach ($addressbooks2 as $key => $value) {
            if (isset($value) && !empty($value)) {
                $organisme = array();
                foreach ($value['Organisme'] as $keyOrg => $valueOrg) {
                    $organisme[$valueOrg['id']] = $valueOrg['name'];
                }
                $organismeOptions[$value['Addressbook']['id']] = $organisme;
            }
        }

        $this->set('organismes', $organismeOptions);
//        $this->set('organismes', $this->Contact->Addressbook->Organisme->find('list', array('conditions' => array('Organisme.active' => 1), 'order' => array('Organisme.name'))));
    }

    /**
     * Récupération de la liste des contacts
     *
     * @logical-group Flux
     * @logical-group Contact
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function search() {
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $this->autoRender = false;
            $response = array();
            $qdContact = array(
                'recursive' => -1,
                'contain' => false,
                'fields' => array(
                    'Contact.name',
                    'Contact.id',
                    'Contact.citoyen',
                    'Addressbook.id',
                    'Addressbook.name'
                ),
                'joins' => array(
                    $this->Contact->join($this->Contact->Addressbook->alias)
                ),
                'conditions' => array(
                    'Contact.name ILIKE' => '%' . $_GET['term'] . '%'
                ),
                'order' => array(
                    'Addressbook.name',
                    'Contact.name'
                )
            );
            $contacts = $this->Contact->find('all', $qdContact);

            $qdOrg = array(
                'recursive' => -1,
                'contain' => false,
                'fields' => array(
                    'Organisme.id',
                    'Addressbook.name',
                    'Organisme.name'
                ),
                'joins' => array(
                    $this->Contact->Organisme->join($this->Contact->Organisme->Addressbook->alias)
                ),
                'conditions' => array(
                    'Organisme.name ILIKE' => '%' . $_GET['term'] . '%'
                ),
                'order' => array(
                    'Organisme.name'
                )
            );
            $orgs = $this->Contact->Organisme->find('all', $qdOrg);


            $i = 0;
            foreach ($contacts as $contact) {
                $response[$i]['id'] = $contact['Contact']['id'];
                $response[$i]['value'] = $contact['Contact']['name'];
                $response[$i]['label'] = $contact['Contact']['name'];
                $response[$i]['img'] = $contact['Contact']['citoyen'] ? 'citoyen' : 'organisme';
                $response[$i]['category'] = $contact['Addressbook']['name'];
                $i++;
            }


            foreach ($orgs as $org) {
                $response[$i]['id'] = $org['Organisme']['id'];
                $response[$i]['value'] = $org['Organisme']['name'];
                $response[$i]['label'] = $org['Organisme']['name'];
                $response[$i]['img'] = 'organisme';
                $response[$i]['category'] = $org['Addressbook']['name'];
                $i++;
            }


            echo json_encode($response);
        }
        $this->_setOptions();
    }

    /**
     * Récupération de la liste des fiches de contact
     *
     * @logical-group Flux
     * @logical-group Contact
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $contactId
     * @return void
     */
    public function searchContactinfo($contactId) {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $this->autoRender = false;

            $contact = $this->Contact->find(
                    'first', array(
                'fields' => array_merge(
                        $this->Contact->fields(), $this->Contact->Ban->fields(), $this->Contact->Bancommune->fields(), $this->Contact->Fonction->fields()
                ),
                'conditions' => array(
                    'Contact.id' => $contactId
                ),
                'joins' => array(
                    $this->Contact->join('Ban', array('type' => 'LEFT OUTER')),
                    $this->Contact->join('Bancommune', array('type' => 'LEFT OUTER')),
                    $this->Contact->join('Fonction', array('type' => 'LEFT OUTER'))
                ),
                'contain' => false,
                'recursive' => -1
                    )
            );

            if (!empty($contact)) {
                $return = $this->Contact->contactTreated($contact);
            }
        }
        echo json_encode($return);
//        $this->_setOptions();
    }

    /**
     * Récupération de la liste des fiches organismes
     *
     * @logical-group Flux
     * @logical-group Organisme
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $orgId
     * @return void
     */
    public function searchOrganismeinfo($orgId) {

        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $this->autoRender = false;

            $organisme = $this->Contact->Courrier->Organisme->find(
                    'first', array(
                'fields' => array_merge(
                        $this->Contact->Courrier->Organisme->fields(), $this->Contact->Courrier->Organisme->Ban->fields(), $this->Contact->Courrier->Organisme->Bancommune->fields()
                ),
                'conditions' => array(
                    'Organisme.id' => $orgId
                ),
                'joins' => array(
                    $this->Contact->Courrier->Organisme->join('Ban', array('type' => 'LEFT OUTER')),
                    $this->Contact->Courrier->Organisme->join('Bancommune', array('type' => 'LEFT OUTER'))
                ),
                'contain' => false,
                'recursive' => -1
                    )
            );
            $return = array();
            if (!empty($organisme)) {
                $return = $this->Contact->Organisme->organismeTreated($organisme);
            }
        }
        echo json_encode($return);
    }

    /**
     *
     */
    public function getHomonyms() {
        Configure::write('debug', 0);
        $this->autoRender = false;
        $return = array();
        if ($this->RequestHandler->isAjax() && !empty($this->request->data['Contact']['name'])) {
            $return = $this->Contact->getHomonyms($this->request->data['Contact']['name']);
        }
        echo json_encode($return);
    }

    /**
     * Récupération de la liste des contacts
     *
     * @logical-group Flux
     * @logical-group Contact
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function searchAdresse() {
        ini_set('memory_limit', '3000M');
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $this->autoRender = false;
            $response = array();
            $qdBanadresse = array(
                'recursive' => -1,
                'contain' => false,
                'fields' => array(
                    'Banadresse.name',
                    'Banadresse.numero',
                    'Banadresse.rep',
                    'Banadresse.nom_afnor',
                    'Banadresse.canton',
                    'Banadresse.id',
                    'Banadresse.bancommune_id',
                    'Bancommune.name'
                ),
                'joins' => array(
                    $this->Ban->Bancommune->Banadresse->join('Bancommune')
                ),
                'conditions' => array(
                    'Banadresse.name ILIKE' => '%' . $_GET['term'] . '%'
                ),
                'order' => array( 'Bancommune.name ASC', 'Banadresse.name ASC', 'Banadresse.numero ASC' )
            );
            $bansadresses = $this->Ban->Bancommune->Banadresse->find('all', $qdBanadresse);
//debug($bansadresses);
//die();
            $i = 0;
            foreach ($bansadresses as $banadresse) {
                $response[$i]['id'] = $banadresse['Banadresse']['id'];
                $response[$i]['value'] = $banadresse['Banadresse']['name'];
                $response[$i]['label'] = $banadresse['Banadresse']['name'];
                $response[$i]['img'] = 'citoyen';
                $response[$i]['category'] = $banadresse['Bancommune']['name'];
                $i++;
            }
            echo json_encode($response);
        }
        $this->_setOptions();
    }

    /**
     * Récupération de la liste des contacts
     *
     * @logical-group Flux
     * @logical-group Contact
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function searchAdresseSpecifique($banadresseId) {
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $this->autoRender = false;
            $response = array();
            $qdBanadresse = array(
                'recursive' => -1,
                'contain' => false,
                'fields' => array_merge(
                        $this->Ban->Bancommune->Banadresse->fields(), $this->Ban->Bancommune->fields(), $this->Ban->fields()
                ),
                'joins' => array(
                    $this->Ban->Bancommune->Banadresse->join('Bancommune'),
                    $this->Ban->Bancommune->join('Ban')
                ),
                'conditions' => array(
                    'Banadresse.id' => $banadresseId
                ),
                'order' => array(
                    'Banadresse.name'
                )
            );
            $banadresse = $this->Ban->Bancommune->Banadresse->find('first', $qdBanadresse);
//debug($banadresse);
//die();
//            $i = 0;
//            foreach ($bansadresses as $banadresse) {
//                $response[$i]['id'] = $banadresse['Banadresse']['id'];
//                $response[$i]['value'] = $banadresse['Banadresse']['name'];
//                $response[$i]['label'] = $banadresse['Banadresse']['name'];
//                $response[$i]['img'] = 'citoyen';
//                $response[$i]['category'] = $banadresse['Bancommune']['name'];
//                $i++;
//            }
            echo json_encode($banadresse);
        }
        $this->_setOptions();
    }

    /**
     * Récupération de la liste des contacts
     *
     * @logical-group Flux
     * @logical-group Contact
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function searchAdresseNomvoie($communeId = null) {
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $this->autoRender = false;
            $response = array();
            $qdBanadresse = array(
                'recursive' => -1,
                'contain' => false,
                'fields' => array(
                    'Banadresse.name',
                    'Banadresse.numero',
                    'Banadresse.rep',
                    'Banadresse.nom_afnor',
                    'Banadresse.canton',
                    'Banadresse.id',
                    'Banadresse.bancommune_id',
                    'Bancommune.name'
                ),
                'joins' => array(
                    $this->Ban->Bancommune->Banadresse->join('Bancommune')
                ),
                'conditions' => array(
                    'Banadresse.bancommune_id' => $communeId,
                    'Banadresse.nom_afnor ILIKE' => '%' . $_GET['term'] . '%'
                ),
                'order' => array(
                    'Banadresse.nom_afnor'
                )
            );
            $bansadresses = $this->Ban->Bancommune->Banadresse->find('all', $qdBanadresse);
//debug($communeId);
//debug($bansadresses);
//die();
            $i = 0;
            foreach ($bansadresses as $banadresse) {
                $response[$i]['id'] = $banadresse['Banadresse']['id'];
                $response[$i]['value'] = $banadresse['Banadresse']['nom_afnor'];
                $response[$i]['label'] = $banadresse['Banadresse']['nom_afnor'];
                $response[$i]['img'] = 'citoyen';
//                $response[$i]['category'] = $banadresse['Bancommune']['nom_afnor'];
                $i++;
            }
            echo json_encode($response);
        }
        $this->_setOptions();
    }

    /**
     * Ajout d'un contact depuis le formulaire du courrier
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @logical-used-by Flux > Ajout d'un flux
     * @user-profile User
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function addFromCourrier($orgId = null) {
        //        $this->autoRender = false;
        $this->Contact->recursive = -1;
        $this->Jsonmsg->init();
        if (!empty($this->request->data)) {


            if (!empty($this->request->data['Organisme']['name'])) {
                $this->Contact->Organisme->begin();

                $this->request->data['Organisme']['addressbook_id'] = $this->request->data['Contact']['addressbook_id'];
                if (empty($this->request->data['Organisme']['email'])) {
                    $this->request->data['Organisme']['email'] = 'noreply@webgfc.fr';
                }

				$this->request->data['Organisme']['ville'] = strtoupper($this->request->data['Organisme']['ville'] );
                $success = $this->Contact->Organisme->save($this->request->data['Organisme']);

                if ($success) {
                    $this->request->data['Contact']['foreign_key'] = $this->Contact->Addressbook->field('foreign_key', array('Addressbook.id' => $this->request->data['Contact']['addressbook_id']));
                    $this->request->data['Contact']['organisme_id'] = $this->Contact->Organisme->id;
                    if (empty($this->request->data['Contact']['name']) || empty($this->request->data['Contact']['nom'])) {
                        $this->request->data['Contact']['name'] = $this->request->data['Contact']['nom'] = $this->request->data['Organisme']['name'];
                    }
                    if (!empty($this->request->data['Contact']['observations'])) {
                        $this->request->data['Contact']['observations'] = str_replace(array("<p>", "</p>", "<br />"), "\t", quoted_printable_decode($this->request->data['Contact']['observations']));
                    }

					$this->request->data['Contact']['ville'] = strtoupper($this->request->data['Contact']['ville'] );
                    $success = $this->Contact->save($this->request->data) && $success;

                    /* if( $success ) {
                      $this->request->data['Contactinfo']['name'] = 'Fiche '.$this->request->data['Contact']['name'];
                      $this->request->data['Contactinfo']['nom'] = $this->request->data['Contact']['nom'];
                      $this->request->data['Contactinfo']['contact_id'] = $this->Contact->id;
                      $success = $this->Contact->Contactinfo->save($this->request->data) && $success;
                      } */

                    if ($success) {
                        $this->autoRender = false;
                        $return = $this->Contact->Organisme->id;
                        echo $return;
                        $this->Contact->Organisme->commit();
                        //                    $this->Jsonmsg->valid();
                    } else {
                        $this->Contact->Organisme->rollback();
                    }
                }
            } else {
                $this->Contact->begin();
                $this->Jsonmsg->init();
                $this->request->data['Contact']['foreign_key'] = $this->Contact->Addressbook->field('foreign_key', array('Addressbook.id' => $this->request->data['Contact']['addressbook_id']));


                $success = $this->Contact->save($this->request->data);

                // Cas de l'ajout depuis la gestion du flux
                if ($_SERVER['HTTP_REFERER'] != Configure::read('WEBGFC_URL') . '/addressbooks') {

                    if ($success) {
                        $this->autoRender = false;
                        $return = $this->Contact->id;
                        echo $return;
                        $this->Contact->commit();
                    } else {
                        $this->Contact->rollback();
                    }
                } else {
                    // Cas de l'ajout depuis l'administration du carnet d'adresse
                    if ($success) {
                        $this->Contact->commit();
                        $this->Jsonmsg->valid();
                    } else {
                        $this->Contact->rollback();
                    }
                    $this->Jsonmsg->send();
                }
            }
        }
        if (!empty($orgId)) {
            $organisme = $this->Contact->Organisme->find(
                    'first', array(
                'conditions' => array(
                    'Organisme.id' => $orgId
                ),
                'recursive' => -1
                    )
            );

            $this->set('orgId', $orgId);
        } else {
            $organisme = $this->Contact->Organisme->find('list', array('conditions' => array('Organisme.active' => true), 'order' => array('Organisme.name ASC')));
        }
        $this->set('organisme', $organisme);
        $this->set('addressbooks', $this->Session->read('Auth.Addressbook') );
        $this->_setOptions();
    }

    /**
     * Ajout d'un contact depuis le formulaire du courrier
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @logical-used-by Flux > Ajout d'un flux
     * @user-profile User
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function editFromCourrier($id, $typecontact, $orgId = null) {
        //        $this->autoRender = false;
        $this->Contact->recursive = -1;
        $this->Jsonmsg->init();
        if (!empty($this->request->data)) {
//debug($this->request->data);
//die();

            if (!empty($this->request->data['Organisme']['name']) && empty($this->request->data['Contact']['organisme_id'])) {
                $this->Contact->Organisme->begin();
//debug($this->request->data);
                $this->request->data['Organisme']['addressbook_id'] = $this->request->data['Contact']['addressbook_id'];
                if (empty($this->request->data['Organisme']['email'])) {
                    $this->request->data['Organisme']['email'] = 'noreply@webgfc.fr';
                }
				$this->request->data['Organisme']['ville'] = strtoupper($this->request->data['Organisme']['ville'] );
                $success = $this->Contact->Organisme->save($this->request->data['Organisme']);

                if ($success) {
                    $this->request->data['Contact']['foreign_key'] = $this->Contact->Addressbook->field('foreign_key', array('Addressbook.id' => $this->request->data['Contact']['addressbook_id']));
                    $this->request->data['Contact']['organisme_id'] = $this->Contact->Organisme->id;
                    if (empty($this->request->data['Contact']['name']) || empty($this->request->data['Contact']['nom'])) {
                        $this->request->data['Contact']['civilite'] = 'Monsieur'; //FIXME
                        $this->request->data['Contact']['name'] = $this->request->data['Contact']['nom'] = $this->request->data['Organisme']['name'];
                    }
                    if (!empty($this->request->data['Contact']['observations'])) {
                        $this->request->data['Contact']['observations'] = str_replace(array("<p>", "</p>", "<br />"), "\t", quoted_printable_decode($this->request->data['Contact']['observations']));
                    }
					$this->request->data['Contact']['ville'] = strtoupper($this->request->data['Contact']['ville'] );
                    $success = $this->Contact->save($this->request->data) && $success;

                    if ($success) {
                        $this->autoRender = false;
                        $return = $this->Contact->Organisme->id;
                        echo $return;
                        $this->Contact->Organisme->commit();
                        //                    $this->Jsonmsg->valid();
                    } else {
                        $this->Contact->Organisme->rollback();
                    }
                }
            } else {
                $this->Contact->begin();
                $this->Jsonmsg->init();
                $this->request->data['Contact']['foreign_key'] = $this->Contact->Addressbook->field('foreign_key', array('Addressbook.id' => $this->request->data['Contact']['addressbook_id']));

                $success = $this->Contact->save($this->request->data);

                /* if (!empty($this->request->data['Contactinfo'])) {

                  $options = $this->Contact->Addressbook->listTypesVoie();
                  $typevoie = Hash::get( $options['Addressbook']['typevoie'], Hash::get( $this->request->data, 'Contactinfo.typevoie' ));
                  $adresse = Hash::get( $this->request->data, 'Contactinfo.numvoie' ).' '.$typevoie.' '.Hash::get( $this->request->data, 'Contactinfo.nomvoie' );
                  $contactinfoValue = array(
                  'Contactinfo' => array(
                  'contact_id' => $this->Contact->id,
                  'civilite' => $this->request->data['Contact']['civilite'],
                  'nom' => $this->request->data['Contact']['nom'],
                  'prenom' => $this->request->data['Contact']['prenom'],
                  'name' => $this->request->data['Contactinfo']['name'],
                  'email' => trim( $this->request->data['Contactinfo']['email'] ),
                  'tel' => $this->request->data['Contactinfo']['tel'],
                  'portable' => $this->request->data['Contactinfo']['portable']
                  )
                  );
                  $this->Contact->Contactinfo->create($contactinfoValue);
                  $success = $this->Contact->Contactinfo->save() && $success;
                  } */


                // Cas de l'ajout depuis la gestion du flux
                if ($_SERVER['HTTP_REFERER'] != Configure::read('WEBGFC_URL') . '/addressbooks') {

                    if ($success) {
                        $this->autoRender = false;
//debug($this->request);
//                        $return = $this->Contact->id;
//                        echo $return;
                        $this->Contact->commit();
                        $this->Jsonmsg->valid();
                    } else {
                        $this->Contact->rollback();
                    }
                    $this->Jsonmsg->send();
                } else {
                    // Cas de l'ajout depuis l'administration du carnet d'adresse
                    if ($success) {
                        $this->Contact->commit();
                        $this->Jsonmsg->valid();
                    } else {
                        $this->Contact->rollback();
                    }
                    $this->Jsonmsg->send();
                }
            }
        } else {
            if ($typecontact == 'citoyen') {
                $this->request->data = $this->Contact->find(
                        'first', array(
                    'fields' => array_merge(
                            $this->Contact->fields(), $this->Contact->Addressbook->fields(),
//                            $this->Contact->Contactinfo->fields(),
                            $this->Contact->Organisme->fields()
                    ),
                    'conditions' => array(
                        'Contact.id' => $id
                    ),
                    'joins' => array(
                        $this->Contact->join($this->Contact->Addressbook->alias),
//                            $this->Contact->join($this->Contact->Contactinfo->alias),
                        $this->Contact->join($this->Contact->Organisme->alias)
                    ),
                    'contain' => false,
                    'recursive' => -1
                        )
                );
                $this->request->data['Contact']['typecontact'] = 'citoyen';
            } else {

                $this->request->data = $this->Contact->Organisme->find(
                        'first', array(
                    'fields' => array_merge(
                            $this->Contact->Organisme->fields(), $this->Contact->Organisme->Addressbook->fields(), $this->Contact->Organisme->Contact->fields()/* ,
                              $this->Contact->Organisme->Contact->Contactinfo->fields() */
                    ),
                    'conditions' => array(
                        'Organisme.id' => $id
                    ),
                    'joins' => array(
                        $this->Contact->Organisme->join($this->Contact->Organisme->Addressbook->alias),
                        $this->Contact->Organisme->join($this->Contact->Organisme->Contact->alias)/* ,
                      $this->Contact->Organisme->Contact->join($this->Contact->Organisme->Contact->Contactinfo->alias) */
                    ),
                    'recursive' => -1
                        )
                );

                $this->set('orgId', $id);

                $this->request->data['Contact']['typecontact'] = 'organisme';
            }
        }
        $organisme = $this->Contact->Organisme->find('list', array('conditions' => array('Organisme.active' => true), 'order' => array('Organisme.name ASC')));
        $this->set('organisme', $organisme);
        $this->set('addressbooks', $this->Session->read('Auth.Addressbook') );
        $this->_setOptions();
    }

    /**
     * Récupération de la liste des contacts
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @access public
     * @param integer $addressbook_id identifiant du carnet d'adresse
     * @return void
     */
    public function get_contactform() {
        //On doit pouvoir obtenir les résultats dès le premier accès à la page
        if (!empty($this->params['named']['page'])) {
            $this->request->data = $this->Session->read('paginationModel.rechercheContact');
        }
        $this->Session->write('paginationModel.rechercheContact', $this->request->data);
        $contacts = array();
        if (!empty($this->request->data)) {
//            $this->Session->write('paginationModel.rechercheContactCarnet', $this->request->data);
            $querydata = $this->Contact->search($this->request->data, null, null);
        } else {
            $this->Contact->recursive = -1;
            $querydata = array(
                'order' => array(
                    'Contact.nom ASC',
                    'Contact.prenom ASC'
                ),
                'contain' => array(
                    'Organisme',
                    'Addressbook'
                ),
                'conditions' => array(
                    'Contact.active' => true
                ),
                'limit' => 20
            );
        }

        $this->paginate = $querydata;

        $contacts = $this->paginate($this->Contact);
        foreach ($contacts as $i => $contact) {

            // Droits d'accès aux actions edit et delete
            $contact['right_edit'] = true;
            $contact['right_delete'] = true;
            if (Configure::read('Conf.SAERP')) {
                $contact['right_export'] = true;
            }
            $contacts[$i] = $contact;

            // Ajout du nom de l'organisme dans le tableau reprenant les contacts
            if (!empty($contact['Organisme']['name'])) {
                $contacts[$i]['Contact']['organisme_name'] = $contact['Organisme']['name'];
            } else {
                $contacts[$i]['Contact']['organisme_name'] = '';
            }
            if (!empty($contact['Addressbook']['name'])) {
                $contacts[$i]['Contact']['carnet_name'] = $contact['Addressbook']['name'];
            } else {
                $contacts[$i]['Contact']['carnet_name'] = '';
            }
        }

        $this->set('contacts', $contacts);
        $this->_setOptions();
    }

    public function view($id = null) {
        $contact = $this->Contact->find('first', array(
            'conditions' => array('Contact.id' => $id),
            'recursive' => -1,
            'contain' => array('Organisme')
        ));
        $this->set('contact', $contact);
    }

    public function addParCarnet() {
        $this->Contact->recursive = -1;
        $this->Jsonmsg->init();
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            if (!empty($this->request->data['Contact']['banadresse'])) {
                $adressecomplete = Hash::get($this->request->data, 'Contact.numvoie') . ' ' . Hash::get($this->request->data, 'Contact.banadresse');
            } else {
                $adressecomplete = Hash::get($this->request->data, 'Contact.numvoie') . ' ' . Hash::get($this->request->data, 'Contact.nomvoie');
            }
            if (!empty($this->request->data['Contact']['observations'])) {
                $this->request->data['Contact']['observations'] = str_replace(array("<p>", "</p>", "<br />"), "\t", quoted_printable_decode($this->request->data['Contact']['observations']));
            }
            $this->request->data['Contact']['adresse'] = $adressecomplete;
            $this->Contact->begin();
			$this->request->data['Contact']['ville'] = strtoupper($this->request->data['Contact']['ville'] );
            $success = $this->Contact->save($this->request->data);
            // Cas de l'ajout depuis la gestion du flux
            if ($_SERVER['HTTP_REFERER'] != Configure::read('WEBGFC_URL') . '/addressbooks') {

                if ($success) {
                    $this->autoRender = false;
                    $this->Contact->commit();
                    $this->Jsonmsg->valid();
                } else {
                    $this->Contact->rollback();
                }
                $this->Jsonmsg->send();
            } else {
                // Cas de l'ajout depuis l'administration du carnet d'adresse
                if ($success) {
                    $this->Contact->commit();
                    $this->Jsonmsg->valid();
                } else {
                    $this->Contact->rollback();
                }
                $this->Jsonmsg->send();
            }
        }

        $addressbooks = $this->Session->read('Auth.Addressbook');

        $organismes = $this->Contact->Organisme->listOrganismes();
        $civilite = $this->Session->read( 'Auth.Civilite');

        $this->set(compact('civilite'));
        $this->set('organismes', $organismes);
        $this->set('addressbooks', $addressbooks);

        $this->_setOptions();

		$sansOrganismeByDefault = Configure::read('Sansorganisme.id');
		$this->set('sansOrganismeByDefault', $sansOrganismeByDefault);
    }

    public function getOrganismeOption($id = null) {
        $this->autoRender = false;
        $organismes = $this->Contact->Organisme->find('list', array(
            'fields' => array('Organisme.id', 'Organisme.name'),
            'conditions' => array('Organisme.active' => TRUE,
                'Organisme.addressbook_id' => $id),
            'recursive' => -1,
            'order' => 'Organisme.name ASC',
            'contain' => false));
        return json_encode($organismes);
    }

    /**
     * Export des carnets d'adresse au format CSV
     *
     * @logical-group Addressbooks
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function export($contact_id = null) {
        ini_set('memory_limit', '3000M');

//        $this->autoRender = false;
//debug($this->request->params);
//debug(Hash::expand( $this->request->params['named'] ) );
//die();
        $conditions = array(
            'Contact.active' => true
        );

        $querydata = array(
            'conditions' => array(
                $conditions
            ),
            'order' => array(
                'Contact.name ASC'
            ),
            'contain' => array(
                'Fonction',
                'Event',
                'Operation',
                'Organisme' => array(
                    'Operation',
                    'Activite'
                )
            )
        );

        $events = $this->Contact->Event->find('list', array('conditions' => array('Event.active' => true)));
        $eventColumns = array();
        foreach ($events as $eventsIds => $eventName) {
            $eventColumns[$eventsIds] = strtoupper($eventName);
        }
        $this->set('eventColumns', $eventColumns);

        if (!empty($contact_id)) {
            $querydata['conditions'] = array_merge(
                    $querydata['conditions'], array(
                'Contact.id' => $contact_id
                    )
            );

            $results = $this->Contact->find('first', $querydata);


            $civiliteOptions = $this->Session->read('Auth.Civilite' );
//            $results['Contact']['events'] = implode( ' - ', Hash::extract($results, 'Activite.{n}.name') );
            $results['Contact']['civilite'] = $civiliteOptions[$results['Contact']['civilite']];
            $results['Contact']['operationscontact'] = implode(' - ', Hash::extract($results, 'Operation.{n}.name'));
            $results['Contact']['operationsorg'] = implode(' - ', Hash::extract($results, 'Organisme.Operation.{n}.name'));
            $results['Contact']['operationsactivites'] = implode(' - ', Hash::extract($results, 'Organisme.Activite.{n}.name'));

            if (!empty($results['Contact']['observations'])) {
                $results['Contact']['observations'] = preg_replace("/[\n\r\"]/", " ", $results['Contact']['observations']);
            }
            foreach ($eventColumns as $eventID => $eventName) {
                $results['Contact']['events'][$eventID] = '';
                foreach ($results['Event'] as $i => $event) {
//debug($contact);
//die();
                    $results['Contact']['events'][$eventID] = '';
//                    foreach( $contact['Event'] as $ev  => $event) {
                    if ($eventID == $event['id']) {
                        $results['Contact']['events'][$eventID] = $eventName;
                    }
//                    }
                }
            }
            $this->set('results', $results);
        } else {
            if (isset($this->request->data['SearchContact']['organisme_id']) && !empty($this->request->data['SearchContact']['organisme_id'])) {
                $querydata = $this->Contact->search($this->request->data, null, array($this->request->data['SearchContact']['organisme_id']));
            } else {
                $querydata = $this->Contact->search($this->request->data, null, array());
            }

            $resultats_tmp = $this->Contact->find('all', $querydata);
            $resultats = array();
            foreach ($resultats_tmp as $i => $results) {
                $results['Contact']['operationscontact'] = implode(' - ', Hash::extract($results, 'Operation.{n}.name'));
                $results['Contact']['operationsorg'] = implode(' - ', Hash::extract($results, 'Organisme.Operation.{n}.name'));
                $results['Contact']['operationsactivites'] = implode(' - ', Hash::extract($results, 'Organisme.Activite.{n}.name'));

                if (!empty($results['Contact']['observations'])) {
                    $results['Contact']['observations'] = preg_replace("/[\n\r\"]/", " ", $results['Contact']['observations']);
                }
                if (isset($results['Event']) && !empty($results['Event'])) {
                    foreach ($eventColumns as $eventID => $eventName) {
                        $results['Contact']['events'][$eventID] = '';
                        foreach ($results['Event'] as $i => $event) {
                            if ($eventID == $event['id']) {
                                $results['Contact']['events'][$eventID] = $eventName;
                            }
                        }
                    }
                }
                $resultats[] = $results;
            }
            $this->set('results', $resultats);
        }
//debug($results);
//die();

        $this->_setOptions();
        $this->layout = '';
    }

    public function searchContacts() {
        if ($this->Session->read('searchAddressbook') != null) {
            $this->paginate = $this->Session->read('searchAddressbook');
        } else {
            $querydata = $this->request->data;
            $this->paginate = $querydata['contacts'];
            $this->Session->write('searchAddressbook', $this->paginate);
        }
        $contacts_tmp = $this->paginate($this->Contact);

		$secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
		$desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
		$isAdmin = false;
		$groupName = $this->Session->read('Auth.User.Desktop.Profil');
		if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
			$isAdmin = true;
		}
        foreach ($contacts_tmp as $i => $contact) {
            $contacts[$i]['name'] = $contact['Contact']['nom'].' '.$contact['Contact']['prenom'] . ' ( '.$contact['Contact']['ville'].' ) ';
            $contacts[$i]['edit'] = '<a href="#" class="editContact" id="contact_' . $contact['Contact']["id"] . '" title="Modifier"><i class="fa fa-pencil" aria-hidden="true" style="color:#5397a7"></i></a>';
			if( $isAdmin ) {
				$contacts[$i]['delete'] = '<a href="#" class="deleteContact" id="contact_' . $contact['Contact']["id"] . '" title="Supprimer" organismeId ="' . $contact['Contact']["organisme_id"] . '"><i class="fa fa-trash" aria-hidden="true" style="color:red"></i></a>';
			}
            $contacts[$i]['exportcsv'] = '<a href="#" class="exportContact" id="contact_' . $contact['Contact']["id"] . '" title="Exporter"><i class="fa fa-download" aria-hidden="true" style="color:#5397a7"></i></a>';
        }
        if (!empty($contacts)) {
            $this->set('contacts', $contacts);
        } else {
            $this->set('contacts', array());
        }
    }

    public function getOrganismeValues($id = null) {
        $this->autoRender = false;
        $organismes = $this->Contact->Organisme->find('first', array(
            'conditions' => array(
                'Organisme.id' => $id),
            'recursive' => -1,
            'contain' => false));
        return json_encode($organismes);
    }

    /**
     *
     * @param type $courrierId
     * @param type $desktopId
     */
    public function search_contacts_courriers( $orgId ) {
        $querydata = array();
        $querydata['conditions'] = array();
        $querydata['contain'] = false;
        $querydata['recursive'] = -1;
        $this->set('contacts', array());
        if( !empty($this->request->data ) ) {
            $querydata = $this->Contact->search( $this->request->data, null );
        }
		$fonctions = $this->Contact->Fonction->find('list', array('order' => array('Fonction.name ASC'), 'conditions' => array('Fonction.active' => true)));
		$titres = $this->Contact->Titre->find('list',array('order' => array('Titre.name ASC'), 'contain' => false ) ) ;

        $querydata['conditions'][] = array('Contact.active' => true, 'Contact.organisme_id' => $orgId );
        $contacts_tmp = $this->Contact->find('all', $querydata );

        $i=0;
        $contacts = array();
        foreach($contacts_tmp as $value){
            $contacts[$i]['id'] = $value['Contact']['id'];
            $contacts[$i]['name'] = $value['Contact']['name'];
            $contacts[$i]['ville'] = $value['Contact']['ville'];
            $contacts[$i]['fonction'] = $fonctions[$value['Contact']['fonction_id']];
            $contacts[$i]['titre'] = $titres[$value['Contact']['titre_id']];
            $i++;
        }

        $this->set('contacts', $contacts);
        $this->set('orgId', $orgId);
    }


    /**
     * Focntion permettant de retourner l'ID du contact possédant le nom de Sans contact
     * @param type $orgId
     */
    public function searchSansContact( $orgId ) {
        $this->autoRender = false;
        $contact = $this->Contact->find(
            'first',
            array(
                'conditions' => array(
                    'Contact.organisme_id' => $orgId,
                    'Contact.name ILIKE '=> '%Sans contact%'
                ),
                'contain' => false
            )
        );

        return json_encode( $contact );
    }

}
