<?php

/**
 * Templatemails
 *
 * Templatemails controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class TemplatemailsController extends AppController {

	/**
	 * Controller name
	 *
	 * @access public
	 * @var string
	 */
	public $name = 'Templatemails';

    public $uses = array('Templatemail', 'Type');
    /**
	 * Gestion des emails interface graphique)
	 *
	 * @logical-group Scanemails
	 * @user-profil Admin
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
                    '<a href="/environnement/index/0/admin">'.__d('menu', 'Administration', true).'</a>',
                    __d('menu', 'templateMails', true)
                ));
	}

	/**
	 * Ajout d'un email
	 *
	 * @logical-group Templatemails
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function add() {

        $templatemails = $this->Templatemail->find( 'all' );


		if (!empty($this->request->data)) {
			$json = array(
				'message' => __d('default', 'save.error'),
				'success' => false
			);

			$templatemail = $this->request->data;


			$this->Templatemail->begin();
			$this->Templatemail->create($templatemail);
			if ($this->Templatemail->save()) {
				$json['success'] = true;
			}
			if ($json['success']) {
				$this->Templatemail->commit();
				$json['message'] = __d('default', 'save.ok');
			} else {
				$this->Templatemail->rollback();
			}
			$this->Jsonmsg->sendJsonResponse($json);
		}
	}

	/**
	 * Edition d'un type
	 *
	 * @logical-group Templatemails
	 * @user-profile Admin
	 *
	 * @access public
	 * @param integer $id identifiant du type
	 * @throws NotFoundException
	 * @return void
	 */
	public function edit($id) {

		if (!empty($this->request->data)) {
//                  debug($this->request->data['Templatemail']['object']);
//            $this->request->data['Templatemail']['object'] = str_replace(array("\r\n","\n"),'<br />',$this->request->data['Templatemail']['object']);
//            $this->request->data['Templatemail']['object'] = strip_tags($this->request->data['Templatemail']['object']);
            $this->Jsonmsg->init();
			$this->Templatemail->create();
            $templatemail = $this->request->data;

			if ($this->Templatemail->save($templatemail)) {
				$this->Jsonmsg->valid();
			}
			$this->Jsonmsg->send();
		} else {
			$this->request->data = $this->Templatemail->find('first', array('conditions' => array('Templatemail.id' => $id)));

//            $this->request->data['Templatemail']['object'] = str_replace(array("<br />"),"\n",$this->request->data['Templatemail']['object']);

			if (empty($this->request->data)) {
				throw new NotFoundException();
			}
		}

        $templatemails = $this->Templatemail->find( 'all' );
	}

	/**
	 * Suppression d'un email
	 *
	 * @logical-group Templatemails
	 * @user-profil Admin
	 *
	 * @access public
	 * @param integer $id identifiant du scanemail
	 * @return void
	 */
	public function delete($id = null) {
		$this->Jsonmsg->init(__d('default', 'delete.error'));
		$this->Templatemail->begin();
		if ($this->Templatemail->delete($id)) {
			$this->Templatemail->commit();
			$this->Jsonmsg->valid(__d('default', 'delete.ok'));
		} else {
			$this->Templatemail->rollback();
		}
		$this->Jsonmsg->send();
	}


    /**
	 * Récupération de la liste des mails à scruter (ajax)
	 *
	 * @logical-group Templatemails
	 * @user-profil Admin
	 *
	 * @access public
	 * @return void
	 */
	public function getTemplatemails() {
		$templatemails_tmp = $this->Templatemail->find(
            "all",
            array(
                'order' => 'Templatemail.name'
            )
        );
        $templatemails = array();
        foreach ($templatemails_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $templatemails[] = $item;
        }
        $this->set(compact('templatemails') );

        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
	}


}

?>
