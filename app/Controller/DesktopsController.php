<?php

App::uses('AppController', 'Controller');

/**
 * Rôles
 *
 * Desktops controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class DesktopsController extends AppController {

    /**
     * Controller uses
     *
     * @access public
     * @var array
     */
    public $uses = array('Desktop');

    /**
     * Controller helpers
     *
     * @access public
     * @var array
     */
    public $helpers = array('DataAcl.DataAcl');

    /**
     * Controller components
     *
     * @access public
     * @var array
     */
    public $components = array('Rights');

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('nextFlux');
    }

    /**
     * Ajout d'un rôle pour un utilisateur
     *
     * @logical-group Rôle
     * @user-profile Admin
     *
     * @access public
     * @param integer $userId identifiant de l'utilisateur
     * @param integer $mainDesktop identifiant du rôle principal
     * @return void
     */
    public function add($userId, $mainDesktop = false) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Desktop->create($this->request->data);

            $this->Desktop->begin();
            if ($this->Desktop->save()) {

                // Lorsqu'on ajoute un rôle, on crée le bureau attenant
                $dktpId = $this->Desktop->id;
                $desktopmanager['Desktopmanager']['name'] = 'Bureau ' . $this->request->data['Desktop']['name'];
                $desktopmanager['Desktopmanager']['isautocreated'] = true;
                $this->Desktop->Desktopmanager->create();
                $newDesktopmanager = $this->Desktop->Desktopmanager->save($desktopmanager);
                // A la création de l'utilisateur, une fois le bureau créé on l'associe au rôle défini
                if ($newDesktopmanager) {
                    $dktpmngId = $this->Desktop->Desktopmanager->id;
                    $dIddMId['DesktopDesktopmanager']['desktop_id'] = $dktpId;
                    $dIddMId['DesktopDesktopmanager']['desktopmanager_id'] = $dktpmngId;
                    $this->Desktop->DesktopDesktopmanager->create();
                    $this->Desktop->DesktopDesktopmanager->save($dIddMId);
                }


                $desktopsuser = $this->Desktop->DesktopsUser->create();
//				$desktopsuser['DesktopsUser']['main_desktop'] = $mainDesktop;
//				$desktopsuser['DesktopsUser']['active'] = true;
                $desktopsuser['DesktopsUser']['desktop_id'] = $this->Desktop->id;
                $desktopsuser['DesktopsUser']['user_id'] = $userId;
//debug($desktopsuser);
//die();
                if ($this->Desktop->DesktopsUser->save($desktopsuser)) {
                    $this->Jsonmsg->valid();
                }
            }
            if ($this->Jsonmsg->json['success']) {
                $this->Desktop->commit();
            } else {
                $this->Desktop->rollback();
            }
            $this->Jsonmsg->send();
        } else {
            $this->set('groups', $this->Desktop->Profil->find('list', array('conditions' => array('id >' => 2, 'Profil.active' => true))));
            $this->set('services', $this->Desktop->Service->generateTreeList(array(), null, null, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
            $this->set('userId', $userId);
        }
    }

    /**
     * Edition d'un rôle
     *
     * @logical-group Rôle
     * @user-profile Admin
     *
     * @access public
     * @param integer $desktopId identifiant du rôle
     * @return void
     */
    public function edit($desktopId) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Desktop->create($this->request->data);

            $changeProfil = false;
            $prevProfilId = $this->Desktop->field('Desktop.profil_id', array('Desktop.id' => $this->request->data['Desktop']['id']));
            if ($prevProfilId == $this->request->data['Desktop']['profil_id']) {
                $changeProfil = true;
            }

            $desktopmanagerId = $this->Desktop->getDesktopManager( $this->request->data['Desktop']['id'] );
            $inEtape = $this->Desktop->Composition->find('count', array(
                'conditions' => array(
                    'Composition.trigger_id' => $desktopmanagerId
                )
            ));
//$this->Log( $this->request->data );
            if (($this->request->data['Desktop']['profil_id'] != 3) || ($this->request->data['Desktop']['profil_id'] == 3 && $inEtape == 0)) {
                $this->Desktop->begin();
                if ($this->Desktop->save()) {
                    $aroId = $this->Acl->Aro->field('Aro.id', array('model' => 'Desktop', 'foreign_key' => $this->request->data['Desktop']['id']));
                    $valid = array();
                    $hasSelfAcl = $this->Acl->Aro->Permission->find('first', array('conditions' => array('Permission.aro_id' => $aroId)));
                    if (!empty($hasSelfAcl)) {
                        $valid['Acl'] = $this->Acl->Aro->Permission->deleteAll(array('Permission.aro_id' => $aroId));
                    }

                    $this->loadModel('Right');
                    $hasSelfRight = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $aroId)));
                    if (!empty($hasSelfRight)) {
//                        $valid['AclSimplified'] = $this->Right->deleteAll(array('Right.aro_id' => $aroId));
                        //					$valid['AclSimplified'] = $this->Right->delete($hasSelfRight['Right']['id']);
                    }


                    if (!in_array(false, $valid, true)) {
                        $this->Jsonmsg->valid();
                        $this->Acl->flushcache();
                    }
                }
            } else {
                $this->Jsonmsg->init(__d('desktop', 'Desktop.edit.errorIni'));
            }
            if ($this->Jsonmsg->json['success']) {
                $this->Desktop->commit();
            } else {
                $this->Desktop->rollback();
            }
            //$this->Jsonmsg->set($inEtape);
            $this->Jsonmsg->send();

            // On enregistre dans le journal
            $desktop = $this->Desktop->find(
                'first',
                array(
                    'conditions' => array(
                        'Desktop.id' => $desktopId
                    ),
                    'contain' => false
                )
            );
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $journalmsg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le profil ".$desktop['Desktop']['name']." le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $journalmsg, 'info');
        } else {
            $this->request->data = $this->Desktop->find(
                    'first', array(
                'conditions' => array(
                    'Desktop.id' => $desktopId
                ),
                'contain' => array(
                    'Service'
                )
                    )
            );
//			$this->request->data = $this->Desktop->read(null, $desktopId);
            $this->set('groups', $this->Desktop->Profil->find('list', array('conditions' => array('id >' => 2, 'active' => true))));
            $services = $this->Desktop->Service->generateTreeList(array(), null, null, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            $this->set('services', $services);

            $srvs = $this->Desktop->getServicesList($desktopId);
            $selectedServices = array();
            foreach ($srvs as $k => $v) {
                $selectedServices[$k] = $services[$k];
            }
            $this->set('selectedServices', $selectedServices);
            $this->set('id', $desktopId);
        }
    }

    /**
     * Gestion des habilitations d'un rôle (interface graphique)
     *
     * @logical-group Rôle
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du rôle
     * @param string $mode choix du type d'habilitations (func, func_classique, data, meta)
     * @return void
     */
    public function setRights($id = null, $mode = 'data') {
        if (!empty($this->request->data)) {

//$this->log($this->request->data);
            $this->Jsonmsg->init();
            $this->loadModel('Right');
            if (!empty($this->request->data['Acl']['Type'])) {
                //Enregistrement en mode classique (a supprimer dans la prochaine version)
                if ($this->request->data['Acl']['Type'] == 'Acl') {
                    if ($this->Acl->setRights($this->request->data['Requester'], $this->request->data['Rights'])) {
                        $qdAro = array(
                            'conditions' => array(
                                'model' => key($this->request->data['Requester']),
                                'foreign_key' => current($this->request->data['Requester'])
                            )
                        );
                        $aro = $this->Acl->Aro->find('first', $qdAro);
                        $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $aro['Aro']['id'])));
                        if (empty($rights)) {
                            $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $aro['Aro']['parent_id'])));
                            $rights['Right']['id'] = null;
                            $rights['Right']['aro_id'] = $aro['Aro']['id'];
                            $rights['Right']['created'] = null;
                            $rights['Right']['modified'] = null;
                            $rights['Right']['model'] = 'Desktop';
                            $rights['Right']['foreign_key'] = current($this->request->data['Requester']);
                        }
                        $rights['Right']['mode_classique'] = true;
                        $this->Right->create($rights);
                        $saved = $this->Right->save();
                        if (!empty($saved)) {
                            $this->Jsonmsg->valid();
                        }
                    }
                } else if ($this->request->data['Acl']['Type'] == 'DataAcl') {
                    if ($this->SessionDataAcl->setRights($this->request->data['Requester'], $this->request->data['Rights'])) {
                        $this->Jsonmsg->valid();
                        $this->SessionDataAcl->flushCache();
                    }
                }
                //Enregistrement en mode simplifié
            } else if (!empty($this->request->data['Right'])) {



                $aro_id = $this->request->data['Right']['aro_id'];
                $aro = $this->Acl->Aro->find('first', array('recursive' => -1, 'conditions' => array('id' => $aro_id)));

                $computedRights = $this->Right->saveRightSets($aro_id, $this->Rights->parseRightsData($this->request->data['Right']), true);


//					$this->ldebug($computedRights);
//					$this->ldebug($computedRights['controllers/Courriers/getMeta']);
//					$this->ldebug($this->Rights->parseRightsData($this->request->data['Right']));

                if (!empty($computedRights)) {
                    $valid = array();
                    $this->Acl->Aro->Permission->deleteAll(array('aro_id' => $aro['Aro']['id']));
                    foreach ($computedRights as $key => $val) {
                        if ($val) {
                            $valid[] = $this->Acl->allow($aro['Aro'], $key);
                        } else {
                            $valid[] = $this->Acl->deny($aro['Aro'], $key);
                        }
                    }
                    if (!in_array(false, $valid, true)) {
                        $this->Jsonmsg->valid();
                        $this->Acl->flushCache();
                    }
                }
            }
			if( isset( $this->request->data['Requester'] ) ) {
				$this->loadModel('DesktopsService');
				$desktopsService = $this->DesktopsService->find(
					'first',
					array(
						'conditions' => array(
							'DesktopsService.id' => $this->request->data['Requester']['DesktopsService']
						),
						'contain' => array(
							'Desktop'
						),
						'recursive' => -1
					)
				);

				$this->loadModel('Journalevent');
				$datasSession = $this->Session->read('Auth.User');
				$msg = "L'utilisateur " . $this->Session->read('Auth.User.username') . " a édité les droits du profil " . $desktopsService['Desktop']['name'] . " le " . date('d/m/Y à H:i:s');
				$this->Journalevent->saveDatas($datasSession, $this->action, $msg, 'info', array());
			}
            $this->Jsonmsg->send();
        } else {
            if ($id != null) {
                if ($mode == 'func') {
                    $this->loadModel('Right');
                    $aro = array('model' => 'Desktop', 'foreign_key' => $id);
                    $aro_id = $this->Acl->Aro->field('id', $aro);
                    $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $aro_id)));
                    if (empty($rights)) {
                        $parent_id = $this->Acl->Aro->field('parent_id', $aro);
                        $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $parent_id)));
                        $rights['Right']['id'] = null;
                        $rights['Right']['aro_id'] = $aro_id;
                        $rights['Right']['created'] = null;
                        $rights['Right']['modified'] = null;
                    }
                    $this->set('rights', $rights);

                    $qdGid = array(
                        'fields' => array(
                            'Profil.id'
                        ),
                        'conditions' => array(
                            'Desktop.id' => $id,
                            'NOT' => array(
// 								'Profil.id' => array(ADMIN_GID, DISP_GID)
                                'Profil.id' => array(ADMIN_GID)
                            )
                        ),
                        'contain' => array('Profil')
                    );
                    $gid = $this->Desktop->find('first', $qdGid);
                    $this->set('editableProfil', !empty($gid));

                    $this->set('tabsForSets', json_encode($this->Right->getTabsForSets()));
                    $this->set('envsForSets', json_encode($this->Right->getEnvsForSets()));
                    $this->set('ctxsForSets', json_encode($this->Right->getCtxsForSets()));
                    $this->set('wkfsForSets', json_encode($this->Right->getWkfsForSets()));
                } else if ($mode == 'func_classique') {
                    set_time_limit(120);
                    //func
                    $node = $this->Acl->Aro->node(array('model' => 'Desktop', 'foreign_key' => $id));
                    $rights = array();
                    $rights[] = $this->Acl->getRightsGrid($node[0]);
                    $this->set('rights', $rights);
                } else if ($mode == 'data') {
                    //data
                    $daros = $this->Desktop->getDaros($id);
                    $dataRights = array();
                    $options = array(
                        'actions' => array('read'),
                        'models' => array('Type', 'Soustype')
                    );

                    $servicesNames = array();
                    foreach ($daros as $daro) {
                        $dataRights[$daro['Daro']['foreign_key']] = $this->DataAcl->getRightsGrid($daro, array(), $options);
                        $serviceId = $this->Desktop->DesktopsService->field('DesktopsService.service_id', array('DesktopsService.id' => $daro['Daro']['foreign_key']));
                        if( !empty($serviceId) ) {
                            $servicesNames[$daro['Daro']['foreign_key']] = $this->Desktop->Service->field('Service.name', array('Service.id' => $serviceId));
                        }
                    }
                    $this->set('dataRights', $dataRights);
                    $this->set('servicesNames', $servicesNames);
                } else if ($mode == 'meta') {
                    //meta
                    $daros = $this->Desktop->getDaros($id);
                    $metaRights = array();
                    $options = array(
                        'actions' => array('read', 'update'),
                        'models' => array('Metadonnee')
                    );
                    $servicesNames = array();
                    foreach ($daros as $daro) {
                        $metaRights[$daro['Daro']['foreign_key']] = $this->DataAcl->getRightsGrid($daro, array(), $options);
                        $serviceId = $this->Desktop->DesktopsService->field('DesktopsService.service_id', array('DesktopsService.id' => $daro['Daro']['foreign_key']));
                        if( !empty($serviceId) ) {
                            $servicesNames[$daro['Daro']['foreign_key']] = $this->Desktop->Service->field('Service.name', array('Service.id' => $serviceId));
                        }
                    }
                    $this->set('metaRights', $metaRights);
                    $this->set('servicesNames', $servicesNames);
                }

                $profil = array('3' => 'creation',
                    '4' => 'validation',
                    '5' => 'edition',
                    '6' => 'documentation',
                    '7' => 'aiguillage'
                );
                $desktop = $this->Desktop->field('profil_id', array('Desktop.id' => $id));
                if ($desktop > 7) {
                    $this->set('rolePers', true);
                    $parentProfilId = $this->Desktop->Profil->field('profil_id', array('Profil.id' => $desktop));
                    $this->set('profil', $profil[$parentProfilId]);
                } else {

                    $this->set('profil', $profil[$desktop]);
                }
                $this->set('requester', array('model' => 'Desktop', 'foreign_key' => $id));
                $this->set('mode', $mode);
            }
        }
    }

    /**
     * Gestion des délégations d'un rôle
     *
     * @logical-group Rôle
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du rôle
     * @param integer $userId identifiant de l'utilisateur
     * @param boolean $unset détermine si l'on doit appliquer ou terminer une délégation
     * @param boolean $plandeleg détermin si la délégation est planifiée
     * @return void
     */
    public function setDeleg($id = null, $userId = null, $unset = false, $plandeleg = false) {
        if ($id != null) {
            $querydata = array(
                'conditions' => array(
                    'DesktopsUser.desktop_id' => $id,
                    'DesktopsUser.delegation' => true
                )
            );
            $desktopuser = $this->Desktop->DesktopsUser->find('all', $querydata);
            $this->set('desktopuser', $desktopuser);
//            debug($desktopuser);

            if (empty($desktopuser)) {
                $this->loadModel('Plandelegation');
                $desktopuser = $this->Plandelegation->find(
                        'all', array(
                    'conditions' => array(
                        'Plandelegation.desktop_id' => $id
                    ),
                    'contain' => array(
                        'User' => array(
                            'Desktop'
                        )
                    )
                        )
                );
                $this->set('desktopuser', $desktopuser);
            }

            if ($userId != null && $unset) {
                $this->Jsonmsg->init(__d('default', 'delete.error'));
                // Suppression des délégations planifiées
                if ($plandeleg) {
                    $this->loadModel('Plandelegation');
                    $plandelegationId = $this->Plandelegation->field('Plandelegation.id', array(array('Plandelegation.user_id' => $userId, 'Plandelegation.desktop_id' => $id)));
                    if ($this->Plandelegation->delete($plandelegationId)) {
                        $this->Jsonmsg->valid(__d('default', 'delete.ok'));
                    }
                } else {

                    // Suppression des délégations simples
                    $delegationId = $this->Desktop->DesktopsUser->field(
                            'DesktopsUser.id', array(
                        array(
                            'DesktopsUser.user_id' => $userId,
                            'DesktopsUser.desktop_id' => $id,
                            'DesktopsUser.delegation' => true
                        )
                            )
                    );

                    if ($this->Desktop->DesktopsUser->delete($delegationId)) {
                        $this->Jsonmsg->valid(__d('default', 'delete.ok'));
                    }
                }
                $this->Jsonmsg->send();
            } else if (isset($this->request->data['admin']) || empty($this->request->data)) {
                $qdUsers = array(
                    'fields' => array_merge(
                            $this->Desktop->User->fields(), $this->Desktop->fields()
                    ),
                    'order' => array('User.nom ASC'),
                    'conditions' => array(
                        'User.active' => true/*,
                        'Desktop.profil_id <>' => ADMIN_GID*/
                    ),
                    'contain' => false,
                    'joins' => array(
                        $this->Desktop->User->join($this->Desktop->alias)
                    )
                );
                $users = $this->Desktop->User->find('all', $qdUsers);
//debug($users);


                $alldesktopuser = Set::classicExtract($desktopuser, '{n}.DesktopsUser.user_id');
//debug($desktopuser);

                $queyrUser = array(
                    'conditions' => array(
                        'User.id' => $alldesktopuser
                    ),
                    'contain' => array(
                        'Desktop'
                    )
                );
                $userDesktopNameDelegated = $this->Desktop->User->find('all', $queyrUser);
                $this->set('userDesktopNameDelegated', $userDesktopNameDelegated);
                if (isset($this->request->data['admin'])) {
                    $this->set('admin', true);
                }
                /* $desktopDelegated = $this->Desktop->find(
                  'all',
                  array(
                  'fields' => array_merge(
                  $this->Desktop->fields(),
                  $this->Desktop->User->fields()
                  ),
                  'conditions' => array(
                  'Desktop.id' => $alldesktopuser
                  ),
                  'joins' => array(
                  $this->Desktop->join( 'User' )
                  ),
                  'recursive' => -1
                  )
                  );
                  $this->set('desktopDelegated', $desktopDelegated); */
//debug( $userDesktopNameDelegated );

                $chooseusers = array();
                foreach ($users as $i => $user) {
                    $userId = $user['User']['id'];
                    $userDesktop = $user['User']['nom'].' '.$user['User']['prenom'];
                    $userDesktopId = $user['Desktop']['id'];
//debug($userDesktop);
//debug($userDesktopId);
					if( isset( $this->request->data['admin']) ){
						$chooseusers[$userId] = $userDesktop;
					}
					else{
						if ($userId != $this->Session->read('Auth.User.id')) {
							if (!is_array($alldesktopuser) || is_array($alldesktopuser) && !in_array($userId, $alldesktopuser)) {
								$chooseusers[$userId] = $userDesktop;
							}
						}
                    }
                }
//$this->log($chooseusers);
                $this->set('chooseusers', $chooseusers);


                $qdPlanDeleg = array(
                    'order' => array(
                        'Plandelegation.date_start, Plandelegation.created, Plandelegation.date_end'
                    ),
                    'conditions' => array(
                        'Plandelegation.desktop_id' => $id
                    ),
                    'contain' => array(
                        'User' => array(
                            'Desktop'
                        )
                    )
                );

                $this->loadmodel('Plandelegation');
                $plandelegations = $this->Plandelegation->find('all', $qdPlanDeleg);


                $this->set('baseDesktops', $this->Desktop->DesktopsUser->getBaseDesktops($this->Session->read('Auth.User.id')));
                $this->set('currentUserDesktops', $this->SessionTools->getDesktopList());
                $this->set('users', $users);
                $this->set('desktopId', $id);
                $this->set('desktopName', $this->Desktop->field('name', array('Desktop.id' => $id)));

                $this->set('plandelegations', $plandelegations);
            } else {

                if (!empty($this->request->data['DesktopsUser']['planification']) ) {
                    $this->Jsonmsg->init(__d('desktop', 'Desktop.delegation.planification.error'));
                    $this->loadmodel('Plandelegation');
                    $plandelegation = $this->Plandelegation->create();
                    foreach ($this->request->data['DesktopsUser'] as $key => $name) {
                        $plandelegation['Plandelegation'][$key] = $name;
                    }
					$plandelegation['Plandelegation']['date_start'] = $this->request->data['Plandelegation']['date_start'];
					$plandelegation['Plandelegation']['date_end'] = $this->request->data['Plandelegation']['date_end'];

                    //transfore 'data_start' 'data_end' to format comparable
                    $new_date_start = date_format(date_create_from_format('d/m/Y H:i:s', $plandelegation['Plandelegation']['date_start'] . " 00:00:00"), 'Y-m-d H:i:s');
                    $new_date_end = date_format(date_create_from_format('d/m/Y H:i:s', $plandelegation['Plandelegation']['date_end'] . " 00:00:00"), 'Y-m-d H:i:s');

                    $qdTest = array(
                        'conditions' => array(
                            'Plandelegation.desktop_id' => $plandelegation['Plandelegation']['desktop_id'],
                            'Plandelegation.user_id' => $plandelegation['Plandelegation']['user_id'],
                            'Plandelegation.date_end > now()'
                        ),
                        'order' => array('Plandelegation.id DESC')
                    );
                    $tests = $this->Plandelegation->find('first', $qdTest);

                    if (empty($tests)) {
                        $planed = $this->Plandelegation->save($plandelegation);
                        if (!empty($planed)) {
                            // ajout d'un mail en cas de délégation
                            $user_id = $plandelegation['Plandelegation']['user_id'];
                            $typeNotif = $this->Desktop->User->find(
                                    'first', array(
                                'fields' => array('User.typeabonnement'),
                                'conditions' => array('User.id' => $user_id),
                                'contain' => false
                                    )
                            );
                            $notificationId = $this->Desktop->Courrier->Notifieddesktop->Notification->id;
                            if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
                                $this->Desktop->User->saveNotif(null, $user_id, $notificationId,'delegation', $plandelegation);
                            } else {
                                $this->Desktop->User->notifier(null, $user_id, $notificationId, 'delegation', $plandelegation);
                            }
                            $this->Jsonmsg->valid(__d('desktop', 'Desktop.delegation.planification.ok'));
                        }
                    } else {

                        //new_plandelegation_date_start > $tests_date_start && new_plandelegation_date_end < $tests_date_end --> faire rien
                        if ($tests['Plandelegation']['date_end'] > $new_date_start && $new_date_start > $tests['Plandelegation']['date_start'] && $tests['Plandelegation']['date_start'] < $new_date_end && $new_date_end < $tests['Plandelegation']['date_end']) {
                            $this->Jsonmsg->json['message'] .= "<br />" . __d('desktop', 'Desktop.delegation.planned');
                        }
                        //new_plandelegation_date_start < $tests_date_start && now() < new_plandelegation_date_end < $tests_date_end --> update tests
                        elseif ($new_date_start < $tests['Plandelegation']['date_start'] && date('Y-m-d H:i:s') < $new_date_end && $new_date_end < $tests['Plandelegation']['date_end'] && $new_date_end > $tests['Plandelegation']['date_start']) {
                            $this->Plandelegation->id = $tests['Plandelegation']['id'];
                            $this->Plandelegation->saveField('date_start', $new_date_start);
                            $this->Jsonmsg->valid(__d('desktop', 'Desktop.delegation.planification.ok'));
                        }
                        //new_plandelegation_date_start > $tests_date_start && now() < new_plandelegation_date_end && new_plandelegation_date_end > $tests_date_end --> update tests
                        elseif ($new_date_start > $tests['Plandelegation']['date_start'] && date('Y-m-d H:i:s') < $new_date_end && $new_date_end > $tests['Plandelegation']['date_end'] && $new_date_start < $tests['Plandelegation']['date_end']) {
                            $this->Plandelegation->id = $tests['Plandelegation']['id'];
                            $this->Plandelegation->saveField('date_end', $new_date_end);
                            $this->Jsonmsg->valid(__d('desktop', 'Desktop.delegation.planification.ok'));
                        }
                        //new_plandelegation_date_end < $tests_date_start &&  now() < new_plandelegation_date_endt   -->create new
                        elseif ($new_date_end <= $tests['Plandelegation']['date_start'] && date('Y-m-d H:i:s') < $new_date_end) {
                            $planed = $this->Plandelegation->save($plandelegation);
                            if (!empty($planed)) {
                                // ajout d'un mail en cas de délégation
                                $user_id = $plandelegation['Plandelegation']['user_id'];
                                $typeNotif = $this->Desktop->User->find(
                                        'first', array(
                                    'fields' => array('User.typeabonnement'),
                                    'conditions' => array('User.id' => $user_id),
                                    'contain' => false
                                        )
                                );
                                $notificationId = $this->Desktop->Courrier->Notifieddesktop->Notification->id;
                                if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
                                    $this->Desktop->User->saveNotif(null, $user_id, $notificationId, 'delegation', $plandelegation);
                                } else {
                                    $this->Desktop->User->notifier(null, $user_id, $notificationId, 'delegation', $plandelegation);
                                }
                                $this->Jsonmsg->valid(__d('desktop', 'Desktop.delegation.planification.ok'));
                            }
                        }
                        //new_plandelegation_date_start >= $tests_date_end &&  now() < new_plandelegation_date_start  --> create new
                        elseif ($new_date_start >= $tests['Plandelegation']['date_end'] && date('Y-m-d H:i:s') < $new_date_end) {
                            $planed = $this->Plandelegation->save($plandelegation);
                            if (!empty($planed)) {
                                // ajout d'un mail en cas de délégation
                                $user_id = $plandelegation['Plandelegation']['user_id'];
                                $typeNotif = $this->Desktop->User->find(
                                        'first', array(
                                    'fields' => array('User.typeabonnement'),
                                    'conditions' => array('User.id' => $user_id),
                                    'contain' => false
                                        )
                                );
                                $notificationId = $this->Desktop->Courrier->Notifieddesktop->Notification->id;
                                if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
                                    $this->Desktop->User->saveNotif(null, $user_id, $notificationId, 'delegation', $plandelegation);
                                } else {
                                    $this->Desktop->User->notifier(null, $user_id, $notificationId,  'delegation', $plandelegation);
                                }
                                $this->Jsonmsg->valid(__d('desktop', 'Desktop.delegation.planification.ok'));
                            }
                        }
                    }
                    $this->Jsonmsg->send();
                }
				else if (!empty($this->request->data['DesktopsUser']['planification']) && empty($this->request->data['DesktopsUser']['date_start'])) {
					$this->Jsonmsg->json['message'] .= 'Aucune délégation possible' . "<br />" . "Veuillez renseigner des dates pour la planification.";

					$this->Jsonmsg->send();
				} else {
                    $this->Jsonmsg->init(__d('desktop', 'Desktop.delegation.error'));
                    $this->Desktop->DesktopsUser->create();
                    if ($this->Desktop->DesktopsUser->save($this->request->data)) {
                        // ajout d'un mail en cas de délégation
                        $user_id = $this->request->data['DesktopsUser']['user_id'];
                        $notificationId = $this->Desktop->Courrier->Notifieddesktop->Notification->id;
                        $this->Desktop->User->notifier(null, $user_id, $notificationId, 'delegation', $this->request->data);

                        $this->Jsonmsg->valid(__d('desktop', 'Desktop.delegation.ok'));
                    }
                    $this->Jsonmsg->send();
                }
            }
        }

		$isAdmin = false;
		$secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
		$desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
		$groupName = $this->Session->read('Auth.User.Desktop.Profil');
		if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
			$isAdmin = true;
		}
		$this->set('isAdmin', $isAdmin);
        $userSessionId = $this->Session->read('Auth.User.id');
        $userProfilSessionId = $this->Session->read('Auth.User.Env.actual.profil_id');
        $this->set('userSessionId', $userSessionId);
        $this->set('userId', $userId);
        $this->set('userProfilSessionId', $userProfilSessionId);
    }

    /**
     * Visualisation des habilitations (Acl simplifiées / Acl / Méta-données / Soustypes)
     *
     * @logical-group Rôle
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du rôle
     * @param string $mode choix du type d'habilitations (func, func_classique, data, meta)
     * @return void
     */
    public function getRights($id = null, $mode = 'data') {
        if ($id != null) {
            if ($mode == 'func') {
                $this->loadModel('Right');
                $aro = array('model' => 'Desktop', 'foreign_key' => $id);
                $aro_id = $this->Acl->Aro->field('id', $aro);
                $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $aro_id)));
                if (empty($rights)) {
                    $parent_id = $this->Acl->Aro->field('parent_id', $aro);
                    $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $parent_id)));
                    $rights['Right']['id'] = null;
                    $rights['Right']['aro_id'] = $aro_id;
                    $rights['Right']['created'] = null;
                    $rights['Right']['modified'] = null;
                }
                $this->set('rights', $rights);
            } else if ($mode == 'func_classique') {
                //func
                $node = $this->Acl->Aro->node(array('model' => 'Desktop', 'foreign_key' => $id));
                $rights = array();
                $rights[] = $this->Acl->getRightsGrid($node[0]);
                $this->set('rights', $rights);
            } else if ($mode == 'data') {
                //data
                $daros = $this->Desktop->getDaros($id);
                $dataRights = array();
                $options = array(
                    'actions' => array('read'),
                    'models' => array('Type', 'Soustype')
                );
                foreach ($daros as $daro) {
                    $dataRights[$daro['Daro']['foreign_key']] = $this->DataAcl->getRightsGrid($daro, array(), $options);
                }
                $this->set('dataRights', $dataRights);
            } else if ($mode == 'meta') {
                //meta
                $metaRights = array();
                $options = array(
                    'actions' => array('read', 'update'),
                    'models' => array('Metadonnee')
                );
                foreach ($daros as $daro) {
                    $metaRights[$daro['Daro']['foreign_key']] = $this->DataAcl->getRightsGrid($daro, array(), $options);
                }
                $this->set('metaRights', $metaRights);
            }
            $this->set('requester', array('model' => 'Desktop', 'foreign_key' => $id));
            $this->set('mode', $mode);
        }
    }

    /**
     * Gestion des habilitations d'un rôle (ajax)
     *
     * @logical-group Rôle
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du rôle
     * @param string $mode détermine si on est en visualisation ou en édition (set, get)
     * @return void
     */
    public function rights($id = null, $mode = 'set') {
        if ($id != null) {
            $this->set('id', $id);
            $this->set('mode', $mode);
            $this->set('desktopName', $this->Desktop->field('name', array('Desktop.id' => $id)));
        }
    }

    /**
     * Suppresion d'un rôle pour un utilisateur
     *
     * @logical-group Rôle
     * @user-profile Admin
     *
     * @access public
     * @param integer $desktopId identifiant du rôle
     * @param interger $userId identifiant de l'utilisateur
     * @return void
     */
    public function delete($desktopId, $userId) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $deletable = $this->Desktop->inDeletable($desktopId);
        $success = $deletable['deletable'];
        if ($success) {
            $message = '';
            $desktopUserId = $this->Desktop->DesktopsUser->field('DesktopsUser.id', array('DesktopsUser.desktop_id' => $desktopId, 'DesktopsUser.user_id' => $userId));
            if ($this->Desktop->DesktopsUser->delete($desktopUserId)) {
                $message = 'Liaison : ' . __d('default', 'delete.ok');
            }

            $desktopUser = $this->Desktop->DesktopsUser->find('all', array('conditions' => array('DesktopsUser.desktop_id' => $desktopId)));
            if (empty($desktopUser)) {
                if ($this->Desktop->delete($desktopId)) {
                    $message .= '<br />Profil : ' . __d('default', 'delete.ok');
                }
            }

            if ($message != '') {
                $this->Jsonmsg->valid($message);
            }

            // On enregistre dans le journal
            $user = $this->Desktop->DesktopsUser->User->find(
                'first',
                array(
                    'conditions' => array(
                        'User.id' => $userId
                    ),
                    'contain' => false
                )
            );
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $journalmsg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé le profil de l'utilisateur ".$user['User']['username']." le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $journalmsg, 'info', $flux);

        } else {
            $message = $deletable['message'];
            $this->Jsonmsg->error($message);
        }

        $this->Jsonmsg->send();
    }

    /**
     * Passage d'un profil en profil principal
     * @param type $desktop_id
     */
    public function setMain($desktop_id, $user_id) {
        $this->layout = false;
        $desktopPrincipal = $this->Desktop->User->find(
                'first', array(
            'fields' => array(
                'User.desktop_id'
            ),
            'conditions' => array(
                'User.id' => $user_id
            ),
            'contain' => false
                )
        );
        $desktopPrincipalId = $desktopPrincipal['User']['desktop_id'];

        $allUserDesktops = $this->Desktop->DesktopsUser->find(
                'all', array(
            'conditions' => array(
                'DesktopsUser.user_id' => $user_id,
                'DesktopsUser.delegation' => false
            ),
            'contain' => false
                )
        );

//debug($allUserDesktops);
//die();
        $success = $this->Desktop->User->updateAll(
                array(
            'User.desktop_id' => $desktop_id
                ), array(
            'User.id' => $user_id
                )
        );

        if ($success) {
            $success = $this->Desktop->DesktopsUser->updateAll(
                            array(
                        'DesktopsUser.desktop_id' => $desktopPrincipalId
                            ), array(
                        'DesktopsUser.user_id' => $user_id,
                        'DesktopsUser.desktop_id' => $desktop_id
                            )
                    ) && $success;
        }
        $this->autoRender = false;
    }

    /**
     * Edition des planifications de délégation
     *
     * @logical-group Rôle
     * @user-profile Admin
     *
     * @access public
     * @param integer $userId identifiant de l'utilisateur
     * @param integer $mainDesktop identifiant du rôle principal
     * @return void
     */
    public function editDeleg($desktopId, $userId, $admin = null) {
        $qdPlanDeleg = array(
            'conditions' => array(
                'Plandelegation.desktop_id' => $desktopId,
                'Plandelegation.user_id' => $userId
            ),
            'contain' => false
        );

        $this->loadmodel('Plandelegation');
        $plandelegation = $this->Plandelegation->find('first', $qdPlanDeleg);

        $planId = $plandelegation['Plandelegation']['id'];
        $this->set('planId', $planId);

        if (!empty($this->request->data)) {
//debug($this->request->data);
            $this->request->data = $this->request->data['Plandelegation'];
            $this->Jsonmsg->init();
            $this->Plandelegation->create($this->request->data);
            $success = $this->Plandelegation->save();
            if ($success) {
                $this->Jsonmsg->valid();
                $this->Plandelegation->commit();
            } else {
                $this->Plandelegation->rollback();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $plandelegation;

            // $dateStart = split(' ', $plandelegation['Plandelegation']['date_start']);
            // $this->request->data['Plandelegation']['date_start'] = date('Y-m-d', strtotime($dateStart[0]));
            // $dateEnd = split(' ', $plandelegation['Plandelegation']['date_end']);
            // $this->request->data['Plandelegation']['date_end'] = date('Y-m-d', strtotime($dateEnd[0]));
        }

        $qdUsers = array(
            'fields' => array_merge(
                    $this->Desktop->User->fields(), $this->Desktop->fields()
            ),
            'order' => array('Desktop.name ASC'),
            'conditions' => array(
                'User.active' => true,
                'Desktop.profil_id <>' => ADMIN_GID
            ),
            'contain' => false,
            'joins' => array(
                $this->Desktop->User->join($this->Desktop->alias)
            )
        );
        $users = $this->Desktop->User->find('all', $qdUsers);

        $chooseusers = array();
        foreach ($users as $i => $user) {
            $user_id = $user['User']['id'];
            $userDesktop = $user['Desktop']['name'];
            $userDesktopId = $user['Desktop']['id'];
            if ($userId != $this->Session->read('Auth.User.id')) {
                $chooseusers[$user_id] = $userDesktop;
            }
        }

        $this->set('chooseusers', $chooseusers);
        $this->set('desktopId', $desktopId);
        $this->set('userId', $userId);

        if (isset($admin) && $admin != null) {
            $this->render('edit_deleg_admin');
        }
//debug($this->request->data);
    }

    /**
     * Suppresion d'une planification de délégation pour un utilisateur
     *
     * @logical-group Rôle
     * @user-profile Admin
     *
     * @access public
     * @param integer $desktopId identifiant du rôle
     * @param interger $userId identifiant de l'utilisateur
     * @return void
     */
    public function deleteDeleg($plandelegationId) {
        $this->autoRender = false;
        if (!empty($plandelegationId)) {

            $plandelegation = $this->Desktop->Plandelegation->find(
                    'first', array(
                'conditions' => array(
                    'Plandelegation.id' => $plandelegationId
                ),
                'contain' => false
                    )
            );
            if (!empty($plandelegation)) {
                $this->Jsonmsg->init();
                $userId = $plandelegation['Plandelegation']['user_id'];
                $desktopId = $plandelegation['Plandelegation']['desktop_id'];
                $success = $this->Desktop->Plandelegation->delete($plandelegationId);
                if ($success) {
                    $message = 'Délégation planifiée supprimée';
                    $desktopUserId = $this->Desktop->DesktopsUser->field('DesktopsUser.id', array('DesktopsUser.desktop_id' => $desktopId, 'DesktopsUser.user_id' => $userId));
                    if (!empty($desktopUserId)) {
                        if ($this->Desktop->DesktopsUser->delete($desktopUserId)) {
                            $message = 'Liaison : ' . __d('default', 'delete.ok');
                        }
                    }

                    if ($message != '') {
                        $this->Jsonmsg->valid($message);
                    }
                } else {
                    $message = '<br /> Délégation non supprimable';
                    $this->Jsonmsg->error($message);
                }
                $this->Jsonmsg->send();
            }
        }
    }

    /**
     * Focntion retournant le prochain flux dans les bannettes d'un agent
     * @param type $desktopId
     */
    public function nextFlux($courrierId, $desktopId) {
        $this->autoRender = false;
        $userId = $this->Desktop->DesktopsUser->User->userIdByDesktop($desktopId);
        $desktopsIds = $this->Desktop->DesktopsUser->User->getDesktops($userId);
        $bannette_id = $this->Desktop->Bancontenu->getBannetteIdByCourrierIdDesktopId($courrierId, $desktopId);
//        $this->Desktop->Profil->find(
//            'first',
//            array(
//                'Profil.desktop_id' => $desktopId
//            )
//        );
        if( !empty($bannette_id ) ) {
            $bancontenus = $this->Desktop->Bancontenu->find(
                    'all', array(
                'conditions' => array(
                    'Bancontenu.desktop_id' => $desktopId,
                    'Bancontenu.bannette_id' => $bannette_id,
                    'Bancontenu.etat' => '1'
                ),
                'recursive' => -1,
                'order' => array('Bancontenu.id ASC')
                    )
            );
            foreach ($bancontenus as $i => $bancontenu) {
                $courriersIds[] = $bancontenu['Bancontenu']['courrier_id'];
            }
            if (!empty($courriersIds) && count($courriersIds) != 0) {
                for ($c = 0; $c < count($courriersIds); $c++) {
    //debug($courriersIds[$c]);
                    $maxCount = count($courriersIds) - 1;
                    if ($c != 0 && $c != $maxCount) {
                        $datas[$c]['before'] = $courriersIds[$c - 1];
                        $datas[$c]['current'] = $courriersIds[$c];
                        $datas[$c]['next'] = $courriersIds[$c + 1];
                    } else if ($c == 0) {
                        $datas[$c]['debut'] = "Il s'agit du flux le plus récent de votre bannette.";
                        $datas[$c]['before'] = $courriersIds[$maxCount];
                        $datas[$c]['current'] = $courriersIds[0];
                        if (count($courriersIds) != 1) {
                            $datas[$c]['next'] = $courriersIds[1];
                        } else {
                            $datas[$c]['fin'] = "Il s'agit du flux le plus ancien de votre bannette.";
                            $datas[$c]['next'] = $courriersIds[0];
                        }
                    } else if ($c == $maxCount) {
                        $datas[$c]['fin'] = "Il s'agit du dernier flux de votre bannette.";
                        $datas[$c]['before'] = $courriersIds[$c - 1];
                        $datas[$c]['current'] = $courriersIds[$c];
                        $datas[$c]['next'] = $courriersIds[0];
                    }
    //debug($courrierId);
                    if ($datas[$c]['current'] == $courrierId) {
                        $currentValue = $c;
    //                debug($currentValue);
                    }
                }
                $json_return = json_encode(array('dataFluxComplet' => $datas[$currentValue]));
            }
        } else {
            $json_return = json_encode(array("void" => "Vous n'avez plus de flux dans cette bannette."));
        }
//debug($currentValue);
//debug($datas);
//debug($datas[$currentValue]);

        return $json_return;
//        return $datas;
    }

}
