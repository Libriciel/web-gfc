<?php

/**
 * Méta-données
 *
 * Sousactivites controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class SousactivitesController extends AppController {

	/**
	 * Controller name
	 *
	 * @var string
	 * @access public
	 */
	public $name = 'Sousactivites';

	/**
	 * Ajout d'une méta-donnée
	 *
	 * @logical-group Méta-données
	 * @user-profil Admin
	 *
	 * @access public
	 * @return void
	 */
	public function add( $activite_id ) {
		if (!empty($this->request->data)) {

            $this->request->data['Sousactivite']['activite_id'] = $activite_id;

			$this->Jsonmsg->init();
			$this->Sousactivite->create($this->request->data);

			if ($this->Sousactivite->save()) {
				$this->Jsonmsg->valid();
			}
			$this->Jsonmsg->send();
		} else {
			$this->set('sousactivites', $this->Sousactivite->find('list'));
		}
        $this->set( 'activiteId', $activite_id);
	}

	/**
	 * Edition d'un méta-donnée
	 *
	 * @logical-group Méta-données
	 * @user-profil Admin
	 *
	 * @access public
	 * @param integer $id identifiant de la méta-donnée
	 * @return void
	 */
	public function edit($id = null) {
		if (!empty($this->request->data)) {
			$this->Jsonmsg->init();
			$this->Sousactivite->create($this->request->data);
			if ($this->Sousactivite->save()) {
				$this->Jsonmsg->valid();
			}
			$this->Jsonmsg->send();
		} else {
			$querydata = array(
				'contain' => array(
					$this->Sousactivite->Activite->alias,
				),
				'conditions' => array(
					'Sousactivite.id' => $id
				)
			);
			$this->request->data = $this->Sousactivite->find('first', $querydata);
		}
        $this->set( 'activiteId', $this->request->data['Activite']['id'] );
        $this->set( 'sousactiviteId', $id );
		$this->set('sousactivites', $this->Sousactivite->find('list'));
	}

	/**
	 * Suppression d'une méta-donnée (delete)
	 *
	 * @logical-group Méta-données
	 * @user-profil Admin
	 *
	 * @access public
	 * @param integer $id identifiant de la méta-donnée
	 * @return void
	 */
	public function delete($id = null) {
		$this->Jsonmsg->init(__d('default', 'delete.error'));
		$this->Sousactivite->begin();
		if ($this->Sousactivite->delete($id)) {
			$this->Sousactivite->commit();
			$this->Jsonmsg->valid(__d('default', 'delete.ok'));
		} else {
			$this->Sousactivite->rollback();
		}
		$this->Jsonmsg->send();
	}

}

?>
