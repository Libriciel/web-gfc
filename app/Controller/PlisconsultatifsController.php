<?php

/**
 * Plisconsultatifs
 *
 * Plisconsultatifs controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class PlisconsultatifsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Plisconsultatifs';
    public $uses = array('Pliconsultatif');

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('ajaxformdata', 'export');
    }

    /**
     * Gestion des opérations interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'marcheSAERP', true)
        ));
    }

    /*
     *
     *
     */

    protected function _setOptions() {
        $origineflux = $this->Pliconsultatif->Origineflux->find('list', array(
            'conditions' => array(
                'Origineflux.active' => true
            ),
            'order' => array('Origineflux.name ASC')
        ));
        $this->set('origineflux', $origineflux);
    }

    /**
     * Ajout d'un e opération
     *
     * @logical-group Plisconsultatifs
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add($consultationId) {

        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $consultation = $this->Pliconsultatif->Consultation->find('first', array(
                'conditions' => array(
                    'Consultation.id' => $consultationId
                ),
                'contain' => false
            ));

            $pliconsultatif = $this->request->data;

            $pliconsultatif['Pliconsultatif']['name'] = $consultation['Consultation']['numero'] . ' - ' . $pliconsultatif['Pliconsultatif']['numero'];

            $this->Pliconsultatif->begin();
            $this->Pliconsultatif->create($pliconsultatif);
            if ($this->Pliconsultatif->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Pliconsultatif->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Pliconsultatif->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }
        $consultation = $this->Pliconsultatif->Consultation->find('first', array(
            'conditions' => array(
                'Consultation.id' => $consultationId
            ),
            'contain' => array(
                'Pliconsultatif' => array(
                    'order' => array('Pliconsultatif.numero ASC')
                )
            )
        ));
        if (!empty($consultation['Pliconsultatif'])) {
            $numeroPli = $consultation['Pliconsultatif'][count($consultation['Pliconsultatif']) - 1]['numero'] + 1;
        } else {
            $numeroPli = 1;
        }
        $this->set(compact('numeroPli'));
        $this->set('consultationId', $consultationId);
        $this->_setOptions();
    }

    /**
     * Edition d'une opération
     *
     * @logical-group Plisconsultatifs
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Pliconsultatif->create();
            $pliconsultatif = $this->request->data;


            if ($this->Pliconsultatif->save($pliconsultatif)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Pliconsultatif->find('first', array(
                'conditions' => array(
                    'Pliconsultatif.id' => $id
                ),
                'contain' => array(
                    'Consultation'
                )
            ));
            $this->request->data['Pliconsultatif']['date'] = date("d/m/Y", strtotime($this->request->data['Pliconsultatif']['date']));

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
        $consultationId = $this->request->data['Consultation']['id'];
        $this->set('pliconsultatifId', $id);
        $this->set('consultationId', $consultationId);
        $this->_setOptions();
    }

    /**
     * Suppression d'une opération
     *
     * @logical-group Plisconsultatifs
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Pliconsultatif->begin();
        if ($this->Pliconsultatif->delete($id)) {
            $this->Pliconsultatif->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Pliconsultatif->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des opérations (ajax)
     *
     * @logical-group Plisconsultatifs
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getPlisconsultatifs() {

        $plisconsultatifs_tmp = $this->Pliconsultatif->find("all", array(
            'order' => array('Pliconsultatif.name')
        ));

        $plisconsultatifs = array();
        foreach ($plisconsultatifs_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $plisconsultatifs[] = $item;
        }
        $this->set(compact('plisconsultatifs'));
        $this->_setOptions();
    }

    public function ajaxformdata($field = null, $consultationId = null) {

        if ($field == 'typedocument') {
            $this->autoRender = false;

            if (!empty($this->request->data['Courrier']['consultation_id']) && empty($consultationId)) {
                $consultation = $this->Pliconsultatif->Consultation->find('first', array(
                    'conditions' => array(
                        'Consultation.id' => $this->request->data['Courrier']['consultation_id']
                    ),
                    'contain' => array(
                        'Pliconsultatif' => array(
                            'order' => array('Pliconsultatif.numero ASC')
                        )
                    )
                ));
                if (!empty($consultation['Pliconsultatif'])) {
                    $valueNumberPli = $consultation['Pliconsultatif'][count($consultation['Pliconsultatif']) - 1]['numero'] + 1;
                } else {
                    $valueNumberPli = 1;
                }

//                $numberOs = $this->Ordreservice->find(
//                    'count',
//                    array(
//                        'conditions' => array(
//                            'Ordreservice.consultation_id' => $this->request->data['Courrier']['consultation_id']
//                        ),
//                        'contain' => false
//                    )
//                );
//                $valueNumberOs = $numberOs + 1;
            } else {
                $consultation = $this->Pliconsultatif->Consultation->find('first', array(
                    'conditions' => array(
                        'Consultation.id' => $consultationId
                    ),
                    'contain' => array(
                        'Pliconsultatif' => array(
                            'order' => array('Pliconsultatif.numero ASC')
                        )
                    )
                ));
                if (!empty($consultation['Pliconsultatif'])) {
                    $valueNumberPli = $consultation['Pliconsultatif'][count($consultation['Pliconsultatif']) - 1]['numero'] + 1;
                } else {
                    $valueNumberPli = 1;
                }

//                $numberOs = $this->Ordreservice->find(
//                    'count',
//                    array(
//                        'conditions' => array(
//                            'Ordreservice.consultation_id' => $consultationId
//                        ),
//                        'contain' => false
//                    )
//                );
//                $valueNumberOs = $numberOs + 1;
            }
            $this->set('valueNumberPli', $valueNumberPli);

            return $valueNumberPli;
        }


        if ($field == 'numero') {
            $this->autoRender = false;
            if (in_array($this->request->data['Pliconsultatif']['numero'], $this->Pliconsultatif->find('list', array('conditions' => array('Pliconsultatif.active' => true), 'fields' => array('id', 'numero'))))) {
                $this->autoRender = false;
                $content = json_encode(array('Pliconsultatif' => array('numero' => 'Valeur déjà utilisée')));
                header('Pragma: no-cache');
                header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
                header('Content-Type: text/x-json');
                header("X-JSON: {$content}");
                Configure::write('debug', 0);
                echo $content;
                return;
            }
        }
    }

    /**
     * Export des informations liées aux plis
     *
     * @logical-group Consultations
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function export($consultationId) {
        $origineflux = $this->Pliconsultatif->Origineflux->find('list', array(
            'conditions' => array(
                'Origineflux.active' => true
            ),
            'order' => array('Origineflux.name ASC')
        ));
        $this->set('origineflux', $origineflux);
        $consultation = $this->Pliconsultatif->Consultation->find('first', array(
            'conditions' => array(
                'Consultation.id' => $consultationId
            ),
            'contain' => false
        ));
        $consultationNumero = $consultation['Consultation']['numero'];
        $this->set('consultationNumero', $consultationNumero);

        $querydata = array(
            'conditions' => array(
                'Pliconsultatif.consultation_id' => $consultationId
            ),
            'order' => array(
                'Pliconsultatif.numero ASC'
            ),
            'contain' => false
        );
        $results = $this->Pliconsultatif->find('all', $querydata);
        $this->set('results', $results);

        $this->_setOptions();
        $this->layout = '';
    }

}

?>
