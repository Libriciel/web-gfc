<?php

/**
 * Référentiel des voies FANTOIR
 *
 *
 * Referentielsfantoir controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller
 */
class BanscommunesController extends AppController {

    /**
     * Controller uses
     *
     * @var array
     * @access public
     */
    public $uses = array('Bancommune');

    /**
     * Gestion des référentiels de voie
     *
     * @logical-group Carnet d'adresse
     * @user-profile Admin
     *
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'gestionBans', true)
        ));
    }

    public function beforeFilter() {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '1024M');
        parent::beforeFilter();
    }

    /**
     * Edition d'un méta-donnée
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant de la méta-donnée
     * @return void
     */
    public function edit($id = null) {

        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Bancommune->create($this->request->data);

            if ($this->Bancommune->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $querydata = array(
                'conditions' => array(
                    'Bancommune.id' => $id
                ),
                'contain' => array(
                    'Banadresse'
                )
            );
            $this->request->data = $this->Bancommune->find('first', $querydata);

//            $bansadresses = $this->Bancommune->Banadresse->find(
//                'all',
//                array(
//                    'conditions' => array(
//                        'Banadresse.bancommune_id' => $this->request->data['Bancommune']['id']
//                    ),
//                    'recursive' => -1
//                )
//            );
            $querydataAdresse = array(
                'conditions' => array(
                    'Banadresse.bancommune_id' => $this->request->data['Bancommune']['id']
                ),
                'limit' => 100,
                'maxLimit' => 2000,
                'recursive' => -1
            );
            $paginate = $this->paginate;
            $paginate['Banadresse'] = $querydataAdresse;
            $this->paginate = $paginate;
            $nbBanadresse = $this->Bancommune->Banadresse->find('count', array(
                'conditions' => array(
                    'Banadresse.bancommune_id' => $this->request->data['Bancommune']['id']
                )
            ));
            $bansadresses = $this->paginate('Banadresse');
        }
//debug($bansadresses);
        $this->set('bancommuneId', $id);
        $this->set('bansadresses', $bansadresses);
        $this->set('nbBanadresse', $nbBanadresse);
    }

    /**
     * Suppression d'une base adresse
     *
     * @logical-group BAN
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant de la BAN
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Bancommune->begin();
        if ($this->Bancommune->delete($id)) {
            $this->Bancommune->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Bancommune->rollback();
        }
        $this->Jsonmsg->send();
    }

}
