<?php

/**
 * Carnet d'adresse
 *
 *
 * Addressbooks controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller
 */
class AddressbooksController extends AppController {

    /**
     * Controller uses
     *
     * @var array
     * @access public
     */
    public $uses = array('Addressbook');

    /**
     * Gestion des carnets d'adresses et contacts
     *
     * @logical-group Carnet d'adresse
     * @user-profile Admin
     *
     *
     * @access public
     * @return void
     */
    public function index() {

    }

//TODO: enlever le beforeFilter et régénérer les droits
    public function beforeFilter() {
        $this->Auth->allow(array('export'));
        parent::beforeFilter();
    }

    protected function _setOptions() {
        $this->set('addressbooksList', $this->Session->read('Auth.Addressbook') );
        $organsme = $this->Addressbook->Organisme->find('list', array( 'fields' => array('Organisme.name', 'Organisme.ville',  'Organisme.id'), 'order' => array('Organisme.name ASC')));
        $organsmeList = array();
        foreach( $organsme as $id => $value ) {
            $org = implode( ",", array_keys( $value )  );
            $ville = implode( ",", $value );
            $organsmeList[$id] = $org.' ( '.$ville . ' )';
        }
        $this->set('organsmeList', $organsmeList );

		$this->set('activites', $this->Addressbook->Organisme->Activite->find('list', array('order' => array('Activite.name ASC'), 'conditions' => array('Activite.active' => true))));
//            $this->set('operations', $this->Addressbook->Organisme->Operation->find('list', array('order' => array('Operation.name ASC'), 'conditions' => array('Operation.active' => true))));
//            $this->set('events', $this->Addressbook->Organisme->Contact->Event->find('list', array('order' => array('Event.name ASC'), 'conditions' => array('Event.active' => true))));
		$this->set('fonctions', $this->Addressbook->Organisme->Contact->Fonction->find('list', array('order' => array('Fonction.name ASC'), 'conditions' => array('Fonction.active' => true))));

        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
        $civilite = $this->Session->read( 'Auth.Civilite');
        $this->set('civilite', $civilite);
    }

    /**
     * Récupération de la liste des carnets d'adresse
     *
     * @logical-group Carnet d'adresse
     * @user-profile Admin
     *
     *
     * @access public
     * @return void
     */
    public function get_addressbooks() {

        if (!isset($this->request->data['SearchOrganisme'])) {
            $this->request->data = Set::merge(
                            $this->Session->read('paginationModel.rechercherOrganisme'), array('SearchOrganisme' => array('active' => true))
            );
        }
        $this->Session->write('paginationModel.rechercherOrganisme', $this->request->data);
        $this->Addressbook->recursive = -1;

        $conditionsAddressbook = array(
            'Addressbook.active' => true
        );
//debug($this->request->data);
//        $conditions = array('Organisme.active' => true);
        $conditions = array();
        $conditionsContact = array();
        if (!empty($this->request->data)) {

            if (isset($this->request->data['SearchOrganisme']['name']) && !empty($this->request->data['SearchOrganisme']['name'])) {
                $conditions[] = 'Organisme.name ILIKE \'' . $this->Addressbook->wildcard('%' . $this->request->data['SearchOrganisme']['name'] . '%') . '\'';
            }
            if (isset($this->request->data['SearchOrganisme']['active']) && !empty($this->request->data['SearchOrganisme']['active'])) {
                $conditions[] = array('Organisme.active' => $this->request->data['SearchOrganisme']['active']);
            }
            if (isset($this->request->data['SearchOrganisme']['addressbook_id']) && !empty($this->request->data['SearchOrganisme']['addressbook_id'])) {
                $conditions[] = array('Organisme.addressbook_id' => $this->request->data['SearchOrganisme']['addressbook_id']);
                $conditionsAddressbook[] = array(
                    'Addressbook.id' => $this->request->data['SearchOrganisme']['addressbook_id']
                );
            }
            if (Configure::read('Conf.SAERP')) {
                // Filtre sur les OP de l'organisme
                if (isset($this->request->data['SearchOperation']['id']) && !empty($this->request->data['SearchOperation']['id'])) {
                    $orgsOps = $this->Addressbook->Organisme->OrganismeOperation->find(
                            'all', array(
                        'conditions' => array(
                            'OrganismeOperation.operation_id' => $this->request->data['SearchOperation']['id']
                        ),
                        'contain' => false
                            )
                    );
                    $orgsIds = Hash::extract($orgsOps, '{n}.OrganismeOperation.organisme_id');
//debug($orgsOps);
                    $conditions[] = array('Organisme.id' => $orgsIds);
                }
                // Filtre sur les activités de l'organisme
                if (isset($this->request->data['SearchActivite']['id']) && !empty($this->request->data['SearchActivite']['id'])) {
                    $orgsActivites = $this->Addressbook->Organisme->ActiviteOrganisme->find(
                            'all', array(
                        'conditions' => array(
                            'ActiviteOrganisme.activite_id' => $this->request->data['SearchActivite']['id']
                        ),
                        'contain' => false
                            )
                    );
                    $orgsIds = Hash::extract($orgsActivites, '{n}.ActiviteOrganisme.organisme_id');
//debug($orgsActivites);
                    $conditions[] = array('Organisme.id' => $orgsIds);
                }
                // Filtre sur les fonctions de contact
                if (isset($this->request->data['SearchFonction']['id']) && !empty($this->request->data['SearchFonction']['id'])) {
                    $contactsFonction = $this->Addressbook->Contact->find(
                            'all', array(
                        'conditions' => array(
                            'Contact.fonction_id' => $this->request->data['SearchFonction']['id']
                        ),
                        'contain' => false
                            )
                    );
                    $contactsIds = Hash::extract($contactsFonction, '{n}.Contact.id');
//debug($contactsFonction);
                    $conditionsContact[] = array('Contact.id' => $contactsIds);

                    $orgsContactsIds = Hash::extract($contactsFonction, '{n}.Contact.organisme_id');
                    $conditions[] = array('Organisme.id' => $orgsContactsIds );
                }

                // Filtre sur les événements des contacts
                if (isset($this->request->data['SearchEvent']['SearchEvent']) && !empty($this->request->data['SearchEvent']['SearchEvent'])) {
                    $contactsEvent = $this->Addressbook->Contact->ContactEvent->find(
                            'all', array(
                        'conditions' => array(
                            'ContactEvent.event_id' => $this->request->data['SearchEvent']['SearchEvent']
                        ),
                        'contain' => false
                            )
                    );
                    $contactsEventsIds = Hash::extract($contactsEvent, '{n}.ContactEvent.contact_id');
                    $conditionsContact[] = array('Contact.id' => $contactsEventsIds);

                    $contactsWithEvent = $this->Addressbook->Contact->find(
                        'all',
                        array(
                            'conditions' => array(
                                'Contact.id' => $contactsEventsIds
                            ),
                            'contain' => false
                        )
                    );

                    $orgsContactsEventsIds = Hash::extract($contactsWithEvent, '{n}.Contact.organisme_id');
                    $conditions[] = array('Organisme.id' => $orgsContactsEventsIds );

                }
            }
        }

        /*
          $querydata = array(
          'fields' => array(
          'Addressbook.id',
          'Addressbook.name'
          ),
          'conditions' => array(
          $conditionsAddressbook
          ),
          'order' => array(
          'Addressbook.name'
          ),
          'contain' => array(
          'Organisme' => array(
          'fields' => array(
          'Organisme.id',
          'Organisme.name'
          ),
          'conditions' => array(
          $conditions
          ),
          'order' => array(
          'Organisme.name ASC'
          ),
          'Contact' => array(
          'fields' => array(
          'Contact.id',
          'Contact.name',
          'Contact.nom',
          'Contact.prenom'
          )
          )
          )
          )
          );
         */
        $conditionsAddressPrive = array();
        $addPrivateIds = $this->Addressbook->find('list', array('conditions' => array('Addressbook.private' => true)) );
        $privateAccess = $this->Session->read( 'Auth.User.addressbook_private');


		$isAdmin = false;
		$secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
		if( !empty($secondaryDesktops)) {
			$desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
		}
		else {
			$desktopName = array();
		}
		$groupName = $this->Session->read('Auth.User.Desktop.Profil');
		if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
			$isAdmin = true;
		}
		if( $isAdmin ) {
			$privateAccess = true;
		}
		if(!$privateAccess && !empty($addPrivateIds) ) {
			$conditionsAddressPrive = array(
				'Addressbook.active' => true,
				'Addressbook.id NOT IN' => array_keys( $addPrivateIds)
			);
		}


		$querydata = array(
            'fields' => array(
                'Addressbook.id',
                'Addressbook.name'
            ),
            'conditions' => array_merge(
                $conditionsAddressbook,
				$conditionsAddressPrive
            ),
            'order' => array(
                'Addressbook.name ASC'
            ),
            'contain' => false
        );



        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        $desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
        $isAdmin = false;
        $groupName = $this->Session->read('Auth.User.Desktop.Profil');
        if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
            $isAdmin = true;
        }

//        $addressbooks = $this->Addressbook->find('list', $querydata);
        $addressbooks_tmp = $this->Addressbook->find('all', $querydata);
        foreach ($addressbooks_tmp as $i => $addressbook) {
            $addressbooks[$i]['name'] = $addressbook['Addressbook']['name'] . '<span style="color:#333333"> (' . $this->Addressbook->Organisme->getNombreOrganismeByCarnetId($addressbook["Addressbook"]["id"]) . ') <span>';
            $addressbooks[$i]['view'] = '<a href="#" class = "viewAddressbook" id="addressbook_' . $addressbook["Addressbook"]["id"] . '" title="Visualiser"><i class="fa fa-eye" aria-hidden="true" ></i></a>';
            $addressbooks[$i]['edit'] = '<a href="#" class="editAddressbook" id="addressbook_' . $addressbook["Addressbook"]["id"] . '" title="Modifier"><i class="fa fa-pencil" aria-hidden="true" ></i></a>';
            if( $isAdmin ) {
                $addressbooks[$i]['delete'] = '<a href="#" class="deleteAddressbook" id="addressbook_' . $addressbook["Addressbook"]["id"] . '" title="Supprimer"><i class="fa fa-trash" aria-hidden="true"></i></a>';
            }
            $addressbooks[$i]['exportcsv'] = '<a href="#" class="exportAddressbook" id="addressbook_' . $addressbook["Addressbook"]["id"] . '" title="Exporter"><i class="fa fa-download" aria-hidden="true" ></i></a>';
        }
//        $this->set('addressbooks', $addressbooks);
//            $test = $this->Addressbook->Organisme->searchTest( $this->request->data);
//            $querydata = array_merge( $querydata, $test);
//            $addressbooks = $this->Addressbook->find( 'all', $querydata);
//
        $this->set('addressbooks', $addressbooks);
//        }

        $this->_setOptions();
    }

    /**
     * Ajout de carnet d'adresse
     *
     * @logical-group Carnet d'adresse
     * @user-profile Admin
     *
     *
     * @access public
     * @return void
     */
    public function add() {
        $this->Addressbook->recursive = -1;
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Addressbook->create($this->request->data);
            if ($this->Addressbook->save()) {
                $sansOrganisme['Organisme']['addressbook_id'] = $this->Addressbook->id;
                $sansOrganisme['Organisme']['name'] = 'Sans organisme';
                $this->Addressbook->Organisme->create($sansOrganisme['Organisme']);
                $success = $this->Addressbook->Organisme->save();
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Edition de carnet d'adresse
     *
     * @logical-group Carnet d'adresse
     * @user-profile Admin
     *
     * @access public
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->Addressbook->recursive = -1;
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Addressbook->create($this->request->data);
            if ($this->Addressbook->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else if ($id != null) {
            $this->request->data = $this->Addressbook->find('first', array('conditions' => array('Addressbook.id' => $id)));
        }
    }

    /**
     * Suppression de carnet d'adresse (soft delete)
     *
     * @logical-group Carnet d'adresse
     * @user-profile Admin
     *
     * @access public
     * @param string $id
     * @return void
     */
    public function delete($id = null, $real = true) {
        if ($id != null) {
            $isContactsIn = $this->Addressbook->Contact->find('count', array(
                'conditions' => array(
                    'Contact.addressbook_id' => $id
                )
            ));

            if ($real) {
                $this->Jsonmsg->init(__d('default', 'delete.error'));
                if ($this->Addressbook->delete($id, true)) {
                    $this->Jsonmsg->valid(__d('default', 'delete.ok'));
                }
            } else {

//            if (!$isContactsIn>0) {
//                    $this->Jsonmsg->init(__d('default', 'delete.error'));
//                    if ($this->Addressbook->delete($id)) {
//                            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
//                    }
//            } else {
                $this->Jsonmsg->init();
                $this->Addressbook->id = $id;
                if ($this->Addressbook->saveField('active', false)) {
                    $this->Jsonmsg->valid();
                }
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Vérifier s'il existe deja le nom d'un carnet d'addresse
     *
     *
     * @return $nb
     */
    public function isExistName() {
        $this->autoRender = false;
        $nb = 0;
        if (!empty($this->request->data)) {
            $name = $this->request->data['Import']['carnet_nom'];
            $nb = $this->Addressbook->find('count', array(
                'conditions' => array(
                    'Addressbook.name' => $name
                )
            ));
        }
        return $nb;
    }

    public function removeRepeat($addressbook_id = null) {
        $this->autoRender = false;
        $contents = $this->Addressbook->Contact->find('all', array(
            'fields' => array(
                'Contact.name',
                'Contact.civilite',
                'Contact.nom',
                'Contact.prenom',
                'Contact.addressbook_id',
                'Contact.foreign_key',
                'Contact.active',
                'Contact.citoyen',
                'Contact.slug',
            ),
            'contain' => false,
            /* 'contain' => array(
              'Contactinfo' => array(
              'fields' => array(
              'Contactinfo.name',
              'Contactinfo.civilite',
              'Contactinfo.nom',
              'Contactinfo.prenom',
              'Contactinfo.email',
              'Contactinfo.adresse',
              'Contactinfo.compl',
              'Contactinfo.cp',
              'Contactinfo.ville',
              'Contactinfo.region',
              'Contactinfo.pays',
              'Contactinfo.tel',
              'Contactinfo.role',
              'Contactinfo.organisation',
              'Contactinfo.active',
              'Contactinfo.slug',
              'Contactinfo.numvoie',
              'Contactinfo.typevoie',
              'Contactinfo.nomvoie',
              'Contactinfo.adressecomplete',
              )
              ),
              'Organiseme' => array(
              'fields' => array(
              'addressbook_id'
              )
              )
              ), */
            'conditions' => array(
                'Contact.addressbook_id' => $addressbook_id
            )
        ));
        $addressbook_name = $this->Addressbook->field('Addressbook.name', array('Addressbook.id' => $addressbook_id));
        $addressbook_des = $this->Addressbook->field('Addressbook.description', array('Addressbook.id' => $addressbook_id));
//        $this->Addressbook->delete($addressbook_id, true);
//        $this->Addressbook->id = $addressbook_id;
//         $this->Addressbook->saveField('avtive', false);
//         $this->Addressbook->saveField('name', $addressbook_name.'_old');
//        debug($contents['Organiseme']['adrressbook_id']);
        $new_addressbook = array(
            'name' => $addressbook_name,
            'description' => $addressbook_des
        );
        $this->Addressbook->create();
        $saveAddressBook = $this->Addressbook->save($new_addressbook);
        $this->Addressbook->commit();
        $AddressBook_id = $saveAddressBook['Addressbook']['id'];
        $contacts = array();
        $contactinfos = array();
        // Organisme
        $orgs = $this->Addressbook->Organisme->find('all', array(
            'fields' => array(
                'Organisme.id',
                'Organisme.name',
                'Organisme.addressbook_id',
                'Organisme.ban_id',
                'Organisme.bancommune_id',
                'Organisme.banadresse',
                'Organisme.numvoie',
                'Organisme.adressecomplete',
                'Organisme.compl',
                'Organisme.cp',
                'Organisme.email',
                'Organisme.tel',
                'Organisme.portable',
                'Organisme.fax',
                'Organisme.slug',
                'Organisme.active'
            ),
            'contain' => false,
            'conditions' => array(
                'Organisme.addressbook_id' => $addressbook_id
            )
        ));
        $removeIndexOrg = array();
        $organismes = array();
        foreach ($orgs as $key => $value) {
            array_push($organismes, $value['Organisme']);
            if (!in_array($key, $removeIndexOrg)) {
                $removeIndexList = $this->_findRepeatOrg($key, $value, $orgs);
            }
            foreach ($removeIndexList as $val) {
                unset($orgs[$val]);
                array_push($removeIndexOrg, $val);
            }
        }

        foreach ($orgs as $key => $value) {
            $organisme = $value['Organisme'];
            $organisme['addressbook_id'] = $AddressBook_id;
            $this->Addressbook->Organisme->begin();
            $this->Addressbook->Organisme->create();
            $savedOrganisme = $this->Addressbook->Organisme->save($organisme);
            $saved = !empty($savedOrganisme);
            if ($saved) {
                $this->Addressbook->Organisme->commit();
            } else {
                $this->Addressbook->Organisme->rollback();
            }
        }
        // Fin organisme
        //
        $return = array();
        $removeIndex = array();
        foreach ($contents as $key => $value) {
            array_push($contacts, $value['Contact']);
//            array_push($contactinfos, $value['Contactinfo']);
            if (!in_array($key, $removeIndex)) {
                $removeIndexList = $this->_findRepeat($key, $value, $contacts/* ,$contactinfos */);
            }
            foreach ($removeIndexList as $val) {
                unset($contents[$val]);
                unset($contacts[$val]);
//                unset($contactinfos[$val]);
                array_push($removeIndex, $val);
            }
        }

        foreach ($contents as $key => $value) {
            $contact = $value['Contact'];
            $contact['addressbook_id'] = $AddressBook_id;
//            $contactINFO=$value['Contactinfo'];
            $this->Addressbook->Contact->begin();
            $this->Addressbook->Contact->create();
            $savedContact = $this->Addressbook->Contact->save($contact);
            if (count($contactINFO) == 1) {
//                $this->Addressbook->Contact->Contactinfo->create();
                $contactINFO[0]['contact_id'] = $savedContact['Contact']['id'];
//                $savedContactinfo = $this->Addressbook->Contact->Contactinfo->save($contactINFO[0]);
                $saved = !empty($savedContact) /* && !empty($savedContactinfo) */;
                if ($saved) {
                    $this->Addressbook->Contact->commit();
                } else {
                    $this->Addressbook->Contact->rollback();
                }
            } else {
                for ($j = 0; $j < count($contactINFO); $j++) {
//                    $this->Addressbook->Contact->Contactinfo->create();
//                    $contactINFO[$j]['contact_id'] = $savedContact['Contact']['id'];
//                    $savedContactinfo[$j] = $this->Addressbook->Contact->Contactinfo->save($contactINFO[$j]);
                    $saved = !empty($savedContact) /* /&& !empty($savedContactinfo[$j]) */;
                    if ($saved) {
                        $return[] = true;
                        $this->Addressbook->Contact->commit();
                    } else {
                        $return[] = false;
                        $this->Addressbook->Contact->rollback();
                    }
                }
            }
        }

//        foreach( $orgs as $i => $org ) {
//            $this->Addressbook->Organisme->delete($org['Organisme']['id'], true);
//        }
//
//        $this->Addressbook->delete($addressbook_id, true);

        return !in_array(false, $return, true);
    }

    protected function _findRepeat($j, $item, $contacts, $contactinfos) {
        $removeIndexList = array();
        foreach ($contacts as $i => $contact) {
            if (count(array_diff($item['Contact'], $contact)) == 1) {
                foreach ($contactinfos[$i] as $index => $contactinfo) {
                    if (count(array_diff($item['Contactinfo'][$index], $contactinfo)) == 1) {
                        if ($j != $i) {
                            array_push($removeIndexList, $i);
                        }
                    }
                }
            }
        }
        return $removeIndexList;
    }

    protected function _findRepeatOrg($j, $item, $orgs) {
        $removeIndexList = array();
        foreach ($orgs as $i => $org) {
            if (count(array_diff($item['Organisme'], $org)) == 1) {
                array_push($removeIndexList, $i);
            }
        }
        return $removeIndexList;
    }

    /**
     * Export des carnets d'adresse au format CSV
     *
     * @logical-group Addressbooks
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function export($addressbook_id) {
        ini_set('memory_limit', '3000M');
        $name = Hash::get($this->request->params['named'], 'SearchOrganisme__name');
        $addressbook = Hash::get($this->request->params['named'], 'SearchOrganisme__addressbook_id');
        $organismeActif = Hash::get($this->request->params['named'], 'SearchOrganisme__active');

        $conditions = array();
        $conditionsAddressbook = array(
            'Addressbook.active' => true
        );
        if (!empty($this->request->params)) {

            if (isset($name) && !empty($name)) {
                $conditions[] = 'Organisme.name ILIKE \'' . $this->Addressbook->wildcard('%' . $name . '%') . '\'';
            }

            if (isset($organismeActif) && !empty($organismeActif)) {
                $conditions[] = array('Organisme.active' => $organismeActif);
            }

            if (isset($addressbook) && !empty($addressbook)) {
                $conditions[] = array('Organisme.addressbook_id' => $addressbook);
                $conditionsAddressbook[] = array(
                    'Addressbook.id' => $addressbook
                );
            }
        }

        $querydata = array(
            'conditions' => array(
                $conditionsAddressbook
            ),
            'order' => array(
                'Addressbook.name'
            ),
            'contain' => array(
                'Organisme' => array(
                    'conditions' => array(
                        $conditions
                    ),
                    'order' => array(
                        'Organisme.name ASC'
                    ),
                    'Activite',
                    'Operation',
                    'Contact' => array(
                        'Fonction',
                        'Event',
                        'Operation'
                    )
                )
            )
        );
        $events = $this->Addressbook->Organisme->Contact->Event->find('list', array('conditions' => array('Event.active' => true)));
        $eventColumns = array();
        foreach ($events as $eventsIds => $eventName) {
            $eventColumns[$eventsIds] = strtoupper($eventName);
        }
        $this->set('eventColumns', $eventColumns);

        if (!empty($addressbook_id)) {
            $querydata['conditions'] = array_merge(
                    $querydata['conditions'], array(
                'Addressbook.id' => $addressbook_id
                    )
            );

            $results = $this->Addressbook->find('first', $querydata);
            $civiliteOptions = $this->Session->read( 'Auth.Civilite');

            foreach ($results['Organisme'] as $o => $organisme) {
                $results['Organisme'][$o]['activites'] = implode(' - ', Hash::extract($organisme, 'Activite.{n}.name'));
                $results['Organisme'][$o]['operationsorg'] = implode(' - ', Hash::extract($organisme, 'Operation.{n}.name'));
                foreach ($organisme['Contact'] as $d => $contact) {
                    $results['Organisme'][$o]['Contact'][$d]['civilite'] = $civiliteOptions[$contact['civilite']];
                }
                foreach ($eventColumns as $eventID => $eventName) {
                    foreach ($organisme['Contact'] as $c => $contact) {
                        $results['Organisme'][$o]['Contact'][$c]['operationscontact'] = implode(' - ', Hash::extract($contact, 'Operation.{n}.name'));
                        $results['Organisme'][$o]['Contact'][$c]['events'][$eventID] = '';
                        foreach ($contact['Event'] as $ev => $event) {
                            if ($eventID == $event['id']) {
                                $results['Organisme'][$o]['Contact'][$c]['events'][$eventID] = $eventName;
                            }
                        }
                    }
                }
            }
            $carnetName = $results['Addressbook']['name'];
            $this->set('carnetName', $carnetName);

//            $this->set( 'neweventColumns', $neweventColumns );
//debug($neweventColumns);
//debug($results);
//die();
        } else {
            $civiliteOptions = $this->Session->read( 'Auth.Civilite');
            $results = $this->Addressbook->find('all', $querydata);
            foreach ($results['Organisme'] as $o => $organisme) {
                $results['Organisme'][$o]['activites'] = implode(' - ', Hash::extract($organisme, 'Activite.{n}.name'));
                $results['Organisme'][$o]['operationsorg'] = implode(' - ', Hash::extract($organisme, 'Operation.{n}.name'));
                foreach ($organisme['Contact'] as $d => $contact) {
                    $results['Organisme'][$o]['Contact'][$d]['civilite'] = $civiliteOptions[$contact['civilite']];
                }
                foreach ($eventColumns as $eventID => $eventName) {
                    foreach ($organisme['Contact'] as $c => $contact) {
                        $results['Organisme'][$o]['Contact'][$c]['operationscontact'] = implode(' - ', Hash::extract($contact, 'Operation.{n}.name'));
                        $results['Organisme'][$o]['Contact'][$c]['events'][$eventID] = '';
                        foreach ($contact['Event'] as $ev => $event) {
                            if ($eventID == $event['id']) {
                                $results['Organisme'][$o]['Contact'][$c]['events'][$eventID] = $eventName;
                            }
                        }
                    }
                }
            }
        }
        $this->set('results', $results);


        $this->_setOptions();
        $this->layout = '';
//		$this->set( compact( 'annee', 'origine', 'origineName' ) );
    }

    public function get_organisme($addressbook_id) {
        $querydata = array(
            'fields' => array(
                'Organisme.id',
                'Organisme.name',
                'Organisme.ville',
                'Organisme.active'
            ),
            'conditions' => array(
                'Organisme.addressbook_id' => $addressbook_id
            ),
            'order' => array(
                'Organisme.name' => 'ASC'
            ),
            'contain' => FALSE,
            'limit' => 12
        );
        $this->paginate = $querydata;
        $organismes_tmp = $this->paginate($this->Addressbook->Organisme);

        $addressbook = $this->Addressbook->find(
            'first',
            array(
                'fields' => array(
                    'Addressbook.name'
                ),
                'conditions' => array(
                    'Addressbook.id' => $addressbook_id
                ),
                'contain' => false,
                'recursive' => -1
            )
        );
        $carnetname = $addressbook['Addressbook']['name'];
        $this->set( compact('carnetname'));

        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        $desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
        $isAdmin = false;
        $groupName = $this->Session->read('Auth.User.Desktop.Profil');
        if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
            $isAdmin = true;
        }
        foreach ($organismes_tmp as $i => $organisme) {
            $organismes[$i]['name'] = $organisme['Organisme']['name'] . ' ( '.$organisme['Organisme']['ville'].' ) '.  '<span style="color:#333333"> (' . $this->Addressbook->Organisme->Contact->getNombreContactByOrganismeId($organisme["Organisme"]["id"]) . ') <span>';
            $organismes[$i]['active'] = $organisme['Organisme']['active'];
            $organismes[$i]['view'] = '<a href="#" class = "viewOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Visualiser"><i class="fa fa-eye" aria-hidden="true" style="color:#5397a7"></i></a>';
            $organismes[$i]['edit'] = '<a href="#" class="editOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Modifier"><i class="fa fa-pencil" aria-hidden="true" style="color:#5397a7"></i></a>';
            if( $isAdmin ) {
                $organismes[$i]['delete'] = '<a href="#" class="deleteOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Supprimer" addressbookId="' . $addressbook_id . '"><i class="fa fa-trash" aria-hidden="true" style="color:red"></i></a>';
            }
            $organismes[$i]['exportcsv'] = '<a href="#" class="exportOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Exporter"><i class="fa fa-download" aria-hidden="true" style="color:#5397a7"></i></a>';
        }

        if (!empty($organismes)) {
            $this->set('organismes', $organismes);
        } else {
            $this->set('organismes', array());
        }
        $this->_setOptions();
    }

    public function searchResultat() {
        $this->autoRender = false;
        $this->Session->delete('searchAddressbook');
        $conditionsOrganisme = array();
        $conditionsOrganismeContact = array();
        $conditionsContact = array();
        if (!empty($this->request->data)) {
            if (isset($this->request->data['Search']['Organisme']['addressbook_id']) && !empty($this->request->data['Search']['Organisme']['addressbook_id'])) {
                $conditionsOrganisme[] = array('Organisme.addressbook_id' => $this->request->data['Search']['Organisme']['addressbook_id']);
            }
            if (isset($this->request->data['Search']['Organisme']['name']) && !empty($this->request->data['Search']['Organisme']['name'])) {
                $conditionsOrganisme[] = array('Organisme.name ILIKE \'' . $this->Addressbook->wildcard('%' . $this->request->data['Search']['Organisme']['name'] . '%') . '\'');
            }
            if (isset($this->request->data['Search']['Organisme']['active']) && !empty($this->request->data['Search']['Organisme']['active'])) {
                $conditionsOrganisme[] = array('Organisme.active' => $this->request->data['Search']['Organisme']['active']);
            }
			if (isset($this->request->data['Search']['Organisme']['Activite']['id']) && !empty($this->request->data['Search']['Organisme']['Activite']['id'])) {
				$orgsActivites = $this->Addressbook->Organisme->ActiviteOrganisme->find('all', array(
					'conditions' => array(
						'ActiviteOrganisme.activite_id' => $this->request->data['Search']['Organisme']['Activite']['id']
					),
					'contain' => false
				));

				if( !empty( $orgsActivites ) ) {
					$orgsIds = Hash::extract($orgsActivites, '{n}.ActiviteOrganisme.organisme_id');
					$conditionsOrganisme[] = array('Organisme.id' => $orgsIds);
				}
				else {
					$conditionsOrganisme[] = array();
				}
				$conditionsOrganisme[] = array('ActiviteOrganisme.activite_id' => $this->request->data['Search']['Organisme']['Activite']['id']);
			}

            if (isset($this->request->data['Search']['Contact']['organisme_id']) && !empty($this->request->data['Search']['Contact']['organisme_id'])) {
                $conditionsContact[] = array('Contact.organisme_id' => $this->request->data['Search']['Contact']['organisme_id']);
            }
            if (isset($this->request->data['Search']['Contact']['name']) && !empty($this->request->data['Search']['Contact']['name'])) {
                $conditionsContact[] = array('Contact.name ILIKE \'' . $this->Addressbook->wildcard('%' . $this->request->data['Search']['Contact']['name'] . '%') . '\'');
            }
            if (isset($this->request->data['Search']['Contact']['nom']) && !empty($this->request->data['Search']['Contact']['nom'])) {
                $conditionsContact[] = array('Contact.nom ILIKE \'' . $this->Addressbook->wildcard('%' . $this->request->data['Search']['Contact']['nom'] . '%') . '\'');
            }
            if (isset($this->request->data['Search']['Contact']['prenom']) && !empty($this->request->data['Search']['Contact']['prenom'])) {
                $conditionsContact[] = array('Contact.prenom ILIKE \'' . $this->Addressbook->wildcard('%' . $this->request->data['Search']['Contact']['prenom'] . '%') . '\'');
            }
            if (isset($this->request->data['Search']['Contact']['active']) && !empty($this->request->data['Search']['Contact']['active'])) {
                $conditionsContact[] = array('Contact.active' => $this->request->data['Search']['Contact']['active']);
            }
			if (isset($this->request->data['Search']['Contact']['Fonction']['id']) && !empty($this->request->data['Search']['Contact']['Fonction']['id'])) {
				$contactsFonction = $this->Addressbook->Contact->find('all', array(
					'conditions' => array(
						'Contact.fonction_id' => $this->request->data['Search']['Contact']['Fonction']['id']
					),
					'contain' => false
				));
				$contactsIds = Hash::extract($contactsFonction, '{n}.Contact.id');
				$conditionsContact[] = array('Contact.id' => $contactsIds);
			}
            if (Configure::read('Conf.SAERP')) {
                // Filtre sur les OP de l'organisme
//                if (isset($this->request->data['Search']['Organisme']['Operation']['id']) && !empty($this->request->data['Search']['Organisme']['Operation']['id'])) {
//                    $orgsOps = $this->Addressbook->Organisme->OrganismeOperation->find('all', array(
//                        'conditions' => array(
//                            'OrganismeOperation.operation_id' => $this->request->data['Search']['Organisme']['Operation']['id']
//                        ),
//                        'contain' => false
//                    ));
//                    $orgsIds = Hash::extract($orgsOps, '{n}.OrganismeOperation.organisme_id');
//                    $conditionsOrganisme[] = array('Organisme.id' => $orgsIds);
//                }

                // Filtre sur les activités de l'organisme
//                if (isset($this->request->data['Search']['Organisme']['Activite']['id']) && !empty($this->request->data['Search']['Organisme']['Activite']['id'])) {
////                    $orgsActivites = $this->Addressbook->Organisme->ActiviteOrganisme->find('all', array(
////                        'conditions' => array(
////                            'ActiviteOrganisme.activite_id' => $this->request->data['Search']['Organisme']['Activite']['id']
////                        ),
////                        'contain' => false
////                    ));
//////debug($orgsActivites);die();
////                    if( !empty( $orgsActivites ) ) {
////                        $orgsIds = Hash::extract($orgsActivites, '{n}.ActiviteOrganisme.organisme_id');
////                        $conditionsOrganisme[] = array('Organisme.id' => $orgsIds);
////                    }
////                    else {
////                        $conditionsOrganisme[] = array();
////                    }
//
//
//                    $conditionsOrganisme[] = array('ActiviteOrganisme.activite_id' => $this->request->data['Search']['Organisme']['Activite']['id']);
//                }
            }
//debug($this->request->data);die();
                // Filtre sur les fonctions de contact
                /*if (isset($this->request->data['Search']['Organisme']['Fonction']['id']) && !empty($this->request->data['Search']['Organisme']['Fonction']['id'])) {
                    $contactsFonction = $this->Addressbook->Contact->find('all', array(
                        'conditions' => array(
                            'Contact.fonction_id' => $this->request->data['Search']['Organisme']['Fonction']['id']
                        ),
                        'contain' => false
                    ));
                    $contactsIds = Hash::extract($contactsFonction, '{n}.Contact.id');
//debug($contactsFonction);
                    $conditionsOrganismeContact[] = array('Contact.id' => $contactsIds);

                    $contactsWithFonction = $this->Addressbook->Contact->find(
                        'all',
                        array(
                            'conditions' => array(
                                'Contact.id' => $contactsIds
                            ),
                            'contain' => false
                        )
                    );

                    $orgsContactsFonctionsIds = Hash::extract($contactsWithFonction, '{n}.Contact.organisme_id');
                    $conditionsOrganisme[] = array('Organisme.id' => $orgsContactsFonctionsIds );
                }

                // Filtre sur les événements des contacts
                if (isset($this->request->data['Search']['Organisme']['Event']['id']) && !empty($this->request->data['Search']['Organisme']['Event']['id'])) {
                    $contactsEvent = $this->Addressbook->Contact->ContactEvent->find('all', array(
                        'conditions' => array(
                            'ContactEvent.event_id' => $this->request->data['Search']['Organisme']['Event']['id']
                        ),
                        'contain' => false
                    ));
                    $contactsEventsIds = Hash::extract($contactsEvent, '{n}.ContactEvent.contact_id');
                    $conditionsOrganismeContact[] = array('Contact.id' => $contactsEventsIds);

                    $contactsWithEvent = $this->Addressbook->Contact->find(
                        'all',
                        array(
                            'conditions' => array(
                                'Contact.id' => $contactsEventsIds
                            ),
                            'contain' => false
                        )
                    );

                    $orgsContactsEventsIds = Hash::extract($contactsWithEvent, '{n}.Contact.organisme_id');
                    $conditionsOrganisme[] = array('Organisme.id' => $orgsContactsEventsIds );
                }


                // Filtre sur les OP du contact
                if (isset($this->request->data['Search']['Contact']['Operation']['id']) && !empty($this->request->data['Search']['Contact']['Operation']['id'])) {
                    $orgsOps = $this->Addressbook->Contact->ContactOperation->find('all', array(
                        'conditions' => array(
                            'ContactOperation.operation_id' => $this->request->data['Search']['Contact']['Operation']['id']
                        ),
                        'contain' => false
                    ));
                    $contactsIds = Hash::extract($orgsOps, '{n}.ContactOperation.contact_id');
                    $conditionsContact[] = array('Contact.id' => $contactsIds);
                }
                // Filtre sur les fonctions de contact
                if (isset($this->request->data['Search']['Contact']['Fonction']['id']) && !empty($this->request->data['Search']['Contact']['Fonction']['id'])) {
                    $contactsFonction = $this->Addressbook->Contact->find('all', array(
                        'conditions' => array(
                            'Contact.fonction_id' => $this->request->data['Search']['Contact']['Fonction']['id']
                        ),
                        'contain' => false
                    ));
                    $contactsIds = Hash::extract($contactsFonction, '{n}.Contact.id');
//debug($contactsFonction);
                    $conditionsContact[] = array('Contact.id' => $contactsIds);
                }

                // Filtre sur les événements des contacts
                if (isset($this->request->data['Search']['Contact']['Event']['id']) && !empty($this->request->data['Search']['Contact']['Event']['id'])) {
                    $contactsEvent = $this->Addressbook->Contact->ContactEvent->find('all', array(
                        'conditions' => array(
                            'ContactEvent.event_id' => $this->request->data['Search']['Contact']['Event']['id']
                        ),
                        'contain' => false
                    ));
                    $contactsEventsIds = Hash::extract($contactsEvent, '{n}.ContactEvent.contact_id');
                    $conditionsContact[] = array('Contact.id' => $contactsEventsIds);
                }
            }*/
//debug($conditionsOrganisme);
//debug($this->Addressbook->Organisme->sq( $querydataOrganisme));

            if ($this->request->data['Search']['search_zone'] == 'organisme') {

                $querydataOrganisme = $this->Addressbook->Organisme->search($this->request->data['Search'], null );
//                $resultat['organismes'] = $this->Addressbook->Organisme->find( 'all', $querydataOrganisme );

//                $querydataOrganisme = array(
//                    'fields' => array(
//                        'Organisme.id',
//                        'Organisme.name',
//                        'Organisme.addressbook_id'
//                    ),
//                    'conditions' => array(
//                        $conditionsOrganisme
//                    ),
//                    'order' => array(
//                        'Organisme.name'
//                    ),
//                    'contain' => array(
//                        'Operation',
//                        'Contact' => array(
//                            'conditions' => array($conditionsOrganismeContact)
//                        )
//                    ),
//                    'recursive' => -1,
//                    'limit' => 12
//                );
//debug($resultat['organismes']);
//                $organismes_tmp = $this->Addressbook->Organisme->find('all', $querydataOrganisme);
//                $organismes = array();
//                foreach ($organismes_tmp as $i => $organisme) {
//                    $organismes[$i]['name'] = $organisme['Organisme']['name'];
//                    $organismes[$i]['view'] = '<a href="#" class = "viewOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Visualiser"><i class="fa fa-eye" aria-hidden="true" style="color:#5397a7"></i></a>';
//                    $organismes[$i]['edit'] = '<a href="#" class="editOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Modifier"><i class="fa fa-pencil" aria-hidden="true" style="color:#5397a7"></i></a>';
//                    $organismes[$i]['delete'] = '<a href="#" class="deleteOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Supprimer"><i class="fa fa-trash" aria-hidden="true" style="color:red"></i></a>';
//                    $organismes[$i]['exportcsv'] = '<a href="#" class="exportOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Exporter"><i class="fa fa-download" aria-hidden="true" style="color:#5397a7"></i></a>';
//                }
                $resultat['organismes'] = $querydataOrganisme;
            } else if ($this->request->data['Search']['search_zone'] == 'contact') {
//debug($this->request->data);
                $querydataContact = $this->Addressbook->Organisme->Contact->search($this->request->data['Search'], null, null );

//                $querydataContact = array(
//                    'fields' => array(
//                        'Contact.id',
//                        'Contact.name',
//                        'Contact.organisme_id'
//                    ),
//                    'conditions' => array(
//                        $conditionsContact
//                    ),
//                    'order' => array(
//                        'Contact.name'
//                    ),
//                    'recursive' => -1,
//                    'limit' => 12
//                );
//debug($querydataContact);
//                $contacts_tmp = $this->Addressbook->Organisme->Contact->find('all', $querydataContact);
//                $contacts = array();
//                foreach ($contacts_tmp as $i => $contact) {
//                    $contacts[$i]['name'] = $contact['Contact']['name'];
//                    $contacts[$i]['view'] = '<a href="#" class = "viewContact" id="contact_' . $contact["Contact"]["id"] . '" title="Visualiser"><i class="fa fa-eye" aria-hidden="true" style="color:#5397a7"></i></a>';
//                    $contacts[$i]['edit'] = '<a href="#" class="editContact" id="contact_' . $contact['Contact']["id"] . '" title="Modifier"><i class="fa fa-pencil" aria-hidden="true" style="color:#5397a7"></i></a>';
//                    $contacts[$i]['delete'] = '<a href="#" class="deleteContact" id="contact_' . $contact['Contact']["id"] . '" title="Supprimer"><i class="fa fa-trash" aria-hidden="true" style="color:red"></i></a>';
//                    $contacts[$i]['exportcsv'] = '<a href="#" class="exportContact" id="contact_' . $contact['Contact']["id"] . '" title="Exporter"><i class="fa fa-download" aria-hidden="true" style="color:#5397a7"></i></a>';
//                }
                $resultat['contacts'] = $querydataContact;
            }
//debug($resultat);
            return json_encode($resultat);
        }
    }

}
