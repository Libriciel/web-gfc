<?php

/**
 * Intituleagentbyoperations
 *
 * Intituleagentbyoperations controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class IntituleagentbyoperationsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Intituleagentbyoperations';
    public $uses = array('Intituleagentbyoperation');

    /**
     * Gestion des emails interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'Intituleagentbyoperationbyoperations', true)
        ));
    }

    /**
     * Ajout d'un email
     *
     * @logical-group Intituleagentbyoperationbyoperations
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {

        $this->Jsonmsg->init();
        $intituleagentbyoperations = $this->Intituleagentbyoperation->find('all', array('contain' => false));
        $ops = Hash::extract($intituleagentbyoperations, '{n}.Intituleagentbyoperation.operation_id');
        $desktopsmanagers = $this->Intituleagentbyoperation->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true), 'order' => array('Desktopmanager.name ASC')));
        if (!empty($ops)) {
            $operations = $this->Intituleagentbyoperation->Operation->find(
                    'list', array(
                'conditions' => array(
                    'Operation.active' => true,
                    'Operation.id NOT IN' => $ops
                ),
                'order' => array('Operation.name ASC')
                    )
            );
        } else {
            $operations = $this->Intituleagentbyoperation->Operation->find(
                    'list', array(
                'conditions' => array(
                    'Operation.active' => true
                ),
                'order' => array('Operation.name ASC')
                    )
            );
        }
        $intitulesagents = $this->Intituleagentbyoperation->Intituleagent->find(
                'all', array(
            'conditions' => array(
                'Intituleagent.active' => true
            ),
            'contain' => array(
                'Desktopmanager' => array(
                    'order' => array(
                        'Desktopmanager.name ASC'
                    )
                )
            ),
            'order' => 'Intituleagent.name ASC'
                )
        );
        $this->set('intitulesagents', $intitulesagents);
        $this->set(compact('desktopsmanagers', 'operations', 'intitulesagents'));


        $intituleagentbyoperation = $this->request->data;

        $valuesToSave = array();
        $liaison = array();
        $save = false;

        // On sauvegarde d'abord la liasion entre les postes et le sbureaux (AO = Bureau 1, RO = Bureau 2 ....)
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            if (!empty($intituleagentbyoperation['Intituleagentbyoperation'])) {

                foreach ($intituleagentbyoperation['Intituleagentbyoperation'] as $key => $intitules) {
                    if (!empty($intitules['desktopmanager_id'])) {
                        $intitules['operation_id'] = $intituleagentbyoperation['Intituleagentbyoperation']['operation_id'];
                        $liaison[$key]['Intituleagentbyoperation'] = $intitules;
                        unset($liaison['operation_id']['Intituleagentbyoperation']);
                        unset($liaison['operation_id']);
                    }
                }

                $this->Intituleagentbyoperation->begin();
                $this->Intituleagentbyoperation->create();
                $save = $this->Intituleagentbyoperation->saveAll($liaison);
                if ($save) {
                    $this->Intituleagentbyoperation->commit();
                    $this->Jsonmsg->valid();
                } else {
                    $this->Intituleagentbyoperation->rollback();
                }
                $this->Jsonmsg->send();
            }
        }
    }

    /**
     * Edition d'un type
     *
     * @logical-group Intituleagentbyoperationbyoperations
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {

        $this->set('id', $id);
        $intituleagentbyoperations = $this->Intituleagentbyoperation->find('all', array('contain' => false));
        $ops = Hash::extract($intituleagentbyoperations, '{n}.Intituleagentbyoperation.operation_id');

        $desktopsmanagers = $this->Intituleagentbyoperation->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true), 'order' => array('Desktopmanager.name ASC')));
        $operations = $this->Intituleagentbyoperation->Operation->find(
                'list', array(
            'conditions' => array(
                'Operation.active' => true,
                'OR' => array(
                    'Operation.id NOT IN' => $ops,
                    'Operation.id' => $id
                )
            ),
            'order' => array('Operation.name ASC')
                )
        );
        $intitulesagents = $this->Intituleagentbyoperation->Intituleagent->find(
                'all', array(
            'conditions' => array(
                'Intituleagent.active' => true
            ),
            'contain' => array(
                'Desktopmanager' => array(
                    'order' => array(
                        'Desktopmanager.name ASC'
                    )
                )
            ),
            'order' => 'Intituleagent.name ASC'
                )
        );
        $this->set('intitulesagents', $intitulesagents);
        $this->set(compact('desktopsmanagers', 'operations', 'intitulesagents'));
//debug($intitulesagents);
//die();
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();


            $liaison = array();
            foreach ($this->request->data as $intituleagent_id => $intitules) {
                if (!empty($intitules['Intituleagentbyoperation']['desktopmanager_id'])) {
                    $intitules['Intituleagentbyoperation']['operation_id'] = $this->request->data['Intituleagentbyoperation']['operation_id'];
                    $liaison[] = $intitules;
                    unset($liaison['Intituleagentbyoperation']['operation_id']);
                    unset($liaison['operation_id']);
                }
            }



            // on regarde les anciens enregistrements entre le circut et les sous-types
            $records = $this->Intituleagentbyoperation->find(
                    'list', array(
                'fields' => array("Intituleagentbyoperation.id", "Intituleagentbyoperation.operation_id"),
                'conditions' => array(
                    "Intituleagentbyoperation.operation_id" => $id
                )
                    )
            );

            $oldrecordsids = array_keys($records);
            // on regarde les nouveautés
            $nouveauxids = Hash::extract($this->request->data, "Intituleagentbyoperation.{n}");
            // si des différences existent on note les ids enlevés, les liens coupés entre circuit et sous-types
            $idsenmoins = array_diff($oldrecordsids, $nouveauxids);
            // on met à jour les sous-types qui ne sont plus liés au circuit en cours
            if (!empty($idsenmoins)) {
                $success = $this->Intituleagentbyoperation->deleteAll(
                        array(
                            'Intituleagentbyoperation.operation_id' => $id,
                            'Intituleagentbyoperation.id' => $idsenmoins
                        )
                );
            }

            $this->Intituleagentbyoperation->begin();
            $this->Intituleagentbyoperation->create();

            $save = $this->Intituleagentbyoperation->saveAll($liaison);
//debug($this->Intituleagentbyoperation->validationErrors);
            if ($save) {
                $this->Intituleagentbyoperation->commit();
                $this->Jsonmsg->valid();
            } else {
                $this->Intituleagentbyoperation->rollback();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Intituleagentbyoperation->find(
                    'all', array(
                'conditions' => array(
                    'Intituleagentbyoperation.operation_id' => $id
                ),
                'contain' => false
                    )
            );

            $intitulesagentsbyoperations_tmp = $this->request->data;
            $intitulesagentsbyoperations = array();
            foreach ($intitulesagentsbyoperations_tmp as $i => $values) {
                $intitulesagentsbyoperations[$values['Intituleagentbyoperation']['intituleagent_id']] = $values;
            }
//debug($intitulesagentsbyoperations);
            $this->set(compact('intitulesagentsbyoperations'));
            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
    }

    /**
     * Suppression d'un email
     *
     * @logical-group Intituleagentbyoperationbyoperations
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Intituleagentbyoperation->begin();
        $intuleagentbyoperations = $this->Intituleagentbyoperation->find(
                'all', array(
            'conditions' => array(
                'Intituleagentbyoperation.operation_id' => $id
            ),
            'contain' => false
                )
        );

        foreach ($intuleagentbyoperations as $i => $intuleagentbyoperation) {
            $ids = $intuleagentbyoperation['Intituleagentbyoperation']['id'];
            if ($this->Intituleagentbyoperation->delete($ids)) {
                $this->Intituleagentbyoperation->commit();
                $this->Jsonmsg->valid(__d('default', 'delete.ok'));
            } else {
                $this->Intituleagentbyoperation->rollback();
            }
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des mails à scruter (ajax)
     *
     * @logical-group Intituleagentbyoperationbyoperations
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getIntituleagentbyoperations() {


        $querydata = $this->Intituleagentbyoperation->search($this->request->data);
//        $this->paginate = $querydata;
//        $intituleagentbyoperations_tmp = $this->paginate($this->Intituleagentbyoperation);
        $intituleagentbyoperations_tmp = $this->Intituleagentbyoperation->find('all', $querydata);

//debug($intituleagentbyoperations_tmp);
        /* intituleagentbyoperations_tmp = $this->Intituleagentbyoperation->find(
          "all",
          array(
          'contain' => false,
          'order' => array('Intituleagentbyoperation.operation_id ASC')
          )
          ); */
        $intituleagentbyoperations = array();
        foreach ($intituleagentbyoperations_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $intituleagentbyoperations[$item['Intituleagentbyoperation']['operation_id']]['Intituleagentbyoperation'][] = $item['Intituleagentbyoperation'];
        }
        $operations = $this->Intituleagentbyoperation->Operation->find('list', array('conditions' => array('Operation.active' => true), 'order' => array('Operation.name ASC')));
        $intituleagents = $this->Intituleagentbyoperation->Intituleagent->find('list', array('conditions' => array('Intituleagent.active' => true), 'order' => array('Intituleagent.name ASC')));
        $desktopsmanagers = $this->Intituleagentbyoperation->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true), 'order' => array('Desktopmanager.name ASC')));
        $this->set(compact('intituleagentbyoperations', 'operations', 'intituleagents', 'desktopsmanagers'));
    }

}

?>
