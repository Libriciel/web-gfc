<?php

/**
 * Events
 *
 * Events controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class EventsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Events';
    public $uses = array('Event');

    /**
     * Gestion des emails interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'Events', true)
        ));
    }

    /**
     * Ajout d'un email
     *
     * @logical-group Events
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $event = $this->request->data;


            $this->Event->begin();
            $this->Event->create($event);

            if ($this->Event->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Event->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Event->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }
    }

    /**
     * Edition d'un type
     *
     * @logical-group Events
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Event->create();
            $event = $this->request->data;

            if ($this->Event->save($event)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Event->find(
                    'first', array(
                'conditions' => array('Event.id' => $id),
                'contain' => false
                    )
            );

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
    }

    /**
     * Suppression d'un email
     *
     * @logical-group Events
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Event->begin();
        if ($this->Event->delete($id)) {
            $this->Event->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Event->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des mails à scruter (ajax)
     *
     * @logical-group Events
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getEvents() {
        $events_tmp = $this->Event->find(
                "all", array(
            'order' => 'Event.name'
                )
        );
        $events = array();
        foreach ($events_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $events[] = $item;
        }
        $this->set(compact('events'));
    }

}

?>
