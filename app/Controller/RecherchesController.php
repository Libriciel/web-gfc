<?php

/**
 * Recherches
 *
 * Recherches controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class RecherchesController extends AppController
{

	/**
	 * Controller name
	 *
	 * @var string
	 * @access public
	 */
	public $name = 'Recherches';

	/**
	 * Controller uses
	 *
	 * @var array
	 * @access public
	 */
	public $uses = array('Recherche', 'Courrier', 'Ordreservice', 'Pliconsultatif', 'Consultation');

	/**
	 * Controller components
	 *
	 * @var array
	 * @access public
	 */
	public $components = array('DataAuthorized', 'Progress');

	/**
	 * Controller helpers
	 *
	 * @var array
	 * @access public
	 */
	public $helpers = array('Csv', 'Paginator');

	/**
	 *
	 */
	public function beforeFilter()
	{
		$this->Auth->allow(array('index', 'exportcsv', 'export', 'progress', 'long_process', 'ajaxSavedSearch', 'deleteSearch'));
		parent::beforeFilter();
	}

	protected function _setOptions()
	{
		$this->loadModel('Desktop');
		$this->set('taches', $this->Recherche->Tache->find('list', array('order' => array('Tache.name ASC'))));
		$this->set('circuits', $this->Recherche->Circuit->find('list', array('order' => array('Circuit.nom ASC'))));
		$this->set('dossiers', $this->Recherche->Dossier->find('list', array('order' => array('Dossier.name ASC'))));
		$this->set('affaires', $this->Recherche->Affaire->find('list', array('order' => array('Affaire.name ASC'))));
		$this->set('origineflux', $this->Courrier->Origineflux->find('list', array('order' => array('Origineflux.name ASC'))));
		$metas = $this->Recherche->Metadonnee->find('all', array('contain' => array('Selectvaluemetadonnee', 'Typemetadonnee'), 'order' => ['Metadonnee.id ASC']));
		$this->set('metas', $metas);
		$valueSelectMeta = array('Non' => 'Non', 'Oui' => 'Oui');
		$this->set('valueSelectMeta', $valueSelectMeta);
		$contacts = $this->Recherche->Contact->find('list', array('order' => 'Contact.name ASC'));
		$this->set('contacts', $contacts);
		if (Configure::read('CD') != 81) {
			$organismes = $this->Recherche->Organisme->find('list', array('order' => 'Organisme.name ASC'));
			$this->set('organismes', $organismes);
		} else {
			$organsme = $this->Recherche->Organisme->find('list', array('fields' => array('Organisme.name', 'Organisme.ville', 'Organisme.id'), 'order' => array('Organisme.name ASC')));
			$organismes = array();
			foreach ($organsme as $id => $value) {
				$org = implode(",", array_keys($value));
				$ville = implode(",", $value);
				$organismes[$id] = $org . ' ( ' . $ville . ' )';
			}
			$this->set('organismes', $organismes);

		}

		if (Configure::read('Use.AuthRights') && !Configure::read('Conf.SAERP')) {
			$this->set('types', $this->Session->read('Auth.TypesAll'));
		} else {
			$this->set('types', $this->Courrier->Soustype->Type->find('all', array('contain' => array('Soustype' => array('order' => 'Soustype.name ASC')), 'order' => array('Type.name ASC'), 'recursive' => -1)));
		}

		$this->set('metadonnees', $this->Session->read('Auth.MetasList'));

		$affairesuiviepars = $this->Courrier->Desktop->getDesktopsByService('all');
		ksort($affairesuiviepars);
		$this->set('affairesuiviepars', $affairesuiviepars);

		// Adresse
		$this->set('bans', $this->Session->read('Auth.Ban'));

		if (Configure::read('CD') == 81) {
			$this->set(
				'etat',
				array(
					'-1' => 'Refusé',
					'1' => 'En cours de traitement',
					'2' => 'Clos'
				)
			);
		} else {
			$this->set(
				'etat',
				array(
					'-1' => 'Refusé',
					'0' => 'Validé',
					'1' => 'En cours de traitement',
					'2' => 'Clos'
				)
			);
		}
		$services = $this->Recherche->Service->find('list', array('order' => 'Service.name ASC'));
		$this->Organisme = ClassRegistry::init('Organisme');
		$this->set('activites', $this->Organisme->Activite->find('list', array('order' => array('Activite.name ASC'), 'conditions' => array('Activite.active' => true))));
		/////////////////////////////
		$this->set('services', $services);
		if (Configure::read('Conf.SAERP')) {
			$marches = $this->Courrier->Marche->find(
				'list', array(
					'joins' => array(
						$this->Courrier->Marche->join('Operation')
					),
					'conditions' => array(
						'Marche.active' => true,
						'Operation.active' => true
					),
					'fields' => array(
						'Marche.numero'
					),
					'order' => array('Marche.name ASC')
				)
			);
			$this->set(compact('marches'));
			$os = $this->Courrier->Ordreservice->find(
				'list', array(
					'fields' => array(
						'Ordreservice.name'
					),
					'order' => array('Ordreservice.name ASC')
				)
			);
			$plis = $this->Courrier->Pliconsultatif->find(
				'list', array(
					'fields' => array(
						'Pliconsultatif.name'
					),
					'order' => array('Pliconsultatif.name ASC')
				)
			);
			$typedocument = array(
				'OS' => 'Ordre de service',
				'LN' => 'Lettre de notification',
				'LC' => 'Lettre de commandes',
				'PLI' => 'Pli'
			);
			$this->set(compact('typedocument'));
			$this->set(compact('os', 'plis'));

			$this->Organisme = ClassRegistry::init('Organisme');
			$this->set('activites', $this->Organisme->Activite->find('list', array('order' => array('Activite.name ASC'), 'conditions' => array('Activite.active' => true))));
			$this->set('events', $this->Organisme->Contact->Event->find('list', array('order' => array('Event.name ASC'), 'conditions' => array('Event.active' => true))));
			$this->set('fonctions', $this->Organisme->Contact->Fonction->find('list', array('order' => array('Fonction.name ASC'), 'conditions' => array('Fonction.active' => true))));

			$operations = $this->Courrier->Marche->Operation->find('list', array('conditions' => array('Operation.active' => true), 'order' => 'Operation.name ASC'));
			$this->set('operations', $operations);

			// Pour les N° RAR
			$rarId = $this->Courrier->Metadonnee->find('first', array('contain' => false, 'recursive' => -1, 'conditions' => array('Metadonnee.id' => Configure::read('MetadonnneRar.id'))));
			$rars = array();
			if (!empty($rarId)) {
				$rars = $this->Courrier->CourrierMetadonnee->find(
					'list',
					array(
						'fields' => array(
							'CourrierMetadonnee.valeur'
						),
						'conditions' => array(
							'CourrierMetadonnee.metadonnee_id' => $rarId['Metadonnee']['id']
						),
						'recursive' => -1,
						'contain' => false
					)
				);
			}
			$this->set('rars', $rars);
			$this->set('operations', $operations);
		}

		$conn = $this->Session->read('Auth.User.connName');
		$this->loadModel('Collectivite');
		$this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));

		$titres = $this->Recherche->Contact->Titre->find('list', array('order' => 'Titre.name ASC'));
		$this->set('titres', $titres);

		$desktops = $this->Session->read('Auth.Desktops');
		$this->set('desktops', $desktops);

		$users = $this->Session->read('Auth.AllUsers');
		$this->set('users', $users);
	}

	/**
	 * Index method
	 *
	 * @access public
	 * @return void
	 */
	public function index()
	{
		$this->set('ariane', array(__d('menu', 'Recherche', true)));
	}

	/**
	 * Formulaire de recherche (enregistrement de recherche)
	 *
	 * @logical-group Recherches
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id
	 * @param string $forcemode
	 * @return void
	 */
	public function formulaire($id = null, $forcemode = null)
	{
		//enregistrement des critères de la recherche
//$this->log($this->request->data);
		if (!empty($this->request->data)) {
//			$this->Jsonmsg->init();
//			$data = array();
//			$data['Recherche'] = $this->request->data['Recherche'];
//			if (!empty($this->request->data['Contact']['Contact'])) {
//				$data['Contact'] = array();
//				foreach ($this->request->data['Contact']['Contact'] as $kcontact => $contact) {
//					$data['Contact']['Contact'][$kcontact] = $contact;
//				}
//			}
//
//			if (!empty($this->request->data['Type']['Type'])) {
//				foreach ($this->request->data['Type']['Type'] as $ktype => $type) {
//					$data['Type']['Type'][$ktype] = $type;
//				}
//			}
//
//			if (!empty($this->request->data['Soustype']['Soustype'])) {
//				foreach ($this->request->data['Soustype']['Soustype'] as $ksoustype => $soustype) {
//					$data['Soustype']['Soustype'][$ksoustype] = $soustype;
//				}
//			}
//
//			if (!empty($this->request->data['Circuit']['Circuit'])) {
//				$data['Circuit'] = array();
//				foreach ($this->request->data['Circuit']['Circuit'] as $kcircuit => $circuit) {
//					$data['Circuit']['Circuit'][$kcircuit] = $circuit;
//				}
//			}
//
//			if (!empty($this->request->data['Dossier']['Dossier'])) {
//				$data['Dossier'] = array();
//				foreach ($this->request->data['Dossier']['Dossier'] as $kdossier => $dossier) {
//					$data['Dossier']['Dossier'][$kdossier] = $dossier;
//				}
//			}
//
//			if (!empty($this->request->data['Affaire']['Affaire'])) {
//				$data['Affaire'] = array();
//				foreach ($this->request->data['Affaire']['Affaire'] as $kaffaire => $affaire) {
//					$data['Affaire']['Affaire'][$kaffaire] = $affaire;
//				}
//			}
//
//			if (!empty($this->request->data['Tache']['Tache'])) {
//				$data['Tache'] = array();
//				foreach ($this->request->data['Tache']['Tache'] as $ktache => $tache) {
//					$data['Tache']['Tache'][$ktache] = $tache;
//				}
//			}
//
//			// Métadonnées
//			if (!empty($this->request->data['Metadonnee']['Metadonnee'])) {
//				foreach ($this->request->data['Metadonnee']['Metadonnee'] as $kmetadonnee => $metadonnee) {
//					$data['Metadonnee']['Metadonnee'][$kmetadonnee] = $metadonnee;
//				}
//			}
//
//			// Valeur par Métadonnées
//			if (!empty($this->request->data['Selectvaluemetadonnee']['Selectvaluemetadonnee'])) {
//				foreach ($this->request->data['Selectvaluemetadonnee']['Selectvaluemetadonnee'] as $typemetadonneeid => $valueMetadonnee) {
//					$data['Selectvaluemetadonnee']['Selectvaluemetadonnee'][$typemetadonneeid] = $valueMetadonnee;
//				}
//			}
//
//			if (!empty($this->request->data['Desktop']['Desktop'])) {
//				foreach ($this->request->data['Desktop']['Desktop'] as $kdesktop => $desktop) {
//					$data['Desktop']['Desktop'][$kdesktop] = $desktop;
//				}
//			}
//
//			if (!empty($this->request->data['Organisme']['Organisme'])) {
//				$data['Organisme'] = array();
//				foreach ($this->request->data['Organisme']['Organisme'] as $korganisme => $organisme) {
//					$data['Organisme']['Organisme'][$korganisme] = $organisme;
//				}
//			}
//
//			if (!empty($this->request->data['Service']['Service'])) {
//				$data['Service'] = array();
//				foreach ($this->request->data['Service']['Service'] as $kservice => $service) {
//					$data['Service']['Service'][$kservice] = $service;
//				}
//			}
//			$data['Recherche']['user_id'] = $this->Session->read('Auth.User.id');
//			$data['Recherche']['desktop_id'] = $this->Session->read('Auth.User.Desktop.id');
//
////			$this->Recherche->begin();
////			$success = $this->Recherche->saveAll($data);
//			$metasList = $this->Recherche->Metadonnee->find(
//				'all', array(
//					'recusive' => -1,
//					'contain' => false
//				)
//			);
//
//			$valOfSelect = Configure::read('Selectvaluemetadonnee.id');
//
//			foreach ($data['Selectvaluemetadonnee']['Selectvaluemetadonnee'] as $meta_id => $selectValueMeta) {
//				$metadon = $this->Recherche->Metadonnee->find('first', array('conditions' => array('Metadonnee.id' => $meta_id), 'recursive' => -1));
//
//				if ($metadon['Metadonnee']['typemetadonnee_id'] != $valOfSelect) {
//					$metaValue = array(
//						'RechercheSelectvaluemetadonnee' => array(
//							'recherche_id' => $this->Recherche->id,
//							'selectvaluemetadonnee_id' => null,
//							'metadonnee_id' => $metadon['Metadonnee']['id'],
//							'valeur' => $selectValueMeta
//						)
//					);
//
////					$this->Recherche->RechercheSelectvaluemetadonnee->create($metaValue);
////					$success = $this->Recherche->RechercheSelectvaluemetadonnee->save() && $success;
//				} else {
//					$metaValue = array(
//						'RechercheSelectvaluemetadonnee' => array(
//							'recherche_id' => $this->Recherche->id,
//							'selectvaluemetadonnee_id' => $selectValueMeta,
//							'metadonnee_id' => $metadon['Metadonnee']['id'],
//							'valeur' => null
//						)
//					);
//
//				}
//			}

			$this->request->data = $this->Recherche->find(
				'first', array(
					'conditions' => array(
						'Recherche.id' => $this->request->data['Recherche']['RecherchesEnregistrees']
					),
					'contain' => array(
						'Desktop',
						'Metadonnee',
						'RechercheSelectvaluemetadonnee',
						'Contact',
						'Organisme',
						'Type',
						'Soustype',
						'Dossier',
						'Affaire',
						'Tache',
						'Circuit'
					)
				)
			);

			$this->request->data['Recherche']['desktop_id'] = $this->Session->read('Auth.User.Desktop.id');


		} else {
			$mode = "";
			if (isset($this->request->data['Recherche']['RecherchesEnregistrees']) && $this->request->data['Recherche']['RecherchesEnregistrees'] != null) {
				if ($this->request->data['Recherche']['RecherchesEnregistrees'] == 0) {
					$mode = "add";
				} else {
					$mode = "edit";
					if ($forcemode != null && $forcemode == 'search') {
						$mode = 'search';
						$this->set('recherches', $this->Recherche->find('list', array('conditions' => array('Recherche.user_id' => $this->Session->read('Auth.User.id')))));
					}
					$this->request->data = $this->Recherche->find(
						'first', array(
							'conditions' => array(
								'Recherche.id' => $this->request->data['Recherche']['RecherchesEnregistrees']
							),
							'contain' => array(
								'Desktop',
								'Metadonnee',
								'RechercheSelectvaluemetadonnee',
								'Contact',
								'Organisme',
								'Type',
								'Soustype',
								'Dossier',
								'Affaire',
								'Tache',
								'Circuit'
							)
						)
					);
					$metaToDisplay = array();
					foreach ($this->request->data['RechercheSelectvaluemetadonnee'] as $valueSelect) {
						$key = $valueSelect['metadonnee_id'];
						if ($valueSelect['selectvaluemetadonnee_id'] === null && $valueSelect['valeur'] === null) {
							$metaToDisplay[$key] = null;
						} else if ($valueSelect['selectvaluemetadonnee_id'] === null) {
							$metaToDisplay[$key] = $valueSelect['valeur'];
						} else {
							$metaToDisplay[$key] = $valueSelect['selectvaluemetadonnee_id'];
						}
					}
					$this->request->data['Selectvaluemetadonnee']['Selectvaluemetadonnee'] = $metaToDisplay;
				}
			} else {
				$mode = "search";
				$this->set('recherches', $this->Recherche->find('list', array('conditions' => array('Recherche.user_id' => $this->Session->read('Auth.User.id')),'order' => ['Recherche.name ASC'])));
			}
		}
		$this->set('mode', $mode);
		$this->_setOptions();
	}

	/**
	 * Récupération de la liste des recherches
	 *
	 * @logical-group Recherches
	 * @user-profile User
	 *
	 * @access public
	 * @return void
	 */
	public function getRecherches()
	{

		$this->Recherche->recursive = -1;
		$recherches = $this->Recherche->find('all', array('conditions' => array('Recherche.user_id' => $this->Session->read('Auth.User.id'))));
		for ($i = 0; $i < count($recherches); $i++) {
			$recherches[$i]['right_view'] = true;
			$recherches[$i]['right_edit'] = true;
			$recherches[$i]['right_delete'] = true;
		}
		$this->set('recherches', $recherches);
	}

	/**
	 * Suppression d'une recherche enregistrée
	 *
	 * @logical-group Recherches
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id identifiant de la recherche enregistrée
	 * @return void
	 */
	public function delete($id = null)
	{
		$this->Jsonmsg->init(__d('default', 'delete.error'));
		$this->Recherche->begin();
		if ($this->Recherche->delete($id)) {
			$this->Recherche->commit();
			$this->Jsonmsg->valid(__d('default', 'delete.ok'));
		} else {
			$this->Recherche->rollback();
		}
		$this->Jsonmsg->send();
	}

	/**
	 * Lancement de la recherche
	 *
	 * @logical-group Recherches
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id identifiant de la recherche
	 * @return void
	 */
	public function recherche($id = null)
	{
		if ($id != null) {
			$this->request->data = $this->Recherche->find('first', array('conditions' => array('Recherche.id' => $id)));
		}
//$this->Log( $this->request->data);
		if (!empty($this->request->data)) {
			foreach (array('cdate', 'cdatereception') as $criteresDatesDebut) {
				if (isset($this->request->data['Recherche'][$criteresDatesDebut]) && !empty($this->request->data['Recherche'][$criteresDatesDebut])) {
					$dateReception = $this->request->data['Recherche'][$criteresDatesDebut];
					if (strpos($dateReception, '/') != 0) {
						$debut = explode('/', $dateReception);
						$day = $debut[0];
						$month = $debut[1];
						$year = $debut[2];
						$dateDebut = $year . '-' . $month . '-' . $day;
						$this->request->data['Recherche'][$criteresDatesDebut] = $dateDebut;
					}
				}
			}
			foreach (array('cdatefin', 'cdatereceptionfin') as $criteresDatesFin) {
				if (isset($this->request->data['Recherche'][$criteresDatesFin]) && !empty($this->request->data['Recherche'][$criteresDatesFin])) {
					$dateReceptionFin = $this->request->data['Recherche'][$criteresDatesFin];
					if (strpos($dateReceptionFin, '/') != 0) {
						$fin = explode('/', $dateReceptionFin);
						$day = $fin[0];
						$month = $fin[1];
						$year = $fin[2];
						$dateFin = $year . '-' . $month . '-' . $day;
						$this->request->data['Recherche'][$criteresDatesFin] = $dateFin;
					}
				}
			}

			if (isset($this->request->data['tri'])) {
				$tri = $this->request->data['tri'];
			}
			if (!empty($this->request->data['Recherche']['contactdept'])) {
				//Pour les régions
				$dept = ClassRegistry::init('Ban')->find(
					'first', array(
						'fields' => array('Ban.name', 'Ban.id'),
						'conditions' => array(
							'Ban.id' => $this->request->data['Recherche']['contactdept']
						),
						'recursive' => -1,
						'contain' => false
					)
				);

				$this->request->data['Recherche']['contactdept'] = $dept['Ban']['id'];
				// Pour les communes
				$commune = ClassRegistry::init('Ban')->Bancommune->find(
					'first', array(
						'fields' => array('Bancommune.name', 'Bancommune.id'),
						'conditions' => array(
							'Bancommune.id' => $this->request->data['Recherche']['contactcommune']
						),
						'recursive' => -1,
						'contain' => false
					)
				);
				$this->request->data['Recherche']['contactcommune'] = $commune['Bancommune']['id'];
			}

			$secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
			$desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
			$isAdmin = false;
			$groupName = $this->Session->read('Auth.User.Desktop.Profil');
			if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
				$isAdmin = true;
			}
			if (!Configure::read('Conf.SAERP')) {
				$datasAuthorizedType = $this->Session->read('Auth.TypesAll');
				foreach ($datasAuthorizedType as $t => $type) {
					$sousTypeIds[$type['Type']['id']] = Hash::extract($type, 'Soustype.{n}.id');
					// si on est admin, on affiche les flux non qualifiés (sans type/sous-type)
					// sinon, cela dépend de la variable définie en paramétrage
					if ($isAdmin || Configure::read('Affichage.FluxSansSoustype')) {
						$sousTypeIds['-1'] = array();
					}
				}
			} else {
				$sousTypeIds = array();
			}
			//on lance la recherche
			$flux_tmp = array();
			$querydata = $this->Recherche->search($this->request->data, $id, true);
			$this->Session->write('Recherche.Querydata', $querydata);

			$mode = "";

			/****************************************************************************/
			if( isset($this->request->data['Recherche']['tosave']) && $this->request->data['Recherche']['tosave'] ) {

				$data['Recherche'] = $this->request->data['Recherche'];
				if (!empty($this->request->data['Contact']['Contact'])) {
					$data['Contact'] = array();
					foreach ($this->request->data['Contact']['Contact'] as $kcontact => $contact) {
						$data['Contact']['Contact'][$kcontact] = $contact;
					}
				} else if( !empty($this->request->data['Recherche']['RecherchesEnregistrees']) ){
					$query = "DELETE FROM recherches_contacts WHERE recherche_id = " . $this->request->data['Recherche']['RecherchesEnregistrees'] . ";";
					$this->Recherche->query($query);
				}
				if (!empty($this->request->data['Type']['Type'])) {
					foreach ($this->request->data['Type']['Type'] as $ktype => $type) {
						$data['Type']['Type'][$ktype] = $type;
					}
				} else if( !empty($this->request->data['Recherche']['RecherchesEnregistrees']) ){
					$query = "DELETE FROM recherches_types WHERE recherche_id = " . $this->request->data['Recherche']['RecherchesEnregistrees'] . ";";
					$this->Recherche->query($query);
				}
				if (!empty($this->request->data['Soustype']['Soustype'])) {
					foreach ($this->request->data['Soustype']['Soustype'] as $ksoustype => $soustype) {
						$data['Soustype']['Soustype'][$ksoustype] = $soustype;
					}
				} else if( !empty($this->request->data['Recherche']['RecherchesEnregistrees']) ){
					$query = "DELETE FROM recherches_soustypes WHERE recherche_id = " . $this->request->data['Recherche']['RecherchesEnregistrees'] . ";";
					$this->Recherche->query($query);
				}
				if (!empty($this->request->data['Circuit']['Circuit'])) {
					$data['Circuit'] = array();
					foreach ($this->request->data['Circuit']['Circuit'] as $kcircuit => $circuit) {
						$data['Circuit']['Circuit'][$kcircuit] = $circuit;
					}
				} else if( !empty($this->request->data['Recherche']['RecherchesEnregistrees']) ){
					$query = "DELETE FROM recherches_circuits WHERE recherche_id = " . $this->request->data['Recherche']['RecherchesEnregistrees'] . ";";
					$this->Recherche->query($query);
				}
				if (!empty($this->request->data['Dossier']['Dossier'])) {
					$data['Dossier'] = array();
					foreach ($this->request->data['Dossier']['Dossier'] as $kdossier => $dossier) {
						$data['Dossier']['Dossier'][$kdossier] = $dossier;
					}
				} else if( !empty($this->request->data['Recherche']['RecherchesEnregistrees']) ){
					$query = "DELETE FROM recherches_dossiers WHERE recherche_id = " . $this->request->data['Recherche']['RecherchesEnregistrees'] . ";";
					$this->Recherche->query($query);
				}
				if (!empty($this->request->data['Affaire']['Affaire'])) {
					$data['Affaire'] = array();
					foreach ($this->request->data['Affaire']['Affaire'] as $kaffaire => $affaire) {
						$data['Affaire']['Affaire'][$kaffaire] = $affaire;
					}
				} else if( !empty($this->request->data['Recherche']['RecherchesEnregistrees']) ){
					$query = "DELETE FROM recherches_affaires WHERE recherche_id = " . $this->request->data['Recherche']['RecherchesEnregistrees'] . ";";
					$this->Recherche->query($query);
				}
				if (!empty($this->request->data['Tache']['Tache'])) {
					$data['Tache'] = array();
					foreach ($this->request->data['Tache']['Tache'] as $ktache => $tache) {
						$data['Tache']['Tache'][$ktache] = $tache;
					}
				} else if( !empty($this->request->data['Recherche']['RecherchesEnregistrees']) ){
					$query = "DELETE FROM recherches_taches WHERE recherche_id = " . $this->request->data['Recherche']['RecherchesEnregistrees'] . ";";
					$this->Recherche->query($query);
				}
				// Métadonnées
				if (!empty($this->request->data['Metadonnee']['Metadonnee'])) {
					foreach ($this->request->data['Metadonnee']['Metadonnee'] as $kmetadonnee => $metadonnee) {
						$data['Metadonnee']['Metadonnee'][$kmetadonnee] = $metadonnee;
					}
				} else if( !empty($this->request->data['Recherche']['RecherchesEnregistrees']) ){
					$query = "DELETE FROM recherches_metadonnees WHERE recherche_id = " . $this->request->data['Recherche']['RecherchesEnregistrees'] . ";";
					$this->Recherche->query($query);
				}
				// Valeur par Métadonnées
				if (!empty($this->request->data['Selectvaluemetadonnee']['Selectvaluemetadonnee'])) {
					foreach ($this->request->data['Selectvaluemetadonnee']['Selectvaluemetadonnee'] as $typemetadonneeid => $valueMetadonnee) {
						$data['Selectvaluemetadonnee']['Selectvaluemetadonnee'][$typemetadonneeid] = $valueMetadonnee;
					}
				}

				if (!empty($this->request->data['Desktop']['Desktop'])) {
					foreach ($this->request->data['Desktop']['Desktop'] as $kdesktop => $desktop) {
						$data['Desktop']['Desktop'][$kdesktop] = $desktop;
					}
				}

				if (!empty($this->request->data['Organisme']['Organisme'])) {
					$data['Organisme'] = array();
					foreach ($this->request->data['Organisme']['Organisme'] as $korganisme => $organisme) {
						$data['Organisme']['Organisme'][$korganisme] = $organisme;
					}
				}

				if (!empty($this->request->data['Service']['Service'])) {
					$data['Service']['Service']  = $this->request->data['Service']['Service'];
				}
				$data['Recherche']['name'] = $this->request->data['Recherche']['name'];
				$data['Recherche']['user_id'] = $this->Session->read('Auth.User.id');
				$data['Recherche']['desktop_id'] = $this->Session->read('Auth.User.Desktop.id');


//$this->log( $data );


				// On modifie la recherche
				if( !empty($this->request->data['Recherche']['RecherchesEnregistrees']) ) {
					$storedSearch = $this->Recherche->find(
						'first', array(
							'conditions' => array(
								'Recherche.id' => $this->request->data['Recherche']['RecherchesEnregistrees'],
								'Recherche.name' => $this->request->data['Recherche']['name']
							),
							'contain' => false,
							'recursive' => -1
						)
					);

					if( !empty($storedSearch) ) {
						$data['Recherche']['id'] = $this->request->data['Recherche']['RecherchesEnregistrees'];
					}
				}
//$this->Log( $data );
				$this->Recherche->saveAll($data);

				$metasList = $this->Recherche->Metadonnee->find(
					'all', array(
						'recusive' => -1,
						'contain' => false
					)
				);
				$valOfSelect = Configure::read('Selectvaluemetadonnee.id');
				if( !empty($data['Selectvaluemetadonnee']['Selectvaluemetadonnee']) ) {
					foreach ($data['Selectvaluemetadonnee']['Selectvaluemetadonnee'] as $meta_id => $selectValueMeta) {
						if (!empty($selectValueMeta)) {
							$metadon = $this->Recherche->Metadonnee->find('first', array('conditions' => array('Metadonnee.id' => $meta_id), 'recursive' => -1));

							if ($metadon['Metadonnee']['typemetadonnee_id'] != $valOfSelect) {
								$metaValue = array(
									'RechercheSelectvaluemetadonnee' => array(
										'recherche_id' => $this->Recherche->id,
										'selectvaluemetadonnee_id' => null,
										'metadonnee_id' => $metadon['Metadonnee']['id'],
										'valeur' => $selectValueMeta
									)
								);

								if( !empty($this->request->data['Recherche']['RecherchesEnregistrees']) ) {
									$alreadyStoredSelectvaluemetadonneeSearch = $this->Recherche->RechercheSelectvaluemetadonnee->find(
										'first', array(
											'conditions' => array(
												'RechercheSelectvaluemetadonnee.recherche_id' => $this->request->data['Recherche']['RecherchesEnregistrees'],
												'RechercheSelectvaluemetadonnee.selectvaluemetadonnee_id' => null,
												'RechercheSelectvaluemetadonnee.metadonnee_id' => $metadon['Metadonnee']['id'],
												'RechercheSelectvaluemetadonnee.valeur IS NOT NULL'
											),
											'contain' => false,
											'recursive' => -1
										)
									);
									if (!empty($alreadyStoredSelectvaluemetadonneeSearch)) {
										$metaValue['RechercheSelectvaluemetadonnee']['id'] = $alreadyStoredSelectvaluemetadonneeSearch['RechercheSelectvaluemetadonnee']['id'];
									} else {
										$this->Recherche->RechercheSelectvaluemetadonnee->create($metaValue);
									}
								}
								$this->Recherche->RechercheSelectvaluemetadonnee->save($metaValue);

							} else {
								$metaValue = array(
									'RechercheSelectvaluemetadonnee' => array(
										'recherche_id' => $this->Recherche->id,
										'selectvaluemetadonnee_id IS NOT NULL',
										'metadonnee_id' => $metadon['Metadonnee']['id'],
										'valeur' => null
									)
								);

								$alreadyStoredSelectvaluemetadonneeSearch = $this->Recherche->RechercheSelectvaluemetadonnee->find(
									'first', array(
										'conditions' => array(
											'RechercheSelectvaluemetadonnee.recherche_id' => $this->request->data['Recherche']['RecherchesEnregistrees'],
											'RechercheSelectvaluemetadonnee.selectvaluemetadonnee_id' => $selectValueMeta,
											'RechercheSelectvaluemetadonnee.metadonnee_id' => $metadon['Metadonnee']['id'],
											'RechercheSelectvaluemetadonnee.valeur' => null
										),
										'contain' => false,
										'recursive' => -1
									)
								);
								if( !empty($alreadyStoredSelectvaluemetadonneeSearch) ) {
									$metaValue['RechercheSelectvaluemetadonnee']['id'] = $alreadyStoredSelectvaluemetadonneeSearch['RechercheSelectvaluemetadonnee']['id'];
								}
								else {
									$this->Recherche->RechercheSelectvaluemetadonnee->create($metaValue);
								}
								$this->Recherche->RechercheSelectvaluemetadonnee->save($metaValue);
							}
						}
					}
				}
			}
			else if( !empty( $this->request->data['Recherche']['RecherchesEnregistrees']) ){
				$mode = "edit";

				$this->request->data = $this->Recherche->find(
					'first', array(
						'conditions' => array(
							'Recherche.id' => $this->request->data['Recherche']['RecherchesEnregistrees']
						),
						'contain' => array(
							'Desktop',
							'Metadonnee',
							'RechercheSelectvaluemetadonnee',
							'Contact',
							'Organisme',
							'Type',
							'Soustype',
							'Dossier',
							'Affaire',
							'Tache',
							'Circuit'
						)
					)
				);

				$querydata = $this->Recherche->search($this->request->data, null, true);
			}
			/****************************************************************************/
//$this->log( $this->request->data );
			$this->set('mode', $mode);
			// On prépare les conditions en fonction des droits
			$flux = array();
			$soustypes = $this->Recherche->Soustype->find('list', array('contain' => false));

			$allowedSoustypes = array();
			if (Configure::read('Use.AuthRights') && !Configure::read('Conf.SAERP')) {
				$allowedSoustypes = $this->Session->read('Auth.Soustypes');
			} else {
				$allowedSoustypes = $soustypes;
			}

			// Si aucune condition, on ne retourne rien
			if (!empty($querydata)) {
				$querydata['limit'] = 10;
				if (isset($tri) && !empty($tri)) {
					$querydata['order'] = $tri;
				} else {
					$querydata['order'] = 'Courrier.datereception DESC';
				}

				$this->paginate = array('Courrier' => $querydata);

				$flux_tmp = $this->paginate('Courrier');
			}

			foreach ($flux_tmp as $kflx => $flx) {
				$flx['Bancontenu']['etat'] = $this->Courrier->getLastEtat($flx['Courrier']['id']);
				$flx['Bancontenu']['bannette_id'] = $this->Courrier->getLastBannette($flx['Courrier']['id']);
				$cycle = ClassRegistry::init('Courrier')->getCycle($flx['Courrier']['id'], false);
				$profils = array();
				foreach ($cycle['bannettes'] as $key => $bannette) {
					if (isset($bannette['Bancontenu']['gras']) && $bannette['Bancontenu']['gras']) {
						$profils[] = $bannette['Desktop']['name'];
					}
				}
				$flx['Courrier']['objet'] = replace_accents($flx['Courrier']['objet']); // Corrige le souci pour CA Albi
				if (strpos($flx['Courrier']['objet'], '<!DOCTYPE') !== false || strpos($flx['Courrier']['objet'], '<html') !== false) {
					$flx['Courrier']['objet'] = null;
				}
				if( is_JSON($flx['Courrier']['objet'] ) ) {
					$flx['Courrier']['objet'] = json_decode($flx['Courrier']['objet'], true);
					$objetData = array();
					foreach ($flx['Courrier']['objet'] as $key => $val) {
						if( is_array($val) ) {
							foreach( $val as $fields => $values ) {
								$data[] = "\n".$fields.': '.$values;
							}
							$objetData[] = $key . ' : ' . implode( ' ', $data );
						}
						else {
							$objetData[] = $key . ' : ' . $val;
						}
					}
					$flx['Courrier']['objet'] = implode("\n", $objetData);
				}
				$flx['parentInfo'] = $this->_dataParent($flx['Courrier']['parent_id']);
				$flx['retard'] = !$this->Courrier->isInTime($flx['Courrier']['id']);
				$flx['delairetard'] = $this->Courrier->getRetardDelaiNbOfDays($flx['Courrier']['id']);
				$flx['agenttraitant'] = $profils;
				$flx['right_read'] = true;
				$flx['right_update'] = true;
				$flux[] = $flx;
			}

			if (Configure::read('CD') == 81) {
				$this->set(
					'etat',
					array(
						'-1' => 'Refusé',
						'0' => 'Traité à clore',
						'1' => 'En cours de traitement',
						'2' => 'Clos'
					)
				);
			} else {
				$this->set(
					'etat',
					array(
						'-1' => 'Refusé',
						'0' => 'Validé',
						'1' => 'En cours de traitement',
						'2' => 'Clos'
					)
				);
			}

			$this->set('flux', $flux);
			if (isset($tri) && $tri != '') {
				list($colone, $order) = explode(' ', $tri);

				switch ($colone) {
					case 'Courrier.reference':
						$ancien_name = 'reference';
						break;
					case 'Courrier.name':
						$ancien_name = 'nom';
						break;
					case 'Courrier.objet':
						$ancien_name = 'objet';
						break;
					case 'Organisme.name':
						$ancien_name = 'expediteur';
						break;
					case 'Courrier.datereception':
						$ancien_name = 'datereception';
						break;
					case 'Courrier.mail_retard_envoye':
						$ancien_name = 'retard';
						break;
					default :
						$ancien_name = '';
						break;
				}
				$this->set('ancien_order', $order);
				$this->set('ancien_name', $ancien_name);
			}
		}
	}

	/**
	 * Export de la recherche au format CSV
	 *
	 * @logical-group Recherches
	 * @user-profile User
	 *
	 * @access public
	 * @return void
	 */
	public function exportcsv()
	{

//		$this->Progress->start(1);

		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '2048M');

		$datasAuthorizedType = $this->Session->read('Auth.TypesAll');
		foreach ($datasAuthorizedType as $type) {
			$sousTypeIds[$type['Type']['id']] = Hash::extract($type, 'Soustype.{n}.id');
		}

		$querydata = $this->Session->read('Recherche.Querydata');
		unset($querydata['limit']);
		$querydata['fields'][] = $this->Courrier->getVfMetaValues('metadonnees', Configure::read('Selectvaluemetadonnee.id'));
		$valuesMetaSelected = $this->Courrier->Metadonnee->Selectvaluemetadonnee->find('list');
		$this->set('valuesMetaSelected', $valuesMetaSelected);

		// Liste de smétadonnéees
		$metasName = $this->Courrier->Metadonnee->find('list');
		$this->set('metasName', $metasName);
		foreach ($metasName as $key => $name) {
			$querydata['fields'][] = $this->Courrier->getVfMeta($name, $key);
		}

		$querydata['order'] = 'Courrier.datereception ASC';

		$querydata['fields'][] = $this->Courrier->getVfAgents('agentstraitants', '//');
		$querydata['fields'][] = $this->Courrier->getDaterefus('daterefus');
		$querydata['fields'][] = $this->Courrier->getDatevalidation('datevalidation');
		$querydata['fields'][] = $this->Courrier->getDatecloture('datecloture');


		if (Configure::read('CD') == 81) {
			$content = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
                "http://www.w3.org/TR/html4/loose.dtd">
                <html>
                <head>
                    <title>Recherche</title>
                    <style type="text/css">
                        @page {
                            size: landscape;
                        }
                        .table {
                            width: 100%;
                            margin-bottom: 0!important;
                            border-bottom: 1px solid #ddd;
                            border-collapse: collapse!important;
                            border-radius: 1px;
                            font-size: 11px;
                        }
                        .table tr.entete td {
                            line-height: 1.42857143
                            vertical-align: top;
                            border-top: 1px solid #000;
                            border-bottom: 1px solid #000;
                            border-left: 1px solid #000;
                            padding: 8px!important;
                            font-weight: bold;
                        }
                        .table tr.corps td {
                            line-height: 1.42857143
                            vertical-align: top;
                            border-top: 1px solid #000;
                            border-bottom: 1px solid #000;
                            border-left: 1px solid #000;
                            padding: 8px!important;
                        }
                    </style>
                </head>
                <body>';

			// Change the $content according to your requrement
			$content .= '<table class="table">';
//
			$content .= '<tr class="entete">
                <td>ID</td>
                <td>Statut</td>
                <td>Retard</td>
                <td>Référence</td>
                <td>Nom</td>
                <td>Objet</td>
                <td>Date entrée dans la collectivité</td>
                <td>Date de réception</td>
                <td>Direction concernée</td>
                <td>Service concerné</td>
                <td>Organisme</td>
                <td>Contact</td>
                <td>Type</td>
                <td>Sous-type</td>
                <td>Dossier</td>
                <td>Affaire</td>
                <td>Nom de la tâche associée</td>';

			foreach ($metasName as $id => $nameMeta) {
				$content .= '<td>' . $nameMeta . '</td>';
			}
			$content .= '</tr>';
		}

		$flux = $this->Courrier->find('all', $querydata);
		$count = count($flux);
//		$this->Progress->start($count);
		foreach ($flux as $i => $flx) {
			$flux[$i]['Bancontenu']['etat'] = $this->Courrier->getLastEtat($flx['Courrier']['id']);

			$objet = $flx['Courrier']['objet'];
			if (strpos($objet, '<!DOCTYPE') !== false || strpos($objet, '<html') !== false || strpos($objet, '--_000') !== false) {
				$objet = strip_tags($objet, "<style>");
				$substring = substr($objet, strpos($objet, "<style"), strpos($objet, "</style>") + 2);
				$objet = str_replace($substring, "", $objet);
				$objet = trim($objet);
				$objet = str_replace('&nbsp;', '', $objet);
				$flux[$i]['Courrier']['objet'] = $objet;
			}

			// Valeurs et noms des métadonnées trouvées
			foreach ($metasName as $key => $name) {
				if (!(strpos(strtolower($flux[$i]['Courrier'][$name]), 'oui') !== false || strpos(strtolower($flux[$i]['Courrier'][$name]), 'non') !== false || strlen($flux[$i]['Courrier'][$name]) >= 6)) {
					$flux[$i]['Courrier'][$name] = @$valuesMetaSelected[$flux[$i]['Courrier'][$name]];
				}
			}
			$flux[$i]['Courrier']['affairesuiviepar'] = '';
			if( !empty($flx['Courrier']['affairesuiviepar_id']) ) {
				$flux[$i]['Courrier']['affairesuiviepar'] = $flx['User']['prenom'] . ' ' . $flx['User']['nom'];
			}

			if (Configure::read('CD') == 81) {
				////// pour e cD81 ///////////////
				$etat = array(
					'-1' => 'Refusé',
					'0' => 'Traité à clore',
					'1' => 'En cours de traitement',
					'2' => 'Clos'
				);
				$services = $this->Courrier->Service->find('list');
				$datasPassed = Hash::expand($this->request->params['named'], '__');
				if (!empty($datasPassed['Recherche']['cretard'])) {
					$retard = $datasPassed['Recherche']['cretard'];
				} else {
					$retard = (Hash::get($flx, 'Courrier.mail_retard_envoye') ? 'Oui' : 'Non');
				}
				$statut = Hash::get($etat, Hash::get($flx, 'Bancontenu.etat'));
				$objet = $flx['Courrier']['objet'];
				if (strpos($objet, '<!DOCTYPE') !== false || strpos($objet, '<html>') !== false || strpos($objet, '--_000') !== false) {
					$objet = strip_tags($objet, "<html>");
					$substring = substr($objet, strpos($objet, "<style"), strpos($objet, "</style>"));
					$objet = str_replace($substring, "", $objet);
					$objet = trim($objet);
				}
				$content .= '<tr class="corps">
                    <td>' . $flx['Courrier']['id'] . '</td>
                    <td>' . $statut . '</td>
                    <td>' . $retard . '</td>
                    <td>' . $flx['Courrier']['reference'] . '</td>
                    <td>' . $flx['Courrier']['name'] . '</td>
                    <td>' . $objet . '</td>
                    <td>' . strftime('%d/%m/%Y', strtotime(Hash::get($flx, 'Courrier.date'))) . '</td>
                    <td>' . strftime('%d/%m/%Y', strtotime(Hash::get($flx, 'Courrier.datereception'))) . '</td>
                    <td>' . Hash::get($flx, 'Service.name') . '</td>
                    <td>' . Hash::get($services, Hash::get($flx, 'Service.parent_id')) . '</td>
                    <td>' . Hash::get($flx, 'Organisme.name') . '</td>
                    <td>' . Hash::get($flx, 'Contact.name') . '</td>
                    <td>' . Hash::get($flx, 'Type.name') . '</td>
                    <td>' . Hash::get($flx, 'Soustype.name') . '</td>
                    <td>' . Hash::get($flx, 'Dossier.name') . '</td>
                    <td>' . Hash::get($flx, 'Affaire.name') . '</td>
                    <td>' . Hash::get($flx, 'Tache.name') . '</td>';

				$valueSelected = '';
				foreach ($metasName as $key => $name) {
					if (!(strpos(strtolower($flx['Courrier'][$name]), 'oui') !== false || strpos(strtolower($flx['Courrier'][$name]), 'non') !== false || strlen($flx['Courrier'][$name]) >= 6)) {
						$valueSelected = @$valuesMetaSelected[$flx['Courrier'][$name]];
					} else {
						$valueSelected = $flx['Courrier'][$name];
					}
					$content .= '<td>' . $valueSelected . '</td>';
				}
				$content .= '</tr>';
			}

		}
		if (Configure::read('CD') == 81) {
			$content .= '</table>';
			$content .= '</body></html>';
			//            echo $content;

			$generatedFile = file_put_contents(TMP . DS . 'resultats_recherche_' . date('Ymd-His') . '.html', utf8_decode($content));
			$document_content = file_get_contents(TMP . DS . 'resultats_recherche_' . date('Ymd-His') . '.html');


			Configure::write('debug', 0);
			header("Content-type: text/html");
			header("Content-Disposition: attachment; filename=\"resultats_recherche_" . date('Ymd-His') . ".html\""); // use 'attachment' to force a file download

			echo $document_content;

			$pathToUnlink = TMP . DS . "resultats_recherche_" . date('Ymd-His') . ".html";
			if (file_exists($pathToUnlink)) {
				unlink($pathToUnlink);
			}
			exit();
		} else {
			$this->layout = '';
		}

//		$this->Progress->stop();


//		$ended = false;
//		foreach ($this->Session->read('Progress.Tokens') as $token) {
//			$this->log($token);
//			if ($token['progress'] == 100) {
//				$ended = true;
//			}
//		}
//		$this->set('ended', $ended);

		$this->set(
			'etat',
			array(
				'-1' => 'Refusé',
				'0' => 'Validé',
				'1' => 'En cours de traitement',
				'2' => 'Clos'
			)
		);
		$this->set('services', $this->Session->read('Auth.Services'));
		$this->set(compact('flux'));
	}

	public function charge_soustypes()
	{
		$resultats = array();
		foreach ($this->request->data['TypeChoix'] as $key => $value) {
			$soustypes = $this->Recherche->Soustype->find('list', array('fields' => array('Soustype.id', 'Soustype.name'), 'conditions' => array('Soustype.type_id' => $value)));
			foreach ($soustypes as $key => $value) {
				$resultats[$key] = $value;
			}
		}
		$this->set('resultats', $resultats);
	}

	/**
	 * Export des données pour la SAERP
	 *
	 * @logical-group Recherches
	 * @user-profile User
	 *
	 * @access public
	 * @return void
	 */
	public function export()
	{

		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '2048M');
		$this->set('ariane', array(
			'<a href="/environnement/userenv/0">' . __d('menu', 'Recherche') . '</a>',
			__d('menu', 'export', true)
		));
		if (!empty($this->request->data)) {
			if (Configure::read('Conf.SAERP')) {
				if (!empty($this->request->data['Recherche']['cdatefilter'])) {
					$dateSaisie = $this->request->data['Recherche']['cdatefilter'];
					$debut = explode('/', $dateSaisie);
					$day = $debut[0];
					$month = $debut[1];
					$year = $debut[2];
					$dateDebut = $year . '-' . $month . '-' . $day;
					$this->request->data['Recherche']['cdatefilter'] = $dateDebut;
				}
				if (!empty($this->request->data['Recherche']['cdatefilterfin'])) {
					$dateSaisie = $this->request->data['Recherche']['cdatefilterfin'];
					$fin = explode('/', $dateSaisie);
					$day = $fin[0];
					$month = $fin[1];
					$year = $fin[2];
					$dateFin = $year . '-' . $month . '-' . $day;
					$this->request->data['Recherche']['cdatefilterfin'] = $dateFin;
				}
			}

			if ($this->request->data['Recherche']['ctypedocument'] == 'PLI') {
				$queryDataPli = $this->Consultation->search($this->request->data);
				$queryDataPli['order'] = 'Operation.name ASC';
				$queryDataPli['group'] = array('Operation.name', 'Consultation.name', 'Pliconsultatif.id', 'Origineflux.id', 'Courrier.id', 'Consultation.id', 'Operation.id', 'CourrierMetadonnee.id', 'Metadonnee.id', 'Organisme.id', 'Contact.id');
				$queryDataPli['limit'] = 1000;
				$queryDataPli['maxLimit'] = 10000;
				$this->paginate = $queryDataPli;
				$flux_tmp = $this->paginate($this->Consultation);
				$flux = array();
				foreach ($flux_tmp as $key => $flx) {
					$flux[$flx['Consultation']['id']] = $flx;
				}
			} else if ($this->request->data['Recherche']['ctypedocument'] == 'OS') {
				$queryDataOS = $this->Ordreservice->search($this->request->data);
				$queryDataOS['order'] = array('Operation.name ASC', 'Marche.numero ASC');
				$queryDataOS['group'] = array('Operation.name', 'Marche.numero', 'Ordreservice.id', 'Courrier.id', 'Marche.id', 'Operation.id', 'CourrierMetadonnee.id', 'Metadonnee.id', 'Organisme.id', 'Contact.id');
				$queryDataOS['limit'] = 20;
				$queryDataOS['maxLimit'] = 10000;
				$this->paginate = $queryDataOS;
				$flux = $this->paginate($this->Ordreservice);
			} else {
				$querydata = $this->Recherche->search($this->request->data, null, array());
				$querydata['order'] = 'Operation.name ASC';
				$this->paginate = array('Courrier' => $querydata);
				$flux = $this->paginate('Courrier');
			}
//debug($flux);
			foreach ($flux as $i => $flx) {

				$associations = $this->Courrier->Soustype->Type->Operation->Intituleagentbyoperation->find(
					'all', array(
						'conditions' => array(
							'Intituleagentbyoperation.operation_id' => $flx['Operation']['id']
						),
						'contain' => array(
							'Desktopmanager' => array(
								'Desktop' => array(
									'User',
									'SecondaryUser'
								)
							),
							'Intituleagent' => array(
								'conditions' => array(
									'Intituleagent.name' => array('RO', 'AO', 'GEST')
								),
								'order' => array('Intituleagent.name ASC')
							)
						)
					)
				);

				if (!empty($associations[0]['Desktopmanager']['Desktop'][0]['User'])) {
					$flux[$i]['Operation']['ao'] = @$associations[0]['Desktopmanager']['Desktop'][0]['User'][0]['prenom'] . ' ' . @$associations[0]['Desktopmanager']['Desktop'][0]['User'][0]['nom'];
					$flux[$i]['Operation']['gest'] = @$associations[1]['Desktopmanager']['Desktop'][0]['User'][0]['prenom'] . ' ' . @$associations[1]['Desktopmanager']['Desktop'][0]['User'][0]['nom'];
					$flux[$i]['Operation']['ro'] = @$associations[2]['Desktopmanager']['Desktop'][0]['User'][0]['prenom'] . ' ' . @$associations[2]['Desktopmanager']['Desktop'][0]['User'][0]['nom'];
				} else {
					$flux[$i]['Operation']['ao'] = @$associations[0]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom'] . ' ' . @$associations[0]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['nom'];
					$flux[$i]['Operation']['gest'] = @$associations[1]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom'] . ' ' . @$associations[1]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['nom'];
					$flux[$i]['Operation']['ro'] = @$associations[2]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom'] . ' ' . @$associations[2]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['nom'];
				}
				if (isset($flx['Metadonnee']['name']) && $flx['Metadonnee']['id'] == Configure::read('MetadonnneMontant.id')) {
					$flux[$i]['Lettre']['montant'] = $flx['CourrierMetadonnee']['valeur'];
				}
				$flx['Courrier']['retard'] = !$this->Courrier->isInTime($flx['Courrier']['id']);

				$flux[$i]['Courrier'] = $flx['Courrier'];
				$flux[$i]['right_read'] = true;
				$flux[$i]['right_view'] = true;
			}

			$countFlux = count($flux);
			$this->set(compact('flux', 'countFlux'));
		}

		if (!empty($this->request->data['Recherche']['cdatefilter'])) {
			$dateSaisie = $this->request->data['Recherche']['cdatefilter'];
			$debut = explode('-', $dateSaisie);
			$day = $debut[0];
			$month = $debut[1];
			$year = $debut[2];
			$dateDebut = $year . '/' . $month . '/' . $day;
			$this->request->data['Recherche']['cdatefilter'] = $dateDebut;
		}
		if (!empty($this->request->data['Recherche']['cdatefilterfin'])) {
			$dateSaisie = $this->request->data['Recherche']['cdatefilterfin'];
			$fin = explode('-', $dateSaisie);
			$day = $fin[0];
			$month = $fin[1];
			$year = $fin[2];
			$dateFin = $year . '/' . $month . '/' . $day;
			$this->request->data['Recherche']['cdatefilterfin'] = $dateFin;
		}

		$this->_setOptions();
	}

	/**
	 * Export des LN pour la SAERP
	 *
	 * @logical-group Recherches
	 * @user-profile User
	 *
	 * @access public
	 * @return void
	 */
	public function exportLettre()
	{

		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '2048M');
		$this->set('ariane', array(
			'<a href="/environnement/userenv/0">' . __d('menu', 'Recherche') . '</a>',
			__d('menu', 'export', true)
		));

		if (isset($this->request->params['named']['Recherche__ctypedocument']) && $this->request->params['named']['Recherche__ctypedocument'] == 'OS') {
			$querydata = $this->Ordreservice->search(Hash::expand($this->request->params['named'], '__'));
			unset($querydata['limit']);
			$querydata['order'] = array('Operation.name ASC', 'Marche.name ASC');
			$flux = $this->Ordreservice->find('all', $querydata);
		} else if (isset($this->request->params['named']['Recherche__ctypedocument']) && $this->request->params['named']['Recherche__ctypedocument'] == 'PLI') {
//            $querydata = $this->Pliconsultatif->search(Hash::expand($this->request->params['named'], '__'));
			$querydata = $this->Consultation->search(Hash::expand($this->request->params['named'], '__'));
			unset($querydata['limit']);
			$querydata['order'] = 'Operation.name ASC';
			$flux_tmp = $this->Consultation->find('all', $querydata);
			$flux = array();
			foreach ($flux_tmp as $key => $flx) {
				$flux[$flx['Consultation']['id']] = $flx;
			}

		} else {
			$querydata = $this->Recherche->search(Hash::expand($this->request->params['named'], '__'), null, array());
			unset($querydata['limit']);
			$querydata['order'] = 'Operation.name ASC';
			$flux = $this->Courrier->find('all', $querydata);
		}
		foreach ($flux as $i => $flx) {
			// Données méta-données
			$courriers_metadonnees = $this->Courrier->CourrierMetadonnee->find(
				'all',
				array(
					'conditions' => array(
						'CourrierMetadonnee.courrier_id' => $flx['Courrier']['id']
					),
					'contain' => array(
						'Metadonnee' => array(
							'order' => array(
								'Metadonnee.name ASC'
							),
							'Selectvaluemetadonnee'
						)
					)
				)
			);
			$metas = array();
			foreach ($courriers_metadonnees as $c => $valeurs) {
				foreach ($valeurs['Metadonnee']['Selectvaluemetadonnee'] as $s => $valeurChoisi) {
					$selected[$valeurChoisi['id']] = $valeurChoisi['name'];
				}
				if ($valeurs['Metadonnee']['typemetadonnee_id'] == 4) {
					$metas[$c] = $valeurs['Metadonnee']['name'] . " : " . $selected[$valeurs['CourrierMetadonnee']['valeur']];
				} else {
					$metas[$c] = $valeurs['Metadonnee']['name'] . " : " . $valeurs['CourrierMetadonnee']['valeur'];
				}
			}
			$flux[$i]['Metadonnee']['valeur'] = implode(' // ', $metas);


			$associations = $this->Courrier->Soustype->Type->Operation->Intituleagentbyoperation->find(
				'all', array(
					'conditions' => array(
						'Intituleagentbyoperation.operation_id' => $flx['Operation']['id']
					),
					'contain' => array(
						'Desktopmanager' => array(
							'Desktop' => array(
								'User',
								'SecondaryUser'
							)
						),
						'Intituleagent' => array(
							'conditions' => array(
								'Intituleagent.name' => array('RO', 'RO m', 'AO', 'GEST')
							),
							'order' => array('Intituleagent.name ASC')
						)
					)
				)
			);
			if (!empty($associations[0]['Desktopmanager']['Desktop'][0]['User'])) {
				$flux[$i]['Operation']['ao'] = isset($associations[0]['Desktopmanager']['Desktop'][0]['User'][0]['prenom']) ? ($associations[0]['Desktopmanager']['Desktop'][0]['User'][0]['prenom'] . ' ' . @$associations[0]['Desktopmanager']['Desktop'][0]['User'][0]['nom']) : ($associations[0]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom'] . ' ' . @$associations[0]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['nom']);
				$flux[$i]['Operation']['gest'] = isset($associations[1]['Desktopmanager']['Desktop'][0]['User'][0]['prenom']) ? ($associations[1]['Desktopmanager']['Desktop'][0]['User'][0]['prenom'] . ' ' . @$associations[1]['Desktopmanager']['Desktop'][0]['User'][0]['nom']) : ($associations[1]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom'] . ' ' . @$associations[1]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['nom']);
				$flux[$i]['Operation']['ro'] = isset($associations[2]['Desktopmanager']['Desktop'][0]['User'][0]['prenom']) ? ($associations[2]['Desktopmanager']['Desktop'][0]['User'][0]['prenom'] . ' ' . @$associations[2]['Desktopmanager']['Desktop'][0]['User'][0]['nom']) : ($associations[2]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom'] . ' ' . @$associations[2]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['nom']);
			} else {
				$flux[$i]['Operation']['ao'] = isset($associations[0]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom']) ? ($associations[0]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom'] . ' ' . @$associations[0]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['nom']) : ($associations[0]['Desktopmanager']['Desktop'][0]['User'][0]['prenom'] . ' ' . @$associations[0]['Desktopmanager']['Desktop'][0]['User'][0]['nom']);
				$flux[$i]['Operation']['gest'] = isset($associations[1]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom']) ? ($associations[1]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom'] . ' ' . @$associations[1]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['nom']) : ($associations[1]['Desktopmanager']['Desktop'][0]['User'][0]['prenom'] . ' ' . @$associations[1]['Desktopmanager']['Desktop'][0]['User'][0]['nom']);
				$flux[$i]['Operation']['ro'] = isset($associations[2]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom']) ? ($associations[2]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['prenom'] . ' ' . @$associations[2]['Desktopmanager']['Desktop'][0]['SecondaryUser'][0]['nom']) : ($associations[2]['Desktopmanager']['Desktop'][0]['User'][0]['prenom'] . ' ' . @$associations[2]['Desktopmanager']['Desktop'][0]['User'][0]['nom']);
			}
			if (isset($flx['Metadonnee']['name']) && $flx['Metadonnee']['id'] == Configure::read('MetadonnneMontant.id')) {
				$flux[$i]['Lettre']['montant'] = $flx['CourrierMetadonnee']['valeur'];
			} else {
				$flux[$i]['Lettre']['montant'] = '';
			}
			$flx['Courrier']['retard'] = !$this->Courrier->isInTime($flx['Courrier']['id']);

			// destinataire du flux
			$civilite = $this->Session->read('Auth.Civilite');

			$flx['Destinataire']['name'] = @$flx['Organisme']['name'] . ' / ' . @$civilite[$flx['Contact']['civilite']] . ' ' . @$flx['Contact']['name'];

			$flux[$i]['Destinataire']['name'] = $flx['Destinataire']['name'];
			$flux[$i]['Destinataire']['siret'] = $flx['Organisme']['siret'];
			$flux[$i]['Destinataire']['numvoie'] = $flx['Contact']['numvoie'];
			$flux[$i]['Destinataire']['nomvoie'] = $flx['Contact']['nomvoie'];
			$flux[$i]['Destinataire']['compl'] = $flx['Contact']['compl'];
			$flux[$i]['Destinataire']['cp'] = $flx['Contact']['cp'];
			$flux[$i]['Destinataire']['ville'] = $flx['Contact']['ville'];

			$flux[$i]['Courrier'] = $flx['Courrier'];
		}


		$this->set(compact('flux'));
		$this->layout = '';

		$this->_setOptions();
	}


	/**
	 *  Fonction permettant de récupérer les données parentes de flux réponses
	 *
	 */
	protected function _dataParent($courrierParent_id)
	{
		$this->Bancontenu = ClassRegistry::init('Bancontenu');
		$courrierParent = $this->Bancontenu->Courrier->find(
			'first', array(
				'fields' => array_merge(
					$this->Bancontenu->Courrier->fields(), $this->Bancontenu->Courrier->Organisme->fields(), $this->Bancontenu->Courrier->Contact->fields(), $this->Bancontenu->Courrier->Soustype->fields(), $this->Bancontenu->Courrier->Soustype->Type->fields(), $this->Bancontenu->Courrier->Desktop->fields()
				),
				'conditions' => array(
					'Courrier.id' => $courrierParent_id
				),
				'joins' => array(
					$this->Bancontenu->Courrier->join('Organisme'),
					$this->Bancontenu->Courrier->join('Contact'),
					$this->Bancontenu->Courrier->join('Soustype'),
					$this->Bancontenu->Courrier->Soustype->join('Type'),
					$this->Bancontenu->Courrier->join('Desktop')
				),
				'recursive' => -1
			)
		);
		return $courrierParent;

	}

	/**
	 * Fonction permettant de calculer le temps d'exécution d'un processus
	 * @use: pour l'exportCSV
	 */
	public function long_process()
	{
		$this->autoRender = false;
		for($i=1;$i<=10;$i++){
			session_start();
			$_SESSION["progress"]=$i;
			session_write_close();
			sleep(1);
		}
	}

	/**
	 * Fonction permettant de retourner si le processus lancé est fini ou non
	 * @use: pour l'exportCSV
	 */
	public function progress()
	{
		$this->autoRender = false;
		session_start();
		echo $_SESSION["progress"];
	}



	/**
	 * Fonction remontant les informations des recherches enregistrées
	 * @param type $field
	 * @return type
	 *
	 */
	public function ajaxSavedSearch($id) {
		$this->autoRender = false;

		if (!empty($this->request->data['Recherche']['RecherchesEnregistrees'])) {
			return json_encode( $this->Recherche->find(
				'first', array(
					'conditions' => array(
						'Recherche.id' => $this->request->data['Recherche']['RecherchesEnregistrees']
					),
					'contain' => array(
						'Desktop',
						'Metadonnee',
						'RechercheSelectvaluemetadonnee',
						'Contact',
						'Organisme',
						'Type',
						'Soustype',
						'Dossier',
						'Service',
						'Affaire',
						'Tache',
						'Circuit'
					)
				)
			)
			);
		}
		return false;
	}


	/**
	 * Fonction supprimant les informations d'une recherche enregistrée
	 * @param type $field
	 * @return type
	 *
	 */
	public function deleteSearch($id) {
			$this->Jsonmsg->init(__d('default', 'delete.error'));
			$this->Recherche->begin();
			if ($this->Recherche->delete($id)) {
				$this->Recherche->commit();
				$this->Jsonmsg->valid('La recherche a bien été supprimée');
			} else {
				$this->Recherche->rollback();
			}
			$this->Jsonmsg->send();
	}
}
?>
