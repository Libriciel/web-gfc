<?php

/**
 * Scanemails
 *
 * Scanemails controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class TitresController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Titres';
    public $uses = array('Titre');

    /**
     * Gestion des emails interface graphique)
     *
     * @logical-group Titres
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {

    }

    /**
     * Ajout d'un email
     *
     * @logical-group Titres
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {

        if (!empty($this->request->data)) {
            $this->Titre->begin();

            $this->Titre->create($this->request->data);
            if ($this->Titre->save()) {
                $this->Titre->commit();
                $this->Jsonmsg->valid();
            } else {
                $this->Titre->rollback();
                $this->Jsonmsg->error('Erreur de sauvegarde');
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Edition d'un type
     *
     * @logical-group Titres
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $titres = $this->request->data;
            $this->Titre->create($titres);
            if ($this->Titre->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Titre->find('first', array('conditions' => array('Titre.id' => $id)));
            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
        $this->set('$titres', $this->Titre->find('list'));
    }

    /**
     * Suppression d'un email
     *
     * @logical-group Titres
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Titre->begin();
        if ($this->Titre->delete($id)) {
            $this->Titre->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Titre->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des mails à scruter (ajax)
     *
     * @logical-group Titres
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getTitre() {
        $titres_tmp = $this->Titre->find("all", array('order' => 'Titre.name'));
        $titres = array();
        foreach ($titres_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $titres[] = $item;
        }
        $this->set(compact('titres'));
    }

    /**
     * Validation d'unicité du nom d'une collectivité (création / édition)
     *
     * @param type $field
     */
    public function ajaxformvalid($field = null, $collectiviteId = 0) {

        $models = array();
        if ($field != null) {
            $models['Titre'] = array($field);
        } else {
            $models[] = 'titre';
        }

        $rd = array();

        foreach ($this->request->data as $k => $v) {
            if ($k == 'Titre') {
                $rd[$k] = $v;
            }
        }

        $this->Ajaxformvalid->valid($models, $rd, $collectiviteId);
    }

}

?>

