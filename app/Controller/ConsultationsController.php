<?php

/**
 * Consultation
 *
 * Consultations controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class ConsultationsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Consultations';
    public $uses = array('Consultation');

    /**
     * Gestion des opérations interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            __d('menu', 'consultationSAERP', true)
        ));
        $operations = $this->Consultation->Operation->find('list', array('conditions' => array('Operation.active' => true), 'order' => 'Operation.name ASC'));
        $this->set(compact('operations'));
    }

    /**
     *
     */
    public function beforeFilter() {
        $this->Auth->allow(array('export'));
        parent::beforeFilter();
    }

    /**
     *
     */
    protected function _setOptions() {
        $this->set('consultationsList', $this->Consultation->find('list', array('conditions' => array('Consultation.active' => true))));
        $this->set('operationsList', $this->Consultation->Operation->find('list', array('conditions' => array('Operation.active' => true))));
        $this->set('origines', $this->Consultation->Pliconsultatif->Origineflux->find('list', array('conditions' => array('Origineflux.active' => true))));
    }

    /**
     * Ajout d'un e opération
     *
     * @logical-group Consultations
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        $operations = $this->Consultation->Operation->find('list', array('conditions' => array('Operation.active' => true), 'order' => 'Operation.name ASC'));
        $this->set('operations', $operations);
        $consultations = $this->Consultation->find('all');

        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $consultation = $this->request->data;
            $consultation['Consultation']['name'] = $consultation['Consultation']['numero'];


            $this->Consultation->begin();
            $this->Consultation->create($consultation);
            if ($this->Consultation->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Consultation->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Consultation->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }
    }

    /**
     * Edition d'une opération
     *
     * @logical-group Consultations
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        $operations = $this->Consultation->Operation->find('list', array('conditions' => array('Operation.active' => true), 'order' => 'Operation.name ASC'));
        $this->set('operations', $operations);
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Consultation->create();
            $consultation = $this->request->data;

            if ($this->Consultation->save($consultation)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Consultation->find(
                    'first', array(
                'conditions' => array(
                    'Consultation.id' => $id
                ),
                'contain' => false
                    )
            );
            $this->request->data['Consultation']['datelimite'] = date("d/m/Y", strtotime($this->request->data['Consultation']['datelimite']));

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
    }

    /**
     * Suppression d'une opération
     *
     * @logical-group Consultations
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Consultation->begin();
        if ($this->Consultation->delete($id)) {
            $this->Consultation->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Consultation->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des opérations (ajax)
     *
     * @logical-group Consultations
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getConsultations() {
        $conditions = array();

        if( !empty($this->request->data) ) {
            if( isset($this->request->data['SearchConsultation']['datenotificationdebut'])  && !empty($this->request->data['SearchConsultation']['datenotificationdebut']) ) {
                $dateNotif = $this->request->data['SearchConsultation']['datenotificationdebut'];
                if( strpos( $dateNotif,  '/'  ) != 0 ) {
                    $debut = explode( '/', $dateNotif );
                    $day = $debut[0];
                    $month = $debut[1];
                    $year = $debut[2];
                    $dateDebut = $year.'-'.$month.'-'.$day;
                    $this->request->data['SearchConsultation']['datenotificationdebut'] = $dateDebut;
                }
            }
            if( isset($this->request->data['SearchConsultation']['datenotificationfin'])  && !empty($this->request->data['SearchConsultation']['datenotificationfin']) ) {
                $dateNotifFin = $this->request->data['SearchConsultation']['datenotificationfin'];
                if( strpos( $dateNotifFin,  '/'  ) != 0 ) {
                    $fin = explode( '/', $dateNotifFin );
                    $day = $fin[0];
                    $month = $fin[1];
                    $year = $fin[2];
                    $dateFin = $year.'-'.$month.'-'.$day;
                    $this->request->data['SearchConsultation']['datenotificationfin'] = $dateFin;
                }
            }
        }

        $querydata = $this->Consultation->search($this->request->data);
        $consultations_tmp = $this->Consultation->find('all', $querydata);
//debug($consultations);
        $consultations = array();
        foreach ($consultations_tmp as $i => $item) {
            $nbplis = $this->Consultation->Pliconsultatif->find(
                'count',
                array(
                    'conditions' => array(
                        'Pliconsultatif.consultation_id' => $item['Consultation']['id']
                    ),
                    'contain' => false
                )
            );
            $item['Consultation']['datelimite'] = date("d/m/Y", strtotime($item['Consultation']['datelimite']));
            $item['Consultation']['operationname'] = $item['Operation']['name'];
            $item['Consultation']['nbplis'] = $nbplis;
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $consultations[$i] = $item;
        }

        if( isset($this->request->data['SearchMarche']['datenotificationdebut'])  && !empty($this->request->data['SearchMarche']['datenotificationdebut']) ) {
            $dateNotif = $this->request->data['SearchMarche']['datenotificationdebut'];
            $debut = explode( '-', $dateNotif );
            $day = $debut[0];
            $month = $debut[1];
            $year = $debut[2];
            $dateDebut = $year.'/'.$month.'/'.$day;
            $this->request->data['SearchMarche']['datenotificationdebut'] = $dateDebut;
        }
        if( isset($this->request->data['SearchMarche']['datenotificationfin'])  && !empty($this->request->data['SearchMarche']['datenotificationfin']) ) {
            $dateNotifFin = $this->request->data['SearchMarche']['datenotificationfin'];
            $fin = explode( '-', $dateNotifFin );
            $day = $fin[0];
            $month = $fin[1];
            $year = $fin[2];
            $dateFin = $year.'/'.$month.'/'.$day;
            $this->request->data['SearchMarche']['datenotificationfin'] = $dateFin;
        }
        $this->set(compact('consultations', 'operations'));
        $this->_setOptions();
    }

    /**
     * Validation d'unicité du nom d'un marché (création / édition)
     *
     * @param type $field
     */
    public function ajaxformvalid($field = null) {

        if ($field == 'consultationname') {
            if (in_array($this->request->data['Consultation']['name'], $this->Consultation->find('list', array('conditions' => array('Consultation.active' => true), 'fields' => array('id', 'name'))))) {
                $this->autoRender = false;
                $content = json_encode(array('Consultation' => array('name' => 'Valeur déjà utilisée')));
                header('Pragma: no-cache');
                header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
                header('Content-Type: text/x-json');
                header("X-JSON: {$content}");
                Configure::write('debug', 0);
                echo $content;
                return;
            }
        }
        $models = array();
        if ($field != null) {
            if ($field == 'consultationname') {
                $models['Consultation'] = array('name');
            }
        } else {
            $models[] = 'Consultation';
        }

        $rd = array();
        foreach ($this->request->data as $k => $v) {
            if (($field != 'consultationname' && $k == 'Consultation')) {
                $rd[$k] = $v;
            }
        }
        $this->Ajaxformvalid->valid($models, $rd);
    }

    /**
     * Export des informations liées aux marchés
     *
     * @logical-group Consultations
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function export() {
//        ini_set('memory_limit', '3000M');
        $name = Hash::get($this->request->params['named'], 'SearchConsultation__name');
        $numeroConsultation = Hash::get($this->request->params['named'], 'SearchConsultation__numero');
        $consultationActif = Hash::get($this->request->params['named'], 'SearchConsultation__active');
//debug($this->request->params);
//die();
        $conditions = array();
        $conditionsPlis = array();
        if (!empty($this->request->params)) {

            if (isset($name) && !empty($name)) {
                $conditions[] = 'Consultation.name ILIKE \'' . $this->Consultation->wildcard('%' . $name . '%') . '\'';
            }

            if (isset($consultationActif) && !empty($consultationActif)) {
                $conditions[] = array('Consultation.active' => $consultationActif);
            }

            if (isset($numeroConsultation) && !empty($numeroConsultation)) {
                $conditions[] = array('Consultation.numero' => $numeroConsultation);
            }
        }

        $querydata = array(
            'conditions' => array(
                $conditions
            ),
            'order' => array(
                'Consultation.name'
            ),
            'contain' => array(
                'Pliconsultatif' => array(
                    'conditions' => array(
                        $conditionsPlis
                    ),
                    'order' => array('Pliconsultatif.numero ASC')
                ),
                'Operation'
            )
        );
        $results = $this->Consultation->find('all', $querydata);
        $this->set('results', $results);

//debug($results);
//die();
        $this->_setOptions();
        $this->layout = '';
//		$this->set( compact( 'annee', 'origine', 'origineName' ) );
    }

    /**
     * Visualisation de la liste des Plis consultatifs liés à 1 marché
     *
     * @logical-group Marches
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getPlis($consultation_id) {

        $consultation = $this->Consultation->find(
                "first", array(
            'conditions' => array(
                'Consultation.id' => $consultation_id
            ),
            'contain' => array(
                'Pliconsultatif'
            ),
            'order' => 'Consultation.name'
                )
        );
        $consultationId = $consultation_id;


        $selectplis_tmp = $this->Consultation->Pliconsultatif->find(
                'all', array(
            'fields' => array_merge(
                    $this->Consultation->Pliconsultatif->fields(), $this->Consultation->Pliconsultatif->Origineflux->fields()
            ),
            'conditions' => array(
                'Pliconsultatif.consultation_id' => $consultation_id
            ),
            'joins' => array(
                $this->Consultation->Pliconsultatif->join('Origineflux', array('type' => 'LEFT OUTER'))
            ),
            'recursive' => -1,
            'order' => array('Pliconsultatif.numero ASC')
                )
        );
        $selectplis = array();
        foreach ($selectplis_tmp as $item) {
            $item['Pliconsultatif']['date'] = date('d/m/Y', strtotime($item['Pliconsultatif']['date']));
            $item['right_edit'] = true;
            $item['right_delete'] = true;
//            $item['Pliconsultatif']['origine'] = $item['Origineflux']['name'];
//            $item['Pliconsultatif']['date'] = date('d/m/Y', strtotime($item['Pliconsultatif']['date']));
            $selectplis[] = $item;
        }
        $this->set('selectplis', $selectplis);
//debug($selectplis);
        $this->set(compact('consultation', 'consultationId', 'selectplis'));
    }

}

?>
