<?php

/**
 * Directmairies
 *
 * Directmairies controller class
 *
 * web-GFC : Gestion de Flux Citoyens
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par Libriciel SCOP
 * @link http://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class DirectmairiesController extends AppController {

	/**
	 * Controller name
	 *
	 * @var string
	 * @access public
	 */
	public $name = 'Directmairies';

	/**
	 * Controller uses
	 *
	 * @var array
	 * @access public
	 */
//	public $uses = array('Directmairie');

	public $components = array('Directmairie', 'Linkeddocs');

	/**
	 *
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('ajaxformvalid');
	}


	/**
	 * Gestion des outils disponibles pour l'administrateur)
	 *
	 * @logical-group Outils
	 * @user-profil Admin
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
			'<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
			__d('menu', 'Outils', true)
		));

		$conn = CakeSession::read('Auth.User.connName');
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$this->Connecteur->setDataSource($conn);
		$hasDm = false;
		$hasDmActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Direct%'
				),
				'contain' => false
			)
		);
		if( !empty( $hasDmActif ) && $hasDmActif['Connecteur']['use_dm'] ) {
			$hasDm = true;
		}
		$this->set('hasDm', $hasDm );
	}

	/**
	 * Recherche des procédures de Directs simplifiées existantes
	 *
	 */
	public function search() {
		$issueAlreadyGenerated = false;
		$courrierGeneratedId = null;
		$referenceGenerated = null;
		$errorMessage = null;
		$datasOfPicture = '';
		$tmpFile = '';
		$conn = CakeSession::read('Auth.User.connName');
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$this->Connecteur->setDataSource($conn);
		$hasDmActif = false;
		$datas = array();
		if( !empty($this->request->data) ) {
			if( Configure::read('Webservice.Directmairie') ) {
				// On vérifie si la requête existe toujours dans la GRC
				$this->loadModel('Connecteur');
				$hasDmActif = $this->Connecteur->find(
					'first',
					array(
						'conditions' => array(
							'Connecteur.name ILIKE' => '%Direct%',
							'Connecteur.use_dm' => true
						),
						'contain' => false
					)
				);

				$issueId = (int)$this->request->data['Directmairie']['issueId'];

				if( !empty( $hasDmActif )) {
					$dm = new DirectmairieComponent;
					// On ne recherche que la procédure
					if( !empty($issueId) ) {
						$retour = $dm->getIssue($issueId);
						if( !empty($retour->body) && $retour->code == 200 ) {
							$datas = json_decode( $retour->body );
							if( !empty($datas) && !empty($datas->pictures[0])) {
								foreach($datas->pictures as $p => $picture)  {
									$pictureData = $dm->getPictures($issueId, $picture->id);
									if( !empty($pictureData)) {
										$datasOfPicture = $pictureData->body;
										$tmpFile = APP . DS . WEBROOT_DIR . DS . 'files/previews/image'.$picture->id;
										$this->Linkeddocs->purgeImage();
										file_put_contents($tmpFile, $datasOfPicture);
									}
								}
							}
						}
						else {
							$errorMessage = $retour;
						}
						if( !empty($datas->id) && is_int($issueId) ) {
							$this->loadModel('Courrier');
							$this->Courrier->setDataSource($conn);
							$courrier = $this->Courrier->find(
								'first',
								array(
									'fields' => array('Courrier.id', 'Courrier.reference', 'Courrier.dm_issue_id'),
									'conditions' => array(
										'Courrier.dm_issue_id' => $issueId
									),
									'contain' => false,
									'recursive' => -1
								)
							);
							if( !empty($courrier['Courrier']['dm_issue_id']) ) {
								if( $courrier['Courrier']['dm_issue_id'] == $datas->id) {
									$issueAlreadyGenerated = true;
									$courrierGeneratedId =  $courrier['Courrier']['id'];
									$referenceGenerated =  $courrier['Courrier']['reference'];
								}
							}
						}
					}
					else {
						$retour = $dm->getAllIssues();
						if( !empty($retour->body)) {
							$content = json_decode( $retour->body );
							foreach( $content->content as $content ) {
								$datas[] = $content;
								if (!empty($content) && !empty($content->pictures[0])) {
									foreach ($content->pictures as $p => $picture) {
										$pictureData = $dm->getPictures($content->id, $picture->id);
										if (!empty($pictureData)) {
											$datasOfPicture = $pictureData->body;
											$tmpFile = APP . DS . WEBROOT_DIR . DS . 'files/previews/image' . $picture->id;
											$this->Linkeddocs->purgeImage();
											file_put_contents($tmpFile, $datasOfPicture);
										}
									}
								}

								if( !empty($content->id) && is_int($content->id) ) {
									$this->loadModel('Courrier');
									$this->Courrier->setDataSource($conn);
									$courrier = $this->Courrier->find(
										'first',
										array(
											'fields' => array('Courrier.id', 'Courrier.reference', 'Courrier.dm_issue_id'),
											'conditions' => array(
												'Courrier.dm_issue_id' => $content->id
											),
											'contain' => false,
											'recursive' => -1
										)
									);
									if( !empty($courrier['Courrier']['dm_issue_id']) ) {
										if( $courrier['Courrier']['dm_issue_id'] == $content->id) {
											$content->alreadyCreated = true;
											$issueAlreadyGenerated[$content->id] = true;
											$courrierGeneratedId[$content->id] =  $courrier['Courrier']['id'];
											$referenceGenerated[$content->id] =  $courrier['Courrier']['reference'];
										}
									}
								}
							}
						}
						else {
							$errorMessage = $retour;
						}
					}
					$this->set( 'tmpFile', $tmpFile );

				}
			}
		}

		$this->set('datas', $datas);
		$this->set('hasDmActif', $hasDmActif);
		$this->set('issueAlreadyGenerated', $issueAlreadyGenerated);
		$this->set('courrierGeneratedId', $courrierGeneratedId);
		$this->set('referenceGenerated', $referenceGenerated);
		$this->set('errorMessage', $errorMessage);
		$this->set('datasOfPicture', $datasOfPicture);

		$translationOfStatus = array(
			'CREATED' => 'Créée',
			'REFUSED' => 'Refusée',
			'REJECTED' => 'Rejetée'
		);
		$this->set('translationOfStatus', $translationOfStatus);
	}


	/**
	 * Fonction permettant de créer un flux avec les données récupérées sur Direct Mairie
	 * @param $procedureId
	 * @param $dossierId
	 */
	public function createCourrier( $issueId ) {

		$this->autoRender = false;

		$this->Jsonmsg->init();
		$conn = CakeSession::read('Auth.User.connName');
		$this->Courrier = ClassRegistry::init('Courrier');
		$this->Courrier->setDataSource($conn);
		$this->Contact = ClassRegistry::init('Contact');
		$this->Contact->setDataSource($conn);
		$this->Organisme = ClassRegistry::init('Organisme');
		$this->Organisme->setDataSource($conn);
		$this->Bancontenu = ClassRegistry::init('Bancontenu');
		$this->Bancontenu->setDataSource($conn);
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$this->Connecteur->setDataSource($conn);


		$hasDmActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Direct%',
					'Connecteur.use_dm' => true
				),
				'contain' => false
			)
		);


		$issueInfo = array();
		$dm = new DirectmairieComponent;
		$retour = $dm->getIssue($issueId);
		if( !empty($retour->body)) {
			$issueInfo = json_decode( $retour->body );
		}
		// On regarde et on associe le contact si présent
		if( !empty($issueInfo) ) {
			$this->Courrier->begin();
			$dateToDisplay = date( 'd/m/Y à H:i:s', strtotime($issueInfo->creationInstant));

			$coordonnees = "https://www.openstreetmap.org/?mlat=".$issueInfo->coordinates->latitude."&mlon=".$issueInfo->coordinates->latitude."&zoom=17";

			$objet = $issueInfo->description . "\n" . 'Coordonnées : '.$coordonnees."\n".'- Latitude : '.$issueInfo->coordinates->latitude . "\n" . '- Longitude : '.$issueInfo->coordinates->longitude . "\n" . 'Compte associé : '.$issueInfo->assignedGovernment->name . "\n" . 'SIREN : '.$issueInfo->assignedGovernment->code . "\n" . 'Date de création : '.$dateToDisplay;

			// Enfin, on crée le flux
			$data = array(
				'Courrier' => array(
					'name' => '[DM] '.$issueInfo->category->label,
					'objet' => $objet,
					'user_creator_id' => CakeSession::read('Auth.User.id'),
					'date' => date('Y-m-d'),
					'datereception' => date( 'Y-m-d H:i:s', strtotime($issueInfo->creationInstant)),
					'reference' => $this->Courrier->genReferenceUnique(),
					'contact_id' => Configure::read('Sanscontact.id'),
					'organisme_id' => Configure::read('Sansorganisme.id'),
					'dm_issue_id' => $issueId
				)
			);
			$this->Courrier->create($data);
			if ($this->Courrier->save()) {
				$courrierId = $this->Courrier->id;
				$return['CodeRetour'] = "CREATED";
				$return['Message'] = "Le flux a été créé";
				$return['Severite'] = "normal";
				$return['CourrierId'] = $courrierId;

				$return['Reference'] = $data['Courrier']['reference'];
				$return['ContactId'] = $data['Courrier']['contact_id'];
				$return['OrganismeId'] = $data['Courrier']['organisme_id'];

				$desktopmanagerId = $hasDmActif['Connecteur']['dm_desktopmanager_id'];
				$desktopsIds = $this->Courrier->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
				$desktops = array();
				foreach ($desktopsIds as $i => $desktopId) {
					$desktops = $this->Bancontenu->Desktop->find(
						'all', array(
							'conditions' => array(
								'Desktop.id' => $desktopsIds[$i]
							),
							'contain' => false
						)
					);

					if ($desktops[$i]['Desktop']['profil_id'] == DISP_GID) {
						$BAN = BAN_AIGUILLAGE;
					} else if ($desktops[$i]['Desktop']['profil_id'] == INIT_GID) {
						$BAN = BAN_DOCS;
					}
					if( !empty($BAN)) {
						$this->Bancontenu->add($desktopId, $BAN, $courrierId);
					}
					else {
						$return['CodeRetour'] = "ERROR";
						$return['Message'] .= "L'utilisateur n'a pas de profil Aiguilleur ou Initiateur.";
					}
				}

				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $data['Courrier']['reference'], true, 0777);
				if(  isset($issueInfo->pictures) && !empty($issueInfo->pictures[0]->id) ) {
					foreach($issueInfo->pictures as $p => $picture)  {
						$ismainDoc = false;
						if( $p === 0 ) {
							$ismainDoc = true;
						}
						$pictureData = $dm->getPictures($issueId, $picture->id);
						if( !empty($pictureData)) {
							$datasOfPicture = $pictureData->body;
							$tmpFile = WORKSPACE_PATH . DS . $conn . DS . $data['Courrier']['reference'] . DS . "{$picture->id}";
							file_put_contents($tmpFile, $datasOfPicture);
							$this->_genFileAssoc($tmpFile, $picture->id, $courrierId, $ismainDoc, $return);
						}
					}
				}
				$this->Courrier->commit();
				$this->Jsonmsg->valid('Le flux a été créé à partir de cette remontée');
				$courrierId  = $this->Courrier->id;
				$this->Jsonmsg->json['newCourrierId'] = $courrierId;
			}
			else {
				$this->Courrier->rollback();
				$this->Jsonmsg->error( 'Erreur lors de la création du flux');
			}
			$this->Jsonmsg->send();
		}
	}

	/**
	 * Génération d'un document lié
	 *
	 * @access private
	 * @param base64 $fichier_associe contenu du document lié
	 * @param integer $fluxId identifiant du flux
	 * @param array $return valeur de retour
	 * @return void
	 */
	private function _genFileAssoc($path, $name, $fluxId, $ismainDoc, &$return) {
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		$fileMimeType = $finfo->buffer(file_get_contents($path));
//$this->log($fileMimeType);
		$conn = CakeSession::read('Auth.User.connName');
		$this->Document = ClassRegistry::init('Document');
		$this->Document->setDataSource($conn);


		$fileMimeTypeError = false;
		if ($fileMimeType == "application/pdf" || $fileMimeType == "PDF") {
			$fileExt = "pdf";
		} else if ($fileMimeType == "application/vnd.oasis.opendocument.text" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.TEXT" || $fileMimeType == "OCTET-STREAM") {
			$fileExt = "odt";
		} else if ($fileMimeType == "image/bmp") {
			$fileExt = "bmp";
		} else if ($fileMimeType == "image/tiff") {
			$fileExt = "tif";
		} else if ($fileMimeType == "text/plain") {
			$fileExt = "txt";
		} else if ($fileMimeType == "application/vnd.ms-office") {
			$fileExt = "doc";
		} else if ($fileMimeType == "application/msword") {
			$fileExt = "doc";
		} else if ($fileMimeType == "application/rtf") {
			$fileExt = "rtf";
		} else if ($fileMimeType == "image/png" || $fileMimeType == "PNG") {
			$fileExt = "png";
		} else if ($fileMimeType == "image/jpeg" || $fileMimeType == "JPEG" || $fileMimeType == "application/x-empty") {
			$fileExt = "jpg";
		} else if ($fileMimeType == "application/vnd.oasis.opendocument.spreadsheet" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.SPREADSHEET") {
			$fileExt = "ods";
		} else if ($fileMimeType == "VND.OPENXMLFORMATS-OFFICEDOCUMENT.WORDPROCESSINGML.DOCUMENT" || $fileMimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
			$fileExt = "docx";
		} else if ($fileMimeType == "GIF" || $fileMimeType == "gif") {
			$fileExt = "gif";
		} else if ($fileMimeType == '') {
			$fileMimeType = 'txt';
			$fileExt = "rtf";
		} else {
			$fileMimeTypeError = true;
		}



		if ($fileMimeTypeError) {
			$return['CodeRetour'] .= "FILENOK";
			$return['Message'] .= ". Le fichier n'est pas reconnu";
		} else {
			$doc = array(
				'Document' => array(
					'name' => $name,
					'size' => strlen(file_get_contents($path)),
					'mime' => $fileMimeType,
					'ext' => $fileExt,
					'courrier_id' => $fluxId,
					'main_doc' => $ismainDoc,
					'path' => $path
				)
			);

			$this->Courrier->setDataSource(Configure::read('conn'));
			$this->Courrier->Document->setDataSource(Configure::read('conn'));
			$this->Courrier->Document->create($doc);
			$docsaved = $this->Courrier->Document->save();
			if (empty($docsaved)) {
				$return['CodeRetour'] .= "NOFILE";
				$return['Message'] .= "Le flux a bien été créé mais le fichier n'a pas été envoyé<br />Veuillez ré-essayer.";
				$this->Courrier->delete($fluxId);
			} else {
				$return['CodeRetour'] .= "FILEOK";
				$return['Message'] .= "Le flux a bien été créé et le fichier a été envoyé";
			}
		}
	}

	/**
	 * Validation du champ procedureId
	 *
	 * @param type $field
	 */
	public function ajaxformvalid($field = null) {
		if( empty( $this->request->data['Directmairie']['issueId'] ) ) {
			$this->autoRender = false;
			$content = json_encode(array('Directmairie' => array($field => 'Valeur obligatoire')));
			header('Pragma: no-cache');
			header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
			header('Content-Type: text/x-json');
			header("X-JSON: {$content}");
			Configure::write('debug', 0);
			echo $content;
			return;
		}

		$models = array();
		if ($field != null) {
			if ($field == 'issueId') {
				$models['Directmairie'] = array('issueId');
			} else {
				$models['Directmairie'] = array('date');
			}
		}

		$rd = array();
		foreach ($this->request->data as $k => $v) {

			if ($field == 'issueId' && $k == 'Directmairie') {
				$rd[$k] = $v;
			}
		}

		$this->Ajaxformvalid->valid($models, $rd);
	}
}

?>
