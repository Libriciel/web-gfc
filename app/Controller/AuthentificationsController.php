<?php


/**
 * Authentifications
 *
 * Authentifications controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

App::uses('AppController', 'Controller');
App::uses('File', 'Utility');
class AuthentificationsController extends AppController {

	/**
	 *
	 * Controller name
	 *
	 * @var array
	 */
	public $name = 'Authentifications';

	/**
	 *
	 * Controller uses
	 *
	 * @var array
	 */
	public $uses = array('Authentification');

	/**
	 * Fonction de rappel beforeFilter
	 *
	 * @description  Paramétrage spécifique du composant Auth. Si on est connecté en tant que superadmin, on peut accèder à toutes les pages sans utiliser les Acl de CakePHP.
	 *
	 * @access public
	 * @return void
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->Auth->User('id') && Configure::read('conn') == "default") {
			$this->Auth->allow('*');
		}
	}

    /**
	 * Gestion des authentifications (interface graphique)
	 *
	 * @logical-group Authentifications
	 * @user-profile Superadmin
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
                    '<a href="/environnement">'.__d('menu', 'Authentifications').'</a>',
                    __d('menu', 'Authentifications')));

        $this->request->data = $this->Authentification->find(
            'first',
            array(
                'contain' => false
            )
        );


        if (file_exists(Configure::read('AuthManager.Cas.cert_path')) && Configure::read('AuthManager.Authentification.type') == 'CAS') {
            $clientCert = openssl_x509_parse(file_get_contents(Configure::read('AuthManager.Cas.cert_path')));
            $this->set('clientCert', $clientCert);
        }
//        debug($this->request->data['A']);
	}

	/**
	 * Récupération de la liste des Authentifications (ajax)
	 *
	 * @logical-group Authentifications
	 * @user-profile Superadmin
	 *
	 * @access public
	 * @return void
	 */
	public function getAuthentifications() {
		$authentifications_tmp = $this->Authentification->find("all", array('order' => 'Authentification.id'));
		$authentifications = array();
		foreach ($authentifications_tmp as $item) {
			$item['right_edit'] = true;
			$item['right_delete'] = $this->Authentification->isDeletable($item['Authentification']['id']);
			$authentifications[] = $item;
		}

		$this->set('authentifications', $authentifications);
	}




     public function makeconf( ) {
        $this->Jsonmsg->init();
        $file = new File(APP . 'Config' . DS . 'webgfc.inc', true);
        $content = $file->read();

        if (!empty($this->request->data)) {
            $this->Authentification->begin();
            $this->Authentification->create();

            if($this->request->data['Authentification']['use_cas'] === 'true'){

                 if (!empty($this->request->params['form']['myfile']['tmp_name'])) {
                    $certs = array();
                    $this->request->data['Authentification']['nom_cert'] = $this->request->params['form']['myfile']['name'];
                    $cert = file_get_contents($this->request->params['form']['myfile']['tmp_name']);
                    $this->request->data['Authentification']['autcert'] = $cert;
                    $path_dir_cas = APP . DS . 'Config' . DS . 'cert_cas' . DS;
                    file_put_contents($path_dir_cas . 'client.pem', $cert);
                    if ($this->Authentification->save($this->request->data)) {
                        $this->Authentification->commit();
                        $this->Jsonmsg->valid();
                    }
                    else {
                        $this->Jsonmsg->error('Erreur lors de l\'enregistrement');
                        $this->Authentification->rollback();
                    }
                    $this->Jsonmsg->send();
                }
                else {
                    unset($this->request->params['form']['myfile']);
                }
                $content = $this->_replaceValue($content, 'AuthManager.Authentification.type', 'CAS');
            }
            else {
                unset($this->request->data['Authentification']['autcert']);
                $content = $this->_replaceValue($content, 'AuthManager.Authentification.type', '');
                if(!empty( $this->request->data['Authentification']['id'] ) ) {
                    $this->Authentification->updateAll(
                        array( 'Authentification.id' => $this->request->data['Authentification']['id']),
                        array( 'Authentification.use_cas' => false)
                    );
                }
            }

            $content = $this->_replaceValue($content, 'AuthManager.Authentification.use', $this->request->data['Authentification']['use_cas']);


            $content = $this->_replaceValue($content, 'AuthManager.Cas.host', $this->data['Authentification']['host']);
            $content = $this->_replaceValue($content, 'AuthManager.Cas.port', $this->data['Authentification']['port']);
            $content = $this->_replaceValue($content, 'AuthManager.Cas.uri', $this->data['Authentification']['contexte']);
            $content = $this->_replaceValue($content, 'AuthManager.Cas.proxy', $this->data['Authentification']['proxy']);
//debug(Configure::read('AuthManager.Authentification.use'));
//die();
//            if(Configure::read('AuthManager.Authentification.use') == true){
//
//                if( !empty($this->data['Authentification']['autcert']['tmp_name'])) {
//
//                }
//            }
        }


        if (!$file->writable()) {
            $this->Session->setFlash(__('Impossible de modifier le fichier de configuration, veuillez donner les droits sur fichier webgfc.inc'), 'growl', array('type' => 'danger'));
        } else {
            $success = $file->open('w+');
            $success &= $file->append($content);
            $success &= $file->close();
            if ($success) {
                $this->Session->setFlash(__('La configuration du connecteur CAS a été enregistrée'), 'growl');
            } else {
                $this->Session->setFlash(__('Un problème est survenu lors de la modification du fichier de configuration webgfc.inc'), 'growl', array('type' => 'danger'));
            }
        }
        $this->Jsonmsg->send();


        return $this->redirect(array('controller' => 'environnement', 'action' => 'index'));
    }
    /**
     * @version 4.2
     * @access private
     * @param type $content
     * @param type $param
     * @param type $new_value
     * @return type
     */
    private function _replaceValue($content, $param, $new_value)
    {
        if (is_bool(Configure::read($param))) {
            $valeur = Configure::read($param) === true ? 'true' : 'false';
        } else {
            $valeur = Configure::read($param);
        }

        $host_b = "Configure::write('$param', '" . $valeur . "');";
        $host_a = "Configure::write('$param', '$new_value');";
        $return = str_replace($host_b, $host_a, $content, $count);

        if ($count === 0) {
            $host_b = "Configure::write('$param', " . $valeur . ");";
            $host_a = "Configure::write('$param', $new_value);";

            $return = str_replace($host_b, $host_a, $content, $count);
        }

        if ($count === 0 && is_array($new_value)) {
            $host_b = "Configure::write('$param', " . var_export($valeur, true) . ");";
            $host_a = "Configure::write('$param', " . var_export($new_value, true) . ");";

            $return = str_replace($host_b, $host_a, $content, $count);
        }

        return $return;
    }



}
