<?php

/**
 * Mots-clefs
 *
 * Keywords controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 *
 * based on As@lae keywordlists / keywords (thesaurus)
 * @link https://adullact.net/projects/asalae/
 */
class KeywordsController extends AppController {

	/**
	 * Controller name
	 *
	 * @access public
	 * @var string
	 */
	public $name = 'Keywords';

	/**
	 *
	 * Controller uses
	 *
	 * @access public
	 * @var array
	 */
	public $uses = array('Keyword', 'Keywordlist');

	/**
	 * Liste des Mots-Clés
	 *
	 * @param type $keywordlist_id
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function index($keywordlist_id) {
//		$tabVersions = array();
//		$this->set('lastVersion', $this->Keywordlist->getLastVersion($keywordlist_id));
//
//		$this->set('isWorkingVersion', false);
//		$this->pageTitle = Configure::read('appName') . ' : ' . __('Keywords', true) . ' : ' . __('liste', true);
////    $this->Keyword->Behaviors->attach('Containable');
//		$conditions = array('Keyword.keywordlist_id' => $keywordlist_id);
//		if (!empty($this->request->data)) {
//			array_push($conditions, array('Keyword.version' => $this->request->data['Keyword']['versions']));
//		} else {
//			array_push($conditions, array('Keyword.version' => $this->Keywordlist->getLastVersion($keywordlist_id)));
//		}
//		$this->paginate = array(
//			'conditions' => $conditions,
//			'contain' => array('Keyword.code', 'Keyword.libelle'),
//			'order' => array('Keyword.lft ASC'),
//			'limit' => $this->Auth->user('nblignes'),
//			'page' => 1
//		);
//		$this->request->data = $this->paginate('Keyword');
//		$this->set('keywordlist_id', $keywordlist_id);
//		$versions = $this->Keyword->find('list', array(
//			'conditions' => array('Keyword.keywordlist_id' => $keywordlist_id),
//			'fields' => array('Keyword.version'),
//			'recursive' => -1));
//		$versions = array_unique($versions);
//		if (empty($versions))
//			$versions[] = 0;
//		asort($versions);
//		foreach ($versions as $key => $version) {
//			$tabVersions[$version] = $version;
//			if ($version == 0) {
//				$tabVersions[$version] = 'Version de travail';
//				$this->set('isWorkingVersion', true);
//			}
//		}
//		$this->set('versions', $tabVersions);
//
//		// mise en forme pour la vue
//		foreach ($this->request->data as $i => $data) {
//			$this->request->data[$i]['ListeActions']['view'] = true;
//			$this->request->data[$i]['ListeActions']['edit'] = true;
//			$this->request->data[$i]['ListeActions']['delete'] = $this->{$this->modelClass}->isDeletable($data['Keyword']['id']);
//		}
//	}

	/**
	 * Ajout d'un mot-clé
	 *
	 * @param type $keywordlist_id
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function add($keywordlist_id) {
//		$sortie = false;
//		$this->set('keywordlist_id', $keywordlist_id);
//		if (!empty($this->request->data)) {
//			/* Initialisations avant sauvegarde */
//			$this->request->data[$this->modelClass]['keywordlist_id'] = $keywordlist_id;
//			$this->request->data[$this->modelClass]['version'] = 0;
//			$this->request->data[$this->modelClass]['created_user_id'] = $this->Auth->user('id');
//			$this->request->data[$this->modelClass]['modified_user_id'] = $this->Auth->user('id');
//			if ($this->{$this->modelClass}->save($this->request->data)) {
//				$this->Session->setFlash(__('Le mot-clé', true) . ' \'' . $this->request->data[$this->modelClass]['libelle'] . '\' ' . __('a &eacute;t&eacute; ajout&eacute;.', true), 'growl');
//				$sortie = true;
//			} else
//				$this->Session->setFlash(__('Veuillez corriger les erreurs du formulaire.', true), 'growl', array('type' => 'erreur'));
//		}
//		if ($sortie)
//			$this->redirect(array('action' => 'index', $keywordlist_id));
//		else {
//			$this->pageTitle = Configure::read('appName') . ' : ' . __("Ajout d'un mot clé", true) . ' : ' . __('ajout', true);
//			$this->set('parents', $this->{$this->modelClass}->listFields(array('conditions' => array(
//							'NOT' => array('Keyword.id' => $this->{$this->modelClass}->mesEnfantsEtMoiId($this->request->data[$this->modelClass]['id']))))));
//			$this->render('edit');
//		}
//	}

	/**
	 * Edition d'un mot-clé
	 *
	 * @param type $id
	 * @param type $keywordlist_id
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function edit($id = null, $keywordlist_id) {
//		$this->set('keywordlist_id', $keywordlist_id);
//		$sortie = false;
//		if (empty($this->request->data)) {
//			$this->request->data = $this->{$this->modelClass}->find('first', array(
//				'conditions' => array('Keyword.id' => $id),
//				'recursive' => 1));
//			if (empty($this->request->data)) {
//				$this->Session->setFlash(__('Invalide id pour le', true) . ' ' . __('mot-clé', true) . ' : ' . __('&eacute;dition impossible.', true), 'growl', array('type' => 'important'));
//				$sortie = true;
//			}
//		} else {
//			/* Initialisations avant sauvegarde */
//			$this->request->data[$this->modelClass]['modified_user_id'] = $this->Auth->user('id');
//			if ($this->{$this->modelClass}->save($this->request->data)) {
//				$this->Session->setFlash(__('Le mot-clé', true) . ' \'' . $this->request->data[$this->modelClass]['libelle'] . '\' ' . __('a &eacute;t&eacute; modifi&eacute;.', true), 'growl');
//				$sortie = true;
//			}
//			else
//				$this->Session->setFlash(__('Veuillez corriger les erreurs du formulaire.', true), 'growl', array('type' => 'erreur'));
//		}
//		if ($sortie)
//			$this->redirect(array('action' => "index/$keywordlist_id"));
//		else {
//			$this->pageTitle = Configure::read('appName') . ' : ' . __('mots-clés', true) . ' : ' . __('&eacute;dition', true);
//			$this->set('parents', $this->{$this->modelClass}->listFields(array('conditions' => array(
//							'NOT' => array('Keyword.id' => $this->{$this->modelClass}->mesEnfantsEtMoiId($this->request->data[$this->modelClass]['id']))))));
//		}
//	}

	/**
	 * Suppression d'un service
	 *
	 * @param type $id
	 * @param type $keywordlist_id
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function delete($id = null, $keywordlist_id) {
//		$eleASupprimer = $this->Keyword->find('first', array(
//			'conditions' => array('Keyword.id' => $id),
//			'recursive' => 0));
//
//		if (!$this->Keyword->isDeletable($id))
//			$this->Session->setFlash(__('Le mot-clé', true) . ' \'' . $eleASupprimer[$this->modelClass]['libelle'] . '\' ' . __('ne peut pas &ecirc;tre supprim&eacute;.', true), 'growl');
//		elseif (!$this->{$this->modelClass}->del($id, true))
//			$this->Session->setFlash(__('Une erreur est survenue pendant la suppression', true), 'growl', array('type' => 'erreur'));
//		else {
//			/* Suppression des modèles liés par belongsTo qui ne permet pas la suppression automatique en cascade */
//			$this->Session->setFlash(__('Le mot-clé', true) . ' \'' . $eleASupprimer[$this->modelClass]['libelle'] . '\' ' . __('a &eacute;t&eacute; supprim&eacute;.', true), 'growl');
//		}
//
//		$this->redirect(array('action' => 'index', $keywordlist_id));
//	}

	/**
	 * Vue détaillée des services
	 *
	 * @param type $id
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function view($id = null) {
//		$this->request->data = $this->{$this->modelClass}->find('first', array(
//			'conditions' => array('Keyword.id' => $id),
//			'recursive' => 1));
//		if (empty($this->request->data)) {
//			$this->Session->setFlash(__('Invalide id pour le', true) . ' ' . __('mot-clé', true) . ' : ' . __('affichage de la vue impossible.', true), 'growl', array('type' => 'important'));
//			$this->redirect(array('action' => 'index'));
//		} else {
//			$this->pageTitle = Configure::read('appName') . ' : ' . __('Services', true) . ' : ' . __('vue d&eacute;taill&eacute;e', true);
//
//			/* préparation des informations à afficher dans la vue détaillée */
//			$maVue = new $this->VueDetaillee(
//							__('Vue d&eacute;taill&eacute;e du mot-clé', true) . ' : ' . $this->request->data[$this->modelClass]['libelle'],
//							__('Retour &agrave; la liste des mots-clés', true));
//			$maVue->ajouteSection(__('Informations principales', true));
//			$maVue->ajouteLigne(__('Identifiant interne (id)', true), $this->request->data[$this->modelClass]['id']);
//			$maVue->ajouteLigne(__('Code', true), $this->request->data[$this->modelClass]['code']);
//			$maVue->ajouteLigne(__('Libellé', true), $this->request->data[$this->modelClass]['libelle']);
//			$maVue->ajouteLigne(__('Actif', true), $this->{$this->modelClass}->libelleActif($this->request->data[$this->modelClass]['actif']));
//			$maVue->ajouteLigne(__('Date de cr&eacute;ation', true), $this->request->data[$this->modelClass]['created']);
//			$maVue->ajouteElement(__('Par', true), $this->{$this->modelClass}->CreatedUser->toString($this->request->data['CreatedUser']));
//			$maVue->ajouteLigne(__('Date de derni&egrave;re modification', true), $this->request->data[$this->modelClass]['modified']);
//			$maVue->ajouteElement(__('Par', true), $this->{$this->modelClass}->ModifiedUser->toString($this->request->data['ModifiedUser']));
//
//			// Affichage des utilisateurs liés
//			if (!empty($this->request->data['User'])) {
//				$maVue->ajouteSection(__('Utilisateurs du service', true));
//				foreach ($this->request->data['User'] as $user) {
//					$maVue->ajouteLigne('&nbsp;', $this->Service->User->toString($user) . ($user['actif'] ? '' : ' - non actif'));
//				}
//			}
//
//			// Affichage des sous services
//			if (!empty($this->request->data['SousService'])) {
//				$maVue->ajouteSection(__('Sous services', true));
//				foreach ($this->request->data['SousService'] as $sousService) {
//					$maVue->ajouteLigne('&nbsp;', $sousService['nom']);
//				}
//			}
//
//			$this->set('contenuVue', $maVue->getContenuVue());
//			$this->render('/elements/view');
//		}
//	}

	/**
	 * Retourne true ou false selon que l'utilisateur connecté a le droit d'accès à la collectivité collecititeId
	 * en fonction du droits Collectivite:accesAToutes
	 *
	 * @param type $collectiviteId
	 * @return boolean
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	private function _collectiviteAccessible($collectiviteId) {
//		if ($this->Droits->check($this->Auth->user('id'), 'Collectivites:accesAToutes'))
//			return true;
//		else
//			return $collectiviteId == $this->Auth->user('collectivite_id');
//	}

	/**
	 * Retourne true si la collectivite $collectiviteId est valide sur les points :
	 * - si l'utilisateur connecté a les droits d'accès à cette collectivité
	 * - existence de cette collectivite en base active
	 * et false dans le cas contraire
	 *
	 * @param type $collectiviteId
	 * @return type
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	private function _collectiviteValide($collectiviteId) {
//		if ($this->Droits->check($this->Auth->user('id'), 'Collectivites:accesAToutes')) {
//			return ($this->{$this->modelClass}->Collectivite->find('count', array(
//						'conditions' => array(
//							'Collectivite.active' => 1,
//							'Collectivite.id' => $collectiviteId),
//						'recursive' => -1)) > 0);
//		} else
//			return $collectiviteId == $this->Auth->user('collectivite_id');
//	}

	/**
	 *
	 * @param type $keywordlist_id
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function tagVersion($keywordlist_id) {
//		$lastVersion = $this->Keywordlist->getLastVersion($keywordlist_id);
//		$this->Keyword->tagVersion($keywordlist_id, $lastVersion);
//		$this->Keywordlist->id = $keywordlist_id;
//		$this->Keywordlist->saveField('version', $lastVersion + 1);
//		$this->redirect(array('controller' => 'keywords', 'action' => "/index/$keywordlist_id"));
//	}

	/**
	 *
	 * @param type $keywordlist_id
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function createWorkVersion($keywordlist_id) {
//		$lastVersion = $this->Keywordlist->getLastVersion($keywordlist_id);
//		$this->Keyword->createWorkVersion($keywordlist_id, $lastVersion);
//		$this->redirect(array('controller' => 'keywords', 'action' => "/index/$keywordlist_id"));
//	}

	/**
	 *
	 * @param type $keywordlist_id
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function suppWorkVersion($keywordlist_id) {
//		$this->Keyword->suppWorkVersion($keywordlist_id);
//		$this->redirect(array('controller' => 'keywords', 'action' => "/index/$keywordlist_id"));
//	}

	/**
	 * fonction Ajax : retourne un select avec les mots clés de la version courante de la liste $keyWordListId
	 *
	 * @param type $keyWordListId
	 * @param type $key
	 * @return type
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function getCurrentList($keyWordListId, $key = 'id') {
//		$currentVersion = $this->Keywordlist->getLastVersion($keyWordListId);
//		if ($currentVersion === 0)
//			return;
//
//		$options = $this->Keyword->listFields(array(
//			'key' => $key,
//			'conditions' => array(
//				'keywordlist_id' => $keyWordListId,
//				'version' => $currentVersion)));
//		$this->set('options', $options);
//
//		$this->layout = 'ajax';
//	}

}

?>
