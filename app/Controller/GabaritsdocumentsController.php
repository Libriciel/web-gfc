<?php

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Gabaritsdocuments
 *
 * Gabaritsdocuments controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class GabaritsdocumentsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Gabaritsdocuments';
    public $uses = array('Gabaritdocument');


    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow( 'add', 'getGabaritsdocuments', 'edit', 'download');
    }


    /**
     * Gestion des opérations interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'gabaritsdocuments', true)
        ));
    }

    /**
     * Controller components
     *
     * @var array
     * @access public
     */
    public $components = array('Linkeddocs');

    /**
     * Etat d'envoi des fichiers
     *
     * @access private
     * @var array
     */
    private $_uploadStatus = array(
        'NO_UPLOAD' => 0,
        'UPLOAD_START' => 1,
        'UPLOAD_END' => 2
    );

    /**
     * Types MIME et extensions autorisés
     *
     * @access private
     * @var array
     */
    private $_allowed = array(
        'ott' => array(
            'application/vnd.oasis.opendocument.text' => 'odt',
            'application/vnd.oasis.opendocument.text-template' => 'ott',
            'application/force-download' => 'odt'
        )
    );

    /**
     * Ajout d'un modèle de gabarit
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles d'accusés de réception
     * @user-profile Admin
     *
     * @access public
     * @param integer $soustype_id
     * @return void
     */
    public function add() {
		$conn = $this->Session->read('Auth.Collectivite.conn');
        if (empty($this->request->data)) {
            $options = array();
            $warnings = array();

            $this->set('warnings', $warnings);
            $this->set('options', $options);
        } else {

            if ($this->request->is('post')) {
				$ext = '';
				if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
					$ext = $matches[1];
				}
                $upload = $this->_uploadStatus['NO_UPLOAD'];

                $gabarit = $this->request->data;

				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . 'gabarits', true, 0777);
				$name = str_replace( "/", "_",$gabarit['Gabaritdocument']['name'] );
				$name = str_replace( "'", "_", $name );
				$ext = $ext;
				$file = WORKSPACE_PATH . DS . $conn  . DS . 'gabarits' . DS . "{$name}.{$ext}";
				file_put_contents( $file, file_get_contents($_FILES['myfile']['tmp_name']));
				$path = $file;
				$gabarit['Gabaritdocument']['path'] = $path;


//debug($gabarit);
                //traitement de l upload des fichiers
                if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                    $upload = $this->_uploadStatus['UPLOAD_START'];
                    $ext = '';
                    if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                        $ext = $matches[1];
                    }
//debug($ext);
//debug($_FILES);
                    if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed[$this->request->data['Gabaritdocument']['format']]))) {
                        $message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
                    } else {
                        if ($ext != $this->_allowed[$this->request->data['Gabaritdocument']['format']][$_FILES['myfile']['type']]) {
                            $message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
                        } else {
                            $upload = $this->_uploadStatus['UPLOAD_END'];
                            $gabarit['Gabaritdocument']['size'] = $_FILES['myfile']['size'];
                            $gabarit['Gabaritdocument']['ext'] = $ext;
                            $gabarit['Gabaritdocument']['mime'] = $_FILES['myfile']['type'];
                        }
                    }
                }

                //enregistrement des infos en base si pas  d upload ou si l upload est terminé
                $create = false;
                if ($upload != $this->_uploadStatus['UPLOAD_START']) {

                    $this->Gabaritdocument->create($gabarit);
                    $gabaritsaved = $this->Gabaritdocument->save();

                    if (!empty($gabaritsaved)) {
                        $create = true;
                        $message = __d('default', 'save.ok');
                    }

                    //suppression du fichier si un fichier est uploadé
                    if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                        unlink($_FILES['myfile']['tmp_name']);
                    }
                }
                $this->set('create', $create);
                $this->set('message', $message);
            }
        }
    }

    /**
     * Edition d'un gabarit
     *
     * @logical-group Gabaritsdocuments
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {

		$conn = $this->Session->read('Auth.Collectivite.conn');
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Gabaritdocument->create();
            $gabaritdocument = $this->request->data;

            if ($this->Gabaritdocument->save($gabaritdocument)) {
                if ($this->request->is('post')) {
                    $upload = $this->_uploadStatus['NO_UPLOAD'];
                    $gabaritdocument = $this->request->data;


					$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . 'gabarits', true, 0777);
					$name  = preg_replace( "/[:']/", "_", $_FILES['myfile']['name'] );
					$name = str_replace( "'", "_", $name );
					$ext = $gabaritdocument['Gabaritdocument']['ext'];
					$file = WORKSPACE_PATH . DS . $conn  . DS . 'gabarits' . DS . "{$name}.{$ext}";
					file_put_contents( $file, file_get_contents($_FILES['myfile']['tmp_name']));
					$path = $file;
					$gabaritdocument['Gabaritdocument']['path'] = $path;



					//traitement de l upload des fichiers
                    if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                        $upload = $this->_uploadStatus['UPLOAD_START'];
                        $ext = '';
                        if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                            $ext = $matches[1];
                        }

                        if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed))) {
                            $message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
                        } else {
                            if ($ext != $this->_allowed[$_FILES['myfile']['type']]) {
    //							$message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
                            } else {
                                $upload = $this->_uploadStatus['UPLOAD_END'];
                                $gabaritdocument['Gabaritdocument']['size'] = $_FILES['myfile']['size'];
                                $gabaritdocument['Gabaritdocument']['ext'] = $ext;
                                $gabaritdocument['Gabaritdocument']['mime'] = $_FILES['myfile']['type'];
                            }
                        }
                    }

                    //enregistrement des infos en base si pas  d upload ou si l upload est terminé
                    $create = false;

                    if ($upload != $this->_uploadStatus['UPLOAD_START']) {
                        $this->Gabaritdocument->create($gabaritdocument);
                        $gabaritdocumentsaved = $this->Gabaritdocument->save();
                        if (!empty($gabaritdocumentsaved)) {
                            $create = true;
                            $message = __d('default', 'save.ok');
                            $this->Jsonmsg->valid($message);
                        }
                        //suppression du fichier si un fichier est uploadé
                        if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                            unlink($_FILES['myfile']['tmp_name']);
                        }
                    }
                    $this->set('create', $create);
                    $this->set('message', $message);
                }



                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $gabaritdocument = $this->Gabaritdocument->find(
                    'first', array(
                'conditions' => array(
                    'Gabaritdocument.id' => $id
                ),
                'contain' => false
                    )
            );

            if (empty($gabaritdocument)) {
                throw new NotFoundException();
            }
        }

        $this->set('gabaritdocument', $gabaritdocument);
    }

    /**
     * Suppression d'une opération
     *
     * @logical-group Gabaritsdocuments
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Gabaritdocument->begin();
        if ($this->Gabaritdocument->delete($id)) {
            $this->Gabaritdocument->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Gabaritdocument->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des opérations (ajax)
     *
     * @logical-group Gabaritsdocuments
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getGabaritsdocuments() {
        $gabaritsdocuments_tmp = $this->Gabaritdocument->find(
            "all",
            array(
                'fields' => array(
                    'Gabaritdocument.id',
                    'Gabaritdocument.name'
                ),
                'contain' => false,
                'order' => array('Gabaritdocument.name'),
                'recursive' => -1
            )
        );

        $gabaritsdocuments = array();
        foreach ($gabaritsdocuments_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $gabaritsdocuments[] = $item;
        }
        $this->set(compact('gabaritsdocuments'));
    }

    /**
     * Téléchargement d'un modèle
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles
     * @user-profile User
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function download($id) {
        $gabaritdocument = $this->Gabaritdocument->find('first', array('conditions' => array('Gabaritdocument.id' => $id), 'recursive' => -1));
		if (empty($gabaritdocument)) {
			throw new NotFoundException();
		}
		$this->autoRender = false;
		Configure::write('debug', 0);
		header("Pragma: public");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"" . Inflector::slug($gabaritdocument['Gabaritdocument']["name"]) . '.' . $gabaritdocument['Gabaritdocument']['ext'] . "\"");
		header("Content-length: " . $gabaritdocument['Gabaritdocument']['size']);
		print file_get_contents($gabaritdocument['Gabaritdocument']['path']);
		exit();
    }

}

?>
