<?php

App::import('Sanitize');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Flux
 *
 * Courriers controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class CourriersController extends AppController {

    /**
     * Controller name
     *
     * @var array
     */
    public $name = 'Courriers';

    /**
     * Controller uses
     *
     * @var array
     */
    public $uses = array('Courrier');

    /**
     * Controller components
     *
     * @var array
     */
    public $components = array('Email', 'DataAuthorized', 'Cmis', 'Conversion', 'Localeo', 'Pastell', 'NewPastell');

    /**
     * Controller helpers
     *
     * @var array
     */
    public $helpers = array('Notification', 'GFCComment');

	/**
	 * @var repository utilisée pour la récupération de la liste des sous-types IP
	 */
    public $repository;

    protected function _setOptions() {

        $this->set( 'bans', $this->Session->read('Auth.Ban') );

        // Valeur de l'organisme par défaut
        $this->set('sansOrganisme', Configure::read('Sansorganisme.id'));
        // Valeur du contact par défaut
        $this->set('sansContact', Configure::read('Sanscontact.id'));

        // Informations sur les carnets d'adresse
//        $this->set('addressbooks', $this->Courrier->Organisme->Addressbook->find('list', array('conditions' => array('Addressbook.active' => true), 'order' => 'Addressbook.name ASC', 'recursive' => -1)));
        $this->set('addressbooks', $this->Session->read('Auth.Addressbook'));

        $orgs = $this->Courrier->Organisme->listOrganismes();
        $this->set('orgs', $orgs );

        $options = array();
        $options = $this->Session->read('Auth.Typesvoie');


        $civilite = $this->Session->read('Auth.Civilite');
        $this->set(compact('options', 'civilite'));

        $this->set('activites', $this->Courrier->Organisme->Activite->find('list', array('order' => array('Activite.name ASC'), 'conditions' => array('Activite.active' => true), 'recursive' => -1)));

        $this->set('origineflux', $this->Session->read('Auth.Origineflux'));
        $this->set('titres', $this->Courrier->Organisme->Contact->Titre->find('list',array('order' => array('Titre.name ASC'), 'contain' => false ) ) );


        $connecteurGRCActif = false;
        if( Configure::read('Webservice.GRC') ) {
            $this->loadModel('Connecteur');
            $hasGrcActif = $this->Connecteur->find(
                'first',
                array(
                    'conditions' => array(
                        'Connecteur.name ILIKE' => '%GRC%',
                        'Connecteur.use_grc' => true
                    ),
                    'contain' => false
                )
            );
            if( !empty( $hasGrcActif )) {
                $connecteurGRCActif = true;
            }
        }
        $this->set('connecteurGRCActif', $connecteurGRCActif);


        // Informations sur les organismes
        $this->set('SoustypeFantome', Configure::read('Soustype.Fantome'));

		$connecteurSMSActif = false;
		if( Configure::read('Webservice.SMS') ) {
			$this->loadModel('Connecteur');
			$hasSmsActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%LsMessage%',
						'Connecteur.use_sms' => true
					),
					'contain' => false
				)
			);
			if( !empty( $hasSmsActif )) {
				$connecteurSMSActif = true;
			}
		}
		$this->set('connecteurSMSActif', $connecteurSMSActif);

		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
    }

    /**
     *
     */
    public function beforeFilter() {
		$this->Auth->allow('acquisition', 'getContext', 'validEtapeLastCol', 'ajaxformtype', 'traitementlot', 'execTraitementlot', 'refusparlot', 'sendcopylot', 'deletelot', 'detachelot', 'sendlot', 'updateContext', 'getFlowControls', 'getDocuments', 'ajaxreference', 'ajaxcontact', 'download_relement');
		parent::beforeFilter();
//        $this->Auth->allow('acquisition');
    }

    /**
     * Vérifie si un flux est présent dans les bannettes de tous les rôles d'un utilisateur
     *
     * @access private
     * @param array $flux
     * @param integer $desktopId si défini, la vérification se fait pour le rôle correspondant à cet identifiant
     * @return integer
     */
    private function _inBannette($flux, $desktopId = null) {
        if ($desktopId != null) {
            $desktops = array($desktopId);
        } else {
            $desktops = $this->SessionTools->getDesktopList();
        }
        $querydata = array(
            'conditions' => array(
                'Bancontenu.courrier_id' => $flux['Courrier']['id'],
                'Bancontenu.desktop_id' => $desktops,
                'Bancontenu.etat' => '1'
            ),
            'recursive' => -1
        );
        $result = $this->Courrier->Bancontenu->find('first', $querydata);
        return !empty($result);
    }

    /**
     * Vérifie si un flux est présent dans la bannette "pour copie" de tous les rôles d'un utilisateur
     *
     * @access private
     * @param array $flux
     * @param integer $desktopId si défini, la vérification se fait pour le rôle correspondant à cet identifiant
     * @return integer
     */
    private function _inBannetteCopy($fluxId, $desktopId = null) {
        if ($desktopId != null) {
            $desktops = array($desktopId);
        } else {
            $desktops = $this->SessionTools->getDesktopList();
        }
        $querydata = array(
            'conditions' => array(
                'Bancontenu.courrier_id' => $fluxId,
                'Bancontenu.desktop_id' => $desktops,
                'Bancontenu.bannette_id' => BAN_COPY,
                'Bancontenu.etat' => 1
            ),
            'recursive' => -1
        );
        $result = $this->Courrier->Bancontenu->find('first', $querydata);
        return !empty($result);
    }

    /**
     * Vérifie si un flux est présent dans les bannettes de tous les rôles d'un utilisateur avec un statut 2 (historique)
     *
     * @access private
     * @param array $flux
     * @param integer $desktopId si défini, la vérification se fait pour le rôle correspondant à cet identifiant
     * @return integer
     */
    private function _inHistorique($flux, $desktopId = null) {
        if ($desktopId != null) {
            $desktops = array($desktopId);
        } else {
            $desktops = $this->SessionTools->getDesktopList();
        }

        $querydata = array(
            'conditions' => array(
                'Bancontenu.courrier_id' => $flux['Courrier']['id'],
                'Bancontenu.desktop_id' => $desktops,
                'Bancontenu.etat' => array('0', '2')
            ),
            'recursive' => -1
        );
        $result = $this->Courrier->Bancontenu->find('first', $querydata);
        return !empty($result);
    }

    /**
     * Vérifie si l'utilisateur à le droit d'accèder au flux parent du flux réponse
     *
     * @access private
     * @param array $flux
     * @param integer $desktopId
     * @return boolean
     */
    private function _isAuthorizedResponse($flux, $desktopId = null) {
        $newFlux = $this->Courrier->find('first', array('conditions' => array('Courrier.parent_id' => $flux['Courrier']['id']), 'recursive' => -1));
        return ($this->_inBannette($newFlux, $desktopId) || $this->_inHistorique($newFlux, $desktopId));
    }

    /**
     * Visualisation / Edition d'un flux (interface graphique)
     *
     * @logical-group Flux
     * @logical-group Visualisation d'un flux
     * @logical-group Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du flux
     * @param integer $desktopId identifiant du rôle de l'utilisateur
     * @param integer $tab numéro de l'onglet à réouvrir une fois les modifications effectuées (javascript)
     * @throws NotFoundException
     * @return void
     */
    public function view($id, $desktopId = null, $tab = null, $mode = null) {

        $this->Courrier->recursive = -1;
        $qdCourrier = array(
            'fields' => array_merge(
                    $this->Courrier->fields(), $this->Courrier->Soustype->fields()
            ),
            'conditions' => array(
                'Courrier.id' => $id
            ),
            'joins' => array(
                $this->Courrier->join('Soustype')
            ),
            'recursive' => -1
        );

        $courrier = $this->Courrier->find('first', $qdCourrier);

        if (empty($courrier)) {
            throw new NotFoundException();
        }

        $fromTask = Configure::read('fromTask');
		// Analyse des commentaires présents
		$newcommentAvailable = false;
		if(Configure::read('Comment.Alert') ) {
			// CAS PUBLIC
			if (!empty($this->Courrier->getComments($id)['unread'])) {
				foreach($this->Courrier->getComments($id)['unread'] as $unread ) {
					// Si  le commentaire est public uniquement
					if( !$unread['Comment']['private'] ) {
						$newcommentAvailable = true;
						// Dans le cas du commentaire public, celui qui a écrit le commentaire ne doit pas être alerté
						if( is_array($this->Courrier->getComments($id)['writerOwner'][$unread['Comment']['id']])) {
							if( in_array( $desktopId, $this->Courrier->getComments($id)['writerOwner'][$unread['Comment']['id']]) ) {
								$newcommentAvailable = false;
							}
						}
						else if( $desktopId == $this->Courrier->getComments($id)['writerOwner'][$unread['Comment']['id']]) {
							$newcommentAvailable = false;
						}
						else {
							$this->Courrier->Comment->setRead($courrier['Courrier']['id'], $desktopId);
						}
					}
				}
			}

			// CAS PRIVE
			// Dans le cas du commentaire privé, on alerte celui qui est ciblé
			$allDesktops = [];
			if (!empty($this->Session->read('Auth.User.Env.secondary'))) {
				$allDesktops = array_keys($this->Session->read('Auth.User.Env.secondary'));
				if (!empty($allDesktops)) {
					array_push($allDesktops, $this->Session->read('Auth.User.Desktop.id'));
				}
			}

			if( !empty($this->Courrier->getComments($id)['unreadprivate']) ) {
				// Pour chaque commentaire privé non lu
				foreach($this->Courrier->getComments($id)['unreadprivate'] as $u => $unread) {
					// Pour chaque profil de l'utilisateur
					foreach( $unread['unreadReaders'] as $commentId => $desktopsIds ) {
						if (in_array($desktopId, $desktopsIds)) {
							$newcommentAvailable = true;
							// On met à jour pôur en as afficher la popup à chaque fois, uniquement la première fois
							$this->Courrier->Comment->SCCommentReader->setRead(array_keys($unread['unreadReaders']), $desktopId);
						}
						else {
							foreach( $allDesktops as $userProfilsIds ) {
								if ($desktopId == $userProfilsIds) {
									$newcommentAvailable = true;
									// On met à jour pôur en as afficher la popup à chaque fois, uniquement la première fois
									$this->Courrier->Comment->SCCommentReader->setRead(array_keys($unread['unreadReaders']), $allDesktops);
								}
							}
						}
					}
				}
			}
		}
		$this->set('newcommentAvailable', $newcommentAvailable);



// echo '<pre>';var_dump($this->Courrier->createRefus($courrier));echo '</pre>';

        $fromMesServices = false;
        $fluxMesServices = $this->Session->read('Auth.User.Courrier.mesServices');

// Ajout du contrôle sur le profil Admin. Si l'utilisateur est Admin alors il voit tout
        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        $desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');

        $isAdmin = false;
        $groupName = $this->Session->read('Auth.User.Desktop.Profil');
        if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
            $isAdmin = true;
        }
        $isUserInit = false;
        if (in_array('Initiateur', $desktopName) || $groupName['name'] == 'Initiateur') {
            $isUserInit = true;
        }
        $this->set('isUserInit', $isUserInit);

        $isUserDisp = false;
        if (in_array('Aiguilleur', $desktopName) || $groupName['name'] == 'Aiguilleur') {
            $isUserDisp = true;
        }
        $this->set('isUserDisp', $isUserDisp);

        $isUserDoc = false;
        if (in_array('Documentaliste', $desktopName) || $groupName['name'] == 'Documentaliste') {
            $isUserDoc = true;
        }
        $this->set('isUserDoc', $isUserDoc);

        if (!empty($fluxMesServices)) {
            if (in_array($id, $fluxMesServices) && !in_array($desktopId, $this->SessionTools->getDesktopList())) {
                $fromMesServices = true;
            }
        }
        $fromHistorique = preg_match('#historique$#', @$_SERVER['HTTP_REFERER']);

        $viewParentMode = false;


        $fromMesCopies = false;
        $fluxMesCopies = $this->Session->read('Auth.User.Courrier.mesCopies');
//        debug($this->SessionTools->getDesktopList());
//        debug($id);
        if (!empty($fluxMesCopies)) {
            if (in_array($id, $fluxMesCopies) /* && !in_array($desktopId, $this->SessionTools->getDesktopList()) */) {
                $fromMesCopies = true;
            }
        }


        $fromBannetteCopy = $this->_inBannetteCopy($courrier['Courrier']['id']);
        $ariane = array(
            '<a href="/environnement/index/0">' . __d('menu', 'Courrier') . '</a>',
            '<a href="/environnement/index/0">' . __d('courrier', 'View Courrier') . '</a>',
            $courrier['Courrier']['name']
        );
        $parent = array();
        if (!empty($courrier['Courrier']['parent_id'])) {
            $qdParent = array(
                'fields' => array(
                    'Courrier.id',
                    'Courrier.name'
                ),
                'conditions' => array(
                    'Courrier.id' => $courrier['Courrier']['parent_id']
                ),
                'recursive' => -1
            );
            $parent = $this->Courrier->find('first', $qdParent);
            $ariane = array(
                '<a href="/environnement/index/0">' . __d('menu', 'Courrier') . '</a>',
                '<a href="/environnement/index/0">' . __d('courrier', 'View Courrier') . '</a>',
                '<a href="/courriers/historiqueGlobal/' . $parent['Courrier']['id'] . '">' . $parent['Courrier']['name'] . '</a>',
                $courrier['Courrier']['name']
            );
        }


// En cas de refus
        $fluxRefus = array();
        if (!empty($courrier['Courrier']['ref_ancien'])) {
            $qdFluxRefus = array(
                'fields' => array(
                    'Courrier.id',
                    'Courrier.name'
                ),
                'conditions' => array(
                    'Courrier.id' => $courrier['Courrier']['ref_ancien']
                ),
                'recursive' => -1
            );
            $fluxRefus = $this->Courrier->find('first', $qdFluxRefus);
        }
        $this->set('fluxRefus', $fluxRefus);

//

        $sortant = false;
        if (isset($courrier['Soustype']['entrant']) && $courrier['Soustype']['entrant'] == 0) {
            $sortant = true;
        }
        $this->set('sortant', $sortant);
        $this->set('parent', $parent);
        $this->set('ariane', $ariane);
        $this->set('courrier', $courrier);
        $this->set('mainDoc', $this->Courrier->getLightMainDoc($courrier['Courrier']['id']));

//on verifie si le courrier est un courrier cloturé
        $isClosed = $this->Courrier->isClosed($courrier['Courrier']['id']);
        $this->set('isClosed', $isClosed);

//on verifie si le courrier est un courrier refusé
        $this->set('isRefused', $this->Courrier->isRefused($courrier['Courrier']['id']));

//on determine si le courrier est dans un circuit
        $this->set('inCircuit', $this->Courrier->isInCircuit($courrier['Courrier']['id']));
//on determine si le courrier est a l etape de fin
        $endCircuit = false;
        if ($this->Session->read('Auth.User.Env.actual.name') != 'disp') {
            $etape = $this->Courrier->Traitement->whichIsNext($courrier['Courrier']['id']);
            if (!empty($etape) && $this->Courrier->Traitement->Circuit->Etape->estDerniereEtape($etape['Etape']['id'])) {
                $endCircuit = true;
            }
        }
        $this->set('endCircuit', $endCircuit);

        if (empty($desktopId)) {
            $desktopId = $this->Session->read('Auth.User.Desktop.id');
        }

        if (!$fromHistorique && !$fromMesServices /* && !$fromTask */) {
//On enleve les notifications concernant ce flux
            $this->Courrier->Notifieddesktop->Notification->setReadByDesktopCourrier($desktopId, $courrier['Courrier']['id']);
            $this->Courrier->Bancontenu->setRead($courrier['Courrier']['id'], $desktopId);
        }


        $desktopUserId = $this->Session->read('Auth.User.Courrier.Desktop.id');
        $desktopManagerId = $this->Courrier->Desktop->DesktopDesktopmanager->field('desktopmanager_id', array('DesktopDesktopmanager.desktop_id' => $desktopUserId));
        $isUserInCircuit = false;
        if ($this->Session->read('Auth.User.Env.actual.name') != 'disp') {
            $traitement = $this->Courrier->Traitement->find(
                    'first', array(
                'fields' => array('circuit_id'),
                'conditions' => array('Traitement.target_id' => $courrier['Courrier']['id']),
                'contain' => false
                    )
            );
            if (!empty($traitement)) {
                $circuitId = $traitement['Traitement']['circuit_id'];
                $isUserInCircuit = $this->Courrier->Traitement->Circuit->userInCircuit($desktopManagerId, $circuitId);
            }
        }
        $this->set(compact('isUserInCircuit'));

        $rights = $this->Session->read('Auth.Rights');

        if (!$rights['setInfos'] && !$rights['setMeta'] && !$rights['setTache'] && !$rights['setAffaire']) {
            $rights['edit'] = false;
        }

        if($isAdmin) {
            $rights['getFlowControls'] = true;
        }
		$this->set( 'isAdmin', $isAdmin );

        // SMS actif ou non
		$this->loadModel('Connecteur');
		$hasSms = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%LsMessage%',
					'Connecteur.use_sms' => false
				),
				'contain' => false
			)
		);

		if(!isset($hasSms) && empty($hasSms)){
			$rights['getSms'] = false;
		}

        $desktopsInBancontenu = $this->Courrier->Bancontenu->find(
            'all',
            array(
                'conditions' => array(
                    'Bancontenu.courrier_id' => $id,
                    'Bancontenu.desktop_id' => $desktopId,
                    'Bancontenu.etat' => 1
                ),
                'contain' => array(
                    'Desktop'
                ),
                'recursive' => -1,
                'order' => 'Bancontenu.created ASC'
            )
        );
        // Si le flux n'est pas dans la bannette des flux en copie du bureau ayant accès au flux, alors on autorise la modification
        if( !empty( $desktopsInBancontenu) ) {
            if( $desktopsInBancontenu[0]['Bancontenu']['bannette_id'] != BAN_COPY ) {
                $fromBannetteCopy = false;
                $fromMesCopies = false;
            }
        }

        $viewOnlyTabs = array(
            'getInfos' => ($rights['getInfos'] && !$rights['setInfos']) || $isClosed || $fromHistorique || $fromBannetteCopy || $fromTask,
            'getTache' => ($rights['getTache'] && !$rights['setTache']) || $isClosed || $fromHistorique || $fromBannetteCopy || $fromTask,
            'getMeta' => ($rights['getMeta'] && !$rights['setMeta']) || $isClosed || $fromHistorique || $fromBannetteCopy || $fromTask,
            'getAffaire' => ($rights['getAffaire'] && !$rights['setAffaire']) || $isClosed || $fromHistorique || $fromBannetteCopy || $fromTask
        );

        $this->Session->delete('Auth.User.Courrier.viewOnlyTabs');
        $this->Session->write('Auth.User.Courrier.viewOnlyTabs', $viewOnlyTabs);

        // Le circuit possède-t-il comme prochaine étape une délégation vers le parapheur ?
        $isDelegatedToParapheur = false;
        if ($this->Session->read('Auth.User.Env.actual.name') != 'disp') {
            $IdCircuit = Hash::get($this->viewVars, 'circuit.Circuit.id');
            $isDelegatedToParapheur = $this->Courrier->Traitement->Circuit->hasEtapeDelegation($IdCircuit);
        }

        $this->set('isDelegatedToParapheur', $isDelegatedToParapheur);

        $this->set('fromTask', $fromTask);
        $this->set('fromBannetteCopy', $fromBannetteCopy);
        $this->set('fromMesServices', $fromMesServices);
        $this->set('fromMesCopies', $fromMesCopies);
        $this->set('fromHistorique', $fromHistorique);
        $this->set('rights', $rights);
        $this->set('tab', $tab);
// debug($this->Session->read('Auth.User.Courrier'));
        $this->set('desktopidactuel', $this->Session->read('Auth.User.Courrier.Desktop.id'));
        $this->set('envactuel', $this->Session->read('Auth.User.Env.actual.name'));
//        debug( $this->Session->read( 'Auth.User.Env' ) );
        $viewOnly = $this->Session->read('Auth.User.Courrier.viewOnlyTabs.getInfos');
        $this->set('viewOnly', $viewOnly);
        if ($mode != null) {
            $this->set('mode', $mode);
        }

        $nbComment = 0;
        $ownerId = $this->SessionTools->getDesktopList();
        $nbComment = $this->Courrier->Comment->getNumberOfCommentByCourrierId($id, $ownerId);
        $this->set('nbComment', $nbComment);
        $this->set('sansContact', Configure::read('Sanscontact.id'));

        if (isset($mode) && $mode = 'edit') {
            $initMode = "edit";
        } else if ($fromMesCopies) {
            $initMode = "view";
        } else {
            if (!$viewOnly && $rights['edit'] && !$isClosed && !$fromHistorique && !$fromMesCopies && !$fromTask && !$fromMesServices) {
                $initMode = "edit";
            } else {
                $initMode = "view";
            }
        }
        if( $fromTask) {
			$initMode = "edit";
		}

		if( $isAdmin && $isClosed) {
			$initMode = "edit";
		}

        $this->set('initMode', $initMode);
        $conn = $this->Session->read('Auth.User.connName');
		$this->set('conn', $conn );
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));

        $this->set('desktopId', $desktopId );


        $this->loadModel('Journalevent');
        $datasSession = $this->Session->read('Auth.User');
        $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a consulté le flux ".$courrier['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
        $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrier);


		$connecteurSMSActif = false;
		if( Configure::read('Webservice.SMS') ) {
			$this->loadModel('Connecteur');
			$hasSmsActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%LsMessage%',
						'Connecteur.use_sms' => true
					),
					'contain' => false
				)
			);
			if( !empty( $hasSmsActif )) {
				$connecteurSMSActif = true;
			}
		}
        $this->set('connecteurSMSActif', $connecteurSMSActif );


		$nbMeta = 0;
		$nbMeta = $this->Courrier->Soustype->MetadonneeSoustype->find(
			'count',
			array(
				'conditions' => array(
					'MetadonneeSoustype.soustype_id' => $courrier['Soustype']['id'],
					'MetadonneeSoustype.obligatoire' => true
				),
				'contain' => array(
					'Metadonnee'
				),
				'order' => array(
					'Metadonnee.name DESC'
				)
			)
		);
		$this->set('nbMeta', $nbMeta);
    }

    /**
     * @logical-group Flux
     * @logical-group Edition d'un flux
     *
     * @logical-used-by Flux > Edition d'un flux
     * @userProfile User
     *
     * Edition d'un flux (uniquement pour la gestion des habilitations
     *
     * @access public
     * @return void
     */
//FIXME: supprimer cette fonction et trouver une méthode pour indiquer le droit d'édition dans la gestion des ACL
    public function edit(/* $id = null */) {
    }

    /**
     * Création d'un flux
     *
     * @logical-group Flux
     * @logical-group Création d'un flux
     * @user-profile User
     *
     * @param type $desktopId
     * @throws ForbiddenException
     * @return void
     */
    public function add($desktopId, $serviceId, $tab = null) {

        $aro = array('model' => 'Desktop', 'foreign_key' => $desktopId);
        $rights = array(
            'createInfos' => $this->Acl->check($aro, 'controllers/Courriers/createInfos'),
            'setInfos' => $this->Acl->check($aro, 'controllers/Courriers/setInfos'),
            'setMeta' => $this->Acl->check($aro, 'controllers/Courriers/setMeta'),
            'setTache' => $this->Acl->check($aro, 'controllers/Courriers/setTache'),
            'setAffaire' => $this->Acl->check($aro, 'controllers/Courriers/setAffaire'),
            'getArs' => true,
            'getRelements' => $this->Acl->check($aro, 'controllers/Courriers/getRelements'),
            'getDocuments' => $this->Acl->check($aro, 'controllers/Courriers/getDocuments'),
//			'setCircuit' => $this->Acl->check($aro, 'controllers/Courriers/setCircuit'),
            'getCircuit' => $this->Acl->check($aro, 'controllers/Courriers/getCircuit'),
            'getSms' => $this->Acl->check($aro, 'controllers/Courriers/getSms')
        );

        if (!$rights['createInfos'] /* || !$rights['setQualification'] */) {
            throw new ForbiddenException();
        }

        $this->set('fluxId', '');
        $this->set('rights', $rights);
        $this->Session->write('Auth.User.Courrier.Desktop.id', $desktopId);
        $this->Session->write('Auth.User.Courrier.Service.id', $serviceId);
        $fromMesServices = false;
        $this->set('fromMesServices', $fromMesServices);
        $this->set('tab', $tab);
        $this->set('sansContact', Configure::read('Sanscontact.id'));
    }

    /**
     * Accès à l'onglet "Informations" (ajax)
     *
     * @logical-group Flux
     * @logical-group Visualisation d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $id
     * @throws NotFoundException
     * @return void
     */
    public function getInfos($id = null) {
        $querydata = array(
            'conditions' => array(
                'Courrier.id' => $id
            ),
            'contain' => array(
                'Organisme',
                'Contact',
                'Soustype' => array(
                    'Type'
                ),
                'Affairesuivie',
                'Origineflux'
            )
        );

        if (Configure::read('Conf.SAERP')) {
            $querydataSAERP = array(
                'contain' => array(
                    'Marche',
                    'Ordreservice',
                    'Pliconsultatif'
                )
            );
            $querydata['contain'] = array_merge($querydata['contain'], $querydataSAERP['contain']);
        }

        $flux = $this->Courrier->find('first', $querydata);

        $contactinfosTreated = array();
        $organismeTreated = array();

		if( is_JSON($flux['Courrier']['objet'] ) ) {
			$flux['Courrier']['objet'] = json_decode($flux['Courrier']['objet'], true);
			$objetData = array();
			foreach ($flux['Courrier']['objet'] as $key => $val) {
				if( is_array($val) ) {
					foreach( $val as $fields => $values ) {
						$data[] = "\n".$fields.': '.$values;
					}
					$objetData[] = $key . ' : ' . implode( ' ', $data );
				}
				else {
					$objetData[] = $key . ' : ' . $val;
				}
			}
			$flux['Courrier']['objet'] = implode("\n", $objetData);
		}

		if (!empty($flux['Courrier']['organisme_id'])) {
            $organisme = $this->Courrier->Organisme->find('first', array(
                'fields' => array_merge(
                        $this->Courrier->Organisme->fields(), $this->Courrier->Organisme->Ban->fields(), $this->Courrier->Organisme->Bancommune->fields()
                ),
                'conditions' => array(
                    'Organisme.id' => $flux['Courrier']['organisme_id'],
                    'Organisme.active' => true
                ),
                'joins' => array(
                    $this->Courrier->Organisme->join('Ban', array('type' => 'LEFT OUTER')),
                    $this->Courrier->Organisme->join('Bancommune', array('type' => 'LEFT OUTER'))
                ),
                'contain' => false,
                'recursive' => -1
            ));
//debug($organisme);
            $return = array();
            if (!empty($organisme)) {
                $organismeTreated = $this->Courrier->Organisme->organismeTreated($organisme);
            }
        }

        if (!empty($flux['Courrier']['contact_id'])) {
            $contact = $this->Courrier->Contact->find('first', array(
                'fields' => array_merge(
                        $this->Courrier->Contact->fields(), $this->Courrier->Contact->Ban->fields(), $this->Courrier->Contact->Bancommune->fields()
                ),
                'conditions' => array(
                    'Contact.id' => $flux['Courrier']['contact_id'],
                    'Contact.active' => true
                ),
                'joins' => array(
                    $this->Courrier->Contact->join('Ban', array('type' => 'LEFT OUTER')),
                    $this->Courrier->Contact->join('Bancommune', array('type' => 'LEFT OUTER'))
                ),
                'contain' => false,
                'recursive' => -1
            ));

            if (!empty($contact)) {
                $contactinfosTreated = $this->Courrier->Contact->contactTreated($contact);
            }
        }

        $this->set('contactinfosTreated', $contactinfosTreated);
        $this->set('organismeTreated', $organismeTreated);


//debug($flux);
        if (empty($flux)) {
            throw new NotFoundException();
        }

        $this->set('types', $this->Courrier->Soustype->Type->find('list'));
        $this->set('soustypes', $this->Courrier->Soustype->find('all', array('recursive' => -1)));
        $this->set('flux', $flux);
        $this->set('viewOnly', $this->Session->read('Auth.User.Courrier.viewOnlyTabs.getInfos'));

        $users = $this->Courrier->Desktop->User->find(
            'all',
            array(
                'contain' => false,
                'recursive' => -1,
                'contain' => array(
                    'Desktop'
                ),
                'order' => 'User.nom ASC'
            )
        );
        foreach( $users as $user) {
            $allDesktops[$user['Desktop']['id']] = $user['User']['nom'].' '.$user['User']['prenom'];
        }
//        $allDesktops = $this->Courrier->Desktop->getDesktopsByService('all');
//        ksort($allDesktops);
//        debug($allDesktops);
        $this->set('allDesktops', $allDesktops);

        // Liste des acteurs
        $bannettes = $this->Courrier->getCycle($id);

        $this->set('bannettes', $bannettes['bannettes']);
        $this->_setOptions();
    }

    /**
     * Récupération du schéma du circuit emprunté par le flux
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $id
     * @throws NotFoundException
     * @return void
     */
    public function getCircuit($fluxId) {
//		if (empty($fluxId)) {
//			throw new BadMethodCallException();
//		}

        $qdCircuit = array(
            'fields' => array(
                'Circuit.id',
                'Circuit.nom'
            ),
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'contain' => false,
            'joins' => array(
                $this->Courrier->join($this->Courrier->Soustype->alias),
                $this->Courrier->Soustype->join($this->Courrier->Soustype->Circuit->alias, array('conditions' => array('Circuit.actif' => true))),
            )
        );
        $circuit = $this->Courrier->find('first', $qdCircuit);

        $etapes = null;
        $visas = null;
		$soustypes = [];
		$compiledVisas = [];
		$documents = [];

		$hasCheminement = true;
		$needConnecteur = false ;
        if (!empty($circuit['Circuit']['id'])) {
			//definition de l identifiant du circuit
			$circuitId = $circuit['Circuit']['id'];

			//recuperation de la structure initiale du circuit
			$qdEtapes = array(
				'fields' => array(
					'Etape.nom',
					'Etape.type',
					'Etape.ordre',
					'Etape.description',
					'Etape.type_document',
					'Etape.inforequired_type_document'
				),
				'contain' => array(
					'Composition.type_validation',
					'Composition.trigger_id',
					'Composition.soustype',
					'Composition.type_document',
					'Composition.inforequired_type_document',
					'Composition' => array(
						CAKEFLOW_TRIGGER_MODEL => array('fields' => CAKEFLOW_TRIGGER_FIELDS)
					)
				),
				'conditions' => array(
					'Etape.circuit_id' => $circuitId
				),
				'order' => 'Etape.ordre'
			);
			$etapes = $this->Courrier->Traitement->Circuit->Etape->find('all', $qdEtapes);

			foreach ($etapes as $kEtape => $compo) {
				$ids = Hash::extract( $compo, 'Composition.{n}.trigger_id' );
				if (in_array( $ids[0], ['-1', '-3'])) {
					$needConnecteur = true;
				}
			}

			//recuperation du traitement du circuit
			$qdVisas = array(
				'fields' => array(
					'Traitement.id',
					'Traitement.target_id',
					'Traitement.numero_traitement',
					'Traitement.treated',
					'Visa.etape_type',
					'Visa.etape_nom',
					'Visa.etape_id',
					'Visa.trigger_id',
					'Visa.action',
					'Visa.soustype',
					'Visa.numero_traitement',
					'Visa.type_document',
					'Visa.inforequired_type_document',
					'Etape.description',
					'Etape.id',
					'Etape.type_document',
					'Etape.inforequired_type_document',
					CAKEFLOW_TRIGGER_MODEL . "." . CAKEFLOW_TRIGGER_FIELDS
				),
				'contain' => false,
				'joins' => array(
					array(
						'table' => 'wkf_visas',
						'alias' => 'Visa',
						'type' => 'right',
						'conditions' => array(
							'Visa.traitement_id = Traitement.id'
						)
					),
					array(
						'table' => 'wkf_etapes',
						'alias' => 'Etape',
						'type' => 'left',
						'conditions' => array(
							'Etape.id = Visa.etape_id'
						)
					),
					$this->Courrier->Traitement->Circuit->Traitement->Visa->join(CAKEFLOW_TRIGGER_MODEL)
				),
				'conditions' => array(
					'Traitement.target_id' => $fluxId
				),
				'order' => array(
					'Traitement.target_id',
					'Visa.numero_traitement',
					'Visa.id'
				)
			);
			$visas = $this->Courrier->Traitement->find('all', $qdVisas);


			$compiledVisas = array();
			$compiledVisasRef = array();
			foreach ($visas as $kVisa => $visa) {

				if( in_array( $visa['Visa']['trigger_id'], ['-1', '-3'] ) ) {
					$needConnecteur = true;
				}
				if (in_array($visa['Visa']['numero_traitement'], $compiledVisasRef)) {
					$key = array_search($visa['Visa']['numero_traitement'], $compiledVisasRef);
					$compiledVisas[$key]['Composition'][] = array(
						CAKEFLOW_TRIGGER_MODEL => array(
							'name' => $visa[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS],
							'action' => $visa['Visa']['action'],
							'trigger_id' => $visa['Visa']['trigger_id'],
							'soustype' => $visa['Visa']['soustype'],
							'type_document' => $visa['Visa']['type_document'],
							'inforequired_type_document' => $visa['Visa']['inforequired_type_document']
						)
					);
				} else {
					$compiledVisasRef[$kVisa] = $visa['Visa']['numero_traitement'];
					$visa['Composition'] = array(
						array(
							CAKEFLOW_TRIGGER_MODEL => array(
								'name' => $visa[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS],
								'action' => $visa['Visa']['action'],
								'trigger_id' => $visa['Visa']['trigger_id'],
								'soustype' => $visa['Visa']['soustype'],
								'type_document' => $visa['Visa']['type_document'],
								'inforequired_type_document' => $visa['Visa']['inforequired_type_document']
							)
						)
					);
					$compiledVisas[$kVisa] = $visa;
				}
			}

			// On masque les descriptions des étapes du circuit ajoutées par la fonction d'envoi avec ou sans AR.
			if (count($visas) >= 2) {
				for ($v = 1; $v <= count($visas) - 1; $v++) {
					// si le flux est issu d'une étape précédente du type IL ou IP (envoi avec ou sans AR)
					if (isset($compiledVisas[$v]) && isset($compiledVisas[$v - 1])) {
						if (in_array($compiledVisas[$v]['Visa']['action'], array('RI', 'OK')) && in_array($compiledVisas[$v - 1]['Visa']['action'], array('IL', 'IP'))) {
							// on ne prend pas de description pour cette novuelle étape
							$compiledVisas[$v]['Etape']['description'] = '';
						}
					}

					// si le flux est issu d'une étape avec AR
					if ($v == count($visas) - 2) {
						// si le flux est issu d'une étape précédente du type IL ou IP (envoi avec ou sans AR)
						if (isset($compiledVisas[$v]) && isset($compiledVisas[$v - 1])) {
							if (in_array($compiledVisas[$v]['Visa']['action'], array('RI', 'OK')) && in_array($compiledVisas[$v - 1]['Visa']['action'], array('RI', 'OK'))) {
								// on ne prend pas de $typeParapheurdescription pour cette novuelle étape
								$compiledVisas[$v]['Etape']['description'] = '';
							}
						}
					}
				}
			}


			if( $needConnecteur ) {
				$this->loadModel('Connecteur');
				$hasParapheurActif = $this->Connecteur->find(
					'first',
					array(
						'conditions' => array(
							'Connecteur.name ILIKE' => '%Parapheur%',
							'Connecteur.use_signature' => true
						),
						'contain' => false
					)
				);
				if( !empty($hasParapheurActif) && $hasParapheurActif['Connecteur']['use_signature'] && $hasParapheurActif['Connecteur']['signature_protocol'] != 'PASTELL') {
					if (empty($this->Session->read('Iparapheur.listSoustype'))) {
						$soustypes = $this->Courrier->Traitement->Circuit->Etape->listeSousTypesParapheur($hasParapheurActif);
						$soustypes = $soustypes['soustype'];
					}
					else {
						$soustypes = $this->Session->read( 'Iparapheur.listSoustype' );
					}
				} else {
					$soustypes = array();
				}
				if (empty($soustypes)) {
					$soustypes = array();
				}

				$courrier = $this->Courrier->find('first', array(
					'fields' => array_merge(
						$this->Courrier->Soustype->fields()
					),
					'conditions' => array(
						'Courrier.id' => $fluxId
					),
					'joins' => array(
						$this->Courrier->join('Soustype')
					),
					'contain' => false
				));


				$hasPastellActif = $this->Connecteur->find(
					'first',
					array(
						'conditions' => array(
							'Connecteur.name ILIKE' => '%Pastell%',
							'Connecteur.use_pastell' => true
						),
						'contain' => false
					)
				);
				$this->set('hasPastellActif', $hasPastellActif);
				$documents = array();
				if( !empty($hasPastellActif) && $hasPastellActif['Connecteur']['use_pastell'] ) {
					$documents = $this->Courrier->getTypeDocNamePastell( $courrier );

					$cheminement = $courrier['Soustype']['envoi_signature'] + $courrier['Soustype']['envoi_mailsec'] + $courrier['Soustype']['envoi_ged'] + $courrier['Soustype']['envoi_sae'];
					if( $cheminement == 0 ) {
						$hasCheminement = false;
					}
				}
			}
		}
        $this->set( 'soustypes', $soustypes);

		$this->set('hasCheminement', $hasCheminement);
        $this->set('circuit', $circuit);
        $this->set('etapes', $etapes);
        $this->set('visas', $compiledVisas);
        $this->set('viewOnly', $this->Session->read('Auth.User.Courrier.viewOnlyTabs.getCircuit'));

		$this->set('documents', $documents);
    }

    /**
     * Accès à l'onglet "Méta-données" (ajax)
     *
     * @logical-group Flux
     * @logical-group Visualisation d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux

     * @return void
     */
    public function getMeta($fluxId) {
        $this->set('metas', $this->DataAuthorized->getAuthMetadonnees(array('fluxId' => $fluxId)));
        $this->set('viewOnly', $this->Session->read('Auth.User.Courrier.viewOnlyTabs.getMeta'));
    }

    /**
     * Accès à l'onglet "Tâches" (ajax)
     *
     * @logical-group Flux
     * @logical-group Visualisation d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du flux

     * @throws NotFoundException
     * @return void
     */
    public function getTache($fluxId) {
        $qd = array(
            'conditions' => array(
                'Courrier.id' => $fluxId
            )
        );
        $courrier = $this->Courrier->find('first', $qd);
        if (empty($courrier)) {
            throw new NotFoundException();
        }
        $this->set('canAddTask', !empty($courrier['Courrier']['soustype_id']));

        $querydata = array(
			'contain' => array(
				'Desktop' => array(
					'User'
				)
			),
            'conditions' => array(
                "Tache.courrier_id" => $fluxId
            ),
            'order' => array(
                'Tache.created'
            )
        );
        $taches_tmp = $this->Courrier->Tache->find('all', $querydata);

        $taches = array();
//on verifie que l utilisateur fasse partie des personnes pouvant valider la tache
        $userDesktops = $this->SessionTools->getDesktopList();
        foreach ($taches_tmp as $tache) {
//calcul du retard
            $retard = false;
            if ($tache['Tache']['statut'] != 2) {
                $tmpTime = preg_split('# #', $tache['Tache']['created']);
                $tmpTime1 = preg_split('#-#', $tmpTime[0]);
                if ($tache['Tache']['delai_unite'] == 0) {
                    $endTime = mktime(0, 0, 0, $tmpTime1[1], $tmpTime1[2] + $tache['Tache']['delai_nb'], $tmpTime1[0]);
                } else if ($tache['Tache']['delai_unite'] == 1) {
                    $endTime = mktime(0, 0, 0, $tmpTime1[1], $tmpTime1[2] + (7 * $tache['Tache']['delai_nb']), $tmpTime1[0]);
                } else if ($tache['Tache']['delai_unite'] == 2) {
                    $endTime = mktime(0, 0, 0, $tmpTime1[1] + $tache['Tache']['delai_nb'], $tmpTime1[2], $tmpTime1[0]);
                }
                if ($endTime < mktime()) {
                    $retard = true;
                }
            }
            $tache['retard'] = $retard;

            $tache['canGrab'] = false;
            $tache['canRelease'] = false;
            $tache['canValid'] = false;

            if ($tache['Tache']['statut'] == 0) {
                if (count($tache['Desktop']) == 1) {
                    if (in_array($tache['Desktop'][0]['id'], $userDesktops)) {
                        $tache['canValid'] = true;
                    }
                } else if ( count($tache['Desktop']) > 1 ) {
                    foreach ($tache['Desktop'] as $dktp) {
                        if (in_array($dktp['id'], $userDesktops)) {
                            $tache['canGrab'] = true;
                        }
                    }
                }
            } else if ($tache['Tache']['statut'] == 1) {
                if (in_array($tache['Tache']['act_desktop_id'], $userDesktops)) {
                    $tache['canValid'] = true;
                    $tache['canRelease'] = true;
                }
            }

            if ($retard) {
                array_unshift($taches, $tache);
            } else {
                array_push($taches, $tache);
            }
        }
        $this->set('taches', $taches);
        $this->set('userDesktops', $userDesktops);
        $this->set('viewOnly', $this->Session->read('Auth.User.Courrier.viewOnlyTabs.getTache'));

		$delaiTranslate = [
			'0' => 'jour(s)',
			'1' => 'semaine(s)',
			'2' => 'mois'
		];
		$this->set('delaiTranslate', $delaiTranslate);
    }

    /**
     * Accès à l'onglet "Dossiers/Affaires" (ajax)
     *
     * @logical-group Flux
     * @logical-group Visualisation d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux

     * @throws NotFoundException
     * @return void
     */
    public function getAffaire($fluxId) {
        $querydata = array(
            'contain' => array(
                'Affaire' => array(
                    'Dossier'
                )
            ),
            'conditions' => array(
                'Courrier.id' => $fluxId
            )
        );
        $courrier = $this->Courrier->find('first', $querydata);

        if (!empty($courrier)) {
            $this->set('courrier', $courrier);
            $this->set('viewOnly', $this->Session->read('Auth.User.Courrier.viewOnlyTabs.getAffaire'));
        } else {
            throw new NotFoundException();
        }
    }

    /**
     * Accès à l'onglet "Informations" (ajax)
     *
     * @logical-group Flux
     * @logical-group Création d'un flux
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function createInfos() {
		$userId = $this->Session->read('Auth.User.id');
        if (!empty($this->request->data)) {
            $this->Courrier->begin();

            $valid = false;

            $desktopCreatorId = $this->Session->read('Auth.User.Courrier.Desktop.id');

            $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
            $groupId = Hash::extract($secondaryDesktops, '{n}.Profil.id');
            $desktopId = Hash::extract($secondaryDesktops, '{n}.id');
            // Si le bureau secondaire est de type Valideur Editeur, Valideur ou Documentaliste
            if( isset($groupId) && !empty($groupId) ) {
                if ( in_array( $groupId, array( 3 ) ) ) {
                    $desktopCreatorId = $desktopId;
                }
            }

            $this->request->data['Courrier']['user_creator_id'] = $this->Session->read('Auth.User.id');
            $this->request->data['Courrier']['desktop_creator_id'] = $desktopCreatorId;
			if( !empty($this->Session->read('Auth.User.Courrier.Service.id') ) ) {
				$this->request->data['Courrier']['service_creator_id'] = $this->Session->read('Auth.User.Courrier.Service.id');
			}
			else {
				$this->request->data['Courrier']['service_creator_id'] = $this->Session->read('Auth.User.Desktop.Services.0.id');
			}


            if (!empty($this->request->data['Courrier']['objet'])) {
                $this->request->data['Courrier']['objet'] = str_replace(array("<p>", "</p>", "<br />"), "\t", quoted_printable_decode($this->request->data['Courrier']['objet']));
                $this->request->data['Courrier']['objet'] = str_replace("/", "-", $this->request->data['Courrier']['objet']);
                $this->request->data['Courrier']['objet'] = str_replace("'", "&#39;", $this->request->data['Courrier']['objet']);

            }

if( in_array( $this->request->data['Courrier']['desktop_creator_id'], array( 95, 96) ) ) {
      $this->log( 'Mme Jolly : ', LOG_ERROR );
      $this->log( $this->Session->read('Auth.User.Courrier'), LOG_ERROR );
}

            if (empty($this->request->data['Courrier']['reference'])) {
                $this->request->data['Courrier']['reference'] = $this->Courrier->genRef2($this->request->data);
            }
            if (!empty($this->request->data['Courrier']['soustype_id'])) {
                $this->request->data['Courrier']['direction'] = $this->Courrier->Soustype->field('Soustype.entrant', array('Soustype.id' => $this->request->data['Courrier']['soustype_id']));
            }
            if (!isset($this->request->data['Courrier']['contact_id']) || empty($this->request->data['Courrier']['contact_id'])) {
                $this->request->data['Courrier']['contact_id'] = Configure::read('Sanscontact.id');
            }
//si champ datereception/date est vide(version 2.0)
            if (empty($this->request->data['Courrier']['datereception'])) {
                $this->request->data['Courrier']['datereception'] = date("Y-m-d");
            }

			$this->request->data['Courrier']['date'] = date("Y-m-d");

///
            if (!empty($this->request->data['Courrier']['ContactId'])) {
                $org = $this->Courrier->Organisme->find('first', array(
                    'conditions' => array(
                        'Organisme.id' => $this->request->data['Courrier']['ContactId'],
                        'Organisme.name' => $this->request->data['Courrier']['ContactName'],
                        'Organisme.active' => true
                    ),
                    'recursive' => -1
                ));
                if (!empty($org)) {
                    $this->request->data['Courrier']['typecontact'] = 'organisme';
                    $this->request->data['Courrier']['organisme_id'] = $this->request->data['Courrier']['ContactId'];
                } else {
                    $this->request->data['Courrier']['typecontact'] = 'citoyen';
                }
            }

            // Ajouté pour la ville d'Angoulême
            if( in_array( $this->request->data['Courrier']['desktop_creator_id'], array( 95, 96) ) ) {
                $this->log( 'Mme Jolly : ', LOG_ERROR );
                $this->log( $this->request->data, LOG_ERROR );
            }

            $this->Courrier->create($this->request->data);
            $saved = $this->Courrier->save();

            // on stocke qui à fait quoi quand

            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $msg = "L'utilisateur ". $this->Session->read('Auth.User.username')." (".$this->request->data['Courrier']['desktop_creator_id'].") a créé le flux ".$this->request->data['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $this->request->data);

            if (!empty($saved)) {
                $newCourrierId = $this->Courrier->id;

                if ($this->Courrier->Bancontenu->add($this->Session->read('Auth.User.Courrier.Desktop.id'), BAN_DOCS, $newCourrierId, true, $userId)) {
                    $valid = true;
                }
            }

            if ($valid) {
                $this->Jsonmsg->valid(__d('courrier', 'Courrier.create_ok'));
                $this->Jsonmsg->json['newCourrierId'] = $newCourrierId;
                $this->Jsonmsg->json['debug'] = $saved;
                $this->Courrier->commit();

            } else {
                $this->Courrier->rollback();
            }

            $this->Jsonmsg->send();
        } else {
            $users = $this->Courrier->Desktop->User->find(
                'all',
                array(
                    'contain' => false,
                    'recursive' => -1,
                    'contain' => array(
                        'Desktop'
                    ),
                    'conditions' => array('User.active' => true),
                    'order' => 'User.nom ASC'
                )
            );
            foreach( $users as $user) {
                $allDesktops[$user['Desktop']['id']] = $user['User']['nom'].' '.$user['User']['prenom'];
            }

            $this->set('allDesktops', $allDesktops);
            $this->set('isInCircuit', false);
            if (Configure::read('Use.AuthRights') && !Configure::read('Conf.SAERP')) {
                $this->set('types', $this->Session->read('Auth.TypesActif') );
            } else {
                $types = $this->Courrier->Soustype->Type->find('all', array('contain' => array('Soustype' => array('conditions' => array('Soustype.active' => true), 'order' => 'Soustype.name ASC')), 'order' => array('Type.name ASC'), 'conditions' => array('Type.active' => 1), 'recursive' => -1));
                $newTypes = Hash::map($types, "{n}", array($this, '__getType'));
                $newSoustypes = Hash::map($types, "{n}", array($this, '__getSousType'));
                foreach ($newTypes as $key => $value) {
                    $typeOptions[$value['id']] = $value['name'];
                }
                foreach ($newSoustypes as $key => $value) {
                    if (isset($value) && !empty($value)) {
                        $soustype = array();
                        foreach ($value as $keySoustype => $valueSoustype) {
                            $soustype[$valueSoustype['id']] = $valueSoustype['name'];
                        }
                        $soustypeOptions[$value[0]['type_id']] = $soustype;
                    }
                }
                $this->set('typeOptions', $typeOptions);
                $this->set('soustypeOptions', $soustypeOptions);
            }

            $this->set('contactname', !empty($this->request->data['Contact']) ? $this->request->data['Contact']['name'] : "");


        }


		$this->set('organismOptions', array( Configure::read('Sansorganisme.id') => 'Sans organisme') );
		$this->set('contactOptions', array( Configure::read('Sansorganisme.id') => array( Configure::read('Sanscontact.id') => ' Sans contact') ) );

        $conn = $this->Session->read('Auth.User.connName');
		$this->set('conn', $conn );
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
        $this->_setOptions();

		// Fonction permettant de savoir si on affiche les actions d'ajout/édition des contacts/organismes ou pas
		$this->set('right_action_carnet_adresse_flux', $this->Session->read('Auth.ContactInFluxAllowed'));
    }

    /**
     * Accès à l'onglet "Informations" (ajax)
     *
     * @logical-group Flux
     * @logical-group Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du flux
     * @return void
     */
    public function setInfos($id = null) {
		//enregistrement des données
        if (!empty($this->request->data)) {
            $msg = 'Onglet ' . __d('courrier', 'Informations') . ' : ';
            $this->Jsonmsg->init($msg . __d('default', 'save.error'));

            if (empty($this->request->data['Courrier']['reference'])) {
                $this->request->data['Courrier']['reference'] = $this->Courrier->genRef2($this->request->data);
            }
            if (!empty($this->request->data['Courrier']['soustype_id'])) {
                $this->request->data['Courrier']['direction'] = $this->Courrier->Soustype->field('Soustype.entrant', array('Soustype.id' => $this->request->data['Courrier']['soustype_id']));
            }
            // si l'admin modifie le bancontenu pour repositionner le flux
            if( !empty( $this->request->data['Bancontenu']['desktop_id'] ) ) {
				$this->Courrier->repositionneFlux( $this->request->data['Courrier']['id'], $this->request->data['Bancontenu']['desktop_id']);
            }
			if( !empty($this->request->data['Courrier']['id']) ) {
				$qd = array(
					'conditions' => array(
						'Courrier.id' => $this->request->data['Courrier']['id']
					),
					'recursive' => -1,
					'contain' => false
				);
				$courrier = $this->Courrier->find('first', $qd);
				if( !empty( $courrier ) ) {
					if (empty($this->request->data['Courrier']['user_creator_id']) && !empty($courrier['Courrier']['user_creator_id'])) {
						$this->request->data['Courrier']['user_creator_id'] = $courrier['Courrier']['user_creator_id'];
					}

					if (empty($this->request->data['Courrier']['desktop_creator_id']) && !empty($courrier['Courrier']['desktop_creator_id'])) {
						$this->request->data['Courrier']['desktop_creator_id'] = $courrier['Courrier']['desktop_creator_id'];
					}
					if (empty($this->request->data['Courrier']['service_creator_id']) && !empty($courrier['Courrier']['service_creator_id'])) {
						$this->request->data['Courrier']['service_creator_id'] = $courrier['Courrier']['service_creator_id'];
					}
				}
			}




            // Pour ne pas autoriser la suppression d'un flux
            if( empty( $this->request->data['Courrier']['user_creator_id'] ) ) {
                $this->request->data['Courrier']['user_creator_id'] = $this->Session->read('Auth.User.id');
            }
			if( empty( $this->request->data['Courrier']['desktop_creator_id']) ) {
				if( $this->Session->read('Auth.User.Desktop.Profil.id') == INIT_GID ) {
					$this->request->data['Courrier']['desktop_creator_id'] = $this->Session->read('Auth.User.Desktop.id');
				}
				else if( in_array( INIT_GID, Hash::extract( $this->Session->read('Auth.User.SecondaryDesktops'), '{n}.Profil.id' ) ) ) {
					$this->request->data['Courrier']['desktop_creator_id'] = $this->Session->read('Auth.User.SecondaryDesktops')[0]['id'];
				}
			}
			if( empty( $this->request->data['Courrier']['service_creator_id']) ) {
				if( !empty($this->Session->read('Auth.User.Courrier.Service.id') ) ) {
					$this->request->data['Courrier']['service_creator_id'] = $this->Session->read('Auth.User.Courrier.Service.id');
				}
				else {
					$this->request->data['Courrier']['service_creator_id'] = $this->Session->read('Auth.User.Desktop.Services.0.id');
				}
			}

            $this->Courrier->create($this->request->data);
            if ($this->Courrier->save()) {

                // on stocke qui à fait quoi quand

                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username')." a modifié le flux ".$this->request->data['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $this->request->data);

                // En cas de modification d'une information sur le flux, on met à jour la date de modification de la table bancontenus pour le profil étant intervenu
                $this->Courrier->Bancontenu->updateAll(
                    array(
                        'Bancontenu.modified' => "'".date( 'Y-m-d H:i:s')."'"
                    ),
                    array(
                        'Bancontenu.courrier_id' => $this->request->data['Courrier']['id'],
                        'Bancontenu.desktop_id' =>  $this->Session->read('Auth.User.Courrier.Desktop.id')
                    )
                );

                $this->Jsonmsg->valid($msg . __d('default', 'save.ok'));
            }
            $isInCircuit = $this->Courrier->isInCircuit($this->request->data['Courrier']['id']);

            $this->Jsonmsg->json['loadContext'] = 'reload';
            $this->Jsonmsg->json['newCourrierId'] = $id;
            if( !$isInCircuit ) {
				$delaiTranslate = [
					'0' => 'jour(s)',
					'1' => 'semaine(s)',
					'2' => 'mois'
				];

				$sens= [
					0 => 'Sortant',
					1 => 'Entrant',
					2 => 'Interne'
				];

				$valueDirection = null;
				if( !empty($this->request->data['Courrier']['soustype_id']) ) {
					$direction = $this->Courrier->Soustype->field('Soustype.entrant', array('Soustype.id' => $this->request->data['Courrier']['soustype_id']));
					$valueDirection = $sens[$direction];
				}
                $this->Jsonmsg->json['newCourrierDelaiNb'] = $this->Courrier->Soustype->field('Soustype.delai_nb', array('Soustype.id' => $this->request->data['Courrier']['soustype_id']));
                $this->Jsonmsg->json['newCourrierDelaiUnite'] = $this->Courrier->Soustype->field('Soustype.delai_unite', array('Soustype.id' => $this->request->data['Courrier']['soustype_id']));
                $this->Jsonmsg->json['newCourrierDelaiUniteTranslated'] = $delaiTranslate[$this->Courrier->Soustype->field('Soustype.delai_unite', array('Soustype.id' => $this->request->data['Courrier']['soustype_id']))];
                $this->Jsonmsg->json['newCourrierSoustypeSens'] = !empty($valueDirection) ? $valueDirection :  null;
            }
            else {
                $this->Jsonmsg->json['newCourrierSoustypeSens'] = 'empty';
            }
            $this->Jsonmsg->send();
        } else {
            $qd = array(
                'fields' => array_merge(
                    $this->Courrier->fields(),
                    $this->Courrier->Organisme->fields(),
                    $this->Courrier->Contact->fields(),
                    $this->Courrier->Soustype->fields(),
                    $this->Courrier->Soustype->Type->fields()
                ),
                'conditions' => array(
                    'Courrier.id' => $id
                ),
                'joins' => array(
                    $this->Courrier->join('Organisme', array('type' => 'LEFT OUTER')),
                    $this->Courrier->join('Contact', array('type' => 'LEFT OUTER')),
                    $this->Courrier->join('Soustype', array('type' => 'LEFT OUTER')),
                    $this->Courrier->Soustype->join('Type', array('type' => 'LEFT OUTER'))
                ),
                'recursive' => -1,
                'contain' => false
            );


            $this->request->data = $this->Courrier->find('first', $qd);

			if( strpos( $this->request->data['Courrier']['name'] , '"') != 0 ) {
				$this->request->data['Courrier']['name'] = str_replace('"', "'", $this->request->data['Courrier']['name']);
			}


            $contactinfosTreated = array();
            $organismeTreated = array();

            if (!empty($this->request->data['Courrier']['organisme_id'])) {
                $organisme = $this->Courrier->Organisme->find(
                    'first',
                    array(
                        'fields' => array_merge(
                                $this->Courrier->Organisme->fields(), $this->Courrier->Organisme->Ban->fields(), $this->Courrier->Organisme->Bancommune->fields()
                        ),
                        'conditions' => array(
                            'Organisme.id' => $this->request->data['Courrier']['organisme_id'],
                            'Organisme.active' => true
                        ),
                        'joins' => array(
                            $this->Courrier->Organisme->join('Ban', array('type' => 'LEFT OUTER')),
                            $this->Courrier->Organisme->join('Bancommune', array('type' => 'LEFT OUTER'))
                        ),
                        'contain' => false,
                        'recursive' => -1
                    )
                );
//debug($organisme);
                $return = array();
                if (!empty($organisme)) {
                    $organismeTreated = $this->Courrier->Organisme->organismeTreated($organisme);
                }
            }
            else {
                $organismeTreated['Organisme']["Nom de l'organisme"] = "Sans organisme";
            }

            if (!empty($this->request->data['Courrier']['contact_id'])) {
                $contact = $this->Courrier->Contact->find(
                    'first',
                    array(
                        'fields' => array_merge(
                                $this->Courrier->Contact->fields(), $this->Courrier->Contact->Ban->fields(), $this->Courrier->Contact->Bancommune->fields()
                        ),
                        'conditions' => array(
                            'Contact.id' => $this->request->data['Courrier']['contact_id'],
                            'Contact.active' => true
                        ),
                        'joins' => array(
                            $this->Courrier->Contact->join('Ban', array('type' => 'LEFT OUTER')),
                            $this->Courrier->Contact->join('Bancommune', array('type' => 'LEFT OUTER'))
                        ),
                        'contain' => false,
                        'recursive' => -1
                    )
                );
                if (!empty($contact)) {
                    $contactinfosTreated = $this->Courrier->Contact->contactTreated($contact);
                }
            }
            else {
                $contactinfosTreated['Contactinfo']['name'] = array( Configure::read('Sanscontact.id') => "Sans contact" );
            }

            $this->set('organismeid', !empty($this->request->data['Courrier']['organisme_id']) ? $this->request->data['Courrier']['organisme_id'] : "");
            $this->set('contactid', !empty($this->request->data['Courrier']['contact_id']) ? $this->request->data['Courrier']['contact_id'] : "");

            $this->set('soustypeid', !empty($this->request->data['Courrier']['soustype_id']) ? $this->request->data['Courrier']['soustype_id'] : "");

            if (!empty($this->request->data['Courrier']['soustype_id'])) {
                $type = $this->Courrier->Soustype->find(
                        'first', array(
                    'conditions' => array(
                        'Soustype.id' => $this->request->data['Courrier']['soustype_id']
                    ),
                    'contain' => false
                        )
                );
                $typeId = $type['Soustype']['type_id'];
                $this->set('typeId', $typeId);
            }

            $this->set('contactinfos', $contactinfosTreated);
            $this->set('organismeinfos', $organismeTreated);

            $isInCircuit = $this->Courrier->isInCircuit($this->request->data['Courrier']['id']);
            $this->set('isInCircuit', $isInCircuit);
            // Si on n'est aps en mode SAERP et que le flux n'est pas  encore inséré
            if (Configure::read('Use.AuthRights') && !Configure::read('Conf.SAERP') && !$isInCircuit) {
                $this->set('typeOptions', $this->Session->read('Auth.TypeOptions'));
                $this->set('soustypeOptions', $this->Session->read('Auth.SoustypeOptions'));
            }
            // sinon le flux est dans un circuit alors on cherche les infos deuis le sous-type déjà associé au flux
            else {
                $soustype = $this->Courrier->Soustype->find(
                    'first',
                    array(
                        'fields' => array_merge(
                            $this->Courrier->Soustype->fields(),
                            $this->Courrier->Soustype->Type->fields()
                        ),
                        'conditions' => array(
                            'Soustype.id' => $this->request->data['Courrier']['soustype_id']
                        ),
                        'joins' => array(
                            $this->Courrier->Soustype->join('Type')
                        ),
                        'recursive' => -1,
                        'contain' => false
                    )
                );
//debug($soustype);
                $typeOptions[$soustype['Soustype']['type_id']] = $soustype['Type']['name'];
                $soustypeSelected[$soustype['Soustype']['id']] = $soustype['Soustype']['name'];
                $soustypeOptions[$soustype['Soustype']['type_id']] = $soustypeSelected;
                $this->set('typeOptions', $typeOptions);
                $this->set('soustypeOptions', $soustypeOptions);

            }

            $this->set('allDesktops', $this->Session->read('Auth.AllDesktops') );
        }
        $this->set('envactuel', $this->Session->read('Auth.User.Env.actual.name'));

        // Select pour les organismes et les contacts

        // Cycle de vie du flux : Liste des acteurs
        $bannettes = $this->Courrier->getCycle($id);
        $this->set('bannettes', $bannettes['bannettes']);

        $this->request->data['Courrier']['date'] = date("d/m/Y", strtotime($this->request->data['Courrier']['date']));
        $this->request->data['Courrier']['datereception'] = date("d/m/Y", strtotime($this->request->data['Courrier']['datereception']));

        // Info sur l'étape en cours
        $this->set('descriptionEtape', $bannettes['descriptionEtape']);
        $conn = $this->Session->read('Auth.User.connName');
		$this->set('conn', $conn );
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));

        $profilActuel = $this->Session->read('Auth.User.Env.actual.profil_id');
        $profilAutres = $this->Session->read('Auth.User.Env.secondary');
        $valuesProfils = Hash::extract( $profilAutres, '{n}');
        $isInit = 0;
        if( in_array('init', $valuesProfils ) || $profilActuel == INIT_GID ) {
            $isInit = 1;
        }
        $this->set('isInit', $isInit );

        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        $desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
        $isAdmin = false;
        $groupName = $this->Session->read('Auth.User.Desktop.Profil');
        if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
            $isAdmin = true;
            $desktopsAdmin = $this->Courrier->User->Desktop->find(
                'list',
                array(
                    'conditions' => array(
                        'Desktop.active' => true,
                        'Desktop.id <>' => -1
                    ),
                    'order' => array(
                        'Desktop.name'
                    ),
					'contain' => false
                )
            );
            $this->set( 'desktopsAdmin', $desktopsAdmin);
        }
        $this->set( 'isAdmin', $isAdmin);


        $fromDisp = 0;
        if( $this->Session->read('Auth.User.Env.actual.profil_id') == DISP_GID ) {
            $fromDisp = 1;
        }
        $this->set('fromDisp', $fromDisp );

        // Motif du refus
        $motifRefus = '';
        if( !empty($this->request->data['Courrier']['ref_ancien']) ) {
            $motifRefus = !empty( $this->request->data['Courrier']['motifrefus'] ) ? $this->request->data['Courrier']['motifrefus'] :  $this->request->data['Courrier']['parapheur_commentaire'];
        }
        $this->set( compact('motifRefus'));

         if( strpos( $this->request->data['Courrier']['objet'], '<!DOCTYPE') !== false || strpos( $this->request->data['Courrier']['objet'], '<html>') !== false || strpos( $this->request->data['Courrier']['objet'], '--_000') !== false ) {
            $this->request->data['Courrier']['objet'] = strip_tags( $this->request->data['Courrier']['objet'], "<html>" );
            $substring = substr($this->request->data['Courrier']['objet'] ,strpos($this->request->data['Courrier']['objet'] ,"<style"),strpos($this->request->data['Courrier']['objet'] ,"</style>"));
            $this->request->data['Courrier']['objet'] = str_replace($substring,"",$this->request->data['Courrier']['objet']);
            $this->request->data['Courrier']['objet'] = trim($this->request->data['Courrier']['objet']);
            $this->request->data['Courrier']['objet'] = str_replace("'", "&#39;", $this->request->data['Courrier']['objet']);
        }

         // CAs où l'objet a été créé depuis DS
         if(is_JSON( $this->request->data['Courrier']['objet'] ) ) {
			 $this->request->data['Courrier']['objet'] = json_decode($this->request->data['Courrier']['objet'], true);
			 $objetData = array();
			 foreach ($this->request->data['Courrier']['objet'] as $key => $val) {
			 	if( is_array($val) ) {
			 		foreach( $val as $fields => $values ) {
			 			$data[] = "\n".$fields.': '.$values;
					}
					$objetData[] = $key . ' : ' . implode( ' ', $data );
				}
			 	else {
					$objetData[] = $key . ' : ' . $val;
				}
			 }
			 $this->request->data['Courrier']['objet'] = implode("\n", $objetData);
		 }

        // LAD / RAD : conversion + OCR
		$hasOcrData = false;
		$ocrData = null;
		$mainDoc = $this->Courrier->getLightMainDoc($id);
		if (!empty($mainDoc)) {
			$ocrData = $mainDoc['Document']['ocr_data'];
			if (!empty($ocrData)) {
				$hasOcrData = true;
			}
		}
		$this->request->data['Document']['ocr_data'] = $ocrData;
		$this->set('hasOcrData', $hasOcrData);

        if( Configure::read('Webservice.GRC') ) {
            $courriergrcId = null;
            if( !empty( $this->request->data['Courrier']['courriergrc_id'] ) ) {
                $courriergrcId = $this->request->data['Courrier']['courriergrc_id'];
                // On vérifie si la requête existe toujours dans la GRC
                $this->loadModel('Connecteur');
                $hasGrcActif = $this->Connecteur->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Connecteur.name ILIKE' => '%GRC%',
                            'Connecteur.use_grc' => true
                        ),
                        'contain' => false
                    )
                );
                if( !empty( $hasGrcActif )) {
                    $localeo = new LocaleoComponent;
                    $retour = $localeo->getStatusGRC( $courriergrcId );
                    if( !empty($retour->statut->id) ) {
                        $this->Courrier->updateAll(
                            array( 'Courrier.statutgrc' => $retour->statut->id),
                            array(
                                'Courrier.id' => $id,
                                'Courrier.courriergrc_id' => $courriergrcId
                            )
                        );

						if( !in_array($retour->statut->id, array(20, 5, 6, 50) )) {
							$note = 'Le flux est en cours de traitement dans web-GFC';
							$statutId = 20;
							$localeo->updateStatusGRC($courriergrcId, $statutId, $note);
						}
                    }
                    if( isset( $retour->err_msg ) && !empty($retour->err_msg)) {
                        $courriergrcId = null;
                    }

                }
            }
            $this->set( 'courriergrcId', $courriergrcId );
        }

        $organismOptions = array( Configure::read('Sansorganisme.id') => 'Sans organisme');
        if( $this->request->data['Courrier']['organisme_id'] != Configure::read('Sansorganisme.id') && !empty($this->request->data['Courrier']['organisme_id'])) {
			$organismOptions = array($this->request->data['Courrier']['organisme_id'] => $this->request->data['Organisme']['name']);
		}
		$this->set('organismOptions', $organismOptions );

		$contactOptions = array( Configure::read('Sansorganisme.id') => array( Configure::read('Sanscontact.id') => ' Sans contact') );
		if( $this->request->data['Courrier']['contact_id'] != Configure::read('Sanscontact.id') && !empty($this->request->data['Courrier']['contact_id'])) {
			$contactOptions = array($this->request->data['Courrier']['contact_id'] => $this->request->data['Contact']['name']);
		}
		$this->set('contactOptions', $contactOptions );
        $this->_setOptions();

        // Fonction permettant de savoir si on affiche les actions d'ajout/édition des contacts/organismes ou pas
		$this->set('right_action_carnet_adresse_flux', $this->Session->read('Auth.ContactInFluxAllowed'));

		$this->loadModel('Connecteur');
		$hasPastellActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Pastell%',
					'Connecteur.use_pastell'=> true
				),
				'contain' => false
			)
		);
		$hasPastell = 'false';
		if( !empty($hasPastellActif)) {
			$hasPastell = 'true';
		}
		$this->set('hasPastell', $hasPastell);
		$this->set('hasPastellActif', $hasPastellActif);


		$isClosed = $this->Courrier->isClosed($id);
		$this->set('isClosed', $isClosed);
    }

    /**
     *
     * @param type $id
     */
    public function setComments($id) {
        $this->set('flux_id', $id);
//        $soustype_id = $this->Courrier->field('soustype_id', array('Courrier.id' => $id));
//        $this->set('canAddPrivateComment', !empty($soustype_id));
        $this->set('canAddPrivateComment', true);
        $this->set('comments', $this->Courrier->getComments($id));
    }

    /**
     *
     * @param type $id
     */
    public function getComments($id) {
//        $soustype_id = $this->Courrier->field('soustype_id', array('Courrier.id' => $id));
//        $this->set('canAddPrivateComment', !empty($soustype_id));
        $this->set('canAddPrivateComment', true);
        $this->set('comments', $this->Courrier->getComments($id));
        $this->set('viewOnly', $this->Session->read('Auth.User.Courrier.viewOnlyTabs.getTache'));
    }

    /**
     *
     */
//TODO: supprimer les fonction commentées
    public function getContacts() {
        $querydata = array(
            'fields' => array(
                'Contact.id',
                'Contact.name',
                'Addressbook.name'
            ),
            'conditions' => array(
                'Addressbook.active' => true,
                'Contact.active' => true
            ),
            'contain' => false,
            'joins' => array(
                $this->Courrier->Contact->Addressbook->join($this->Courrier->Contact->alias)
            )
        );
        $this->set('contacts', $this->Courrier->Contact->Addressbook->find('list', $querydata));
    }

    /**
     * Récupération des boutons d'action concernant les circuits de traitement (ajax)
     *
     * @logical-group Flux
     * @logical-group CakeFlow
     * @user-profile User
     *
     * @param integer $fluxId identifiant du flux

     * @throws NotFoundException
     */
    public function getFlowControls($fluxId, $cpyOnly = false) {

        $this->Courrier->id = $fluxId;
        $courrier = $this->Courrier->find('first', array(
            'fields' => array(
                'Courrier.id',
                'Courrier.soustype_id',
                'Soustype.id',
                'Soustype.envoi_signature',
                'Soustype.envoi_mailsec',
                'Soustype.envoi_ged',
                'Soustype.envoi_sae',
                'Soustype.circuit_id',
                'Soustype.name',
                'Document.id',
				'Contact.email'
            ),
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'joins' => array(
                $this->Courrier->join('Soustype', array('type' => 'LEFT OUTER')),
                $this->Courrier->join('Document', array('type' => 'LEFT OUTER')),
                $this->Courrier->join('Contact', array('type' => 'LEFT OUTER'))
            ),
            'contain' => false
        ));

		$envoiMailsec = $courrier['Soustype']['envoi_mailsec'];
		$this->set('envoiMailsec', $envoiMailsec);

        if (empty($courrier)) {
            throw new NotFoundException();
        }


        // On regarde si le soustype sélectionné possède une entrée dans la table de liaison
        // cela signifierait qu'il est vu comme un sous-type cible de référence d'1 ou plusieurs flux, i.e. ceux sélectionnables pour la réponse
        $reponse = false;
//        $stypeReponse = $this->Courrier->Soustype->find('first', array('conditions' => array('Soustype.parent_id' => $courrier['Courrier']['soustype_id']), 'contain ' => false));
        $stypeReponse = $this->Courrier->Soustype->SoustypeSoustypecible->find(
            'first',
            array(
                'conditions' => array(
                    'SoustypeSoustypecible.soustypecible_id' => $courrier['Courrier']['soustype_id']
                ),
                'contain ' => false
            )
        );
        if (!empty($stypeReponse)) {
            $reponse = true;
        }

//TODO: verifier si besoin de isRefused
//        $isRefused = $this->Courrier->isRefused($courrier['Courrier']['id']);
        $isRefused = false;
        $isInCircuit = $this->Courrier->isInCircuit($courrier['Courrier']['id']);
        $isClosed = $this->Courrier->isClosed($courrier['Courrier']['id']);

        $rebond = false;
        $etapes = $this->Courrier->Traitement->Circuit->Etape->find('list', array('conditions' => array('Etape.circuit_id' => $courrier['Soustype']['circuit_id']), 'contain' => false));

        $nexts = $this->Courrier->Traitement->whoIsNext($courrier['Courrier']['id']);
        if (count($nexts) == 1 && !in_array($nexts[0], array_keys($etapes))) {
            $rebond = true;
        }

		$hasMailSecWithPastell = false;
		$hasCheminement = true;
		$this->loadModel('Connecteur');
		$hasPastellActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Pastell%',
					'Connecteur.use_pastell'=> true
				),
				'contain' => false
			)
		);
		$this->set('hasPastellActif', $hasPastellActif);
		$nextStep = $this->Courrier->Traitement->whoIsFurther($courrier['Courrier']['id']);
		if(!empty($hasPastellActif)) {
			if( !empty($nextStep)) {
				if ($nextStep[0] == '-3' ) {
					if ($courrier['Soustype']['envoi_mailsec'] && empty($courrier['Contact']['email'])) {
						$hasMailSecWithPastell = true;
					}
				}
			}

			$traitement = $this->Courrier->Traitement->find(
				'first', array(
					'conditions' => array(
						'Traitement.target_id' => $courrier['Courrier']['id']
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			if( !empty($traitement) ) {
				$visas = $this->Courrier->Traitement->Visa->find(
					'all',
					array(
						'conditions' => array(
							'Visa.traitement_id' => $traitement['Traitement']['id'],
							'Visa.trigger_id' => '-3'
						),
						'order' => ['Visa.numero_traitement DESC'],
						'contain' => false,
						'recursive' => -1
					)
				);

				$cheminement = $courrier['Soustype']['envoi_signature'] + $courrier['Soustype']['envoi_mailsec'] + $courrier['Soustype']['envoi_ged'] + $courrier['Soustype']['envoi_sae'];
				if ($cheminement == 0) {
					$hasCheminement = false;
					$hasMailSecWithPastell = false;
				}

				if (!empty($visas)) {
					if ($visas[0]['Visa']['type_document'] == 'document-a-signer') {
						$hasMailSecWithPastell = false;
					}
					if ($visas[0]['Visa']['type_document'] == Configure::read('Pastell.fluxStudioName') . '-mailsec') {
						if (empty($courrier['Contact']['email']) && $nextStep[0] == '-3') {
							$hasMailSecWithPastell = true;
						}
					}
					if ($visas[0]['Visa']['type_document'] == Configure::read('Pastell.fluxStudioName') . '-visa') {
						$hasMailSecWithPastell = false;
					}
				}
			}
		}
		$this->set('hasMailSecWithPastell', $hasMailSecWithPastell);
		$this->set('hasCheminement', $hasCheminement);
		$this->set('nextStep', $nextStep );

//on determine si le courrier est a l etape de fin
        $endCircuit = false;
        $endCircuitIsCollaborative = false;
        $endCircuitIsCollaborativeDerniereUtilisateur = false;
        if ($this->Courrier->cakeflowIsEnd($courrier['Courrier']['id'])) {
            $endCircuit = true;
        }
//debug($endCircuit);
        if ($endCircuit) {
            $last_etape_query = 'select max(E.ordre) from wkf_etapes E where E.circuit_id = ' . $courrier['Soustype']['circuit_id'] . ';';
            $last_etape_id = $this->Courrier->Traitement->Circuit->Etape->query($last_etape_query);
            $last_etape = $this->Courrier->Traitement->Circuit->Etape->find('first', array('conditions' => array('Etape.ordre' => $last_etape_id[0][0]['max'], 'Etape.circuit_id' => $courrier['Soustype']['circuit_id']), 'contain' => false));
            $traitement = $this->Courrier->Traitement->find(
                    'first', array(
                'conditions' => array(
                    'Traitement.circuit_id' => $last_etape['Etape']['circuit_id'],
                    'Traitement.target_id' => $courrier['Courrier']['id']
                ),
                'contain' => false,
                'recursive' => -1
                    )
            );
            if( !empty($traitement) ) {
                $visas = $this->Courrier->Traitement->Visa->find('all', array(
                    'fields' => array(
                        'Visa.action',
                        'Visa.trigger_id',
                        'Visa.id'
                    ),
                    'conditions' => array(
                        'Visa.traitement_id' => $traitement['Traitement']['id'],
                        'Visa.etape_id' => $last_etape['Etape']['id']
                    ),
                    'contain' => false,
                    'recursive' => -1
                ));
                $list_visa = Hash::extract($visas, "{n}.Visa.action");
                $list_desktopId = Hash::extract($visas, "{n}.Visa.trigger_id");
                $listRi = array_count_values($list_visa);
                $endCircuitIsCollaborative = false;
                if (isset($listRi['RI']) && !empty($listRi['RI'])) {
                    if ($last_etape['Etape']['type'] == 3 && $listRi['RI'] > 0) {
                        $endCircuitIsCollaborative = true;
                    }
                }
                if ($traitement['Traitement']['numero_traitement'] > $last_etape['Etape']['ordre']) {
                    $endCircuitIsCollaborative = false;
                }

                $endCircuitIsCollaborativeDerniereUtilisateur = false;
                if ($endCircuitIsCollaborative && $listRi['RI'] == 1) {
                    $this->set('list_desktopId', $list_desktopId);
                    $endCircuitIsCollaborativeDerniereUtilisateur = true;
                    $endCircuitIsCollaborative = false;
                }
            }
        }


        $sendCpyCheck = $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendcpy');
        $sendWkfCheck = $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendwkf');


        $isUserInCircuit = false;
        $desktopUserId = $this->Session->read('Auth.User.Courrier.Desktop.id');
        if ($this->Session->read('Auth.User.Env.actual.name') != 'disp') {
            $traitement = $this->Courrier->Traitement->find(
                    'first', array(
                'fields' => array('circuit_id'),
                'conditions' => array('Traitement.target_id' => $courrier['Courrier']['id']),
                'contain' => false,
                'recursive' => -1
                    )
            );
            if (!empty($traitement)) {
                $circuitId = $traitement['Traitement']['circuit_id'];
                $desktopmanagersIds = $this->Courrier->Desktop->getDesktopManager($desktopUserId);
                $isUserInCircuit = $this->Courrier->Traitement->Circuit->userInCircuit($desktopmanagersIds, $circuitId);
            }
        }
        $this->set(compact('isUserInCircuit'));


    // Le circuit possède-t-il comme prochaine étape une délégation vers le parapheur ?
        $hasEtapeParapheur = $this->Courrier->Traitement->Circuit->hasEtapeDelegation($courrier['Soustype']['circuit_id']);
        $this->set(compact('hasEtapeParapheur'));


		$circuitIsActivated = $this->Courrier->Traitement->Circuit->isActif($courrier['Soustype']['circuit_id']);
		$this->set( 'circuitIsActivated', $circuitIsActivated );

        $fairesuivre = true;
        if (empty($courrier['Document']['id'])) {
            $fairesuivre = false;
        }
        $this->set(compact('fairesuivre'));


// Autorisation de versement
        $isAuthVersGed = false;
        $isAuthVersGed = $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendToGed');
        $this->set('isAuthVersGed', $isAuthVersGed);


// Le bureau actuel est-il celui de l'étape du circuit
        $desktopCurrentEtapeCircuit = false;
        $desktopIdInWkf = $this->Courrier->Traitement->whoIs($courrier['Courrier']['id'], 'current');

        $ensession = $this->Session->read('Auth.User.Env');
        $desktopSessionId = $this->Session->read('Auth.User.Courrier.Desktop.id');
        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        $groupId = Hash::extract($secondaryDesktops, '{n}.Profil.id');
        $desktopId = Hash::extract($secondaryDesktops, '{n}.id');
        // Si le bureau secondaire est de type Valideur Editeur, Valideur ou Documentaliste
        if( isset($groupId) && !empty($groupId) ) {
            if ( in_array( $groupId[0], array( 4, 5, 6 )  ) ) {
                $desktopIdCurrent = $desktopId[0];
            }
            else {
                $desktopIdCurrent = $desktopSessionId;
            }
        }
        else {
            $desktopIdCurrent = $desktopSessionId;
        }

//        $desktopmanagersIds = $this->Courrier->Desktop->getDesktopManager($desktopUserId);
        $desktopmanagersIds = $this->Courrier->Desktop->getDesktopManager($desktopIdCurrent);
        foreach ($desktopIdInWkf as $t => $val) {
            if (in_array($val, $desktopmanagersIds)) {
                $desktopCurrentEtapeCircuit[$t] = true;
            }
        }
        if (is_array($desktopCurrentEtapeCircuit)) {
            $deskInCircuit = in_array(true, $desktopCurrentEtapeCircuit);
            if ($deskInCircuit) {
                $desktopCurrentEtapeCircuit = true;
            }
        }

        $this->set('desktopCurrentEtapeCircuit', $desktopCurrentEtapeCircuit);
        $this->set('endCircuitIsCollaborative', $endCircuitIsCollaborative);
        $this->set('endCircuitIsCollaborativeDerniereUtilisateur', $endCircuitIsCollaborativeDerniereUtilisateur);
//$this->set('etapes',$nexts);
        $this->set('sendwkf', $sendWkfCheck);
        $this->set('sendcpy', $sendCpyCheck);
        $this->set('rebond', $rebond);
        $this->set('reponse', $reponse);
        $this->set('courrier', $courrier);
        $this->set('isRefused', $isRefused);
        $this->set('isInCircuit', $isInCircuit);
        $this->set('isClosed', $isClosed);
        $this->set('endCircuit', $endCircuit);
        $this->set('cpyOnly', $cpyOnly && $sendCpyCheck);
        $this->set('desktopidactuel', $this->Session->read('Auth.User.Courrier.Desktop.id'));
//document principal
        $this->set('mainDoc', $this->Courrier->getMainDoc($courrier['Courrier']['id']));
        $this->set('SoustypeFantome', Configure::read('Soustype.Fantome'));

        $this->loadModel('Connecteur');
        $hasGedActif = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%GED%',
                    'Connecteur.use_ged' => true
                ),
                'contain' => false
            )
        );
        if( !empty( $hasGedActif )) {
            $sendGED = true;
        }
        else {
            $sendGED = false;
        }
        $this->set('sendGED', $sendGED);

        $isSelectvaluemetaSendGRC = false;
        if( Configure::read('Webservice.GRC') ) {
            $isSelectvaluemetaSendGRC = $this->Courrier->getMetaValuesAuthorizedSendToGRC($courrier['Courrier']['id']);
        }
        $this->set(compact('isSelectvaluemetaSendGRC') );

		$this->loadModel('Connecteur');
		$hasGrcActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%GRC%',
					'Connecteur.use_grc' => true
				),
				'contain' => false
			)
		);
		$this->set( 'hasGrcActif', $hasGrcActif);

        // Id du document qui vient tout juste d'être généré, pour l'envoi au IP
		$this->set('docId', $this->Session->read('Session.DocToSendId' ));

		$connecteurSMSActif = false;
		if( Configure::read('Webservice.SMS') ) {
			$this->loadModel('Connecteur');
			$hasSmsActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%LsMessage%',
						'Connecteur.use_sms' => true
					),
					'contain' => false
				)
			);
			if( !empty( $hasSmsActif )) {
				$connecteurSMSActif = true;
			}
		}
		$this->set('connecteurSMSActif', $connecteurSMSActif);

		// Méta obligatoire
		$hasEmptyRequiredMeta = false;
		$metasBySoustype = $this->Courrier->Soustype->MetadonneeSoustype->find(
			'all', array(
				'conditions' => array(
					'MetadonneeSoustype.soustype_id' => $courrier['Courrier']['soustype_id']
				),
				'recursive' => -1
			)
		);
//debug($metasBySoustype);
		if (!empty($metasBySoustype)) {
			$metasIdsBySoustype = Hash::extract($metasBySoustype, '{n}.MetadonneeSoustype.metadonnee_id');
		}

		if (!empty($metasIdsBySoustype)) {
			$metasIdsObligatoireBySoustype = array_combine($metasIdsBySoustype, Hash::extract($metasBySoustype, '{n}.MetadonneeSoustype.obligatoire'));
		}

		$metas = $this->Session->read('Auth.MetasUpdate');
		$compiledMetas = array();
		$metas_values = $this->DataAuthorized->getAuthMetadonnees(array('action' => 'update', 'fluxId' => $fluxId));
		if( !empty($metas ) ) {
			foreach ($metas as $meta) {
				if (!empty($metasIdsBySoustype)) {
					//Si des métadonnées sont liées au sous-typr, on réduit la liste uniquement à ces métadonnées
					if (in_array($meta['Metadonnee']['id'], $metasIdsBySoustype)) { // réduction de la liste des métadonnées selon le sous-type défini dans le flux
						// ajout pour la notion de champ obligatoire sur la métadonnée pour le sous-type en question
						if (!empty($metasIdsObligatoireBySoustype)) {
							$meta['Metadonnee']['obligatoire'] = $metasIdsObligatoireBySoustype[$meta['Metadonnee']['id']];
						}
						foreach ($metas_values as $meta_value) {
							if ($meta['Metadonnee']['id'] == $meta_value['Metadonnee']['id']) {
								if (!empty($meta_value['CourrierMetadonnee'])) {
									$meta['CourrierMetadonnee'] = $meta_value['CourrierMetadonnee'];
								}
							}
						}
						$compiledMetas[] = $meta;
					}
				} else {
					//sinon, on affiche toutes les métadonnées
					foreach ($metas_values as $meta_value) {
						if ($meta['Metadonnee']['id'] == $meta_value['Metadonnee']['id']) {
							if (!empty($meta_value['CourrierMetadonnee'])) {
								$meta['CourrierMetadonnee'] = $meta_value['CourrierMetadonnee'];
							}
						}
					}
					$compiledMetas[] = $meta;
				}
			}
		}
		if( !empty( $compiledMetas) ) {
			foreach ($compiledMetas as $metasCo) {
				if (!empty($metasCo['Metadonnee']['obligatoire']) && $metasCo['Metadonnee']['obligatoire']) {
					if (!isset($metasCo['CourrierMetadonnee'])) {
						$hasEmptyRequiredMeta = true;
					}
				}
			}
		}

		$this->set('hasEmptyRequiredMeta', $hasEmptyRequiredMeta);

		// Aiguillage
		$isdisp = false;
		if( $this->Session->read('Auth.User.Env.actual.name')  == 'disp') {
			$isdisp = true;
		}
		$this->set('isdisp', $isdisp);
    }

    /**
     * Accès à l'onglet "Méta-données" (ajax)
     *
     * @logical-group Flux
     * @logical-flux Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux

     * @return void
     */
    public function setMeta($fluxId = null) {
//enregistrement des données
        if (!empty($this->request->data)) {
            $this->Courrier->Metadonnee->CourrierMetadonnee->begin();
            $msg = 'Onglet ' . __d('courrier', 'Metadonnees') . ' : ';
            $this->Jsonmsg->init($msg . __d('default', 'save.error'));
//preparation des metadonnee
            $success = true;
            $idsASupprimer = array();
            $aEnregistrer = array();
            $aMettreAJour = array();

            if( !empty($this->request->data['CourrierMetadonnee']) ) {
                foreach ($this->request->data['CourrierMetadonnee'] as $key => $values) {

                    // Meta obligatoire devant être renseigné pour avancer dans le circuit
                    $checkboxUnchecked = false;
                    if ($this->Courrier->Metadonnee->field('Metadonnee.typemetadonnee_id', array('Metadonnee.id' => $values['metadonnee_id'])) == 3 && $values['valeur'] == 0) {
                        $checkboxUnchecked = true;
                    }

                    if (trim($values['valeur']) == '') {
                        if (!empty($values['id'])) {
                            $idsASupprimer[] = $values['id'];
                        }
                    } else if ($this->_isExistMetaCourrier($values['courrier_id'], $values['metadonnee_id'])) {
                        $ancienValeur = $this->_getMetaValueExistMetaCourrier($values['courrier_id'], $values['metadonnee_id']);
                        if ($values['valeur'] != $ancienValeur) {
    //                        $aEnregistrer[] = $values;
                            $success = $this->Courrier->Metadonnee->CourrierMetadonnee->updateAll( array('CourrierMetadonnee.valeur' => "'".$values['valeur']."'"), array('CourrierMetadonnee.id' => $values['id'], 'CourrierMetadonnee.courrier_id' => $values['courrier_id']) ) && $success;
                        } else {
                            $success = true;
                        }
                    } else {
                        $aEnregistrer[] = $values;
                    }
                }
            }
            if (!empty($idsASupprimer)) {
                $success = $this->Courrier->Metadonnee->CourrierMetadonnee->deleteAll(array('CourrierMetadonnee.id' => $idsASupprimer)) && $success;
            }
            if (!empty($aEnregistrer)) {
                $success = $this->Courrier->Metadonnee->CourrierMetadonnee->saveAll($aEnregistrer) && $success;
            }

            if ($success) {
                $flux = $this->Courrier->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Courrier.id' => $fluxId
                        ),
                        'contain' => false,
                        'recursive' => -1
                    )
                );
                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (ajout d'une méta-donnée) le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);

                // En cas d'ajout de commentaire, on met à jour la date de modification de la table bancontenus pour le profil étant intervenu
                $this->Courrier->Bancontenu->updateAll(
                    array(
                        'Bancontenu.modified' => "'".date( 'Y-m-d H:i:s')."'"
                    ),
                    array(
                        'Bancontenu.courrier_id' => $fluxId,
                        'Bancontenu.desktop_id' => $this->Session->read('Auth.User.Courrier.Desktop.id')
                    )
                );

                $this->Courrier->Metadonnee->CourrierMetadonnee->commit();
                $this->Jsonmsg->json['newCourrierId'] = $fluxId;
                $this->Jsonmsg->valid($msg . __d('default', 'save.ok'));
            } else {
                $this->Courrier->Metadonnee->CourrierMetadonnee->rollback();
                $this->Jsonmsg->error($msg . __d('default', 'save.error'));
            }
            $this->Jsonmsg->send();
        } else {
            //affichage du formulaire
            // On récupère l'ID du sous type présent dans le flux
            $flux = $this->Courrier->find(
                    'first', array(
                'fields' => array('Soustype.id'),
                'conditions' => array(
                    'Courrier.id' => $fluxId
                ),
                'contain' => array(
                    'Soustype'
                )
                    )
            );
            $metasIdsBySoustype = array();
            // Si un sous type est lié, on regarde la liste des méta données associées
            if (!empty($flux)) {
                $sousTypeId = $flux['Soustype']['id'];

                $metasBySoustype = $this->Courrier->Soustype->MetadonneeSoustype->find(
                        'all', array(
                    'conditions' => array(
                        'MetadonneeSoustype.soustype_id' => $sousTypeId
                    ),
                    'recursive' => -1
                        )
                );
//debug($metasBySoustype);
                if (!empty($metasBySoustype)) {
                    $metasIdsBySoustype = Hash::extract($metasBySoustype, '{n}.MetadonneeSoustype.metadonnee_id');
                }

                if (!empty($metasIdsBySoustype)) {
                    $metasIdsObligatoireBySoustype = array_combine($metasIdsBySoustype, Hash::extract($metasBySoustype, '{n}.MetadonneeSoustype.obligatoire'));
                }
            }


//            $metas = $this->DataAuthorized->getAuthMetadonnees(array('action' => 'update'));

            $metas = $this->Session->read('Auth.MetasUpdate');
            $compiledMetas = array();
            $metas_values = $this->DataAuthorized->getAuthMetadonnees(array('action' => 'update', 'fluxId' => $fluxId));
            if( !empty($metas ) ) {
                foreach ($metas as $meta) {
                    if (!empty($metasIdsBySoustype)) {
    //Si des métadonnées sont liées au sous-typr, on réduit la liste uniquement à ces métadonnées
                        if (in_array($meta['Metadonnee']['id'], $metasIdsBySoustype)) { // réduction de la liste des métadonnées selon le sous-type défini dans le flux
    // ajout pour la notion de champ obligatoire sur la métadonnée pour le sous-type en question
                            if (!empty($metasIdsObligatoireBySoustype)) {
                                $meta['Metadonnee']['obligatoire'] = $metasIdsObligatoireBySoustype[$meta['Metadonnee']['id']];
                            }
                            foreach ($metas_values as $meta_value) {
                                if ($meta['Metadonnee']['id'] == $meta_value['Metadonnee']['id']) {
                                    if (!empty($meta_value['CourrierMetadonnee'])) {
                                        $meta['CourrierMetadonnee'] = $meta_value['CourrierMetadonnee'];
                                    }
                                }
                            }
                            $compiledMetas[] = $meta;
                        }
                    } else {
    //sinon, on affiche toutes les métadonnées
                        foreach ($metas_values as $meta_value) {
                            if ($meta['Metadonnee']['id'] == $meta_value['Metadonnee']['id']) {
                                if (!empty($meta_value['CourrierMetadonnee'])) {
                                    $meta['CourrierMetadonnee'] = $meta_value['CourrierMetadonnee'];
                                }
                            }
                        }
                        $compiledMetas[] = $meta;
                    }
                }
            }
// debug($compiledMetas);
            $this->set('metas', $compiledMetas);
            $this->set('fluxId', $fluxId);
        }
    }

    protected function _isExistMetaCourrier($courrier_id, $metadonnee_id) {
        $count = $this->Courrier->Metadonnee->CourrierMetadonnee->find('count', array(
            'conditions' => array(
                "CourrierMetadonnee.courrier_id" => $courrier_id,
                "CourrierMetadonnee.metadonnee_id" => $metadonnee_id
            )
        ));
        return $count;
    }

    protected function _getMetaValueExistMetaCourrier($courrier_id, $metadonnee_id) {
        $res = $this->Courrier->Metadonnee->CourrierMetadonnee->field('valeur', array('CourrierMetadonnee.courrier_id' => $courrier_id, 'CourrierMetadonnee.metadonnee_id' => $metadonnee_id));
        return $res;
    }

    /**
     * Accès à l'onglet "Tâches" (ajax)
     *
     * @logical-group Flux
     * @logical-flux Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux

     * @throws NotFoundException
     * @return void
     */
    public function setTache($fluxId) {

        $qd = array(
            'recursive' => -1,
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'contain' => false
        );

        $courrier = $this->Courrier->find('first', $qd);
        if (empty($courrier)) {
            throw new NotFoundException();
        }
        $canAddTask = true;
        if (empty($courrier['Courrier']['soustype_id'])) {
            $canAddTask = false;
        }
        $this->set('canAddTask', $canAddTask);

        $querydata = array(
            'contain' => array(
                'Desktop' => array(
                	'User'
				)
            ),
            'conditions' => array(
                "Tache.courrier_id" => $fluxId
            ),
            'order' => array(
                'Tache.created'
            ),
            'recursive' => -1
        );
        $taches_tmp = $this->Courrier->Tache->find('all', $querydata);

        $taches = array();
//on verifie que l utilisateur fait partie des personnes pouvant valider la tache
        $userDesktops = $this->SessionTools->getDesktopList();
        foreach ($taches_tmp as $tache) {
//calcul du retard
            $retard = false;
            if ($tache['Tache']['statut'] != 2 && !empty($tache['Tache']['delai_nb'])) {
                $tmpTime = preg_split('# #', $tache['Tache']['created']);
                $tmpTime1 = preg_split('#-#', $tmpTime[0]);
                if ($tache['Tache']['delai_unite'] == 0) {
                    $endTime = mktime(0, 0, 0, $tmpTime1[1], $tmpTime1[2] + $tache['Tache']['delai_nb'], $tmpTime1[0]);
                } else if ($tache['Tache']['delai_unite'] == 1) {
                    $endTime = mktime(0, 0, 0, $tmpTime1[1], $tmpTime1[2] + (7 * $tache['Tache']['delai_nb']), $tmpTime1[0]);
                } else if ($tache['Tache']['delai_unite'] == 2) {
                    $endTime = mktime(0, 0, 0, $tmpTime1[1] + $tache['Tache']['delai_nb'], $tmpTime1[2], $tmpTime1[0]);
                }
                if ($endTime < mktime()) {
                    $retard = true;
                }
            }
            $tache['retard'] = $retard;
            $tache['canGrab'] = false;
            $tache['canRelease'] = false;
            $tache['canValid'] = false;

            if ($tache['Tache']['statut'] == 0) {
                if (count($tache['Desktop']) == 1) {
                    if (in_array($tache['Desktop'][0]['id'], $userDesktops)) {
                        $tache['canValid'] = true;
                        $tache['canGrab'] = true;
                    }
                } else if (count($tache['Desktop'] ) > 1 ) {
                    foreach ($tache['Desktop'] as $dktp) {
                        if (in_array($dktp['id'], $userDesktops)) {
                            $tache['canGrab'] = true;
                        }
                    }
                }
            } else if ($tache['Tache']['statut'] == 1) {
                if (in_array($tache['Tache']['act_desktop_id'], $userDesktops)) {
                    $tache['canValid'] = true;
                    $tache['canRelease'] = true;
                }
            }

			$tache['isOwner'] = false;
            if( $tache['Tache']['creator_desktop_id'] == $this->Session->read('Auth.User.Courrier.Desktop.id') ) {
				$tache['isOwner'] = true;
			}

            if ($retard) {
                array_unshift($taches, $tache);
            } else {
                array_push($taches, $tache);
            }
        }
        $this->set('taches', $taches);
        $this->set('userDesktops', $userDesktops);

        $rights = array(
            'add' => ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Taches/add') ? true : false),
            'edit' => ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Taches/edit') ? true : false),
            'delete' => ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Taches/delete') ? true : false)
        );
        $this->set('rights', $rights);
        $this->set('id', $fluxId);

		$delaiTranslate = [
			'0' => 'jour(s)',
			'1' => 'semaine(s)',
			'2' => 'mois'
		];
		$this->set('delaiTranslate', $delaiTranslate);
    }

    /**
     * Accès à l'onglet "Dossiers/Affaires" (ajax)
     *
     * @logical-group Flux
     * @logical-flux Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $courrierId identifiant du flux
     * @param integer $affaireId identifiant de l'affaire
     * @throws NotFoundException
     * @return void
     */
    public function setAffaire($courrierId = null, $affaireId = null) {
//enregistrement des données
//association d un courrier a une affaire
        if ($courrierId != null && $affaireId != null) {
            $courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $courrierId)));
            $msg = 'Onglet ' . __d('courrier', 'Affaires') . ' : ';
            $this->Jsonmsg->init($msg . __('default', 'save.error'));
            $this->Courrier->begin();

             if ($courrier['Courrier']['affaire_id'] == $affaireId) { //le courrier et l affaire sont liés, on va les dissocier
                $success = $this->Courrier->updateAll(
                    array( 'Courrier.affaire_id' => null),
                    array('Courrier.id' => $courrier['Courrier']['id'])
                );
                if ($success) {
                    $this->Courrier->commit();
                    $this->Jsonmsg->valid("Le flux a bien été dissocié de l'affaire");
                } else {
                    $this->Courrier->rollback();
                }
            } else { //sinon on les associe
                $success = $this->Courrier->updateAll(
                    array( 'Courrier.affaire_id' => $affaireId),
                    array('Courrier.id' => $courrier['Courrier']['id'])
                );
                if ($success) {
                    $this->Courrier->commit();
                    $this->Jsonmsg->valid("Le flux a bien été associé à l'affaire");
                } else {
                    $this->Courrier->rollback();
                }
            }
            $this->Jsonmsg->send();
        } else {
            //formulaire de creation d un affaire ( et d un dossier)
            if (!empty($this->request->data)) {
                if (!empty($this->request->data['Affaire'])) {
                    if (!empty($this->request->data['Dossier']['name'])) {
                        $this->Courrier->Affaire->Dossier->begin();
                        $saveDossier = $this->Courrier->Affaire->Dossier->save($this->request->data['Dossier']);
                        if ($saveDossier) {
                            $this->Courrier->Affaire->Dossier->commit();
                            $this->request->data['Affaire']['dossier_id'] = $this->Courrier->Affaire->Dossier->id;
                        } else {
                            $this->Courrier->Affaire->Dossier->rollback();
                            $this->request->data['Affaire']['dossier_id'] = '';
                        }
                    }
                    $this->Jsonmsg->init();
                    if ($this->Courrier->Affaire->save($this->request->data['Affaire'])) {
                        $this->Courrier->id = $courrierId;
                        if ($this->Courrier->saveField('affaire_id', $this->Courrier->Affaire->id)) {
                            $this->Jsonmsg->valid();
                        }
                    }
                    $this->Jsonmsg->send();
                }
            }

//affichage du formulaire
            if ($courrierId != null) {
                $querydata = array(
                    'contain' => array(
                        $this->Courrier->Affaire->alias => array(
                            $this->Courrier->Affaire->Dossier->alias
                        )
                    ),
                    'conditions' => array(
                        'Courrier.id' => $courrierId
                    )
                );
                $courrier = $this->Courrier->find('first', $querydata);

                if (!empty($courrier)) {
                    $this->Courrier->Affaire->recursive = 1;
                    $this->Courrier->Affaire->Dossier->recursive = 1;



//                    $this->set('dossiers', $this->Courrier->Affaire->Dossier->getDossiersAffairesTree());
                    $dossiers_tmp = $this->Courrier->Affaire->Dossier->find(
                        "threaded",
                        array(
                            'contain' => array(
                                $this->Courrier->Affaire->alias => array(
                                    'order' => 'Affaire.name'
                                )
                            ),
                            'order' => 'Dossier.name'
                        )
                    );
                    foreach($dossiers_tmp as $t => $dossier) {
                        foreach( $dossier['Affaire'] as $s => $affaire ) {
                            $dossiers_tmp[$t]['children'][$s]['Affaire'] = $affaire;
                            $dossiers_tmp[$t]['children'][$s]['Dossier'] = $dossier['Dossier'];
                            $dossiers_tmp[$t]['children'][$s]['Affaire']['hasParent'] = isset( $dossier['Affaire'][0] ) ?  true : false;
                        }
                        $dossiers_tmp[$t]['Dossier']['hasParent'] = isset( $dossier['Affaire'][0] ) ?  true : false;
                    }
                    $dossiers = array();
                    foreach ($dossiers_tmp as $item) {
                        $item['right_edit'] = true;
                        $item['right_delete'] = true;
                        $dossiers[] = $item;
                    }

                    // Pour les affaires hors dossiers
                    $affaires['-1']['Dossier']['name'] = 'Affaires hors dossier';
                    $affaires['-1']['Dossier']['id'] = '-1';
                    $affaires['-1']['children'] = $this->Courrier->Affaire->find('all', array('conditions' => array('Affaire.dossier_id IS NULL'), 'contain' => false));
                    $dossiers = $dossiers + $affaires;

                    $this->set('dossiers', $dossiers);


//                    $this->set('listDossiers', $this->Courrier->Affaire->Dossier->find('list'));

                    $listDossiers = $this->Courrier->Affaire->Dossier->find('list', array('order' => array('Dossier.name ASC')));
                    $ajoutDossier = array('new' => __d('dossier', 'Dossier.add'));
                    $listDossiers = ($ajoutDossier + $listDossiers );
                    $this->set('listDossiers', $listDossiers);

                    $this->set('selectedAffaire', $courrier['Affaire']['id']);
                    $this->set('selectedFolder', $courrier['Affaire']['dossier_id']);
                    $this->request->data = $courrier;
                } else {
                    throw new NotFoundException();
                }
            }
            $this->autoLayout = false;
        }
    }

    /**
     * Renvoi la liste des sous-types liés au sous-type du flux en fin de circuit
     *
     * @logical-group Flux
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du flux
     * @return void
     */
    public function reponse($id = null) {
        if ($id != null) {
            $soustype_id = $this->Courrier->field('soustype_id', array('Courrier.id' => $id));
//            $soustypes_lies = $this->Courrier->Soustype->find('list', array('conditions' => array('Soustype.parent_id' => $soustype_id), 'order' => array('Soustype.name ASC')));
            $soustypes_lies_tmp = $this->Courrier->Soustype->SoustypeSoustypecible->find(
                'all',
                array(
                    'conditions' => array(
                        'SoustypeSoustypecible.soustypecible_id' => $soustype_id
                    ),
                    'contain' => array(
                        'Soustype' => array(
							'conditions' => array('Soustype.active' => true)
						)
                    ),
                    'order' => array('Soustype.name ASC')
                )
            );
            $soustypes_lies = array();
            foreach($soustypes_lies_tmp as $key => $soustype ) {
                $soustypes_lies[$soustype['Soustype']['id']] = $soustype['Soustype']['name'];
            }
            $this->set('soustypes_lies', $soustypes_lies);
        }

		$checked = true;
		$this->set('checked', $checked);
    }

    /**
     * Validation d'une étape d'un circuit
     *
     * @logical-group Flux
     * @logical-group CakeFlow
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux
     * @param integer $parent identifiant du fluw parent
     * @param integer $stype_id identifiant du sous-type
     * @param integer $insertAuto
     * @return void
     */
    public function valid($fluxId, $desktop_id, $parent = 0, $stype_id = null, $insertAuto = 0) {
        $this->autoRender = false;

		if( strpos( $_SERVER['HTTP_REFERER'], 'historiqueGlobal') !== false ) {
			$desktop_id = $this->Session->read('Auth.User.Courrier.Desktop.id');
			$secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
			$groupId = Hash::extract($secondaryDesktops, '{n}.Profil.id');
			$desktopId = Hash::extract($secondaryDesktops, '{n}.id');
			$groupName = $this->Session->read('Auth.User.Desktop.Profil');
			// Si le bureau secondaire est de type Valideur Editeur, Valideur ou Documentaliste
			if( isset($groupId) && !empty($groupId) ) {
				if ( in_array( $groupId, array( 4, 5, 6 )  ) ) {
					$desktop_id = $desktopId;
				}
			}
		}

        if( empty($desktop_id) ) {
            $desktop_id = $this->Session->read('Auth.User.Courrier.Desktop.id');

            $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
            $groupId = Hash::extract($secondaryDesktops, '{n}.Profil.id');
            $desktopId = Hash::extract($secondaryDesktops, '{n}.id');
            $groupName = $this->Session->read('Auth.User.Desktop.Profil');
            // Si le bureau secondaire est de type Valideur Editeur, Valideur ou Documentaliste
            if( isset($groupId) && !empty($groupId) ) {
                if ( in_array( $groupId, array( 4, 5, 6 )  ) ) {
                    $desktop_id = $desktopId;
                }
            }
        }
        $flux = $this->Courrier->find(
            'first',
            array(
                'conditions' => array(
                    'Courrier.id' => $fluxId
                ),
                'contain' => false,
                'recursive' => -1
            )
        );
        $ret = $this->Courrier->cakeflowExecuteNext($desktop_id, $fluxId, $parent, $stype_id, $insertAuto);


        if ($ret === true) {
			$this->Session->write('Session.DocToSendId', null );
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a validé le flux ".$flux['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);


            $this->Session->setFlash(__d('courrier', 'Courrier.treated.ok'), 'growl', array('type' => 'default'));
        } else {
            $this->Session->setFlash(__d('courrier', 'Courrier.treated.error') . ' : ' . json_decode($ret), 'growl', array('type' => 'erreur'));
        }
    }

    /**
     * Validation d'une étape collaborative qui est le dernière etape d'un circuit
     *
     * @logical-group Flux
     * @logical-group CakeFlow
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux
     * @param integer $parent identifiant du fluw parent
     * @param integer $stype_id identifiant du sous-type
     * @param integer $insertAuto
     * @return void
     */
    public function validEtapeLastCol($fluxId, $parent = 0, $stype_id = null, $insertAuto = 0) {
        $this->autoRender = false;
        $courrier = $this->Courrier->find(
                'first', array(
            'fields' => array(
                'Courrier.id',
                'Courrier.soustype_id',
                'Soustype.id',
                'Soustype.circuit_id',
                'Document.id'
            ),
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'joins' => array(
                $this->Courrier->join('Soustype', array('type' => 'LEFT OUTER')),
                $this->Courrier->join('Document', array('type' => 'LEFT OUTER'))
            ),
            'contain' => false
                )
        );
        $last_etape_query = 'select max(E.ordre) from wkf_etapes E where E.circuit_id = ' . $courrier['Soustype']['circuit_id'] . ';';
        $last_etape_id = $this->Courrier->Traitement->Circuit->Etape->query($last_etape_query);
        $last_etape = $this->Courrier->Traitement->Circuit->Etape->find('first', array('conditions' => array('Etape.ordre' => $last_etape_id[0][0]['max'], 'Etape.circuit_id' => $courrier['Soustype']['circuit_id']), 'contain' => false));
        $traitement_id = $this->Courrier->Traitement->field('Traitement.id', array('Traitement.circuit_id' => $last_etape['Etape']['circuit_id'], 'Traitement.target_id' => $courrier['Courrier']['id']));
        $visas = $this->Courrier->Traitement->Visa->find('all', array(
            'fields' => array(
                'Visa.action',
                'Visa.trigger_id',
                'Visa.id'
            ),
            'conditions' => array(
                'Visa.traitement_id' => $traitement_id,
                'Visa.etape_id' => $last_etape['Etape']['id']
            )
        ));
        $list_desktopId = Hash::extract($visas, "{n}.Visa.trigger_id");
        $this->sendwkf($list_desktopId, $fluxId);
    }

    /**
     * Insertion d'un flux dans un circuit
     *
     * @logical-group Flux
     * @logical-group CakeFlow
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux
     * @throws BadFunctionCallException
     * @return void
     */
    public function insert($fluxId) {
//		if ($fluxId == null) {
//			throw new BadFunctionCallException();
//		}
        $envactual = $this->Session->read('Auth.User.Env.actual.name');
        $envmain = $this->Session->read('Auth.User.Env.main');

        // si on n'est pas la SAERP et que le champ sous-ype fantôme est vide, alors on regarde les étapes
        // sinon, on valide le fait qu'il existe une étape (pour ne pas casser le système)
        if (empty(Configure::read('Soustype.Fantome')) ) {
            $isEtape = $this->Courrier->isEtape($fluxId);
        } else {
            $isEtape = true;
        }
        $this->autoRender = false;

        if ($isEtape == true) {
            $ret = $this->Courrier->cakeflowInsert($fluxId, $this->Session->read('Auth.User.Courrier.Desktop.id'));

            $flux = $this->Courrier->find(
                'first',
                array(
                    'conditions' => array(
                        'Courrier.id' => $fluxId
                    ),
                    'contain' => array(
                        'Soustype'
                    ),
                    'recursive' => -1
                )
            );
            if ($ret === true) {

                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a inséré le flux ".$flux['Courrier']['reference']." dans un circuit dont le sous-type est (".$flux['Courrier']['soustype_id']."), le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);

                if( !empty(Configure::read('Soustype.Fantome')) && $flux['Soustype']['name'] == Configure::read('Soustype.Fantome')) {
                    $this->Session->setFlash('Le flux a été clos', 'growl', array('type' => 'default'));
                }
                else {
                    $this->Session->setFlash(__d('courrier', 'Courrier.insert.ok'), 'growl', array('type' => 'default'));
                }
            } else {
                $this->Session->setFlash(__d('courrier', 'Courrier.insert.error') . ' : ' . json_decode($ret), 'growl', array('type' => 'erreur'));
            }
        } else {
            $this->Session->setFlash(__d('courrier', 'Courrier.insert.emptyStep'), 'growl');
        }

        $this->redirect(array('controller' => 'environnement', 'action' => 'userenv'));
    }

    /**
     * Refus d'un flux inséré dans un circuit
     *
     * @logical-group Flux
     * @logical-group CakeFlow
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du flux
     * @throws BadFunctionCallException
     * @return void
     */

    public function refus($id = null, $message = null) {
		$userId = $this->Session->read('Auth.User.id');

        if ($id != null) {
            $flux = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $id)));
            $this->set('courrier', $flux);
            $msg = array(
                'success' => false,
                'message' => ''
            );
            if (!empty($this->request->data)) {

                $this->Courrier->begin();

//Enregistrement du motif de refus dans le courrier
                if (empty($message)) {
                    if (empty($this->request->data['Notification']['message'])) {
                        $this->request->data['Notification']['message'] = "Motif de refus à demander à l'agent ayant refusé";
                    }
                    $this->Courrier->updateAll(
                            array(
                        'Courrier.motifrefus' => "'" . Sanitize::clean($this->request->data['Notification']['message'], array('encode', false)) . "'",
                        'Courrier.ref_ancien' => $id,
                        'Courrier.parapheur_etat' => null,
                        'Courrier.parapheur_id' => null
                            ), array('Courrier.id' => $id)
                    );
                } else {
                    $this->Courrier->updateAll(
                            array(
                        'Courrier.motifrefus' => "'" . Sanitize::clean($message, array('encode', false)) . "'",
                        'Courrier.ref_ancien' => $id,
                        'Courrier.parapheur_etat' => null,
                        'Courrier.parapheur_id' => null
                            ), array('Courrier.id' => $id)
                    );
                }

//action de refus pour le traitement
                $this->Courrier->Traitement->execute('KO', $this->Session->read('Auth.User.Courrier.Desktop.id'), $flux['Courrier']['id']);
//                $this->Courrier->Traitement->delete( $flux['Courrier']['id'] );
                $traitements = $this->Courrier->Traitement->find(
                        'all', array(
                    'conditions' => array(
                        'Traitement.target_id' => $flux['Courrier']['id']
                    ),
                    'contain' => false
                        )
                );
                foreach ($traitements as $t => $traitement) {
                    $this->Courrier->Traitement->delete($traitement['Traitement']['id']);
                }
                //on change l etat du flux dans toutes les bannettes
                $this->Courrier->Bancontenu->cancel($flux['Courrier']['id']);

				$desktopIdWhoRefused = $this->Session->read('Auth.User.Desktop.id');
				if($this->Session->read('Auth.User.Desktop.Profil.id') == INIT_GID){
					foreach( $this->Session->read('Auth.User.SecondaryDesktops') as $secondaryProfils ) {
						if( in_array($secondaryProfils['Profil']['id'], array(VAL_GID, VALEDIT_GID, ARCH_GID))) {
							$desktopIdWhoRefused = $secondaryProfils['id'];
						}
					}
				}
                $this->Courrier->Bancontenu->refus($desktopIdWhoRefused, $flux['Courrier']['id'], $userId);

                if( !empty( $flux['Courrier']['desktop_creator_id']) ) {
                    $desktopCreatorId = $flux['Courrier']['desktop_creator_id'];
                }
                else {
					if( $this->Session->read('Auth.User.Desktop.Profil.id') == INIT_GID ) {
						$desktopCreatorId = $this->Session->read('Auth.User.Desktop.id');
					}
					else if( in_array( INIT_GID, Hash::extract( $this->Session->read('Auth.User.SecondaryDesktops'), '{n}.Profil.id' ) ) ) {
						$desktopCreatorId = $this->Session->read('Auth.User.SecondaryDesktops')[0]['id'];
					}
					else {
						$desktopCreatorId = $this->Session->read('Auth.User.Courrier.Desktop.id');
					}
                }

				if( Configure::read('Refus.VersProfilUnique') ) {
					$desktopsIds[] = $desktopCreatorId;
					foreach ($desktopsIds as $i => $desktopId) {
						$fluxId = $flux['Courrier']['id'];
						$notifyDesktops[$desktopId] = array($fluxId);
						$this->Courrier->Notifieddesktop->Notification->add(__d('notification', 'ban_cancel.name'), __d('notification', 'ban_cancel.description'), BAN_REFUS, $this->Session->read('Auth.User.Courrier.Desktop.id'), $notifyDesktops);

						// On vérifie que le profil cible du retour d'un flux refusés est bien de type Init
						$typeProfil = $this->Courrier->Desktop->find('first', array('conditions' => array('Desktop.id' => $desktopId), 'contain' => false));
						$groupId = $typeProfil['Desktop']['profil_id'];
						if( $groupId == 3 ) {
							$this->Courrier->Bancontenu->add($desktopId, BAN_REFUS, $flux['Courrier']['id'], false, $userId);
						}
					}
				}
				else {
					if( Configure::read('RefusOnResponse.ToInitProfilOfValidActor')) {
						$desktopmanagerId = $this->Courrier->Desktop->getDesktopManager($desktopCreatorId);
						$desktopsIds = $this->Courrier->Desktop->Desktopmanager->getDesktops($desktopmanagerId);

						$firstsInitsBancontenu = $this->Courrier->Bancontenu->find(
							'all',
							array(
								'conditions' => array(
									'Bancontenu.bannette_id' => BAN_DOCS,
									'Bancontenu.courrier_id' => $flux['Courrier']['id']
								),
								'contain' => false
							)
						);

						if( !empty($firstsInitsBancontenu) ) {
							foreach( $firstsInitsBancontenu as $firstInitBancontenu ) {
								$this->Courrier->Bancontenu->add($firstInitBancontenu['Bancontenu']['desktop_id'], BAN_REFUS, $flux['Courrier']['id'], false, $userId);
								$fluxId = $flux['Courrier']['id'];
								$notifyDesktops[$firstInitBancontenu['Bancontenu']['desktop_id']] = array($fluxId);
								$this->Courrier->Notifieddesktop->Notification->add(__d('notification', 'ban_cancel.name'), __d('notification', 'ban_cancel.description'), BAN_REFUS, $this->Session->read('Auth.User.Courrier.Desktop.id'), $notifyDesktops);
							}
						}
						// Cas de la réponse inséré directement dans le circuit et donc qui ne possède pas d'initiateur, on vise l'initiateur du flux d'origine
						else {
							foreach ($desktopsIds as $i => $desktopId) {
								$fluxId = $flux['Courrier']['id'];
								$notifyDesktops[$desktopId] = array($fluxId);
								$this->Courrier->Notifieddesktop->Notification->add(__d('notification', 'ban_cancel.name'), __d('notification', 'ban_cancel.description'), BAN_REFUS, $this->Session->read('Auth.User.Courrier.Desktop.id'), $notifyDesktops);

								// On vérifie que le profil cible du retour d'un flux refusés est bien de type Init
								$typeProfil = $this->Courrier->Desktop->find('first', array('conditions' => array('Desktop.id' => $desktopId), 'contain' => false));
								$groupId = $typeProfil['Desktop']['profil_id'];
								if( $groupId == 3 ) {
									$this->Courrier->Bancontenu->add($desktopId, BAN_REFUS, $flux['Courrier']['id'], false, $userId);
								}
								else {
									$allOtherInitDesktopsIds = $this->Courrier->Desktop->getOtherDesktops($desktopId);
									if( !empty($allOtherInitDesktopsIds)) {
										foreach( $allOtherInitDesktopsIds as $desktopToAddRefusId ) {
											$this->Courrier->Bancontenu->add($desktopToAddRefusId, BAN_REFUS, $flux['Courrier']['id'], false, $userId);
										}
									}
								}
							}
						}
					}
					else {
						$desktopmanagerId = $this->Courrier->Desktop->getDesktopManager($desktopCreatorId);
						$desktopsIds = $this->Courrier->Desktop->Desktopmanager->getDesktops($desktopmanagerId);

						$firstsInitsBancontenu = $this->Courrier->Bancontenu->find(
							'all',
							array(
								'conditions' => array(
									'Bancontenu.bannette_id' => BAN_DOCS,
									'Bancontenu.courrier_id' => $flux['Courrier']['id'],
									'Bancontenu.desktop_id' => $desktopsIds
								),
								'contain' => false
							)
						);
						if( !empty($firstsInitsBancontenu) ) {
							foreach( $firstsInitsBancontenu as $firstInitBancontenu ) {
								$alreadyAddedRefusBancontenu = $this->Courrier->Bancontenu->find(
									'first',
									array(
										'conditions' => array(
											'Bancontenu.bannette_id' => BAN_REFUS,
											'Bancontenu.courrier_id' => $flux['Courrier']['id'],
											'Bancontenu.desktop_id' => $firstInitBancontenu['Bancontenu']['desktop_id']
										),
										'contain' => false
									)
								);
								if( empty($alreadyAddedRefusBancontenu) ) {
									$this->Courrier->Bancontenu->add($firstInitBancontenu['Bancontenu']['desktop_id'], BAN_REFUS, $flux['Courrier']['id'], false, $userId);
									$fluxId = $flux['Courrier']['id'];
									$notifyDesktops[$firstInitBancontenu['Bancontenu']['desktop_id']] = array($fluxId);
									$this->Courrier->Notifieddesktop->Notification->add(__d('notification', 'ban_cancel.name'), __d('notification', 'ban_cancel.description'), BAN_REFUS, $this->Session->read('Auth.User.Courrier.Desktop.id'), $notifyDesktops);
								}

							}
						}
						// Cas de la réponse inséré directement dans le circuit et donc qui ne possède pas d'initiateur, on vise l'initiateur du flux d'origine
						else {
							foreach ($desktopsIds as $i => $desktopId) {
								$fluxId = $flux['Courrier']['id'];
								$notifyDesktops[$desktopId] = array($fluxId);
								$this->Courrier->Notifieddesktop->Notification->add(__d('notification', 'ban_cancel.name'), __d('notification', 'ban_cancel.description'), BAN_REFUS, $this->Session->read('Auth.User.Courrier.Desktop.id'), $notifyDesktops);

								// On vérifie que le profil cible du retour d'un flux refusés est bien de type Init
								$typeProfil = $this->Courrier->Desktop->find('first', array('conditions' => array('Desktop.id' => $desktopId), 'contain' => false));
								$groupId = $typeProfil['Desktop']['profil_id'];
								if( $groupId == 3 ) {
									$this->Courrier->Bancontenu->add($desktopId, BAN_REFUS, $flux['Courrier']['id'], false, $userId);
								}
								else {
									$allOtherInitDesktopsIds = $this->Courrier->Desktop->getOtherDesktops($desktopId);
									if( !empty($allOtherInitDesktopsIds)) {
										foreach( $allOtherInitDesktopsIds as $desktopToAddRefusId ) {
											$this->Courrier->Bancontenu->add($desktopToAddRefusId, BAN_REFUS, $flux['Courrier']['id'], false, $userId);
										}
									}
								}
							}
						}
					}
				}
				// On enregistre chaque motif de refus en tant que commentaire pour l'historique
				$objet = 'Motif du refus: '.$this->request->data['Notification']['message'];
				$datas['Comment']['owner_id'] = $this->Session->read('Auth.User.Courrier.Desktop.id');
				$datas['Comment']['private'] = false;
				$datas['Comment']['target_id'] = $id;
				$datas['Comment']['objet'] = $objet;
				$this->Courrier->Comment->save($datas);


				// On récupère toutes les infos de bancontenus sauf les copies
				$conditions = array(
					'Bancontenu.courrier_id' => $flux['Courrier']['id'],
					'Bancontenu.bannette_id <>' => BAN_COPY
				);
				// On récupère les infos de bancontenus uniquement pour les initiateurs
				if( Configure::read('Refus.InitiateurOnly') ) {
					$conditions = array(
						'Bancontenu.courrier_id' => $flux['Courrier']['id'],
						'Bancontenu.bannette_id' => BAN_DOCS
					);
				}
				$bancontenus = $this->Courrier->Bancontenu->find(
					'all',
					array(
						'conditions' => $conditions,
						'contain' => false
					)
				);
				$destinataires = Hash::extract($bancontenus, '{n}.Bancontenu.desktop_id');
				$userFound = array();
				foreach ($destinataires as $destinatairem_id) {
					if ($destinatairem_id != -1 && !empty($destinatairem_id)) {
						$userFound[] = $this->Courrier->User->userIdByDesktop($destinatairem_id);
					}
				}
				// On retrouve tous les utilisateurs et on ne prend en compte qu'1 seule fois le même utilisateur
				// pour ne pas le notifier plusieurs fois
				foreach( array_unique( $userFound ) as $u => $user_id ) {
					$typeNotif = $this->Courrier->User->find(
							'first', array(
							'fields' => array('User.typeabonnement'),
							'conditions' => array('User.id' => $user_id),
							'contain' => false
						)
					);
					$notificationId = $this->Courrier->Notifieddesktop->Notification->id;
					if( !empty($typeNotif) ) {
						if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
							$this->Courrier->User->saveNotif($flux['Courrier']['id'], $user_id, $notificationId, 'refus');
						} else {
							$this->Courrier->User->notifier($flux['Courrier']['id'], $user_id, $notificationId, 'refus');
						}
					}
				}

				if (empty($message)) {
					$this->loadModel('Journalevent');
					$datasSession = $this->Session->read('Auth.User');
					$journalmsg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a refusé le flux ".$flux['Courrier']['reference']." le ".date('d/m/Y à H:i:s'). " avec le motif suivant : <br />" . $this->request->data['Notification']['message'];
					$this->Journalevent->saveDatas( $datasSession, $this->action, $journalmsg, 'info', $flux);

					$msg['message'] = 'Le flux ' . $flux['Courrier']['name'] . ' a été refusé avec le motif suivant : <br />' . $this->request->data['Notification']['message'];
				} else {
					$this->loadModel('Journalevent');
					$datasSession = $this->Session->read('Auth.User');
					$journalmsg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a refusé le flux ".$flux['Courrier']['reference']." le ".date('d/m/Y à H:i:s'). " avec le motif suivant : <br />" . $message;
					$this->Journalevent->saveDatas( $datasSession, $this->action, $journalmsg, 'info', $flux);

					$msg['message'] = 'Le flux ' . $flux['Courrier']['name'] . ' a été refusé avec le motif suivant : <br />' . $message;
				}
				$msg['success'] = true;
            } else {
                $msg['message'] .= '<br />Courrier duplicate: error';
            }
//debug($msg);
            if ($msg['success']) {
                $this->Courrier->commit();
                $this->Session->setFlash('Le flux a été refusé', 'growl');

                if( Configure::read('Webservice.GRC') ) {
                    $courriergrcId = null;
                    if( !empty( $flux['Courrier']['courriergrc_id'] ) ) {
                        $courriergrcId = $flux['Courrier']['courriergrc_id'];

                        // On vérifie si la requête existe toujours dans la GRC
                        $this->loadModel('Connecteur');
                        $hasGrcActif = $this->Connecteur->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Connecteur.name ILIKE' => '%GRC%',
                                    'Connecteur.use_grc' => true
                                ),
                                'contain' => false
                            )
                        );
                        if( !empty( $hasGrcActif )) {
                            $localeo = new LocaleoComponent;
                            $note = 'Le flux a été refusé dans web-GFC';
                            $this->Courrier->saveField('statutgrc', '29');
                            $retour = $localeo->updateStatusGRC( $courriergrcId,  '6', $note );
                            if( isset( $retour->err_msg ) && !empty($retour->err_msg)) {
                                $msg['message'] .= '<br />Aucun retour disponible.';
                            }
                            else {
                                $msg['message'] .= $retour;
                            }
                        }
                    }
                    $this->set( 'courriergrcId', $courriergrcId );
                }

            } else {
                $this->Courrier->rollback();
                if (Configure::read('debug') > 0) {
                    $msg['message'] .= '<br />Le flux n\'a pas été refusé.';
                } else {
                    $msg['message'] = 'Le flux n\'a pas été refusé.';
                }
                $this->Session->setFlash($msg['message'], 'growl');
            }
        }
    }

    /**
     * Envoi d'un flux en copie
     *
     * @logical-group Flux
     * @logical-group Edition de flux
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux
     * @return void
     */
    public function sendcpy() {
        if (!empty($this->request->data)) {
//cas d envoi pour copie
            $this->Jsonmsg->init();
            if ($this->Courrier->copy($this->Session->read('Auth.User.Courrier.Desktop.id'), $this->request->data['Send']['flux_id'], $this->request->data['Send']['copy_id'])) {
                $success = true;
//                if (!empty($this->request->data['Comment']['content'])) {
                if (!empty($this->request->data['Comment']['objet'])) {
                    $success = $this->Courrier->Comment->save($this->request->data);
                }
                if (!empty($success)) {
                    $this->Jsonmsg->valid();
                }
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Envoi d'un flux à une étape hors circuit
     *
     * @logical-group Flux
     *
     * @logical-used-by CakeFlow > Envoi d'un flux (avec ou sans rebond) à une nouvelle étape
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux
     * @return void
     */
    public function sendwkf($list_desktopId = null, $fluxId = null) {

        if (!empty($this->request->data)) {
            $this->autoRender = false;
//rebond
            $newDesktopId = $this->request->data['Send']['desktop_id'];
            if( empty($fluxId) ) {
                $fluxId = $this->request->data['Send']['flux_id'];
            }


            $endOfCircuit = $this->Courrier->cakeflowIsEnd($fluxId);
			$isParapheurCible = false;
			if( $newDesktopId == '-1' ) {
				$isParapheurCible = true;
			}
			$nextStep = $this->Courrier->Traitement->whoIsFurther($fluxId);
			if ( ( $endOfCircuit && $isParapheurCible ) || ( $nextStep[0] == '-1' && $isParapheurCible ) ) {
				$this->request->data['Send']['getback'] = true;
			}

			$isPastellCible = false;
			if( $newDesktopId == '-3' ) {
				$isPastellCible = true;
			}
			if ( ($endOfCircuit && $isPastellCible) || ( $nextStep[0] == '-3' && $isPastellCible ) ) {
				$this->request->data['Send']['getback'] = true;
			}


            $newDesktop = $this->Courrier->Desktop->Desktopmanager->find(
                    'first', array(
                'conditions' => array(
                    'Desktopmanager.id' => $newDesktopId
                ),
                'contain' => array(
                    'Desktop'
                )
                    )
            );

            $desktopSessionId = $this->Session->read('Auth.User.Desktop.id');
			$groupSessionId = $this->Session->read('Auth.User.Desktop.Profil.id');
            $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
            $groupId = Hash::extract($secondaryDesktops, '{n}.Profil.id');
            $desktopId = Hash::extract($secondaryDesktops, '{n}.id');
            if ( in_array( $groupSessionId, array( 4, 5 )  ) ) {
				$desktopId = $desktopSessionId;
			}
            else {
				// Si le bureau secondaire est de type Valideur Editeur, Valideur ou Documentaliste
				if (isset($groupId) && !empty($groupId)) {
					if (in_array($groupId[0], array(4, 5, 6))) {
						if (isset($desktopId[1]) && !empty($desktopId[1])) { // cas de la délégation à surveiller .... FIXME
							$desktopId = $desktopId[1];
						} else {
							$desktopId = $desktopId[0];
						}
					} else {
						$desktopId = $desktopSessionId;
					}
				} else {
					$desktopId = $desktopSessionId;
				}
			}


            if( strpos( $_SERVER['REQUEST_URI'], 'lot') === false ) {
				$desktopId = substr($_SERVER['HTTP_REFERER'], strrpos($_SERVER['HTTP_REFERER'], '/') + 1) ; // FIXME: On regarde l'URL au moment de l'envoi
			}

			// Ca de l'envoi depuis l'écran de l'historique global
			if( strpos( $_SERVER['HTTP_REFERER'], 'historiqueGlobal') != 0 ) {

				$desktopsOfUser = $this->Courrier->Desktop->User->getDesktops($this->Session->read('Auth.User.id') );
				$userInBancontenu = $this->Courrier->Bancontenu->find(
					'all',
					array(
						'fields' => array('Bancontenu.courrier_id', 'Bancontenu.desktop_id'),
						'conditions' => array(
							'Bancontenu.desktop_id' => $desktopsOfUser,
							'Bancontenu.courrier_id' => $fluxId,
							'Bancontenu.bannette_id <>' => 7
						),
						'contain' => false
					)
				);
				$displayBecauseUserIsInBancontenu = false;
				if( !empty($userInBancontenu) || $isAdmin) {
					$displayBecauseUserIsInBancontenu = true;
				}
				$this->set( 'displayBecauseUserIsInBancontenu', $displayBecauseUserIsInBancontenu );

				// On regarde les ids des profils possédant le flux
				$desktopIdToAdd = [];
				foreach($userInBancontenu as $user ) {
					$desktopIdToAdd[] = $user['Bancontenu']['desktop_id'];
				}
				// Récupération de toutes les infos échangés sur le flux entre les diférentes bannettes
				$bannettes = $this->Courrier->getCycle($courrier['Courrier']['id']);
				// On regarde les ids des profils possédant le flux
				$desktopIdToAdd = [];
				foreach($userInBancontenu as $user ) {
					$desktopIdToAdd[] = $user['Bancontenu']['desktop_id'];
				}
				$desktopIdCurrent = $this->Session->read('Auth.User.Courrier.Desktop.id');
				// si un des profils de l'agent connecté est présent das la liste des profils possédant le flux
				// alors on ajoute cet ID dans la requête pour quela validation puisse se faire sans soucis
				$desktopIdToUse = null;
				if(in_array($desktopIdCurrent, $desktopIdToAdd)) {
					$desktopIdToUse = $desktopIdCurrent;
				}

				// On regarde les bannettes possédant le flux
				$bannettesContent = Hash::extract($bannettes, 'bannettes.{n}.Bancontenu');
				$desktopsWhoGetTheFlux = [];
				foreach( $bannettesContent as $content ) {
					if( $content['etat'] == 1 ) {
						// On stocke les profils possédant le flux en ce moment
						$desktopsWhoGetTheFlux[] = $content['desktop_id'];
						// Si le profil de l'agent connecté est présent dans les bannettes possédant le flux,
						// alors on ne surcharge pas la valeur
						if( $content['desktop_id'] == $desktopIdCurrent) {
							$desktopsWhoGetTheFlux = [];
						}
					}
				}
				$delegations = $this->Courrier->User->getDelegatesDesktopsOtherThanMe($this->Session->read('Auth.User.id'), [VALEDIT_GID]);
				if( !empty( $delegations ) ) {
					// Parmi la liste des profils possédant le flux, si l'agent possède une délégation
					// on vérifie si un des profils est présent parmi les délégations, et dans ce cas, on prend sa valeur
					foreach( $desktopsWhoGetTheFlux as $desktopsWhoGetTheFluxId ) {
						if( in_array( $desktopsWhoGetTheFluxId, $delegations )) {
							$desktopIdToUse = $desktopsWhoGetTheFluxId;
						}
					}
				}
				$desktopId = $desktopIdToUse;
			}

            $desktopsReadersIds = $this->Courrier->Desktop->Desktopmanager->getDesktops($newDesktopId);

// initialisation des visas a ajouter au traitement
            if($newDesktopId == '-1' ) {
                $conditions = array(
                    'trigger_id' => $newDesktopId,
                    'type_validation' => 'D'
                );

                $conditionsEtapes = array(
                    'etape_nom' => $newDesktop[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS],
                    'etape_type' => 1,
                    'soustype' => $this->request->data['Send']['signature']
                );
            }
            else if($newDesktopId == '-3' ) {
				$conditions = array(
					'trigger_id' => $newDesktopId,
					'type_validation' => 'P',
					'type_document' => $this->request->data['Send']['pastell_type_document'],
					'inforequired_type_document' => $this->request->data['Send']['pastell_inforequired_type_document']
				);

				$conditionsEtapes = array(
					'etape_nom' => $newDesktop[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS],
					'etape_type' => 1,
					'type_document' => $this->request->data['Send']['pastell_type_document'],
					'inforequired_type_document' => $this->request->data['Send']['pastell_inforequired_type_document']
				);
			}
            else {
                $conditions = array(
                    'trigger_id' => $newDesktopId,
                    'type_validation' => 'V'
                );

                $conditionsEtapes = array(
                    'etape_nom' => $newDesktop[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS],
                    'etape_type' => 1
                );
            }
            $options = array(
                'insertion' => array(
                    '0' => array(
                        'Etape' => $conditionsEtapes,
                        'Visa' => array(
                            '0' => $conditions
                        )
                    )
                ),
				'email' => $this->request->data['Send']['email']
            );
            $action = $this->request->data['Send']['getback'] ? 'IL' : 'IP';

			$isForPastellSignature = false;
            if( $newDesktopId = '-3' && !empty( $this->request->data['Send']['pastell_inforequired_type_document'] ) ) {
            	$isForPastellSignature = true;
			}
            $success = false;
            // Si une étape Parapheur existe déjà, on fait comme si elle n'avait pas existé
            // on passe le parapheur_id et le parapheur _etat à null
            if($newDesktopId == '-1' || $isForPastellSignature ) {
                $success = $this->Courrier->updateAll(
                    array(
                        'Courrier.parapheur_etat' => NULL,
                        'Courrier.parapheur_id' => NULL,
                        'Courrier.signee' => false
                    ),
                    array(
                        'Courrier.id' => $fluxId
                    )
                );

            }

            if ($this->Courrier->cakeflowExecuteSend($action, $desktopId, $fluxId, $options)) {
                $success = true;
                if (!empty($this->request->data['Comment']['objet'])) {
                    $datas = $this->request->data;
                    $datas['Comment']['owner_id'] = $this->Session->read('Auth.User.Courrier.Desktop.id');
                    $datas['Comment']['private'] = true;
                    $datas['Comment']['target_id'] = $fluxId;
                    $datas['Comment']['objet'] = preg_replace("/[\n\r]/", " ", $this->request->data['Comment']['objet'] );
                    $datas['Reader'] = array('Reader' => $desktopsReadersIds);

                    $success = $this->Courrier->Comment->save($datas) && $success;
                }
            }

            if (!empty($success)) {
				$this->Session->setFlash(__d('default', 'save.ok'), 'growl');
            } else {
                $this->Session->setFlash(__d('default', 'save.error'), 'growl');
            }
        }
    }

    /**
     *
     */
    public function send($fluxId = null, $cpyOnly = false) {

		$userId = $this->Session->read('Auth.User.id');
        $this->set('sendwkf', $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendwkf'));
        $this->set('sendcpy', $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendcpy'));

        if (!empty($this->request->data)) {

            if (isset($this->request->data['Send']['copy']) && $this->request->data['Send']['copy']) {
                $valid = array();
                $this->Jsonmsg->init();


				$flux = $this->Courrier->find(
					'first',
					array(
						'conditions' => array(
							'Courrier.id' => $fluxId
						),
						'contain' => false,
						'recursive' => -1
					)
				);
                $desktopsFromManagerIds = array();
                $desktopsCopyFromManagerIds = array();

// On récupère les ids sélectionnés pour envoi pour copie
                $desktopCopyIds = Hash::get($this->request->data, 'Send.copy_id');
                $desktopCopyIds = ( empty($desktopCopyIds) ? array() : (array) $desktopCopyIds );
// On regarde le bureau sélectionné et on retourne les profils associés
                foreach ($desktopCopyIds as $desktopmanagercopyId) {
                    $desktopsCopyFromManagerIds[$desktopmanagercopyId] = $this->Courrier->Desktop->Desktopmanager->getDesktops($desktopmanagercopyId);
                }

                $desktopsIds = array();
                foreach ($desktopCopyIds as $i => $desktopmanager) {
                    $this->Desktopmanager = ClassRegistry::init('Desktopmanager');
                    $desktopsIds[] = $this->Desktopmanager->getDesktops($desktopmanager);
                }


                // Enregistrement des commentaires pour les flux (copie ou aiguillage)
                $readersCopyIds = Hash::extract($desktopsCopyFromManagerIds, '{n}.{n}');
                $readers = $readersCopyIds;
                if (!empty($this->request->data['Comment']['objet'])) {
                    $datas = $this->request->data;
                    $this->request->data['Comment']['owner_id'] = $this->Session->read('Auth.User.Courrier.Desktop.id');
                    $this->request->data['Comment']['private'] = true;
                    $this->request->data['Comment']['target_id'] = $this->request->data['Send']['flux_id'];
                    $this->request->data['Reader'] = array('Reader' => $readers);
                    $this->request->data['Send']['flux_id'] = $fluxId;
                    $this->Courrier->Comment->saveAll($this->request->data);
                }




                $banettesCopieActuelles = $this->Courrier->Bancontenu->find(
                    'all',
                    array(
                        'conditions' => array(
                            'Bancontenu.courrier_id' => $fluxId,
                            'Bancontenu.etat IN' => array(1, 3),
                            'Bancontenu.bannette_id' => BAN_COPY
                        ),
                        'contain' => false
                    )
                );
                $oldBannettesDesktopsIds = Hash::extract($banettesCopieActuelles, '{n}.Bancontenu.desktop_id');


                foreach ($desktopsIds as $i => $desktopId) {
                    $success = true;
// Envoi des notifications pour chacun des bureaux en copie
                    $destId = array();
                    $notifInfos = $this->Courrier->Notifieddesktop->Notification->setNotification('ban_copy');
//                    $desktopId = $this->Session->read('Auth.User.desktop_id');
                    $desktopAuthId = $this->Session->read('Auth.User.Courrier.Desktop.id');
                    $fluxId = $this->request->data['Send']['flux_id'];


                    for ($d = 0; $d < count($desktopId); $d++) {

                        // Si le flux est déjà en copie chez les gens, on ne les ajoute plus
                        if( !in_array( $desktopId[$d], $oldBannettesDesktopsIds ) ) {
                            $notifyDesktops[$desktopId[$d]] = array($fluxId);
                            $notified = $this->Courrier->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $desktopAuthId, $notifyDesktops);
    // On ajoute le flux dans la bannette copie
                            $valid[$this->request->data['Send']['flux_id']] = array(
                                'add' => $this->Courrier->Bancontenu->add($desktopId[$d], BAN_COPY, $this->request->data['Send']['flux_id'], false, $userId)
                            );
                        }
                    }

					$this->loadModel('Journalevent');
					$desktopsIdsForCopy = implode(', ', $desktopId);
					$datasSession = $this->Session->read('Auth.User');
					$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a envoyé en copie le flux ".$flux['Courrier']['reference']." vers le(s) profil(s) (".$desktopsIdsForCopy."), le ".date('d/m/Y à H:i:s');
					$this->Journalevent->saveDatas( $datasSession, 'sendcpy', $msg, 'info', $flux);
                }

// Ajout pour les notifs durant le traitement
                foreach ($desktopCopyIds as $destinataire_id) {
                    $this->Desktopmanager = ClassRegistry::init('Desktopmanager');
                    $desktopsToSendIds = $this->Desktopmanager->getDesktops($destinataire_id);
                    foreach ($desktopsToSendIds as $deskId) {
                        if ($deskId != -1) {
                            $user_id = $this->Courrier->User->userIdByDesktop($deskId);
                            if (!empty($user_id)) {
                                $typeNotif = $this->Courrier->User->find(
                                        'first', array(
                                    'fields' => array('User.typeabonnement'),
                                    'conditions' => array('User.id' => $user_id),
                                    'contain' => false
                                        )
                                );
                                $notificationId = $this->Courrier->Notifieddesktop->Notification->id;
                                if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
                                    $this->Courrier->User->saveNotif($this->request->data['Send']['flux_id'], $user_id, $notificationId, 'copy');
                                } else {
                                    $this->Courrier->User->notifier($this->request->data['Send']['flux_id'], $user_id, $notificationId, 'copy');
                                }
                            }
                        }
                    }
                }

                if ($valid == true) {
                    $this->Jsonmsg->valid();
                }

                $this->Jsonmsg->send();
            }
// envoi à la signature manuelle
// ajout d'une étape "fictive"
            else if (isset($this->request->data['Send']['signature_manuelle']) && $this->request->data['Send']['signature_manuelle']) {
//                $this->request->data['Send']['getback'] = fa;
                $this->request->data['Send']['desktop_id'] = $this->Session->read('Auth.User.Courrier.Desktop.id');
                $this->sendwkfsignaturemanuelle($this->request->data);
            } else {
                $this->sendwkf();
            }
        } else {

			// Récupération de la liste de tous les bureaux
            if( empty($this->Session->read('Auth.DesktopsByProfils' ))) {
				$allDesktops = $this->Courrier->User->Desktop->Desktopmanager->getAllDesktopsByProfils();
			}
			else {
				$allDesktops = $this->Session->read('Auth.DesktopsByProfils' );
			}


            // Récupation des sous-types IP
            $this->Connecteur = ClassRegistry::init('Connecteur');
            $hasParapheurActif = $this->Connecteur->find(
                'first',
                array(
                    'conditions' => array(
                        'Connecteur.name ILIKE' => '%Parapheur%',
						'Connecteur.use_signature'=> true
                    ),
                    'contain' => false
                )
            );
            $this->set('hasParapheurActif', $hasParapheurActif);

			$typeParapheur = array();
            if( !empty($hasParapheurActif) && $hasParapheurActif['Connecteur']['use_signature'] && $hasParapheurActif['Connecteur']['signature_protocol'] != 'PASTELL') {
                $desktopsToSend = $this->Session->read('Auth.DeskInCircuitWithParapheur' );
				$typeParapheur = $this->Session->read( 'Iparapheur.listSoustype' );
				if (!empty($typeParapheur)) {
					$this->set('typeParapheur', $typeParapheur	);
				} else {
					$this->Session->setFlash('Connexion au parapheur impossible', 'growl');
				}
            }
            else {
				$desktopsToSend = $this->Session->read('Auth.DeskInCircuit' );
            }

			// Récupation des types de dossiers Pastell
			$hasPastellActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Pastell%',
						'Connecteur.use_pastell'=> true
					),
					'contain' => false
				)
			);
			$this->set('hasPastellActif', $hasPastellActif);
			// On ne permet pas les envois vers Parapheur ou Pastell selon le type de protocole de signature
			if( !empty($hasParapheurActif) && !empty($hasPastellActif) ) {
					unset($desktopsToSend['-1']);
			}
			elseif(!empty($hasParapheurActif) && empty($hasPastellActif) ) {
				unset($desktopsToSend['-3']);
			}
			elseif(empty($hasParapheurActif) && empty($hasPastellActif) ) {
				unset($desktopsToSend['-1']);
				unset($desktopsToSend['-3']);
			}

			$courrier = $this->Courrier->find('first', array(
				'fields' => array_merge(
					$this->Courrier->Soustype->fields(),
					$this->Courrier->Document->fields(),
					$this->Courrier->Contact->fields()
				),
				'conditions' => array(
					'Courrier.id' => $fluxId
				),
				'joins' => array(
					$this->Courrier->join('Soustype'),
					$this->Courrier->join('Document'),
					$this->Courrier->join('Contact')
				),
				'contain' => false
			));


			$typePastell = array();
			$listSoustypeIp = array();

			if( !empty($hasPastellActif) && $hasPastellActif['Connecteur']['use_pastell'] ) {
				if( empty($this->Session->read('Pastell.listSoustypeIp')) ) {
					$id_entity = $hasPastellActif['Connecteur']['id_entity'];
					// Récup des sous-types dispos
					$listSoustypeIp = $this->NewPastell->getCircuits($id_entity, Configure::read('Pastell.fluxStudioName'));
					$this->Session->write('Pastell.listSoustypeIp', $listSoustypeIp);
				}

				$listSoustypeIp = $this->Session->read('Pastell.listSoustypeIp');
				if( isset($listSoustypeIp['soustype']) ) {
					$listSoustypeIp = $listSoustypeIp['soustype'];
				}
				else {
					$listSoustypeIp = [];
				}
				$typePastell = $this->Courrier->getTypeDocNamePastell( $courrier );
				if( !empty( $courrier['Soustype']['soustype_parapheur']) ) {
					$this->request->data['Send']['pastell_inforequired_type_document'] = $courrier['Soustype']['soustype_parapheur'];
				}
			}
			$this->set('typePastell', $typePastell);
			$this->set('listSoustypeIp', $listSoustypeIp);


			$soustypeAvecMailsec = false;
			if( $courrier['Soustype']['envoi_mailsec'] ) {
				$soustypeAvecMailsec = true;
			}
			$this->set('soustypeAvecMailsec', $soustypeAvecMailsec);

			$endCircuit = false;
			if ($this->Courrier->cakeflowIsEnd($fluxId)) {
				$endCircuit = true;
			}
			$this->set('endCircuit', $endCircuit);

			$getback = false;
			$nextStep = $this->Courrier->Traitement->whoIsFurther($fluxId);
			if ( $endCircuit || ( !empty($nextStep) && $nextStep[0] == '-1' ) ) {
				$getback = true;
			}
			if ( $endCircuit || ( !empty($nextStep) && $nextStep[0] == '-3' ) ) {
				$getback = true;
			}
			$this->set('getback', $getback);

			$docIsValid = $this->Courrier->Document->isValid( $courrier['Document']['id'] );

			$hasCheminement = true;
			$cheminement = $courrier['Soustype']['envoi_signature'] + $courrier['Soustype']['envoi_mailsec'] + $courrier['Soustype']['envoi_ged'] + $courrier['Soustype']['envoi_sae'];
			if( $cheminement == 0 ) {
				$hasCheminement = false;
			}
			$this->set('hasCheminement', $hasCheminement);

			$this->set('typeParapheur', $typeParapheur);
			$this->set('docId', $this->Session->read('Session.DocToSendId' ));
            $this->set('desktopsToSend', $desktopsToSend);
            $this->set('allDesktops', $allDesktops);
            $this->set('cpyOnly', $cpyOnly);
            $this->set('fluxId', $fluxId);
            $this->set('courrier', $courrier);
            $this->set('docIsValid', $docIsValid);

        }
    }

    /**
     * Envoi d'un flux à un aiguilleur
     *
     * @logical-group Flux
     * @user-profile User
     * @user-profile Aiguilleur
     *
     * @access public
     * @return void
     */
    public function dispsend() {
		$userId = $this->Session->read('Auth.User.id');
        $this->autoRender = false;
        if (!empty($this->request->data)) {
            $desktopId = $this->request->data['DispForm']['initDesktopService'];
            $flux = array();
            foreach ($this->request->data['DispForm'] as $key => $val) {
                if ($key != 'initDesktopService') {
                    if ($val == true) {
                        $tmp = explode('_', $key);
                        if ($tmp[0] == 'checkItem') {
                            $flux[] = $tmp[1];
                        }
                    }
                }
            }

            $groupId = $this->Courrier->Bancontenu->Desktop->field('Desktop.profil_id', array('Desktop.id' => $desktopId));
            $dispDesktops = array_keys($this->getSessionDispDesktops());
            $senderDispId = $dispDesktops[0];
            $notifyDesktops = array($desktopId => $flux);
            if ($groupId == DISP_GID) {
                $valid = array();
                for ($i = 0; $i < count($flux); $i++) {
                    $valid[$flux[$i]] = array(
                        'end' => $this->Courrier->Bancontenu->end($senderDispId, BAN_AIGUILLAGE, $flux[$i], $userId),
                        'add' => $this->Courrier->Bancontenu->add($desktopId, BAN_AIGUILLAGE, $flux[$i], false, $userId)
                    );

					// on stocke qui fait quoi quand
					$courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $flux[$i]), 'contain' => false));
					$this->loadModel('Journalevent');
					$datasSession = $this->Session->read('Auth.User');
					$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a aiguillé le flux ".$courrier['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
					$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrier);
                }
            } else if ($groupId == INIT_GID) {
                $valid = array();
                for ($i = 0; $i < count($flux); $i++) {
                    $courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $flux[$i])));
                    $courrier['Courrier']['desktop_creator_id'] = $desktopId;
                    $this->Courrier->create();
                    $this->Courrier->save($courrier);
                    $valid[$flux[$i]] = array(
                        'end' => $this->Courrier->Bancontenu->end($senderDispId, BAN_AIGUILLAGE, $flux[$i], $userId),
                        'add' => $this->Courrier->Bancontenu->add($desktopId, BAN_DOCS, $flux[$i], false, $userId)
                    );

					// on stocke qui fait quoi quand
					$courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $flux[$i]), 'contain' => false));
					$this->loadModel('Journalevent');
					$datasSession = $this->Session->read('Auth.User');
					$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a aiguillé le flux ".$courrier['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
					$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrier);
                }
                $notifInfos = $this->Courrier->Notifieddesktop->Notification->setNotification('ban_xnew');
                $this->Courrier->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $this->Session->read('Auth.User.Courrier.Desktop.id'), $notifyDesktops);
            }


        }
    }

    /**
     * Suppression d'un flux
     *
     * @logical-group Flux
     * @user-profile User
     *
     * @param integer $id identifiant du flux
     * @throws NotFoundException
     */
    public function delete($id = null) {

        $this->autoRender = false;
        $this->Courrier->id = $id;
        if (!$this->Courrier->exists()) {
            throw new NotFoundException();
        }

        $deleteAll = array();
        $bancontenus = array_keys($this->Courrier->Bancontenu->find('list', array('conditions' => array('Bancontenu.courrier_id' => $id))));
        $this->Courrier->begin();

        // on stocke qui a supprimé quel flux
        $flux = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $id), 'contain' => false, 'recursive' => -1) );
        $this->loadModel('Journalevent');
        $datasSession = $this->Session->read('Auth.User');
        $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé le flux ".$flux['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
        $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);

        for ($i = 0; $i < count($bancontenus); $i++) {
            $deleteAll[] = $this->Courrier->Bancontenu->delete($bancontenus[$i]);
        }

        // On supprime les documents liés au flux qui sont encore stockés sur le disque
        $conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
        $documentsPath = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'];
        $documents = $this->Courrier->Document->find('all', array('fields' => array('Document.id', 'Document.name', 'Document.path', 'Document.courrier_id'), 'conditions' => array('Document.courrier_id' => $id), 'recursive' => -1));
        if( !empty( $documentsPath ) && !empty($documents) ) {
            foreach( $documents as $doc ) {
				if( file_exists($documentsPath . DS . $doc['Document']['name'])) {
					unlink($documentsPath . DS . $doc['Document']['name']);
				}
            }
            rmdir($documentsPath);
        }

        $deleteAll[] = $this->Courrier->delete($id);

        if (!in_array(false, $deleteAll, true)) {
            $this->Courrier->commit();
            $this->Session->setFlash(__d('courrier', 'Courrier deleted', true), 'growl');
        } else {
            $this->Courrier->rollback();
            $this->Session->setFlash(__d('courrier', 'Courrier was not deleted', true), 'growl');
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }


    /**
     * Vérification de la possibilité d'insertion d'un flux dans un circuit à la fin du wizard de création
     *
     * @logical-group Flux
     * @logical-group Création de flux
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du flux
     * @return void
     */
    public function getInsertEnd($id) {
        $this->Courrier->id = $id;
        $courrier = $this->Courrier->find(
                'first', array(
            'fields' => array(
                'Courrier.id',
                'Courrier.soustype_id',
                'Soustype.id',
                'Soustype.name',
                'Soustype.circuit_id',
                'Document.id'
            ),
            'conditions' => array(
                'Courrier.id' => $id
            ),
            'joins' => array(
                $this->Courrier->join('Soustype', array('type' => 'LEFT OUTER')),
                $this->Courrier->join('Document', array('type' => 'LEFT OUTER'))
            ),
            'contain' => false
                )
        );
// Le circuit possède-t-il comme prochaine étape une délégation vers le parapheur ?
        $hasEtapeParapheur = $this->Courrier->Traitement->Circuit->hasEtapeDelegation($courrier['Soustype']['circuit_id']);
        $this->set(compact('hasEtapeParapheur'));
		$allmembers = $this->Courrier->Traitement->Circuit->getAllMembers($courrier['Soustype']['circuit_id']);
        $soustypeDefined = false;
        $canInsert = false;
        $circuitIsActivated = false;
        if (!empty($courrier['Courrier']['soustype_id'])) {
            $soustypeDefined = true;

			if( !empty(Configure::read('Soustype.Fantome')) && $courrier['Soustype']['name'] == Configure::read('Soustype.Fantome')) {
				$canInsert = true;
			}


			$circuitIsActivated = $this->Courrier->Traitement->Circuit->isActif($courrier['Soustype']['circuit_id']);
			if( !empty(Configure::read('Soustype.Fantome')) && $courrier['Soustype']['name'] == Configure::read('Soustype.Fantome')) {
				$circuitIsActivated = true;
			}
            if (!empty($courrier['Soustype']["circuit_id"]) && $circuitIsActivated ) {
                $canInsert = true;
            }
            if ($hasEtapeParapheur) {
                if (!empty($courrier['Document']['id'])) {
                    $canInsert = true;
                } else {
                    $canInsert = false;
                    // Si le premier membre n'est pas le Parapheur, alors on autorise l'insertion
					if( !empty($allmembers) && $allmembers[1] != '-1' ) {
						$canInsert = true;
					}
                }
            }
        }
        $this->set('courrier', $courrier);
        $this->set('soustypeDefined', $soustypeDefined);
        $this->set('canInsert', $canInsert);
		$this->set( 'circuitIsActivated', $circuitIsActivated );
    }

    /**
     * Récupération de la liste des accusés de réception
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux

     * @throws NotFoundException
     * @return void
     */
    public function getArs($fluxId) {
        $qdFlux = array(
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'contain' => array(
                'Soustype' => array(
                    'Armodel' => array(
                        'fields' => $this->Courrier->Soustype->Armodel->getLightFields(),
                        'order' => array('Armodel.name')
                    )
                )
            )
        );
        $flux = $this->Courrier->find('first', $qdFlux);

        if (empty($flux)) {
            throw new NotFoundException();
        }

        $qdAr = array(
            'fields' => $this->Courrier->Ar->getLightFields(),
            'recursive' => -1,
            'conditions' => array(
                'Ar.courrier_id' => $fluxId
            ),
            'order' => 'Ar.created DESC'
        );
        $ar = $this->Courrier->Ar->find('first', $qdAr);

        $this->set('ar', $ar);
        $this->set('armodels', !empty($flux['Soustype']['Armodel']) ? $flux['Soustype']['Armodel'] : array());
        $this->set('fluxId', $fluxId);

        $this->set('fromForbiddenLocation', $this->_isFromFobiddenLocation($flux));

        $this->set('right_sendmail', true);


		$this->Connecteur = ClassRegistry::init('Connecteur');
		$hasParapheurActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true
				),
				'contain' => false
			)
		);
		$this->set('hasParapheurActif', $hasParapheurActif);

		$hasEtapeParapheur = $this->Courrier->Traitement->Circuit->hasEtapeDelegation($flux['Soustype']['circuit_id']);
		$this->set( 'hasEtapeParapheur', $hasEtapeParapheur );
		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
    }

    /**
     * Génération d'un accusé de réception
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux

     * @throws NotFoundException
     * @return void
     */
    public function setArs($fluxId) {
        $conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
        $qdFlux = array(
            'fields' => array('Courrier.id', 'Courrier.reference'),
            'recursive' => -1,
            'conditions' => array(
                'Courrier.id' => $fluxId
            )
        );
        $flux = $this->Courrier->find('first', $qdFlux);
        if (empty($flux)) {
            throw new NotFoundException();
        }

        $qdArmodel = array(
            'recursive' => -1,
            'conditions' => array(
                'Armodel.id' => $this->request->data['Armodel']['choice']
            )
        );
        $armodel = $this->Courrier->Soustype->Armodel->find('first', $qdArmodel);
        if (empty($armodel)) {
            throw new NotFoundException();
        }

        if (!empty($this->request->data)) {
            $this->Jsonmsg->init(__d('ar', 'Ar.generated.error'));
            $this->Courrier->Ar->begin();
            $this->Courrier->Ar->deleteAll(array('Ar.courrier_id' => $fluxId));
            $ar = array(
                'Ar' => array(
//                    'name' => 'AR ' . $armodel['Armodel']['name'] . " " . date('YmdHis') . '.pdf',
                    'name' => 'AR_' . $armodel['Armodel']['name'] . "_" . date('YmdHis') . '.' . $armodel['Armodel']['ext'],
                    'format' => $armodel['Armodel']['format'],
                    'email_title' => $armodel['Armodel']['email_title'],
                    'email_content' => $armodel['Armodel']['email_content'],
                    'sms_content' => $armodel['Armodel']['sms_content'],
                    'courrier_id' => $fluxId
                )
            );

            if ($armodel['Armodel']['format'] == "ott") {
                $gedoooResult = $this->_generate($fluxId, array('name' => 'AR ' . $armodel['Armodel']['name'], 'content' => $armodel['Armodel']['content']), null, $this->Session->read('Auth.User.id'));

                // Conversion en PDF
                $ar['Ar']['content'] = $gedoooResult['content'];
                $ar['Ar']['size'] = $gedoooResult['size'];
                $ar['Ar']['mime'] = $gedoooResult['mime'];
                $ar['Ar']['ext'] = $gedoooResult['ext'];
            } else if ($armodel['Armodel']['format'] == "video" || $armodel['Armodel']['format'] == "audio") {
//TODO: prendre en charge la gestion de la ged
                $ar['Ar']['content'] = $armodel['Armodel']['content'];
                $ar['Ar']['size'] = $armodel['Armodel']['size'];
                $ar['Ar']['mime'] = $armodel['Armodel']['mime'];
                $ar['Ar']['ext'] = $armodel['Armodel']['ext'];
            }

            $this->Courrier->Ar->create();
            $saved = $this->Courrier->Ar->save($ar);

            $fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'], true, 0777);
            $name  = preg_replace( "/[:']/", "_", $ar['Ar']['name'] );
            $name = str_replace( '/', '_', $name );
            $file = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'] . DS . "{$name}";
            file_put_contents( $file, $ar['Ar']['content'] );
            $path = $file;


            if ($saved) {
                $documentAr = array(
                    'Document' => array(
                        'name' => $ar['Ar']['name'],
                        'ext' => $ar['Ar']['ext'],
                        'size' => $ar['Ar']['size'],
                        'main_doc' => false,
                        'desktop_creator_id' => $this->Session->read('Auth.User.id'),
                        'courrier_id' => $fluxId,
//                        'content' => $ar['Ar']['content'],
                        'path' => $path,
                        'mime' => $ar['Ar']['mime']
                    )
                );

                $this->Courrier->Document->create($documentAr);
                $saved = $this->Courrier->Document->save() && $saved;


                if ($saved) {
// Ajout du fichier généré dans le répertoire du webdav pour le webdav
                    $sessionFluxIdDir = WEBDAV_DIR . DS . $conn . DS .$fluxId;
                    $Folder = new Folder($sessionFluxIdDir, true, 0777);
                    $generatedFile = file_put_contents($sessionFluxIdDir . DS . $documentAr['Document']['name'], file_get_contents( $file ) );
                }
            }


            if (!empty($saved)) {
                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (génération de l'AR ".$ar['Ar']['name'].") le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);


                $this->Courrier->Ar->commit();
                $this->Jsonmsg->valid(__d('ar', 'Ar.generated.ok'));
            } else {
                $this->Courrier->Ar->rollback();
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Génération d'un document réponse à partir du modèle de document initial et des éléments (webdav) de réponse (composants de réponse)
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function genResponse() {
        $conn = $this->Session->read('Auth.Collectivite.conn');
        $this->set('conn', $conn );
        if ($this->request->data) {
            $this->Jsonmsg->init();
            $fluxId = $this->request->data['ResponseDoc']['courrier_id'];
            if (isset($this->request->data['ResponseDoc']['rmodel_id'])) {
                $rmodelId = $this->request->data['ResponseDoc']['rmodel_id'];
            }
            if (isset($this->request->data['ResponseDoc']['gabarit_id'])) {
                $gabaritId = $this->request->data['ResponseDoc']['gabarit_id'];
            }

            if (!empty($gabaritId)) {
                $this->Gabaritdocument = ClassRegistry::init('Gabaritdocument');
                $this->Gabaritdocument->id = $gabaritId;
                $gabaritInfo = $this->Gabaritdocument->find('first', array('recursive' => -1, 'fields' => array('Gabaritdocument.path', 'Gabaritdocument.id'), 'conditions' => array('Gabaritdocument.id' => $gabaritId)));
                $rmodel = array(
                    'name' => $this->Gabaritdocument->field('name', array('id' => $gabaritId)),
                    'content' => file_get_contents( $gabaritInfo['Gabaritdocument']['path'] )
                );
            } else if (!empty($rmodelId)) {
                $this->Courrier->Soustype->Rmodel->id = $rmodelId;
                $rmodelInfo = $this->Courrier->Soustype->Rmodel->find('first', array('recursive' => -1, 'fields' => array('Rmodel.content', 'Rmodel.id'), 'conditions' => array('Rmodel.id' => $rmodelId)));
                $rmodel = array(
                    'name' => $this->Courrier->Soustype->Rmodel->field('name', array('id' => $rmodelId)),
                    'content' => $rmodelInfo['Rmodel']['content']
                );
            }

            $relements = array();
            if (!empty($this->request->data['ResponseDoc']['Relement'])) {
                foreach ($this->request->data['ResponseDoc']['Relement'] as $key => $val) {
                    if ($val == 1) {
                        $name = $this->Courrier->Relement->field('Relement.name', array('Relement.id' => $key));
                        if (!empty($name)) {
                            $tab = array(
                                'id' => $key,
                                'name' => $name,
                                'content' => file_get_contents(WEBDAV_DIR . DS . $conn . DS .$fluxId . DS . $name)
                            );
                            $relements[] = $tab;
                        }
                    }
                }
            }

            $name = $rmodel['name'];
            $name = preg_replace("/[: ]/", "_", $name);
            if (Configure::read('Conf.SAERP')) {
                $valueName = date('Ymd_His') . '_' . trim($name) . ".odt";
            } else {
                $valueName = date('Ymd_His') . '_' . $name;
            }
            $extension = substr($name, -3);
            if( $extension != 'odt' ) {
                $valueName = date('Ymd_His') . '_' . $name . ".odt";
            }
            $document = array(
                'Document' => array(
                    'name' => $valueName,
                    'courrier_id' => $fluxId
                )
            );
            $gedoooResult = $this->_generate($fluxId, $rmodel, $relements, $this->Session->read('Auth.User.id'));

             $courrier = $this->Courrier->find(
                'first',
                array(
                    'conditions' => array(
                        'Courrier.id' => $fluxId
                    ),
                    'contain' => false,
                    'recursive' => -1
                )
            );
            $fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
            $name  = preg_replace( "/[:']/", "_", $valueName );
            $name = str_replace( '/', '_', $name );
            $file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
            file_put_contents( $file, $gedoooResult['content']);

            $gedooResultPath = $file;
            if (!empty($gedoooResult)) {
//                $document['Document']['content'] = $gedoooResult['content'];
                $document['Document']['path'] = $gedooResultPath;
                $document['Document']['size'] = $gedoooResult['size'];
                $document['Document']['mime'] = $gedoooResult['mime'];
                $document['Document']['ext'] = $gedoooResult['ext'];
// Passage en doc principal pour les modèles de réponse générés
// car ils finiront par être adressés au parapheur
                $document['Document']['main_doc'] = true;
// Seul les créateurs de doc peuvent les supprimer
                $document['Document']['desktop_creator_id'] = $this->Session->read('Auth.User.id');

                $docPrincipalFlux = $this->Courrier->getMainDoc($fluxId);
                if (!empty($docPrincipalFlux)) {
                    $this->Courrier->Document->updateAll(
                            array('Document.main_doc' => false), array('Document.id' => $docPrincipalFlux['Document']['id'])
                    );
                }
                $this->Courrier->Document->create($document);
                $docSaved = $this->Courrier->Document->save();
				$this->Session->write('Session.DocToSendId', null );
                if (!empty($docSaved)) {
                	// On stocke l'ID du document nouvellement généré pour l'envoi au IP (si aucun rechargement de apge n'est effectué)
					$docId = $this->Courrier->Document->id;
					$this->Session->write('Session.DocToSendId', $docId );
                    $flux = $this->Courrier->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Courrier.id' => $fluxId
                            ),
                            'contain' => false,
                            'recursive' => -1
                        )
                    );
                    $this->loadModel('Journalevent');
                    $datasSession = $this->Session->read('Auth.User');
                    $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (génération de la réponse ".$document['Document']['name'].") le ".date('d/m/Y à H:i:s');
                    $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);

                    $this->Jsonmsg->valid();
                }

// Ajout du fichier généré dans le répertoire du webdav pour le webdav
                $sessionFluxIdDir = WEBDAV_DIR . DS . $conn . DS .$fluxId;
                $Folder = new Folder($sessionFluxIdDir, true, 0777);
                $generatedFile = file_put_contents($sessionFluxIdDir . DS . $document['Document']['name'], file_get_contents($file));
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Génération de document odt via GED'OOo
     *
     * @access private
     * @param integer $courrier_id identifiant du flux
     * @param integer $soustype_id identifiant du sous-type
     * @return boolean
     */
    private function _generate($courrier_id, $model, $elements = null, $user_id) {
        $return = array();

//generation du fichier avec GedOOo
        App::import('Vendor', 'GDO_Utility', array('file' => 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_Utility.class'));
        App::import('Vendor', 'GDO_FieldType', array('file' => 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_FieldType.class'));
        App::import('Vendor', 'GDO_ContentType', array('file' => 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_ContentType.class'));
        App::import('Vendor', 'GDO_IterationType', array('file' => 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_IterationType.class'));
        App::import('Vendor', 'GDO_PartType', array('file' => 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_PartType.class'));
        App::import('Vendor', 'GDO_FusionType', array('file' => 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_FusionType.class'));
        App::import('Vendor', 'GDO_MatrixType', array('file' => 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_MatrixType.class'));
        App::import('Vendor', 'GDO_MatrixRowType', array('file' => 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_MatrixRowType.class'));
        App::import('Vendor', 'GDO_AxisTitleType', array('file' => 'GEDOOo' . DS . 'phpgedooo' . DS . 'GDO_AxisTitleType.class'));

        $u = new GDO_Utility();
//*****************************************
// Organisation des données
//*****************************************
        $oMainPart = new GDO_PartType();

        $querydata = array(
            'fields' => array_merge(
                    $this->Courrier->fields(), $this->Courrier->Contact->fields(), $this->Courrier->Contact->Ban->fields(), $this->Courrier->Contact->Bancommune->fields(), $this->Courrier->Organisme->fields(),
                    $this->Courrier->Soustype->fields(), $this->Courrier->Service->fields(), $this->Courrier->Soustype->Type->fields(), $this->Courrier->Soustype->Circuit->fields(), $this->Courrier->Affairesuivie->fields(), $this->Courrier->Affairesuivie->User->fields(), array(
                'Courrier.name',
                'Courrier.objet',
                'Courrier.date',
                'Courrier.datereception',
                'Courrier.reference',
                'Contact.name',
                'Type.name',
                'Soustype.name',
                'Soustype.delai_nb',
                'Soustype.delai_unite',
                'Soustype.mailservice',
                'Circuit.nom',
                'Dossier.name',
                'Affaire.name',
                'Titre.name',
                'Origineflux.name'
                    )
            ),
            'contain' => false,
            'joins' => array(
                $this->Courrier->join($this->Courrier->Contact->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->Contact->join($this->Courrier->Contact->Ban->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->Contact->join($this->Courrier->Contact->Ban->Bancommune->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Organisme->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Soustype->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Service->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->Soustype->join($this->Courrier->Soustype->Type->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->Soustype->join($this->Courrier->Soustype->Circuit->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Affairesuivie->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->Affairesuivie->join($this->Courrier->Affairesuivie->User->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Affaire->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->Affaire->join($this->Courrier->Affaire->Dossier->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->Contact->join($this->Courrier->Contact->Titre->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Origineflux->alias, array('type' => 'LEFT OUTER'))
            ),
            'conditions' => array(
                'Courrier.id' => $courrier_id
            )
        );
        $courrier = $this->Courrier->find('first', $querydata);

// Récuéprationd e sifnos de l'affaire suviie par
        $affairesuivieDesktop = $this->Courrier->Desktop->find('first', array(
            'conditions' => array(
                'Desktop.id' => $courrier['Courrier']['affairesuiviepar_id']
            ),
            'recursive' => -1
        ));


//debug($affairesuivieDesktop);

        $this->User = ClassRegistry::init('User');
        $userAffaire = $this->User->find('first', array(
            'fields' => array(
                'User.nom',
                'User.prenom',
                'User.mail',
                'User.numtel',
                'User.username'
            ),
            'conditions' => array(
                'User.desktop_id' => $courrier['Courrier']['affairesuiviepar_id']
            ),
            'recursive' => -1
        ));

        if (!empty($userAffaire)) {
            $courrier['Affairesuivie']['name'] = $userAffaire['User']['prenom'] . ' ' . $userAffaire['User']['nom'];
            $courrier['Affairesuivie']['tel'] = $userAffaire['User']['numtel'];
            $courrier['Affairesuivie']['mail'] = $userAffaire['User']['mail'];
            $extractName = explode(" ", $courrier['Affairesuivie']['name']);
            $initials = '';
            foreach ($extractName as $extract) {
				$extract = replace_accents($extract);
                $initials.=strtoupper(substr($extract, 0, 1));
            }
            $affairesuivieInits = $initials;
            $courrier['Affairesuivie']['initiales'] = $affairesuivieInits;
        } else {
            $secondaryDesktop = $this->Courrier->Desktop->DesktopsUser->find('first', array(
                'fields' => array(
                    'DesktopsUser.user_id'
                ),
                'conditions' => array(
                    'DesktopsUser.desktop_id' => $courrier['Courrier']['affairesuiviepar_id']
                ),
                'contain' => false
            ));
            if (!empty($secondaryDesktop)) {
                $secondaryDesktopUser = $this->User->find('first', array(
                    'fields' => array(
                        'User.nom',
                        'User.prenom',
                        'User.mail',
                        'User.numtel',
                        'User.username'
                    ),
                    'conditions' => array(
                        'User.id' => $secondaryDesktop['DesktopsUser']['user_id']
                    ),
                    'recursive' => -1
                ));

                if (!empty($secondaryDesktopUser)) {
                    $courrier['Affairesuivie']['name'] = $secondaryDesktopUser['User']['prenom'] . ' ' . $secondaryDesktopUser['User']['nom'];
                    $courrier['Affairesuivie']['tel'] = $secondaryDesktopUser['User']['numtel'];
                    $courrier['Affairesuivie']['mail'] = $secondaryDesktopUser['User']['mail'];
                    $extractName = explode(" ", $courrier['Affairesuivie']['name']);
                    $initials = '';
                    foreach ($extractName as $extract) {
                        $initials.=strtoupper(substr($extract, 0, 1));
                    }
                    $affairesuivieInits = $initials;
                    $courrier['Affairesuivie']['initiales'] = $affairesuivieInits;
                }
            }
        }

        $metadonnees = $this->Courrier->CourrierMetadonnee->find('all', array(
            'fields' => array_merge(
                    $this->Courrier->CourrierMetadonnee->fields(), $this->Courrier->CourrierMetadonnee->Metadonnee->fields()
            ),
            'conditions' => array(
                'CourrierMetadonnee.courrier_id' => $courrier_id
            ),
            'joins' => array(
                $this->Courrier->CourrierMetadonnee->join('Metadonnee', array('type' => 'LEFT OUTER'))
            ),
            'recursive' => -1,
            'order' => array('Metadonnee.name ASC')
        ));
        if (!empty($metadonnees)) {
            $metas = array();
            $courrier['Courrier']['listemetadonnees'] = implode(', ', Hash::extract($metadonnees, '{n}.Metadonnee.name'));
            foreach ($metadonnees as $key => $metadonnee) {
                if ($metadonnee['Metadonnee']['typemetadonnee_id'] == 3) {
                    if ($metadonnee['CourrierMetadonnee']['valeur'] == 1) {
                        $metadonnee['CourrierMetadonnee']['valeur'] = 'Réalisé';
                    } else if ($metadonnee['CourrierMetadonnee']['valeur'] == 'Oui') {
                        $metadonnee['CourrierMetadonnee']['valeur'] = 'Oui';
                    } else if ($metadonnee['CourrierMetadonnee']['valeur'] == 'Non') {
                        $metadonnee['CourrierMetadonnee']['valeur'] = 'Non';
                    } else {
                        $metadonnee['CourrierMetadonnee']['valeur'] = 'Non réalisé';
                    }
                } else if ($metadonnee['Metadonnee']['typemetadonnee_id'] == Configure::read('Selectvaluemetadonnee.id')) {
                    $selectvaluemetadonnee = $this->Courrier->Metadonnee->Selectvaluemetadonnee->find('first', array(
                        'fields' => array(
                            'Selectvaluemetadonnee.name'
                        ),
                        'conditions' => array(
                            'Selectvaluemetadonnee.id' => $metadonnee['CourrierMetadonnee']['valeur']
                        ),
                        'contain' => false
                    ));
                    if (!empty($selectvaluemetadonnee)) {
                        $metadonnee['CourrierMetadonnee']['valeur'] = $selectvaluemetadonnee['Selectvaluemetadonnee']['name'];
                    }
                }
                if (!empty($metadonnee['Metadonnee']['champfusion'])) {
                    $name = $metadonnee['Metadonnee']['champfusion'];
                    $metas['valeurmet'][$key] = $metadonnee['CourrierMetadonnee']['valeur'];
                    $oMainPart->addElement(new GDO_FieldType("metadonnee_{$name}", $metas['valeurmet'][$key], 'text'));
                } else {
                    $metas['valeurmet'][$key] = $metadonnee['Metadonnee']['name'] . ': ' . $metadonnee['CourrierMetadonnee']['valeur'];
                    $oMainPart->addElement(new GDO_FieldType("metadonnee_{$key}_name", $metas['valeurmet'][$key], 'text'));
                }
//debug($selectvaluemetadonnee);
//debug($name);
            }
            $courrier['Courrier']['listemetadonnees'] = implode(', ', Hash::extract($metas, 'valeurmet'));
        }

        $user = $this->Courrier->User->find('first', array(
            'conditions' => array(
                'User.id' => $user_id
            ),
            'contain' => array(
                'Desktop' => array(
                    'Service'
                )
            )
        ));

        $courrier = Hash::merge($courrier, $user);

// Gestion des différents services liés à l'utilisateur
        $type_id = $courrier['Type']['id'];
        $soustype_id = $courrier['Soustype']['id'];
// en fonction du sous-type, on regarde les droits attribués à ce sous-type
        $daco = $this->Daco->find('first', array(
            'conditions' => array(
                'Daco.foreign_key' => $soustype_id
            ),
            'contain' => false
        ));
        if (!empty($daco)) {
            $daco_id = $daco['Daco']['id'];
        }

// On regarde les services associés au(x) bureau(x) de l'utilisateur
        $servicesIds = Hash::extract($courrier, 'Desktop.Service.{n}.id');

// pour chaque service, on regarde les droits qui lui sont attribués
        // on cherche les services enfants des services associés
        $enfants = array();
        foreach( $servicesIds as $service_id) {
            $enfants = $this->Courrier->Service->getServiceEnfants( $service_id );
        }

        $servenfants = new GDO_IterationType("enfant");
        foreach( $enfants as $enfant ) {
            $oDevPart = new GDO_PartType();
            $oDevPart->addElement(new GDO_FieldType("service_enfant", $enfant, 'text'));
            $servenfants->addPart($oDevPart);
        }
        $oMainPart->addElement($servenfants);

        $daros = $this->Daro->find('all', array(
            'conditions' => array(
                'Daro.foreign_key' => $servicesIds
            ),
            'contain' => false
        ));


        $bannettes = $this->Courrier->getCycle( $courrier_id );
        foreach( $bannettes['bannettes'] as $b => $bannette ) {
            // Gestion des comentaires repris pour le cycle de vie (ubniquement les commentaires publics)
            $comments = $this->Courrier->getComments($courrier_id);
            $bannettes['bannettes'][$b]['Bancontenu']['commentaire'] = '';
            if( !empty($comments) ) {
//debug($comments['all']);
                 foreach ($comments['all'] as $key => $comment) {
                    for( $j=0; $j < count( $bannettes['bannettes'] ); $j++) {
                        if( $bannettes['bannettes'][$j]['Bancontenu']['created'] === $comment['Comment']['created'] ) {
                            $bannettes['bannettes'][$j-1]['Bancontenu']['commentaire'] = @$comment['Comment']['objet'];
                        }
                        if( $bannettes['bannettes'][$j]['Bancontenu']['desktop_id'] == $comment['Owner']['id'] && $bannettes['bannettes'][$j]['Bancontenu']['created'] <= $comment['Comment']['created'] ) {
							$bannettes['bannettes'][$j+1]['Bancontenu']['commentaire'] = @$comment['Comment']['objet'];
                        }
                    }
                }
            }
            else {
                $bannettes['bannettes'][$b]['Bancontenu']['commentaire'] = '';
            }
        }
//debug($bannettes);
//debug($visas);
//die();
        $cycle = new GDO_IterationType("acteurs");
        foreach( $bannettes['bannettes'] as $b => $bannette ) {

            $oDevPart = new GDO_PartType();
            // Utilisateur
            $oDevPart->addElement(new GDO_FieldType("acteur_name", $bannette['Desktop']['name'], 'text'));
            // Circuit / Destinataire
            $oDevPart->addElement(new GDO_FieldType("acteur_etape", isset( $bannette['Bancontenu']['cible'] ) ? $bannette['Bancontenu']['cible'] : null, 'text'));
            $oDevPart->addElement(new GDO_FieldType("acteur_service", isset( $bannette['Desktop']['Desktopmanager'] ) ? $bannette['Desktop']['Desktopmanager'][0]['name'] : null, 'text'));
            // Date darrivée
            $datecreated = date("d/m/Y \à H:i", strtotime($bannette['Bancontenu']['created']) );
            $oDevPart->addElement(new GDO_FieldType("acteur_date", $datecreated, 'text'));
            if( isset($bannette['Bancontenu']['validated']) && !empty( $bannette['Bancontenu']['validated'] ) ) {
                $datevalidated = date("d/m/Y \à H:i", strtotime($bannette['Bancontenu']['validated']) );
            }
            else {
                $datevalidated = '';
            }
            // Date de traitement
            $oDevPart->addElement(new GDO_FieldType("acteur_valid", !empty( $datevalidated ) ? $datevalidated : null, 'text'));
            // action
            if( isset($bannette['Bancontenu']['rebondir']) && !empty( $bannette['Bancontenu']['rebondir'] ) ) {
                $action = $bannette['Bancontenu']['rebondir'];
            }
            else if( isset($bannette['Bancontenu']['copie']) && !empty( $bannette['Bancontenu']['copie'] ) ) {
                $action = $bannette['Bancontenu']['copie'];
            }
            else if( isset($bannette['Bancontenu']['insertion'] ) && !empty($bannette['Bancontenu']['insertion']) ) {
                $action = $bannette['Bancontenu']['insertion'];
            }
            else if( isset($bannette['Bancontenu']['clore'] ) && !empty($bannette['Bancontenu']['clore']) ) {
                $action = $bannette['Bancontenu']['clore'];
            }
            else if( isset($bannette['Bancontenu']['validation'] ) && !empty($bannette['Bancontenu']['validation']) ) {
                $action = $bannette['Bancontenu']['validation'];
            }
            else {
                $action = '';
            }
            // Envoi pour copie
            if( $bannette['Bancontenu']['bannette_id'] == BAN_COPY ) {
                $copie = $bannette['Bancontenu']['cible'];
            }
            else {
                $copie = '';
            }

            // Bureau
            $oDevPart->addElement(new GDO_FieldType("acteur_bureau", $bannette['Desktop']['Desktopmanager'][0]['name'], 'text'));
            // Action
            $oDevPart->addElement(new GDO_FieldType("acteur_action", !empty( $action ) ? $action : null, 'text'));
            // commentaire
            $oDevPart->addElement(new GDO_FieldType("acteur_commentaire", !empty($bannette['Bancontenu']['commentaire']) ? $bannette['Bancontenu']['commentaire'] : null, 'text'));
            // Envoi pour copie
            $oDevPart->addElement(new GDO_FieldType("acteur_copie", !empty($copie) ? $copie : null, 'text'));
            $cycle->addPart($oDevPart);
        }
        $oMainPart->addElement($cycle);


        if (!empty($daros)) {
            $darosids = Hash::extract($daros, '{n}.Daro.id');
        }

        if (!empty($darosids) && !empty($daco_id)) {
            $this->DarosDaco = ClassRegistry::init('DarosDaco');
// pour chaque droit du service (darosids), on vérifie si les droits d'accès au sous -type (daco_id) sont autorisés pour ce(s) service(s)
            $darodaco = $this->DarosDaco->find('first', array(
                'conditions' => array(
                    'DarosDaco.daco_id' => $daco_id,
                    'DarosDaco.daro_id' => $darosids
                ),
                'contain' => false
            ));
            if (!empty($darodaco)) {
                $darodaco_service_id = $darodaco['Daro']['foreign_key'];
            }
        }

        // les banenttes d'insertion OU de réponse
        if( Configure::read('CD') == 81 ) {
            $bannettesIds = array( 1, 4 );
        }
        else {
            $bannettesIds = array( 1 );
        }

        $bancontenu = $this->Courrier->Bancontenu->find(
            'all',
            array(
                'conditions' => array(
                    'Bancontenu.courrier_id' => $courrier['Courrier']['id'],
                    'Bancontenu.bannette_id' => $bannettesIds
                ),
                'contain' => false,
                'order' => array(
                    'Bancontenu.created ASC'
                ),
                'recursive' => -1
            )
        );

        $serviceInitName = '';
        $serviceInitMail = '';
        if( !empty( $bancontenu[0]['Bancontenu']['desktop_id']) ) {
            $desktop = $this->Courrier->Desktop->find(
                'first',
                array(
                    'conditions' => array('Desktop.id' => $bancontenu[0]['Bancontenu']['desktop_id']),
                    'contain' => array('Service'),
                    'recursive' => -1
                )
            );
            $this->Service = ClassRegistry::init('Service');
            $service = $this->Service->find('first', array(
                'conditions' => array(
                    'Service.id' => $desktop['Service'][0]['id']
                ),
                'contain' => false
            ));
            $serviceInitName = $service['Service']['name'];
			$serviceInitMail = $service['Service']['email'];
        }

		// si le champ "Affaire suvie par" est renseigné, on récupère e nom et le mail du service
		if( !empty($courrier['Courrier']['affairesuiviepar_id']) ) {
			$desktop = $this->Courrier->Desktop->find(
				'first',
				array(
					'conditions' => array('Desktop.id' => $courrier['Courrier']['affairesuiviepar_id']),
					'contain' => array('Service'),
					'recursive' => -1
				)
			);
			$this->Service = ClassRegistry::init('Service');
			$service = $this->Service->find('first', array(
				'conditions' => array(
					'Service.id' => $desktop['Service'][0]['id']
				),
				'contain' => false
			));
			$serviceInitName = $service['Service']['name'];
			$serviceInitMail = $service['Service']['email'];
		}



		if ( !empty($serviceInitName) ) {
            $serviceName = $serviceInitName;
            $serviceMail = $serviceInitMail;
        }
        else if (empty( $bancontenu[0]['Bancontenu']['desktop_id']) && !empty($darodaco_service_id)) {
// on récupère donc l'ID du service concerné par le sous-type défini et on retourne son nom
            $this->Service = ClassRegistry::init('Service');
            $service = $this->Service->find('first', array(
                'conditions' => array(
                    'Service.id' => $darodaco_service_id
                ),
                'contain' => false
            ));
            $serviceName = $service['Service']['name'];
            $serviceMail = $service['Service']['email'];
        }
        else {
            $serviceName = '';
            $serviceMail = '';
        }

		// Valeur du service enregistré à la création
		if ( !empty($courrier['Courrier']['service_creator_id']) && !Configure::read('ServiceMailToDisplayIntoResponseDoc.Affairesuiviepar') ) {
			$this->Service = ClassRegistry::init('Service');
			$service = $this->Service->find('first', array(
				'conditions' => array(
					'Service.id' => $courrier['Courrier']['service_creator_id']
				),
				'contain' => false
			));
			$serviceName = $service['Service']['name'];
			$serviceMail = $service['Service']['email'];
		}

		if( Configure::read('Soustype.MailForService') ) {
			if( !empty($courrier['Soustype']['mailservice'] ) ) {
				$serviceMail = $courrier['Soustype']['mailservice'];
			}
		}


// Infos sur la collectivité
        $collectivite = ClassRegistry::init('Collectivite')->find('first', array(
            'fields' => array(
                'Collectivite.name',
                'Collectivite.logo',
                'Collectivite.siren',
                'Collectivite.adresse',
                'Collectivite.codeinsee',
                'Collectivite.codepostal',
                'Collectivite.ville',
                'Collectivite.complementadresse',
                'Collectivite.telephone'
            ),
            'conditions' => array(
                'Collectivite.conn' => $this->Session->read('Auth.User.connName')
            ),
            'contain' => false
        ));

// Gestion du cas de l'adresse reprise sur le document
        if (empty($courrier['Contact']['id'])) {
            $courrier['Contact']['adresse'] = $courrier['Organisme']['adressecomplete'];
            $courrier['Contact']['cp'] = $courrier['Organisme']['cp'];
            $courrier['Contact']['ville'] = $courrier['Organisme']['ville'];
        }

        $civiliteOptions = $this->Session->read( 'Auth.Civilite');
        $civilite = Hash::get($civiliteOptions, $courrier['Contact']['civilite']);
//debug($civilite);
//die();
// Information sur le courrier envoyée à Gedooo
        $objet =  $courrier['Courrier']['objet'];
        if( strpos( $objet, '<!DOCTYPE') !== false || strpos( $objet, '<html>') !== false || strpos( $objet, '--_000') !== false ) {
            $objet = strip_tags( $objet, "<html>" );
            $substring = substr($objet ,strpos($objet ,"<style"),strpos($objet ,"</style>"));
            $objet = str_replace($substring,"",$objet);
            $objet = trim($objet);
        }
        $oMainPart->addElement(new GDO_FieldType('courrier_name', $courrier['Courrier']['name'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('courrier_objet', $objet, 'text'));
        $oMainPart->addElement(new GDO_FieldType('courrier_reference', $courrier['Courrier']['reference'], 'text'));

        $delaiuniteCourrier = '';
        if(isset($courrier['Courrier']['delai_unite']) ) {
            if ($courrier['Courrier']['delai_unite'] == '0') {
                $delaiuniteCourrier = 'jour(s)';
            }
            else if ($courrier['Courrier']['delai_unite'] == '1') {
                $delaiuniteCourrier = 'semaine(s)';
            }
            else if ($courrier['Courrier']['delai_unite'] == '2') {
                $delaiuniteCourrier = 'mois';
            }
        }
        $oMainPart->addElement(new GDO_FieldType('courrier_delai_nb', isset($courrier['Courrier']['delai_nb']) ? $courrier['Courrier']['delai_nb'] : null, 'text'));
        $oMainPart->addElement(new GDO_FieldType('courrier_delai_unite', $delaiuniteCourrier, 'text'));

         if ($courrier['Courrier']['direction'] == '1') {
            $courrier['Courrier']['direction'] = 'Entrant';
        } else {
            $courrier['Courrier']['direction'] = 'Sortant';
        }
        $oMainPart->addElement(new GDO_FieldType('courrier_direction', $courrier['Courrier']['direction'], 'text'));
// Affaire suivie par
        $oMainPart->addElement(new GDO_FieldType('affairesuivie_name', $courrier['Affairesuivie']['name'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('affairesuivie_tel', isset($courrier['Affairesuivie']['tel']) ? $courrier['Affairesuivie']['tel'] : '', 'text'));
        $oMainPart->addElement(new GDO_FieldType('affairesuivie_mail', isset($courrier['Affairesuivie']['mail']) ? $courrier['Affairesuivie']['mail'] : '', 'text'));
        $oMainPart->addElement(new GDO_FieldType('affairesuivie_ini', isset($courrier['Affairesuivie']['initiales']) ? $courrier['Affairesuivie']['initiales'] : '', 'text'));

        $date_tmp = explode("-", $courrier['Courrier']['date']);
        $date = $date_tmp[2] . "/" . $date_tmp[1] . "/" . $date_tmp[0];
        $oMainPart->addElement(new GDO_FieldType('courrier_date', $date, 'date'));

        $date_reception_tmp = explode("-", $courrier['Courrier']['datereception']);
        $date_reception = $date_reception_tmp[2] . "/" . $date_reception_tmp[1] . "/" . $date_reception_tmp[0];
        $oMainPart->addElement(new GDO_FieldType('courrier_datereception', $date_reception, 'date'));

        $oMainPart->addElement(new GDO_FieldType('courrier_emetteur', $courrier['Contact']['name'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('courrier_type', $courrier['Type']['name'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('courrier_soustype', $courrier['Soustype']['name'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('courrier_circuit', $courrier['Circuit']['nom'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('service_name', isset($serviceName) ? $serviceName : $courrier['Service']['name'], 'text'));
		$oMainPart->addElement(new GDO_FieldType('service_email', isset($serviceMail) ? $serviceMail : $courrier['Service']['email'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('dossier_name', $courrier['Dossier']['name'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('affaire_name', $courrier['Affaire']['name'], 'text'));
        if (!empty($metadonnees)) {
            $oMainPart->addElement(new GDO_FieldType('courrier_metadonnee', $courrier['Courrier']['listemetadonnees'], 'text'));
        }

        if ($courrier['Contact']['nom'] == '0Sans contact') {
            $contactName = '';
        } else {
            $contactName = $courrier['Contact']['name'];
        }

// Information sur la personne ayant contacté la collectivité
        $oMainPart->addElement(new GDO_FieldType('contact_name', $contactName, 'text'));
//        $oMainPart->addElement(new GDO_FieldType('contactinfo_civilite', $courrier['Contact']['civilite'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_civilite', $civilite, 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_nom', !empty($contactName) ? $courrier['Contact']['nom'] : '', 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_prenom', !empty($contactName) ? $courrier['Contact']['prenom'] : '', 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_numvoie', $courrier['Contact']['numvoie'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_nomvoie', $courrier['Contact']['nomvoie'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_adresse', stripslashes($courrier['Contact']['adresse']), 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_compl', $courrier['Contact']['compl'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_cp', $courrier['Contact']['cp'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_email', $courrier['Contact']['email'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_ville', $courrier['Contact']['ville'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_departement', $courrier['Ban']['name'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_pays', $courrier['Contact']['pays'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_canton', $courrier['Contact']['canton'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_compl2', $courrier['Contact']['compl2'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_cedex', $courrier['Contact']['cedex'], 'text'));

        $oMainPart->addElement(new GDO_FieldType('contactinfo_tel', $courrier['Contact']['tel'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('contactinfo_role', $courrier['Contact']['role'], 'text'));
        if( isset($courrier['Contact']['titre_id']) && !empty($courrier['Contact']['titre_id']) ) {
            $oMainPart->addElement(new GDO_FieldType('contactinfo_titre', $courrier['Titre']['name'], 'text'));
        }else {
            $oMainPart->addElement(new GDO_FieldType('contactinfo_titre', $courrier['Contact']['titre'], 'text'));
        }

        if ($courrier['Organisme']['name'] == 'Sans organisme') {
            $organismeName = '';
        } else {
            $organismeName = $courrier['Organisme']['name'];
        }

        $oMainPart->addElement(new GDO_FieldType('contactinfo_organisation', $organismeName, 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_name', $organismeName, 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_adressecomplete', stripslashes($courrier['Organisme']['adressecomplete']), 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_numvoie', $courrier['Organisme']['numvoie'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_nomvoie', $courrier['Organisme']['nomvoie'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_compl', $courrier['Organisme']['compl'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_cp', $courrier['Organisme']['cp'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_ville', $courrier['Organisme']['ville'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_email', $courrier['Organisme']['email'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_tel', $courrier['Organisme']['tel'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_portable', $courrier['Organisme']['portable'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_fax', $courrier['Organisme']['fax'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_pays', $courrier['Organisme']['pays'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('organisme_canton', $courrier['Organisme']['canton'], 'text'));
		$oMainPart->addElement(new GDO_FieldType('organisme_antenne', $courrier['Organisme']['antenne'], 'text'));
		$oMainPart->addElement(new GDO_FieldType('organisme_compl2', $courrier['Organisme']['compl2'], 'text'));
		$oMainPart->addElement(new GDO_FieldType('organisme_cedex', $courrier['Organisme']['cedex'], 'text'));

// Information sur l'utilsiateur connecté
        $oMainPart->addElement(new GDO_FieldType('user_username', $user['User']['username'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('user_nom', $user['User']['nom'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('user_prenom', $user['User']['prenom'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('user_mail', $user['User']['mail'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('user_numtel', $user['User']['numtel'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('desktop_name', $user['Desktop']['name'], 'text'));

// Information sur la collectivité de l'utilsiateur
        $oMainPart->addElement(new GDO_FieldType('collectivite_name', $collectivite['Collectivite']['name'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('collectivite_logo', $collectivite['Collectivite']['logo'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('collectivite_siren', $collectivite['Collectivite']['siren'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('collectivite_adresse', $collectivite['Collectivite']['adresse'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('collectivite_codeinsee', $collectivite['Collectivite']['codeinsee'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('collectivite_codepostal', $collectivite['Collectivite']['codepostal'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('collectivite_ville', $collectivite['Collectivite']['ville'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('collectivite_complementadresse', $collectivite['Collectivite']['complementadresse'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('collectivite_telephone', $collectivite['Collectivite']['telephone'], 'text'));

         // Référence du flux d'origine
        if( !empty($courrier['Courrier']['parent_id'] )) {
            $courrierParent = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $courrier['Courrier']['parent_id']), 'contain' => false ) );
            $referenceOrigine = $courrierParent['Courrier']['reference'];

        }
        else {
            $referenceOrigine = '';
        }
        $oMainPart->addElement(new GDO_FieldType('courrier_referenceparent', $referenceOrigine, 'text'));


// Information sur le type/sous type du courrier
        $delaiunite = '';
        if ($courrier['Soustype']['delai_unite'] == '0') {
            $delaiunite = 'jour(s)';
        } else if ($courrier['Soustype']['delai_unite'] == '1') {
            $delaiunite = 'semaine(s)';
        }
        if ($courrier['Soustype']['delai_unite'] == '2') {
            $delaiunite = 'mois';
        }

        $direction = 'entrant';
        if ($courrier['Soustype']['entrant'] == '0') {
            $direction = 'sortant';
        }
        $oMainPart->addElement(new GDO_FieldType('type_name', $courrier['Type']['name'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('soustype_name', $courrier['Soustype']['name'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('soustype_delai_nb', $courrier['Soustype']['delai_nb'], 'text'));
        $oMainPart->addElement(new GDO_FieldType('soustype_delai_unite', $delaiunite, 'text'));
        $oMainPart->addElement(new GDO_FieldType('soustype_direction', $direction, 'text'));
        $oMainPart->addElement(new GDO_FieldType('origineflux_name', $courrier['Origineflux']['name'], 'text'));

		//inclusion des fichiers webdav
        if (!empty($elements)) {
            $idToUse = '';
            foreach ($this->request->data['ResponseDoc']['Relement'] as $idrelement => $value) {
                if ($value == "1") {
                    $idToUse = $idrelement;
                }
            }
            $documents = new GDO_IterationType("documents");
            foreach ($elements as $element) {
                if ($element['id'] == $idToUse) {
                    $oDevPart = new GDO_PartType();

					$oDevPart->addElement(new GDO_FieldType('courrier_name', $courrier['Courrier']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('courrier_objet', $objet, 'text'));
					$oDevPart->addElement(new GDO_FieldType('courrier_reference', $courrier['Courrier']['reference'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('courrier_delai_nb', isset($courrier['Courrier']['delai_nb']) ? $courrier['Courrier']['delai_nb'] : null, 'text'));
					$oDevPart->addElement(new GDO_FieldType('courrier_delai_unite', $delaiuniteCourrier, 'text'));
					$oDevPart->addElement(new GDO_FieldType('courrier_direction', $courrier['Courrier']['direction'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('affairesuivie_name', $courrier['Affairesuivie']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('affairesuivie_tel', isset($courrier['Affairesuivie']['tel']) ? $courrier['Affairesuivie']['tel'] : '', 'text'));
					$oDevPart->addElement(new GDO_FieldType('affairesuivie_mail', isset($courrier['Affairesuivie']['mail']) ? $courrier['Affairesuivie']['mail'] : '', 'text'));
					$oDevPart->addElement(new GDO_FieldType('affairesuivie_ini', isset($courrier['Affairesuivie']['initiales']) ? $courrier['Affairesuivie']['initiales'] : '', 'text'));
					$oDevPart->addElement(new GDO_FieldType('courrier_date', $date, 'date'));
					$oDevPart->addElement(new GDO_FieldType('courrier_datereception', $date_reception, 'date'));
					$oDevPart->addElement(new GDO_FieldType('courrier_emetteur', $courrier['Contact']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('courrier_type', $courrier['Type']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('courrier_soustype', $courrier['Soustype']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('courrier_circuit', $courrier['Circuit']['nom'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('service_name', isset($serviceName) ? $serviceName : $courrier['Service']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('service_email', isset($serviceMail) ? $serviceMail : $courrier['Service']['email'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('dossier_name', $courrier['Dossier']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('affaire_name', $courrier['Affaire']['name'], 'text'));
					if (!empty($metadonnees)) {
						$oDevPart->addElement(new GDO_FieldType('courrier_metadonnee', $courrier['Courrier']['listemetadonnees'], 'text'));
					}
					$oDevPart->addElement(new GDO_FieldType('contact_name', $contactName, 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_civilite', $civilite, 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_nom', !empty($contactName) ? $courrier['Contact']['nom'] : '', 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_prenom', !empty($contactName) ? $courrier['Contact']['prenom'] : '', 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_numvoie', $courrier['Contact']['numvoie'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_nomvoie', $courrier['Contact']['nomvoie'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_adresse', stripslashes($courrier['Contact']['adresse']), 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_compl', $courrier['Contact']['compl'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_cp', $courrier['Contact']['cp'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_email', $courrier['Contact']['email'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_ville', $courrier['Contact']['ville'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_departement', $courrier['Ban']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_pays', $courrier['Contact']['pays'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_canton', $courrier['Contact']['canton'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_tel', $courrier['Contact']['tel'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_role', $courrier['Contact']['role'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_compl2', $courrier['Contact']['compl2'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('contactinfo_cedex', $courrier['Contact']['cedex'], 'text'));
					if( isset($courrier['Contact']['titre_id']) && !empty($courrier['Contact']['titre_id']) ) {
						$oDevPart->addElement(new GDO_FieldType('contactinfo_titre', $courrier['Titre']['name'], 'text'));
					}else {
						$oDevPart->addElement(new GDO_FieldType('contactinfo_titre', $courrier['Contact']['titre'], 'text'));
					}
					$oDevPart->addElement(new GDO_FieldType('contactinfo_organisation', $organismeName, 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_name', $organismeName, 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_adressecomplete', stripslashes($courrier['Organisme']['adressecomplete']), 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_numvoie', $courrier['Organisme']['numvoie'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_nomvoie', $courrier['Organisme']['nomvoie'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_compl', $courrier['Organisme']['compl'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_compl2', $courrier['Organisme']['compl2'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_cp', $courrier['Organisme']['cp'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_ville', $courrier['Organisme']['ville'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_email', $courrier['Organisme']['email'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_tel', $courrier['Organisme']['tel'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_portable', $courrier['Organisme']['portable'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_fax', $courrier['Organisme']['fax'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_pays', $courrier['Organisme']['pays'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_canton', $courrier['Organisme']['canton'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_antenne', $courrier['Organisme']['antenne'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('organisme_cedex', $courrier['Organisme']['cedex'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('user_username', $user['User']['username'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('user_nom', $user['User']['nom'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('user_prenom', $user['User']['prenom'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('user_mail', $user['User']['mail'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('user_numtel', $user['User']['numtel'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('desktop_name', $user['Desktop']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('collectivite_name', $collectivite['Collectivite']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('collectivite_logo', $collectivite['Collectivite']['logo'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('collectivite_siren', $collectivite['Collectivite']['siren'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('collectivite_adresse', $collectivite['Collectivite']['adresse'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('collectivite_codeinsee', $collectivite['Collectivite']['codeinsee'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('collectivite_codepostal', $collectivite['Collectivite']['codepostal'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('collectivite_ville', $collectivite['Collectivite']['ville'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('collectivite_complementadresse', $collectivite['Collectivite']['complementadresse'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('collectivite_telephone', $collectivite['Collectivite']['telephone'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('courrier_referenceparent', $referenceOrigine, 'text'));
					$oDevPart->addElement(new GDO_FieldType('type_name', $courrier['Type']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('soustype_name', $courrier['Soustype']['name'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('soustype_delai_nb', $courrier['Soustype']['delai_nb'], 'text'));
					$oDevPart->addElement(new GDO_FieldType('soustype_delai_unite', $delaiunite, 'text'));
					$oDevPart->addElement(new GDO_FieldType('soustype_direction', $direction, 'text'));
					$oDevPart->addElement(new GDO_FieldType('origineflux_name', $courrier['Origineflux']['name'], 'text'));

//$this->log($element );
//die();
                    $oDevPart->addElement(new GDO_ContentType("document", "", "application/vnd.oasis.opendocument.text", "binary", $element['content']));
                    $documents->addPart($oDevPart);
                }
            }
            $oMainPart->addElement($documents);
        }

//On charge le fichier
//TODO: prevoir la gestion de la ged
        $oTemplate = new GDO_ContentType("", Inflector::slug($model['name']) . $courrier_id . "_" . date('YmdHis') . ".odt", "application/vnd.oasis.opendocument.text", "binary", $model['content']);



// Debug: plugin Gedooo2
// Gedooo
//         CakePlugin::load('Gedooo2');
//          App::uses( 'Gedooo2Debugger', 'Gedooo2.Utility' );
//          Configure::write(
//          'Gedooo2',
//          array(
//          'debugHashPathsToCsv' => true,
//          'debugAllPathsToCsv' => true,
//          'debugAllPathsValuesToCsv' => true,
//          )
//          );
//          $this->log( Gedooo2Debugger::hashPathsToCsv( $oMainPart ) );
//		$this->log( Gedooo2Debugger::allPathsToCsv( $oMainPart, Configure::read( 'Gedooo2.debugAllPathsValuesToCsv' ) === true ) );
//          die();

//Fusion
        $oFusion = new GDO_FusionType($oTemplate, "application/vnd.oasis.opendocument.text", $oMainPart);
        $oFusion->process();

        $success = ( $oFusion->getCode() == 'OK' );
        if ($success) {
            $content = $oFusion->getContent();

            $return['content'] = $content->binary;
            $return['size'] = strlen($return['content']);
            $return['ext'] = 'odt';
            $return['mime'] = 'application/vnd.oasis.opendocument.text';
//            }
        }


        return $return;
    }

    /**
     * Récupération du panneau contextuel
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux

     * @throws NotFoundException
     * @return void
     */
    public function getContext($fluxId) {
        $qdFlux = array(
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'contain' => array(
                'Document' => array(
                    'conditions' => array('Document.main_doc' => false)
                )
            ),
            'fields' => array('Courrier.id')
        );
        $flux = $this->Courrier->find('first', $qdFlux);

        if (empty($flux)) {
            throw new NotFoundException();
        }

//		TODO: utiliser $this->Acl->check
        $rights = array('getDocuments' => ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/getDocuments') ? true : false));
        $this->set('mainDoc', $this->Courrier->getMainDoc($fluxId));
        $this->set('rights', $rights);
        $this->set('fluxId', $fluxId);
        $this->set('documents', $flux['Document']);


//		BAN_AIGUILLAGE
        $qd = array(
            'recursive' => -1,
            'conditions' => array(
                'Bancontenu.bannette_id' => BAN_AIGUILLAGE,
                'Bancontenu.courrier_id' => $fluxId,
                'Bancontenu.desktop_id' => $this->SessionTools->getDesktopList(),
                'Bancontenu.etat' => 1
            )
        );
        $result = $this->Courrier->Bancontenu->find('first', $qd);
        if (!empty($result) && count($result) == 1) {
            $this->Courrier->Bancontenu->setRead($result['Bancontenu']['courrier_id'], $result['Bancontenu']['desktop_id']);
        }

        $fromMesServices = false;
        $fluxMesServices = $this->Session->read('Auth.User.Courrier.mesServices');

        if (!empty($fluxMesServices)) {
            if (in_array($fluxId, $fluxMesServices) && !in_array($this->Session->read('Auth.User.Courrier.Desktop.id'), $this->SessionTools->getDesktopList())) {
                $fromMesServices = true;
            }
        }
        $this->set('fromMesServices', $fromMesServices);

		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
    }

    /**
     * Récupération des composants de réponse
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux

     * @throws NotFoundException
     * @return void
     */
    public function getRelements($fluxId) {
//		if (!is_integer($fluxId) || $fluxId <= 0) {
//			throw new BadMethodCallException();
//		}

		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
        $qdFlux = array(
            'recursive' => -1,
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'fields' => array(
                'Courrier.id',
                'Courrier.soustype_id',
				'Soustype.circuit_id'
			),
			'joins' => array(
				$this->Courrier->join($this->Courrier->Soustype->alias)
			)
        );
        $flux = $this->Courrier->find('first', $qdFlux);
        if (empty($flux)) {
            throw new NotFoundException();
        }
        if ( Configure::read('Conf.Gabarit')) {
            $this->Gabaritdocument = ClassRegistry::init('Gabaritdocument');
            $qdRmodels = array(
                'recursive' => -1,
                'fields' => $this->Gabaritdocument->getLightFields(),
                'order' => ['Gabaritdocument.name ASC']
            );
            $rmodels = $this->Gabaritdocument->find('all', $qdRmodels);
            $this->set('rmodels', $rmodels);
        } else {
//debug($gabarits);
            $qdRmodels = array(
                'recursive' => -1,
                'fields' => $this->Courrier->Soustype->Rmodel->getLightFields(),
                'conditions' => array(
                    'Rmodel.soustype_id' => $flux['Courrier']['soustype_id']
                ),
				'order' => ['Rmodel.name ASC']
            );
            $rmodels = $this->Courrier->Soustype->Rmodel->find('all', $qdRmodels);
            $this->set('rmodels', $rmodels);
        }
// 2. Recherche des fichiers déjà liés*
        $qdRelements = array(
            'recursive' => -1,
            'conditions' => array(
                'Relement.courrier_id' => $flux['Courrier']['id']
            ),
            'fields' => array(
                'Relement.id',
                'Relement.name',
                'Relement.mime',
                '( \'' . WEBDAV_DIR . DS . $conn . DS .'\' || "Relement"."courrier_id" || \'' . DS . '\' || "Relement"."name" ) AS "Relement__path"'
            ),
			'order' => ['Relement.name ASC']
        );
        $relements = $this->Courrier->Relement->find('all', $qdRelements);

// Ajouter un modèle ODT "type" par défaut
        $fichiers = array();
        $modeleDir = WEBDAV_DIR . DS . $conn . DS .'modeles';
        $Folder = new Folder($modeleDir, false, 0777);
        foreach ($Folder->find('.+\.odt$') as $i => $fileName) {
            $fichiers[$i] = array(
                'id' => '',
				'courrier_id' => $flux['Courrier']['id'],
                'name' => $fileName,
                'path' => $modeleDir . DS . $fileName,
                'mime' => 'application/vnd.oasis.opendocument.text',
                'ext' => 'odt',
                'size' => strlen(file_get_contents($modeleDir . DS . $fileName))
            );
            $relementsName = Hash::extract($relements, '{n}.Relement.name');
            if (in_array($fichiers[$i]['name'], $relementsName)) {
                unset($fichiers[$i]['name']);
            }
        }


        $fichiersSoustypes = array();
        if( !empty( $flux['Courrier']['soustype_id'] )) {
            $modeleDirSoustype = WEBDAV_DIR . DS . $conn . DS .'modeles' . DS . $flux['Courrier']['soustype_id'];
            $Folder = new Folder($modeleDirSoustype, false, 0777);

//            foreach ($Folder->find('.+\.odt$') as $i => $fileName) {
            foreach ($Folder->find('.*') as $i => $fileName) {
                $fichiersSoustypes[$i] = array(
                    'id' => '',
					'courrier_id' => $flux['Courrier']['id'],
                    'name' => $fileName,
                    'path' => $modeleDirSoustype . DS . $fileName,
                    'mime' => 'application/vnd.oasis.opendocument.text',
					'size' => strlen(file_get_contents($modeleDirSoustype . DS . $fileName))
                );
                $relementsName = Hash::extract($relements, '{n}.Relement.name');
                if (in_array($fichiersSoustypes[$i]['name'], $relementsName)) {
                    unset($fichiersSoustypes[$i]['name']);
                }
            }
            $fichiers = array_merge($fichiers, $fichiersSoustypes);
        }

        $fichiersName = Hash::extract($fichiers, '{n}.name');
        $this->set('fichiersName', $fichiersName);
        $sessionId = $this->Session->id();
        $this->set('sessionId', $sessionId);

        $aro = array('model' => 'Desktop', 'foreign_key' => $this->Session->read('Auth.User.Desktop.id'));
        $this->set('right_upload', $this->Acl->check($aro, 'controllers/Relements/add') && !$this->_isFromFobiddenLocation($flux));

        $inCircuit = $this->Courrier->isInCircuit($fluxId);
//        $this->set('right_delete', $this->Acl->check($aro, 'controllers/Relements/delete') && !$this->_isFromFobiddenLocation($flux) && !$inCircuit);
        $this->set('right_delete', true);
        $this->set('right_sendmail', $this->Acl->check($aro, 'controllers/Courriers/sendmail') && !$this->_isFromFobiddenLocation($flux));
        $this->set('right_sendged', $this->Acl->check($aro, 'controllers/Courriers/sendDocumentToGed') && !$this->_isFromFobiddenLocation($flux));
        $this->set('relements', $relements);
		sort($fichiers);
        $this->set('fichiers',$fichiers);
        $this->set('fluxId', $fluxId);
        $soustypeId = $flux['Courrier']['soustype_id'];
        $this->set('soustypeId', $soustypeId);

        $this->loadModel('Connecteur');
        $hasGedActif = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%GED%',
                    'Connecteur.use_ged' => true
                ),
                'contain' => false
            )
        );
        if( !empty( $hasGedActif )) {
            $sendGED = true;
        }
        else {
            $sendGED = false;
        }
        $this->set('sendGED', $sendGED);

		$hasParapheurActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature' => true
				),
				'contain' => false
			)
		);
		$this->set( 'hasParapheurActif', $hasParapheurActif );


		$hasEtapeParapheur = $this->Courrier->Traitement->Circuit->hasEtapeDelegation($flux['Soustype']['circuit_id']);
		$this->set( 'hasEtapeParapheur', $hasEtapeParapheur );
    }

    /**
     * Vérification de la présence du flux dans une bannette ne donnant pas lieu à la modification du flux (Mes services, En copie, Historique)
     *
     * @param type $flux
     * @return boolean
     */
    private function _isFromFobiddenLocation($flux) {
        $fromHistorique = false;
        if (strpos($_SERVER['HTTP_REFERER'], 'historiqueGlobal') !== false) {
            $fromHistorique = true;
        }
        return $fromHistorique;
    }

    /**
     * Récupération du document principal et des pièces jointes d'un flux
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux

     * @throws NotFoundException
     * @return void
     */
    public function getDocuments($fluxId) {

		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
        $qdFlux = array(
            'recursive' => -1,
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'fields' => array(
                'Courrier.id',
				'Soustype.circuit_id'
            ),
			'joins' => array(
				$this->Courrier->join($this->Courrier->Soustype->alias)
			)
        );
        $flux = $this->Courrier->find('first', $qdFlux);

        if (empty($flux)) {
            throw new NotFoundException();
        }

        $documents = $this->Courrier->getDocs($flux['Courrier']['id']);
        $desktopCreatorId = array();

        foreach ($documents as $i => $document) {
            $desktopCreatorId[] = $document['Document']['desktop_creator_id'];

            $documents[$i]['Document']['isgenerated'] = false;
            $documentName = $document['Document']['name'];
            if (is_dir(WEBDAV_DIR . DS . $conn . DS .$fluxId)) {
                $dirFileGenerated = scandir(WEBDAV_DIR . DS . $conn . DS .$fluxId);
                if (in_array($documentName, $dirFileGenerated)) {
                    $documents[$i]['Document']['isgenerated'] = true;
                }
            }
        }
        $this->set('documents', $documents);

//document principal
        $this->set('mainDoc', $this->Courrier->getMainDoc($flux['Courrier']['id']));

// liste des mails envoyés par document
        $this->set('mailsSent', $this->Courrier->getMailsSent($flux['Courrier']['id']));


        $documentssignes = $this->Courrier->getDocsSignes($flux['Courrier']['id']);
        $this->set('documentssignes', $documentssignes);
        $this->set('fluxId', $fluxId);
        $aro = array('model' => 'Desktop', 'foreign_key' => $this->Session->read('Auth.User.Desktop.id'));
        $this->set('right_upload', $this->Acl->check($aro, 'controllers/Documents/upload') && !$this->_isFromFobiddenLocation($flux));
        $inCircuit = $this->Courrier->isInCircuit($fluxId);
        $this->set('right_delete', $this->Acl->check($aro, 'controllers/Documents/delete') && !$this->_isFromFobiddenLocation($flux) );
        $this->set('right_sendmail', $this->Acl->check($aro, 'controllers/Courriers/sendmail') /* && !$this->_isFromFobiddenLocation($flux) */);
        $this->set('right_sendged', $this->Acl->check($aro, 'controllers/Courriers/sendDocumentToGed') && !$this->_isFromFobiddenLocation($flux));
        $this->set('right_setAsDefault', $this->Acl->check($aro, 'controllers/Documents/setMainDoc') /*&& !$this->_isFromFobiddenLocation($flux)*/);
        $this->set('fromDispEnv', $this->Session->read('Auth.User.Env.actual.profil_id') == DISP_GID);
        $fromMesServices = false;
        $this->set('fromMesServices', $fromMesServices);

        $this->loadModel('Connecteur');
        $hasGedActif = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%GED%',
                    'Connecteur.use_ged' => true
                ),
                'contain' => false
            )
        );
        if( !empty( $hasGedActif )) {
            $sendGED = true;
        }
        else {
            $sendGED = false;
        }
        $this->set('sendGED', $sendGED);

		$hasParapheurActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature' => true
				),
				'contain' => false
			)
		);
        $this->set( 'hasParapheurActif', $hasParapheurActif );
		$hasEtapeParapheur = $this->Courrier->Traitement->Circuit->hasEtapeDelegation($flux['Soustype']['circuit_id']);
		$this->set( 'hasEtapeParapheur', $hasEtapeParapheur );


		$secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
		$desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
		$isAdmin = false;
		$groupName = $this->Session->read('Auth.User.Desktop.Profil');
		if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
			$isAdmin = true;
		}
		$this->set('isAdmin', $isAdmin);

    }

    /**
     * Récupération des informations du flux parent dans le cas d'une réponse à un flux
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux

     * @throws NotFoundException
     * @return void
     */
    public function getParent($fluxId) {
        $conditions = array();

        $querydata = array(
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'contain' => array(
                'Organisme',
                'Contact',
                'Soustype' => array(
                    'Type'
                ),
                'Metadonnee',
                'Affaire' => array(
                    'Dossier'
                ),
                'Bancontenu' => array(
                    'conditions' => array(
                        'Bancontenu.bannette_id is null',
                        'Bancontenu.etat' => '-1'
                    ),
                    'order' => 'Bancontenu.modified DESC',
                    'limit' => 1
                )
            )
        );
        $flux = $this->Courrier->find('first', $querydata);

        $selects = $this->Courrier->Metadonnee->Selectvaluemetadonnee->find('list');
        $this->set('selects', $selects);

        if( empty($flux['Bancontenu']) ) {
            $querydata = array(
                'conditions' => array(
                    'Courrier.id' => $fluxId
                ),
                'contain' => array(
                    'Organisme',
                    'Contact',
                    'Soustype' => array(
                        'Type'
                    ),
                    'Metadonnee',
                    'Affaire' => array(
                        'Dossier'
                    ),
                    'Bancontenu' => array(
                        'conditions' => array(
                            'Bancontenu.etat' => '-1'
                        ),
                        'order' => 'Bancontenu.modified DESC',
                        'limit' => 1
                    )
                )
            );
            $flux = $this->Courrier->find('first', $querydata);
        }


//debug($flux);
        if (!empty($flux['Bancontenu'][0]['desktop_id'])) {
            $desktopId = $flux['Bancontenu'][0]['desktop_id'];
            if($desktopId == '-1') {
				$desktopName = 'Bureau Parapheur';
			}
            else {

				$userId = $this->Courrier->Desktop->getUsers($desktopId);
				$user = $this->Courrier->Desktop->User->find(
					'first',
					array(
						'conditions' => array(
							'User.id' => $userId
						),
						'contain' => false,
						'recursive' => -1
					)
				);
				if (!empty($user)) {
					$desktopName = $user['User']['prenom'] . ' ' . $user['User']['nom'];
				}
			}
        } else {
            $desktopName = '';
        }

        if (!empty($flux['Bancontenu'][0]['desktop_id'])) {
            $desktopmanagerId = $this->Courrier->Desktop->getDesktopManager($flux['Bancontenu'][0]['desktop_id']);
            $desktopManagerName = $this->Courrier->Desktop->Desktopmanager->find(
                    'first', array(
                'conditions' => array(
                    'Desktopmanager.id' => $desktopmanagerId
                ),
                'recursive' => -1,
                'fields' => array('Desktopmanager.name')
                    )
            );
        } else {
            $desktopManagerName = '';
        }
        $this->set(compact('desktopManagerName'));

        if( isset($desktopManagerName) && !empty($desktopManagerName) ) {
            $desktopName = $desktopName. ' ( '.$desktopManagerName['Desktopmanager']['name'].' )';
        }
        $this->set(compact('desktopName'));
        if (empty($flux)) {
            throw new NotFoundException();
        }

        $metadonnees = Hash::extract($flux, 'Metadonnee.{n}');
        $this->set(compact('metadonnees'));

// Ajout pour récupérer les documents liés au flux d'origine
        $this->set('documents', $this->Courrier->getDocs($flux['Courrier']['id']));
        $this->set('mainDoc', $this->Courrier->getMainDoc($flux['Courrier']['id']));
        $aro = array('model' => 'Desktop', 'foreign_key' => $this->Session->read('Auth.User.Desktop.id'));
        $this->set('right_setAsDefault', $this->Acl->check($aro, 'controllers/Documents/setMainDoc') /*&& !$this->_isFromFobiddenLocation($flux)*/);

		$this->set('comments', $this->Courrier->getComments($flux['Courrier']['id']) );
        $this->set('flux', $flux);
    }

    /**
     *
     * @throws NotFoundException
     *
     * exmeple de crontab :
     * 		0 13 * * * /var/www/webgfc/lib/Cake/Console/cake --app /var/www/webgfc/app CreateFromFiles process
     *
     */
    public function acquisition() { //TODO: revoir le principe d acquisition (mettre le code du shell dans le modele)
        if (!$this->Session->read('Auth.User.id')) {
            throw new NotFoundException();
        }

        $this->loadModel('Collectivite');
        $this->Collectivite->useDbConfig = 'default';
        $scan_desktop_id = $this->Collectivite->field('scan_desktop_id', array('conn' => $this->Session->read('Auth.User.connName')));
        $desktops = $this->SessionTools->getDesktopList();

        if (!in_array($scan_desktop_id, $desktops)) {
            throw new NotFoundException();
        }
    }

    /**
     * Avant Ré-aiguillage d'un flux à un aiguilleur ou un initiateur, vérifier
     * si ce flux est déjà inserer dans un circuit
     *

     */
    public function isInTraite() {
        $this->autoRender = false;
        $fluxIds = $this->request->data['InCircuit'];
        $inCircuit = array();
        if( !empty($fluxIds) ) {
			if (count($fluxIds) > 0) {
				foreach ($fluxIds as $i => $fluxId) {
					$inCircuit[] = $this->Courrier->isInCircuit($fluxId);
				}
				return in_array(true, $inCircuit);
			} else {
				return false;
			}
		}
        else {
			return false;
		}
    }

    /**
     * Réaliser un list de fluxId en traitement
     *

     */
    public function fluxIdInTraite() {
        $this->autoRender = false;
        $listFluxId = array_values($this->Courrier->Traitement->find('list', array('fields' => array('Traitement.target_id'), 'contain' => false)));
        echo json_encode($listFluxId);
    }

    /**
     * Fonction permettant de retourner l'historique global  d'un flux
     * @user-profile User
     *
     *
     * @access public
     * @return void
     */
    public function historiqueGlobal($fluxId = null, $generated = false) {

		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
        $desktops = $this->getSessionAllDesktops();
        $historiqueGlobal = array();
        $namedModel = Hash::get($this->request->params, 'named.model');
//ajouter ariane
        $domain = strrchr($_SERVER['HTTP_REFERER'], '/');
        $courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $fluxId), 'contain' => false));
        if ($domain == '/historique') {
            $arianeParent = __d('courrier', 'Bannette.archives');
        } else if ($domain == '/recherches') {
            $arianeParent = __d('menu', 'Recherche', true);
        } else {

            $arianeParent = __d('courrier', 'Bannette.archives');
        }
        $ariane = array(
            '<a href="/environnement/index/0">' . __d('menu', 'Courrier') . '</a>',
            '<a href="/environnement/historique">' . $arianeParent . '</a>',
            $courrier['Courrier']['name']
        );
        $parent = array();
        if (!empty($courrier['Courrier']['parent_id'])) {
            $qdParent = array(
                'fields' => array(
                    'Courrier.id',
                    'Courrier.name'
                ),
                'conditions' => array(
                    'Courrier.id' => $courrier['Courrier']['parent_id']
                ),
                'recursive' => -1
            );
            $parent = $this->Courrier->find('first', $qdParent);

            $ariane = array(
                '<a href="/environnement/index/0">' . __d('menu', 'Courrier') . '</a>',
                '<a href="/environnement/historique">' . $arianeParent . '</a>',
                '<a href="/courriers/historiqueGlobal/' . $parent['Courrier']['id'] . '">' . $parent['Courrier']['name'] . '</a>',
                $courrier['Courrier']['name']
            );
        }
        $this->set('ariane', $ariane);

// Données liées au courrier
        $querydata = array(
            'fields' => array_merge(
                    $this->Courrier->fields(),
                    $this->Courrier->Organisme->fields(), $this->Courrier->Contact->fields(),
                    $this->Courrier->Soustype->fields(), $this->Courrier->Service->fields(), $this->Courrier->Soustype->Type->fields(), $this->Courrier->Soustype->Circuit->fields(), array(
                'Courrier.name',
                'Courrier.objet',
                'Courrier.date',
                'Courrier.datereception',
                'Organisme.name',
                'Contact.name',
                'Type.name',
                'Soustype.name',
                'Circuit.nom',
                'Service.name',
                'Affairesuivie.name',
                'Origineflux.name',
                'Dossier.name',
                'Affaire.name'
                    )
            ),
            'contain' => false,
            'joins' => array(
                $this->Courrier->join($this->Courrier->Organisme->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Contact->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Soustype->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Service->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->Soustype->join($this->Courrier->Soustype->Type->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->Soustype->join($this->Courrier->Soustype->Circuit->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Affairesuivie->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Origineflux->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->join($this->Courrier->Affaire->alias, array('type' => 'LEFT OUTER')),
                $this->Courrier->Affaire->join($this->Courrier->Affaire->Dossier->alias, array('type' => 'LEFT OUTER'))
            ),
            'conditions' => array(
                'Courrier.id' => $fluxId
            )
        );

        $courrier = $this->Courrier->find('first', $querydata);
		if(strpos($courrier['Courrier']['objet'], '<!DOCTYPE') !== false || strpos($courrier['Courrier']['objet'], '<html') !== false  ) {
			$courrier['Courrier']['objet'] = null;
		}

		if( is_JSON($courrier['Courrier']['objet'] ) ) {
			$courrier['Courrier']['objet'] = json_decode($courrier['Courrier']['objet'], true);
			$objetData = array();
			foreach ($courrier['Courrier']['objet'] as $key => $val) {
				if( is_array($val) ) {
					foreach( $val as $fields => $values ) {
						$data[] = "\n".$fields.': '.$values;
					}
					$objetData[] = $key . ' : ' . implode( ' ', $data );
				}
				else {
					$objetData[] = $key . ' : ' . $val;
				}
			}
			$courrier['Courrier']['objet'] = implode("\n", $objetData);
		}

		// LAD / RAD : conversion + OCR
		$hasOcrData = false;
		$ocrData = null;
		$mainDoc = $this->Courrier->getLightMainDoc($fluxId);
		if (!empty($mainDoc)) {
			$ocrData = $mainDoc['Document']['ocr_data'];
			if( !empty($ocrData) ) {
				$hasOcrData = true;
			}
		}
		$courrier['Document']['ocr_data'] = $ocrData;
		$this->set('hasOcrData', $hasOcrData);


//debug($courrier);
// Données nécessaire pour récupérer les différents documents liés au flux
        $qdCourrier = array(
            'conditions' => array(
                'Document.courrier_id' => $fluxId
            ),
            'fields' => array(
                'Document.name'
            ),
            'contain' => false
        );
        $documents = $this->Courrier->Document->find('all', $qdCourrier);

        $rights = array(
            'getDocuments' => true,
            'getRelements' => true,
            'getArs' => true,
            'getCircuit' => true,
            'getSms' => true
        );

// Si le sous-type est sortant, on affiche les composants de réponse sinon les ARs
        $sortant = false;
        if (isset($courrier['Soustype']['entrant']) && $courrier['Soustype']['entrant'] == 0) {
            $sortant = true;
        }
        $this->set('sortant', $sortant);

        $qdParent = array(
            'fields' => array(
                'Courrier.id',
                'Courrier.name'
            ),
            'conditions' => array(
                'Courrier.id' => $courrier['Courrier']['parent_id']
            )
        );
        $parent = $this->Courrier->find('first', $qdParent);

//         $metas_values = $this->DataAuthorized->getAuthMetadonnees(array( 'fluxId' => $fluxId));
        $this->set('metas', $this->DataAuthorized->getAuthMetadonnees(array('fluxId' => $fluxId)));


//       debug($this->params);
// En cas de refus
        $fluxRefus = array();
        if (!empty($courrier['Courrier']['ref_ancien'])) {
            $qdFluxRefus = array(
                'fields' => array(
                    'Courrier.id',
                    'Courrier.name'
                ),
                'conditions' => array(
                    'Courrier.id' => $courrier['Courrier']['ref_ancien']
                )
            );
            $fluxRefus = $this->Courrier->find('first', $qdFluxRefus);
        }
        $this->set('fluxRefus', $fluxRefus);



         // Récupération de toutes les infos échangés sur le flux entre les diférentes bannettes
        $bannettes = $this->Courrier->getCycle($fluxId);

// Récupération des commentaires liés au flux
        $comments = $this->Courrier->getComments($fluxId);

//        $this->set('mainDoc', $this->Courrier->getMainDoc($fluxId));
        $this->set('mainDoc', $this->Courrier->getLightMainDoc($fluxId));
        $this->set('documents', $documents);
        $this->set('rights', $rights);
        $this->set('parent', $parent);
        $this->set('courrier', $courrier);
        $this->set('comments', $comments);
        $this->set('bannettes', $bannettes['bannettes']);
        $fromMesServices = false;
        $this->set('fromMesServices', $fromMesServices);

        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        $desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
        $isAdmin = false;
        $groupName = $this->Session->read('Auth.User.Desktop.Profil');
        if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
            $isAdmin = true;
        }
		$this->set('isAdmin', $isAdmin);

        $deletable = false;
        $hasRightToDelete = $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/delete');
        if ($hasRightToDelete && $isAdmin) {
            $deletable = true;
        }
        $this->set('deletable', $deletable);

        $versable = false;
        $hasRightToSendToGed = $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendToGed');
        if ($hasRightToSendToGed && $isAdmin) {
            $versable = true;
        }
        $this->set('versable', $versable);


        $connecteurgedIsActif = false;
        $this->Connecteur = ClassRegistry::init('Connecteur');
        $connecteurGed = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%GED%'
                ),
                'contain' => false
            )
        );
        if( $connecteurGed['Connecteur']['use_ged'] ) {
            $connecteurgedIsActif = true;
        }
        $this->set('connecteurgedIsActif', $connecteurgedIsActif);

        $docBordereau = $this->Courrier->Document->find(
            'first',
            array(
                'conditions' => array(
                    'OR' => array(
                        'Document.name' => 'Bordereau historique.odt',
                        'Document.ishistorique' => true
                    )
                ),
                'contain' => false,
                'recursive' => -1
            )
        );

        if( !empty( $docBordereau ) ) {
            $modelFileName = $docBordereau['Document']['name'];
            $documentId = $docBordereau['Document']['id'];
            $this->set( compact( 'documentId' ) );
        }

        if( Configure::read('CD') == 81 && $generated == true ) {

            require_once 'XML/RPC2/Client.php';
            // initialisations
            $ret = array();
            $options = array(
                'uglyStructHack' => true
            );

            $gedoooResult = $this->_generate($fluxId, array('name' => 'Bordereau', 'content' => $docBordereau['Document']['content']), null, $this->Session->read('Auth.User.id'));


            $documentBordereau = array(
                'Document' => array(
                    'name' => 'Bordereau.odt',
                    'courrier_id' => $fluxId
                )
            );

            $fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
            $file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "Bordereau.odt";
            file_put_contents( $file, $gedoooResult['content']);

//            $documentBordereau['Document']['content'] = $gedoooResult['content'];
            $documentBordereau['Document']['size'] = $gedoooResult['size'];
            $documentBordereau['Document']['mime'] = $gedoooResult['mime'];
            $documentBordereau['Document']['ext'] = $gedoooResult['ext'];
            $documentBordereau['Document']['main_doc'] = false;
            $documentBordereau['Document']['desktop_creator_id'] = $this->Session->read('Auth.User.id');


            $docAlreadyExist = false;
            $docAttachedToCourrier = $this->Courrier->Document->find(
                'first',
                array(
                    'conditions' => array(
                        'Document.name' => 'Bordereau.odt',
                        'Document.courrier_id' => $fluxId
                    ),
                    'contain' => false,
                    'recursive' => -1
                )
            );
            if( !empty($docAttachedToCourrier) ) {
                $docAlreadyExist = true;
            }

            if( !$docAlreadyExist ){
                $this->Courrier->Document->create($documentBordereau);
                $docToSave = $this->Courrier->Document->save();
            }
            else {
                $docToSave = true;
            }

            if ($docToSave) {
                // Ajout du fichier généré dans le répertoire du webdav pour le webdav
                $sessionFluxIdDir = WEBDAV_DIR . DS . $conn . DS .$fluxId;
                $Folder = new Folder($sessionFluxIdDir, true, 0777);
                $generatedFile = file_put_contents($sessionFluxIdDir . DS . $documentBordereau['Document']['name'], file_get_contents( $file ) );
            }

            if($docToSave ) {
                $file = $documentBordereau['Document']['name'];
                $documentGenerated = file_get_contents(WEBDAV_DIR . DS . $conn . DS .$fluxId . DS . $file);

                if (!empty($documentGenerated) && isset($documentGenerated) && $documentGenerated !== false) {
                    $document_content = $this->Conversion->convertirFlux($documentGenerated, 'odt', 'pdf');
                } else {
                    $document_content = $this->Conversion->convertirFlux($documentBordereau['Document']['content'], 'odt', 'pdf');
                }
                if(strpos($documentBordereau['Document']['name'], 'odt') !== false ) {
                    $document_name = str_replace('odt', 'pdf', $documentBordereau['Document']['name']);
                }
                else {
                    $document_name = $documentBordereau['Document']['name'].'.pdf';
                }
                Configure::write('debug', 0);
                header("Content-type: application/pdf");
                header("Content-Disposition: attachment; filename=\"" . $document_name . "\""); // use 'attachment' to force a file download

                print $document_content;
                exit();
            }
        }

        if( Configure::read('Webservice.GRC') ) {
            $courriergrcId = null;
            if( !empty( $courrier['Courrier']['courriergrc_id'] ) ) {
                $courriergrcId = $courrier['Courrier']['courriergrc_id'];

                // On vérifie si la requête existe toujours dans la GRC
                $this->loadModel('Connecteur');
                $hasGrcActif = $this->Connecteur->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Connecteur.name ILIKE' => '%GRC%',
                            'Connecteur.use_grc' => true
                        ),
                        'contain' => false
                    )
                );
                if( !empty( $hasGrcActif )) {
                    $localeo = new LocaleoComponent;
                    $retour = $localeo->getStatusGRC( $courriergrcId );
                    if( !empty($retour->statut->id) ) {
                        $this->Courrier->updateAll(
                            array( 'Courrier.statutgrc' => $retour->statut->id),
                            array(
                                'Courrier.id' => $fluxId,
                                'Courrier.courriergrc_id' => $courriergrcId
                            )
                        );
                    }
                    if( isset( $retour->err_msg ) && !empty($retour->err_msg)) {
                        $courriergrcId = null;
                    }
                }
            }
            $this->set( 'courriergrcId', $courriergrcId );
        }
        $this->_setOptions();

        $desktopsOfUser = $this->Courrier->Desktop->User->getDesktops($this->Session->read('Auth.User.id') );
		$userInBancontenu = $this->Courrier->Bancontenu->find(
			'all',
			array(
				'fields' => array('Bancontenu.courrier_id', 'Bancontenu.desktop_id'),
				'conditions' => array(
					'Bancontenu.desktop_id' => $desktopsOfUser,
					'Bancontenu.courrier_id' => $fluxId,
					'Bancontenu.bannette_id <>' => 7
				),
				'contain' => false
			)
		);
		$displayBecauseUserIsInBancontenu = false;
		if( !empty($userInBancontenu) || $isAdmin) {
			$displayBecauseUserIsInBancontenu = true;
		}
		$this->set( 'displayBecauseUserIsInBancontenu', $displayBecauseUserIsInBancontenu );

		// On regarde les ids des profils possédant le flux
		$desktopIdToAdd = [];
		foreach($userInBancontenu as $user ) {
			$desktopIdToAdd[] = $user['Bancontenu']['desktop_id'];
		}
		// On regarde la liste de sprofils disponibles pour l'agent connecté
		$desktop_id = $this->Session->read('Auth.User.Courrier.Desktop.id');
		$secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
		$groupId = Hash::extract($secondaryDesktops, '{n}.Profil.id');
		$desktopId = Hash::extract($secondaryDesktops, '{n}.id');
		$groupName = $this->Session->read('Auth.User.Desktop.Profil');
		// Si le bureau secondaire est de type Valideur Editeur, Valideur ou Documentaliste
		if( isset($groupId) && !empty($groupId) ) {
			if ( in_array( $groupId, array( 4, 5, 6 )  ) ) {
				$desktop_id = $desktopId;
			}
		}
		// si un des profils de l'agent connecté est présent das la liste des profils possédant le flux
		// alors on ajoute cet ID dans la requête pour quela validation puisse se faire sans soucis
		$desktopIdToUse = null;
		if(in_array($desktop_id, $desktopIdToAdd)) {
			$desktopIdToUse = $desktop_id;
		}

		// On regarde les bannettes possédant le flux
		$bannettesContent = Hash::extract($bannettes, 'bannettes.{n}.Bancontenu');
		$desktopsWhoGetTheFlux = [];
		foreach( $bannettesContent as $content ) {
			if( $content['etat'] == 1 ) {
				// On stocke les profils possédant le flux en ce moment
				$desktopsWhoGetTheFlux[] = $content['desktop_id'];
				// Si le profil de l'agent connecté est présent dans les bannettes possédant le flux,
				// alors on ne surcharge pas la valeur
				if( $content['desktop_id'] == $desktop_id) {
					$desktopsWhoGetTheFlux = [];
				}
			}
		}
		$delegations = $this->Courrier->User->getDelegatesDesktopsOtherThanMe($this->Session->read('Auth.User.id'), [VALEDIT_GID]);
		if( !empty( $delegations ) ) {
			// Parmi la liste des profils possédant le flux, si l'agent possède une délégation
			// on vérifie si un des profils est présent parmi les délégations, et dans ce cas, on prend sa valeur
			foreach( $desktopsWhoGetTheFlux as $desktopsWhoGetTheFluxId ) {
				if( in_array( $desktopsWhoGetTheFluxId, $delegations )) {
					$desktopIdToUse = $desktopsWhoGetTheFluxId;
				}
			}
		}
		$this->set( 'desktopIdToUse', $desktopIdToUse );

		$this->loadModel('Connecteur');
		$hasPastellActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Pastell%',
					'Connecteur.use_pastell'=> true
				),
				'contain' => false
			)
		);
		$this->set('hasPastellActif', $hasPastellActif);
		$documents = array();
		if( !empty($hasPastellActif)) {
			$documents = Configure::read('Pastell.typesdoc');
		}
		$this->set('documents', $documents);
    }

    /** Envoie pour un ged en protocole CMIS
     *
     * @param type $courrier_id
     */
    public function sendToGed($courrier_id) {
        App::uses('Folder', 'Utility');
        App::uses('File', 'Utility');
        $this->autoRender = false;

        $this->Connecteur = ClassRegistry::init('Connecteur');
        $connecteurGed = $this->Connecteur->find(
                'first', array(
            'conditions' => array(
                'Connecteur.name ILIKE' => '%GED%'
            ),
            'contain' => false
                )
        );
        $folderTmp = new Folder(TMP . 'files' . DS . 'export' . DS . $courrier_id, true, 0777);

        try {
            $cmis = new CmisComponent;
            $cmis->CmisComponent_Service();

            $folderSend = new Folder($folderTmp->pwd() . DS . 'send', true, 0777);

            $cmisFolder = $this->{'_sendToGedVersion' . $connecteurGed['Connecteur']['ged_xml_version']}($cmis, $folderTmp, $folderSend, $courrier_id);

            $files = $folderSend->find('.*');
            foreach ($files as $file) {
                $file = new File($folderSend->pwd() . DS . $file);
                $contents = $file->read();
                $cmis->client->createDocument($cmisFolder->id, preg_replace("/[: ]/", "_", $file->name), array(), $file->read(), $file->mime());

                $file->close();
            }
            $folderTmp->delete();

            if ($this->params['params']['action'] == 'getFlowControls') {
                $desktopidactuel = $this->Session->read('Auth.User.Courrier.Desktop.id');
                $this->redirect(array('controler' => 'courriers', 'action' => 'getFlowControls', $courrier_id, $desktopidactuel));
            }
            $this->redirect(array('controller' => 'courriers', 'action' => 'historiqueGlobal', $courrier_id));
        } catch (Exception $e) {
            switch ($e->getCode()) {
                case '500':
                    $message = 'Erreur interne';
                    break;
                case '404':
                    $message = 'Ressource non trouvée';
                    break;
                case '409':
                    $message = 'Conflit, le courrier existe déjà dans la GED';
                    break;
                default:
                    $message = strip_tags($e->getMessage());
                    break;
            }

            $this->log('Export CMIS: Erreur ' . $e->getCode() . " ! \n" . $e->getMessage(), 'error');
            $this->Session->setFlash('CMIS: Erreur ' . $e->getCode() . ' ! ' . $message, 'growl', array('type' => 'erreur'));
        }

    }

    /** Envoi d'un document pour un ged en protocole CMIS
     *
     * @param type $doc_id
     */
    public function sendDocumentToGed($doc_id) {
        App::uses('Folder', 'Utility');
        App::uses('File', 'Utility');

        $this->autoRender = false;
        $doc = $this->Courrier->Document->find(
                'first', array(
            'fields' => array(
                'Document.id',
                'Courrier.id'
            ),
            'conditions' => array(
                'Document.id' => $doc_id
            ),
            array(
                'Courrier'
            )
                )
        );

        $this->Connecteur = ClassRegistry::init('Connecteur');
        $connecteurGed = $this->Connecteur->find(
                'first', array(
            'conditions' => array(
                'Connecteur.name ILIKE' => '%GED%'
            ),
            'contain' => false
                )
        );
        $folderTmp = new Folder(TMP . 'files' . DS . 'export' . DS . $doc_id, true, 0777);

        try {
            $cmis = new CmisComponent;
            $cmis->CmisComponent_Service();

            $folderSend = new Folder($folderTmp->pwd() . DS . 'send', true, 0777);

            $cmisFolder = $this->_sendDocToGedVersion($cmis, $folderTmp, $folderSend, $doc_id);

            $files = $folderSend->find('.*');
            foreach ($files as $file) {
                $file = new File($folderSend->pwd() . DS . $file);
                $contents = $file->read();
                $cmis->client->createDocument($cmisFolder->id, $file->name, array(), $file->read(), $file->mime());

                $file->close();
            }


            $folderTmp->delete();
//debug($this->params['params']);die();

            $this->redirect(array('controller' => 'courriers', 'action' => 'view', $doc['Courrier']['id'], $this->Session->read('Auth.User.Desktop.id')));
        } catch (Exception $e) {
            switch ($e->getCode()) {
                case '500':
                    $message = 'Erreur interne';
                    break;
                case '404':
                    $message = 'Ressource non trouvée';
                    break;
                case '409':
                    $message = 'Conflit, le courrier existe déjà dans la GED';
                    break;
                default:
                    $message = strip_tags($e->getMessage());
                    break;
            }

            $this->log('Export CMIS: Erreur ' . $e->getCode() . " ! \n" . $e->getMessage(), 'error');
            $this->Session->setFlash('CMIS: Erreur ' . $e->getCode() . ' ! ' . $message, 'growl', array('type' => 'erreur'));
        }
    }

    /**
     * Création des fichiers pour l'export GED en version 2 et 3
     * @param type $cmis
     * @param type $folderTmp
     * @param type $folderSend
     * @param type $courrier_id
     * @param type $message_tdt
     * @return type
     * @throws Exception
     */
    function _sendToGedVersion2(&$cmis, &$folderTmp, &$folderSend, $courrier_id, $message_tdt = false) {

        try {
            $this->Conversion = new ConversionComponent();
// Création du répertoire de séance
            $result = $cmis->client->getFolderTree($cmis->folder->id, 1);

            $courrier = $this->Courrier->find(
                    'first', array(
                'conditions' => array('Courrier.id' => $courrier_id),
                'contain' => array(
                    'Document',
                    'Metadonnee' => array(
                        'Selectvaluemetadonnee'
                    ),
                    'Contact',
                    'Origineflux',
                    'Soustype' => array(
                        'Type'
                    )
                )
                    )
            );

// Récupération de toutes les infos échangés sur le flux entre les diférentes bannettes
            $bannettes = $this->Courrier->Bancontenu->find(
                    'all', array(
                'conditions' => array(
                    'Bancontenu.courrier_id' => $courrier_id
                ),
                'contain' => array(
                    'Desktop' => array(
                        'SecondaryUser',
                        'User',
                        'Profil',
                        'Desktopmanager',
                    ),
                    'Bannette'
                ),
                'order' => array('Bancontenu.modified ASC')
                    )
            );

            $etat = $bannettes[0]['Bancontenu']['etat'];
// On récupère les valeurs des métadonnées
            foreach ($courrier['Metadonnee'] as $m => $flux) {
                $idSelect = Configure::read('Selectvaluemetadonnee.id');
                if ($flux['typemetadonnee_id'] == $idSelect) {
                    foreach ($flux['Selectvaluemetadonnee'] as $s => $selectedValue) {
                        if ($flux['CourrierMetadonnee']['valeur'] == $selectedValue['id']) {
                            $courrier['Metadonnee'][$m]['CourrierMetadonnee']['valeur'] = $selectedValue['name'];
                        }
                    }
                }
            }

            $date_courrier = $courrier['Courrier']['date'];
            $date_reception = $courrier['Courrier']['datereception'];
            $nom_courrier = $courrier['Courrier']['name'];
            $libelle_courrier = $courrier['Courrier']['reference'] . " " . date('Ymd_His'); //CakeTime::i18nFormat($courrier['Courrier']['date'], '%A %d %B %G à %k:%M');
//            $libelle_courrier = $courrier['Courrier']['reference'];

            $this->_deletetoGed($cmis, $libelle_courrier);

// Création des dossiers
            $my_courrier_folder = $cmis->client->createFolder($cmis->folder->id, $libelle_courrier);

            $courrier_id = $courrier['Courrier']['id'];

            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->formatOutput = true;

//            $idDepot = $courrier['Courrier']['numero_depot'] + 1;
            $idDepot = $courrier['Courrier']['reference'];

// origine du dépôt
            $dom_depot = $this->_createElement($dom, 'depot', null, array(
                'versionDepot' => 2,
                'idDepot' => $idDepot,
                'dateDepot' => date("Y-m-d H:i:s"),
                'xmlns:webgfcdossier' => 'http://www.adullact.org/webgfc/infodossier/1.0',
                'xmlns:xm' => 'http://www.w3.org/2005/05/xmlmine'));

// Informations du courrier
            $dom_courrier = $this->_createElement($dom, 'courrier', null, array('idCourrier' => $courrier_id));
            $dom_courrier->appendChild($this->_createElement($dom, 'nomCourrier', $nom_courrier));
            $dom_courrier->appendChild($this->_createElement($dom, 'referenceCourrier', $courrier['Courrier']['reference']));
            $dom_courrier->appendChild($this->_createElement($dom, 'origineCourrier', $courrier['Origineflux']['name']));
            $dom_courrier->appendChild($this->_createElement($dom, 'objetCourrier', $courrier['Courrier']['objet']));
            $dom_courrier->appendChild($this->_createElement($dom, 'directionCourrier', $courrier['Courrier']['direction']));
            $dom_courrier->appendChild($this->_createElement($dom, 'prioriteCourrier', $courrier['Courrier']['priorite']));
            $dom_courrier->appendChild($this->_createElement($dom, 'etatCourrier', $etat));
            $dom_courrier->appendChild($this->_createElement($dom, 'typeCourrier', @$courrier['Soustype']['Type']['name']));
            $dom_courrier->appendChild($this->_createElement($dom, 'soustypeCourrier', @$courrier['Soustype']['name']));
            $dom_courrier->appendChild($this->_createElement($dom, 'dateCourrier', $date_courrier));
            $dom_courrier->appendChild($this->_createElement($dom, 'dateReception', $date_reception));

// Document (principal) attaché au courrier
            $aDocuments = array();
            $aDocuments = $this->Courrier->getDocumentsForDelegation($courrier_id, true);
            $courrier_filename = preg_replace("/[: ]/", "_", /* date('Ymd_His') . '_' . */ $aDocuments['docPrincipale']['Document']['name']);
            $document = $this->_createElement($dom, 'document', null, array('idDocument' => $aDocuments['docPrincipale']['Document']['id']));
            $document->appendChild($this->_createElement($dom, 'nom', $courrier_filename));
            $document->appendChild($this->_createElement($dom, 'relName', 'Document' . DS . $courrier_filename));
            $document->appendChild($this->_createElement($dom, 'type', 'Document'));
            $document->appendChild($this->_createElement($dom, 'mimetype', $aDocuments['docPrincipale']['Document']['mime']));
            $document->appendChild($this->_createElement($dom, 'encoding', 'utf-8'));
            $dom_courrier->appendChild($document);


// Fichiers supplémentaires
            $annexesDocuments = $this->_createElement($dom, 'annexes', null, array('nom' => 'Annexes', 'type' => 'document'));
            foreach ($aDocuments['annexes'] as $i => $annexe) {
                $annexe_filename = preg_replace("/[: ]/", "_", $annexe['filename']);
                $annexesDoc = $this->_createElement($dom, 'annexes', null, array('idDocument' => $i));
                $annexesDoc->appendChild($this->_createElement($dom, 'idAnnexe', $i));
                $annexesDoc->appendChild($this->_createElement($dom, 'nom', $annexe_filename));
                $annexesDoc->appendChild($this->_createElement($dom, 'relName', 'Annexe' . DS . $annexe_filename));
                $annexesDoc->appendChild($this->_createElement($dom, 'type', 'Annexe'));
                $annexesDoc->appendChild($this->_createElement($dom, 'signature', 'false'));
                $annexesDoc->appendChild($this->_createElement($dom, 'mimetype', $annexe['mimetype']));
                $annexesDoc->appendChild($this->_createElement($dom, 'encoding', 'utf-8'));
                $annexesDocuments->appendChild($annexesDoc);

                $isAR = strpos($annexe_filename, 'AR');
                if ($isAR !== false && $annexe['mimetype'] != 'application/pdf') {
                    $annexe['content'] = $this->Conversion->convertirFlux($annexe['content'], 'odt', 'pdf');
                    $annexe['mimetype'] = 'application/pdf';
                    $annexe_filename = str_replace('odt', 'pdf', $annexe_filename);
                }
                $pathinfo = pathinfo($annexe_filename);

                if (!isset($pathinfo['extension'])) {
                    if ($annexe['mimetype'] == 'application/pdf') {
                        $annexe_filename = trim($annexe_filename) . '.pdf';
                    } else {
                        $annexe_filename = trim($annexe_filename) . '.' . $annexe['ext'];
                    }
                }
                $pjs = new File($folderSend->pwd() . DS . $annexe_filename);
                $pjs->write($annexe['content']);
                $pjs->close();
            }
            $dom_courrier->appendChild($annexesDocuments);

// Méta-données
            $metas = $this->_createElement($dom, 'metadonnees', null, array('nom' => 'Metadonnéées', 'type' => 'text'));
            foreach ($courrier['Metadonnee'] as $m => $meta) {
                $metaData = $this->_createElement($dom, 'metadonnees', null, array('idMeta' => $meta['id']));
                $metaData->appendChild($this->_createElement($dom, 'nom', $meta['name']));
                $metaData->appendChild($this->_createElement($dom, 'relName', 'Métadonnée' . DS . $meta['name']));
                $metaData->appendChild($this->_createElement($dom, 'type', 'Metadonnee'));
                $metaData->appendChild($this->_createElement($dom, 'value', $meta['CourrierMetadonnee']['valeur']));
                $metas->appendChild($metaData);
            }
            $dom_courrier->appendChild($metas);

// Mails adressés depuis le flux
            $mailssents = $this->Courrier->getMailsSent($courrier_id);
            $mails = $this->_createElement($dom, 'mails', null, array('nom' => 'Mails', 'type' => 'text'));
            foreach ($mailssents as $mail) {
                $mailData = $this->_createElement($dom, 'mails', null, array('idMail' => $mail['Sendmail']['id']));
                $mailData->appendChild($this->_createElement($dom, 'sender', $mail['Desktop']['name']));
                $mailData->appendChild($this->_createElement($dom, 'sendto', $mail['Sendmail']['email']));
                $mailData->appendChild($this->_createElement($dom, 'sujet', $mail['Sendmail']['sujet']));
                $mailData->appendChild($this->_createElement($dom, 'message', $mail['Sendmail']['message']));
                $mailData->appendChild($this->_createElement($dom, 'docconcerne', preg_replace("/[: ]/", "_", $mail['Document']['name'])));
                $mailData->appendChild($this->_createElement($dom, 'dateenvoi', strftime('%d/%m/%Y %T', strtotime($mail['Sendmail']['created']))));
                $mails->appendChild($mailData);
            }
            $dom_courrier->appendChild($mails);

// Commentaires associés au flux
            $comments = $this->Courrier->getComments($courrier_id);
            $commentsDom = $this->_createElement($dom, 'commentaires', null, array('nom' => 'Commentaires', 'type' => 'text'));
            if (!empty($comments)) {
                foreach ($comments['public'] as $key => $comment) {
                    $commentData = $this->_createElement($dom, 'commentaires', null, array('idComment' => $comment['Comment']['id']));
//                    $commentData->appendChild($this->_createElement($dom, 'contenu', $comment['Comment']['content']));
                    $commentData->appendChild($this->_createElement($dom, 'contenu', $comment['Comment']['objet']));
                    $commentData->appendChild($this->_createElement($dom, 'owner', $comment['Owner']['name']));
                    $commentData->appendChild($this->_createElement($dom, 'dateenvoi', strftime('%d/%m/%Y %T', strtotime($comment['Comment']['created']))));
                    $commentsDom->appendChild($commentData);
                }
            }
            $dom_courrier->appendChild($commentsDom);

// Contact du flux
            $contactDom = array();
            $contact = $this->Courrier->Contact->find(
                    'first', array(
                'conditions' => array(
                    'Contact.id' => $courrier['Courrier']['contact_id']
                ),
                'contain' => false,
                'recursive' => -1
                    )
            );
            if (!empty($contact)) {
                $contactDom = $this->_createElement($dom, 'contact', null, array('idContact' => $contact['Contact']['id']));
                $contactDom->appendChild($this->_createElement($dom, 'name', $contact['Contact']['name']));
                $contactDom->appendChild($this->_createElement($dom, 'civilite', $contact['Contact']['civilite']));
                $contactDom->appendChild($this->_createElement($dom, 'nom', $contact['Contact']['nom']));
                $contactDom->appendChild($this->_createElement($dom, 'prenom', $contact['Contact']['prenom']));
                $contactDom->appendChild($this->_createElement($dom, 'email', $contact['Contact']['email']));
                $contactDom->appendChild($this->_createElement($dom, 'adresse', $contact['Contact']['adresse']));
                $contactDom->appendChild($this->_createElement($dom, 'tel', $contact['Contact']['tel']));
                $contactDom->appendChild($this->_createElement($dom, 'portable', $contact['Contact']['portable']));
                $contactDom->appendChild($this->_createElement($dom, 'role', $contact['Contact']['role']));
                $contactDom->appendChild($this->_createElement($dom, 'ville', $contact['Contact']['ville']));
                $contactDom->appendChild($this->_createElement($dom, 'numvoie', $contact['Contact']['numvoie']));
                $contactDom->appendChild($this->_createElement($dom, 'nomvoie', $contact['Contact']['nomvoie']));
                $contactDom->appendChild($this->_createElement($dom, 'compl', $contact['Contact']['compl']));
                $contactDom->appendChild($this->_createElement($dom, 'cp', $contact['Contact']['cp']));
                $contactDom->appendChild($this->_createElement($dom, 'pays', $contact['Contact']['pays']));
                $contactDom->appendChild($this->_createElement($dom, 'lignedirecte', $contact['Contact']['lignedirecte']));
                $contactDom->appendChild($this->_createElement($dom, 'observations', $contact['Contact']['observations']));
                $dom_courrier->appendChild($contactDom);


// Organisme du flux (lié au contact)
                $organismeDom = array();
                $organisme = $this->Courrier->Contact->Organisme->find(
                        'first', array(
                    'conditions' => array(
                        'Organisme.id' => $contact['Contact']['organisme_id']
                    ),
                    'contain' => false,
                    'recursive' => -1
                        )
                );
                if (!empty($organisme)) {
                    $organismeDom = $this->_createElement($dom, 'organisme', null, array('idOrganisme' => $organisme['Organisme']['id']));
                    $organismeDom->appendChild($this->_createElement($dom, 'name', $organisme['Organisme']['name']));
                    $organismeDom->appendChild($this->_createElement($dom, 'numvoie', $organisme['Organisme']['numvoie']));
                    $organismeDom->appendChild($this->_createElement($dom, 'nomvoie', $organisme['Organisme']['nomvoie']));
                    $organismeDom->appendChild($this->_createElement($dom, 'adressecomplete', $organisme['Organisme']['adressecomplete']));
                    $organismeDom->appendChild($this->_createElement($dom, 'compl', $organisme['Organisme']['compl']));
                    $organismeDom->appendChild($this->_createElement($dom, 'cp', $organisme['Organisme']['cp']));
                    $organismeDom->appendChild($this->_createElement($dom, 'ville', $organisme['Organisme']['ville']));
                    $organismeDom->appendChild($this->_createElement($dom, 'pays', $organisme['Organisme']['pays']));
                    $organismeDom->appendChild($this->_createElement($dom, 'email', $organisme['Organisme']['email']));
                    $organismeDom->appendChild($this->_createElement($dom, 'tel', $organisme['Organisme']['tel']));
                    $organismeDom->appendChild($this->_createElement($dom, 'portable', $organisme['Organisme']['portable']));
                    $organismeDom->appendChild($this->_createElement($dom, 'fax', $organisme['Organisme']['fax']));
                    $organismeDom->appendChild($this->_createElement($dom, 'antenne', $organisme['Organisme']['antenne']));
                    $dom_courrier->appendChild($organismeDom);
                }
            }

// Cycle de vie du flux
            $vie = $this->_createElement($dom, 'cyclevie', null, array('nom' => 'Cycle de vie', 'type' => 'text'));
            foreach ($bannettes as $b => $bannette) {
                $traitement = $this->Courrier->Traitement->find('first', array(
                    'fields' => array(
                        'Traitement.id',
                        'Traitement.circuit_id',
                        'Traitement.numero_traitement'
                    ),
                    'conditions' => array(
                        'Traitement.target_id' => $courrier_id
                    ),
                    'contain' => false,
                    'recursive' => -1
                ));
                $traitement_id = Hash::get($traitement, 'Traitement.id');
                $visas = $this->Courrier->Traitement->Visa->find(
                        'all', array(
                    'fields' => array_merge(
                            $this->Courrier->Traitement->Visa->fields(), $this->Courrier->Traitement->Visa->Desktopmanager->fields()
                    ),
                    'conditions' => array(
                        'Visa.traitement_id' => $traitement_id,
                        'Visa.action' => array('OK', 'IL')
                    ),
                    'contain' => array(
                        'Desktopmanager' => array(
                            'Desktop'
                        )
                    ),
                    'recursive' => -1,
                    'order' => 'Visa.numero_traitement ASC'
                        )
                );

                $validated = array();
                foreach ($visas as $v => $visa) {
                    $validated[$visa['Visa']['trigger_id']] = $visa['Visa']['date'];
                }

                foreach ($validated as $id => $date) {
                    $validationDesktopmanagerId[$id] = $this->Courrier->Desktop->Desktopmanager->getDesktops($id);
                }
//debug($validated);
                if (!empty($validated[$bannette['Bancontenu']['desktop_id']])) {
                    $bannette['Bancontenu']['validated'] = $validated[$bannette['Bancontenu']['desktop_id']];
                } else if (!empty($validationDesktopmanagerId)) {
                    foreach ($bannette['Desktop']['Desktopmanager'] as $dKey => $desktopmanager) {
                        if (isset($validated[$desktopmanager['id']])) {
                            $bannettes['Bancontenu']['validated'] = $validated[$desktopmanager['id']];
                        }
                    }
                }

// Si le flux n'est pas lu, on n'affiche pas la date de modification, ni celle de validation
                if ($bannette['Bancontenu']['read'] == false) {
                    $bannettes['Bancontenu']['validated'] = '';
                }
                $bannetteName = $bannette['Bannette']['name'];
// Utilisateur ayant réalisé l'action
                $user = @$bannette['Desktop']['User'][0]['nom'] . ' ' . @$bannette['Desktop']['User'][0]['prenom'];
                if ($user == ' ') {
                    $user = @$bannette['Desktop']['SecondaryUser'][0]['nom'] . ' ' . @$bannette['Desktop']['SecondaryUser'][0]['prenom'];
                }

                $vieDom = $this->_createElement($dom, 'cyclevie', null, array('idBannette' => $bannette['Bancontenu']['id']));
                $vieDom->appendChild($this->_createElement($dom, 'bureautraitant', $bannette['Desktop']['name']));
                $vieDom->appendChild($this->_createElement($dom, 'agenttraitant', $user));
                $vieDom->appendChild($this->_createElement($dom, 'datearrivee', strftime('%d/%m/%Y %T', strtotime($bannette['Bancontenu']['created']))));
                $vieDom->appendChild($this->_createElement($dom, 'datedernieremodif', strftime('%d/%m/%Y %T', strtotime($bannette['Bancontenu']['modified']))));
                $vieDom->appendChild($this->_createElement($dom, 'datevalidation', !empty($bannette['Bancontenu']['validated']) ? strftime('%d/%m/%Y %T', strtotime($bannette['Bancontenu']['validated'])) : ''));
                $vieDom->appendChild($this->_createElement($dom, 'pourcopie', ($bannetteName == 'copy' ) ? 'Oui' : 'Non' ));
                $vie->appendChild($vieDom);
            }
            $dom_courrier->appendChild($vie);

//Insertion du noeud xml courrier
            $dom_depot->appendChild($dom_courrier);

            $dom->appendChild($dom_depot);

            $file = new File($folderSend->pwd() . DS . 'XML_DESC_' . date('Ymd_His') . '.xml');
            $file->append($dom->saveXML());
            $file->close();

            $docprincipal = new File($folderSend->pwd() . DS . $courrier_filename);
            $docprincipal->write($aDocuments['docPrincipale']['Document']['content']);
            $docprincipal->close();

            $this->Courrier->id = $courrier_id;
            $this->Courrier->saveField('numero_depot', $idDepot);

            $this->Session->setFlash('Le dossier \"' . $libelle_courrier . '\" a été ajouté (Depot n°' . $idDepot . ')', 'growl', array('type' => 'success'));
        } catch (Exception $e) {
            $this->log($e);
            $this->log('Export CMIS: Erreur ' . $e->getCode() . "! \n File:" . $e->getFile() . ' Line:' . $e->getLine(), 'error');
            throw $e;
        }

        if( !empty($my_courrier_folder)) {
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a versé en GED le flux ".$courrier['Courrier']['reference']." (".$libelle_courrier."), le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrier);
        }

        return !empty($my_courrier_folder) ? $my_courrier_folder : false;
    }

    /**
     * Création des fichiers pour l'export GED en version 3
     * @param type $cmis
     * @param type $folderTmp
     * @param type $folderSend
     * @param type $courrier_id
     * @return type
     */
    function _sendToGedVersion3(&$cmis, &$folderTmp, &$folderSend, $courrier_id) {

        return $this->_sendToGedVersion2($cmis, $folderTmp, $folderSend, $courrier_id, true);
    }

    /**
     * Création des fichiers pour l'export GED en version 3
     * @param type $cmis
     * @param type $folderTmp
     * @param type $folderSend
     * @param type $courrier_id
     * @return type
     */
    function _deletetoGed(&$cmis, $libelle_courrier) {

        $this->Connecteur = ClassRegistry::init('Connecteur');
        $connecteurGed = $this->Connecteur->find('first', array(
            'conditions' => array(
                'Connecteur.name ILIKE' => '%GED%'
            ),
            'contain' => false
        ));
        // Règle de gestion on écrase les documents existants
        try {
            //On recherche le dossier
            $objet_cmis = $cmis->client->getObjectByPath($connecteurGed['Connecteur']['ged_repo'] . '/' . $libelle_courrier);

            if (is_object($objet_cmis)) {
                //On recherche tous les enfants du dossier
                $children = $cmis->client->getChildren($objet_cmis->id);
                //On boucle sur les enfants
                foreach ($children->objectList as $child) {
                    //On supprimer l'enfant selectionné
                    $cmis->client->deleteObject($child->id);
                }
                //On peut maitenant supprimer le dossier
                $cmis->client->deleteObject($objet_cmis->id);
            }
        } catch (CmisObjectNotFoundException $e) {
            // L'objet n'existe pas encore : ne rien faire
        }
    }

    function _createElement($domObj, $tag_name, $value = NULL, $attributes = NULL) {
        try {
            $element = ($value != NULL ) ? $domObj->createElement($tag_name, AppTools::xml_entity_encode($value)) : $domObj->createElement($tag_name);

            if ($attributes != NULL) {
                foreach ($attributes as $attr => $val) {
                    $element->setAttribute($attr, $val);
                }
            }
        } catch (Exception $e) {
            $this->log('Export CMIS: Erreur _createElement (' . $tag_name . ') ' . $e->getCode() . "! \n" . $e->getMessage() . ' line:' . $e->getLine(), 'error');
            throw $e;
        }
        return $element;
    }

    /**
     * Envoi d'un mail
     */
    public function sendmail($documentId = null, $fluxId = null, $ar) {

		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
		$success = true;
		$document = [];
		if( $documentId != 'undefined' ) {
			if ($ar == 'false') {
				$document = $this->Courrier->Document->find(
					'first',
					array(
						'recursive' => -1,
						'conditions' => array(
							'Document.id' => $documentId,
							'Document.courrier_id' => $fluxId
						)
					)
				);
			} else if ($ar == 'true') {
				$document = $this->Courrier->Ar->find(
					'first',
					array(
						'recursive' => -1,
						'conditions' => array(
							'Ar.id' => $documentId
						)
					)
				);
				$arInDocument = $this->Courrier->Document->find(
					'first',
					array(
						'fields' => array(
							'Document.id',
							'Document.courrier_id',
							'Document.name',
							'Document.path',
							'Document.ext',
							'Document.mime'
						),
						'recursive' => -1,
						'conditions' => array(
							'Document.name' => $document['Ar']['name'],
							'Document.ext' => 'pdf',
							'Document.mime' => 'application/pdf'
						)
					)
				);
				$document = $arInDocument;
			}
		}

		$courrier = $this->Courrier->find(
			'first',
			array(
				'conditions' => array(
					'Courrier.id' => $fluxId
				),
				'contain' => [
					'Contact'
				],
				'recursive' => -1
			)
		);
        if ( /*!empty($document) &&*/ !empty($this->request->data)) {
			$errorMessage['error-message'] = "Le mail n'a pas été envoyé";

			$separator = '; ';
			$this->loadModel('Connecteur');
			$pastellActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%PASTELL%',
						'Connecteur.use_pastell' => true
					),
					'contain' => false
				)
			);
			if( !empty($pastellActif) ) {
				$separator = ',';
			}

            // DESTINATIRE = A
            // Liste déroulante utilisée uniquement
            $emails = $this->request->data['Sendmail']['email'];
            if (is_array($emails) || !empty($emails)) {
                $this->request->data['Sendmail']['email'] = implode($separator, $emails);
            }
            // Champ texte libre uniquement
            if (!empty($this->request->data['Sendmail']['email2']) && empty($emails)) {
                $this->request->data['Sendmail']['email'] = $this->request->data['Sendmail']['email2'];
            }
            // Liste déroulante utilisée + Champ texte libre
            if (!empty($this->request->data['Sendmail']['email2']) && !empty($emails)) {
                $this->request->data['Sendmail']['email'] = implode($separator, array($this->request->data['Sendmail']['email2'], $this->request->data['Sendmail']['email']));
            }

            // POUR COPIE = CC
            // Liste déroulante utilisée uniquement
            $ccmails = $this->request->data['Sendmail']['ccmail'];
            if (is_array($ccmails) || !empty($ccmails)) {
                $this->request->data['Sendmail']['ccmail'] = implode($separator, $ccmails);
            }
            // Champ texte libre uniquement
            if (!empty($this->request->data['Sendmail']['ccmail2']) && empty($ccmails)) {
                $this->request->data['Sendmail']['ccmail'] = $this->request->data['Sendmail']['ccmail2'];
            }
            // Liste déroulante utilisée + Champ texte libre
            if (!empty($this->request->data['Sendmail']['ccmail2']) && !empty($ccmails)) {
                $this->request->data['Sendmail']['ccmail'] = implode($separator, array($this->request->data['Sendmail']['ccmail2'], $this->request->data['Sendmail']['ccmail']));
            }

            // POUR COPIE CACHEE = CCI
            // Liste déroulante utilisée uniquement
            $ccimails = $this->request->data['Sendmail']['ccimail'];
            if (is_array($ccimails) || !empty($ccimails)) {
                $this->request->data['Sendmail']['ccimail'] = implode($separator, $ccimails);
            }
            // Champ texte libre uniquement
            if (!empty($this->request->data['Sendmail']['ccimail2']) && empty($ccimails)) {
                $this->request->data['Sendmail']['ccimail'] = $this->request->data['Sendmail']['ccimail2'];
            }
            // Liste déroulante utilisée + Champ texte libre
            if (!empty($this->request->data['Sendmail']['ccimail2']) && !empty($ccimails)) {
                $this->request->data['Sendmail']['ccimail'] = implode($separator, array($this->request->data['Sendmail']['ccimail2'], $this->request->data['Sendmail']['ccimail']));
            }

            $attachements = array();
            if (!empty($this->request->data['Document']['Document'])) {
                $attachements = $this->Courrier->Document->find(
                    'all',
                    array(
                        'conditions' => array(
                            'Document.id' => $this->request->data['Document']['Document']
                        ),
                        'contain' => false
                    )
                );
            }

            // Mail sécurisé
			$this->loadModel('Connecteur');
			$pastellActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%PASTELL%',
						'Connecteur.use_pastell' => true
					),
					'contain' => false
				)
			);
			$mailSecActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%PASTELL%',
						'Connecteur.use_pastell' => true,
						'Connecteur.use_mailsec' => true
					),
					'contain' => false
				)
			);
			$typedocpastell = null;
			if( !empty($mailSecActif) ) {
				$typedocpastell = Configure::read('Pastell.TypeDocMailsec');
			}
			if( !empty($typedocpastell) && !empty($pastellActif) ) {
				$identity = $pastellActif['Connecteur']['id_entity'];
				if (!empty($identity)) {
					// on appelle le document PASTELL mail sécurisé
					$documentPastell = $this->NewPastell->createDocument($identity, $typedocpastell);
					$iddoc = $documentPastell->getIdD();

					$this->Courrier->updateAll(
						array('Courrier.pastell_id' => "'".$iddoc."'"),
						array( 'Courrier.id' => $fluxId)
					);

					$this->request->data['Sendmail']['sujet'] = $this->_paramMails($courrier, $this->Session->read('Auth.User'), $this->request->data['Sendmail']['sujet']);
					$this->request->data['Sendmail']['message'] = $this->_paramMails($courrier, $this->Session->read('Auth.User'), $this->request->data['Sendmail']['message']);

					// on ajoute les données (formulaire + document) au document précédemment créé
					$docPastell = $this->NewPastell->modifDocumentMailsec($identity, $iddoc, $this->request->data, $document, $attachements);

					// on déclenche l'action d'envoi
					$send = $this->NewPastell->action($identity, $iddoc, 'envoi');
					$success = true;
					if( !is_array( $send ) ) {
						$errorMessage = json_decode( $send, true );
						$success = false;
					}
				}
				else {
					$success = false;
				}
			}
			else {
				if( empty($this->request->data['Sendmail']['sujet']) ) {
					$this->Jsonmsg->init();
					$this->Jsonmsg->error("Le mail n'a pas été envoyé: le champ 'sujet' est obligatoire");
					$this->Jsonmsg->send();
					return false;
				}

				if( !empty($document) ) {
					$success = $this->Courrier->docParMail(
							trim($this->request->data['Sendmail']['email']),
							$document,
							$this->_paramMails($courrier, $this->Session->read('Auth.User'), $this->request->data['Sendmail']['sujet']),
							$this->_paramMails($courrier, $this->Session->read('Auth.User'), $this->request->data['Sendmail']['message']),
							trim($this->request->data['Sendmail']['ccmail']),
							trim($this->request->data['Sendmail']['ccimail']),
							$attachements
						) && $success;
				}
				else {
					$success = $this->Courrier->docParMail(
							trim($this->request->data['Sendmail']['email']),
							$this->request->data['Sendmail']['courrier_id'],
							$this->_paramMails($courrier, $this->Session->read('Auth.User'), $this->request->data['Sendmail']['sujet']),
							$this->_paramMails($courrier, $this->Session->read('Auth.User'), $this->request->data['Sendmail']['message']),
							trim($this->request->data['Sendmail']['ccmail']),
							trim($this->request->data['Sendmail']['ccimail']),
							$attachements
						) && $success;
				}
			}

			if ($success) {
				$this->Courrier->Sendmail->begin();

				if( $ar == 'true' ) {
					$this->request->data['Sendmail']['document_id'] = $arInDocument['Document']['id'];
				}

				if( $documentId == 'undefined' ) {
					$this->request->data['Sendmail']['document_id'] = null;
				}

				$this->Courrier->Sendmail->create($this->request->data);
				$success = $this->Courrier->Sendmail->save() && $success;
				if($success) {
					$this->Courrier->Sendmail->commit();
				}
				else {
					$this->Courrier->Sendmail->rollback();
					$success = false;
				}
			}

            $this->Jsonmsg->init();
            if ($success) {
				$this->Courrier->Document->commit();
                $this->Jsonmsg->valid("Le mail a bien été envoyé");
            }
            else {
				$this->Courrier->Document->rollback();
				$this->Jsonmsg->error($errorMessage['error-message']);
			}
            $this->Jsonmsg->send();
        }
        $desktopId = $this->Session->read('Auth.User.Courrier.Desktop.id');
        $this->set(compact('fluxId', 'desktopId', 'documentId'));

        $templatemails = $this->Courrier->Sendmail->Templatemail->find('all');
        $this->set(compact('templatemails'));

        $userMails = $this->Courrier->User->getUserMails();
        $contactMails = array();

        $allMails = array_merge($userMails, $contactMails);

// On récupère les mails des services actifs
        $serviceMails = $this->Courrier->Service->getServiceMails();
        $contactMails = $this->Courrier->Contact->getContactMails();
        $allMails = array_merge($allMails, $contactMails, $serviceMails);
        $this->set(compact('allMails'));

        // Liste des autres fichiers associés au flux
		$documentsLies = [];
		if( $documentId != 'undefined' ) {
			$documentsLies = $this->Courrier->Document->find(
				'list', array(
					'conditions' => array(
						'Document.courrier_id' => $fluxId,
						'Document.id <>' => $documentId
					),
					'contain' => false
				)
			);
		}
		else {
			$documentsLies = $this->Courrier->Document->find(
				'list', array(
					'conditions' => array(
						'Document.courrier_id' => $fluxId
					),
					'contain' => false
				)
			);
		}
        $this->set('documentsLies', $documentsLies);

		$contactMail = $courrier['Contact']['email'];
        $this->set('contactMail', $contactMail);
    }

	/**
	 * Détermine le contenu du mail à envoyer en fonction du type de mail, le flux et l'utilisateur
	 * @param array $flux
	 * @param array $user
	 * @return string
	 */
	function _paramMails($flux, $user, $message) {

		$addrTraiter = Configure::read('WEBGFC_URL') . '/courriers/view/' . $flux['Courrier']['id'] . '/' . $user['User']['desktop_id'];
		$addrTraiter = "<a href=".$addrTraiter.">$addrTraiter</a>";
		$addrView = Configure::read('WEBGFC_URL') . '/courriers/view/' . $flux['Courrier']['id'] . '/' . $user['User']['desktop_id'];
		$addrView = "<a href=".$addrView.">$addrView</a>";
		$addrEdit = Configure::read('WEBGFC_URL') . '/courriers/view/' . $flux['Courrier']['id'] . '/' . $user['User']['desktop_id'];
		$addrEdit = "<a href=".$addrEdit.">$addrEdit</a>";

		$civiliteOptions = $this->Session->read('Auth.Civilite' );
		$civiliteContact = $civiliteOptions[$flux['Contact']['civilite']];

		$searchReplace = array(
			"#NOM#" => $user['nom'],
			"#PRENOM#" => $user['prenom'],
			"#IDENTIFIANT_FLUX#" => $flux['Courrier']['id'],
			"#NOM_FLUX#" => $flux['Courrier']['name'],
			"#OBJET_FLUX#" => $flux['Courrier']['objet'],
			"#CIVILITE_CONTACT#" => $civiliteContact,
			"#NOM_CONTACT#" => $flux['Contact']['nom'],
			"#PRENOM_CONTACT#" => $flux['Contact']['prenom'],
			"#COMMENTAIRE_FLUX#" => isset($flux['SCComment']['content']) ? $flux['SCComment']['content'] : null,
			"#LIBELLE_CIRCUIT#" => isset($flux['Soustype']['circuit_id']) ? $this->Courrier->Traitement->Circuit->getLibelle($flux['Soustype']['circuit_id']) : null,
			"#ADRESSE_A_TRAITER#" => $addrTraiter,
			"#ADRESSE_A_VISUALISER#" => $addrView,
			"#ADRESSE_A_MODIFIER#" => $addrEdit,
			"#FLUX_ORIGINE#" => isset($flux['Courrier']['parent_id']) ? $flux['Courrier']['parent_id'] : null,
			"#REFERENCE_FLUX#" => isset($flux['Courrier']['reference']) ? $flux['Courrier']['reference'] : null,
			"#DATERECEPTION_FLUX#" => isset($flux['Courrier']['datereception']) ? date('d/m/Y', strtotime($flux['Courrier']['datereception'])) : null,
			"#DATECREATION_FLUX#" => isset($flux['Courrier']['date']) ? date('d/m/Y', strtotime($flux['Courrier']['date'])) : null,
			"#AFFAIRESUIVIE_PAR#" => isset($user['affairesuiviepar']) ? $user['affairesuiviepar'] : null,
			"#MOTIF_REFUS#" => isset($flux['Courrier']['motifrefus']) ? $flux['Courrier']['motifrefus'] : null
		);

		return str_replace(array_keys($searchReplace), array_values($searchReplace), $message);
	}
    /**
     * Envoi d'un flux à une étape hors circuit
     *
     * @logical-group Flux
     *
     * @logical-used-by CakeFlow > Envoi d'un flux (avec ou sans rebond) à une nouvelle étape
     * @user-profile User
     *
     * @access public
     * @param integer $fluxId identifiant du flux
     * @return void
     */
    public function sendwkfsignaturemanuelle($data) {
        if (!empty($data)) {
            $this->autoRender = false;
            //rebond
            $newDesktopId = $data['Send']['desktop_id'];
            $fluxId = $data['Send']['flux_id'];

            $newDesktop = $this->Courrier->Desktop->find('first', array('conditions' => array('Desktop.id' => $newDesktopId)));
            $desktopId = $this->Session->read('Auth.User.Courrier.Desktop.id');

            // initialisation des visas a ajouter au traitement
            $options = array(
                'insertion' => array(
                    '0' => array(
                        'Etape' => array(
                            'etape_nom' => 'En cours de signature',
                            'etape_type' => 1
                        ),
                        'Visa' => array(
                            '0' => array(
                                'trigger_id' => $newDesktopId,
                                'type_validation' => 'V'
                            )
                        )
                    )
                )
            );
            $action = $data['Send']['getback'] ? 'IL' : 'IP';

            $success = false;
            if ($this->Courrier->cakeflowExecuteSend($action, $desktopId, $fluxId, $options)) {
                $success = true;
            }

            if (!empty($success)) {
                $this->Session->setFlash(__d('default', 'save.ok'), 'growl');
            } else {
                $this->Session->setFlash(__d('default', 'save.error'), 'growl');
            }
        }
    }

    /**
     * Création des fichiers pour l'export GED en version 2 et 3
     * @param type $cmis
     * @param type $folderTmp
     * @param type $folderSend
     * @param type $courrier_id
     * @param type $message_tdt
     * @return type
     * @throws Exception
     */
    function _sendDocToGedVersion(&$cmis, &$folderTmp, &$folderSend, $doc_id, $message_tdt = false) {
//debug($cmis->folder);
//die();
        try {
            $this->Conversion = new ConversionComponent();
// Création du répertoire de séance
            $result = $cmis->client->getFolderTree($cmis->folder->id, 1);

            $qdDoc = array(
                'recursive' => -1,
                'fields' => array(
                    'Document.id',
                    'Document.courrier_id',
                    'Document.name',
                    'Document.ext',
                    'Document.content',
                    'Document.gedpath',
                    'Document.size',
                    'Document.mime',
                    'Document.main_doc',
                    'Document.desktop_creator_id'
                ),
                'conditions' => array(
                    'Document.id' => $doc_id
                ),
                'contain' => false
            );


            $doc = $this->Courrier->Document->find('first', $qdDoc);

            $courrier = $this->Courrier->find('first', array(
                'conditions' => array('Courrier.id' => $doc['Document']['courrier_id']),
                'contain' => array(
                    'Document',
                    'Soustype' => array(
                        'Type'
                    ),
                    'Contact' => array(
                        'Organisme'
                    ),
                    'Origineflux',
                    'Metadonnee' => array(
                        'Selectvaluemetadonnee'
                    )
                )
            ));

            $date_courrier = $courrier['Courrier']['date'];
            $date_reception = $courrier['Courrier']['datereception'];
            $nom_courrier = $courrier['Courrier']['name'];
            $libelle_courrier = $courrier['Courrier']['reference'] . " " . date('Ymd_His'); //CakeTime::i18nFormat($courrier['Courrier']['date'], '%A %d %B %G à %k:%M');

            $this->_deletetoGed($cmis, $libelle_courrier);

            // Création des dossiers
            $my_courrier_folder = $cmis->client->createFolder($cmis->folder->id, $libelle_courrier);
            $courrier_id = $courrier['Courrier']['id'];

            // Récupération de toutes les infos échangés sur le flux entre les diférentes bannettes
            $bannettes = $this->Courrier->Bancontenu->find(
                    'all', array(
                'conditions' => array(
                    'Bancontenu.courrier_id' => $courrier_id
                ),
                'contain' => array(
                    'Desktop' => array(
                        'SecondaryUser',
                        'User',
                        'Profil',
                        'Desktopmanager',
                    ),
                    'Bannette'
                ),
                'order' => array('Bancontenu.modified ASC')
                    )
            );

            $etat = $bannettes[0]['Bancontenu']['etat'];

            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->formatOutput = true;

            $idDepot = $courrier['Courrier']['numero_depot'] + 1;

// origine du dépôt
            $dom_depot = $this->_createElement($dom, 'depot', null, array(
                'versionDepot' => 2,
                'idDepot' => $idDepot,
                'dateDepot' => date("Y-m-d H:i:s"),
                'xmlns:webgfcdossier' => 'http://www.adullact.org/webgfc/infodossier/1.0',
                'xmlns:xm' => 'http://www.w3.org/2005/05/xmlmine'));

// Informations du courrier
            $dom_courrier = $this->_createElement($dom, 'courrier', null, array('idCourrier' => $courrier_id));
            $dom_courrier->appendChild($this->_createElement($dom, 'nomCourrier', $nom_courrier));
            $dom_courrier->appendChild($this->_createElement($dom, 'referenceCourrier', $courrier['Courrier']['reference']));
            $dom_courrier->appendChild($this->_createElement($dom, 'origineCourrier', $courrier['Origineflux']['name']));
            $dom_courrier->appendChild($this->_createElement($dom, 'objetCourrier', $courrier['Courrier']['objet']));
            $dom_courrier->appendChild($this->_createElement($dom, 'directionCourrier', $courrier['Courrier']['direction']));
            $dom_courrier->appendChild($this->_createElement($dom, 'prioriteCourrier', $courrier['Courrier']['priorite']));
            $dom_courrier->appendChild($this->_createElement($dom, 'etatCourrier', $etat));
            $dom_courrier->appendChild($this->_createElement($dom, 'typeCourrier', $courrier['Soustype']['Type']['name']));
            $dom_courrier->appendChild($this->_createElement($dom, 'soustypeCourrier', $courrier['Soustype']['name']));
            $dom_courrier->appendChild($this->_createElement($dom, 'dateCourrier', $date_courrier));
            $dom_courrier->appendChild($this->_createElement($dom, 'dateReception', $date_reception));

// Document (principal) attaché au courrier
            $aDocuments = array();
            $aDocuments = $this->Courrier->getDocumentsForDelegation($courrier_id, true);
            $courrier_filename = preg_replace("/[: ]/", "_", /* date('Ymd_His') . '_' . */ $aDocuments['docPrincipale']['Document']['name']);
            $document = $this->_createElement($dom, 'document', null, array('idDocument' => $aDocuments['docPrincipale']['Document']['id']));
            $document->appendChild($this->_createElement($dom, 'nom', $courrier_filename));
            $document->appendChild($this->_createElement($dom, 'relName', 'Document' . DS . $courrier_filename));
            $document->appendChild($this->_createElement($dom, 'type', 'Document'));
            $document->appendChild($this->_createElement($dom, 'mimetype', $aDocuments['docPrincipale']['Document']['mime']));
            $document->appendChild($this->_createElement($dom, 'encoding', 'utf-8'));
            $dom_courrier->appendChild($document);


//Insertion du noeud xml courrier
            $dom_depot->appendChild($dom_courrier);

//            $zip->close();
            $dom->appendChild($dom_depot);

            $file = new File($folderSend->pwd() . DS . 'XML_DESC_' . date('Ymd_His') . '.xml');
            $file->append($dom->saveXML());
            $file->close();

// On ne sauve que l fichier envoyé
            $isAR = strpos($doc['Document']['name'], 'AR');
            if ($isAR !== false && $doc['Document']['mime'] != 'application/pdf') {
                $doc['Document']['content'] = $this->Conversion->convertirFlux($doc['Document']['content'], 'odt', 'pdf');
                $doc['Document']['mime'] = 'application/pdf';
                $doc['Document']['name'] = str_replace('odt', 'pdf', $doc['Document']['name']);
            }
            $doc['Document']['name'] = date('Ymd_His') . '_' . trim($doc['Document']['name']);
            $file = new File($folderSend->pwd() . DS . $doc['Document']['name']);
            $file->write($doc['Document']['content'], 'w');
            $file->close();

            $this->Courrier->id = $courrier_id;
            $this->Courrier->saveField('numero_depot', $idDepot);

            $this->Session->setFlash('Le dossier \"' . $libelle_courrier . '\" a été ajouté (Depot n°' . $idDepot . ')', 'growl', array('type' => 'success'));
        } catch (Exception $e) {
            $this->log('Export CMIS: Erreur ' . $e->getCode() . "! \n File:" . $e->getFile() . ' Line:' . $e->getLine(), 'error');
            throw $e;
        }

        return !empty($my_courrier_folder) ? $my_courrier_folder : false;
    }

    /**
     * Validation d'unicité du nom d'une collectivité (création / édition)
     *
     * @param type $field
     */
    public function ajaxformvalid($field = null, $collectiviteId = 0) {
        //On vérifie que le nom de bureau n'est pas un nom de groupe en lecture seule (Initiateur, Valideur, ...)

        $datereception_tmp = explode("/", $this->request->data['Courrier']['datereception']);
        $dateinsertion_tmp = explode("/", $this->request->data['Courrier']['date']);
        $datereception = !empty($datereception_tmp[1]) ? $datereception_tmp[2] . '/' . $datereception_tmp[1] . '/' . $datereception_tmp[0] : '';
        $dateinsertion = !empty($dateinsertion_tmp[1]) ? $dateinsertion_tmp[2] . '/' . $dateinsertion_tmp[1] . '/' . $dateinsertion_tmp[0] : '';
        if ($field == 'datereception') {
            if ($datereception > $dateinsertion) {
                $this->autoRender = false;
                $content = json_encode(array('Courrier' => array('datereception' => 'La date d\'entrée dans la collectivité ne peut etre postérieure à la date de création')));
                header('Pragma: no-cache');
                header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
                header('Content-Type: text/x-json');
                header("X-JSON: {$content}");
                Configure::write('debug', 0);
                echo $content;
                return;
            }
        } else if ($field == 'date') {
            if ($dateinsertion < $datereception) {
                $this->autoRender = false;
                $content = json_encode(array('Courrier' => array('date' => 'La date de création ne peut être inférieure à la date d\'entrée dans la collectivité')));
                header('Pragma: no-cache');
                header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
                header('Content-Type: text/x-json');
                header("X-JSON: {$content}");
                Configure::write('debug', 0);
                echo $content;
                return;
            }
        }

        $models = array();
        if ($field != null) {
            if ($field == 'datereception') {
                $models['Courrier'] = array('datereception');
            } else {
                $models['Courrier'] = array('date');
            }
        }

        $rd = array();
        foreach ($this->request->data as $k => $v) {

            if ($field == 'datereception' && $k == 'Courrier') {
                $rd[$k] = $v;
            }
        }

        $this->Ajaxformvalid->valid($models, $rd, $collectiviteId);
    }

    public function dispmultiplesend($fluxIds = array(), $cpyOnly = false) {

        $this->set('sendwkf', $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendwkf'));
        $this->set('sendcpy', $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendcpy'));



		// Aiguillage
		$isdisp = false;
		if( $this->Session->read('Auth.User.Env.actual.name')  == 'disp') {
			$isdisp = true;
		}
		$this->set('isdisp', $isdisp);

        if (!empty($this->request->data['Send'])) {

            $flux = array();
            $desktopsFromManagerIds = array();
            $desktopsCopyFromManagerIds = array();
            // On récupère les ids sélectionnés pour aiguillage
            $desktopmanagerIds = Hash::get($this->request->data, 'Send.initDesktopService');
            // On regarde le bureau sélectionné et on retourne les profils associés
            if( !empty($desktopmanagerIds) ){
                if( Configure::read('Disp.Multipledestination') ) {
                    foreach ($desktopmanagerIds as $desktopmanagerId) {
                        $desktopsFromManagerIds[$desktopmanagerId] = $this->Courrier->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
                    }
                }
                else {
                    $desktopsFromManagerIds = $this->Courrier->Desktop->Desktopmanager->getDesktops($desktopmanagerIds);
                }
            }
            // On récupère les ids sélectionnés pour envoi pour copie
            $desktopCopyIds = Hash::get($this->request->data, 'Send.copy_id');
            $desktopCopyIds = ( empty($desktopCopyIds) ? array() : (array) $desktopCopyIds );
            // On regarde le bureau sélectionné et on retourne les profils associés
            foreach ($desktopCopyIds as $desktopmanagercopyId) {
                $desktopsCopyFromManagerIds[$desktopmanagercopyId] = $this->Courrier->Desktop->Desktopmanager->getDesktops($desktopmanagercopyId);
            }


			if( !isset($this->request->data['DispForm']['itemToLink']) || 	empty($this->request->data['DispForm']['itemToLink'])) {
				$this->request->data['DispForm']['itemToLink'] = $this->request->params['pass'];
			}

            // pour chacun des flux sélectionnés
            foreach ($this->request->data['DispForm']['itemToLink'] as $key => $fluxId) {
                $flux[] = $fluxId;

                // on regarde si le flux existe dans d'autres bannettes d'aiguilleur
                $banettesActuelles = $this->Courrier->Bancontenu->find(
                        'all', array(
                    'conditions' => array(
                        'Bancontenu.courrier_id' => $fluxId,
                        'Bancontenu.etat' => 1,
                        'Bancontenu.bannette_id' => BAN_AIGUILLAGE
                    ),
                    'contain' => false
                        )
                );
                $oldBannettesDesktopsIds = Hash::extract($banettesActuelles, '{n}.Bancontenu.desktop_id');

                // Enregistrement des commentaires pour les flux (copie ou aiguillage)
                if( Configure::read('Disp.Multipledestination') ) {
                    $readersIds = Hash::extract($desktopsFromManagerIds, '{n}.{n}');
                }
                else {
                    $readersIds = $desktopsFromManagerIds;
                }
                $readersCopyIds = Hash::extract($desktopsCopyFromManagerIds, '{n}.{n}');
                $readers = array_merge(@$readersIds, @$readersCopyIds);

                if (!empty($this->request->data['Comment']['content']) || !empty($this->request->data['Comment']['objet'])) {
                    $this->request->data['Comment']['owner_id'] = $this->Session->read('Auth.User.Courrier.Desktop.id');
                    $this->request->data['Comment']['private'] = true;
                    $this->request->data['Comment']['target_id'] = $fluxId;
                    $this->request->data['Reader'] = array('Reader' => $readers);
                    $this->request->data['Send']['flux_id'] = $fluxId;
                    $this->Courrier->Comment->saveAll($this->request->data);
                }
            }
            $desktopAuthId = $this->Session->read('Auth.User.desktop_id');

            // Pour chacun de ces profils, on regarde le groupe auquel il est lié
            foreach ($desktopsFromManagerIds as $key => $desktopsIds) {

                if( Configure::read('Disp.Multipledestination') ) {
                    foreach ($desktopsIds as $desktopId) {

                        $groupsIds[] = $this->Courrier->Bancontenu->Desktop->field('Desktop.profil_id', array('Desktop.id' => $desktopId));

                        $dispDesktops = array_keys($this->getSessionDispDesktops());
                        $senderDispId = $dispDesktops[0];

                        // Pour chacun de ces groupes, on vérifie s'il est INIT ou AIGUILLEUR
                        foreach ($groupsIds as $i => $groupId) {
                            $valid = array();
                            for ($i = 0; $i < count($flux); $i++) {
                                if ($groupId == INIT_GID) {
                                    $courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $flux[$i])));
                                    $courrier['Courrier']['desktop_creator_id'] = $desktopAuthId;
                                    $this->Courrier->create();
                                    $this->Courrier->save($courrier);
                                    $bannetteAdd = BAN_DOCS;
                                } else {
                                    $bannetteAdd = BAN_AIGUILLAGE;
                                }
                            }
                        }
                            // AIGUILLAGE NORMAL
                            // On notifie chacun des profils sélectionnés
                        for ($i = 0; $i < count($flux); $i++) {
                            // Envoi des notifications pour chacun des bureaux en copie
                            $destId = array();
                            $valid[$flux[$i]] = array(
                                'end' => $this->Courrier->Bancontenu->end($senderDispId, BAN_AIGUILLAGE, $flux[$i], $userId),
                                'add' => $this->Courrier->Bancontenu->add($desktopId, $bannetteAdd, $flux[$i], false, $userId)
                            );

                            // Dans le cas où le même flux est chez plusieurs utilisateurs,
                            // on l'enlève des bannettes car il a été traité par l'un d'entre eux
                            if (!empty($oldBannettesDesktopsIds)) {
                                foreach ($oldBannettesDesktopsIds as $j => $otherDesktopId) {
                                    $valid[$flux[$i]] = array(
                                        'end' => $this->Courrier->Bancontenu->end($otherDesktopId, BAN_AIGUILLAGE, $flux[$i], $userId)
                                    );
                                }
                            }


							// on stocke qui fait quoi quand
							$courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $flux[$i]), 'contain' => false));
							$this->loadModel('Journalevent');
							$datasSession = $this->Session->read('Auth.User');
							$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a aiguillé le flux ".$courrier['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
							$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrier);
                        }
                        // Pour chaque flux, on notifie les bureaux et on envoie un mail aux abonnés
                        for ($i = 0; $i < count($flux); $i++) {

                            $fluxId = $flux[$i];
                            // Envoi des notifications
                            $notifInfos = $this->Courrier->Notifieddesktop->Notification->setNotification('ban_xnew');
                            $notifyDesktops = array(
                                $desktopId => array($fluxId)
                            );
                            $notified = $this->Courrier->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $desktopAuthId, $notifyDesktops);

                            // Envoi des mails
                            $user_id = $this->Courrier->User->userIdByDesktop($desktopId);
                            $typeNotif = $this->Courrier->User->find(
                                    'first', array(
                                'fields' => array('User.typeabonnement'),
                                'conditions' => array('User.id' => $user_id),
                                'contain' => false
                                    )
                            );
                            $notificationId = $this->Courrier->Notifieddesktop->Notification->id;
                            if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
                                $this->Courrier->User->saveNotif($fluxId, $user_id, $notificationId, 'aiguillage');
                            } else {
                                $this->Courrier->User->notifier($flux[$i], $user_id, $notificationId, 'aiguillage');
                            }


							// on stocke qui fait quoi quand
							$courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $flux[$i]), 'contain' => false));
							$this->loadModel('Journalevent');
							$datasSession = $this->Session->read('Auth.User');
							$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a aiguillé le flux ".$courrier['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
							$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrier);
                        }
                    }
                }
                else {
                    $groupsIds[] = $this->Courrier->Bancontenu->Desktop->field('Desktop.profil_id', array('Desktop.id' => $desktopsIds));

                    $dispDesktops = array_keys($this->getSessionDispDesktops());
                    $senderDispId = $dispDesktops[0];

                    // Pour chacun de ces groupes, on vérifie s'il est INIT ou AIGUILLEUR
                    foreach ($groupsIds as $i => $groupId) {
                        $valid = array();
                        for ($i = 0; $i < count($flux); $i++) {
                            if ($groupId == INIT_GID) {
                                $courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $flux[$i])));
                                $courrier['Courrier']['desktop_creator_id'] = $desktopAuthId;
                                $this->Courrier->create();
                                $this->Courrier->save($courrier);
                                $bannetteAdd = BAN_DOCS;
                            } else {
                                $bannetteAdd = BAN_AIGUILLAGE;
                            }
                        }
                    }
                        // AIGUILLAGE NORMAL
                        // On notifie chacun des profils sélectionnés
                    for ($i = 0; $i < count($flux); $i++) {
                        // Envoi des notifications pour chacun des bureaux en copie
                        $destId = array();
                        $valid[$flux[$i]] = array(
                            'end' => $this->Courrier->Bancontenu->end($senderDispId, BAN_AIGUILLAGE, $flux[$i], $userId),
                            'add' => $this->Courrier->Bancontenu->add($desktopsIds, $bannetteAdd, $flux[$i], false, $userId)
                        );

                        // Dans le cas où le même flux est chez plusieurs utilisateurs,
                        // on l'enlève des bannettes car il a été traité par l'un d'entre eux
                        if (!empty($oldBannettesDesktopsIds)) {
                            foreach ($oldBannettesDesktopsIds as $j => $otherDesktopId) {
                                $valid[$flux[$i]] = array(
                                    'end' => $this->Courrier->Bancontenu->end($otherDesktopId, BAN_AIGUILLAGE, $flux[$i], $userId)
                                );
                            }
                        }

						// on stocke qui fait quoi quand
						$courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $flux[$i]), 'contain' => false));
						$this->loadModel('Journalevent');
						$datasSession = $this->Session->read('Auth.User');
						$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a aiguillé le flux ".$courrier['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
						$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrier);
                    }
                    // Pour chaque flux, on notifie les bureaux et on envoie un mail aux abonnés
                    for ($i = 0; $i < count($flux); $i++) {

                        $fluxId = $flux[$i];
                        // Envoi des notifications
                        $notifInfos = $this->Courrier->Notifieddesktop->Notification->setNotification('ban_xnew');
                        $notifyDesktops = array(
                            $desktopsIds => array($fluxId)
                        );
                        $notified = $this->Courrier->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $desktopAuthId, $notifyDesktops);

                        // Envoi des mails
                        $user_id = $this->Courrier->User->userIdByDesktop($desktopsIds);
                        $typeNotif = $this->Courrier->User->find(
                                'first', array(
                            'fields' => array('User.typeabonnement'),
                            'conditions' => array('User.id' => $user_id),
                            'contain' => false
                                )
                        );
                        $notificationId = $this->Courrier->Notifieddesktop->Notification->id;
                        if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
                            $this->Courrier->User->saveNotif($fluxId, $user_id, $notificationId, 'aiguillage');
                        } else {
                            $this->Courrier->User->notifier($flux[$i], $user_id, $notificationId, 'aiguillage');
                        }

						// on stocke qui fait quoi quand
						$courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $flux[$i]), 'contain' => false));
						$this->loadModel('Journalevent');
						$datasSession = $this->Session->read('Auth.User');
						$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a aiguillé le flux ".$courrier['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
						$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrier);
                    }
                }
            }


            // AIGUILLAGE POUR COPIE
            // Gestion des desktops auxquels sont envoyés les flux
            if (!empty($this->request->data['Send']['copy_id']) && isset($this->request->data['Send']['copy']) && $this->request->data['Send']['copy']) {
                foreach ($desktopsCopyFromManagerIds as $key => $desktopsCopy) {
                    foreach ($desktopsCopy as $c => $valueCopy) {
                        for ($i = 0; $i < count($flux); $i++) {
                            // Envoi des notifications pour chacun des bureaux en copie
                            $destId = array();
                            $notifInfos = $this->Courrier->Notifieddesktop->Notification->setNotification('ban_copy');
                            $desktopAuthId = $this->Session->read('Auth.User.desktop_id');
                            $fluxId = $flux[$i];

                            $notifyCopyDesktops = array(
                                $valueCopy => array($fluxId)
                            );

                            $notified = $this->Courrier->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $desktopAuthId, $notifyCopyDesktops);

                            $valid[$flux[$i]] = array(
                                'add' => $this->Courrier->Bancontenu->add($valueCopy, BAN_COPY, $flux[$i], false, $userId)
                            );

                            $user_id = $this->Courrier->User->userIdByDesktop($valueCopy);
                            $typeNotif = $this->Courrier->User->find(
                                    'first', array(
                                'fields' => array('User.typeabonnement'),
                                'conditions' => array('User.id' => $user_id),
                                'contain' => false
                                    )
                            );
                            $notificationId = $this->Courrier->Notifieddesktop->Notification->id;
                            if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
                                $this->Courrier->User->saveNotif($fluxId, $user_id, $notificationId, 'copy');
                            } else {
                                $this->Courrier->User->notifier($flux[$i], $user_id, $notificationId, 'copy');
                            }


							// on stocke qui fait quoi quand
							$courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $flux[$i]), 'contain' => false));
							$this->loadModel('Journalevent');
							$datasSession = $this->Session->read('Auth.User');
							$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a aiguillé le flux ".$courrier['Courrier']['reference']." (en copie) le ".date('d/m/Y à H:i:s');
							$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrier);
                        }
                    }
                }
            }
        } else {

			if( isset($this->request->data['DispForm']['itemToLink']) && !empty($this->request->data['DispForm']['itemToLink'])) {
				$fluxChoix = $this->request->data['DispForm']['itemToLink'];
				foreach ($fluxChoix as $key => $fluxId) {
					$flux = $this->Courrier->find(
						'first', array(
							'fields' => array_merge(
								$this->Courrier->fields(), $this->Courrier->Bancontenu->fields(), $this->Courrier->Desktop->fields()
							),
							'conditions' => array(
								'Courrier.id' => $fluxId
							),
							'joins' => array(
								$this->Courrier->join('Bancontenu'),
								$this->Courrier->Bancontenu->join('Desktop')
							),
							'contain' => false
						)
					);

					$desktop_id = $this->Session->read('Auth.User.Desktop.id');
					$env = $this->Session->read('Auth.User.Env');
					if ($env['actual']['name'] == 'disp') {
						$secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
						$groupId = Hash::extract($secondaryDesktops, '{n}.Profil.id');
						foreach ($secondaryDesktops as $i => $value) {
							if ($value['Profil']['id'] == BAN_COPY) {
								$desktop_id = $value['id'];
							}
						}
					}

					$isInCopy[] = $this->_inBannetteCopy($fluxId, $desktop_id);
				}
				if (in_array(false, $isInCopy)) {
					$cpyOnly = false;
				} else {
					$cpyOnly = true;
				}

				$this->set('cpyOnly', $cpyOnly);
				$this->set('fluxIds', $fluxIds);
				$this->set('itemList', $this->request->data);
			}
		}
        // Récupération de la liste des bureaux associés aux initiateurs et aiguilleurs uniquement
        $services = $this->Session->read('Auth.DeskDispInits');
		foreach( $this->Session->read('Auth.AllDesktopsmanagers') as $dmI => $desktopmanager ) {
			if (strpos('Valideur', $desktopmanager['Desktopmanager']['name']) !== false) {
				unset($services[$desktopmanager['Desktopmanager']['id']]);
			}
		}
		$this->set('services', $services);

        // Récupération de la liste de tous les bureaux
        $allDesktops = $this->Session->read('Auth.DesktopsByProfils');
        $this->set('allDesktops', $allDesktops);

        $this->set('bannettes', $this->_bannettes);

    }

    /**
     * Ré-aiguillage d'un flux à un aiguilleur ou un initiateur, depuis l'initiateur
     *
     * @logical-group Flux
     * @user-profile User
     * @user-profile Initiateur
     *
     * @access public
     * @return void
     */
    public function sendBackToDisp( $fluxId = null ) {
		$userId = $this->Session->read('Auth.User.id');

// Liste des services concernés (aiguilleur + initiateur)
        $this->loadModel('Desktop');
        $this->Desktop->setDataSource(Configure::read('conn'));
        $this->set('userId', $this->Session->read('Auth.User.id'));

        $services = $this->Session->read('Auth.DeskDispInits');
        $this->set('services', $services);


        if (!empty($this->request->data)) {
            if (!empty($this->request->data['checkItem'])) {
                $this->set('checkItem', $this->request->data);
            }

			foreach ($this->request->data['checkItem'] as $key => $val) {
				if ($val == true) {
					$isInCopy[] = $this->_inBannetteCopy($val);
					$inCircuit[] = $this->Courrier->isInCircuit($val);
				}
			}
			$messageAlert = false;
			if( count($inCircuit) == array_sum($inCircuit) ) {
				$messageAlert = true;
			}
			$this->set('messageAlert', $messageAlert );

			$fluxIsInCopy = false;
			if( $this->_inBannetteCopy($this->request->data['checkItem']) ) {
				$fluxIsInCopy = true;
			}
			$this->set('fluxIsInCopy', $fluxIsInCopy );

            if (!empty($this->request->data['Courrier'])) {
                $desktopManagerId = $this->request->data['Courrier']['dispInitDesktop'];
                $desktopsIds = $this->Courrier->Desktop->Desktopmanager->getDesktops($desktopManagerId);
                $flux = array();

				foreach ($this->request->data['checkItem'] as $key => $val) {
					if ($val == true) {
						$flux[] = $val;
					}
				}

                foreach ($desktopsIds as $i => $desktopId) {
					// On stocke les profils pour connaître les bannettes à ajouter
                    $groupId = $this->Courrier->Bancontenu->Desktop->field('Desktop.profil_id', array('Desktop.id' => $desktopId));

                    $initDesktops = array_keys($this->getSessionInitDesktops());
                    $senderInitId = $initDesktops[0]; // FIXME
                    $notifyDesktops = array(
                        $desktopId => $flux
                    );

					$dispDesktops = array_keys($this->getSessionDispDesktops());
					$senderDispId = $dispDesktops[0]; // FIXME

                    $bannetteInitiateurReaiguillant = $this->Courrier->Bancontenu->find('all', array(
                        'conditions' => array(
                            'Bancontenu.courrier_id' => $flux[$i],
                            'Bancontenu.desktop_id' => $senderInitId,
                            'Bancontenu.etat' => 1
                        ),
                        'contain' => false
                    ));
                    $typeBannetteInitiateurReaiguillant = $bannetteInitiateurReaiguillant[0]['Bancontenu']['bannette_id'];

					$bannetteAiguilleurReaiguillant = $this->Courrier->Bancontenu->find('all', array(
                        'conditions' => array(
                            'Bancontenu.courrier_id' => $flux[$i],
                            'Bancontenu.desktop_id' => $senderDispId,
                            'Bancontenu.etat' => 1
                        ),
                        'contain' => false
                    ));
                    $typeBannetteAiguilleurReaiguillant = $bannetteAiguilleurReaiguillant[0]['Bancontenu']['bannette_id'];


                    // Pour chaque agent présent dans le bureau de celui ré-aiguillant, on vide la bannette
					$bureauSenderInitId = $this->Courrier->Desktop->getDesktopManager($senderInitId);
					$allDesktopsFromDesktopmanager = $this->Courrier->Desktop->Desktopmanager->getDesktops($bureauSenderInitId);

                    foreach( $this->request->data['checkItem'] as $k => $fluxId) {
                        // on enlève des autres bannettes des autres intis aussi
                        $banettesActuelles = $this->Courrier->Bancontenu->find(
                            'all',
                            array(
                                'conditions' => array(
                                    'Bancontenu.courrier_id' => $fluxId,
                                    'Bancontenu.etat' => 1,
                                    'Bancontenu.bannette_id' => [1,5]
                                ),
                                'contain' => false
                            )
                        );
                        $oldBannettesDesktopsIds = Hash::extract($banettesActuelles, '{n}.Bancontenu.desktop_id');
                        // Dans le cas où le même flux est chez plusieurs utilisateurs,
                        // on l'enlève des bannetes car il a été traité par l'un d'entre eux
                        if (!empty($oldBannettesDesktopsIds)) {
							if(Configure::read('Detachement.SuiteReaiguillage')){
								foreach ($oldBannettesDesktopsIds as $j => $otherDesktopId) {
									$desktops = $this->Courrier->Desktop->find(
										'all',
										array(
											'conditions' => array(
												'Desktop.id' => $otherDesktopId
											),
											'contain' => false
										)
									);
									$querydata = array(
										'contain' => false,
										'conditions' => array(
											'Courrier.id' => $fluxId
										)
									);
									$courrier = $this->Courrier->find('first', $querydata);

									if ($desktops[$j]['Desktop']['profil_id'] == DISP_GID) {
										$BAN = BAN_AIGUILLAGE;
									} else if ($desktops[$j]['Desktop']['profil_id'] == INIT_GID) {
										$BAN = BAN_DOCS;
									} else if (!empty($courrier['Courrier']['ref_ancien']) || !empty($courrier['Courrier']['parent_id'])) {
										$BAN = BAN_REFUS;
									}
									if (!empty($BAN)) {
										$valid[$fluxId] = array(
											'end' => $this->Courrier->Bancontenu->end($otherDesktopId, $BAN, $fluxId, $userId)
										);
									}
								}
							}
                        }

						$querydata = array(
							'contain' => false,
							'conditions' => array(
								'Courrier.id' => $fluxId
							)
						);
						$courrierJournal = $this->Courrier->find('first', $querydata);

						$this->loadModel('Journalevent');
						$datasSession = $this->Session->read('Auth.User');
						$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a ré-aiguillé le flux ".$courrierJournal['Courrier']['reference'].", le ".date('d/m/Y à H:i:s');
						$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrierJournal);
                    }

					$private = true;
					if (isset($this->request->data['Comment']['private']) && $this->request->data['Comment']['private'] != 1) {
						$private = false;
					}
					/// Ajout du commentaire privé au moment du ré-aiguillage
					if( !empty( $this->request->data['Comment']['objet'] ) ){
						$this->request->data['Comment']['owner_id'] = $this->Session->read('Auth.User.Courrier.Desktop.id');
						$this->request->data['Comment']['private'] = $private;
						if( $private ) {
							$this->request->data['Reader'] = array('Reader' => $desktopId);
						}
						$this->request->data['Comment']['target_id'] = $fluxId;
						$this->request->data['Send']['flux_id'] = $fluxId;
						$this->Courrier->Comment->saveAll($this->request->data);
					}
//envoi des notifications
                    $notifInfos = $this->Courrier->Notifieddesktop->Notification->setNotification('ban_xnew');
                    $this->Courrier->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $this->Session->read('Auth.User.Courrier.Desktop.id'), $notifyDesktops);

                    $user_id = $this->Courrier->User->userIdByDesktop($desktopId);
                    $typeMessage = '';
                    if ($groupId == DISP_GID) {
                        $typeMessage = 'aiguillage';
                    } else if ($groupId == INIT_GID) {
                        $typeMessage = 'insertion';
                    }

                    for ($j = 0; $j < count($flux); $j++) {
                        $typeNotif = $this->Courrier->User->find(
                                'first', array(
                            'fields' => array('User.typeabonnement'),
                            'conditions' => array('User.id' => $user_id),
                            'contain' => false
                                )
                        );
                        $notificationId = $this->Courrier->Notifieddesktop->Notification->id;
                        if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
                            $this->Courrier->User->saveNotif($flux[$j], $user_id, $notificationId, $typeMessage);
                        } else {
                            $this->Courrier->User->notifier($flux[$j], $user_id, $notificationId, $typeMessage);
                        }
                    }


//					if ($groupId == DISP_GID) {
//						$BAN = BAN_AIGUILLAGE;
//					} else if ($groupId == INIT_GID) {
//						$BAN = BAN_DOCS;
//					}
					$valid = array();
//					$bureauxDispatch = $this->Courrier->Desktop->Desktopmanager->find(
//						'all',
//						array(
//							'conditions' => array(
//								'Desktopmanager.id' => $bureauSenderInitId,
//								'Desktopmanager.isdispatch' => true
//							),
//							'contain' => false,
//							'recursive' => -1
//						)
//					);

					/*if( !empty($bureauxDispatch) ) {
						$allDesktopsToDetacheBecauseOfReaiguillage = $this->Courrier->Desktop->Desktopmanager->getDesktops($bureauxDispatch[0]['Desktopmanager']['id']);
					}
					else */if (!empty($typeBannetteInitiateurReaiguillant) ){
						$bureauSenderInitId = $this->Courrier->Desktop->getDesktopManager($senderInitId);
						foreach( $bureauSenderInitId as $bureauInit ) {
							$desktopmanagerDesktopsIds = $this->Courrier->Desktop->Desktopmanager->getDesktops($bureauInit);
						}
						$allDesktopsToDetacheBecauseOfReaiguillage = $desktopmanagerDesktopsIds;
					}
					else {
						$bureauSenderDispId = $this->Courrier->Desktop->getDesktopManager($senderDispId);
						foreach( $bureauSenderDispId as $bureauDisp ) {
							$desktopmanagerDesktopsIds = $this->Courrier->Desktop->Desktopmanager->getDesktops($bureauDisp);
						}
						$allDesktopsToDetacheBecauseOfReaiguillage = $desktopmanagerDesktopsIds;
					}

					for ($i = 0; $i < count($flux); $i++) {
						$typeBannetteReaiguillant = $typeBannetteInitiateurReaiguillant;
						if( empty($typeBannetteInitiateurReaiguillant) && !empty($typeBannetteAiguilleurReaiguillant) ) {
							$typeBannetteReaiguillant = $typeBannetteAiguilleurReaiguillant;
						}

						foreach( $allDesktopsToDetacheBecauseOfReaiguillage as $desktopsIdsToDetach ) {
							$valid[$flux[$i]] = array(
								'end' => $this->Courrier->Bancontenu->end($desktopsIdsToDetach, $typeBannetteReaiguillant, $flux[$i], $userId)
							);
						}

						$desktopCible = $this->Courrier->Desktop->find(
							'first',
							array(
								'conditions' => array(
									'Desktop.id' => $desktopId
								),
								'contain' => false
							)
						);
						$profilCibleId = $desktopCible['Desktop']['profil_id'];
						if ($profilCibleId == DISP_GID) {
							$BANCible = BAN_AIGUILLAGE;
						} else if ($profilCibleId == INIT_GID) {
							$BANCible = BAN_DOCS;
						}
						$valid[$flux[$i]] = array(
							'add' => $this->Courrier->Bancontenu->add($desktopId, $BANCible, $flux[$i], false, $userId)
						);
					}
                }

                $this->Session->setFlash(__d('default', 'save.ok'), 'growl');
            }
        }
    }

    /**
     * Traitement des flux par lot, validation de flux en une seule fois
     *
     * @param type $field
     */
    public function traitementlot($cpyOnly = false) {

        $desktopsToSend = $this->Courrier->Desktop->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true)));
        $this->set('desktopsToSend', $desktopsToSend);

        $isAuthVersGed = false;
        $isAuthVersGed = $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendToGed');
        $this->set('isAuthVersGed', $isAuthVersGed);

        $endCircuitIsCollaborative = false;
        $this->set('endCircuitIsCollaborative', $endCircuitIsCollaborative);

        $this->set('endCircuitIsCollaborativeDerniereUtilisateur', false);

        $sendCpyCheck = $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendcpy');
        $this->set('cpyOnly', $cpyOnly && $sendCpyCheck);

        $sendWkfCheck = $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/sendwkf');

        $this->set('sendwkf', $sendWkfCheck);
        $this->set('sendcpy', $sendCpyCheck);

        $desktopUserId = $this->Session->read('Auth.User.Courrier.Desktop.id');
        if (!empty($this->request->data)) {

            if (!empty($this->request->data['checkItem'])) {

                foreach ($this->request->data['checkItem'] as $t => $fluxId) {
                    $flux = $this->Courrier->find(
                            'first', array(
                        'fields' => array(
                            'Courrier.id',
                            'Courrier.soustype_id',
                            'Courrier.name',
                            'Courrier.reference',
                            'Courrier.created',
                            'Courrier.motifrefus',
                            'Soustype.id',
                            'Soustype.circuit_id',
                            'Soustype.name',
                            'Courrier.user_creator_id',
                            'Document.id',
                            'Document.ishistorique'
                        ),
                        'conditions' => array('Courrier.id' => $fluxId),
                        'joins' => array(
                            $this->Courrier->join($this->Courrier->Soustype->alias),
                            $this->Courrier->join($this->Courrier->Document->alias)
//                                $this->Courrier->join('Document', array('type' => 'LEFT OUTER'))
                        ),
                        'contain' => false,
//                            'recursive' => -1
                            )
                    );


                    $hasSoustype = false;
                    if (!empty($flux['Soustype']['id'])) {
                        $hasSoustype = true;
                    }


                    $hasCircuit = false;
                    if (!empty($flux['Soustype']['circuit_id']) || $flux['Soustype']['name'] == Configure::read('Soustype.Fantome') ) {
                        $hasCircuit = true;
                    }

                    $hasEtapeParapheur = $this->Courrier->Traitement->Circuit->hasEtapeDelegation($flux['Soustype']['circuit_id']);
//                    $this->set( compact('hasEtapeParapheur' ) );

                    $isInCircuit = $this->Courrier->isInCircuit($fluxId);
//debug( $this->Courrier->Traitement->targetExists($fluxId) );
                    if (!empty($flux['Courrier']['motifrefus']) && !$this->Courrier->Traitement->targetExists($fluxId)) {
                        $isInCircuit = false;
                    }
//debug($isInCircuit);
//                    $this->set('isInCircuit', $isInCircuit);

                    $mainDoc = $this->Courrier->getLightMainDoc($fluxId);
                    $hasDoc = false;
                    if (!empty($mainDoc)) {
                        $hasDoc = true;
                    }
//debug($mainDoc);
                    $endCircuit = false;
                    if ($this->Courrier->cakeflowIsEnd($fluxId)) {
                        $endCircuit = true;
                    }

					$isCircuitActif = false;
                    if (!empty($flux['Soustype']['circuit_id']) || $flux['Soustype']['name'] == Configure::read('Soustype.Fantome') ) {
						$isCircuitActif = $this->Courrier->Traitement->Circuit->isActif($flux['Soustype']['circuit_id']);
					}

                    $reponse = false;
                    $stypeReponse = $this->Courrier->Soustype->SoustypeSoustypecible->find(
                        'first',
                        array(
                            'conditions' => array(
                                'SoustypeSoustypecible.soustypecible_id' => $flux['Courrier']['soustype_id']
                            ),
                            'contain ' => false
                        )
                    );
                    if (!empty($stypeReponse)) {
                        $reponse = true;
                    }
                    $desktopCurrentEtapeCircuit = false;
                    $desktopIdInWkf = $this->Courrier->Traitement->whoIs($fluxId, 'current');
                    $desktopmanagersIds = $this->Courrier->Desktop->getDesktopManager($desktopUserId);
                    if (!empty($desktopIdInWkf) && !empty($desktopmanagersIds)) {
                        foreach ($desktopIdInWkf as $t => $val) {
                            if (in_array($val, $desktopmanagersIds)) {
                                $desktopCurrentEtapeCircuit[$t] = true;
                            }
                        }
                        if (!empty($desktopCurrentEtapeCircuit)) {
                            $deskInCircuit = in_array(true, $desktopCurrentEtapeCircuit) || $desktopCurrentEtapeCircuit == true;
                            if ($deskInCircuit) {
                                $desktopCurrentEtapeCircuit = true;
                            }
                        }
                    }

                    $deletable = false;
                    $userCreatorId = $flux['Courrier']['user_creator_id'];
                    $userConnectedId = $this->Session->read('Auth.User.id');

                    if ($userCreatorId == $userConnectedId ||  empty($userCreatorId) ) {
                        $deletable = true;
                    }
                    if( $isInCircuit) {
						$deletable = false;
					}

                    // pour les flux en copie détachables
                    $detachable = false;
                    $querydata = array(
                        'conditions' => array(
                            'Bancontenu.bannette_id' => 7,
                            'Bancontenu.desktop_id' => $this->SessionTools->getDesktopList(),
                            'Bancontenu.courrier_id' => $fluxId,
                            'Bancontenu.etat' => 1
                        ),
                        'contain' => false
                    );
                    $courrierDetachable = $this->Courrier->Bancontenu->find('first', $querydata);
                    if (!empty($courrierDetachable)) {
                        $detachable = true;
                    }
                    if (isset($flux['Document']['id']) && $flux['Document']['id'] != null) {
                        $this->request->data['treatableItem']['hasDocument'][$fluxId] = true;
                    }

                    $this->request->data['treatableItem']['hasEtapeParapheur'][$fluxId] = $hasEtapeParapheur;
                    $this->request->data['treatableItem']['endCircuit'][$fluxId] = $endCircuit;
                    $this->request->data['treatableItem']['isCircuitActif'][$fluxId] = $isCircuitActif;
                    $this->request->data['treatableItem']['isInCircuit'][$fluxId] = $isInCircuit;
                    $this->request->data['treatableItem']['reponse'][$fluxId] = $reponse;
                    $this->request->data['treatableItem']['desktopCurrentEtapeCircuit'][$fluxId] = $desktopCurrentEtapeCircuit;

                    $this->request->data['treatableItem']['hasDoc'][$fluxId] = $hasDoc;
                    $this->request->data['treatableItem']['hasSoustype'][$fluxId] = $hasSoustype;
                    $this->request->data['treatableItem']['hasCircuit'][$fluxId] = $hasCircuit;

                    $this->request->data['treatableItem']['info'][$fluxId] = $flux;
                    $this->request->data['treatableItem']['delete'][$fluxId] = $deletable;

                    $this->request->data['treatableItem']['copy'][$fluxId] = $detachable;

                    $this->set('flux', $flux);
//                    $this->set('checkItem', $this->request->data);
                    $this->set('checkItem', $this->request->data);

                    $typeEtape[$fluxId] = $this->Courrier->Traitement->typeEtape($fluxId);
                    $this->request->data['treatableItem']['typeEtape'] = $typeEtape;

					$this->request->data['treatableItem']['sameCourrierId'][$fluxId] = false;
					foreach( array_count_values( $this->request->data['checkItem'] ) as $fluxId => $allowed ) {
						if( $allowed != 1 ) {
							$this->request->data['treatableItem']['sameCourrierId'][$fluxId] = true;
						}
					}
                }

                // 0 = false/false, 1 = true / false ou 2= true/true
                $hasEtapeParapheur = array_sum(($this->request->data['treatableItem']['hasEtapeParapheur']));
                $endCircuit = array_sum(($this->request->data['treatableItem']['endCircuit']));
                $isCircuitActif = array_sum(($this->request->data['treatableItem']['isCircuitActif']));
                $isInCircuit = array_sum(($this->request->data['treatableItem']['isInCircuit']));
                $reponse = array_sum(($this->request->data['treatableItem']['reponse']));
                $desktopCurrentEtapeCircuit = array_sum(($this->request->data['treatableItem']['desktopCurrentEtapeCircuit']));
                $hasDoc = array_sum(($this->request->data['treatableItem']['hasDoc']));
                $hasSoustype = array_sum(($this->request->data['treatableItem']['hasSoustype']));
                $hasCircuit = array_sum(($this->request->data['treatableItem']['hasCircuit']));
                $nbSelected = count($this->request->data['checkItem']);

                $isDeletable = array_sum($this->request->data['treatableItem']['delete']);

                $isDetachable = array_sum($this->request->data['treatableItem']['copy']);

                $isCloturable = in_array(ETAPESIMPLE, $this->request->data['treatableItem']['typeEtape']);

				$hasTheSameCourrierId = array_sum($this->request->data['treatableItem']['sameCourrierId']);
                $this->set(compact( 'hasTheSameCourrierId', 'isCloturable', 'hasEtapeParapheur', 'isCircuitActif', 'endCircuit', 'isInCircuit', 'reponse', 'desktopCurrentEtapeCircuit', 'hasDoc', 'nbSelected', 'hasSoustype', 'hasCircuit', 'isDeletable', 'isDetachable'));
            }
        }
    }

    /**
     * Actions devant être déclenchées
     * @param type $typeTraitement
     */
    public function execTraitementlot($desktop_id, $typeTraitement, $fluxIds = array()) {
        $this->autoRender = false;
//        $desktop_id = $this->Session->read('Auth.User.Courrier.Desktop.id');

        if (!empty($this->request->data['checkItem'])) {
            $fluxIds = $this->request->data['checkItem'];

            if ($typeTraitement == 'insert') {
                foreach ($fluxIds as $f => $fluxId) {
                    $ret[$f] = $this->Courrier->cakeflowInsert($fluxId, $this->Session->read('Auth.User.Courrier.Desktop.id'), true);
                    if ($ret[$f] === true || empty($ret[0])) {

                        $flux = $this->Courrier->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Courrier.id' => $fluxId
                                ),
                                'contain' => false,
                                'recursive' => -1
                            )
                        );
                        $this->loadModel('Journalevent');
                        $datasSession = $this->Session->read('Auth.User');
                        $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a inséré par lot le flux ".$flux['Courrier']['reference']." dans un circuit dont le sous-type est (".$flux['Courrier']['soustype_id']."), le ".date('d/m/Y à H:i:s');
                        $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);

                        $this->Session->setFlash(__d('courrier', 'Courrier.insertlot.ok'), 'growl', array('type' => 'default'));
                    } else {
                        $this->Session->setFlash(__d('courrier', 'Courrier.insertlot.error') . ' : ' . json_decode($ret[$f]), 'growl', array('type' => 'erreur'));
                    }
                }
            } else if ($typeTraitement == 'valid') {
                foreach ($fluxIds as $f => $fluxId) {
                    if( empty($desktop_id)) {
                        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
                        $groupId = Hash::extract($secondaryDesktops, '{n}.Profil.id');
                        $desktopId = Hash::extract($secondaryDesktops, '{n}.id');
                        $groupName = $this->Session->read('Auth.User.Desktop.Profil');
                        // Si le bureau secondaire est de type Valideur Editeur, Valideur ou Documentaliste
                        if ( in_array( $groupId, array( 4, 5, 6 )  ) ) {
                            $desktop_id = $desktopId;
                        }
                    }

                    $ret[$f] = $this->Courrier->cakeflowExecuteNext($desktop_id, $fluxId, 0, null, 0);
                    if ($ret[$f] === true) {
                        $flux = $this->Courrier->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Courrier.id' => $fluxId
                                ),
                                'contain' => false,
                                'recursive' => -1
                            )
                        );
                        $this->loadModel('Journalevent');
                        $datasSession = $this->Session->read('Auth.User');
                        $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a validé par lot le flux ".$flux['Courrier']['reference']." dans un circuit dont le sous-type est (".$flux['Courrier']['soustype_id']."), le ".date('d/m/Y à H:i:s');
                        $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);

                        $this->Session->setFlash(__d('courrier', 'Courrier.treatedlot.ok'), 'growl', array('type' => 'default'));
                    } else {
                        $this->Session->setFlash(__d('courrier', 'Courrier.treatedlot.error') . ' : ' . json_decode($ret[$f]), 'growl', array('type' => 'erreur'));
                    }
                }
            }
        }
    }

    /**
     *
     * @param type $typeTraitement
     * @param type $fluxIds
     */
    public function refusparlot($typeTraitement = null, $fluxIds = array()) {
//debug($this->request->data);
//die();
        if (!empty($this->request->data['checkItem'])) {
            $fluxIds = $this->request->data['checkItem'];


            $flux = $this->Courrier->find(
                    'all', array(
                'fields' => array(
                    'Courrier.id',
                    'Courrier.reference',
                    'Courrier.name',
                    'Courrier.motifrefus',
                    'Courrier.created',
                    'Courrier.user_creator_id'
                ),
                'conditions' => array(
                    'Courrier.id' => $fluxIds
                ),
                'contain' => false
                    )
            );
//        debug($flux);
            $this->set('flux', $flux);
            $this->render('refusparlot');
        }

        if (!empty($typeTraitement)) {

            foreach ($this->request->data as $n => $notif) {
                $flux = $this->Courrier->find(
                        'all', array(
                    'fields' => array(
                        'Courrier.id',
                        'Courrier.reference',
                        'Courrier.name',
                        'Courrier.soustype_id',
                        'Courrier.motifrefus',
                        'Courrier.created',
                        'Courrier.user_creator_id'
                    ),
                    'conditions' => array(
                        'Courrier.id' => $notif['Notification']['courrier_id']
                    ),
                    'contain' => false
                        )
                );
//                    debug($flux);
//                    die();
                foreach ($flux as $f => $courrier) {
                    $ret[$f] = $this->refus($courrier['Courrier']['id'], $notif['Notification']['message']);

                    $this->loadModel('Journalevent');
                    $datasSession = $this->Session->read('Auth.User');
                    $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a validé le flux ".$courrier['Courrier']['reference']." dans un circuit dont le sous-type est (".$courrier['Courrier']['soustype_id']."), le ".date('d/m/Y à H:i:s');
                    $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $courrier);

                }

                if (in_array(true, $ret) || empty($ret[0]) ) {
                    $this->Session->setFlash(__d('courrier', 'Courrier.refuslot.ok'), 'growl', array('type' => 'default'));
                } else {
                    $this->Session->setFlash(__d('courrier', 'Courrier.refuslot.error') . ' : ' . json_decode($ret), 'growl', array('type' => 'erreur'));
                }

                $this->set('flux', $flux);
            }
        }
    }

    /**
     *
     * @param type $typeTraitement
     * @param type $fluxIds
     */
    public function sendcopylot($typeTraitement = null, $fluxIds = array()) {
        $allDesktops = $this->Session->read('Auth.DesktopsByProfils');
        $this->set('allDesktops', $allDesktops);
        if (!empty($this->request->data['checkItem'])) {

            $fluxIds = $this->request->data['checkItem'];


            $flux = $this->Courrier->find(
                    'all', array(
                'fields' => array(
                    'Courrier.id',
                    'Courrier.reference',
                    'Courrier.name',
                    'Courrier.motifrefus',
                    'Courrier.created',
                    'Courrier.user_creator_id'
                ),
                'conditions' => array(
                    'Courrier.id' => $fluxIds
                ),
                'contain' => false
                    )
            );
//        debug($flux);
            $this->set('flux', $flux);
            $this->set('fluxIds', $fluxIds);
            $this->render('sendcopylot');
        }

        if (!empty($typeTraitement)) {
            $desktopId = $this->Session->read('Auth.User.Courrier.Desktop.id');
            $flux = $this->Courrier->find(
                    'all', array(
                'fields' => array(
                    'Courrier.id',
                    'Courrier.reference',
                    'Courrier.name',
                    'Courrier.motifrefus',
                    'Courrier.created',
                    'Courrier.user_creator_id'
                ),
                'conditions' => array(
                    'Courrier.id' => $this->request->data['Send']['courrier_id']
                ),
                'contain' => false
                    )
            );

            $comment = array();
            foreach ($flux as $f => $courrier) {
				$this->Courrier->copy($desktopId, $courrier['Courrier']['id'], $this->request->data['Send']['copy_id']);

                // Création du commentaire associé
				if( !empty($this->request->data['Comment']['objet']) ) {
					$this->Courrier->Comment->create();
					$comment['Comment']['owner_id'] = $this->Session->read('Auth.User.Courrier.Desktop.id');
					$comment['Comment']['target_id'] = $courrier['Courrier']['id'];
					$comment['Comment']['private'] = true;
					$comment['Comment']['created'] = date( 'Y-m-d H:i:s' );
					$comment['Comment']['modified'] = date( 'Y-m-d H:i:s' );
					$comment['Comment']['objet']= preg_replace("/[\n\r]/", " ", $this->request->data['Comment']['objet']);
					$this->Courrier->Comment->save($comment);
				}
            }
            $this->set('flux', $flux);
        }
    }

    /**
     * Fonction permettant à l'utilisateur de détacher un flux d'une bannette
     * La bannette doit posséder le flux dans un état différent de traité (=0) et appartenir au bureau de l'utilisateur connecté
     * @param type $fluxId
     */
    public function detache($fluxId, $desktopId) {
        $this->autoRender = false;
        $this->Courrier->id = $fluxId;

        if (!$this->Courrier->exists()) {
            throw new NotFoundException();
        }
        $success = false;

        if (!empty($fluxId)) {
            $this->Courrier->Bancontenu->recursive = -1;
            $success = $this->Courrier->Bancontenu->updateAll(
                    array(
                'Bancontenu.etat' => 3
                    ), array(
                'Bancontenu.courrier_id' => $fluxId,
                'Bancontenu.etat <>' => 0,
                'Bancontenu.desktop_id' => $desktopId,
                'Bancontenu.bannette_id' => BAN_COPY
                    )
            );
        }

//        if (!in_array(false, $success, true)) {
        if ($success) {
            $flux = $this->Courrier->find(
                'first',
                array(
                    'conditions' => array(
                        'Courrier.id' => $fluxId
                    ),
                    'contain' => false,
                    'recursive' => -1
                )
            );
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $msg = "L'utilisateur ". $this->Session->read('Auth.User.username')." a détaché le flux ".$flux['Courrier']['reference']." de sa bannette des flux en copie, le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);

            $this->Courrier->Bancontenu->commit();
            $this->Session->setFlash(__d('courrier', 'Courrier detached', true), 'growl');
        } else {
            $this->Courrier->Bancontenu->rollback();
            $this->Session->setFlash(__d('courrier', 'Courrier was not detached', true), 'growl');
        }

        if( $this->Session->read('Auth.User.Env.actual.name') == 'disp' ) {
          $this->redirect(array('controller' => 'environnement', 'action' => 'index/0/disp'));
        }
        else {
            $this->redirect(array('controller' => 'environnement', 'action' => 'userenv'));
        }
    }

    /**
     * Fonction permettant l'ajout d'un contact (organisme et/ou contact)
     * @param type $fluxId
     */
    public function add_organisme() {

        if (!empty($this->request->data)) {

            $this->Jsonmsg->init();
            $this->Courrier->Organisme->begin();
            if (!empty($this->request->data['Organisme']['banadresse'])) {
                $adressecomplete = addslashes( @$this->request->data['Organisme']['numvoie']) . ' ' . @$this->request->data['Organisme']['banadresse'];
            } else {
                $adressecomplete = addslashes( @$this->request->data['Organisme']['numvoie'] ) . ' ' . addslashes( @$this->request->data['Organisme']['nomvoie'] );
            }
            if( !isset($this->request->data['Organisme']['banadresse'])) {
                $this->request->data['Organisme']['banadresse'] = null;
            }
            $this->request->data['Organisme']['adressecomplete'] = $adressecomplete;
            $this->Courrier->Organisme->create($this->request->data);

			if( strpos($this->request->data['Organisme']['nomvoie'], "'") !== false ) {
				$this->request->data['Organisme']['nomvoie'] = Sanitize::clean($this->request->data['Organisme']['nomvoie'], array('encode', false));
			}
			if( strpos($this->request->data['Organisme']['adressecomplete'], "'") !== false ) {
				$this->request->data['Organisme']['adressecomplete'] = Sanitize::clean($this->request->data['Organisme']['adressecomplete'], array('encode', false));
			}
			$this->request->data['Organisme']['ville'] = strtoupper($this->request->data['Organisme']['ville']);
			$success = $this->Courrier->Organisme->save();

            if ($success) {
                // On crée le contact 0Sans contact quoiqu'il arrive !
                if (!empty($this->request->data['Organisme']['ban_id'])) {
                    $sansContact['Contact']['ban_id'] = $this->request->data['Organisme']['ban_id'];
                }
                if (!empty($this->request->data['Organisme']['bancommune_id'])) {
                    $sansContact['Contact']['bancommune_id'] = $this->request->data['Organisme']['bancommune_id'];
                }
                if (!empty($this->request->data['Organisme']['banadresse'])) {
                    $sansContact['Contact']['banadresse'] = $this->request->data['Organisme']['banadresse'];
                    $sansContact['Contact']['nomvoie'] = @$this->request->data['Organisme']['banadresse'];
                }
                else {
                    $sansContact['Contact']['nomvoie'] = addslashes( @$this->request->data['Organisme']['nomvoie'] );
                }

                if( !isset($this->request->data['Organisme']['banadresse'])) {
                    $sansContact['Contact']['banadresse'] = null;
                }
                $sansContact['Contact']['addressbook_id'] = $this->request->data['Organisme']['addressbook_id'];
                $sansContact['Contact']['organisme_id'] = $this->Courrier->Organisme->id;
                $sansContact['Contact']['adresse'] = $adressecomplete;
                $sansContact['Contact']['numvoie'] = @$this->request->data['Organisme']['numvoie'];
                $sansContact['Contact']['cp'] = $this->request->data['Organisme']['cp'];
                $sansContact['Contact']['compl'] = $this->request->data['Organisme']['compl'];
                $sansContact['Contact']['ville'] = strtoupper($this->request->data['Organisme']['ville']);
                $sansContact['Contact']['name'] = ' Sans contact';
                $sansContact['Contact']['nom'] = '0Sans contact';
                $this->Courrier->Organisme->Contact->create($sansContact['Contact']);
                $success = $this->Courrier->Organisme->Contact->save() && $success;

                $contactInfos = array(
                    'id' => $this->Courrier->Organisme->Contact->id,
                    'name' => $sansContact['Contact']['name']
                );

            }

            // Si on crée un contact au moment de l'ajout d'un organisme
            if ($success && $this->request->data['Organisme']['infoscontact'] == 1) {
                if (!empty($this->request->data['Organisme']['ban_id'])) {
                    $this->request->data['Contact']['ban_id'] = $this->request->data['Organisme']['ban_id'];
                }
                if (!empty($this->request->data['Organisme']['bancommune_id'])) {
                    $this->request->data['Contact']['bancommune_id'] = $this->request->data['Organisme']['bancommune_id'];
                }
                if (!empty($this->request->data['Organisme']['banadresse'])) {
                    $this->request->data['Contact']['banadresse'] = $this->request->data['Organisme']['banadresse'];
                    $this->request->data['Contact']['nomvoie'] = @$this->request->data['Organisme']['banadresse'];
                }
                else {
                    $this->request->data['Contact']['nomvoie'] = @$this->request->data['Organisme']['nomvoie'];
                }
                if( !isset($this->request->data['Organisme']['banadresse'])) {
                    $this->request->data['Contact']['banadresse'] = null;
                }
                $this->request->data['Contact']['addressbook_id'] = $this->request->data['Organisme']['addressbook_id'];
                $this->request->data['Contact']['organisme_id'] = $this->Courrier->Organisme->id;
                $this->request->data['Contact']['adresse'] = $adressecomplete;
                $this->Courrier->Organisme->Contact->create($this->request->data['Contact']);
                $success = $this->Courrier->Organisme->Contact->save() && $success;

                $contactInfos = array(
                    'id' => $this->Courrier->Organisme->Contact->id,
                    'name' => $this->request->data['Contact']['name']
                );
            }

            if ($success) {
                $this->Courrier->Contact->MiseajourData();
                // on stocke qui fait quoi quand

                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a créé l'organisme ".$this->request->data['Organisme']['name']." le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $this->request->data);

                $this->autoRender = false;
                $this->Courrier->Organisme->commit();


                if( Configure::read('Webservice.GRC') ) {
                    $this->loadModel('Connecteur');
                    $hasGrcActif = $this->Connecteur->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Connecteur.name ILIKE' => '%GRC%',
                                'Connecteur.use_grc' => true
                            ),
                            'contain' => false
                        )
                    );
                    if( !empty( $hasGrcActif )) {
                        $localeo = new LocaleoComponent;
                        $this->request->data['Organisme']['externalId'] = $this->Courrier->Organisme->id;
                        $retour = $localeo->createOrganismeGRC( $this->request->data );
                        $result = $retour;
                        $this->Jsonmsg->json['contactgrc_id'] = $result->id;

                        if( !empty($result->id) ) {
                            $this->Courrier->Organisme->updateAll(
                                array( 'Organisme.contactgrc_id' => $result->id),
                                array('Organisme.id' => $this->Courrier->Organisme->id)
                            );
                        }
                    }
                }

                $return = json_encode(
                    array(
                        'id' => $this->Courrier->Organisme->id,
                        'name' => $this->request->data['Organisme']['name'],
                        'contacts' => $contactInfos
                    )
                );

                echo $return;


            } else {
                $this->Courrier->Organisme->rollback();
            }
        }
        $this->_setOptions();
    }

    /**
     * Fonction permettant l'édition d'un contact (organisme et/ou contact)
     * @param type $fluxId
     */
    public function edit_organisme($orgId, $courrierId = null) {

        if (!empty($this->request->data)) {
            $this->Courrier->Organisme->begin();
            if (!empty($this->request->data['Organisme']['banadresse'])) {
                $adressecomplete = addslashes( @$this->request->data['Organisme']['numvoie'] ). ' ' . @$this->request->data['Organisme']['banadresse'];
            } else {
//                $adressecomplete = addslashes( @$this->request->data['Organisme']['numvoie'] ). ' ' . addslashes( @$this->request->data['Organisme']['nomvoie'] );
				$adressecomplete = @$this->request->data['Organisme']['numvoie']. ' ' . @$this->request->data['Organisme']['nomvoie'];
            }
            if( !isset($this->request->data['Organisme']['banadresse'])) {
                $this->request->data['Organisme']['banadresse'] = null;
            }
            $this->request->data['Organisme']['adressecomplete'] = $adressecomplete;
            $this->Courrier->Organisme->create($this->request->data);

			if( strpos($this->request->data['Organisme']['nomvoie'], "'") !== false ) {
				$this->request->data['Organisme']['nomvoie'] = Sanitize::clean($this->request->data['Organisme']['nomvoie'], array('encode', false));
			}
			if( strpos($this->request->data['Organisme']['adressecomplete'], "'") !== false ) {
				$this->request->data['Organisme']['adressecomplete'] = Sanitize::clean($this->request->data['Organisme']['adressecomplete'], array('encode', false));
			}
			$this->request->data['Organisme']['ville'] = strtoupper($this->request->data['Organisme']['ville']);
            $success = $this->Courrier->Organisme->save();
            if ($success) {

                // Si on décide de diffuser la modif de l'adresse de l'organisme, alors on  enregistre l'adresse de l'organisme pour TOUS les contacts
                if( Configure::read('AdresseOrganisme.DiffuseContact') == 'yes' && $this->request->data['Organisme']['diffuse_contact'] == 'Oui' ) {
                    $this->Courrier->Organisme->Contact->begin();
                    $this->Courrier->Organisme->Contact->create();

                    $success = $this->Courrier->Organisme->Contact->updateAll(
                        array(
//                            'Contact.ban_id' => isset( $this->request->data['Organisme']['ban_id'] ) ? $this->request->data['Organisme']['ban_id'] : NULL,
//                            'Contact.banadresse' => isset( $this->request->data['Organisme']['banadresse'] ) ?  "'".$this->request->data['Organisme']['banadresse']."'" : null,
                            'Contact.nomvoie' => isset( $this->request->data['Organisme']['nomvoie'] ) ? "'".$this->request->data['Organisme']['nomvoie']."'" : null,
                            'Contact.numvoie' => isset( $this->request->data['Organisme']['numvoie'] ) ? "'".$this->request->data['Organisme']['numvoie']."'" : null,
                            'Contact.compl' => isset( $this->request->data['Organisme']['compl'] ) ? "'".$this->request->data['Organisme']['compl']."'" : null,
                            'Contact.cp' => isset( $this->request->data['Organisme']['cp'] ) ? "'".$this->request->data['Organisme']['cp']."'" : null,
                            'Contact.adressecomplete' => isset( $this->request->data['Organisme']['adressecomplete'] ) ? "'".$this->request->data['Organisme']['adressecomplete']."'" : null,
                            'Contact.adresse' => isset( $this->request->data['Organisme']['adresse'] ) ? "'".$this->request->data['Organisme']['adresse']."'" : "'".@$this->request->data['Organisme']['adressecomplete']."'",
                            'Contact.ville' => isset( $this->request->data['Organisme']['ville'] ) ? "'".strtoupper($this->request->data['Organisme']['ville'])."'" : null,
                            'Contact.pays' => isset( $this->request->data['Organisme']['pays'] ) ? "'".$this->request->data['Organisme']['pays']."'" : null
                        ),
                        array( 'Contact.organisme_id' => $this->request->data['Organisme']['id'] )
                    ) && $success;

                    if( $success ) {
                        $this->Courrier->Organisme->Contact->commit();
                    }
                    else {
                        $this->Courrier->Organisme->Contact->rollback();
                    }
                }

                // Si on change l'adresse de l'organisme, on répercute la nouvelle adresse quoiqu'il arrive sur le 0Sans contact
                $contactSanscontact = $this->Courrier->Organisme->Contact->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Contact.organisme_id' => $orgId,
                            'Contact.name' => ' Sans contact',
                            'Contact.nom' => '0Sans contact'
                        ),
                        'recursive' => -1,
                        'contain' => false
                    )
                );

                if( !empty($contactSanscontact) ) {
                    $success = $this->Courrier->Organisme->Contact->updateAll(
                        array(
    //                        'Contact.ban_id' => isset( $this->request->data['Organisme']['ban_id'] ) ? $this->request->data['Organisme']['ban_id'] : NULL,
    //                        'Contact.bancommune_id' => isset( $this->request->data['Organisme']['bancommune_id'] ) ?  "'".$this->request->data['Organisme']['bancommune_id']."'" : null,
    //                        'Contact.banadresse' => isset( $this->request->data['Organisme']['banadresse'] ) ?  "'".$this->request->data['Organisme']['banadresse']."'" : null,
                            'Contact.nomvoie' => isset( $this->request->data['Organisme']['nomvoie'] ) ? "'".$this->request->data['Organisme']['nomvoie']."'" : null,
                            'Contact.numvoie' => isset( $this->request->data['Organisme']['numvoie'] ) ? "'".$this->request->data['Organisme']['numvoie']."'" : null,
                            'Contact.compl' => isset( $this->request->data['Organisme']['compl'] ) ? "'".$this->request->data['Organisme']['compl']."'" : null,
                            'Contact.cp' => isset( $this->request->data['Organisme']['cp'] ) ? "'".$this->request->data['Organisme']['cp']."'" : null,
                            'Contact.adressecomplete' => isset( $this->request->data['Organisme']['adressecomplete'] ) ? "'".$this->request->data['Organisme']['adressecomplete']."'" : null,
                            'Contact.adresse' => isset( $this->request->data['Organisme']['adresse'] ) ? "'".$this->request->data['Organisme']['adresse']."'" : "'".@$this->request->data['Organisme']['adressecomplete']."'",
                            'Contact.ville' => isset( $this->request->data['Organisme']['ville'] ) ? "'".strtoupper($this->request->data['Organisme']['ville'])."'" : null,
                            'Contact.pays' => isset( $this->request->data['Organisme']['pays'] ) ? "'".$this->request->data['Organisme']['pays']."'" : null
                        ),
                        array( 'Contact.id' => $contactSanscontact['Contact']['id'] )
                    ) && $success;
                }

                if( $success ) {
                    $this->Courrier->Organisme->Contact->commit();
                }
                else {
                    $this->Courrier->Organisme->Contact->rollback();
                }




                // on stocke qui fait quoi quand
                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié l'organisme ".$this->request->data['Organisme']['name']." (".$this->request->data['Organisme']['id'].") le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $this->request->data);

                $courrier = $this->Courrier->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Courrier.id' => $courrierId
                        ),
                        'contain' => array(
                            'Contact'
                        ),
                        'recursive' => -1
                    )
                );
                if( !empty($courrier['Courrier']['contact_id'])) {
                    $contactsData = array(
                        'id' => $courrier['Courrier']['contact_id'],
                        'name' => $courrier['Contact']['name']
                    );
                }
                else {
                    $contactsData = array(
                        'id' => $contactSanscontact['Contact']['id'],
                        'name' => $contactSanscontact['Contact']['name']
                    );
                }

                $this->autoRender = false;
                $return = json_encode(
                        array(
                            'id' => $this->Courrier->Organisme->id,
                            'name' => $this->request->data['Organisme']['name'],
                            'contacts' => $contactsData
                        )
                );
                echo $return;
                $this->Courrier->Organisme->commit();

				if( Configure::read('Webservice.GRC') && Configure::read('GRC.Createcontact') ) {
                    $this->loadModel('Connecteur');
                    $hasGrcActif = $this->Connecteur->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Connecteur.name ILIKE' => '%GRC%',
                                'Connecteur.use_grc' => true
                            ),
                            'contain' => false
                        )
                    );
                    if( !empty( $hasGrcActif )) {
                        $localeo = new LocaleoComponent;
                        $org = $this->Courrier->Organisme->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Organisme.id' => $orgId
                                ),
                                'contain' => false,
                                'recursive' => -1
                            )
                        );
                        $this->request->data['Organisme']['externalId'] = $orgId;
                        $this->request->data['Organisme']['contactgrc_id'] = $org['Organisme']['contactgrc_id'];
                        $retour = $localeo->createContactGRC( $this->request->data );
                        $result = $retour;
                    }
                }
            } else {
                $this->Courrier->Organisme->rollback();
            }
        } else {
            if (Configure::read('Conf.SAERP')) {
                $contain = array(
                    'Operation',
                    'Activite'
                );
            } else {
                $contain = array(
                    'Activite'
                );
            }
            $this->request->data = $this->Courrier->Organisme->find(
                    'first', array(
                'conditions' => array(
                    'Organisme.id' => $orgId,
                    'Organisme.active' => true
                ),
                'contain' => $contain,
                'recursive' => -1
                    )
            );
        }

        $this->_setOptions();
    }

    /**
     * Fonction permettant l'ajout d'un contact (organisme et/ou contact)
     * @param type $fluxId
     */
    public function add_contact($orgId) {
        if (!empty($this->request->data)) {
            $this->Courrier->Contact->begin();
            if (!empty($this->request->data['Contact']['organisme_id'])) {
                $organismeSelected = $this->Courrier->Organisme->find('first', array(
                    'fields' => array('Organisme.name', 'Organisme.addressbook_id'),
                    'conditions' => array('Organisme.id' => $this->request->data['Contact']['organisme_id'], 'Organisme.active' => true),
                    'contain' => false,
                    'recursive' => -1
                ));
                $organismeSelectedAddressbookId = $organismeSelected['Organisme']['addressbook_id'];
            }
            $this->request->data['Contact']['addressbook_id'] = $organismeSelectedAddressbookId;
            if (!empty($this->request->data['Contact']['banadresse'])) {
                $adressecomplete = addslashes( @$this->request->data['Contact']['numvoie'] ). ' ' . @$this->request->data['Contact']['banadresse'];
            } else {
                $adressecomplete = addslashes( @$this->request->data['Contact']['numvoie'] ). ' ' . addslashes( @$this->request->data['Contact']['nomvoie'] );
            }
             if( !isset($this->request->data['Contact']['banadresse'])) {
                $this->request->data['Contact']['banadresse'] = null;
            }

            $this->request->data['Contact']['adresse'] = $adressecomplete;
            $this->Courrier->Contact->create($this->request->data);

			if( strpos($this->request->data['Contact']['nomvoie'], "'") !== false ) {
				$this->request->data['Contact']['nomvoie'] = Sanitize::clean($this->request->data['Contact']['nomvoie'], array('encode', false));
			}
			if( strpos($this->request->data['Contact']['adressecomplete'], "'") !== false ) {
				$this->request->data['Contact']['adressecomplete'] = Sanitize::clean($this->request->data['Contact']['adressecomplete'], array('encode', false));
			}
			$this->request->data['Contact']['ville'] = strtoupper($this->request->data['Contact']['ville']);
            $success = $this->Courrier->Contact->save();
            if ($success) {
                $this->Courrier->Contact->MiseajourData();
                // on stocke qui fait quoi quand

                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a créé le contact ".$this->request->data['Contact']['name']." le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $this->request->data);

                $this->autoRender = false;
                $return = json_encode(
                        array(
                            'contacts' => array(
                                'id' => $this->Courrier->Contact->id,
                                'name' => $this->request->data['Contact']['name'],
                            )
                        )
                );
                echo $return;
                $this->Courrier->Contact->commit();

				if( Configure::read('Webservice.GRC') && Configure::read('GRC.Createcontact') ) {
                    $this->loadModel('Connecteur');
                    $hasGrcActif = $this->Connecteur->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Connecteur.name ILIKE' => '%GRC%',
                                'Connecteur.use_grc' => true
                            ),
                            'contain' => false
                        )
                    );
                    if( !empty( $hasGrcActif )) {
                        $localeo = new LocaleoComponent;
                        $this->request->data['Contact']['externalId'] = $this->Courrier->Contact->id;
                        $retour = $localeo->createContactGRC( $this->request->data );
                        $result = $retour;
                        $this->Jsonmsg->json['contactgrc_id'] = $result->id;

                        if( !empty($result->id) ) {
                            $this->Courrier->Contact->updateAll(
                                array( 'Contact.contactgrc_id' => $result->id),
                                array('Contact.id' => $this->Courrier->Contact->id)
                            );
                        }
                    }
                }

            } else {
                $this->Courrier->Contact->rollback();
            }
        }
        $this->set('orgId', $orgId);
        $organisme = $this->Courrier->Contact->Organisme->find(
                'first', array(
            'conditions' => array(
                'Organisme.id' => $orgId,
                'Organisme.active' => true
            ),
            'contain' => false
                )
        );
        $this->set(compact('organisme'));
        $this->_setOptions();
    }

    /**
     * Fonction permettant l'édition d'un contact (organisme et/ou contact)
     * @param type $fluxId
     */
    public function edit_contact($contactId) {
        if (!empty($this->request->data)) {
            $this->Courrier->Contact->begin();
            if (!empty($this->request->data['Contact']['organisme_id'])) {
                $organismeSelected = $this->Courrier->Organisme->find('first', array(
                    'fields' => array('Organisme.name', 'Organisme.addressbook_id'),
                    'conditions' => array('Organisme.id' => $this->request->data['Contact']['organisme_id'], 'Organisme.active' => true),
                    'contain' => false,
                    'recursive' => -1
                ));
                $organismeSelectedAddressbookId = $organismeSelected['Organisme']['addressbook_id'];
            }
            $this->request->data['Contact']['addressbook_id'] = $organismeSelectedAddressbookId;
            if (!empty($this->request->data['Contact']['banadresse'])) {
                $adressecomplete = addslashes( @$this->request->data['Contact']['numvoie'] ) . ' ' . @$this->request->data['Contact']['banadresse'];
            } else {
				$adressecomplete = @$this->request->data['Contact']['numvoie'] . ' ' . @$this->request->data['Contact']['nomvoie'];
            }
            if( !isset($this->request->data['Contact']['banadresse'])) {
                $this->request->data['Contact']['banadresse'] = null;
            }
            $this->request->data['Contact']['adressecomplete'] = null;


			if( strpos($this->request->data['Contact']['nomvoie'], "'") !== false ) {
				$this->request->data['Contact']['nomvoie'] = Sanitize::clean($this->request->data['Contact']['nomvoie'], array('encode', false));
			}
			if( strpos($this->request->data['Contact']['adressecomplete'], "'") !== false ) {
				$this->request->data['Contact']['adressecomplete'] = str_replace( "'", "''", $this->request->data['Contact']['adressecomplete'] );
			}

            $this->request->data['Contact']['adresse'] = $adressecomplete;
			$this->request->data['Contact']['ville'] = strtoupper($this->request->data['Contact']['ville']);
            $this->Courrier->Contact->create($this->request->data['Contact']);
            $success = $this->Courrier->Contact->save();
            if ($success) {

                $this->Courrier->Contact->MiseajourData();
                // on stocke qui fait quoi quand


                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le contact ".$this->request->data['Contact']['name']." (".$this->request->data['Contact']['id'].") le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $this->request->data);

                $this->autoRender = false;
                $return = json_encode(
                        array(
                            'contacts' => array(
                                'id' => $contactId,
                                'name' => $this->request->data['Contact']['name'],
                                'organisme_id' => $this->request->data['Contact']['organisme_id']
                            )
                        )
                );
                echo $return;

                $this->Courrier->Contact->commit();


				if( Configure::read('Webservice.GRC') && Configure::read('GRC.Createcontact') ) {
                    $this->loadModel('Connecteur');
                    $hasGrcActif = $this->Connecteur->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Connecteur.name ILIKE' => '%GRC%',
                                'Connecteur.use_grc' => true
                            ),
                            'contain' => false
                        )
                    );
                    if( !empty( $hasGrcActif )) {
                        $localeo = new LocaleoComponent;
                        $contact = $this->Courrier->Contact->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Contact.id' => $contactId
                                ),
                                'contain' => false,
                                'recursive' => -1
                            )
                        );
                        $this->request->data['Contact']['externalId'] = $contactId;
                        $this->request->data['Contact']['contactgrc_id'] = $contact['Contact']['contactgrc_id'];
                        $retour = $localeo->createContactGRC( $this->request->data );
                        $result = $retour;
                    }
                }
            } else {
                $this->Courrier->Contact->rollback();
            }
        } else {
            $this->request->data = $this->Courrier->Contact->find(
                    'first', array(
                'fields' => array_merge(
                        $this->Courrier->Contact->fields(), $this->Courrier->Contact->Organisme->fields()
                ),
                'conditions' => array(
                    'Contact.id' => $contactId,
                    'Contact.active' => true
                ),
                'contain' => array(
                    'Organisme',
                    'Operation',
                    'Event'
                ),
                'recursive' => -1
                    )
            );
        }
        $this->_setOptions();
    }

    /**
     * Suppression des flux par lot
     * @param $fluxIds
     */
    public function deletelot($fluxIds = array()) {
        $this->autoRender = false;
        $desktop_id = $this->Session->read('Auth.User.Courrier.Desktop.id');

        if (!empty($this->request->data['checkItem'])) {
            $fluxIds = $this->request->data['checkItem'];

            $deleteAll = array();
            $bancontenus = array_keys($this->Courrier->Bancontenu->find('list', array('conditions' => array('Bancontenu.courrier_id' => $fluxIds))));
            $this->Courrier->begin();

            $comments = $this->Courrier->getComments($fluxIds);
			unset( $comments['owners']);
            if (!empty($comments)) {
                foreach ($comments as $types) {
					if( $types != 'owners' ) {
						foreach ($types as $c => $comment) {
							$commentId = $comment['Comment']['id'];
							$deleteAll[] = $this->Courrier->Comment->delete($commentId);
						}
					}
                }
            }

            for ($i = 0; $i < count($bancontenus); $i++) {
                $deleteAll[] = $this->Courrier->Bancontenu->delete($bancontenus[$i]);
            }
            $deleteAll[] = $this->Courrier->delete($fluxIds);

            if (!in_array(false, $deleteAll, true)) {
                $this->Courrier->commit();
                $this->Session->setFlash(__d('courrier', 'Courriers deleted', true), 'growl');
            } else {
                $this->Courrier->rollback();
                $this->Session->setFlash(__d('courrier', 'Courriers was not deleted', true), 'growl');
            }
        }
    }

    /**
     * Fonction utilisée uniquement par la SAERP
     * @param type $field
     * @return type
     *
     */
    public function ajaxformtype($field = null) {
        if ($field == 'marcheid') {
            $marcheOperation = array();
            $this->autoRender = false;

            if (!empty($this->request->data['Courrier']['marche_id'])) {
                $marche = $this->Courrier->Marche->find(
                    'first',
                    array(
                        'fields' => array_merge(
                            $this->Courrier->Marche->fields(),
                            $this->Courrier->Marche->Operation->fields(),
                            $this->Courrier->Marche->Operation->Type->fields()
                        ),
                        'conditions' => array(
                            'Marche.id' => $this->request->data['Courrier']['marche_id']
                        ),
                        'joins' => array(
                            $this->Courrier->Marche->join('Operation'),
                            $this->Courrier->Marche->Operation->join('Type')
                        ),
                        'contain' => false
                    )
                );
                $marcheOperation = $marche['Type']['id'];
            }
        }

        return $marcheOperation;
    }

    /**
     * Fonction permettant à l'utilisateur de détacher par lots des flux de sa bannette  des flux en copie
     * La bannette doit posséder le flux dans un état différent de 3 et appartenir au bureau de l'utilisateur connecté
     * @param type $fluxId
     */
    public function detachelot($typeTraitement = null, $fluxIds = array()) {
//        $desktopId = $this->Session->read('Auth.User.Courrier.Desktop.id');

        $this->autoRender = false;
        $profil = $this->Session->read('Auth.User.Env.actual.name');

        if ($profil == 'disp') {
            $unlinkCourriers = $this->request->data['DispForm']['itemToLink'];
            $desktopId = $this->request->data['DispForm']['checkDesktop'];
//            $desktopId = $this->Session->read('Auth.User.Desktop.id' );
        } else {
            $unlinkCourriers = $this->request->data['checkItem'];
            $desktopId = $this->request->data['checkDesktop'];
        }

        if (!empty($unlinkCourriers)) {
            foreach ($unlinkCourriers as $fluxId) {

                $success = array();
                $this->Courrier->Bancontenu->recursive = -1;
                $success[] = $this->Courrier->Bancontenu->updateAll(
                    array(
                        'Bancontenu.etat' => 3
                    ),
                    array(
                        'Bancontenu.courrier_id' => $fluxId,
                        'Bancontenu.etat <>' => 0,
                        'Bancontenu.desktop_id' => $desktopId,
                        'Bancontenu.bannette_id' => 7
                    )
                );
            }

            if (!in_array(false, $success, true)) {
                $this->Courrier->Bancontenu->commit();
                $this->Session->setFlash(__d('courrier', 'Courriers detached', true), 'growl');
                return true;
            } else {
                $this->Courrier->Bancontenu->rollback();
                $this->Session->setFlash(__d('courrier', 'Courriers were not detached', true), 'growl');
				return false;
            }
        }
    }

    /**
     * Indique si le flux est en copie
     */
    public function isInCopy() {
        $this->autoRender = false;
//debug($this->request->data);
        if( !empty($this->request->data['InCopy']) || !empty($this->request->data['fluxChoix']) ) {
            if( !empty($this->request->data['fluxChoix'])) {
                $fluxChoix = $this->request->data['fluxChoix'];
            }
            else {
                $fluxChoix = $this->request->data['InCopy'];
            }
            foreach ($fluxChoix as $key => $fluxId) {
                $flux = $this->Courrier->find(
                        'first', array(
                    'fields' => array_merge(
                            $this->Courrier->fields(), $this->Courrier->Bancontenu->fields(), $this->Courrier->Desktop->fields()
                    ),
                    'conditions' => array(
                        'Courrier.id' => $fluxId
                    ),
                    'joins' => array(
                        $this->Courrier->join('Bancontenu'),
                        $this->Courrier->Bancontenu->join('Desktop')
                    ),
                    'contain' => false
                        )
                );

                $desktop_id = $this->Session->read('Auth.User.Desktop.id');
                $env = $this->Session->read('Auth.User.Env');
                if( $env['actual']['name'] == 'disp' ) {
                    $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
                    $groupId = Hash::extract($secondaryDesktops, '{n}.Profil.id');
                    foreach( $secondaryDesktops as $i => $value) {
                        if( $value['Profil']['id'] == 7 ) {
                            $desktop_id = $value['id'];
                        }
                    }
                }

                $isInCopy[] = $this->_inBannetteCopy($fluxId, $desktop_id);
            }
            if (in_array(false, $isInCopy)) {
                $res = false;
            } else {
                $res = true;
            }
        }
        else {
            $fluxChoix = array();
            $res = false;
        }
        return json_encode(array('incopy' => $res));
    }


    function getFluxPreview($reference) {
        $this->autoRender = false;
        $courrier = $this->Courrier->find('first', array(
            'conditions' => array(
                'Courrier.reference' => $reference
            ),
            'contain' => array(
                'Document' => array(
                    'conditions' => array(
                        'Document.main_doc' => true
                    )
                )
            )
        ));
        $documentId = null;
        if (!empty($courrier['Document'])) {
            $documentId = $courrier['Document'][0]['id'];
        }
        return json_encode(array('documentId' => $documentId));
    }

    function __getType($array) {
        return $array['Type'];
    }

    function __getSousType($array) {
        return $array['Soustype'];
    }

    function __getOrganisme($array) {
        return $array['Organisme'];
    }

    function __getContact($array) {
        return $array['Contact'];
    }

    function updateContext($fluxId, $desktopId) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn);
        $this->Courrier->recursive = -1;
        $qdCourrier = array(
            'fields' => array_merge(
                    $this->Courrier->fields(), $this->Courrier->Soustype->fields()
            ),
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'joins' => array(
                $this->Courrier->join('Soustype')
            ),
            'recursive' => -1
        );
        $courrier = $this->Courrier->find('first', $qdCourrier);

        $parent = array();
        if (!empty($courrier['Courrier']['parent_id'])) {
            $qdParent = array(
                'fields' => array(
                    'Courrier.id',
                    'Courrier.name'
                ),
                'conditions' => array(
                    'Courrier.id' => $courrier['Courrier']['parent_id']
                ),
                'recursive' => -1
            );
            $parent = $this->Courrier->find('first', $qdParent);
        }

        $sortant = false;
        if (isset($courrier['Soustype']['entrant']) && $courrier['Soustype']['entrant'] == 0) {
            $sortant = true;
        }

        $userDesktops = $this->getSessionAllDesktops();
        if (in_array($desktopId, array_keys($userDesktops))) {
            $this->Session->write('Auth.User.Courrier.Desktop.id', $desktopId);
            $aro = array('model' => 'Desktop', 'foreign_key' => $desktopId);
        } else {
            $aro = array('model' => 'Desktop', 'foreign_key' => $this->Session->read('Auth.User.Desktop.id'));
        }
        $rights = array(
            'getInfos' => $this->Acl->check($aro, 'controllers/Courriers/getInfos'),
            'getMeta' => $this->Acl->check($aro, 'controllers/Courriers/getMeta'),
            'getTache' => $this->Acl->check($aro, 'controllers/Courriers/getTache'),
            'getAffaire' => $this->Acl->check($aro, 'controllers/Courriers/getAffaire'),
            'getFlowControls' => $this->Acl->check($aro, 'controllers/Courriers/getFlowControls'),
            'setInfos' => $this->Acl->check($aro, 'controllers/Courriers/setInfos'),
            'setMeta' => $this->Acl->check($aro, 'controllers/Courriers/setMeta'),
            'setTache' => $this->Acl->check($aro, 'controllers/Courriers/setTache'),
            'setAffaire' => $this->Acl->check($aro, 'controllers/Courriers/setAffaire'),
            'getArs' => true,
            'getRelements' => $this->Acl->check($aro, 'controllers/Courriers/getRelements'),
            'getDocuments' => $this->Acl->check($aro, 'controllers/Courriers/getDocuments'),
            'getCircuit' => $this->Acl->check($aro, 'controllers/Courriers/getCircuit'),
            'getSms' => $this->Acl->check($aro, 'controllers/Courriers/getSms'),
            'edit' => true
        );
        if (!$rights['setInfos'] && !$rights['setMeta'] && !$rights['setTache'] && !$rights['setAffaire']) {
            $rights['edit'] = false;
        }

        $this->set('mainDoc', $this->Courrier->getLightMainDoc($fluxId));
        $this->set('fluxId', $fluxId);
        $this->set('sortant', $sortant);
        $this->set('parent', $parent);
        $this->set('rights', $rights);

		$connecteurSMSActif = false;
		if( Configure::read('Webservice.SMS') ) {
			$this->loadModel('Connecteur');
			$hasSmsActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%LsMessage%',
						'Connecteur.use_sms' => true
					),
					'contain' => false
				)
			);
			if( !empty( $hasSmsActif )) {
				$connecteurSMSActif = true;
			}
		}
		$this->set('connecteurSMSActif', $connecteurSMSActif);
    }


    /**
     *
     * @param type $typeTraitement
     * @param type $fluxIds
     */
    public function sendlot($typeTraitement = null, $fluxIds = array()) {
        $desktopsToSend = $this->Courrier->User->Desktop->Desktopmanager->getAllDesktopsByProfils(array( VALEDIT_GID, VAL_GID, ARCH_GID));
        $this->set('desktopsToSend', $desktopsToSend);

        if (!empty($this->request->data['checkItem'])) {
            $fluxIds = $this->request->data['checkItem'];

            if ( !empty( $typeTraitement ) ) {
                foreach ($fluxIds as $f => $fluxId) {
//
                    $this->sendwkf(null, $fluxId);

                    $flux = $this->Courrier->find(
                        'first',
                        array(
                            'fields' => array(
                                'Courrier.id',
                                'Courrier.reference',
                                'Courrier.name',
                                'Courrier.soustype_id',
                                'Courrier.motifrefus',
                                'Courrier.created',
                                'Courrier.user_creator_id'
                            ),
                            'conditions' => array(
                                'Courrier.id' => $fluxId
                            ),
                            'contain' => false,
                            'recursive' => -1
                        )
                    );
                    $this->loadModel('Journalevent');
                    $datasSession = $this->Session->read('Auth.User');
                    $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a envoyé le flux ".$flux['Courrier']['reference']." dans un circuit dont le sous-type est (".$flux['Courrier']['soustype_id']."), le ".date('d/m/Y à H:i:s');
                    $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);
                }
            }
        }
    }


    /** Envoie pour la GRC Localeo
     *
     * @param type $courrier_id
     */
    public function sendToGrc($courrier_id) {
        $this->autoRender = false;
        $this->Jsonmsg->valid('Ok tout va bien');
        $retour = $this->Courrier->sendGrcAllowed($courrier_id);

        $ret = json_decode( $retour );

        if ( !empty( $ret->id ) ) {
            $this->Session->setFlash('Envoi à la GRC réussi', 'growl', array('type' => 'default'));
        } else {
            $this->Session->setFlash("Echec lors de l'envoi à la GRC". ' : ' . $ret, 'growl', array('type' => 'erreur'));
        }
    }

	/**
	 * Récupération des SMS adressés vers l'extérieur
	 *
	 * @logical-group Flux
	 * @logical-group Context
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id
	 * @throws NotFoundException
	 * @return void
	 */
	public function getSms($fluxId) {
		$smsDatas = $this->Courrier->Sms->find(
			'all',
			array(
				'conditions' => array(
					'Sms.courrier_id' => $fluxId
				),
				'contain' => false
			)
		);
		$this->set('smsDatas', $smsDatas );
		$this->set('fluxId', $fluxId );
	}

	/**
	 * Fonction utilisée pour afficher le n° de référence lors de la création
	 * @param type $field
	 * @return type
	 *
	 */
	public function ajaxreference( $fluxId ) {
		$this->autoRender = false;
		$reference = '';
		if (!empty($fluxId)) {
			$courrier = $this->Courrier->find(
				'first',
				array(
					'conditions' => array(
						'Courrier.id' => $fluxId
					),
					'contain' => false
				)
			);
			$reference = $courrier['Courrier']['reference'];
		}

		return $reference;
	}

	/**
	 * Fonction utilisée pour retourner les infos du contact associé
	 * @param type $field
	 * @return type
	 *
	 */
	public function ajaxcontact( $contactId ) {
		$this->autoRender = false;
		$email = '';
		if (!empty($contactId) ) {
			$contact = $this->Courrier->Contact->find(
				'first',
				array(
					'conditions' => array(
						'Contact.id' => $contactId
					),
					'contain' => false
				)
			);
			$email = $contact['Contact']['email'];
		}

		return $email;
	}

	public function download_relement( $name, $courrierId ) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
		$this->autoRender = false;
		$courrier = $this->Courrier->find(
			'first',
			array(
				'conditions' => array(
					'Courrier.id' => $courrierId
				),
				'contain' => ['Soustype']
			)
		);

		$file = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'webdav' . DS . $conn .DS . 'modeles' . DS . $courrier['Courrier']['soustype_id'] . DS . $name;
		if (is_file($file) && file_exists($file)) {
			$this->autoRender = false;
			Configure::write('debug', 0);
			header("Pragma: public");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"" . $name . "\"");
			header("Content-length: " . filesize($file) );
			print file_get_contents($file);
			exit();
		}
	}

}

?>
