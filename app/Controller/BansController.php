<?php

/**
 * Référentiel des voies FANTOIR
 *
 *
 * Referentielsfantoir controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller
 */
class BansController extends AppController {

	/**
	 * Controller uses
	 *
	 * @var array
	 * @access public
	 */
	public $uses = array('Ban');

	/**
	 * Gestion des référentiels de voie
	 *
	 * @logical-group Carnet d'adresse
	 * @user-profile Admin
	 *
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
                    '<a href="environnement/index/0/admin">'.__d('menu', 'Administration', true).'</a>',
                    __d('menu', 'gestionBans', true)
                ));
	}


//    public function beforeFilter() {
//        ini_set( 'max_execution_time', 0 );
//        ini_set( 'memory_limit', '1024M' );
//        parent::beforeFilter();
//    }
		/**
	 * Récupération de la liste des circuits de traitements (ajax)
	 *
	 * @logical-group Circuits de traitements
	 * @logical-group CakeFlow
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function getBan() {
        $bans_tmp = $this->Ban->find(
			"all",
			array(
				'order' => 'Ban.name',
                'contain' => array(
                    'Bancommune' => array(
                        'order' => array( 'Bancommune.name ASC')
                    )
                ),
			)
		);
		$bans = array();
		foreach ($bans_tmp as $i => $item) {
			$item['right_edit'] = true;
			$item['right_delete'] = true;
			$bans[] = $item;
		}
		$this->set('bans', $bans);
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
	}

    /**
	 * Edition d'un méta-donnée
	 *
	 * @logical-group Méta-données
	 * @user-profil Admin
	 *
	 * @access public
	 * @param integer $id identifiant de la méta-donnée
	 * @return void
	 */
	public function edit($id = null) {

		if (!empty($this->request->data)) {
			$this->Jsonmsg->init();
			$this->Ban->create($this->request->data);

			if ($this->Ban->save()) {
				$this->Jsonmsg->valid();
			}
			$this->Jsonmsg->send();
		} else {
			$querydata = array(
				'conditions' => array(
					'Ban.id' => $id
				),
                'recursive' => -1
			);
			$this->request->data = $this->Ban->find('first', $querydata);
		}

        $this->set( 'banId', $id );
	}


	/**
	 * Importation de référentiels à partir d'un fichier BAN
	 *
	 * @logical-group Referentielfantoir
	 * @user-profile Admin
	 *
	 *	Modifier le php.ini /etc/php5/apache2/php.ini avec :
     *      - upload_max_filesize = 100M (au lieu de 2M)
            - post_max_size = 100M (au lieu de 8M)
	 * @access public
	 * @return void
	 */
	public function import() {


		if (!empty($this->request->data)) {
			if (!empty($this->request->data['Import']['file'])) {
				$tmpName = $this->request->data['Import']['file']['tmp_name'];
				$filename = TMP . $this->request->data['Import']['file']['name'];
				if (move_uploaded_file($tmpName, $filename)) {
//					if ($this->Ban->import($filename, $this->request->data['Import']['departement_id'])) {
					if ($this->Ban->importBanCsv($filename, $this->request->data['Import']['departement_id'])) {
						$this->Session->setFlash(__d('default', 'save.ok'), 'growl', array('type' => 'default'));
					} else {
						$this->Session->setFlash(__d('default', 'save.error'), 'growl', array('type' => 'error'));
					}
				}

				if (is_file($filename)) {
					unlink($filename);
				}
				$this->redirect(array('controller' => 'bans', 'action' => 'index'));
			}
		} else {
			$this->set('bans', $this->Ban->find(
                    'list',
                    array(
                        'conditions' => array('Ban.active' => 1)
                    )
                )
            );
		}
		$this->set('departements', $this->Ban->listeDepartements());
	}


    /**
	 * Suppression d'une base adresse
	 *
	 * @logical-group BAN
	 * @user-profil Admin
	 *
	 * @access public
	 * @param integer $id identifiant de la BAN
	 * @return void
	 */
	public function delete($id = null) {
		$this->Jsonmsg->init(__d('default', 'delete.error'));
		$this->Ban->begin();
		if ($this->Ban->delete($id)) {
			$this->Ban->commit();
			$this->Jsonmsg->valid(__d('default', 'delete.ok'));
		} else {
			$this->Ban->rollback();
		}
		$this->Jsonmsg->send();
	}

}
