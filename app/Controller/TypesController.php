<?php

/**
 * Types
 *
 * Types controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class TypesController extends AppController {

	/**
	 * Controller name
	 *
	 * @access public
	 * @var string
	 */
	public $name = 'Types';

	/**
	 * Ajout d'un type
	 *
	 * @logical-group Types
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function add() {
		if (!empty($this->request->data)) {
			$json = array(
				'message' => __d('default', 'save.error'),
				'success' => false
			);
			$type = $this->request->data;
			$this->Type->begin();
			$this->Type->create($type);
			if ($this->Type->save()) {
				$json['success'] = true;
			}
			if ($json['success']) {
				$this->Type->commit();
				$json['message'] = __d('default', 'save.ok');
			} else {
				$this->Type->rollback();
			}
			$this->Jsonmsg->sendJsonResponse($json);
//            $this->redirect(array('controller' => 'soustypes', 'action' => 'index'));

		}
	}

	/**
	 * Edition d'un type
	 *
	 * @logical-group Types
	 * @user-profile Admin
	 *
	 * @access public
	 * @param integer $id identifiant du type
	 * @throws NotFoundException
	 * @return void
	 */
	public function edit($id) {
		if (!empty($this->request->data)) {
			$json = array(
				'message' => __d('default', 'save.error'),
				'success' => false
			);
			$this->Type->create();
			if ($this->Type->save($this->request->data)) {
				$json['success'] = true;
				$json['message'] = __d('default', 'save.ok');
			}
			$this->Jsonmsg->sendJsonResponse($json);
		} else {
			$this->request->data = $this->Type->find('first', array('conditions' => array('Type.id' => $id)));
			if (empty($this->request->data)) {
				throw new NotFoundException();
			}
		}
	}

	/**
	 * Suppression d'un type
	 *
	 * @logical-group Types
	 * @user-profile Admin
	 *
	 * @access public
	 * @param integer $id identifiant du type
	 * @throws NotFoundException
	 * @return void
	 */
	public function delete($id = null) {
		$type = $this->Type->find('first', array(
                    'conditions' => array('Type.id' => $id),
                    'contain' => array(
                        'Soustype'
                    )
                ));

		if (empty($type)) {
			throw new NotFoundException();
		}

		$isUsed = array();
		if(count($type['Soustype'])>0){
                    $isUsed[] = true;

                }
                $stypes = $this->Type->Soustype->find('all', array('conditions' => array('Soustype.type_id' => $id)));
		foreach ($stypes as $stype) {
			$used = false;
			$ref = $this->Type->Soustype->Courrier->find('first', array('conditions' => array('Courrier.soustype_id' => $stype['Soustype']['id'])));
			if (!empty($ref)) {
				$used = true;
			}
			$isUsed[] = $used;
		}
		$canDelete = false;
		if (!in_array(true, $isUsed, true)) {
			$canDelete = true;
		}
		if ($canDelete) {
			if ($this->Type->delete($id)) {
				$this->Jsonmsg->valid(__d('default', 'delete.ok'));
			}
		} else {
                        if(count($type['Soustype'])>0){
                            $this->Jsonmsg->init(__d('type', 'Type.delete.notAllowed.existeSoustype'));
                        }else{
                            $this->Jsonmsg->init(__d('type', 'Type.delete.notAllowed'));
                        }
		}


                $this->Jsonmsg->send();
	}

}

?>
