<?php

/**
 * Uniterifs
 *
 * Uniterifs controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class UniterifsController extends AppController {

	/**
	 * Controller name
	 *
	 * @access public
	 * @var string
	 */
	public $name = 'Uniterifs';

    public $uses = array('Uniterif');
    /**
	 * Gestion des emails interface graphique)
	 *
	 * @logical-group Scanemails
	 * @user-profil Admin
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
                    '<a href="/environnement/index/0/admin">'.__d('menu', 'Administration', true).'</a>',
                    __d('menu', 'Uniterifs', true)
                ));
	}

	/**
	 * Ajout d'un email
	 *
	 * @logical-group Uniterifs
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function add() {
		if (!empty($this->request->data)) {
			$json = array(
				'message' => __d('default', 'save.error'),
				'success' => false
			);

			$uniterif = $this->request->data;


			$this->Uniterif->begin();
			$this->Uniterif->create($uniterif);

			if ($this->Uniterif->save()) {
				$json['success'] = true;
			}
			if ($json['success']) {
				$this->Uniterif->commit();
				$json['message'] = __d('default', 'save.ok');
			} else {
				$this->Uniterif->rollback();
			}
			$this->Jsonmsg->sendJsonResponse($json);
		}

	}

	/**
	 * Edition d'un type
	 *
	 * @logical-group Uniterifs
	 * @user-profile Admin
	 *
	 * @access public
	 * @param integer $id identifiant du type
	 * @throws NotFoundException
	 * @return void
	 */
	public function edit($id) {
		if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
			$this->Uniterif->create();
            $uniterif = $this->request->data;

			if ($this->Uniterif->save($uniterif)) {
                $this->Jsonmsg->valid();
			}
			$this->Jsonmsg->send();
		} else {
			$this->request->data = $this->Uniterif->find(
                'first',
                array(
                    'conditions' => array('Uniterif.id' => $id),
                    'contain' => false
                )
            );

			if (empty($this->request->data)) {
				throw new NotFoundException();
			}

		}
	}

	/**
	 * Suppression d'un email
	 *
	 * @logical-group Uniterifs
	 * @user-profil Admin
	 *
	 * @access public
	 * @param integer $id identifiant du scanemail
	 * @return void
	 */
	public function delete($id = null) {
		$this->Jsonmsg->init(__d('default', 'delete.error'));
		$this->Uniterif->begin();
		if ($this->Uniterif->delete($id)) {
			$this->Uniterif->commit();
			$this->Jsonmsg->valid(__d('default', 'delete.ok'));
		} else {
			$this->Uniterif->rollback();
		}
		$this->Jsonmsg->send();
	}


    /**
	 * Récupération de la liste des mails à scruter (ajax)
	 *
	 * @logical-group Uniterifs
	 * @user-profil Admin
	 *
	 * @access public
	 * @return void
	 */
	public function getUniterifs() {
		$uniterifs_tmp = $this->Uniterif->find(
            "all",
            array(
                'order' => 'Uniterif.name'
            )
        );
        $uniterifs = array();
        foreach ($uniterifs_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $uniterifs[] = $item;
        }
        $this->set(compact('uniterifs') );
	}
}

?>
