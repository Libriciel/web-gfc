<?php

/**
 * Activites
 *
 * Activites controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class ActivitesController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Activites';
    public $uses = array('Activite');

    /**
     * Gestion des emails interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'Activites', true)
        ));
    }

    /**
     * Ajout d'un email
     *
     * @logical-group Activites
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $activite = $this->request->data;


            $this->Activite->begin();
            $this->Activite->create($activite);

            if ($this->Activite->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Activite->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Activite->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }
    }

    /**
     * Edition d'un type
     *
     * @logical-group Activites
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Activite->create();
            $activite = $this->request->data;

            if ($this->Activite->save($activite)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Activite->find(
                    'first', array(
                'conditions' => array('Activite.id' => $id),
                'contain' => false
                    )
            );

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
        $sousactivites_tmp = $this->Activite->Sousactivite->find('all', array('conditions' => array('Sousactivite.activite_id' => $id), 'recursive' => -1));
        $sousactivites = array();
        foreach ($sousactivites_tmp as $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $sousactivites[] = $item;
        }
        $this->set('sousactivites', $sousactivites);
        $this->set('activiteId', $id);
    }

    /**
     * Suppression d'un email
     *
     * @logical-group Activites
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Activite->begin();
        if ($this->Activite->delete($id)) {
            $this->Activite->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Activite->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des mails à scruter (ajax)
     *
     * @logical-group Activites
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getActivites() {
        $activites_tmp = $this->Activite->find(
                "all", array(
            'order' => 'Activite.name'
                )
        );
        $activites = array();
        foreach ($activites_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            foreach ($item['Sousactivite'] as $s => $sousactivite) {
                $item['Activite']['sousactivite'][] = $sousactivite['name'];
                sort($item['Activite']['sousactivite']);
            }
            $activites[] = $item;
        }
        $this->set(compact('activites'));
    }

}

?>
