<?php

App::uses('ConnectionManager', 'Cake.Model');

/**
 * App Controller
 *
 * App controller class override.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class AppController extends Controller {

    /**
     * Controller components
     *
     * @var array
     * @access public
     */
    public $components = array(
        'DebugKit.Toolbar',
        'Jsonmsg.Jsonmsg',
        'Ajaxformvalid',
        'DataAcl.SessionDataAcl',
        'DataAcl.AclExtended',
        'RequestHandler',
        'Session',
        'SessionTools',
        'DataAuthorized',
        'Auth' => array(
            'loginAction' => array('controller' => 'users', 'action' => 'login'),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
            'loginRedirect' => array('controller' => 'environnement', 'action' => 'index'),
            'authenticate' => array(
                //Paramètre à passer pour tous les objets d'authentifications
                'all' => array(
                    'userModel' => 'User',
                    'scope' => array('User.active' => true),
                ),
                'AuthManager.Cas' => array(
                    'loginAction' => array('controller' => 'users', 'action' => 'casLogin'),
                    'logoutRedirect' => array('controller' => 'users', 'action' => 'casLogout'),
                    'except' => array('admin')),
				'Form' => [
					'passwordHasher' => [
						'className' => 'Simple',
						'hashType' => 'sha256'
					]
				],
                'AuthManager.Ldap' => array(
                    'except' => array('superadmin', 'admin')
                )
            ),
            'authorize' => array('Controller'),
        )
    );

    /**
     * Controller helpers
     *
     * @var array
     * @access public
     */
    public $helpers = array('Html', 'Html2', 'Form', 'Session', 'Js', 'String', 'Liste', 'TreeJs', 'Droits', 'Bannette', 'Locale');

    /**
     *
     * Initialise la connexion afin d'utiliser la base de données de la collectivité choisie
     *
     * @access protected
     * @param string $conn
     * @return void
     */
    protected function _setConn($conn) {
        if (!empty($conn)) {
            Configure::write('conn', $conn);
            if ($conn != 'default') {
                Configure::write('Acl.database', $conn);
                Configure::write('DataAcl.database', $conn);
                $this->loadModel('Aro');
                $this->Aro->setDataSource($conn);
                $this->loadModel('Aco');
                $this->Aco->setDataSource($conn);
                $this->loadModel('Permission');
                $this->Permission->setDataSource($conn);
                $this->loadModel('Daro');
                $this->Daro->setDataSource($conn);
                $this->loadModel('Daco');
                $this->Daco->setDataSource($conn);
            }
        }
    }

    /**
     * Contruction des classes de l'application
     *
     * @access public
     * @return void
     */
    public function constructClasses() {
        parent::constructClasses();
        $conn = CakeSession::read('Auth.User.connName');
        $this->_setConn($conn);
    }

    /**
     *
     * Fonction de rappel beforeFilter
     *
     * @description paramétrage du composant Auth
     *
     * @access public
     * @return void
     */
    public function beforeFilter() {
        //Configure AuthComponent
//debug(Configure::read('AuthManager.Authentification.type'));
        $casActif = Configure::read('AuthManager.Authentification.type') === 'CAS' && Configure::read('AuthManager.Authentification.use');
//debug($casActif);
//        if($casActif) {
//            $this->Auth->loginAction = array('controller' => 'users', 'action' => 'chooseCollForCas');
//            $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'chooseCollForCas');
//            $this->Auth->loginRedirect = array('controller' => 'environnement', 'action' => 'index');
//        }
//        else {
        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
        $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
        $this->Auth->loginRedirect = array('controller' => 'environnement', 'action' => 'index');
//        }

        $this->Auth->allowedActions = array(
            'controller' => 'users',
            'action' => 'chooseCollForCas'
        );

        $isAllo = ( strpos(substr($_SERVER['REQUEST_URI'], strlen($this->request->base)), '/api/rest/allo/') === 0 );
        if ($isAllo) {
            $this->Auth->allowedActions = array(
                'controller' => 'allos',
                'action' => 'version'
            );
        }

        $isCheck = ( strpos(substr($_SERVER['REQUEST_URI'], strlen($this->request->base)), '/checks') === 0 );
        if ($isCheck) {
            $this->Auth->allowedActions = array(
                'controller' => 'checks',
                'action' => 'index'
            );
        }


        if (!$isCheck && !$isAllo && ($this->Auth->User('id') && Configure::read('conn') != 'default')) {
//            if( $this->Session->read('Auth.User.Env.actual.name') != 'disp' ) {
            $this->Auth->authorize = array(
                'Actions' => array('actionPath' => 'controllers')
            );


            /* $this->Auth->allowedActions = array(
              'controller' => 'environnement',
              'action' => 'index'
              ); */
//            }
        }

        //Remise à zéro de l'indication permettant de déterminer si l'on vient de cliquer sur la visualisation
        //d'une tache via les notifications de taches
        if ($this->Session->check('Auth.User.Env.fromTask') && $this->Session->read('Auth.User.Env.fromTask')) {
            Configure::write('fromTask', true);
            $this->Session->delete('Auth.User.Env.fromTask');
        }



        if (Configure::read('conn') == 'default') {
            $this->Auth->authorize = array(
                'Actions' => array('*')
            );
        }
        // Fichier de configuration pour les langues
        /* $this->Session->write('Config.language', 'fre');
          if ($this->Session->check('Config.language')) {
          Configure::write('Config.language', $this->Session->read('Config.language'));
          } */
    }

    /**
     *
     * Fonction de rappel beforeRender
     *
     * @description détermination du layout à utiliser
     *
     * @access public
     * @return void
     */
    public function beforeRender() {
        if ($this->Auth->User('id') && Configure::read('conn') == 'default') {
            $this->layout = 'superadmin';
        }
        if ($this->RequestHandler->isAjax()) {
            $this->layout = '';
        }

        $this->getAddParams();
    }

    /**
     *
     * @access protected
     * @param array $data
     * @param string $key
     * @return array
     */
    protected function _selectedArray($data, $key = 'id') {
        if (!is_array($data)) {
            $model = $data;
            if (!empty($this->request->data[$model][$model])) {
                return $this->request->data[$model][$model];
            }
            if (!empty($this->request->data[$model])) {
                $data = $this->request->data[$model];
            }
        }
        $array = array();
        if (!empty($data)) {
            foreach ($data as $var) {
                $array[$var[$key]] = $var[$key];
            }
        }
        return $array;
    }

    /**
     * Vérification des droits d'accès pour un utilisateur (tout les bureaux)
     *
     * @logical-group Gestion des habilitations
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @param mixed $aro
     * @param mixed $aco
     * @param string $action
     * @return boolean
     */
    public function userAclCheck($aro, $aco, $action = '*') {
        $this->loadModel('User');
        $mainDesktop = $this->User->field('desktop_id', array('User.id' => $aro['foreign_key']));
        $desktops = $this->User->Desktop->DesktopsUser->find('all', array('conditions' => array('DesktopsUser.user_id' => $aro['foreign_key']), 'recursive' => -1));
        $checks = array();
        $checks[$mainDesktop] = $this->Acl->check(array('model' => 'Desktop', 'foreign_key' => $mainDesktop), $aco, $action);
        foreach ($desktops as $desktop) {
            $checks[$desktop['DesktopsUser']['desktop_id']] = $this->Acl->check(array('model' => 'Desktop', 'foreign_key' => $desktop['DesktopsUser']['desktop_id']), $aco, $action);
        }
        return in_array(true, $checks, true);
    }

    /**
     * Récupère les rôles de l'utilisateur en session
     *
     * @access private
     * @param integer $gid
     * @return array
     */
    private function _getSessionDesktops($gid) {
        $sessionDesktops = array();
        if ($this->Session->read('Auth.User.Desktop.Profil.id') == $gid) {
            $sessionDesktops[$this->Session->read('Auth.User.Desktop.id')] = $this->Session->read('Auth.User.Desktop.name'); // Récupération des services
        }
        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        for ($i = 0; $i < count($secondaryDesktops); $i++) {
            if ($secondaryDesktops[$i]['Profil']['id'] == $gid) {
                $sessionDesktops[$secondaryDesktops[$i]['id']] = $secondaryDesktops[$i]['name'];
            }
        }

        return $sessionDesktops;
    }

    /**
     * Récupération des services liés au bureau "Initiateur" de l'utilisateur connecté à partir de la session PHP
     *
     * @logical-group Environnement
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return array
     */
    public function getSessionInitServices() {
        return $this->_getSessionInitServices(INIT_GID);
    }

    /**
     * @logical-group Environnement
     * @user-profile User
     * @user-profile Initiateur
     *
     * @access public
     * @params integer $id du profil initiateur
     * @return array
     *
     */
    private function _getSessionInitServices($initGID) {
        $sessionServicesByDesktop = array();

        if ($this->Session->read('Auth.User.Desktop.Profil.id') == $initGID) {
            // Liste des service spour le bureau principal
            $sessionServices = $this->Session->read('Auth.User.Desktop.Services');
            foreach ($sessionServices as $i => $nameService) {
                $sessionServicesByDesktop[$this->Session->read('Auth.User.Desktop.id')][$nameService['id']] = $sessionServices[$i]['name'];
            }
        }

        // Liste des services pour les bureaux secondaires
        $secondaryServicesByDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        for ($i = 0; $i < count($secondaryServicesByDesktops); $i++) {
            $secondaryDesktopId = $secondaryServicesByDesktops[$i]['id'];
            if ($secondaryServicesByDesktops[$i]['Profil']['id'] == $initGID) {
                $services = $secondaryServicesByDesktops[$i]['Services'];
                for ($j = 0; $j < count($services); $j++) {
                    $sessionServicesByDesktop[$secondaryDesktopId][$services[$j]['id']] = $services[$j]['name'];
                }
            }
        }

        return $sessionServicesByDesktop;
    }

    /**
     * Récupération des services liés au bureau "Personnalisé" de l'utilisateur connecté à partir de la session PHP
     *
     * @logical-group Environnement
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return array
     */
    public function getSessionAppDefinedServices() {
        return $this->_getSessionAppDefinedServices();
    }

    /**
     * @logical-group Environnement
     * @user-profile User
     * @user-profile Initiateur
     *
     * @access public
     * @params integer $id du profil créé par l'administrateur
     * @return array
     *
     */
    private function _getSessionAppDefinedServices() {
        $sessionServicesByDesktop = array();

        if ($this->Session->read('Auth.User.Desktop.Profil.id') > LAST_GID) {
            // Liste des service spour le bureau principal
            $sessionServices = $this->Session->read('Auth.User.Desktop.Services');
            foreach ($sessionServices as $i => $nameService) {
                $sessionServicesByDesktop[$this->Session->read('Auth.User.Desktop.id')][$nameService['id']] = $sessionServices[$i]['name'];
            }
        }

        // Liste des services pour les bureaux secondaires
        $secondaryServicesByDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        for ($i = 0; $i < count($secondaryServicesByDesktops); $i++) {
            $secondaryDesktopId = $secondaryServicesByDesktops[$i]['id'];
//            if ($secondaryServicesByDesktops[$i]['Profil']['id'] > LAST_GID) {
                $services = $secondaryServicesByDesktops[$i]['Services'];
                for ($j = 0; $j < count($services); $j++) {
                    $sessionServicesByDesktop[$secondaryDesktopId][$services[$j]['id']] = $services[$j]['name'];
                }
//            }
        }

        return $sessionServicesByDesktop;
    }

    /**
     * Récupération des bureaux ayant un profil personnalisé de l'utilisateur connecté à partir de la session PHP
     *
     * @logical-group Environnement
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return array
     */
    public function getSessionAppDefinedDesktops() {
        $sessionDesktops = array();
        if ($this->Session->read('Auth.User.Desktop.Profil.id') > LAST_GID) {
            $sessionDesktops[$this->Session->read('Auth.User.Desktop.id')] = $this->Session->read('Auth.User.Desktop.name');
        }
        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        for ($i = 0; $i < count($secondaryDesktops); $i++) {
//            if ($secondaryDesktops[$i]['Profil']['id'] > LAST_GID) {
            if ($secondaryDesktops[$i]['Profil']['id'] == 3 ) {
                $sessionDesktops[$secondaryDesktops[$i]['id']] = $secondaryDesktops[$i]['name'];
            }
        }

        return $sessionDesktops;
    }

    /**
     * Récupération des bureaux "Initiateur" de l'utilisateur connecté à partir de la session PHP
     *
     * @logical-group Environnement
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return array
     */
    public function getSessionInitDesktops() {
        return $this->_getSessionDesktops(INIT_GID);
    }

    /**
     * Récupération des bureaux "Valideur" de l'utilisateur connecté à partir de la session PHP
     *
     * @logical-group Environnement
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return array
     */
    public function getSessionValDesktops() {
        return $this->_getSessionDesktops(VAL_GID);
    }

    /**
     * Récupération des bureaux "Valideur Editeur" de l'utilisateur connecté à partir de la session PHP
     *
     * @logical-group Environnement
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return array
     */
    public function getSessionValeditDesktops() {
        return $this->_getSessionDesktops(VALEDIT_GID);
    }

    /**
     *
     * Récupération des bureaux "Aiguilleur (Dispatcher)" de l'utilisateur connecté à partir de la session PHP
     *
     * @logical-group Environnement
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return array
     */
    public function getSessionDispDesktops() {
        return $this->_getSessionDesktops(DISP_GID);
    }

    /**
     * Récupération des bureaux "documentaliste (Archiviste)" de l'utilisateur connecté à partir de la session PHP
     *
     *
     * @logical-group Environnement
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return array
     */
    public function getSessionArchDesktops() {
        return $this->_getSessionDesktops(ARCH_GID);
    }

    /**
     * Récupération de tous les bureaux de l'utilisateur connecté à partir de la session PHP
     *
     * @logical-group Environnement
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return array
     */
    public function getSessionAllDesktops() {
        $sessionDesktops = array();
        $sessionDesktops[$this->Session->read('Auth.User.Desktop.id')] = $this->Session->read('Auth.User.Desktop.name');
        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        for ($i = 0; $i < count($secondaryDesktops); $i++) {
            $sessionDesktops[$secondaryDesktops[$i]['id']] = $secondaryDesktops[$i]['name'];
        }
        return $sessionDesktops;
    }

    /**
     * Récupération du nom de l'environnement
     *
     * @logical-group Environnement
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @param integer $groupId
     * @return string
     */
    public function getEnv($groupId) {
        $return = '';
        if ($groupId == SUPERADMIN_GID) {
            $return = 'superadmin';
        } else if ($groupId == ADMIN_GID) {
            $return = 'admin';
        } else if ($groupId == INIT_GID) {
            $return = 'init';
        } else if ($groupId == VAL_GID) {
            $return = 'val';
        } else if ($groupId == DISP_GID) {
            $return = 'disp';
        } else if ($groupId == VALEDIT_GID) {
            $return = 'valedit';
        } else if ($groupId == ARCH_GID) {
            $return = 'arch';
        } else {
            $return = 'appdefined';
        }
        return $return;
    }

    /**
     * Récupère les paramètres relatifs à la création de flux pour la construction du menu
     *
     */
    public function getAddParams() {
//debug($this->layout);
//echo '<pre>';var_dump( $this->Session->read('Auth.User.id') );
        if ($this->layout != '' && $this->layout != 'superadmin' && $this->layout != 'login' && $this->layout != 'choose_coll_for_cas' && $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/add')) {
            $initDesktops = $this->getSessionInitDesktops();
            $initServices = $this->getSessionInitServices();
            $appDefinedDesktops = $this->getSessionAppDefinedDesktops();
            $appdefinedServices = $this->getSessionAppDefinedServices();
            if (!empty($appDefinedDesktops)) {
                $initDesktops = (array) $initDesktops + (array) $appDefinedDesktops;
            }
            if (!empty($appdefinedServices)) {
                $initServices = (array) $initServices + (array) $appdefinedServices;
//                foreach($initServices as $s => $service_id ) {
//                    $initServices[$s] = array_unique($service_id);
//                }
            }

            // Ajout permettant de masquer l'action de séleciton du Profil et service si 1 seul Profil et 1 seul service
            $displayForm = true;
            $desktopId = '';
            $serviceId = '';
            if( count( $initDesktops ) == 1 ) {
                $desktop_id = array_keys($initDesktops );
                $desktopId = $desktop_id[0];
				$service_id = '';
				if( !empty($initServices) ) {
					$service_id = array_keys($initServices[$desktopId]);
				}
                if( !empty($service_id) ) {
					if (count($service_id) == 1) {
						$serviceId = $service_id[0];
						$displayForm = false;
					} else {
						$displayForm = true;
					}
				}
				else {
					$displayForm = true;
				}
            }
            $this->set('desktopId', $desktopId);
            $this->set('serviceId', $serviceId);
            $this->set('displayForm', $displayForm);
            $this->set('addservice', $initServices);
            $this->set('add', $initDesktops);
        }
        else {
            $displayForm = true;
            $this->set('displayForm', $displayForm);
        }
    }

    /**
     *
     * Ajoute le nom et la valeur d'une variable dans le log debug
     *
     * @access protected
     * @param mixed $var
     * @return void
     */
    protected function _logDebug($var) {
        $debugvar = array(
            '================================================================',
            'date' => date('Y-m-d H:i:s'),
            'trace' => Debugger::trace(),
            'var' => $var,
            'type' => gettype($var)
        );
        $this->log(var_export($debugvar, true), 'debug');
    }

    /**
     *
     * @param type $var
     */
    public function ldebug($var) {
        $this->_logDebug($var);
    }

    protected $_paginatorClass = 'Xpaginator';

    /**
     * Provides backwards compatibility access to the request object properties.
     * Also provides the params alias.
     *
     * @param string $name
     * @return void
     */
    public function __get($name) {
        switch ($name) {
            case 'paginate':
                return $this->Components->load($this->_paginatorClass)->settings;
        }

        return parent::__get($name);
    }

    /**
     * Provides backwards compatibility access for setting values to the request object.
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set($name, $value) {
        switch ($name) {
            case 'paginate':
                return $this->Components->load($this->_paginatorClass)->settings = $value;
        }

        return parent::__set($name, $value);
    }

    /**
     * Handles automatic pagination of model records.
     *
     * @param Model|string $object Model to paginate (e.g: model instance, or 'Model', or 'Model.InnerModel')
     * @param string|array $scope Conditions to use while paginating
     * @param array $whitelist List of allowed options for paging
     * @return array Model query results
     * @link http://book.cakephp.org/2.0/en/controllers.html#Controller::paginate
     * @deprecated Use PaginatorComponent instead
     */
    public function paginate($object = null, $scope = array(), $whitelist = array()) {
        return $this->Components->load($this->_paginatorClass, $this->paginate)->paginate($object, $scope, $whitelist);
    }

    /**
     * @version 4.3
     * @access public
     * @param type $user
     * @return boolean
     */
    public function isAuthorized($user = null) {

        App::uses('Component', 'Controller');
        App::uses('ComponentCollection', 'Controller');
        App::uses('CrudAuthorize', 'AuthManager.Controller/Component/Auth');

        $collection = new ComponentCollection();
        $CrudAuthorize = new CrudAuthorize($collection);
        //initialisation des mapActions pour les droits CRUD
        if (isset($this->components['Auth']['mapActions'])) {
            $CrudAuthorize->mapActions($this->components['Auth']['mapActions']);
        }

        // Par défaut n'autorise pas
        return $CrudAuthorize->authorize(array('id' => $this->Auth->user('id')), $this->request);
//        return true;
    }

}

?>
