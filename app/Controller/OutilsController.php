<?php

/**
 * Outils
 *
 * Outils controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class OutilsController extends AppController {

	/**
	 * Controller name
	 *
	 * @access public
	 * @var string
	 */
	public $name = 'Outils';

	public function beforeFilter() {
		$this->Auth->allow(array('index', 'indexParapheur', 'inject', 'searchParapheur', 'search', 'searchFluxnonclos', 'fluxNonClos', 'passageenlulot', 'searchFluxEncoursBannetteAInserer', 'detachenlotbannetteainserer', 'deleteFluxClos', 'searchFluxClos', 'fluxClosASupprimer', 'deleteenlot', 'deleteEveryThing',  'searchFluxClosErreur',  'fluxClosEnErreur', ));
		parent::beforeFilter();
	}

	/**
	 * Controller helpers
	 *
	 * @var array
	 * @access public
	 */
	public $helpers = array('Csv', 'Paginator');

	/**
	 * Controller uses
	 *
	 * @var array
	 * @access public
	 */
	public $uses = array('Courrier');

	/**
	 * Gestion des outils disponibles pour l'administrateur)
	 *
	 * @logical-group Outils
	 * @user-profil Admin
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
			'<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
			__d('menu', 'Outils', true)
		));

		$this->loadModel('Connecteur');
		$hasParapheur = false;
		$hasParapheurActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true
				),
				'contain' => false
			)
		);
		if( !empty( $hasParapheurActif ) && $hasParapheurActif['Connecteur']['use_signature'] ) {
			$hasParapheur = true;
		}
		$this->set('hasParapheur', $hasParapheur );

		$this->loadModel('Courrier');
		$nbCourriers = $this->Courrier->find('count');
		$this->set('nbCourriers', $nbCourriers);
	}

	/**
	 *
	 */
	public function searchParapheur() {
		$this->autoRender = true;
	}
	/**
	 *
	 */
	public function formulaireParapheur() {
		$this->autoRender = true;
	}


	/**
	 *
	 */
	public function result() {
		$reference = '';

		$nbOfBancontenus = 0;
		$success = false;
		$retard = false;
		$contactInfos = '';
		$displayResult = true;
		$courrier = [];
		$courriers = [];

		$this->Courrier = ClassRegistry::init('Courrier');
		if( !empty($this->request->data['Courrier']['reference'])) {
			$reference = $this->request->data['Courrier']['reference'];
			$courrier = $this->Courrier->find(
				'first',
				array(
					'conditions' => array(
						'Courrier.reference' => $reference,
					) ,
					'contain' => array(
						'Organisme',
						'Contact',
						'Bancontenu' => array(
							'order' => array('Bancontenu.created ASC')
						)
					),
					'recursive' => -1
				)
			);

			if( !empty($courrier) ) {

				$infosCircuits = $this->Courrier->getCircuitInfos($courrier['Courrier']['id']);
				if( !empty($infosCircuits['visas']) ) {
					foreach ($infosCircuits['visas'] as $v => $visa) {
						if( $visa['Visa']['trigger_id'] == '-1' ) {
							$success = true;
						}
					}
				}
				$this->set('infosCircuits', $infosCircuits);

				if( $success ) {
					$courrier['Courrier']['objet'] = replace_accents($courrier['Courrier']['objet']); // Corrige le souci pour CA Albi
					$courrier['Courrier']['retard'] = !$this->Courrier->isInTime($courrier['Courrier']['id']);
					foreach ($courrier['Bancontenu'] as $b => $bancontenu) {
						$courrier['Bancontenu'][$b]['username'] = $this->Courrier->Bancontenu->Desktop->User->userIdByDesktop($bancontenu['desktop_id'], true);
						$courrier['Bancontenu'][$b]['unread'] = (!$bancontenu['read'] && $bancontenu['etat'] == 1);
						$courrier['Bancontenu'][$b]['clos'] = false;
						if ($bancontenu['etat'] === 2) {
							$courrier['Bancontenu'][$b]['clos'] = true;
							$courrier = array();
						}
					}

					if (!empty($courrier['Organisme']['name']) && !empty($courrier['Contact']['name'])) {
						if ($courrier['Organisme']['id'] == Configure::read('Sansorganisme.id')) {
							$contactInfos = $courrier['Contact']["name"];
						} else {
							$contactInfos = $courrier['Organisme']['name'] . ' / ' . $courrier['Contact']["name"];
						}
					}
					if (!empty($courrier['Organisme']['name']) && empty($courrier['Contact']['name'])) {
						$contactInfos = $courrier['Organisme']["name"];
					}
					if (empty($courrier['Organisme']['name']) && !empty($courrier['Contact']['name'])) {
						$contactInfos = $courrier['Contact']["name"];
					}

					if (isset($courrier['Courrier']['retard']) && $courrier['Courrier']['retard']) {
						$retard = true;
					}

					if( !empty( $courrier ) ) {
						$nbOfBancontenus = count($courrier['Bancontenu']);
					}
				}
				else {
					$displayResult = false;
				}
			}
			$this->set('courrier', $courrier);
		}
		else {
			$courriers_tmp = $this->Courrier->find(
				'all',
				array(
					'contain' => array(
						'Organisme',
						'Contact',
						'Bancontenu' => array(
							'conditions' => array(
								'Bancontenu.desktop_id' => '-1',
								'Bancontenu.etat' => 1
							),
							'order' => array('Bancontenu.created ASC')
						)
					),
					'recursive' => -1
				)
			);
			foreach( $courriers_tmp as $c =>  $courrier ) {
				if( !empty( $courrier['Bancontenu'] ) ) {
					$courriers[$c] = $courrier;
					$courriers[$c]['retard'] = false;

					if (!empty($courrier['Organisme']['name']) && !empty($courrier['Contact']['name'])) {
						if ($courrier['Organisme']['id'] == Configure::read('Sansorganisme.id')) {
							$courriers[$c]['contactInfos'] = $courrier['Contact']["name"];
						} else {
							$courriers[$c]['contactInfos'] = $courrier['Organisme']['name'] . ' / ' . $courrier['Contact']["name"];
						}
					}
					if (!empty($courrier['Organisme']['name']) && empty($courrier['Contact']['name'])) {
						$courriers[$c]['contactInfos'] = $courrier['Organisme']["name"];
					}
					if (empty($courrier['Organisme']['name']) && !empty($courrier['Contact']['name'])) {
						$courriers[$c]['contactInfos'] = $courrier['Contact']["name"];
					}

					if (isset($courrier['Courrier']['retard']) && $courrier['Courrier']['retard']) {
						$courriers[$c]['retard'] = true;
					}
					$courriers[$c]['inject'] = true;
				}
			}
		}
		$this->set( 'courriers', $courriers );
		$this->set( 'nbOfBancontenus', $nbOfBancontenus );
		$this->set( 'contactInfos', $contactInfos );
		$this->set( 'retard', $retard );
		$this->set( 'displayResult', $displayResult );
		$bannettes = $this->Courrier->Bancontenu->Bannette->find('list');
		$this->set( 'bannettes', $bannettes );


		$etat = array(
			'-1' => 'Flux refusé',
			'0' => 'Flux traité',
			'1' => 'Flux en cours de traitement',
			'2' => 'Flux clos',
			'3' => 'Flux en copie détaché',
		);
		$this->set( 'etat', $etat );
	}

	/**
	 * Fonction permettant de réinjecter un flux dans le i-Parapheur
	 * @param $numreference
	 */
	public function inject($numreference) {
		$this->autoRender = false;

		$this->Courrier = ClassRegistry::init('Courrier');

		$success = true;
		// En premier lieu on remet le flux a l'étape et l'état précédents
		$sql = 'select * from bancontenus where courrier_id in (select id from courriers where reference=\''.$numreference.'\');';
		$sql2 = 'delete from bancontenus where courrier_id in (select id from courriers where reference=\''.$numreference.'\') and desktop_id= \'-1\';';
		$sql3 = 'update wkf_traitements set numero_traitement = (numero_traitement - 1) where target_id in (select id from courriers where reference=\''.$numreference.'\');';
		$sql4 = 'update wkf_visas set action=\'RI\' where traitement_id in ( select id from wkf_traitements where target_id in (select id from courriers where reference=\''.$numreference.'\')) and numero_traitement = (select numero_traitement - 1 from wkf_visas where trigger_id = \'-1\' and traitement_id in (select id from wkf_traitements where target_id in (select id from courriers where reference=\''.$numreference.'\') ) );';
		$sql5 = 'update bancontenus set etat= 1 where courrier_id in (select id from courriers where reference=\''.$numreference.'\') and desktop_id in (select desktop_id from desktops_desktopsmanagers where desktopmanager_id in (select trigger_id from wkf_visas where traitement_id in (select id from wkf_traitements where target_id in (select id from courriers where reference=\''.$numreference.'\')) and numero_traitement = (select numero_traitement - 1 from wkf_visas where trigger_id = \'-1\' and traitement_id in (select id from wkf_traitements where target_id in (select id from courriers where reference=\''.$numreference.'\') ) ))) ;';
		$this->Courrier->query('BEGIN;');
		$this->Courrier->query( $sql );
		$this->Courrier->query( $sql2 );
		$this->Courrier->query( $sql3 );
		$this->Courrier->query( $sql4 );
		$this->Courrier->query( $sql5 );
		$this->Courrier->query('COMMIT;');

		// Ensuite, on récupère le dernier profil ayant traité le flux
		$sql7 = 'select desktop_id from bancontenus where etat=0 and courrier_id in (select id from courriers where reference=\''.$numreference.'\') order by id desc limit 1;';
		$this->Courrier->query('BEGIN;');
		$desktop_id  = $this->Courrier->query( $sql7 );
		$this->Courrier->query('COMMIT;');

		// Et enfin, on réinjecte le flux dans le IP
		$flux = $this->Courrier->find('first', array('conditions' => array('Courrier.reference' => $numreference), 'contain' => false));
		$this->Courrier->cakeflowExecuteNext($desktop_id[0][0], $flux['Courrier']['id'], 0, null, 0);

		if ($success) {
			$this->Session->setFlash(__('Le flux a été ré-adressé au i-Parapheur'), 'growl');
		} else {
			$this->Session->setFlash(__("Un problème est survenu lors du ré-adressage"), 'growl', array('type' => 'danger'));
		}
	}

	/**
	 *  Fonction permettant de récupérer les données parentes de flux réponses
	 *
	 */
	protected function _dataParent($courrierParent_id) {
		$this->Bancontenu = ClassRegistry::init('Bancontenu');
		$courrierParent = $this->Bancontenu->Courrier->find(
			'first', array(
				'fields' => array_merge(
					$this->Bancontenu->Courrier->fields(), $this->Bancontenu->Courrier->Organisme->fields(), $this->Bancontenu->Courrier->Contact->fields(), $this->Bancontenu->Courrier->Soustype->fields(), $this->Bancontenu->Courrier->Soustype->Type->fields(), $this->Bancontenu->Courrier->Desktop->fields()
				),
				'conditions' => array(
					'Courrier.id' => $courrierParent_id
				),
				'joins' => array(
					$this->Bancontenu->Courrier->join('Organisme'),
					$this->Bancontenu->Courrier->join('Contact'),
					$this->Bancontenu->Courrier->join('Soustype'),
					$this->Bancontenu->Courrier->Soustype->join('Type'),
					$this->Bancontenu->Courrier->join('Desktop')
				),
				'recursive' => -1
			)
		);
		return $courrierParent;
	}


	/**
	 * Fonction permettant de lister les flux non clos liés à des agents non actifs
	 * @param $desktop_id
	 */
	public function fluxNonClos($desktop_id = null) {
		$Courrier = ClassRegistry::init('Courrier');
		$User = ClassRegistry::init('User');
		$courriers = array();
		$conditions = array();

		$hasReference = !empty( $this->request->data['Courrier']['reference'] );
		$hasUserId = !empty( $this->request->data['Bancontenu']['user_id'] );
		if( $hasReference || $hasUserId ) {
			$fields = array_merge(
				$Courrier->fields(),
				$Courrier->Organisme->fields(),
				$Courrier->Contact->fields(),
				$Courrier->Soustype->fields(),
				$Courrier->Soustype->Type->fields(),
				$Courrier->Bancontenu->fields(),
				$Courrier->Desktop->fields(),
				$Courrier->Origineflux->fields()
			);

			$joins = array(
				$Courrier->join('Contact', array('type' => 'LEFT OUTER')),
				$Courrier->join('Organisme', array('type' => 'LEFT OUTER')),
				$Courrier->join('Soustype', array('type' => 'LEFT OUTER')),
				$Courrier->Soustype->join('Type', array('type' => 'LEFT OUTER')),
				$Courrier->join('Desktop', array('type' => 'LEFT OUTER')),
				$Courrier->join('Origineflux', array('type' => 'LEFT OUTER')),
				$Courrier->join('Bancontenu', array('type' => 'LEFT OUTER'))
			);
			$contain = false;
			$order = array(
				'Courrier.date',
				'Courrier.datereception',
				'Courrier.name'
			);


			$conditions[] = array(
				'Bancontenu.etat' => 1,
				'Bancontenu.bannette_id <>' => 7
			);
			if (!empty($this->request->data['Bancontenu']['user_id'])) {
				$desktops = $User->getDesktops($this->request->data['Bancontenu']['user_id']);

				$conditions[] = array(
					'Bancontenu.desktop_id' => $desktops
				);
			}
			if (!empty($this->request->data['Courrier']['reference'])) {
				$conditions[] = array(
					'Courrier.reference' => $this->request->data['Courrier']['reference']
				);
			}

			$query = array(
				'fields' => $fields,
				'conditions' => $conditions,
				'contain' => $contain,
				'joins' => $joins,
				'recursive' => -1,
				'order' => $order
			);
			$query['fields'][] = $Courrier->getVfAgents('agentstraitants', "\n");

			$courriers = $Courrier->find('all', $query );
		}
		$nbCourriers = count($courriers);
		$this->set( 'courriers', $courriers );
		$this->set( 'nbCourriers', $nbCourriers );
	}

	/**
	 * Fonction permettant de lister les flux non clos liés à des agents non actifs
	 * @param $desktop_id
	 */
	public function searchFluxnonclos($desktop_id = null) {
		$User = ClassRegistry::init('User');
		$users = array();
		$users_tmp = $User->find(
			'all',
			array(
				'conditions' => array('User.active' => false),
				'recursive' => -1,
				'contain' => false,
				'order' => ['User.nom ASC, User.prenom ASC']
			)
		);
		foreach( $users_tmp as $user ) {
			$users[$user['User']['id']] = $user['User']['prenom'].' '.$user['User']['nom'];
		}
		$this->set('users', $users);
		$checkItem = array();
		$this->set('checkItem', $checkItem);
	}


	/**
	 * Fonction permettant à l'administrateur de passer en "lu" par lots les flux non clos encore présent chez un agent inactif
	 * @param type $fluxId
	 */
	public function passageenlulot($fluxIds = array()) {
		$this->autoRender = false;

		$Courrier = ClassRegistry::init('Courrier');
		$unlinkCourriers = $this->request->data['checkItem'];
		$desktopId = $this->request->data['checkDesktop'];

		if (!empty($this->request->data)) {

			if (!empty($unlinkCourriers)) {
				foreach ($unlinkCourriers as $fluxId) {

					$success = array();
					$Courrier->Bancontenu->recursive = -1;
					$success[] = $Courrier->Bancontenu->updateAll(
						array(
							'Bancontenu.etat' => 0
						),
						array(
							'Bancontenu.courrier_id' => $fluxId,
							'Bancontenu.etat <>' => 0,
							'Bancontenu.desktop_id' => $desktopId
						)
					);
				}

				if (!in_array(false, $success, true)) {
					$Courrier->Bancontenu->commit();
					$this->Session->setFlash(__d('courrier', 'Courriers detached', true), 'growl');
				} else {
					$Courrier->Bancontenu->rollback();
					$this->Session->setFlash(__d('courrier', 'Courriers were not detached', true), 'growl');
				}
			}
		}
	}



	/**
	 * Fonction permettant de lister les bannettes dont le flux est en cours de traitement mais toujours dans la bannette des flux à insérer
	 * @param $desktop_id
	 */
	public function searchFluxEncoursBannetteAInserer() {
		$Desktop = ClassRegistry::init('Desktop');
		$desktops = array();
		$desktops_tmp = $Desktop->find(
			'all',
			array(
				'conditions' => array(
					'Desktop.active' => true,
					'Desktop.profil_id' => INIT_GID
				),
				'recursive' => -1,
				'contain' => false
			)
		);
		foreach( $desktops_tmp as $desktop ) {
			$desktops[$desktop['Desktop']['id']] = $desktop['Desktop']['name'];
		}
		$this->set('desktops', $desktops);
		$checkItem = array();
		$this->set('checkItem', $checkItem);
	}


	/**
	 * Fonction permettant de lister les bannettes dont le flux est en cours de traitement mais toujours dans la bannette des flux à insérer
	 */
	public function fluxEncoursBannetteAInserer() {
		$Courrier = ClassRegistry::init('Courrier');
		$User = ClassRegistry::init('User');
		$courriers = array();
		$conditions = array();

		$hasReference = !empty( $this->request->data['Courrier']['reference'] );
		$hasUserId = !empty( $this->request->data['Bancontenu']['user_id'] );
//		if( $hasReference || $hasUserId ) {
		$fields = array_merge(
			$Courrier->fields(),
			$Courrier->Organisme->fields(),
			$Courrier->Contact->fields(),
			$Courrier->Soustype->fields(),
			$Courrier->Soustype->Type->fields(),
			$Courrier->Bancontenu->fields(),
			$Courrier->Desktop->fields(),
			$Courrier->Origineflux->fields()
		);

		$joins = array(
			$Courrier->join('Contact', array('type' => 'LEFT OUTER')),
			$Courrier->join('Organisme', array('type' => 'LEFT OUTER')),
			$Courrier->join('Soustype', array('type' => 'LEFT OUTER')),
			$Courrier->Soustype->join('Type', array('type' => 'LEFT OUTER')),
			$Courrier->join('Desktop', array('type' => 'LEFT OUTER')),
			$Courrier->join('Origineflux', array('type' => 'LEFT OUTER')),
			$Courrier->join('Bancontenu', array('type' => 'LEFT OUTER'))
		);
		$contain = false;
		$order = array(
			'Courrier.date',
			'Courrier.datereception',
			'Courrier.name'
		);


		$conditions[] = array(
			'Bancontenu.etat' => 1,
			'Bancontenu.bannette_id' => 1,
			'Courrier.id IN (select target_id from wkf_traitements)'
		);

		/*if (!empty($this->request->data['Bancontenu']['user_id'])) {
			$desktops = $User->getDesktops($this->request->data['Bancontenu']['user_id']);

			$conditions[] = array(
				'Bancontenu.desktop_id' => $desktops
			);
		}
		if (!empty($this->request->data['Courrier']['reference'])) {
			$conditions[] = array(
				'Courrier.reference' => $this->request->data['Courrier']['reference']
			);
		}*/

		$query = array(
			'fields' => $fields,
			'conditions' => $conditions,
			'contain' => $contain,
			'joins' => $joins,
			'recursive' => -1,
			'order' => $order
		);
		$query['fields'][] = $Courrier->getVfAgents('agentstraitants', "\n");

		$courriers = $Courrier->find('all', $query );
//		}
		$nbCourriers = count($courriers);
		$this->set( 'courriers', $courriers );
		$this->set( 'nbCourriers', $nbCourriers );
	}

	/**
	 * Fonction permettant à l'administrateur de passer en "lu" par lots les flux toujours présents dans les bannettes
	 * des flux à insérer alors que le flux est en cours de traitement
	 * @param type $fluxId
	 */
	public function detachenlotbannetteainserer($fluxIds = array()) {
		$this->autoRender = false;

		$Courrier = ClassRegistry::init('Courrier');
		$unlinkCourriers = $this->request->data['checkItem'];
		$desktopId = $this->request->data['checkDesktop'];

		if (!empty($this->request->data)) {

			if (!empty($unlinkCourriers)) {
				foreach ($unlinkCourriers as $fluxId) {

					$success = array();
					$Courrier->Bancontenu->recursive = -1;
					$success[] = $Courrier->Bancontenu->updateAll(
						array(
							'Bancontenu.etat' => 0
						),
						array(
							'Bancontenu.courrier_id' => $fluxId,
							'Bancontenu.etat' => 1,
							'Bancontenu.bannette_id' => 1
						)
					);
				}

				if (!in_array(false, $success, true)) {
					$Courrier->Bancontenu->commit();
					$this->Session->setFlash(__d('courrier', 'Courriers detached', true), 'growl');
				} else {
					$Courrier->Bancontenu->rollback();
					$this->Session->setFlash(__d('courrier', 'Courriers were not detached', true), 'growl');
				}
			}
		}
	}

	/**
	 * Fonction permettant de lister les flux non clos liés à des agents non actifs
	 * @param $desktop_id
	 */
	public function deleteFlux( $fluxId ) {

	}


	/**
	 * Fonction permettant de lister les bannettes dont le flux est en cours de traitement mais toujours dans la bannette des flux à insérer
	 * @param $desktop_id
	 */
	public function searchFluxClos() {

	}
	/**
	 * Fonction permettant de lister les bannettes dont le flux est en cours de traitement mais toujours dans la bannette des flux à insérer
	 */
	public function fluxClosASupprimer() {
		$Courrier = ClassRegistry::init('Courrier');
		$courriers_tmp = [];
		$courriers = [];

		$querydata = $Courrier->getFluxClosParErreur($this->request->data);
		if (!empty($querydata)) {
			$querydata['limit'] = 20;
			if (isset($tri) && !empty($tri)) {
				$querydata['order'] = $tri;
			} else {
				$querydata['order'] = 'Courrier.datereception DESC';
			}

			$this->paginate = array('Courrier' => $querydata);
			$courriers_tmp = $this->paginate('Courrier');
		}

		foreach ($courriers_tmp as $kflx => $flx) {

			$flx['Courrier']['objet'] = replace_accents($flx['Courrier']['objet']);
			if (strpos($flx['Courrier']['objet'], '<!DOCTYPE') !== false || strpos($flx['Courrier']['objet'], '<html') !== false) {
				$flx['Courrier']['objet'] = null;
			}
			if( is_JSON($flx['Courrier']['objet'] ) ) {
				$flx['Courrier']['objet'] = json_decode($flx['Courrier']['objet'], true);
				$objetData = array();
				if( !empty($flx['Courrier']['objet'])) {
					foreach ($flx['Courrier']['objet'] as $key => $val) {
						if (is_array($val)) {
							foreach ($val as $fields => $values) {
								$data[] = "\n" . $fields . ': ' . $values;
							}
							$objetData[] = $key . ' : ' . implode(' ', $data);
						} else {
							$objetData[] = $key . ' : ' . $val;
						}
					}
					$flx['Courrier']['objet'] = implode("\n", $objetData);
				}
			}

			$courriers[] = $flx;
		}
		$this->set('courriers', $courriers);
	}


	/**
	 * Fonction permettant à l'administrateur de passer en "lu" par lots les flux non clos encore présent chez un agent inactif
	 * @param type $fluxId
	 */
	public function deleteenlot() {
		$this->autoRender = false;
		$this->Jsonmsg->init();

		$Courrier = ClassRegistry::init('Courrier');
		$unlinkCourriers = $this->request->data['checkItem'];

		if (!empty($unlinkCourriers)) {
			$fluxIds = $unlinkCourriers;
			$deleteAll = array();
			$bancontenus = array_keys($Courrier->Bancontenu->find('list', array('conditions' => array('Bancontenu.courrier_id' => $fluxIds))));
			$Courrier->begin();

			$comments = $Courrier->getComments($fluxIds);

			if (!empty($comments)) {
				foreach ($comments['all'] as $c => $allcomment) {
					$commentId = $allcomment['Comment']['id'];
					if( !empty( $commentId ) ){
						$deleteAll[] = $Courrier->Comment->delete($commentId);
					}
				}
			}

			for ($i = 0; $i < count($bancontenus); $i++) {
				$deleteAll[] = $Courrier->Bancontenu->delete($bancontenus[$i]);
			}

			$conn = $this->Session->read('Auth.Collectivite.conn');
			$documentsPath = WORKSPACE_PATH . DS . $conn ;
			$documents = $Courrier->Document->find(
				'all',
				array(
					'fields' => array(
						'Document.id',
						'Document.name',
						'Document.path',
						'Document.courrier_id',
						'Courrier.reference'
					),
					'conditions' => array(
						'Document.courrier_id' => $unlinkCourriers
					),
					'contain' => array(
						'Courrier'
					),
					'recursive' => -1
				)
			);

			if( !empty( $documentsPath ) && !empty($documents) ) {
				foreach( $documents as $doc ) {
					if( file_exists($documentsPath . DS . $doc['Courrier']['reference'] . DS . $doc['Document']['name'])) {
						unlink($documentsPath .  DS . $doc['Courrier']['reference'] . DS . $doc['Document']['name']);
					}
					rmdir($documentsPath . DS . $doc['Courrier']['reference']);
				}
			}

			foreach( $fluxIds as $id ) {
				// on stocke qui a supprimé quel flux
				$flux = $Courrier->find('first', array('conditions' => array('Courrier.id' => $id), 'contain' => false, 'recursive' => -1) );
				$this->loadModel('Journalevent');
				$datasSession = $this->Session->read('Auth.User');
				$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé le flux ".$flux['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
				$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);
			}


			$deleteAll[] = $Courrier->delete($fluxIds);
			if (!in_array(false, $deleteAll, true)) {
				$Courrier->commit();
				$this->Jsonmsg->valid(__d('courrier', 'Courriers deleted'));
			} else {
				$Courrier->rollback();
				$this->Jsonmsg->error(__d('courrier', 'Courriers was not deleted'));
			}

			$this->Jsonmsg->send();
		}
	}

	/**
	 * Fonction permettant à l'administrateur de supprimer l'intégralité des flux (sans reotur en arrière possible)
	 *
	 */
	public function deleteEveryThing() {
		$this->autoRender = false;

		$this->Courrier = ClassRegistry::init('Courrier');

		$sql1 = 'delete from bancontenus;';
		$sql2 = 'delete from courriers;';
		$sql3 = 'delete from notifications;';
		$sql4 = 'delete from notifieddesktops;';
		$sql5 = 'delete from comments;';
		$sql6 = 'delete from wkf_traitements;';
		$sql7 = 'delete from journalevents;';
		$sql8 = 'alter sequence bancontenus_id_seq restart with 1;';
		$sql9 = 'alter sequence courriers_id_seq restart with 1;';
		$sql10 = 'alter sequence notifications_id_seq restart with 1;';
		$sql11 = 'alter sequence notifieddesktops_id_seq restart with 1;';
		$sql12 = 'alter sequence comments_id_seq restart with 1;';
		$sql13 = 'alter sequence wkf_traitements_id_seq restart with 1;';
		$sql14 = 'alter sequence journalevents_id_seq restart with 1;';

		$this->Courrier->query('BEGIN;');
		$this->Courrier->query( $sql1 );
		$this->Courrier->query( $sql2 );
		$this->Courrier->query( $sql3 );
		$this->Courrier->query( $sql4 );
		$this->Courrier->query( $sql5 );
		$this->Courrier->query( $sql6 );
		$this->Courrier->query( $sql7 );
		$this->Courrier->query( $sql8 );
		$this->Courrier->query( $sql9 );
		$this->Courrier->query( $sql10 );
		$this->Courrier->query( $sql11 );
		$this->Courrier->query( $sql12 );
		$this->Courrier->query( $sql13 );
		$this->Courrier->query( $sql14 );
		$this->Courrier->query('COMMIT;');


		$this->Session->setFlash(__('Tous les flux ont été supprimés'), 'growl');
	}

	/**
	 * Fonction permettant de lister les flux clos
	 */
	public function searchFluxClosErreur() {
		$User = ClassRegistry::init('User');
		$users = array();
		$users_tmp = $User->find(
			'all',
			array(
				'conditions' => array('User.active' => true),
				'recursive' => -1,
				'contain' => false,
				'order' => ['User.nom ASC, User.prenom ASC']
			)
		);
		foreach( $users_tmp as $user ) {
			$users[$user['User']['id']] = $user['User']['prenom'].' '.$user['User']['nom'];
		}
		$this->set('users', $users);
	}


	/**
	 * Fonction permettant de lister les bannettes dont le flux est clos
	 */

	public function fluxClosEnErreur() {
		$flux_tmp = [];

		if (isset($this->request->data['tri'])) {
			$tri = $this->request->data['tri'];
		}

		$querydata = $this->Courrier->getFluxClosParErreur($this->request->data);
		$this->Session->write('Courrier.FluxClosParErreur', $querydata);

		$flux = [];

		if (!empty($querydata)) {
			$querydata['limit'] = 20;
			if (isset($tri) && !empty($tri)) {
				$querydata['order'] = $tri;
			} else {
				$querydata['order'] = 'Courrier.datereception DESC';
			}

			$this->paginate = array('Courrier' => $querydata);
			$flux_tmp = $this->paginate('Courrier');
		}

		foreach ($flux_tmp as $kflx => $flx) {
			$flux[] = $flx;
		}
		$this->set('flux', $flux);

	}


	/**
	 * Fonction permettant à l'administrateur de passer en "lu" par lots les flux non clos encore présent chez un agent inactif
	 * @param type $fluxId
	 */
	public function decloreenlot() {
		$this->autoRender = false;
		$this->Jsonmsg->init();

		$Courrier = ClassRegistry::init('Courrier');
		$decloreCourriers = $this->request->data['checkItem'];

		if (!empty($decloreCourriers)) {
			$Courrier->begin();

			foreach( $decloreCourriers as $id ) {
				$flux = $Courrier->find('first', array('conditions' => array('Courrier.id' => $id), 'contain' => false, 'recursive' => -1) );
				$sql1 = "update wkf_traitements  set treated =false where target_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}');";
				$sql2 = "update wkf_visas set action='RI' where traitement_id IN (select id from wkf_traitements where target_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}' ) ) and numero_traitement = (select numero_traitement from wkf_traitements where target_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}' ) )";
				$sql3 = "update bancontenus set etat=0 where courrier_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}') and ( bannette_id IN (1, 2, 3, 4, 5, 6, 7) or bannette_id is null);";
				$sql4 = "update bancontenus set etat=1 where courrier_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}') and bannette_id=4 and desktop_id in (
			select desktop_id from desktops_desktopsmanagers where desktopmanager_id in (select trigger_id from wkf_visas where traitement_id IN (select id from wkf_traitements where target_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}' ) ) and numero_traitement = (select numero_traitement from wkf_traitements where target_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}' ) ) )
			    );";
				$Courrier->query('BEGIN;');
				$Courrier->query( $sql1 );
				$Courrier->query( $sql2 );
				$Courrier->query( $sql3 );
				$Courrier->query( $sql4 );
				$Courrier->query('COMMIT;');


				// on stocke qui a déclos quel flux
				$this->loadModel('Journalevent');
				$datasSession = $this->Session->read('Auth.User');
				$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a déclôturé le flux ".$flux['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
				$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);

				$this->Jsonmsg->valid();
			}
			$this->Jsonmsg->send();
		}
	}

	/**
	 * Fonction permettant à l'administrateur de déclore 1 flux
	 * @param type $fluxId
	 */
	public function declore( $fluxId ) {
		$this->autoRender = false;
		$this->Jsonmsg->init();

		$Courrier = ClassRegistry::init('Courrier');
		$Courrier->begin();
		$flux = $Courrier->find('first', array('conditions' => array('Courrier.id' => $fluxId), 'contain' => false, 'recursive' => -1) );

		$sql1 = "update wkf_traitements  set treated =false where target_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}');";
		$sql2 = "update wkf_visas set action='RI' where traitement_id IN (select id from wkf_traitements where target_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}' ) ) and numero_traitement = (select numero_traitement from wkf_traitements where target_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}' ) )";
		$sql3 = "update bancontenus set etat=0 where courrier_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}') and ( bannette_id IN (1, 2, 3, 4, 5, 6, 7) or bannette_id is null);";
		$sql4 = "update bancontenus set etat=1 where courrier_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}') and bannette_id=4 and desktop_id in (
			select desktop_id from desktops_desktopsmanagers where desktopmanager_id in (select trigger_id from wkf_visas where traitement_id IN (select id from wkf_traitements where target_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}' ) ) and numero_traitement = (select numero_traitement from wkf_traitements where target_id in (select id from courriers where reference ='{$flux['Courrier']['reference']}' ) ) )
			    );";
		$Courrier->query('BEGIN;');
		$Courrier->query( $sql1 );
		$Courrier->query( $sql2 );
		$Courrier->query( $sql3 );
		$Courrier->query( $sql4 );
		$Courrier->query('COMMIT;');


		// on stocke qui a déclos quel flux
		$this->loadModel('Journalevent');
		$datasSession = $this->Session->read('Auth.User');
		$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a déclôturé le flux ".$flux['Courrier']['reference']." le ".date('d/m/Y à H:i:s');
		$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);

		$this->Jsonmsg->send();
	}

}

?>
