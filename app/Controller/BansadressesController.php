<?php

/**
 * Référentiel des adresses FANTOIR
 *
 *
 * Bansadresses controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller
 */
class BansadressesController extends AppController {

	/**
	 * Controller uses
	 *
	 * @var array
	 * @access public
	 */
	public $uses = array('Banadresse');

	/**
	 * Gestion des référentiels de voie
	 *
	 * @logical-group Carnet d'adresse
	 * @user-profile Admin
	 *
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
//		$this->set('ariane', array(
//                    '<a href="environnement/index/0/admin">'.__d('menu', 'Administration', true).'</a>',
//                    __d('menu', 'gestionBans', true)
//                ));
	}


    public function beforeFilter() {
        ini_set( 'max_execution_time', 0 );
        ini_set( 'memory_limit', '1024M' );
        parent::beforeFilter();
        $this->Auth->allow('getCodePostal');
    }
    /**
	 * Récupération des codes postaux associés aux adresses
	 *
	 * @logical-group Banadresse
	 * @user-profil Courrier
	 *
	 * @access public
	 * @param integer $id identifiant de la méta-donnée
	 * @return void
	 */

	public function getCodePostal( $bancommuneId, $banadresseName = null ) {
        $this->autoRender = false;
        if( !empty( $banadresseName ) ) {
            $conditions = array(
                'OR' => array(
                    array(
                        'Banadresse.nom_voie' => $banadresseName,
                        'Banadresse.bancommune_id' => $bancommuneId
                    ),
                    array(
                        'Banadresse.nom_afnor' => $banadresseName,
                        'Banadresse.bancommune_id' => $bancommuneId
                    )
                )
            );

            $fields = array(
                'Banadresse.code_post',
                'Banadresse.nom_afnor',
                'Banadresse.nom_commune',
                'Banadresse.nom_voie',
                'Banadresse.canton'
            );
        }
        else {
            $conditions = array(
                'Banadresse.bancommune_id' => $bancommuneId
            );

            $fields = array(
                'Banadresse.code_post',
                'Banadresse.nom_commune',
                'Banadresse.canton'
            );
        }
        $banadresse = $this->Banadresse->find(
            'first',
            array(
                'fields' => $fields,
                'conditions' => $conditions,
                'contain' => false,
                'recursive' => -1
            )
        );
        $codepostal = $banadresse['Banadresse']['code_post'];
        $ville = $banadresse['Banadresse']['nom_commune'];
        $nom_afnor = $banadresse['Banadresse']['nom_afnor'];
        $nom_voie = $banadresse['Banadresse']['nom_voie'];
        $canton = $banadresse['Banadresse']['canton'];

        if( empty( $codepostal ) && empty($ville) && empty($nom_voie) ) {
            return '';
        }
        return json_encode(
            array(
                'codepostal' => $codepostal,
                'ville' => $ville,
                'nom_afnor' => $nom_afnor,
                'nom_voie' => $nom_voie,
                'canton' => $canton
            )
        );
    }

}
