<?php

/**
 * Fiches de contact
 *
 * Contactinfos controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class ContactinfosController extends AppController {

    /**
     * Controller uses
     *
     * @var array
     */
    public $uses = array('Contactinfo');

    protected function _setOptions() {
        $options = array();
        $options = $this->Contactinfo->Contact->Addressbook->listTypesVoie();

        $civilite = array('Monsieur' => 'Monsieur', 'Madame' => 'Madame', /*'Mademoiselle' => 'Mademoiselle',*/ 'Monsieur et Madame' => 'Monsieur et Madame');
//        debug($options);
        $this->set(compact('options', 'civilite'));

        $this->set('bans', $this->Contactinfo->Ban->find(
                        'all', array(
                    'order' => array('Ban.name ASC'),
                    'contain' => array(
                        'Bancommune' => array(
                            'order' => 'Bancommune.name ASC'
                        )
                    )
                        )
                )
        );
    }

    /**
     * Ajout d'une fiche de contact
     *
     * @logical-group Contact
     * @logical-group Fiche de contact
     * @user-profile Admin
     *
     * @logical-used-by Flux > Ajout d'un flux
     * @user-profile User
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $contactId
     * @return void
     */
    public function add($contactId) {
        $contact = $this->Contactinfo->Contact->find(
                'first', array(
            'conditions' => array(
                'Contact.id' => $contactId
            ),
            'contain' => false
                )
        );
        $this->Contactinfo->recursive = -1;

        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $options = $this->Contactinfo->Contact->Addressbook->listTypesVoie();
            $typevoie = Hash::get($options['Addressbook']['typevoie'], Hash::get($this->request->data, 'Contactinfo.typevoie'));
            $this->request->data['Contactinfo']['adresse'] = Hash::get($this->request->data, 'Contactinfo.numvoie') . ' ' . $typevoie . ' ' . Hash::get($this->request->data, 'Contactinfo.nomvoie');
            $this->request->data['Contactinfo']['contact_id'] = $contactId;

            $this->Contactinfo->create($this->request->data);

            // Cas de l'ajout depuis la gestion du flux
            $pageAdd = explode("/", $_SERVER['HTTP_REFERER']);
            if ($pageAdd[3] != 'addressbooks') {
                if ($this->Contactinfo->save()) {
                    $this->autoRender = false;
                    $result = $contactId;
                    echo $result;
                    $this->Contactinfo->commit(); // FIXME
                } else {
                    $result = false;
                    echo $result;
                    $this->Contactinfo->rollback();
                }
            } else {
                // Cas de l'ajout depuis l'administration du carnet d'adresse
                if ($this->Contactinfo->save()) {
                    $this->Jsonmsg->valid();
                } else {
                    $this->Contactinfo->rollback();
                }
                $this->Jsonmsg->send();
            }
        } else {
            $this->request->data['Contactinfo']['civilite'] = $contact['Contact']['civilite'];
            $this->request->data['Contactinfo']['nom'] = $contact['Contact']['nom'];
            $this->request->data['Contactinfo']['prenom'] = $contact['Contact']['prenom'];
        }
        $this->_setOptions();
    }

    /**
     *
     */
    public function getHomonyms() {
        Configure::write('debug', 0);
        $this->autoRender = false;
        $return = array();
        if ($this->RequestHandler->isAjax() && !empty($this->request->data['Contactinfo']['nom']) && !empty($this->request->data['Contactinfo']['prenom'])) {
            $return = $this->Contactinfo->getHomonyms($this->request->data['Contactinfo']['nom'], $this->request->data['Contactinfo']['prenom']);
        }
        echo json_encode($return);
    }

    /**
     * Edition d'une fiche de contact
     *
     * @logical-group Contact
     * @logical-group Fiche de contact
     * @user-profile Admin
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function edit($id = null) {
        $this->Contactinfo->recursive = -1;
        if (!empty($this->request->data)) {
            $options = $this->Contactinfo->Contact->Addressbook->listTypesVoie();
            $typevoie = Hash::get($options['Addressbook']['typevoie'], Hash::get($this->request->data, 'Contactinfo.typevoie'));
            $this->request->data['Contactinfo']['adresse'] = Hash::get($this->request->data, 'Contactinfo.numvoie') . ' ' . $typevoie . ' ' . Hash::get($this->request->data, 'Contactinfo.nomvoie');
            $this->Jsonmsg->init();
            $this->Contactinfo->create($this->request->data);
            if ($this->Contactinfo->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else if ($id != null) {
            $this->request->data = $this->Contactinfo->find('first', array('conditions' => array('Contactinfo.id' => $id), 'contain' => 'Contact.id'));
        }
        $this->_setOptions();
    }

    /**
     * Suppression d'une fiche de contact (soft delete - delete)
     *
     * @logical-group Contact
     * @logical-group Fiche de contact
     * @user-profile Admin
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function delete($id = null, $real = true) {
        if ($id != null) {
            if ($real) {
                $this->Jsonmsg->init(__d('default', 'delete.error'));
                if ($this->Contactinfo->delete($id, true)) {
                    $this->Jsonmsg->valid(__d('default', 'delete.ok'));
                }
            } else {
                $this->Jsonmsg->init();
                $this->Contactinfo->id = $id;

                if ($this->Contactinfo->saveField('active', false)) {
                    $this->Jsonmsg->valid();
                }
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Ajout d'une fiche de contact
     *
     * @logical-group Contact
     * @logical-group Fiche de contact
     * @user-profile Admin
     *
     * @logical-used-by Flux > Ajout d'un flux
     * @user-profile User
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $contactId
     * @return void
     */
    public function addFromCourrier($contactId, $org) {
        if ($org == 'organisme') {
            $organisme = $this->Contactinfo->Contact->Organisme->find(
                    'first', array(
                'conditions' => array(
                    'Organisme.id' => $contactId
                ),
                'joins' => array(
                    $this->Contactinfo->Contact->Organisme->join('Contact', array('type' => 'LEFT OUTER'))
                ),
                'recursive' => -1
                    )
            );
        } else {
            $contact = $this->Contactinfo->Contact->find(
                    'first', array(
                'conditions' => array(
                    'Contact.id' => $contactId
                ),
                'contain' => false
                    )
            );
        }
//debug($this->request->data);
        $this->Contactinfo->recursive = -1;
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $options = $this->Contactinfo->Contact->Addressbook->listTypesVoie();
            $typevoie = Hash::get($options['Addressbook']['typevoie'], Hash::get($this->request->data, 'Contactinfo.typevoie'));
            $this->request->data['Contactinfo']['adresse'] = Hash::get($this->request->data, 'Contactinfo.numvoie') . ' ' . $typevoie . ' ' . Hash::get($this->request->data, 'Contactinfo.nomvoie');
//debug($contact);

            $save = true;
            if ($org == 'organisme') {
                $infoContact = array();
                $infoContact['Contact']['civilite'] = $this->request->data['Contactinfo']['civilite'];
                $infoContact['Contact']['nom'] = $this->request->data['Contactinfo']['nom'];
                $infoContact['Contact']['prenom'] = $this->request->data['Contactinfo']['prenom'];
                $infoContact['Contact']['organisme_id'] = $contactId;
                $infoContact['Contact']['addressbook_id'] = $organisme['Organisme']['addressbook_id'];
                $infoContact['Contact']['name'] = $this->request->data['Contactinfo']['nom'] . ' ' . $this->request->data['Contactinfo']['prenom'];
                $infoContact['Contact']['citoyen'] = false;
//    debug($infoContact);
                $this->Contactinfo->Contact->create($infoContact);
                $save = $this->Contactinfo->Contact->save();
            }
//debug($save);
            if ($save) {

                if ($org == 'organisme') {
                    $this->request->data['Contactinfo']['contact_id'] = $this->Contactinfo->Contact->id;
                } else {
                    $this->request->data['Contactinfo']['contact_id'] = $contactId;
                }

                $this->Contactinfo->create($this->request->data);
                if ($this->Contactinfo->save()) {
                    $this->Jsonmsg->valid();
                }
                $this->Jsonmsg->send();
            }
        } else {
            if ($org != 'organisme') {
                $this->request->data['Contactinfo']['civilite'] = $contact['Contact']['civilite'];
                $this->request->data['Contactinfo']['nom'] = $contact['Contact']['nom'];
                $this->request->data['Contactinfo']['prenom'] = $contact['Contact']['prenom'];
            }
        }
        $this->_setOptions();
    }

    /**
     * Edition d'une fiche de contact
     *
     * @logical-group Contact
     * @logical-group Fiche de contact
     * @user-profile Admin
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function editFromCourrier($id = null) {
        $this->Contactinfo->recursive = -1;
        if (!empty($this->request->data)) {
            $options = $this->Contactinfo->Contact->Addressbook->listTypesVoie();
            $typevoie = Hash::get($options['Addressbook']['typevoie'], Hash::get($this->request->data, 'Contactinfo.typevoie'));
            $this->request->data['Contactinfo']['adresse'] = Hash::get($this->request->data, 'Contactinfo.numvoie') . ' ' . $typevoie . ' ' . Hash::get($this->request->data, 'Contactinfo.nomvoie');
            $this->Jsonmsg->init();
            $this->Contactinfo->create($this->request->data);
            if ($this->Contactinfo->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else if ($id != null) {
            $this->request->data = $this->Contactinfo->find('first', array('conditions' => array('Contactinfo.id' => $id)));
        }
        $this->_setOptions();
    }

}
