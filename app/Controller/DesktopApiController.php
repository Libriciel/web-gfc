<?php
/**
 * Flux
 *
 * DesktopApi controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par LIBRICIEL SCOP
 * @link https://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

class DesktopApiController extends AppController
{

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public $uses = ['Desktop', 'Collectivite'];
	public $components = ['Api'];


	public function list()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$Desktops = $this->getDesktops( $this->Api->getLimit(), $this->Api->getOffset() );
		return new CakeResponse([
			'body' => json_encode($Desktops),
			'status' => 200,
			'type' => 'application/json'
		]);
	}

	public function find($DesktopId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$Desktop = $this->getDesktop($DesktopId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'body' => json_encode($Desktop),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	/**
	 * @param $userId
	 * @return CakeResponse
	 * Ajout d'un profil pour un utilisateur donné
	 */
	public function add($userId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		// Jeu d'essai
		/*$this->request->data = '{
			"name": "Desktop principal"
		}';*/

		$Desktop = json_decode($this->request->input(), true);
		$res = $this->Desktop->save($Desktop);
		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->Desktop->validationErrors);
		}

		// TODO: Service a associé au profil nouvellement créé
		$desktopAlreadyUsed = $this->Desktop->DesktopsUser->find(
			'first',
			array(
				'conditions' => array(
					'DesktopsUser.user_id' => $userId
				),
				'contain' => false
			)
		);

		if( !empty($desktopAlreadyUsed) ){
			$desktopId = $desktopAlreadyUsed['DesktopsUser']['desktop_id'];
			$serviceAlreadyUsed = $this->Desktop->DesktopsService->find(
				'first',
				array(
					'conditions' => array(
						'DesktopsService.desktop_id' => $desktopId
					),
					'contain' => false
				)
			);
			if( !empty($serviceAlreadyUsed) ) {
				$datasLiaisonsService = [
					'desktop_id' => $this->Desktop->id,
					'service_id' => $serviceAlreadyUsed['DesktopsService']['service_id']
				];
				$this->Desktop->Service->DesktopsService->save($datasLiaisonsService);
			}
			else {
				$this->response->statusCode(403);
				return "error_desktops_service";
			}
		}
		else {
			$this->response->statusCode(403);
			return "error_desktops_user";
		}


		$datasLiaison = [
			'user_id' => $userId,
			'desktop_id' => $this->Desktop->id
		];
		$success = $this->Desktop->DesktopsUser->save($datasLiaison);

		// création du bureau associé
		$desktomanagerData = [
			'name' => 'Bureau '.$Desktop['name'],
			'isautocreated' => true
		];
		if( $success ) {
			$success = $this->Desktop->Desktopmanager->save($desktomanagerData) && $success;
			$datasLiaisons = [
				'desktop_id' => $this->Desktop->id,
				'desktopmanager_id' => $this->Desktop->Desktopmanager->id
			];
			$success = $this->Desktop->Desktopmanager->DesktopDesktopmanager->save($datasLiaisons) && $success;
		}
		else {
			$this->response->statusCode(403);
			return "error_desktops_desktopmanager";
		}

		return new CakeResponse([
			'body' => json_encode(['DesktopId' => $res['Desktop']['id']]),
			'status' => 201,
			'type' => 'application/json'
		]);
	}


	public function delete($DesktopId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getDesktop($DesktopId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$this->Desktop->id = $DesktopId;
		$this->Desktop->delete();
		return new CakeResponse([
			'body' => json_encode(['message' => "Le profil utilisateur a bien été supprimé" ]),
			'type' => 'application/json',
			'status' => 200
		]);
	}


	public function update($DesktopId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getDesktop($DesktopId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$Desktop = $this->sanitizeDesktop($this->request->input());
		$Desktop['id'] = $DesktopId;
		$res = $this->Desktop->save($Desktop);
		if (!$res) {
			return $this->formatCakePhpValidationMessages($this->Desktop->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['DesktopId' => $res['Desktop']['id']]),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	private function sanitizeDesktop($jsonData)
	{
		$authorizedFields = ['id', 'name', "profil_id", "active"];
		return array_intersect_key(json_decode($jsonData, true) ?? [], array_flip($authorizedFields));
	}


	private function getDesktops($limit, $offset)
	{
		$conditions = array(
			'limit' => $limit,
			'offset' => $offset,
			'fields' => ['id', 'name'],
			'contain' => false,
			'recursive' => -1
		);
		$Desktops = $this->Desktop->find( 'all', $conditions );

		return Hash::extract($Desktops, "{n}.Desktop");
	}


	private function getDesktop($DesktopId)
	{
		$Desktop = $this->Desktop->find(
			'first', [
				'fields' => array_merge(
					$this->Desktop->fields(),
					$this->Desktop->User->fields()
				),
				'conditions' => [
					'Desktop.id' => $DesktopId,
					'Desktop.profil_id <>' => ADMIN_GID
				],
				'joins' => [
					$this->Desktop->join('User')
				],
				'contain' => false
			]
		);
		if (empty($Desktop)) {
			throw new NotFoundException('Profil utilisateur non trouvé / inexistant');
		}

		if( !empty( $Desktop['User']['id'] ) ) {
			throw new NotFoundException("Profil principal associé à un utilisateur: veuillez supprimer l'utilisateur pour pouvoir supprimer ce profil utilsiateur");
		}

		$formattedDesktop = Hash::extract($Desktop, "Desktop");

		return $formattedDesktop;
	}


}
