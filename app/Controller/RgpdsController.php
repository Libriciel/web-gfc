<?php

/**
 * Rgpds
 *
 * Rgpds controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par Libriciel SCOP
 * @link http://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class RgpdsController extends AppController {

	/**
	 *
	 * Controller name
	 *
	 * @var array
	 */
	public $name = 'Rgpds';

	/**
	 *
	 * Controller uses
	 *
	 * @var array
	 */
	public $uses = array('Rgpd');

	/**
	 * Fonction de rappel beforeFilter
	 *
	 * @description  Paramétrage spécifique du composant Auth. Si on est connecté en tant que superadmin, on peut accèder à toutes les pages sans utiliser les Acl de CakePHP.
	 *
	 * @access public
	 * @return void
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('*');
	}

	/**
	 * Gestion des Rgpds (interface graphique)
	 *
	 * @logical-group Rgpd
	 * @user-profile Superadmin
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
			'<a href="/environnement">' . __d('menu', 'Administration') . '</a>',
			__d('menu', 'gestionRgpds')));
	}

	/**
	 * Récupération de la liste des Rgpds (ajax)
	 *
	 * @logical-group Rgpd
	 * @user-profile Superadmin
	 *
	 * @access public
	 * @return void
	 */
	public function policy( $conn = null ) {

		$rgpd = $this->Rgpd->find(
			'first',
			array(
				'conditions' => array(
					'Rgpd.conn' => $conn
				),
				'recursive' => -1,
				'contain' => false
			)
		);

		$values = array();
		if( !empty( $rgpd ) ) {
			$values = $rgpd;
		}
		$this->set('values', $values);
	}

}

?>
