<?php

/**
 * Affaires
 *
 * Affaires controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class AffairesController extends AppController {

	/**
	 * Controller name
	 *
	 * @var string
	 * @access public
	 */
	public $name = 'Affaires';

	/**
	 * Controller uses
	 *
	 * @var array
	 * @access public
	 */
	public $uses = array('Affaire');

	public function beforeFilter() {
		$this->Auth->allow('getComment');
		parent::beforeFilter();
	}

	/**
	 * Ajout d'une affaire (avec incrémentation du compteur)
	 *
	 * @logical-group Affaires
	 * @user-profile Admin
	 * @user-profile User
	 *
	 * @access public
	 * @return void
	 */
	public function add( $courrier_id = null) {
//$this->log($this->request->data);
		if (!empty($this->request->data)) {
            $this->Affaire->begin();


            if (!empty($this->request->data['Dossier']['name'])) {
                $this->Affaire->Dossier->begin();
                $saveDossier = $this->Affaire->Dossier->save($this->request->data['Dossier']);

                if ($saveDossier) {
                    $this->Affaire->Dossier->commit();
                    $this->request->data['Affaire']['dossier_id'] = $saveDossier['Dossier']['id'];
                } else {
                    $this->Affaire->Dossier->rollback();
                    $this->request->data['Affaire']['dossier_id'] = '';
                }
            }

            $this->Affaire->create($this->request->data);
            if ($this->Affaire->save()) {
                if( !empty( $courrier_id )) {
                    $courrier = $this->Affaire->Courrier->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Courrier.id' => $courrier_id
                            ),
                            'contain' => false,
                            'recursive' => -1
                        )
                    );
                    $courrier['Courrier']['affaire_id'] = $this->Affaire->id;
                    $this->Affaire->Courrier->save($courrier);

                }
                $this->Affaire->commit();
                $this->Jsonmsg->valid();
            } else {
                $this->Affaire->rollback();
                $this->Jsonmsg->error('Erreur de sauvegarde');
            }
            $this->Jsonmsg->send();
		}

        $listDossiers = $this->Affaire->Dossier->find('list', array('order' => array('Dossier.name ASC')) );
        $ajoutDossier = array('new' => __d('dossier', 'Dossier.add'));
        $listDossiers = ($ajoutDossier + $listDossiers );
        $this->set('dossiers', $listDossiers);

		$affaires = $this->Affaire->find('list');
	}

	/**
	 * Edition d'une affaire
	 *
	 * @logical-group Affaires
	 * @user-profile Admin
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id The id of the Affaire
	 * @return void
	 */
	public function edit($id = null) {
		if ($id != null) {
			if (!empty($this->request->data)) {
                $this->Affaire->begin();
                $success = $this->Affaire->save($this->request->data);
                if ($success) {
                    $this->Affaire->commit();
                    $this->Jsonmsg->valid();
                } else {
                    $this->Affaire->rollback();
                    $this->Jsonmsg->error('Erreur de sauvegarde');
                }
                $this->Jsonmsg->send();
			} else {
				$this->request->data = $this->Affaire->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Affaire.id' => $id
                        ),
                        'contain' => false
                    )
                );
			}
			$this->set('dossiers', $this->Affaire->Dossier->find('list'));
		}
	}

	/**
	 * Suppression d'une affaire
	 *
	 * @logical-group Affaires
	 * @user-profile Admin
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id The id of the Affaire
	 * @return void
	 */
	public function delete($id = null) {
        if ($id != null) {
            $this->Jsonmsg->init(__d('default', 'delete.error'));
            $this->Affaire->begin();
            if ($this->Affaire->delete($id)) {
                $this->Affaire->commit();
                $this->Jsonmsg->valid(__d('default', 'delete.ok'));
            } else {
                $this->Affaire->rollback();
            }
            $this->Jsonmsg->send();
        }


		$this->autoRender = false;
	}

        /**
	 * obtenir d'un commentaire d'une affaire
	 *
	 * @logical-group Affaires
	 * @user-profile Admin
	 * @user-profile User
	 *
	 */
        public function getComment(){
                $this->autoRender = false;
//debug(array_keys($this->request->data));
                if( !empty($this->request->data) ) {
                    $id =   array_keys( $this->request->data);
                    if($id != null){
                            $comment = $this->Affaire->field('comment', array('Affaire.id' => $id));
                    }
                }
                else if( !empty($this->request->params['pass']) ) {
                    $id = $this->request->params['pass'][0];
                    $comment = $this->Affaire->field('comment', array('Affaire.id' => $id));
                }
//debug($comment);
//                $id =   $this->request->data['id'];
//                if($id != null){
//                        $comment = $this->Affaire->field('comment', array('Affaire.id' => $id));
//                }
                return $comment;
        }

}

?>
