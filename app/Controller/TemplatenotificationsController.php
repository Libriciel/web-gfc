<?php

/**
 * Templatenotifications
 *
 * Templatenotifications controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class TemplatenotificationsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Templatenotifications';
    public $uses = array('Templatenotification', 'Type');

    /**
     * Gestion des emails interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'templateNotifications', true)
        ));
    }

    protected function _setOptions() {

        $templates = array(
			'insertion' => 'Flux à insérer dans un circuit',
			'traitement' => 'Flux à traiter',
			'refus' => 'Flux refusé',
			'retard_validation' => 'Retard de validation',
			'copy' => 'Flux envoyé pour copie',
			'insertion_reponse' => 'Flux réponse reçu',
			'aiguillage' => 'Aiguillage de flux',
			'delegation' => 'Bureaux qui me sont délégués',
			'quotidien' => 'Récapitulatif journalier',
			'bloque_parapheur' => 'Flux dans le i-Parapheur et non traité',
			'commentaire' => 'Nouveau commentaire',
        );
        $this->set('templates', $templates);
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Ajout d'un email
     *
     * @logical-group Templatenotifications
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        $templates = array(
			'insertion' => 'Flux à insérer dans un circuit',
			'traitement' => 'Flux à traiter',
			'refus' => 'Flux refusé',
			'retard_validation' => 'Retard de validation',
			'copy' => 'Flux envoyé pour copie',
			'insertion_reponse' => 'Flux réponse reçu',
			'aiguillage' => 'Aiguillage de flux',
			'delegation' => 'Bureaux qui me sont délégués',
			'quotidien' => 'Récapitulatif journalier',
			'bloque_parapheur' => 'Flux dans le i-Parapheur et non traité',
			'commentaire' => 'Nouveau commentaire',
        );

        $templatenotifications = $this->Templatenotification->find('all');
        if (!empty($templatenotifications)) {
            foreach ($templatenotifications as $key => $templateValue) {
                if (in_array($templateValue['Templatenotification']['name'], array_keys($templates))) {
                    unset($templates[$templateValue['Templatenotification']['name']]);
                }
            }
        }
        $this->set('templates', $templates);

        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $templatenotification = $this->request->data;


            $this->Templatenotification->begin();
            $this->Templatenotification->create($templatenotification);
            if ($this->Templatenotification->save()) {
                $json['success'] = true;

                $path = APP . DS . 'Config' . DS . 'emails' . DS . "{$this->request->data['Templatenotification']['name']}.txt";
//                if( !empty($path)) {
                fopen($path, "r");
                $file = file_put_contents($path, $this->request->data['Templatenotification']['object']);
//                fclose ($file);
//                }
//                else {
//
//                }
            }
            if ($json['success']) {
                $this->Templatenotification->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Templatenotification->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }

//        $this->_setOptions();
    }

    /**
     * Edition d'un type
     *
     * @logical-group Templatenotifications
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {

        if (!empty($this->request->data)) {
//                  debug($this->request->data['Templatenotification']['object']);
//            $this->request->data['Templatenotification']['object'] = str_replace(array("\r\n","\n"),'<br />',$this->request->data['Templatenotification']['object']);
//            $this->request->data['Templatenotification']['object'] = strip_tags($this->request->data['Templatenotification']['object']);
            $this->Jsonmsg->init();
            $this->Templatenotification->create();
            $templatenotification = $this->request->data;

            if ($this->Templatenotification->save($templatenotification)) {
                $this->Jsonmsg->valid();
                $path = APP . DS . 'Config' . DS . 'emails' . DS . "{$this->request->data['Templatenotification']['name']}.txt";
//                if( !empty($path)) {
                fopen($path, "r");
                $file = file_put_contents($path, $this->request->data['Templatenotification']['object']);
//                fclose ($file);
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Templatenotification->find('first', array('conditions' => array('Templatenotification.id' => $id)));
            $notifValue = $this->request->data['Templatenotification']['name'];
//            $this->request->data['Templatenotification']['object'] = str_replace(array("<br />"),"\n",$this->request->data['Templatenotification']['object']);

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }

            $templates = array(
				'insertion' => 'Flux à insérer dans un circuit',
				'traitement' => 'Flux à traiter',
				'refus' => 'Flux refusé',
				'retard_validation' => 'Retard de validation',
				'copy' => 'Flux envoyé pour copie',
				'insertion_reponse' => 'Flux réponse reçu',
				'aiguillage' => 'Aiguillage de flux',
				'delegation' => 'Bureaux qui me sont délégués',
				'quotidien' => 'Récapitulatif journalier',
				'bloque_parapheur' => 'Flux dans le i-Parapheur et non traité',
				'commentaire' => 'Nouveau commentaire',
            );

            $notifTraduction = $templates[$notifValue];

            $templatenotifications = $this->Templatenotification->find('all');
            if (!empty($templatenotifications)) {
                foreach ($templatenotifications as $key => $templateValue) {
                    if (in_array($templateValue['Templatenotification']['name'], array_keys($templates))) {
                        unset($templates[$templateValue['Templatenotification']['name']]);
                        $templates = array_merge(
                                $templates, array(
                            $notifValue => $notifTraduction
                                )
                        );
                    }
                }
            }
            $this->set('templates', $templates);
        }

//        $templatenotifications = $this->Templatenotification->find( 'all' );
//        $this->_setOptions();
    }

    /**
     * Suppression d'un email
     *
     * @logical-group Templatenotifications
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Templatenotification->begin();
        if ($this->Templatenotification->delete($id)) {
            $this->Templatenotification->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Templatenotification->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des mails à scruter (ajax)
     *
     * @logical-group Templatenotifications
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getTemplatenotifications() {
        $templatenotifications_tmp = $this->Templatenotification->find(
                "all", array(
            'order' => 'Templatenotification.name'
                )
        );
        $templatenotifications = array();
        foreach ($templatenotifications_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $templatenotifications[] = $item;
        }
        $this->set(compact('templatenotifications'));
        $this->_setOptions();
    }

}

?>
