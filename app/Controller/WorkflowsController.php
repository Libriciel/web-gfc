<?php

/**
 * Circuit de traitement
 *
 * Workflows controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

App::uses('Folder', 'Utility');
class WorkflowsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var sting
     */
    public $name = "Workflows";

    /**
     * Controller uses
     *
     * @access public
     * @var array
     */
    public $uses = array('Cakeflow.Circuit', 'Cakeflow.Etape', 'Cakeflow.Visa', 'Cakeflow.Composition', 'Cakeflow.Traitement');

	/**
	 * @var repository utilisée pour la récupération de la liste des sous-types IP
	 */
    public $repository;

	/**
	 * Controller components
	 *
	 * @var array
	 */
	public $components = array('NewPastell');

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow( 'export');
    }


    /**
     * Gestion des circuits de traitements (interface graphique)
     *
     * @logical-group Circuits de traitements
     * @logical-group CakeFlow
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'gestionCircuits', true)
        ));
    }

    /**
     * Récupération de la liste des circuits de traitements (ajax)
     *
     * @logical-group Circuits de traitements
     * @logical-group CakeFlow
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function getWorkflows() {
		$this->Circuit->recursive = -1;
		$querydata = $this->Circuit->search($this->request->data);
		$nbCircuit = $this->Circuit->find('count', $querydata);

		$workflows_tmp = $this->Circuit->find('all', $querydata);

		$workflows = array();
		foreach ($workflows_tmp as $i => $item) {
			$item['right_edit'] = true;
			$item['right_copieCircuit'] = true;
			$item['right_delete'] = $this->_isDeletable($item);
			$workflows[] = $item;
		}

        $this->set('workflows', $workflows);

        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    private function _isDeletable($workflow) {
        return $this->Circuit->isDeletable($workflow['Circuit']['id']);
    }

    /**
     * Ajout d'un circuit
     *
     * @logical-group Circuits de traitements
     * @logical-group CakeFlow
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        $listSoustypes = $this->Circuit->Soustype->find(
                'list', array(
            'conditions' => array(
                'Soustype.circuit_id IS NULL',
                'Soustype.active' => 1
            ),
            'order' => array(
                'Soustype.name ASC'
            )
                )
        );
        $this->set('listSoustypes', $listSoustypes);

        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->request->data["Circuit"]["created_user_id"] = $this->Session->read('Auth.User.id');
            $this->Circuit->create($this->request->data);
            if ($this->Circuit->save()) {
                // sauvegarde du soustype depuis le circuit
                if (!empty($this->request->data['Circuit']['soustype'])) {
                    $soustype = $this->Circuit->Soustype->find('first', array('conditions' => array('Soustype.id' => $this->request->data['Circuit']['soustype']), 'recursive' => -1));

                    $save = $this->Circuit->Soustype->updateAll(
                            array('Soustype.circuit_id' => $this->Circuit->id), array('Soustype.id' => $this->request->data['Circuit']['soustype'])
                    );
                    if ($save) {
                        $this->Circuit->Soustype->commit();
                        $this->Jsonmsg->valid();
                    } else {
                        $this->Circuit->Soustype->rollback();
                        $this->Jsonmsg->error('Erreur de sauvegarde lors de l\'association du circuit au sous-type');
                    }
                }
                $this->Jsonmsg->valid();
                $this->Jsonmsg->json['CircuitId'] = $this->Circuit->id;
            }

            $this->Jsonmsg->send();
        }
    }

    /**
     * Edition des informations d'un circuit
     *
     * @logical-group Circuits de traitements
     * @logical-group CakeFlow
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un circuit
     * @return void
     */
    public function edit($id = null) {

        // Liste de tous les soutypes
        $listSoustypes = $this->Circuit->Soustype->find(
                'list', array(
            'conditions' => array(
                'OR' => array(
                    'Soustype.circuit_id IS NULL',
                    'Soustype.circuit_id' => $id
                ),
                'Soustype.active' => 1
            ),
            'order' => array(
                'Soustype.name ASC'
            )
                )
        );
        $this->set('listSoustypes', $listSoustypes);

        if (!empty($this->request->data)) {

            $this->Jsonmsg->init();
            $this->Circuit->create($this->request->data);

            if ($this->Circuit->save()) {

                if (!empty($this->request->data['Soustype']['Soustype'])) {
                    $soustype = $this->Circuit->Soustype->find('first', array('conditions' => array('Soustype.id' => $this->request->data['Soustype']['Soustype']), 'recursive' => -1));

                    // on regarde les anciens enregistrements entre le circut et les sous-types
                    $records = $this->Circuit->Soustype->find(
                        'list',
                        array(
                            'fields' => array("Soustype.id", "Soustype.circuit_id"),
                            'conditions' => array(
                                "Soustype.circuit_id" => $this->request->data['Circuit']['id'],
                                "Soustype.circuit_id IS NOT NULL"
                            ),
                            'contain' => false
                        )
                    );
                    $oldrecordsids = array_keys($records);
                    // on regarde les nouveautés
                    $nouveauxids = Hash::extract($this->request->data, "Soustype.Soustype.{n}");
                    // si des différences existent on note les ids enlevés, les liens coupés entre circuit et sous-types
                    $idsenmoins = array_diff($oldrecordsids, $nouveauxids);

                    // on met à jour les sous-types qui ne sont plus liés au circuit en cours
                    if (!empty($idsenmoins)) {
                        $success = $this->Circuit->Soustype->updateAll(
                                array('Soustype.circuit_id' => null), array(
                            'Soustype.circuit_id' => $this->request->data['Circuit']['id'],
                            'Soustype.id' => $idsenmoins
                                )
                        );
                    }

                    // on sauvegarde les sous-types
                    $save = $this->Circuit->Soustype->updateAll(
                            array('Soustype.circuit_id' => $id), array('Soustype.id' => $this->request->data['Soustype']['Soustype'])
                    );
                    if ($save) {
                        $this->Circuit->Soustype->commit();
                        $this->Jsonmsg->valid();
                    } else {
                        $this->Circuit->Soustype->rollback();
                        $this->Jsonmsg->error('Erreur de sauvegarde lors de l\'association du circuit au sous-type');
                    }
                } else {
                    $records = $this->Circuit->Soustype->find(
                            'list', array(
                        'fields' => array("Soustype.id", "Soustype.circuit_id"),
                        'conditions' => array(
                            "Soustype.circuit_id" => $id
                        )
                            )
                    );
                    $oldrecordsids = array_keys($records);
                    // si des différences existent on note les ids enlevés, les liens coupés entre circuit et sous-types
                    $idsenmoins = $oldrecordsids;

                    // on met à jour les sous-types qui ne sont plus liés au circuit en cours
                    if (!empty($idsenmoins)) {
                        $success = $this->Circuit->Soustype->updateAll(
                                array('Soustype.circuit_id' => null), array(
                            'Soustype.circuit_id' => $id,
                            'Soustype.id' => $idsenmoins
                                )
                        );
                    }
                }



                $this->Jsonmsg->valid();
            } else {
                $invalidFields = $this->Circuit->invalidFields();
                $str = "";
                foreach ($invalidFields as $field => $errors) {
                    $str .= (empty($str) ? '' : '<br />') . ( Configure::read('debug') > 0 ? $field . ' :<br />' : '') . implode('<br />', array_unique($errors, SORT_REGULAR));
                }
                $this->Jsonmsg->error($str);
            }
            $this->Jsonmsg->send();
        }
        $this->request->data = $this->Circuit->find(
			'first',
			array(
				'conditions' => array(
					'Circuit.id' => $id
				),
				'contain' => array(
					'Soustype'
				)
			)
        );
    }

    /**
     * Edition du schema d'un circuit de traitement
     *
     * @logical-group Circuits de traitements
     * @logical-group CakeFlow
     * @user-profile Admin
     *
     * @access public
     * @param integer $circuitId identifiant d'un circuit
     * @throws BadMethodCallException
     * @return void
     */
    public function setSchema($circuitId) {
        if (empty($circuitId)) {
            throw new BadMethodCallException();
        }
        //recuperation de la structure initiale du circuit
        $qdEtapes = array(
            'fields' => array(
                'Etape.id',
                'Etape.nom',
                'Etape.type',
                'Etape.ordre',
                'Etape.description',
                'Etape.soustype',
                'Etape.type_document',
				'Etape.inforequired_type_document'
            ),
            'contain' => array(
                'Composition.id',
                'Composition.type_validation',
                'Composition.trigger_id',
                'Composition.soustype',
				'Composition.type_document',
				'Composition.inforequired_type_document',
                'Composition' => array(
                    CAKEFLOW_TRIGGER_MODEL => array('fields' => CAKEFLOW_TRIGGER_FIELDS)
                )
            ),
            'conditions' => array(
                'Etape.circuit_id' => $circuitId
            ),
            'order' => 'Etape.ordre'
        );
        $etapes = $this->Etape->find('all', $qdEtapes);
//    $this->_logDebug($etapes);
        for ($i = 0; $i < count($etapes); $i++) {
            $etapes[$i]['canAdd'] = $this->Etape->canAdd($etapes[$i]['Etape']['id']);
            $etapes[$i]['isDeletable'] = $this->Etape->isDeletable($etapes[$i]['Etape']['id']);
        }
        $this->set('circuit', $this->Circuit->find('first', array('conditions' => array('Circuit.id' => $circuitId))));
        $this->set('etapes', $etapes);


        $this->loadModel('Connecteur');
        $hasParapheurActif = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true
                ),
                'contain' => false
            )
        );
        if( !empty( $hasParapheurActif ) && $hasParapheurActif['Connecteur']['signature_protocol'] == 'IPARAPHEUR') {
			if (empty($this->Session->read('Iparapheur.listSoustype'))) {
				$soustypes = $this->Etape->listeSousTypesParapheur($hasParapheurActif);
				$soustypes = $soustypes['soustype'];
			}
			else {
				$soustypes = $this->Session->read( 'Iparapheur.listSoustype' );
			}
        }
        else {
            $soustypes = array();
        }

		if( empty($soustypes) ) {
			$soustypes = array();
		}
		$this->set( 'soustypes', $soustypes);


		// Partie PASTELL
		$hasPastellActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%PASTELL%',
					'Connecteur.use_pastell'=> true
				),
				'contain' => false
			)
		);
		$this->set('hasPastellActif', $hasPastellActif);

		$documents = [];
		$listSoustypeIp = [];
		if( !empty($hasPastellActif) ) {
			if( empty($this->Session->read('Pastell.listSoustypeIp')) ) {
				$id_entity = $hasPastellActif['Connecteur']['id_entity'];
				// Récup des sous-types dispos
				$PastellComponent = new NewPastellComponent();
				$listSoustypeIp = $PastellComponent->getCircuits($id_entity, Configure::read('Pastell.fluxStudioName'));
				$this->Session->write('Pastell.listSoustypeIp', $listSoustypeIp);
			}
			$documents = $this->Etape->Circuit->getDocNamePastell($circuitId);
			$listSoustypeIp = $this->Session->read('Pastell.listSoustypeIp');
		}
		$this->set('documents', $documents);
		$this->set('listSoustypeIp', $listSoustypeIp);
    }

    /**
     * Suppression d'un circuit
     *
     * @logical-group Circuits de traitements
     * @logical-group CakeFlow
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un circuit
     * @throws NotFoundException
     * @return void
     */
    public function delete($id = null) {
        $circuit = $this->Circuit->find(
        	'first',
			array(
				'conditions' => array('Circuit.id' => $id),
				'contain' => array(
					'Soustype'
				)
			)
		);

        if (empty($circuit)) {
            throw new NotFoundException();
        }
        $this->Jsonmsg->init();
        if ($this->Circuit->delete($circuit['Circuit']['id'])) {
        	if( !empty($circuit['Soustype'][0] ) ) {
				foreach($circuit['Soustype'] as $s => $soustype) {
					$this->Circuit->Soustype->updateAll(
						array('Soustype.circuit_id' => NULL),
						array('Soustype.id' => $soustype['id'])
					);
				}
        	}
            $this->Jsonmsg->valid();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Changement du positionnement d'une étape dans un circuit de traitement
     *
     * @logical-group Circuits de traitements
     * @logical-group CakeFlow
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'une étape
     * @param string $direction indique le sens de déplacement de l'étape before / up - after / down
     * @return void
     */
    public function moveEtape($id = null, $direction = null) {
        $this->render(false);
        if ($id != null && $direction != null) {
            if ($direction == "up" || $direction == "before") {
                $this->Etape->moveUp($id);
            }
            if ($direction == "down" || $direction == "after") {
                $this->Etape->moveDown($id);
            }
        }
    }

    /**
     *  Copie de circuit
     *
     */
    public function copieCircuit($id = null) {

        if ($id != null) {
            $circuit = $this->Circuit->find(
                    'first', array(
                'conditions' => array(
                    'Circuit.id' => $id
                ),
                'contain' => array(
                    'Soustype',
                    'Etape' => array(
                        'Composition'
                    )
                )
                    )
            );
// debug($circuit);


            $this->Jsonmsg->init();

            $newCircuit = array(
                'Circuit' => array(
                    'nom' => $circuit['Circuit']['nom'] . ' (copie)',
                    'description' => $circuit['Circuit']['description'],
                    'actif' => $circuit['Circuit']['actif'],
                    'defaut' => $circuit['Circuit']['defaut'],
                    'created_user_id' => $this->Session->read('Auth.User.id'),
                    'modified_user_id' => $circuit['Circuit']['modified_user_id']
                )
            );

// debug($newCircuit);
// die();
            $this->Circuit->create($newCircuit);
            $success = $this->Circuit->save();

            if (isset($circuit['Etape']) && !empty($circuit['Etape'])) {
                $etapes = $circuit['Etape'];
                foreach ($circuit['Etape'] as $i => $etape) {
                    $newEtape = array(
                        'Etape' => array(
                            'circuit_id' => $this->Circuit->id,
                            'nom' => $etape['nom'],
                            'description' => $etape['description'],
                            'type' => $etape['type'],
                            'ordre' => $etape['ordre'],
                            'created_user_id' => $this->Session->read('Auth.User.id'),
                            'modified_user_id' => $etape['modified_user_id'],
                            'soustype' => $etape['soustype'],
                            'cpt_retard' => $etape['cpt_retard'],
                            'type_document' => $etape['type_document'],
                            'inforequired_type_document' => $etape['inforequired_type_document']
                        )
                    );
                    $this->Circuit->Etape->create($newEtape);
                    $success = $this->Circuit->Etape->save() && $success;

                    if (isset($etape['Composition']) && !empty($etape['Composition'])) {
                        foreach ($etape['Composition'] as $idc => $composition) {
                            $newComposition = array(
                                'Composition' => array(
                                    'etape_id' => $this->Circuit->Etape->id,
                                    'type_validation' => $composition['type_validation'],
                                    'created_user_id' => $this->Session->read('Auth.User.id'),
                                    'modified_user_id' => $composition['modified_user_id'],
                                    'trigger_id' => $composition['trigger_id'],
                                    'soustype' => $composition['soustype'],
                                    'type_composition' => $composition['type_composition'],
									'type_document' => $composition['type_document'],
									'inforequired_type_document' => $composition['inforequired_type_document']
                                )
                            );
                            $this->Circuit->Etape->Composition->create($newComposition);
                            $success = $this->Circuit->Etape->Composition->save() && $success;
                        }
                    }
                }
            }

            if ($success) {
                $this->Jsonmsg->valid();
            } else {
                $this->Jsonmsg->error('Erreur lors de la copie du circuit');
                $this->Circuit->rollback();
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Vérifier s'il exit assez de utilisateur dans un étape(surtout type
     * Collaborative et Concurrente)
     *
     *
     * @access public
     * @param integer $etapeId
     * @return  array($isUserEnough,$falseEtapeId);
     */
    public function userEnough($etapeId) {
        $isUserEnough = true;
        $falseEtapeId = array();
        $querydata = array(
            'fields' => array(
                'Etape.id',
                'Etape.type'
            ),
            'contain' => false,
            'conditions' => array(
                'Etape.id' => $etapeId
            )
        );
        $etapes = $this->Etape->find('all', $querydata);

        foreach ($etapes as $value) {
            $qdComp = array(
                'fields' => array(
                    'Etape.id',
                    'Etape.nom'
                ),
                'contain' => array(
                    'Composition.id'
                ),
                'conditions' => array(
                    'Etape.id' => $value['Etape']['id']
                )
            );
            $comp = $this->Etape->find('all', $qdComp);
            if ($value['Etape']['type'] != 1) {
                for ($i = 0; $i < count($comp); $i++) {
                    if (count($comp[$i]['Composition']) < 2) {
                        $isUserEnough = false;
                        array_push($falseEtapeId, $comp[$i]['Etape']['id']);
                    }
                }
            } else {
                for ($i = 0; $i < count($comp); $i++) {
                    if (count($comp[$i]['Composition']) < 1) {
                        $isUserEnough = false;
                        array_push($falseEtapeId, $comp[$i]['Etape']['id']);
                    }
                }
            }
        }
        return array($isUserEnough, $falseEtapeId);
    }

    /**
     * obtenir une description d'une affaire
     *
     * @logical-group Etapes
     * @user-profile Admin
     * @user-profile User
     *
     */
    public function getDescription() {
        $this->autoRender = false;
        $id = $this->request->data['id'];
        if ($id != null) {
            $comment = $this->Etape->field('description', array('Etape.id' => $id));
        }
        return $comment;
    }

    /**
     * Export des circuits + etapes + compositions d'étapes
     */
    public function export() {
        $desktopsmanagers = $this->Etape->Composition->{CAKEFLOW_TRIGGER_MODEL}->find('list');
        $typesEtapes = array(
            ETAPESIMPLE => 'Simple',
            ETAPECONCURENTE => 'OU',
            ETAPECOLLABORATIVE => 'ET'
        );
        $querydata = array(
            'conditions' => array( 'Circuit.actif' => true ),
            'order' => array(
                'Circuit.nom'
            ),
            'contain' => array(
                'Etape' => array(
                    'Composition',
                    'order' => array('Etape.ordre ASC')
                ),
            )
        );
        $results = $this->Circuit->find('all', $querydata);

        foreach ($results as $i => $circuit) {
            foreach ($circuit['Etape'] as $e => $etape) {
                $results[$i]['Circuit']['listecompos'][$etape['ordre']] = '';
                $results[$i]['Circuit']['listeetapes'][$etape['ordre']] = '- Etape '.$etape['ordre'] . ' : ' .$etape['nom']. "\r\n";
                foreach( $etape['Composition'] as $c => $compo) {
                    $results[$i]['Circuit']['listecompos'][$etape['ordre']] .= '- Etape '.$etape['ordre'] . ' ( '.$typesEtapes[$etape['type']].' ) : '.$desktopsmanagers[$compo['trigger_id']]. "\r\n";
                }
            }
        }
        $this->set('results', $results);
        $this->layout = '';
    }

}

?>
