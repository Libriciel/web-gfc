<?php
/**
 * Flux
 *
 * ServiceApi controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par LIBRICIEL SCOP
 * @link https://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

class ServiceApiController extends AppController
{

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public $uses = ['Service', 'Collectivite'];
	public $components = ['Api'];


	public function list()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$Services = $this->getServices( $this->Api->getLimit(), $this->Api->getOffset() );
		return new CakeResponse([
			'body' => json_encode($Services),
			'status' => 200,
			'type' => 'application/json'
		]);
	}

	public function find($ServiceId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$Service = $this->getService($ServiceId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'body' => json_encode($Service),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	/**
	 * @param $userId
	 * @return CakeResponse
	 * Ajout d'un profil pour un utilisateur donné
	 */
	public function add()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		// Jeu d'essai
		/*$this->request->data = '{
			"name": "Service principal"
		}';*/

		$Service = json_decode($this->request->input(), true);
		$res = $this->Service->save($Service);
		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->Service->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['ServiceId' => $res['Service']['id']]),
			'status' => 201,
			'type' => 'application/json'
		]);
	}


	public function delete($ServiceId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getService($ServiceId);
			$this->Service->id = $ServiceId;
			$this->Service->delete();
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'status' => 204,
		]);
	}


	public function update($ServiceId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getService($ServiceId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

//		$Service = $this->sanitizeService($this->request->input());
		$Service = json_decode($this->request->input(), true);
		$Service['id'] = $ServiceId;
		$res = $this->Service->save($Service);
		if (!$res) {
			return $this->formatCakePhpValidationMessages($this->Service->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['ServiceId' => $res['Service']['id']]),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	private function sanitizeService($jsonData)
	{
		$authorizedFields = ['name', 'parent_id'];
		return array_intersect_key(json_decode($jsonData, true) ?? [], array_flip($authorizedFields));
	}


	private function getServices($limit, $offset)
	{
		$conditions = array(
			'limit' => $limit,
			'offset' => $offset,
			'fields' => ['id', 'name'],
			'contain' => false,
			'recursive' => -1
		);
		$Services = $this->Service->find( 'all', $conditions );

		return Hash::extract($Services, "{n}.Service");
	}


	private function getService($ServiceId)
	{
		$Service = $this->Service->find('first', [
			'fields' => ['Service.id', 'Service.name', 'Service.parent_id'],
			'conditions' => ['Service.id' => $ServiceId],
			'contain' => false
		]);

		if (empty($Service)) {
			throw new NotFoundException('Service non trouvé / inexistant');
		}

		$formattedService = Hash::extract($Service, "Service");

		return $formattedService;
	}


}
