<?php
/**
 * Flux
 *
 * SoustypeApi controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par LIBRICIEL SCOP
 * @link https://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

class SoustypeApiController extends AppController
{

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public $uses = ['Soustype', 'Collectivite'];
	public $components = ['Api'];


	public function list()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$Soustypes = $this->getSoustypes( $this->Api->getLimit(), $this->Api->getOffset() );
		return new CakeResponse([
			'body' => json_encode($Soustypes),
			'status' => 200,
			'type' => 'application/json'
		]);
	}

	public function find($SoustypeId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$Soustype = $this->getSoustype($SoustypeId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'body' => json_encode($Soustype),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	public function add()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		// Jeu d'essai
		/*'{
			"name": "Soustype principal",
			"type_id": 1,
			"entrant": 1,
			"delai_nb": 15,
			"delai_unite": 0,
			"information": "",
			"active": true
		}'*/

		$Soustype = json_decode($this->request->input(), true);

		$typeExists = $this->Soustype->Type->find(
			'first',
			[
				'conditions' => [
					'Type.id' => $Soustype['type_id']
				],
				'contain' => false,
				'recursive' => -1
			]
		);
		if( !$typeExists ) {
			return new CakeResponse([
				'body' => json_encode(['message' => "Création impossible: le type sélectionné n'existe pas"]),
				'status' => 200,
				'type' => 'application/json'
			]);
		}

		$res = $this->Soustype->save($Soustype);
		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->Soustype->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['SoustypeId' => $res['Soustype']['id']]),
			'status' => 201,
			'type' => 'application/json'
		]);
	}


	public function delete($SoustypeId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getSoustype($SoustypeId);
			$this->Soustype->id = $SoustypeId;
			$this->Soustype->delete();
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'status' => 200,
			'body' => json_encode(['message' => 'Sous-type supprimé']),
			'type' => 'application/json'
		]);
	}


	public function update($SoustypeId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getSoustype($SoustypeId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$Soustype = json_decode($this->request->input(), true);
		$Soustype['id'] = $SoustypeId;
		$res = $this->Soustype->save($Soustype);
		if (!$res) {
			return $this->formatCakePhpValidationMessages($this->Soustype->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['SoustypeId' => $res['Soustype']['id']]),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	private function getSoustypes( $limit, $offset )
	{
		$conditions = array(
			'limit' => $limit,
			'offset' => $offset
		);
		$Soustypes = $this->Soustype->find('all', $conditions);

		return Hash::extract($Soustypes, "{n}.Soustype");
	}


	private function getSoustype($SoustypeId)
	{
		$Soustype = $this->Soustype->find('first', [
			'conditions' => ['Soustype.id' => $SoustypeId],
			'contain' => ['SousSoustype' => [
				'fields' => ['SousSoustype.id', 'SousSoustype.name']
			]]
		]);

		if (empty($Soustype)) {
			throw new NotFoundException('Sous-type non trouvé / inexistant');
		}

		$formattedSoustype = Hash::extract($Soustype, "Soustype");

		foreach ($Soustype['SousSoustype'] as &$sousSoustype) {
			unset($sousSoustype['SousSoustype']);
		}

		$formattedSoustype['sousSoustypes'] = $Soustype['SousSoustype'] ?? [];

		return $formattedSoustype;
	}


}
