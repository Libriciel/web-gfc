<?php

/**
 * Administration
 *
 * Checks controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller
 */
class ConsolesController extends AppController {

	/**
	 *
	 * Controller name
	 *
	 * @var string
	 * @access public
	 */
	public $name = 'Consoles';

    public $components = array(
        'Auth' => array(
            'mapActions' => array(
                'allow' => array('index', 'tail', 'ajaxdisk'),
            )
        )
    );

    /**
     * Gestion des connecteurs interface graphique)
     *
     * @logical-group Connecteurs
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'Console', true)
        ));
    }


    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('ajaxlogs', 'tail', 'ajaxdisk');
    }

    /**
     * Visualisation des logs de l'application
     */
    public function ajaxlogs()
    {

        $this->layout = 'ajax';
        $logs = array();
        foreach (glob(LOGS.'*.log') as $filename) {
            $output = null;
            exec('tail -n500 "'.$filename.'"', $output);
            $logs[basename($filename)] = implode("\n", $output);
        }
        $this->set('logs', $logs);
    }


    /**
     * Effectue un tail -f sur un fichier log
     * @param string $logfile
     * @throws Exception
     */
    public function tail($logfile)
    {
        $filename = LOGS.$logfile;
        if (!is_readable($filename)) {
            throw new Exception(__("Impossible de lire le fichier {0}", $filename));
        }
        ob_end_clean();
        ob_start();

        $handle = popen("timeout 5 tail -f -n100 \"$filename\" 2>&1", 'r');
        while (!feof($handle)) {
            echo fgets($handle);
            ob_flush();
            flush();
        }
        pclose($handle);

        ob_end_flush();
        exit;
    }

    /**
     * Etat des disques
     */
    public function ajaxdisk()
    {
        $this->layout = 'ajax';

        exec('df -h', $disks);

        $this->set('disks', $disks);
    }

}

?>
