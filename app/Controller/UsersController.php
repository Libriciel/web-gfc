<?php

/**
 * Utilisateurs
 *
 * Users controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Users';

    /**
     * Controller uses
     *
     * @access public
     * @var array
     */
    public $uses = array('User');

    /**
     * Controller helpers
     *
     * @access public
     * @var array
     */
    public $helpers = array('DataAcl.DataAcl');

    /**
     * Controller components
     *
     * @access public
     * @var array
     */
    public $components = array(
        'Linkeddocs',
        'Password',
        'Auth' => array(
            'mapActions' => array(
                'allow' => array('login', 'logout', 'casLogin', 'casLogout', 'view', 'index', 'chooseCollForCas'),
                'loginRedirect' => array('controller' => 'environnement', 'action' => 'index'),
                'logoutRedirect' => array('controller' => 'users', 'action' => 'logout')
            ),
            'authenticate' => array('Form')
        ),
        'Rights',
        'Progress'
//        'AuthManager.AclManager'
    );

    /**
     * menu (interface graphique)
     *
     * @access private
     * @var array
     */
    private $_menu = array();

    /**
     * Conversion du nom de l'environnement
     *
     * @access private
     * @param string $env
     * @return string
     */
    private function _getEnvName($env) {
        $return = '';
        if ($env == 'admin') {
            $return = 'Administrateur';
        } else if ($env == 'disp') {
            $return = 'Aiguilleur';
        } else {
            $return = 'Utilisateur';
        }
        return $return;
    }

    /**
     * Génération du lien retour vers l'environnement
     *
     * @access private
     * @param type $env
     * @return string
     */
    private function _getEnvUrl($env) {
        $return = array('controller' => 'environnement', 'action' => 'index', 0);
        if ($env == 'admin') {
            $return[] = 'admin';
        } else if ($env == 'disp') {
            $return[] = 'disp';
        } else {
            $return[] = 'user';
        }
        return $return;
    }

    /**
     * Génération des éléments de base du menu
     *
     * @access private
     * @return void
     */
    private function _genBaseMenu() {
		$conn = $this->Session->read('Auth.User.connName');
        $userMainEnv = $this->Session->read('Auth.User.Env.main');
        $mainEnvName = $this->_getEnvName($userMainEnv);
        $environnements = array(
            'type' => 'menuHeader',
            'img' => 'menu_desktop.png',
            'txt' => __d('menu', 'Environnements'),
            'title' => __d('menu', 'Environnements'),
            'items' => array(
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_webgfc_about.png',
                    'txt' => '<i class="fa fa-heart" aria-hidden="true"></i> A propos de web-GFC',
                    'url' => array('plugin' => null, 'controller' => 'environnement', 'action' => 'infosLogiciel')
                ),
                array('type' => 'separator'),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_env_env.png',
                    'txt' => '<i class="fa fa-user" aria-hidden="true"></i> ' . $mainEnvName,
                    'url' => $this->_getEnvUrl($userMainEnv)
                )
            )
        );
        $envUniqueControl = array($mainEnvName);
        $secEnvs = array_unique($this->Session->read('Auth.User.Env.secondary'), SORT_REGULAR);
        foreach ($secEnvs as $env) {
            $envName = $this->_getEnvName($env);

            if (!in_array($envName, $envUniqueControl)) {
                $item = array(
                    'type' => 'menuItem',
                    'img' => 'menu_desktop.png',
                    'txt' => '<i class="fa fa-user" aria-hidden="true" aria-hidden="true"></i> ' . $envName,
                    'url' => $this->_getEnvUrl($env)
                );
                $envUniqueControl[] = $envName;
                $environnements['items'][] = $item;
            }
        }
        $listSystem = $environnements['items'];
        array_push(
            $listSystem,
            array(
                'type' => 'menuItem',
                'img' => 'menu_systeme_infosperso.png',
                'txt' => '<i class="fa fa-info" aria-hidden="true"  style="margin-left:3px"></i> ' . __d('menu', 'Informations personnelles'),
                'url' => array('plugin' => null, 'controller' => 'users', 'action' => 'setInfosPersos')
            ),
            array(
                'type' => 'menuItem',
                'img' => 'menu_systeme_infoscompte.png',
                'txt' => '<i class="fa fa-desktop" aria-hidden="true"></i> ' . __d('menu', 'Informations du compte'),
                'url' => array('plugin' => null, 'controller' => 'users', 'action' => 'getInfosCompte')
            ),
			array(
				'type' => 'menuItem',
				'img' => 'menu_systeme_infosperso.png',
				'txt' => '<i class="fa fa-user-secret" aria-hidden="true" style="margin-left:3px"></i> ' . __d('menu', 'Politique de confidentialité'),
				'url' => array('plugin' => null, 'controller' => 'rgpds', 'action' => 'policy', $conn)
			),
            array('type' => 'separator'),
            array(
                'type' => 'menuItem',
                'img' => 'menu_systeme_logout.png',
                'txt' => '<i class="fa fa-sign-out" aria-hidden="true"></i> ' . __d('menu', 'Deconnexion'),
                'url' => array('plugin' => null, 'controller' => 'users', 'action' => 'logout')
            )
        );
//$this->log($listSystem);
//        array_unique($listSystem, SORT_REGULAR);
//$this->log($listSystem);
        $this->_menu = array(
            'administration' => array(),
            'flux' => array(),
            'historique' => array(),
//            'gestion_mes_flux' => array(),
            'new_flux' => array(),
            'statistiques' => array(),
            'marche' => array(),
            'addressbook' => array(),
            'recherche' => array(),
            'systeme' => array(
                'type' => 'menuHeader',
                'img' => 'menu_systeme.png',
                'txt' => __d('menu', 'Mon compte'),
                'title' => __d('menu', 'Mon compte'),
                'items' => $listSystem
            )
        );
    }

    /**
     * Génération des éléments du menu pour l'envionnement "Superadmin"
     *
     * @access private
     * @return void
     */
    private function _genSuperadminMenu() {
		$conn = $this->Session->read('Auth.User.connName');
        $this->_menu = array(
            'administration' => array(
                'type' => 'menuHeader',
                'img' => 'menu_administration.png',
                'txt' => __d('menu', 'Administration'),
                'title' => __d('menu', 'Administration'),
                'items' => array(
                    array(
                        'type' => 'menuItem',
                        'img' => 'menu_administration_informations.png',
                        'txt' => __d('menu', 'gestionCollectivites'),
                        'url' => array('plugin' => null, 'controller' => 'collectivites', 'action' => 'index')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'menu_administration_check.png',
                        'txt' => __d('menu', 'checkInstall'),
                        'url' => array('plugin' => null, 'controller' => 'checks', 'action' => 'index')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'server.png',
                        'txt' => __d('menu', 'Authentification'),
                        'url' => array('plugin' => null, 'controller' => 'authentifications', 'action' => 'index')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'server.png',
                        'txt' => __d('menu', "Test d'envoi de mail"),
                        'url' => array('plugin' => null, 'controller' => 'checks', 'action' => 'get_test_mail')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'server.png',
                        'txt' => __d('menu', "Test d'impression"),
                        'url' => array('plugin' => null, 'controller' => 'checks', 'action' => 'testgedooo')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'server.png',
                        'txt' => __d('menu', "Console d'administration"),
                        'url' => array('plugin' => null, 'controller' => 'consoles', 'action' => 'index')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'server.png',
                        'txt' => __d('menu', "Configuration de la page d'accueil"),
                        'url' => array('plugin' => null, 'controller' => 'configurations', 'action' => 'config')
                    ),
					array(
						'type' => 'menuItem',
						'img' => 'server.png',
						'txt' => __d('menu', "Gestion des super-administrateurs"),
						'url' => array('plugin' => null, 'controller' => 'users', 'action' => 'index_superadmin')
					)
                )
            ),
            'systeme' => array(
                'type' => 'menuHeader',
                'img' => 'menu_systeme.png',
                'txt' => __d('menu', 'Mon compte'),
                'title' => __d('menu', 'Mon compte'),
                'items' => array(
                    array(
                        'type' => 'menuItem',
                        'img' => 'menu_systeme_infosperso.png',
                        'txt' => '<i class="fa fa-info" aria-hidden="true" style="margin-left:3px"></i> ' . __d('menu', 'Informations personnelles'),
                        'url' => array('plugin' => null, 'controller' => 'users', 'action' => 'setInfosPersos')
                    ),
//                    array(
//                        'type' => 'menuItem',
//                        'img' => 'menu_systeme_infoscompte.png',
//                        'txt' => __d('menu', 'Informations du compte'),
//                        'url' => array('plugin' => null, 'controller' => 'users', 'action' => 'getInfosCompte')
//                    ),
                    array('type' => 'separator'),
					array(
                        'type' => 'menuItem',
                        'img' => 'menu_systeme_logout.png',
                        'txt' => '<i class="fa fa-sign-out" aria-hidden="true"></i> ' . __d('menu', 'Deconnexion'),
                        'url' => array('plugin' => null, 'controller' => 'users', 'action' => 'logout')
                    ),
                )
            ),
        );
    }

    /**
     * Génération des éléments du menu pour l'envionnement "Administrateur"
     *
     * @access private
     * @return void
     */
    private function _genAdminMenu() {

        //TODO: intégrer le menu export pour l'admin

        $this->_menu['administration'] = array(
            'type' => 'menuHeader',
            'img' => 'menu_administration.png',
            'txt' => '<i class="fa fa-user" aria-hidden="true"></i>   <span class="text_menu hidden-md hidden-sm hidden-xs">' . __d('menu', 'Administration') . '</span>',
            'title' => __d('menu', 'Administration'),
            'items' => array(
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_informations.png',
                    'txt' => __d('default', 'Informations'),
                    'url' => array('plugin' => null, 'controller' => 'collectivites', 'action' => 'admin')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_organisation.png',
                    'txt' => __d('menu', 'structureCollectivite'),
                    'url' => array('plugin' => null, 'controller' => 'users', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_organisation.png',
                    'txt' => __d('menu', 'Connecteurs'),
                    'url' => array('plugin' => null, 'controller' => 'connecteurs', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_organisation.png',
                    'txt' => __d('menu', 'gestionScanemails'),
                    'url' => array('plugin' => null, 'controller' => 'scanemails', 'action' => 'index')
                ),
                array('type' => 'separator'),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_typessoustypes.png',
                    'txt' => __d('menu', 'gestionOriginesflux'),
                    'url' => array('plugin' => null, 'controller' => 'originesflux', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_typessoustypes.png',
                    'txt' => __d('menu', 'gestionTypesSoustypes'),
                    'url' => array('plugin' => null, 'controller' => 'soustypes', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'preferences-desktop-display.png',
                    'txt' => __d('menu', 'gestionBureaux'),
                    'url' => array('plugin' => null, 'controller' => 'desktopsmanagers', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_circuits.png',
                    'txt' => __d('menu', 'gestionCircuits'),
                    'url' => array('plugin' => null, 'controller' => 'workflows', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_meta.png',
                    'txt' => __d('menu', 'gestionMeta'),
                    'url' => array('plugin' => null, 'controller' => 'metadonnees', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_dossiersaffaires.png',
                    'txt' => __d('menu', 'gestionDossiersAffaires'),
                    'url' => array('plugin' => null, 'controller' => 'dossiers', 'action' => 'index')
                ),
                array('type' => 'separator'),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_titre.png',
                    'txt' => __d('menu', 'gestionTitre'),
                    'url' => array('plugin' => null, 'controller' => 'titres', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_addressbook.png',
                    'txt' => __d('menu', 'gestionAddressbooks'),
                    'url' => array('plugin' => null, 'controller' => 'addressbooks', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_organisation.png',
                    'txt' => __d('menu', 'Activites'),
                    'url' => array('plugin' => null, 'controller' => 'activites', 'action' => 'index')
                ),
				array(
					'type' => 'menuItem',
					'img' => 'menu_administration_organisation.png',
					'txt' => __d('menu', 'Gestion des fonctions de contacts'),
					'url' => array('plugin' => null, 'controller' => 'fonctions', 'action' => 'index')
				),
                array(
                    'type' => 'menuItem',
                    'img' => 'menu_administration_addressbook.png',
                    'txt' => __d('menu', 'gestionBans'),
                    'url' => array('plugin' => null, 'controller' => 'bans', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'email.png',
                    'txt' => __d('menu', 'templateMails'),
                    'url' => array('plugin' => null, 'controller' => 'templatemails', 'action' => 'index')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'email.png',
                    'txt' => __d('menu', 'templateNotifications'),
                    'url' => array('plugin' => null, 'controller' => 'templatenotifications', 'action' => 'index')
                )
            )
        );
        if( Configure::read('Conf.Gabarit') && !Configure::read('Conf.SAERP')) {
            $this->_menu['administration']['items'] = array_merge(
                $this->_menu['administration']['items'],
                array(
                    array(
                        'type' => 'menuItem',
                        'img' => 'kmenuedit.png',
                        'txt' => __d('menu', 'gabaritsdocuments'),
                        'title' => __d('menu', 'gabaritsdocuments'),
                        'url' => array('plugin' => null, 'controller' => 'gabaritsdocuments', 'action' => 'index')
                    )
                )
            );
        }

        if (Configure::read('CD') == 81 ) {
            $this->_menu['administration']['items'] = array_merge(
                $this->_menu['administration']['items'],
                array(
                    array(
                        'type' => 'menuItem',
                        'img' => 'kmenuedit.png',
                        'txt' => __d('menu', 'documents'),
                        'title' => __d('menu', 'documents'),
                        'url' => array('plugin' => null, 'controller' => 'documents', 'action' => 'index')
                    )
                )
            );
        }

		$this->_menu['administration']['items'] = array_merge($this->_menu['administration']['items'], array(
			array('type' => 'separator'),
			array(
				'type' => 'menuItem',
				'img' => 'kmenuedit.png',
				'txt' => __d('menu', "Outils pour l'administrateur"),
				'title' => __d('menu', "Outils pour l'administrateur"),
				'url' => array('plugin' => null, 'controller' => 'outils', 'action' => 'index')
			)
		));

        $this->_menu['statistiques'] = array(
            'type' => 'menuHeader',
            'img' => 'kchart_chrt.png',
            'title' => 'Statistiques',
            'txt' => '<i class="fa fa-bar-chart" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs">  ' . __d('menu', 'Statistiques') . '</span>',
            'items' => array(
                array(
                    'type' => 'menuItem',
                    'img' => 'kcmpartitions.png',
                    'txt' => __d('menu', 'statistiquesQuant'),
                    'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursQuant')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'kcmpartitions.png',
                    'txt' => __d('menu', 'statistiquesQual'),
                    'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursQual')
                ),
                array(
                    'type' => 'menuItem',
                    'img' => 'kcmpartitions.png',
                    'txt' => __d('menu', 'statistiquesGeo'),
                    'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursGeo')
                ),
				array(
					'type' => 'menuItem',
					'img' => 'kcmpartitions.png',
					'txt' => __d('menu', 'statistiquesAgents'),
					'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursAgents')
				)
            )
        );

//		if (Configure::read('CD') != 81 ) {
//			$this->_menu['statistiques']['items'] = array_merge(
//				$this->_menu['statistiques']['items'], array(
//					'items' => array(
//						'type' => 'menuItem',
//						'img' => 'icons/composition.png',
//						'txt' => __d('menu', 'statistiquesAgents'),
//						'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursAgents')
//					)
//				)
//			);
//		}

        if (!empty(Configure::read('Url.Stat'))) {
            if( Configure::read('Mode.Multicoll')) {
                $url = Configure::read('Url.Stat')."/".Configure::read('conn');
            }
            else {
                $url = Configure::read('Url.Stat');
            }
            $this->_menu['statistiques']['items'] = array_merge(
                    $this->_menu['statistiques']['items'], array(
                'items' => array(
                    'type' => 'menuItem',
                    'img' => 'icons/composition.png',
                    'txt' => 'Outils de statistiques externe',
                    'url' => $url,
                    'escape' => true
                )
                    )
            );
        }

        $this->_menu['addressbook'] = array(
            'type' => 'menuHeader',
            'img' => 'menu_env.png',
            'txt' => __d('menu', 'Carnet'),
            'title' => __d('menu', 'Carnet'),
            'withoutitems' => true,
            'url' => array('plugin' => null, 'controller' => 'addressbooks', 'action' => 'index')
        );

        // Ajout du menu recherche pour l'administrateur
        if ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Recherches')) {
            if (!Configure::read('Conf.SAERP')) {
                $this->_menu['recherche'] = array(
                    'type' => 'menuHeader',
                    'img' => 'menu_recherche.png',
                    'txt' => __d('menu', 'Recherche'),
                    'title' => __d('menu', 'Rechercherflux'),
                    'withoutitems' => true,
                    'url' => array('plugin' => null, 'controller' => 'recherches', 'action' => 'index')
                );
            } else {
                $this->_menu['recherche'] = array(
                    'type' => 'menuHeader',
                    'img' => 'menu_recherche.png',
                    'title' => __d('menu', 'Rechercherflux'),
                    'txt' => '<i class="fa fa-search" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('menu', 'Recherche') . '</span>',
                    'items' => array(
                        array(
                            'type' => 'menuItem',
                            'img' => 'menu_recherche_flux.png',
                            'txt' => __d('menu', 'rechercheFlux'),
                            'url' => array('plugin' => null, 'controller' => 'recherches', 'action' => 'index')
                        ),
                        array(
                            'type' => 'menuItem',
                            'img' => 'export.png',
                            'txt' => __d('menu', 'exportFlux'),
                            'url' => array('plugin' => null, 'controller' => 'recherches', 'action' => 'export')
                        )
                    )
                );
            }
        }
    }

    /**
     * Génération des éléments du menu pour l'envionnement "Utilisateur"
     *
     * @access private
     * @return void
     */
    private function _genUserMenu() {
        $this->_menu['flux'] = array(
            'type' => 'menuHeader',
            'img' => 'menu_env.png',
            'txt' => '<i class="fa fa-briefcase" aria-hidden="true"></i>  <span class="text_menu hidden-md hidden-sm hidden-xs">' . __d('menu', 'Courrier') . '</span>',
            'title' => __d('menu', 'Courrier'),
            'items' => array()
        );

        $this->_menu['historique'] = array(
            'type' => 'menuHeader',
            'img' => 'kchart_chrt.png',
            'txt' => '<i class="fa fa-history" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('menu', 'Historique') . '</span>',
            'title' => __d('menu', 'Historique'),
            'items' => array(
            )
        );

        if (Configure::read('Conf.SAERP')) {
            $this->_menu['marche'] = array(
                'type' => 'menuHeader',
                'img' => 'desktop.png',
                'title' => __d('menu', 'marcheSAERP'),
                'txt' => '<i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('menu', 'marcheSAERP') . '</span>',
                'items' => array(
                    array(
                        'type' => 'menuItem',
                        'img' => 'kcmpartitions.png',
                        'txt' => __d('menu', 'consultationSAERP'),
                        'url' => array('plugin' => null, 'controller' => 'consultations', 'action' => 'index')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'kcmpartitions.png',
                        'txt' => __d('menu', 'marcheSAERP'),
                        'url' => array('plugin' => null, 'controller' => 'marches', 'action' => 'index')
                    )
                )
            );
        }

        if ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Addressbooks')) {
            $this->_menu['addressbook'] = array(
                'type' => 'menuHeader',
                'img' => 'menu_env.png',
                'txt' => __d('menu', 'Carnet'),
                'withoutitems' => true,
                'title' => __d('menu', 'Carnet'),
                'url' => array('plugin' => null, 'controller' => 'addressbooks', 'action' => 'index')
            );
        }

        if ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Recherches')) {
            if (!Configure::read('Conf.SAERP')) {
                $this->_menu['recherche'] = array(
                    'type' => 'menuHeader',
                    'img' => 'menu_recherche.png',
                    'txt' => __d('menu', 'Recherche'),
                    'title' => __d('menu', 'Rechercherflux'),
                    'withoutitems' => true,
                    'url' => array('plugin' => null, 'controller' => 'recherches', 'action' => 'index')
                );
            } else {
                $this->_menu['recherche'] = array(
                    'type' => 'menuHeader',
                    'img' => 'menu_recherche.png',
                    'title' => __d('menu', 'Rechercherflux'),
                    'txt' => '<i class="fa fa-search" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('menu', 'Recherche') . '</span>',
                    'items' => array(
                        array(
                            'type' => 'menuItem',
                            'img' => 'menu_recherche_flux.png',
                            'txt' => __d('menu', 'rechercheFlux'),
                            'url' => array('plugin' => null, 'controller' => 'recherches', 'action' => 'index')
                        ),
                        array(
                            'type' => 'menuItem',
                            'img' => 'export.png',
                            'txt' => __d('menu', 'exportFlux'),
                            'url' => array('plugin' => null, 'controller' => 'recherches', 'action' => 'export')
                        )
                    )
                );
            }
        }

        if ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/add')) {
            $rolesUser = array();
            $userRoleSecondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
            foreach ($userRoleSecondaryDesktops as $userRoleSecondaryDesktop) {
                array_push($rolesUser, $userRoleSecondaryDesktop['Profil']['id']);
            }
            array_push($rolesUser, $this->Session->read('Auth.User.Desktop.Profil.id'));

			$this->loadModel('Connecteur');
			$dm = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%direct%',
						'Connecteur.use_dm' => true
					),
					'contain' => false
				)
			);
			$ds = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%simplifi%',
						'Connecteur.use_ds' => true
					),
					'contain' => false
				)
			);
            if (in_array(3, $rolesUser)) {
				if( Configure::read('Webservice.DS') || Configure::read('Webservice.Directmairie') ) {
					if (!empty($ds) && empty($dm)) {
						$this->_menu['new_flux'] = array(
							'type' => 'menuHeader',
							'img' => 'menu_recherche.png',
							'txt' => '<i class="fa fa-plus" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('courrier', 'New Courrier') . '</span>',
							'title' => __d('courrier', 'New Courrier'),
							'items' => array(
								array(
									'type' => 'menuItem',
									'img' => 'kcmpartitions.png',
									'txt' => __d('courrier', 'New Courrier'),
									'url' => "#",
									'action' => array("data-toggle" => "modal", 'data-target' => "#myModal")
								),
								array(
									'type' => 'menuItem',
									'img' => 'kcmpartitions.png',
									'txt' => __d('menu', 'Créer depuis un dossier issu de Démarches Simplifiées'),
									'url' => array('plugin' => null, 'controller' => 'demarchessimplifiees', 'action' => 'index')
								)
							)
						);
					} else if (empty($ds) && !empty($dm)) {
						$this->_menu['new_flux'] = array(
							'type' => 'menuHeader',
							'img' => 'menu_recherche.png',
							'txt' => '<i class="fa fa-plus" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('courrier', 'New Courrier') . '</span>',
							'title' => __d('courrier', 'New Courrier'),
							'items' => array(
								array(
									'type' => 'menuItem',
									'img' => 'kcmpartitions.png',
									'txt' => __d('courrier', 'New Courrier'),
									'url' => "#",
									'action' => array("data-toggle" => "modal", 'data-target' => "#myModal")
								),
								array(
									'type' => 'menuItem',
									'img' => 'kcmpartitions.png',
									'txt' => __d('menu', 'Créer depuis un dossier issu de Direct-Mairie'),
									'url' => array('plugin' => null, 'controller' => 'directmairies', 'action' => 'index')
								)
							)
						);
					} else if (!empty($ds) && !empty($dm)) {
						$this->_menu['new_flux'] = array(
							'type' => 'menuHeader',
							'img' => 'menu_recherche.png',
							'txt' => '<i class="fa fa-plus" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('courrier', 'New Courrier') . '</span>',
							'title' => __d('courrier', 'New Courrier'),
							'items' => array(
								array(
									'type' => 'menuItem',
									'img' => 'kcmpartitions.png',
									'txt' => __d('courrier', 'New Courrier'),
									'url' => "#",
									'action' => array("data-toggle" => "modal", 'data-target' => "#myModal")
								),
								array(
									'type' => 'menuItem',
									'img' => 'kcmpartitions.png',
									'txt' => __d('menu', 'Créer depuis un dossier issu de Démarches Simplifiées'),
									'url' => array('plugin' => null, 'controller' => 'demarchessimplifiees', 'action' => 'index')
								),
								array(
									'type' => 'menuItem',
									'img' => 'kcmpartitions.png',
									'txt' => __d('menu', 'Créer depuis un dossier issu de Direct-Mairie'),
									'url' => array('plugin' => null, 'controller' => 'directmairies', 'action' => 'index')
								)
							)
						);
					} else {
						$this->_menu['new_flux'] = array(
							'type' => 'menuHeader',
							'img' => 'menu_recherche.png',
							'txt' => __d('courrier', 'New Courrier'),
							'title' => __d('courrier', 'New Courrier'),
							'withoutitems' => true,
							'url' => "#",
							'action' => array("data-toggle" => "modal", 'data-target' => "#myModal")
						);
					}
				}
				else {
					$this->_menu['new_flux'] = array(
						'type' => 'menuHeader',
						'img' => 'menu_recherche.png',
						'txt' => __d('courrier', 'New Courrier'),
						'title' => __d('courrier', 'New Courrier'),
						'withoutitems' => true,
						'url' => "#",
						'action' => array("data-toggle" => "modal", 'data-target' => "#myModal")
					);
				}
            }
        }

        if ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Statistiques')) {
            $this->_menu['statistiques'] = array(
                'type' => 'menuHeader',
                'img' => 'kchart_chrt.png',
                'txt' => '<i class="fa fa-bar-chart" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('menu', 'Statistiques') . '</span>',
                'title' => __d('menu', 'Statistiques'),
                'items' => array(
                    array(
                        'type' => 'menuItem',
                        'img' => 'kcmpartitions.png',
                        'txt' => __d('menu', 'statistiquesQuant'),
                        'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursQuant')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'kcmpartitions.png',
                        'txt' => __d('menu', 'statistiquesQual'),
                        'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursQual')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'kcmpartitions.png',
                        'txt' => __d('menu', 'statistiquesGeo'),
                        'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursGeo')
                    ),
					array(
						'type' => 'menuItem',
						'img' => 'kcmpartitions.png',
						'txt' => __d('menu', 'statistiquesAgents'),
						'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursAgents')
					)
                )
            );

//			if (Configure::read('CD') != 81 ) {
//                $this->_menu['statistiques']['items'] = array_merge(
//                    $this->_menu['statistiques']['items'], array(
//                        'items' => array(
//                            'type' => 'menuItem',
//                            'img' => 'icons/composition.png',
//                            'txt' => __d('menu', 'statistiquesAgents'),
//                            'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursAgents')
//                        )
//                    )
//				);
//			}

            if (!empty(Configure::read('Url.Stat'))) {
                if( Configure::read('Mode.Multicoll')) {
                    $url = Configure::read('Url.Stat')."/".Configure::read('conn');
                }
                else {
                    $url = Configure::read('Url.Stat');
                }
                $this->_menu['statistiques']['items'] = array_merge(
                        $this->_menu['statistiques']['items'], array(
                    'items' => array(
                        'type' => 'menuItem',
                        'img' => 'icons/composition.png',
                        'txt' => 'Outils de statistiques externe',
						'title' => 'Outils de statistiques externe',
                        'url' => $url,
                        'escape' => true
                    )
                        )
                );
            }
        }
    }

    /**
     * Génération des éléments du menu pour l'envionnement "Aiguilleur" (Dispatcher)
     *
     * @access private
     * @return void
     */
    private function _genDispMenu() {
        if ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Recherches')) {
            /*
              $this->_menu['gestion_mes_flux'] = array(
              'type' => 'menuHeader',
              'img' => 'menu_env.png',
              'txt' => '<i class="fa fa-gavel" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('menu', 'Gestion') . '</span>',
              'items' => array(
              array(
              'type' => 'menuItem',
              'img' => 'menu_env_env_repertoires.png',
              'txt' => __d('menu', 'Mes Repertoires'),
              'url' => array('plugin' => null, 'controller' => 'repertoires', 'action' => 'index')
              )
              )
              );
             *
             */
            if (!Configure::read('Conf.SAERP')) {
                $this->_menu['recherche'] = array(
                    'type' => 'menuHeader',
                    'img' => 'menu_recherche.png',
                    'txt' => __d('menu', 'Recherche'),
                    'title' => __d('menu', 'Rechercherflux'),
                    'withoutitems' => true,
                    'url' => array('plugin' => null, 'controller' => 'recherches', 'action' => 'index')
                );
            } else {
                $this->_menu['recherche'] = array(
                    'type' => 'menuHeader',
                    'img' => 'menu_recherche.png',
                    'title' => __d('menu', 'Rechercherflux'),
                    'txt' => '<i class="fa fa-search" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('menu', 'Recherche') . '</span>',
                    'items' => array(
                        array(
                            'type' => 'menuItem',
                            'img' => 'menu_recherche_flux.png',
                            'txt' => __d('menu', 'rechercheFlux'),
                            'url' => array('plugin' => null, 'controller' => 'recherches', 'action' => 'index')
                        ),
                        array(
                            'type' => 'menuItem',
                            'img' => 'export.png',
                            'txt' => __d('menu', 'exportFlux'),
                            'url' => array('plugin' => null, 'controller' => 'recherches', 'action' => 'export')
                        )
                    )
                );
            }
            $this->_menu['historique'] = array(
                'type' => 'menuHeader',
                'img' => 'kchart_chrt.png',
                'txt' => '<i class="fa fa-history" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs">' . __d('menu', 'Historique') . '</span>',
                'title' => __d('menu', 'Historique' ),
                'items' => array(
                )
            );

            $this->_menu['flux'] = array(
                'type' => 'menuHeader',
                'img' => 'menu_env.png',
                'txt' => '<i class="fa fa-briefcase" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs">' . __d('menu', 'Courrier') . '</span>',
                'title' => __d('menu', 'Courrier'),
                'items' => array(
                )
            );
            if ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Addressbooks')) {
                $this->_menu['addressbook'] = array(
                    'type' => 'menuHeader',
                    'img' => 'menu_env.png',
                    'txt' => __d('menu', 'Carnet'),
                    'withoutitems' => true,
//                     'txt' => '<i class="fa fa-book" aria-hidden="true"></i>  <span class="text_menu hidden-md hidden-sm hidden-xs"> ' . __d('menu', 'Carnet') . '</span>',
                    'title' => __d('menu', 'Carnet'),
                    'url' => array('plugin' => null, 'controller' => 'addressbooks', 'action' => 'index')
                );

            }
            $this->_menu['statistiques'] = array(
                'type' => 'menuHeader',
                'img' => 'kchart_chrt.png',
                'txt' => '<i class="fa fa-bar-chart" aria-hidden="true"></i> <span class="text_menu hidden-md hidden-sm hidden-xs">' . __d('menu', 'Statistiques') . '</span>',
				'title' => 'Statistiques',
                'items' => array(
                    array(
                        'type' => 'menuItem',
                        'img' => 'kcmpartitions.png',
                        'txt' => __d('menu', 'statistiquesQuant'),
                        'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursQuant')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'kcmpartitions.png',
                        'txt' => __d('menu', 'statistiquesQual'),
                        'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursQual')
                    ),
                    array(
                        'type' => 'menuItem',
                        'img' => 'kcmpartitions.png',
                        'txt' => __d('menu', 'statistiquesGeo'),
                        'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursGeo')
                    ),
					array(
						'type' => 'menuItem',
						'img' => 'kcmpartitions.png',
						'txt' => __d('menu', 'statistiquesAgents'),
						'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursAgents')
					)
                )
            );

//			if (Configure::read('CD') != 81 ) {
//				$this->_menu['statistiques']['items'] = array_merge(
//					$this->_menu['statistiques']['items'], array(
//						'items' => array(
//							'type' => 'menuItem',
//							'img' => 'icons/composition.png',
//							'txt' => __d('menu', 'statistiquesAgents'),
//							'url' => array('plugin' => null, 'controller' => 'statistiques', 'action' => 'indicateursAgents')
//						)
//					)
//				);
//			}


            if (!empty(Configure::read('Url.Stat'))) {
                if( Configure::read('Mode.Multicoll')) {
                    $url = Configure::read('Url.Stat')."/".Configure::read('conn');
                }
                else {
                    $url = Configure::read('Url.Stat');
                }
                $this->_menu['statistiques']['items'] = array_merge(
                        $this->_menu['statistiques']['items'], array(
                    'items' => array(
                        'type' => 'menuItem',
                        'img' => 'icons/composition.png',
                        'txt' => 'Outils de statistiques externe',
                        'title' => 'Outils de statistiques externe',
                        'url' => $url,
                        'escape' => true
                    )
                        )
                );
            }
        }
    }

    /**
     * Nettoyage des éléemnts du menu
     *
     * @access private
     * @return void
     */
    private function _cleanMenu() {
        foreach ($this->_menu as $kMenu => $menu) {
            if (empty($menu)) {
                unset($this->_menu[$kMenu]);
            }
        }
    }

    /**
     * Génération des éléments du menu
     *
     * @access private
     * @return void
     */
    private function _genMenu() {
        if (Configure::read('conn') == 'default') {
            $this->_genSuperadminMenu();
            $setMenus['superadmin'] = true;
        } else {
            $this->_genBaseMenu();
            $envs = array_unique(array_merge(array($this->Session->read('Auth.User.Env.main')), array_unique($this->Session->read('Auth.User.Env.secondary'), SORT_REGULAR)), SORT_REGULAR);
            $setMenus = array(
                'admin' => false,
                'disp' => false,
                'user' => false
            );
            if (in_array('admin', $envs)) {
                if (!$setMenus['admin']) {
                    $this->_genAdminMenu();
                    $setMenus['admin'] = true;
                }
            }
            if (in_array('disp', $envs)) {
                if (!$setMenus['disp']) {
                    $this->_genDispMenu();
                    $setMenus['disp'] = true;
                }
            }
            if (in_array('init', $envs)) {
                if (!$setMenus['user']) {
                    $this->_genUserMenu();
                    $setMenus['user'] = true;
                }
            }
            if (in_array('val', $envs)) {
                if (!$setMenus['user']) {
                    $this->_genUserMenu();
                    $setMenus['user'] = true;
                }
            }
            if (in_array('valedit', $envs)) {
                if (!$setMenus['user']) {
                    $this->_genUserMenu();
                    $setMenus['user'] = true;
                }
            }
            if (in_array('arch', $envs)) {
                if (!$setMenus['user']) {
                    $this->_genUserMenu();
                    $setMenus['user'] = true;
                }
            }
            if (in_array('appdefined', $envs)) {
                if (!$setMenus['user']) {
                    $this->_genUserMenu();
                    $setMenus['user'] = true;
                }
            }
        }
        $this->_cleanMenu();
        $this->Session->write('Auth.User.menu', $this->_menu);
    }

    /**
     * CakePHP callback
     *
     * configuration du composant Auth dans le cas d'un superadmin
     *
     * @access public
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('login', 'logout', 'mdplost', 'exportcsv', 'casLogin', 'casLogout', 'chooseCollForCas', 'logincasafterchoosingcoll', 'habilPerson'));
        if (Configure::read('conn') == "default") {
            $this->Auth->allow('*');
        }
    }

    /**
     * Modification de l'organisation d'une collectivité (interface graphique)
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function index() {
//        $this->set('ariane', array(
//            '<a href="environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
//            __d('menu', 'structureCollectivite', true)
//        ));

        $this->set('nbUsers', $this->User->getNbUsers());
        $this->set('nbServices', $this->User->Desktop->Service->getNbServices());
        $this->set('nbProfils', $this->User->Desktop->Profil->getNbProfils());
    }

    /**
     * Récupétration de la liste des utilisateurs (ajax)
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function getUsers($cancel = null) {
        // On doit pouvoir obtenir les résultats dès le premier accès à la page
//        if (!isset($this->request->data['User'])) {
//            $this->request->data = Set::merge(
//                            $this->Session->read('paginationModel.rechercherUser'), array('User' => array('active' => true))
//            );
//            if ($cancel != null) {
//                $this->request->data = array('User' => array('active' => true));
//            }
//        }
        $this->Session->write('paginationModel.rechercherUser', $this->request->data);
        $queryData = $this->User->search($this->request->data);

        //Ce serait bien que l'ordre par défaut soit l'odre alpha des Noms (et non des identifiants)
        $queryData['order'] = 'User.nom';
		$active = true;
		if( isset($this->request->data) && !empty($this->request->data) ) {
			if( $this->request->data['User']['active'] == '0' ) {
				$active = false;
			}
		}
		$queryData['conditions'][] = array('User.active' => $active );


        $nbUsers = $this->User->find('count', $queryData);

//        $this->paginate = $queryData;


        $users = array();
        $users = $this->User->find('all', $queryData);
//debug($users);
        foreach ($users as $key => $user) {
            // Permissions
            $user['right_edit'] = true;
            $user['right_delete'] = true;

            // Services liés au Desktop et au SecondaryDesktop
            $query = array(
                'fields' => array(
                    'DesktopsUser.id',
                    'DesktopsUser.user_id',
                    'DesktopsUser.desktop_id',
                    'DesktopsService.service_id',
                    'DesktopsService.id',
                    'DesktopsService.desktop_id',
                    'SecondaryDesktop.id',
                    'SecondaryDesktop.profil_id',
                ),
                'contain' => false,
                'joins' => array(
                    $this->User->join('DesktopsUser', array('type' => 'INNER')),
                    alias_querydata(
                            $this->User->DesktopsUser->join('Desktop', array('type' => 'INNER')), array('Desktop' => 'SecondaryDesktop')
                    ),
                    alias_querydata(
                            $this->User->DesktopsUser->Desktop->join('DesktopsService', array('type' => 'LEFT OUTER')), array('Desktop' => 'SecondaryDesktop')
                    ),
                ),
                'conditions' => array(
                    'User.id' => $user['User']['id']
                )
            );
            $results = $this->User->find('all', $query);


            // Ajout des services associés à l'user dans le tableau de résultats
            $user['User']['services'] = array();
            $services_ids = array_unique(
                    array_merge(
                            array_keys($this->User->Desktop->getServicesList($user['Desktop']['id'])), Hash::extract($results, '{n}.DesktopsService.service_id')
                    ), SORT_REGULAR
            );
            $user['Service'] = array();
            foreach ($services_ids as $service_id) {
                if (!empty($service_id)) {
                    $user['Service'][] = array('id' => $service_id);
                }
            }
            $servicesNames = array();
            if (!empty($user['Service'])) {
                foreach ($user['Service'] as $s => $servicesIds) {
                    $servicesNames[] = $this->User->Desktop->Service->find(
                            'first', array(
                        'fields' => array(
                            'Service.name'
                        ),
                        'conditions' => array(
                            'Service.id' => $servicesIds
                        ),
                        'recursive' => -1
                            )
                    );
                }
            }
            $services_names = array_unique(
                    array_merge(
                            array_values($this->User->Desktop->getServicesList($user['Desktop']['id'])), Hash::extract($servicesNames, '{n}.Service.name')
                    ), SORT_REGULAR
            );
//debug($services_names);
            foreach ($services_names as $serviceName) {
                $user['User']['services'][] = implode(' / ', array($serviceName));
            }
            if (!empty($user['User']['services'])) {
                $user['User']['services'] = implode(' / ', $user['User']['services']);
            } else {
                $user['User']['services'] = null;
            }

            // Ajout des profils associés à l'user dans le tableau de résultats
            $user['User']['profils'] = array();
            $groups_ids = array_unique(
                    array_merge(
                            array($user['Profil']['id']), Hash::extract($results, '{n}.SecondaryDesktop.profil_id')
                    ), SORT_REGULAR
            );
            $user['Profil'] = array();
            foreach ($groups_ids as $group_id) {
                $user['Profil'][] = array('id' => $group_id);
            }

            $secondary_desktops_ids = array_unique(Hash::extract($results, '{n}.SecondaryDesktop.id'), SORT_REGULAR);
            $user['SecondaryDesktop'] = array();
            foreach ($secondary_desktops_ids as $secondary_desktop_id) {
                if (!empty($secondary_desktop_id)) {
                    $user['SecondaryDesktop'][] = array('id' => $secondary_desktop_id);
                }
            }
            $groups_ids = Hash::extract($user, 'Profil.{n}.id');
            foreach ($groups_ids as $i => $groupId) {
                $groupName = $this->User->Desktop->Profil->find(
                        'first', array(
                    'fields' => array(
                        'Profil.name'
                    ),
                    'conditions' => array(
                        'Profil.id' => $groupId
                    ),
                    'recursive' => -1
                        )
                );
                $user['User']['profils'][] = Hash::get($groupName, 'Profil.name');
            }
            $user['User']['profils'] = implode(' / ', $user['User']['profils']);
//debug($user);
            // On complète le résultat
            $users[$key] = $user;
        }
//debug( $users );
        $this->set('groups', $this->User->Desktop->Profil->find(
                        'list', array(
                    'conditions' => array(
                        'Profil.id' => array(ADMIN_GID, DISP_GID, INIT_GID, VALEDIT_GID, VAL_GID, ARCH_GID)
                    ),
                    'order' => 'Profil.name ASC'
                        )
                )
        );
        $this->set('services', $this->User->Desktop->Service->find('list', array('order' => 'Service.name ASC')));
        $this->set('users', $users);
        $this->set('nbusers', $nbUsers);

        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Ajout d'un utilisateur
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $this->User->begin();
            $this->Jsonmsg->init();

            $desktop = array('Desktop' => $this->request->data['Desktop'], 'Service' => array());
            $this->User->Desktop->create();

            if ($this->User->Desktop->save($desktop)) {
                $dktpId = $this->User->Desktop->id;

                // A la création de l'utilisateur, on lui crée son bureau
                $desktopmanager['Desktopmanager']['name'] = 'Bureau ' . $desktop['Desktop']['name'];
                $desktopmanager['Desktopmanager']['isautocreated'] = true;
                $this->User->Desktop->Desktopmanager->create();
                $newDesktopmanager = $this->User->Desktop->Desktopmanager->save($desktopmanager);
                // A la création de l'utilisateur, une fois le bureau créé on l'associe au rôle défini
                if ($newDesktopmanager) {
                    $dktpmngId = $this->User->Desktop->Desktopmanager->id;
                    $dIddMId['DesktopDesktopmanager']['desktop_id'] = $dktpId;
                    $dIddMId['DesktopDesktopmanager']['desktopmanager_id'] = $dktpmngId;
                    $this->User->Desktop->DesktopDesktopmanager->create();
                    $this->User->Desktop->DesktopDesktopmanager->save($dIddMId);
                }
                //

                $user = array('User' => $this->request->data['User']);
                $user['User']['desktop_id'] = $dktpId;
				$user['User']['password'] = Security::hash($this->request->data['User']['password'], 'sha256', true);
                $this->User->create($user);
                if ($this->User->save()) {
                    $saveRights = $this->setRights($dktpId);
//debug($saveRights);
                    if ($saveRights) {
                        $this->Jsonmsg->valid();
                    }
                }
            }

            if ($this->Jsonmsg->json['success']) {
                $this->User->commit();
                $this->Jsonmsg->valid();
            } else {
                $this->User->rollback();
            }
            $this->Jsonmsg->send();
        } else {
            $this->set('groups', $this->User->Desktop->Profil->find('list', array('conditions' => array('id >' => 2, 'active' => true))));
            $this->set('services', $this->User->Desktop->Service->generateTreeList(array(), null, null, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
            $this->set('notif', array('1' => 'oui', '0' => 'non'));
            $typeabonnement = array(
                'quotidien' => 'Résumé journalier',
                'event' => 'Envoi à chaque événement'
            );
            $this->set(compact('typeabonnement'));
        }
    }

    /**
     * Validation d'unicité du nom d'une collectivité (création / édition)
     *
     * @param type $field
     */
    public function ajaxformvalid($field = null, $collectiviteId = 0) {

        //On vérifie que le nom de bureau n'est pas un nom de groupe en lecture seule (Initiateur, Valideur, ...)
        if ($field == 'desktopname') {
            if (in_array($this->request->data['Desktop']['name'], $this->User->Desktop->Profil->find('list', array('conditions' => array('Profil.readonly' => true), 'fields' => array('id', 'name'))))) {
                $this->autoRender = false;
                $content = json_encode(array('Desktop' => array('name' => 'Valeur déjà utilisée')));
                header('Pragma: no-cache');
                header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
                header('Content-Type: text/x-json');
                header("X-JSON: {$content}");
                Configure::write('debug', 0);
                echo $content;
                return;
            }
        }
        //Vérifie que le nom de user est unique
        else if ($field == 'username') {
            if (in_array($this->request->data['User']['username'], $this->User->find('list', array('fields' => array('id', 'username'))))) {
                $this->autoRender = false;
                $content = json_encode(array('User' => array('username' => 'Valeur déjà utilisée')));
                header('Pragma: no-cache');
                header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
                header('Content-Type: text/x-json');
                header("X-JSON: {$content}");
                Configure::write('debug', 0);
                echo $content;
                return;
            }
            if (strpos($this->request->data['User']['username'], '@')) {
                $this->autoRender = false;
                $content = json_encode(array('User' => array('username' => "L'identifiant ne doit pas posséder de @")));
                header('Pragma: no-cache');
                header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
                header('Content-Type: text/x-json');
                header("X-JSON: {$content}");
                Configure::write('debug', 0);
                echo $content;
                return;
            }
        }
		else if ($field == 'password2') {
			if (!$this->User->validatesPassword($this->request->data) ) {
				$this->autoRender = false;
				$content = json_encode(array('User' => array('password2' => 'Veuillez vérifier le mot de passe')));
				header('Pragma: no-cache');
				header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
				header('Content-Type: text/x-json');
				header("X-JSON: {$content}");
				Configure::write('debug', 0);
				echo $content;
				return;
			}
		}

        $models = array();
        if ($field != null) {
            if ($field == 'desktopname') {
                $models['Desktop'] = array('name');
            } else {
                $models['User'] = array($field);
            }
        } else {
            $models[] = 'User';
        }

        $rd = array();
        foreach ($this->request->data as $k => $v) {
            if (($field != 'desktopname' && $k == 'User') || ($field == 'desktopname' && $k == 'Desktop')) {
                $rd[$k] = $v;
            }
        }
//		$this->Ajaxformvalid->valid($models, $this->request->data, $collectiviteId);
        $this->Ajaxformvalid->valid($models, $rd, $collectiviteId);
    }

//	public function checkUserName() {
//		if (empty($this->request->data)) {
//			throw new BadMethodCallException();
//		}
//
//		$this->Jsonmsg->init(__d('user', 'User.already_exists'));
//		$userExists = $this->User->find('first', array('conditions' => array('User.username' => $this->request->data['User']['username'])));
//		if (empty($userExists)) {
//			$this->Jsonmsg->valid(__d('user', 'User.not_exists'));
//		}
//		$this->Jsonmsg->send();
//	}

    /**
     * Edition d'un utilisateur (interface graphique)
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un utilisateur
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();

            $success = true;
//            $this->User->create($this->request->data);
            $this->User->begin();


            //Liste des desktops de l'utilsiateur
            $desktops = $this->User->DesktopsUser->find(
                    'all', array(
                'fields' => array(
                    'Desktop.id'
                ),
                'conditions' => array(
                    'DesktopsUser.user_id' => $id
                ),
                'contain' => array(
                    'Desktop'
                )
                    )
            );
            $user = $this->User->find('first', array('conditions' => array('User.id' => $id), 'contain' => false, 'fields' => array('User.desktop_id')));
            $desktopUserId = Hash::get($user, 'User.desktop_id');
            $userDesktopsId = array_values(Hash::flatten(Hash::extract($desktops, '{n}.Desktop.id')));
            $userDesktopsId[] = $desktopUserId;

            if ($this->request->data['User']['active'] == '0') {
                $success = $this->User->Desktop->updateAll(
					array('Desktop.active' => false),
					array('Desktop.id' => $userDesktopsId)
				) && $success;

				// On supprime le(s) profil(s) de(s) bureau(x) dans le(s)quel(s) il(s) se trouve(nt)
				$desktopsmanagers = $this->User->Desktop->DesktopDesktopmanager->find(
					'all', array(
						'conditions' => array(
							'DesktopDesktopmanager.desktop_id' => $userDesktopsId
						),
						'contain' => false,
						'recursive' => -1
					)
				);
				if( !empty($desktopsmanagers) ) {
					foreach ($desktopsmanagers as $desktopdesktopmanager) {
						$this->User->Desktop->DesktopDesktopmanager->delete($desktopdesktopmanager['DesktopDesktopmanager']['id']);

						// On désactive également les bureaux unitaires créés automatiquement quand on crée un utilisateur
						$bureaux = $this->User->Desktop->Desktopmanager->find(
							'all', array(
								'conditions' => array(
									'Desktopmanager.id' => $desktopdesktopmanager['DesktopDesktopmanager']['desktopmanager_id'],
									'Desktopmanager.isautocreated' => true
								),
								'contain' => false,
								'recursive' => -1
							)
						);
						if (!empty($bureaux)) {
							foreach ($bureaux as $bureau) {
								$this->User->Desktop->Desktopmanager->updateAll(
									array('Desktopmanager.active' => false),
									array('Desktopmanager.id' => $bureau['Desktopmanager']['id'])
								);
							}
						}
					}
				}
            } else if ($this->request->data['User']['active'] == '1') {
                $success = $this->User->Desktop->updateAll(
					array('Desktop.active' => true),
					array('Desktop.id' => $userDesktopsId)
				) && $success;
            }

            if ($success && $this->User->save($this->request->data)) {
                $this->User->commit();
                $this->Jsonmsg->valid();
            } else {
                $this->User->rollback();
            }
            $this->Jsonmsg->send();


            $qd = array(
                'conditions' => array(
                    'User.id' => $id
                ),
                'contain' => false
            );
            $user = $this->User->find('first', $qd);
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $journalmsg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié l'utilisateur ".$user['User']['username']." le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $journalmsg, 'info', $flux);
        } else if ($id != null) {

            $delegatedDesktops = $this->User->getDelegatedDesktops($id);
            $this->set('delegatedDesktops', $delegatedDesktops);
            $delegateToDesktops = $this->User->getDelegateToDesktops($id);
            $this->set('delegateToDesktops', array_keys($delegateToDesktops));

            $qd = array(
                'conditions' => array(
                    'User.id' => $id
                ),
                'contain' => array(
                    $this->User->Desktop->alias,
                    $this->User->SecondaryDesktop->alias => array(
                        'order' => array(
                            'DesktopsUser.delegation'
                        )
                    )
                )
            );
            $user = $this->User->find('first', $qd);
            $this->request->data = $user;
            $this->set('user', $user);
        }
    }

    /**
     * Edition d'un utilisateur (interface graphique)
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un utilisateur
     * @return void
     */
    //TODO: supprimer les fonctions commentées
//	public function setInfos($id = null) {
//		if ($id != null) {
//			if (!empty($this->request->data)) {
//				$this->User->create($this->request->data);
//				$this->User->save();
//			}
//			$this->request->data = $this->User->find('first', array('conditions' => array('User.id' => $id), 'contain' => array($this->User->Service->alias)));
//			$this->set('user', $this->request->data);
//
//			$this->set('groups', $this->User->Profil->find('list', array('conditions' => array('id <>' => 1))));
//			$this->set('services', $this->User->Service->generateTreeList(array(), null, null, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
//			$this->_selectedArray($this->request->data['Service']);
//		}
//	}

    /**
     * Suppression d'un utilisateur (soft delete), désactivation de ses bureaux, suppression des délégations des bureaux de l'utilsateur vers d'autres utilisateurs, suppression des délégations des bureaux vers l'utilisateur supprimé
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un utilisateur
     * @throws NotFoundException
     * @return void
     */
    public function delete($id = null) {
        $user = $this->User->find('first', array('fields' => array('User.id', 'User.active', 'User.username', 'User.mail'), 'recursive' => -1, 'conditions' => array('User.id' => $id)));
        if (empty($user)) {
            throw new NotFoundException();
        }
        $this->User->begin();
        $this->Jsonmsg->init();
        $valid = array();
        //desactivation de l utilisateur
        $user['User']['active'] = false;

        //TODO: verifier plus finement les username disponible
        $user['User']['username'] .= "_" . $user['User']['id']; // renommage de l'utilisateur pour ne pas avoir de conflit si on créer un utilisateur du meme nom

        $ancienMail = explode("@", $user['User']['mail']); // renommage de l'adresse mail pour ne pas avoir de conflit si on recrée l'utilisateur par la suite
        $user['User']['mail'] = $ancienMail[0] . "_" . $user['User']['id'] . "@" . $ancienMail[1];

        $this->User->create($user);
        if ($this->User->save()) {
            $valid['user'] = true;
        } else {
            $valid['user'] = false;
        }
        //desactivation des bureaux de l'utilisateur
        $validDesktops = array();

        $desktopsIds = $this->User->getDesktops($id);
		// On supprime le(s) profil(s) de(s) bureau(x) dans le(s)quel(s) il(s) se trouve(nt)
		$desktopsmanagers = $this->User->Desktop->DesktopDesktopmanager->find(
			'all', array(
				'conditions' => array(
					'DesktopDesktopmanager.desktop_id' => $desktopsIds
				),
				'contain' => false,
				'recursive' => -1
			)
		);
		foreach ($desktopsmanagers as $dmId => $desktopdesktopmanager) {
			$this->User->Desktop->DesktopDesktopmanager->delete($desktopdesktopmanager['DesktopDesktopmanager']['id']);
		}

		// On supprime le(s) profil(s) de(s) service(x) dans le(s)quel(s) il(s) se trouve(nt)
		$desktopsService = $this->User->Desktop->DesktopsService->find(
                'all', array(
            'conditions' => array(
                'DesktopsService.desktop_id' => $desktopsIds
            )
                )
        );
        foreach ($desktopsService as $dSId => $service) {
            $this->User->Desktop->DesktopsService->delete($service['DesktopsService']['id']);
        }

        foreach ($desktopsIds as $desktopId) {
            $desktop = $this->User->Desktop->find('first', array('recursive' => -1, 'fields' => array('Desktop.id', 'Desktop.active', 'Desktop.name'), 'conditions' => array('Desktop.id' => $desktopId)));
            $desktop['Desktop']['active'] = false;
            $desktop['Desktop']['name'] .= "_" . $user['User']['id'];
//debug($desktop);
//die();
            $this->User->Desktop->create($desktop);
            if ($this->User->Desktop->save()) {
                $validDesktops[] = true;
            } else {
                $validDesktops[] = false;
            }
        }


        $valid['desktops'] = !in_array(false, $validDesktops, true);
        //suppression des délégations vers d autres utilisateurs
        $qdDesktops = array(
            'fields' => array(
                'DesktopsUser.id',
                'DesktopsUser.desktop_id'
            ),
            'conditions' => array(
                'DesktopsUser.user_id' => $id,
                'DesktopsUser.delegation' => false
            )
        );
        $desktops = $this->User->DesktopsUser->find('list', $qdDesktops);
        $validDelegations = array();
        $qdDelegations = array(
            'conditions' => array(
                'DesktopsUser.user_id <>' => $id,
                'DesktopsUser.desktop_id ' => $desktops,
                'DesktopsUser.delegation' => true
            )
        );
        $delegations = $this->User->DesktopsUser->find('all', $qdDelegations);
        foreach ($delegations as $delegation) {
            $validDelegations[] = $this->User->DesktopsUser->delete($delegation['DesktopsUser']['id']);
        }
        $valid['delegations'] = !in_array(false, $validDelegations, true);
        //suppression des délégations vers l utilisateur a desactiver
        $validDelegated = array();
        $delegated = $this->User->DesktopsUser->find('all', array('conditions' => array('DesktopsUser.user_id' => $id, 'DesktopsUser.delegation' => true)));
        foreach ($delegated as $delegation) {
            $validDelegated[] = $this->User->DesktopsUser->delete($delegation['DesktopsUser']['id']);
        }
        $valid['delegated'] = !in_array(false, $validDelegated, true);


        if (!in_array(false, $valid, true)) {
            $this->Jsonmsg->valid();
            $this->User->commit();
        } else {
            $this->User->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Edition des informations personnelles d'un utilisateur
     *
     * @logical-group Utilisateurs
     * @logical-group Environnement
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function setInfosPersos() {
        $this->set('ariane', array(
            '<a href="/environnement">' . __d('menu', 'Mon compte', true) . '</a>',
            __d('menu', 'Informations personnelles', true)
        ));
        $this->User->id = $this->Session->read('Auth.User.id');

        if (!empty($this->request->data) && empty($this->request->data['User']['oldpassword'])) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__d('default', 'Les informations ont bien été enregistrées.', true), 'growl');
            } else {
                $this->Session->setFlash(__d('default', 'Les informations n\'ont pas été enregistrées.', true), 'growl');
            }
//            }
        } else if (!empty($this->request->data['User']['oldpassword']) && !empty($this->request->data['User']['password']) && !empty($this->request->data['User']['password2'])) {

            $actUser = $this->User->find('first', [
                'fields' => [
					'id',
                    'password'
                ],
                'conditions' => [
                    'User.id' => $this->User->id
                ],
                'contain' => false
            ]);

            if ($actUser['User']['password'] == Security::hash($this->request->data['User']['oldpassword'], 'sha256', true)) {
                if ($this->User->validatesPassword($this->request->data)) {
					if($this->Session->read('Auth.User.Env.main') == 'superadmin' ) {
						$success = $this->User->updateAll(
							array(
								'User.password' => "'".Security::hash($this->request->data['User']['password'], 'sha256', true)."'"
							),
							array( 'User.id' => $this->User->id)
						);
					}
					else {
						$pwd = $this->request->data['User']['pwdmodified'];
						$success = $this->User->updateAll(
							array(
								'User.password' => "'".Security::hash($this->request->data['User']['password'], 'sha256', true)."'",
								'User.pwdmodified' => "'".$pwd."'"
							),
							array( 'User.id' => $this->User->id)
						);
					}


                    if ($success) {
                        $this->Session->setFlash('Le mot de passe a &eacute;t&eacute; modifi&eacute;', 'growl');
                    } else {
                        $this->Session->setFlash('Erreur lors de la saisie des mots de passe. Modif non exécuté', 'growl');
                    }
                } else {
                    $this->Session->setFlash('Erreur lors de la saisie des mots de passe. Validation non passée', 'growl');
                }
            } else {
                $this->Session->setFlash('L ancien mot de passe n est pas correct, Veuillez reessayer', 'growl');
            }
        }


		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['SERVER_ADDR'];
		}
		if( !empty( $this->request->data ) &&  isset($this->request->data['User']['accept_notif']) ) {
			if($this->request->data['User']['accept_notif'] == 1) {
				$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a consenti à la réception des notifications le ".date('d/m/Y à H:i:s'). " depuis l'IP : ".$ip;
			}
			else  if ($this->request->data['User']['accept_notif'] != 1) {
				$msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " n'a pas consenti à la réception des notifications le ".date('d/m/Y à H:i:s'). " depuis l'IP : ".$ip;
			}
			$this->loadModel('Journalevent');
			$datasSession = $this->Session->read('Auth.User');
			$this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info');
		}
        $user = $this->User->find(
                'first', array(
            'conditions' => array(
                'User.id' => $this->Session->read('Auth.User.id')
            ),
            'contain' => false
                )
        );

        $this->request->data = $user;
        $this->set(compact('user'));
        $username = $this->Session->read('Auth.User.username');
        $this->set('username', $username);
        $typeabonnement = array(
            'quotidien' => 'Résumé journalier',
            'event' => 'Envoi à chaque événement'
        );
        $this->set(compact('typeabonnement'));
        //on vide le mot de passe avant l affichage
        $this->request->data['User']['password'] = '';
    }

    /**
     * Visualisation des informations du compte
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @param boolean $reloadDesktops
     * @return void
     */
    public function getInfosCompte($reloadDesktops = false) {
        $this->set('ariane', array(
            '<a href="/environnement">' . __d('menu', 'Mon compte', true) . '</a>',
            __d('menu', 'Informations du compte', true)
        ));
        $userId = $this->Session->read('Auth.User.id');
        $querydata = array(
            'conditions' => array(
                'User.id' => $userId
            ),
            'contain' => array(
                $this->User->Desktop->alias => array(
                    $this->User->Desktop->Service->alias,
                    $this->User->Desktop->Profil->alias
                ),
                $this->User->SecondaryDesktop->alias => array(
                    $this->User->SecondaryDesktop->Service->alias,
                    $this->User->SecondaryDesktop->Profil->alias,
                    'order' => array(
                        'DesktopsUser.delegation'
                    )
                )
            )
        );
        $user = $this->User->find('first', $querydata);

        $okicondelegate = false;
        if (!empty($user['SecondaryDesktop'])) {
            foreach ($user['SecondaryDesktop'] as $o => $other) {
                if ($other['DesktopsUser']['delegation'] == true) {
                    $okicondelegate = true;
                }
            }
        }
        $this->set('okicondelegate', $okicondelegate);

        $this->set('user', $user);
        $this->set('baseDesktops', $this->User->DesktopsUser->getBaseDesktops($this->Session->read('Auth.User.id')));
        $delegatedDesktops = $this->User->getDelegatedDesktops($userId);
        $this->set('delegatedDesktops', $delegatedDesktops);
        $delegateToDesktops = $this->User->getDelegateToDesktops($userId);
        $this->set('delegateToDesktops', array_keys($delegateToDesktops));
        $this->set('reloadDesktops', $reloadDesktops);
    }

    /**
     * Récupération des informations d'un utilisateur (ajax)
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un utilisateur
     * @return void
     */
    public function editinfo($id = null) {
        $this->set('ariane', array(
            '<a href="/environnement">' . __d('menu', 'Mon compte', true) . '</a>',
            __d('menu', 'Informations personnelles', true)
        ));
        if ($id == null) {
            $id = $this->Auth->user('id');
        }
        if (empty($this->request->data)) {

            $this->User->id = $id;
            $user = $this->User->read();
//debug($user);
            $this->set('user', $user);
            $this->loadModel('Collectivite');
            $this->Collectivite->id = $this->User->getCollectiviteId($user['User']['id']);
            $this->set('collectivite', $this->Collectivite->read());
            $isAway = $this->User->isAway($user['User']['id']);
            $this->set('isAway', $isAway);
            if ($isAway) {
                $this->set('delegue', $this->User->find('first', array('conditions' => array('User.delegue' => $user['User']['id']))));
            }
            $this->set('users', $this->User->convertFindAllToList($this->User->findListInCollectivite($this->User->getCollectiviteId($user['User']['id']))));
        } else {
            //des infos sont retournée, on va les enregistrer
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash('Les informations sont enregistrées', 'growl');
                $this->redirect(array('controller' => 'environnement', 'action' => 'index'));
            } else {
                $this->Session->setFlash('Les informations ne sont pas enregistrées', 'growl');
            }
        }
    }

    /**
     * Chamgement du mot de passe personnel
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un utilisateur
     * @return void
     */
    public function changemdp($id = null) {
//        $this->set('ariane', array(__d('menu', 'Mon compte', true), __d('menu', 'Changer de mot de passe', true)));

        $actUser = $this->User->find('first', array('conditions' => array("User.id" => $id), "contain" => false));
        if ($id == null) {
            $id = $this->Auth->user('id');
        }

        if (empty($this->request->data)) {
            $this->request->data = $this->User->read(null, $id);
            if (empty($this->request->data)) {
                $this->Session->setFlash('Invalide id pour l\'utilisateur', 'growl');
                $this->redirect('/users/index');
            } else
                $this->request->data['User']['password'] = '';
        } else {

            $this->Jsonmsg->init();
            //verification de l'ancien mot de passe
                if ($this->User->validatesPassword($this->request->data)) {
                    $dataUser = [
						'id' => $this->request->data['User']['id'],
                    	'password' => Security::hash($this->request->data['User']['password'], 'sha256', true),
						'pwdmodified' => true
					];

                    if ($this->User->save($dataUser)) {
                        $this->Jsonmsg->valid();
                    }
                } else {
    //                $this->Session->setFlash('Erreur lors de la saisie des mots de passe.', 'growl');
                }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Visualisation des habilitations (Acl simplifiées / Acl / Méta-données / Soustypes) (interface graphique)
     *
     * @logical-group Utilisateurs
     * @logical-group Gestion des habilitations
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un uitilisateur
     * @param string $mode
     * @return void
     */
    public function rights($id = null, $mode = 'get') {
        if ($id != null) {
            $this->set('id', $id);
            $this->set('mode', $mode);
            $this->set('userName', $this->User->field('username', array('User.id' => $id)));
        }
    }

    /**
     * Visualisation des habilitations (Acl simplifiées / Acl / Méta-données / Soustypes) (ajax)
     *
     * @logical-group Utilisateurs
     * @logical-group Gestion des habilitations
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un uitilisateur
     * @param string $mode
     * @return void
     */
    public function getRights($id = null, $mode = 'data') {
        if ($id != null) {
            if ($mode == 'func') {
                $this->loadModel('Right');
                $allrights = array();
                $desktops = $this->User->getDesktops($id);
                foreach ($desktops as $desktop) {
                    $rights = array();
                    $aro = array('model' => 'Desktop', 'foreign_key' => $desktop);
                    $aro_id = $this->Acl->Aro->field('id', $aro);
                    $aro_alias = $this->Acl->Aro->field('alias', $aro);
                    $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $aro_id)/* , 'recursive' => -1 */));
                    if (empty($rights)) {
                        $parent_id = $this->Acl->Aro->field('parent_id', $aro);
                        $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $parent_id)/* , 'recursive' => -1 */));
                        $rights['Right']['id'] = null;
                        $rights['Right']['aro_id'] = $aro_id;
                        $rights['Right']['created'] = null;
                        $rights['Right']['modified'] = null;
                    }
                    $allrights[$aro_alias] = $rights;
                }
                $this->set('rights', $allrights);
            } else if ($mode == 'func_classique') {
                //func
                $maxTimeOut = 600;
                $baseTimeOut = 120;
                $desktops = $this->User->getDesktops($id);
                $aros = array();
                $cpt = 0;
                foreach ($desktops as $desktop) {
                    $aros[] = array('model' => 'Desktop', 'foreign_key' => $desktop);
                    $cpt++;
                }
                $totalTimeOut = $baseTimeOut * $cpt;
                set_time_limit($totalTimeOut < $maxTimeOut ? $totalTimeOut : $maxTimeOut);
                $this->set('rights', $this->_getFuncRightsGrid($aros));
            } else if ($mode == 'data') {
                //data
                $desktopsList = $this->User->getDesktops($id);
                $options = array(
                    'actions' => array('read'),
                    'models' => array('Type', 'Soustype')
                );
                $desktops = array();
                for ($i = 0; $i < count($desktopsList); $i++) {
                    $daros = $this->User->Desktop->getDaros($desktopsList[$i]);
                    $desktops[$i]['name'] = $this->User->Desktop->field('name', array('Desktop.id' => $desktopsList[$i]));
                    $desktops[$i]['dataRights'] = array();
                    $desktops[$i]['servicesNames'] = array();
                    foreach ($daros as $daro) {
                        $desktops[$i]['dataRights'][$daro['Daro']['foreign_key']] = $this->DataAcl->getRightsGrid($daro, array(), $options);
                        $serviceId = $this->User->Desktop->DesktopsService->field('DesktopsService.service_id', array('DesktopsService.id' => $daro['Daro']['foreign_key']));
                        $desktops[$i]['servicesNames'][$daro['Daro']['foreign_key']] = $this->User->Desktop->Service->field('Service.name', array('Service.id' => $serviceId));
                    }
                }
                $this->set('desktops', $desktops);
            } else if ($mode == 'meta') {
                //meta
                $desktopsList = $this->User->getDesktops($id);
                $options = array(
                    'actions' => array('read', 'update'),
                    'models' => array('Metadonnee')
                );
                $desktops = array();
                for ($i = 0; $i < count($desktopsList); $i++) {
                    $daros = $this->User->Desktop->getDaros($desktopsList[$i]);
                    $desktops[$i]['name'] = $this->User->Desktop->field('name', array('Desktop.id' => $desktopsList[$i]));
                    $desktops[$i]['metaRights'] = array();
                    $desktops[$i]['servicesNames'] = array();
                    foreach ($daros as $daro) {
                        $desktops[$i]['metaRights'][$daro['Daro']['foreign_key']] = $this->DataAcl->getRightsGrid($daro, array(), $options);
                        $serviceId = $this->User->Desktop->DesktopsService->field('DesktopsService.service_id', array('DesktopsService.id' => $daro['Daro']['foreign_key']));
                        $desktops[$i]['servicesNames'][$daro['Daro']['foreign_key']] = $this->User->Desktop->Service->field('Service.name', array('Service.id' => $serviceId));
                    }
                }
                $this->set('desktops', $desktops);
            } else if ($mode == 'circuit') {
                // Partie pour obtenir les cicuits dans lesquels l'utilisateur intervient

                $desktopsList = $this->User->getDesktops($id);
                $options = array(
                    'actions' => array('read', 'update'),
                    'models' => array('Circuit')
                );
                $desktops = array();
                for ($i = 0; $i < count($desktopsList); $i++) {
                    $desktopmanagersIds = $this->User->Desktop->getDesktopManager($desktopsList[$i]);
                    foreach ($desktopmanagersIds as $t => $dId) {
//debug($i);
                        $desktops['circuitName'][$dId]['name'] = $this->User->Desktop->Desktopmanager->field('name', array('Desktopmanager.id' => $dId));
//debug($desktops);
                        $desktopmanager = $this->User->Desktop->Desktopmanager->find('first', array('conditions' => array('Desktopmanager.id' => $dId)));

                        $desktops['circuitName'][$dId]['multiple'] = false;
                        if (count($desktopmanager['Desktop']) > 1) {
                            $desktops['circuitName'][$dId]['multiple'] = true;
                        }
                        $circuitsIds[$dId] = $this->User->Courrier->Traitement->Circuit->listeCircuitsParDesktopsmanagers($dId);
                        foreach ($circuitsIds[$dId] as $k => $cId) {
                            $desktops['circuitName'][$dId][$cId] = $this->User->Courrier->Traitement->Circuit->getLibelle($cId);
                        }
                    }
                }
                $this->set('desktops', $desktops);
            }
            $this->set('requester', array('model' => 'Desktop', 'foreign_key' => $id));
            $this->set('mode', $mode);
        }
    }

    /**
     * Vérificatoin de l'autorisation d'accès d'un ARO (profil, rôle) à un ACO (action d'un controlleur)
     *
     * @access private
     * @param array $aros tableau représentant un ARO (ACL)
     * @param string $aco alias d'un ACO (ACL) exemple:  "/controllers/controller/action"
     * @param mixed $action action CRUD ou "*"
     * @return boolean si l'ARO (profil, rôle) et autorisé à accéder à l'ACO (action d'un controlleur) alors on retourne true sinon on retourne false
     */
    private function _funcCheck($aros, $aco, $action = "*") {
        $checks = array();
        foreach ($aros as $aro) {
            $checks[] = $this->Acl->check($aro, $aco, $action);
        }
        return in_array(true, $checks, true);
    }

    /**
     * Récupération de la grille complète des droits pour une hiérarchie d'AROs (profils, rôles)	 *
     *
     * @access private
     * @param array $aros tableau contenants des ARO (profils, rôles) exemple:  $aros = array(array('model' => 'Profil', 'foreign_key' => 12), array('model' => 'Desktop', 'foreign_key' => 15))
     * @param array $acos tableau contenants des alias d'ACO (action d'un controlleur) exemple:  array('controllers/controller/action', 'controllers/controller/other_action')
     * @param array $options
     * @return array
     */
    private function _getFuncRightsGrid($aros, $acos = array(), $options = array('actions' => '*', 'alias' => '')) {
        if (empty($acos)) {
            $acos = $this->Acl->Aco->find('threaded');
        }
        $return = array();
        foreach ($acos as $aco) {
            $return[$aco['Aco']['id']] = $this->_getFuncRights($aros, $aco, $options);
        }
        return $return;
    }

    /**
     * Récupération des droits pour un hiérarchie d'AROs
     * cette méthode appelle récursivement _getFuncRightsGrid pour les enfants des AROs
     *
     * @access private
     * @param array $aros
     * @param string $aco
     * @param array $options
     * @return array
     */
    private function _getFuncRights($aros, $aco, $options = array('actions' => '*')) {
        $options['alias'] .= '/' . $aco['Aco']['alias'];
        $options['alias'] = ltrim($options['alias'], '/');

        $actions = $options['actions'];
        $checks = array();
        if ($actions == '*' || is_array($actions) && in_array('*', $actions)) {
            $chk = $this->_funcCheck($aros, $options['alias']);
            $checks['read'] = $chk;
        }
        if ($actions == 'create' || is_array($actions) && in_array('create', $actions)) {
            $checks['create'] = $this->_funcCheck($aros, $options['alias'], 'create');
        }
        if ($actions == 'read' || is_array($actions) && in_array('read', $actions)) {
            $checks['read'] = $this->_funcCheck($aros, $options['alias'], 'read');
        }
        if ($actions == 'update' || is_array($actions) && in_array('update', $actions)) {
            $checks['update'] = $this->_funcCheck($aros, $options['alias'], 'update');
        }
        if ($actions == 'delete' || is_array($actions) && in_array('delete', $actions)) {
            $checks['delete'] = $this->_funcCheck($aros, $options['alias'], 'delete');
        }

        $return = array(
            'checks' => $checks,
            'alias' => $options['alias']
        );
        if (!empty($aco['children'])) {
            $return['children'] = $this->_getFuncRightsGrid($aros, $aco['children'], $options);
            $this->_getFuncRightsGrid($aros, $aco['children']);
        }
        return $return;
    }

    /**
     * Récupération de la configuration de connexion de la collectivité
     *
     * @param string $conn
     * @return void
     */
    private function _getLoginConf($conn) {
        /*$this->loadModel('Collectivite');
        $collectivite = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn)));
        if (!empty($collectivite)) {
            Configure::write('ldap.active', $collectivite['Collectivite']['ldap_active']);
//            Configure::write('ldap.type', $collectivite['Collectivite']['ldap_type']);
            Configure::write('ldap.host', $collectivite['Collectivite']['ldap_host']);
            Configure::write('ldap.port', $collectivite['Collectivite']['ldap_port']);
            Configure::write('ldap.uniqueid', $collectivite['Collectivite']['ldap_uniqueid']);
            Configure::write('ldap.base_dn', $collectivite['Collectivite']['ldap_base_dn']);
            Configure::write('ldap.account_suffix', $collectivite['Collectivite']['ldap_account_suffix']);
            Configure::write('ldap.dn', $collectivite['Collectivite']['ldap_dn']);
        }*/
    }

    /**
     * Autentification via LDAP
     *
     * @access private
     * @param string $username
     * @param string $password
     * @return boolean
     */
    private function _ldapLogin($username, $password) {
        $return = false;
        $this->Session->write('Auth.User.connType', 'LDAP');
        $this->loadModel('Connecteur');
        $ldap = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%LDAP%'
                ),
                'contain' => false
            )
        );


//        if (Configure::read('ldap.use_ad') === true) {
        if ($ldap['Connecteur']['ldap_type'] === 'ActiveDirectory') {
            $return = $this->_adLogin($username, $password);
        } else {
//            $conn = ldap_connect('192.168.2.10', 389);
            $conn = ldap_connect($ldap['Connecteur']['ldap_host'], $ldap['Connecteur']['ldap_port']) or die("connexion impossible au serveur LDAP");
            ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($conn, LDAP_OPT_REFERRALS, 0); // required for AD
            $bind_attr = 'dn';
            $r = ldap_bind($conn, $ldap['Connecteur']['ldap_login'], $ldap['Connecteur']['ldap_password']);
            if ($ldap['Connecteur']['ldap_uniqueid'] == 'UID') {
                $search_filter = "(" . $ldap['Connecteur']['ldap_uniqueid'] . "=" . strtoupper($username) . ")";
            } else if ($ldap['Connecteur']['ldap_uniqueid'] == 'mail') {
                $search_filter = "(" . $ldap['Connecteur']['ldap_uniqueid'] . "=" . strtoupper($username) . $ldap['Connecteur']['ldap_account_suffix'] . ")";
            }
            $result = ldap_search($conn, $ldap['Connecteur']['ldap_base_dn'], $search_filter, array('dn', $bind_attr));

            $info = ldap_get_entries($conn, $result);
            if ($info['count'] > 0) {
                if ($bind_attr == "dn") {
                    $found_bind_user = $info[0]['dn'];
                } else {
                    $found_bind_user = $info[0][strtolower($bind_attr)][0];
                }

                if (!empty($found_bind_user)) {
                    $return = ldap_bind($conn, $info[0]['dn'], $password);
                }
            }
        }
        if ($return) {
            $user = $this->User->find('first', array('conditions' => array('User.username' => $this->request->data['User']['username']), 'recursive' => -1));
            $this->Session->write('Auth', $user);
        }
        return $return;
    }

    /**
     * Autentification via Active Directory
     *
     * @access private
     * @param string $username
     * @param string $password
     * @return boolean
     */
    private function _adLogin($username, $password) {
        $this->loadModel('Connecteur');
        $ldap = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%LDAP%'
                ),
                'contain' => false
            )
        );
        define('LDAP_HOST', $ldap['Connecteur']['ldap_host']);
        define('LDAP_PORT', $ldap['Connecteur']['ldap_port']);
        define('BASE_DN', $ldap['Connecteur']['ldap_base_dn']);
        define('ACCOUNT_SUFFIX', $ldap['Connecteur']['ldap_account_suffix']);
        define('LDAP_LOGIN', $ldap['Connecteur']['ldap_login']);
        define('LDAP_PASSWD', $ldap['Connecteur']['ldap_password']);

        include_once (APP . "Vendor/adLDAP.php");
        $adLdap = new adLDAP();
        $return = $adLdap->authenticate($username, $password);
        return $return;

    }

    /**
     * Récupération des informations de l'utilisateur
     *
     * @access private
     * @return void
     */
    private function _getUserConf() {
        if (Configure::read('conn') != 'default') {
            $querydata = array(
                'contain' => array(
                    'Desktop' => array(
                        'Service',
                        'Profil'
                    ),
                    'SecondaryDesktop' => array(
                        'Service',
                        'Profil'
                    )
//                    $this->User->Desktop->alias => array(
//                        $this->User->Desktop->Service->alias,
//                        $this->User->Desktop->Profil->alias
//                    ),
//                    $this->User->SecondaryDesktop->alias => array(
//                        $this->User->SecondaryDesktop->Service->alias,
//                        $this->User->SecondaryDesktop->Profil->alias
//                    )
                ),
                'conditions' => array(
                    'User.id' => $this->Session->read('Auth.User.id')
                )
            );
            $userConf = $this->User->find('first', $querydata);
//$this->Log( $userConf );
            $this->Session->write('Auth.User.Desktop.id', $userConf['Desktop']['id']);
            $this->Session->write('Auth.User.Courrier.Desktop.id', $userConf['Desktop']['id']);
            $this->Session->write('Auth.User.Desktop.name', $userConf['Desktop']['name']);
            $this->Session->write('Auth.User.Desktop.Profil.id', $userConf['Desktop']['Profil']['id']);
            $this->Session->write('Auth.User.Desktop.Profil.name', $userConf['Desktop']['Profil']['name']);
            $this->Session->write('Auth.User.Env.main', $this->getEnv($userConf['Desktop']['Profil']['id']));
            $mainServices = array();
            foreach ($userConf['Desktop']['Service'] as $srv) {
                $mainServices[] = array(
                    'id' => $srv['id'],
                    'name' => $srv['name']
                );
            }
            $this->Session->write('Auth.User.Desktop.Services', $mainServices);

            $secondaryEnvs = array();
            $secondaryDesktops = array();
            foreach ($userConf['SecondaryDesktop'] as $desktop) {
                $secDesktop = array(
                    'id' => $desktop['id'],
                    'name' => $desktop['name'],
                    'Profil' => array(
                        'id' => $desktop['Profil']['id'],
                        'name' => $desktop['Profil']['name']
                    ),
                    'Services' => array()
                );
                $secondaryEnvs[$desktop['id']] = $this->getEnv($desktop['Profil']['id']);
                foreach ($desktop['Service'] as $srv) {
                    $secDesktop['Services'][] = array(
                        'id' => $srv['id'],
                        'name' => $srv['name']
                    );
                }
                $secondaryDesktops[] = $secDesktop;
            }
            $this->Session->write('Auth.User.SecondaryDesktops', $secondaryDesktops);
            $this->Session->write('Auth.User.Env.secondary', $secondaryEnvs);
        }
        //construction du menu
        $this->_genMenu();
    }

    /**
     * Connexion à l'application
     *
     * @logical-group Utilsateurs
     * @logical-group Environnemnt
     * @user-profile All
     *
     * @access public
     * @return void
     */
    public function login() {
        $this->layout = 'login';

        $error = false;
        $this->loadModel('Collectivite');
        $conns = $this->Collectivite->find('list', array(
        	'fields' => array(
        		'Collectivite.conn',
				'Collectivite.name'
			),
			'conditions' => array(
				'Collectivite.active' => true
			)
		));
        $this->set('conns', $conns);

        // Utilisé pour le paramétrage de la page d'accueil
        $this->loadModel('Configuration');
        $config = $this->Configuration->find('first', array(
        	'contain' => false,
			'recursive' => -1
		));
        $this->set(compact('config') );
//die();
        // Variable pour déterioner si le cache est bien rempli
        // et déclencement d'un message indiquant le chargement du cache.
        $auths = array(
            'TypesActif',
            'TypesAll',
            'Soustypes',
            'Ban',
            'Origineflux',
            'AllDesktops',
            'Addressbook',
            'Typesvoie',
            'MetasList',
            'MetasUpdate',
            'Rights',
            'Civilite',
            'User'
        );
        $nbAuths = count($auths);
        $this->Session->write('Auth.Fullcache', false );
        $count = 0;
        $sessionUsername = $this->Session->read('Auth.Username');
        $userConnected = '';

        // Valeur pour masquer le lien Mot de passe oublié dans le cas où UNE collectivité (parmi toutes) est connectée derrière un AD
        $motdepasseoublieHide = false;
        foreach( $conns as $conn => $name ) {
            $coll = $this->Collectivite->find(
                'first',
                array(
                    'conditions' => array(
                        'Collectivite.active' => true,
                        'Collectivite.conn' => $conn
                    ),
                    'contain' => false
                )
            );
            $ldapActive[$conn] = $coll['Collectivite']['ldap_active'];
            if( $ldapActive[$conn] ) {
                $motdepasseoublieHide = true;
            }
        }
        $this->set(compact('motdepasseoublieHide'));
        if( !empty($this->request->data) ) {
            $hasArrobase = strpos($this->request->data['User']['username'], '@');
            if ($hasArrobase) {
                $usernameWithoutSuffixe = strpos($this->request->data['User']['username'], '@');
                $userConnected = substr($this->request->data['User']['username'], 0, $usernameWithoutSuffixe);
            }
        }
        if( !empty($userConnected) ) {
            $this->Session->delete('Auth');
            $this->Session->write('Auth.Fullcache', false );
        }
        if( !empty($this->Session->read('Auth') ) ) {
            $names = array_keys( $this->Session->read('Auth') );
            foreach( $names as $key => $name ) {
                if( empty( $this->Session->read("Auth.{$name}") ) ) {
                    $count++;
                }
            }
        }
        else {
            $count++;
        }
        if($count == 0) {
            $this->Session->write('Auth.Fullcache', true );
        }
        $this->set( 'nbAuths', $nbAuths );
        $this->set( 'auths', $auths);

        $db_login_suffix = null;
//        if (isset($this->request->data['User'])) {
//            $matches = array();
//            if (preg_match('#(.*)@admin$#', $this->request->data['User']['username'], $matches)) {
//                $this->request->data['User']['username'] = $matches[1];
//                $db_login_suffix = 'default';
//            } else if (preg_match('#(.*)@(.*)$#', $this->request->data['User']['username'], $matches)) {
//                $this->request->data['User']['username'] = $matches[1];
//                $db_login_suffix = $matches[2];
//            }
//        }
// Si configuration par CAS
        if (Configure::read('AuthManager.Authentification.use') && Configure::read('AuthManager.Authentification.type') === 'CAS') {
			if( !Configure::read('ConfCas.WithoutLemonLdapOrOtherApp') ) {
				$headers = apache_request_headers();
				foreach ($headers as $header => $value) {
					if ($header == 'Auth-User') {
						if ($value == 'admin' || $value == 'superadmin') {
							$this->request->data['User']['username'] = 'superadmin';
							$db_login_suffix = 'default';
						} else {
							$this->request->data['User']['username'] = $value;
							$this->chooseCollForCas();
							if (Configure::read('Suffixe.connexion') == '') {
								$matches = array();
								if (preg_match('#(.*)@(.*)$#', $this->request->data['User']['username'], $matches)) {
									$this->request->data['User']['username'] = $matches[1];
									$db_login_suffix = $matches[2];
								}
							} else {
								$db_login_suffix = Configure::read('Suffixe.connexion');
							}
						}
					} else {
						if (!isset($headers['Auth-User'])) {
							$this->request->data['User']['username'] = 'superadmin';
							$db_login_suffix = 'default';
						}
					}
				}
			}
			else {
				$db_login_suffix = Configure::read('Suffixe.connexion');
				$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				if(strpos($actual_link, '/cas/login') !== false ) {
					$this->request->data['User']['username'] = phpCAS::getUser();
				}
			}

        }
        // Sinon connexion normale
        else {
            if (isset($this->request->data['User'])) {
                // Si + d'1 collectivité, on lit le suffixe de connexion
                if (Configure::read('Suffixe.connexion') == '') {
					$matches = array();
					if (Configure::read('Mode.Multicoll') && Configure::read('Affichage.Selectcoll') ) {
						if (!empty($this->request->data['User']['coll'])) {
							$this->request->data['User']['username'] = $this->request->data['User']['username'];
							$db_login_suffix = $this->request->data['User']['coll'];
						}
						else if (preg_match('#(.*)@admin$#', $this->request->data['User']['username'], $matches)) {
							$this->request->data['User']['username'] = $matches[1];
							$db_login_suffix = 'default';
						}
					}
					else {
						if (preg_match('#(.*)@admin$#', $this->request->data['User']['username'], $matches)) {
							$this->request->data['User']['username'] = $matches[1];
							$db_login_suffix = 'default';
						} else if (preg_match('#(.*)@(.*)$#', $this->request->data['User']['username'], $matches)) {
							$this->request->data['User']['username'] = $matches[1];
							$db_login_suffix = $matches[2];
						}
					}
                }
                // Sinon, si une seule collectivité, alors on récupère le suffixe depuis webgfc.inc
                else {
                    $matches = array();
                    if (preg_match('#(.*)@admin$#', $this->request->data['User']['username'], $matches)) {
                        $this->request->data['User']['username'] = $matches[1];
                        $db_login_suffix = 'default';
                    } else {
                        $db_login_suffix = Configure::read('Suffixe.connexion');
                    }
                }
            }
        }

        if (!empty($db_login_suffix)) {
            $logged = false;
            $isActive = $this->Collectivite->find('first', array('conditions' => array('Collectivite.active' => true, 'Collectivite.login_suffix' => $db_login_suffix)));
            if (!empty($isActive) || $db_login_suffix == "default") {
                Configure::write('conn', !empty($isActive) ? $isActive['Collectivite']['conn'] : $db_login_suffix);
                Configure::write('login_suffix', @$isActive['Collectivite']['login_suffix']);
                Configure::write('collectivite_name', !empty($isActive) ? $isActive['Collectivite']['name'] : "");
                if (Configure::read('conn') != 'default') {
                    $this->loadModel('Connecteur');
                    $ldap = $this->Connecteur->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Connecteur.name ILIKE' => '%LDAP%'
                            ),
                            'contain' => false
                        )
                    );
                    if( !empty($ldap)) {
                        $isLdapActive = $ldap['Connecteur']['use_ldap'];
                    }
                    else {
                        $isLdapActive = false;
                    }
//                    $this->_getLoginConf(Configure::read('conn'));
                    if ($isLdapActive && Configure::read('AuthManager.Authentification.type') !== 'CAS') {
                        $ldaplogged = $this->_ldapLogin($this->request->data['User']['username'], $this->request->data['User']['password']);
//$this->log( $ldaplogged );
                        $userValid = $this->User->find('first', [
							'conditions' => array(
								'User.active' => true,
								'User.username' => $this->request->data['User']['username']
							),
							'contain' => array(
								'Desktop' => array(
									'Service',
									'Profil'
								),
								'SecondaryDesktop' => array(
									'Service',
									'Profil'
								)
							)
						]);

                        if( !empty( $userValid['Desktop'] )) {
                            $profilName = $userValid['Desktop']['Profil']['name'];
                        }

                        $isAdmin = false;
                        $secondaryEnvs = array();
                        $secondaryDesktops = array();
                        if( !empty($userValid['SecondaryDesktop']) ){
                            foreach ($userValid['SecondaryDesktop'] as $desktop) {
                                $secDesktop = array(
                                    'id' => $desktop['id'],
                                    'name' => $desktop['name'],
                                    'Profil' => array(
                                        'id' => $desktop['Profil']['id'],
                                        'name' => $desktop['Profil']['name']
                                    ),
                                    'Services' => array()
                                );
                                $secondaryEnvs[$desktop['id']] = $this->getEnv($desktop['Profil']['id']);
                                foreach ($desktop['Service'] as $srv) {
                                    $secDesktop['Services'][] = array(
                                        'id' => $srv['id'],
                                        'name' => $srv['name']
                                    );
                                }
                                $secondaryDesktops[] = $secDesktop;
                            }
                            $this->Session->write('Auth.User.SecondaryDesktops', $secondaryDesktops);
                            $this->Session->write('Auth.User.id', $userValid['User']['id']);
                            $this->Session->write('Auth.User', $userValid['User']);

                            if( !empty($secondaryDesktops) ) {
                                $desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
                                if (in_array('Admin', $desktopName) || !empty($profilName) && $profilName == 'Admin') {
                                    $isAdmin = true;
                                }
                            }
                        }
                        // Le cas du profil Admin est à part
                        /*if( $isAdmin ) {
							$userAdminValid = $this->User->find('first', array(
								'conditions' => array(
									'User.active' => true,
									'User.username' => $this->request->data['User']['username'],
									'User.password' => Security::hash($this->request->data['User']['password'], 'sha256', true)
								),
								'contain' => false
							));

							if ( $ldaplogged ) {
								$logged = true;
							}
							else if( !empty($userAdminValid)) {
								$logged = true;
							}
						}*/

                        if ($ldaplogged && !empty($userValid)) {
                            $logged = true;
                        }
                    } else if ($this->Auth->login()) {
                        if (Configure::read('AuthManager.Authentification.use') &&
							Configure::read('AuthManager.Authentification.type') === 'CAS'
						) {
							if(Configure::read('ConfCas.WithoutLemonLdapOrOtherApp')) {
								$this->request->data['User']['username'] = phpCAS::getUser();
							}
							ClassRegistry::init(('User'));
							$this->User = new User();
							$this->User->setDataSource(Configure::read('conn'));
							$userValid = $this->User->find('first', [
                                'conditions' => [
                                    'User.active' => true,
                                    'User.username' => $this->request->data['User']['username']
                                ]
							]);

                            if (!empty($userValid)) {
                                $logged = true;
                                $this->Session->write('Auth.User.connType', 'Database');
                            }
                        } else {
							ClassRegistry::init(('User'));
							$this->User = new User();
							$this->User->setDataSource(Configure::read('conn'));
                            $userValid = $this->User->find('first', [
                                'conditions' => [
                                    'User.active' => true,
                                    'User.username' => $this->request->data['User']['username'],
                                    'User.password' => Security::hash($this->request->data['User']['password'], 'sha256', true)
                                ]
							]);
                            if (!empty($userValid)) {
                                $logged = true;
                                $this->Session->write('Auth.User.connType', 'Database');
                            }
                        }
                    } else if (Configure::read('AuthManager.Authentification.use') &&
						Configure::read('AuthManager.Authentification.type') === 'CAS'
					) {
                        $logged = true;
                    }
                    // Sinon, on regarde les anciens mots de passe
					else {

						$userValidSha1 = $this->User->find('first', [
							'conditions' => [
								'User.active' => true,
								'User.username' => $this->request->data['User']['username'],
								'User.password' => Security::hash($this->request->data['User']['password'], 'sha1', OLD_SALT)
							]
						]);
						if (!empty($userValidSha1)) {
							$this->User->id = $userValidSha1['User']['id'];
							$data = [
								'User' => [
									'id' => $userValidSha1['User']['id'],
									'password' => Security::hash($this->request->data['User']['password'], 'sha256', true)
								]
							];
							$this->User->create($data);
							$success = $this->User->save(null, false);

							if (!empty($success) &&
								$this->Auth->login($data)
							) {
								$userValid = $userValidSha1;
								$logged = true;
								$this->Session->write('Auth', $userValidSha1);
								$this->Session->write('Auth.User.connType', 'Database');
							}
						}
						else {
							$userValid256 = $this->User->find('first', [
								'conditions' => [
									'User.active' => true,
									'User.username' => $this->request->data['User']['username'],
									'User.password' => Security::hash($this->request->data['User']['password'], 'sha256', true)
								]
							]);

							if( !empty($userValid256) ) {
								$userValid = $userValid256;
								$logged = true;
								$this->Session->write('Auth', $userValid256);
								$this->Session->write('Auth.User.connType', 'Database');
							}
						}
					}
                } else {
                    // SI CAS actif, en tant qu'admin on peut se connecter à webgfc en tant que superadmin
                    if (Configure::read('AuthManager.Authentification.use') &&
						Configure::read('AuthManager.Authentification.type') === 'CAS'
					) {
						ClassRegistry::init(('User'));
						$this->User = new User();
						$this->User->setDataSource(Configure::read('conn'));
                        $userValid = $this->User->find('first', [
                        	'conditions' => [
                        		'User.active' => true,
								'User.username' => $this->request->data['User']['username']
							]
						]);
                        if (!empty($userValid)) {
                            $this->Session->write('Auth', $userValid);
                            $this->Session->write('Auth.User.Env.main', 'superadmin');
                            $logged = true;
                        }
                    } else {
                        if (isset($this->request->data['User']['password'])) {
							$oldSuperadminValid = $this->User->find('first', [
								'conditions' => [
									'User.active' => true,
									'User.username' => $this->request->data['User']['username'],
									'User.password' => Security::hash($this->request->data['User']['password'], 'sha1', OLD_SALT)
								]
							]);
							if (!empty($oldSuperadminValid)) {
								$this->User->id = $oldSuperadminValid['User']['id'];
								$data = [
									'User' => [
										'id' => $oldSuperadminValid['User']['id'],
										'password' => Security::hash($this->request->data['User']['password'], 'sha256', true)
									]
								];
								$this->User->create($data);
								$success = $this->User->save(null, false);

								if (!empty($success) &&
									$this->Auth->login($data)
								) {
									$userValid = $oldSuperadminValid;
									$this->Session->write('Auth', $oldSuperadminValid);
									$this->Session->write('Auth.User.Env.main', 'superadmin');
									$logged = true;
								}
							}
							else {
								$superadminValid = $this->User->find('first', [
									'conditions' => [
										'User.active' => true,
										'User.username' => $this->request->data['User']['username'],
										'User.password' => Security::hash($this->request->data['User']['password'], 'sha256', true)
									]
								]);

								if (!empty($superadminValid)) {
									$userValid = $superadminValid;
									$this->Session->write('Auth', $superadminValid);
									$this->Session->write('Auth.User.Env.main', 'superadmin');
									$logged = true;
								}
							}
                        }
                    }
                }
            }

            if ($logged) {
                $this->Session->write('Auth.Username', $this->request->data['User']['username']);
                $this->Session->write('Auth.Fullcache', true);
                $this->Session->write('Auth.User.connName', Configure::read('conn'));
                $this->Session->write('Auth.User.login_suffix', Configure::read('login_suffix'));
                $this->Session->write('Auth.User.collectivite_name', Configure::read('collectivite_name'));
                $this->_setConn(Configure::read('conn'));
                $this->_getUserConf();
                $this->User->id = $this->Session->read('Auth.User.id');
                $this->User->saveField('last_login', 'NOW()');
                $this->Session->setFlash(__d('default', 'welcome message', true), 'growl');
                $this->Linkeddocs->purge();
                // on stocke qui à fait quoi quand
//                $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " s'est connecté à l'application le ".date('d/m/Y à H:i:s'), LOG_WARNING );


                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip = $_SERVER['SERVER_ADDR'];
                }

                if( $this->Session->read('Auth.User.Env.main') !== 'superadmin' ) {
                    $this->loadModel('Journalevent');
                    $datasSession = $this->Session->read('Auth.User');
                    $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " s'est connecté à l'application le ".date('d/m/Y à H:i:s'). " depuis l'IP : ".$ip;
                    $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info');
                }

                if (!empty($userValid['User']['pwdmodified']) && $userValid['User']['pwdmodified'] == true) {
                    $this->redirect('setInfosPersos');
                } else {
                    $this->redirect($this->Auth->redirect());
                }
            } else /* if ($this->Session->read('Message.flash.message') != __d('default', 'good-bye')) */ {
                $error = true;
                $this->Session->setFlash(__d('login', 'login_error'), 'growl');
            }
        }
        $this->set(compact( 'error') );
    }

    /**
     * Déconnexion de l'application
     *
     * @logical-group Utilsateurs
     * @logical-group Environnemnt
     * @user-profile All
     *
     * @access public
     * @return void
     */
    public function logout() {
        $this->Linkeddocs->purge();
        $this->Session->setFlash(__d('default', 'good-bye'), 'growl');
        if ($this->Session->read('Auth.User.id') > 0) {
            $this->User->id = $this->Session->read('Auth.User.id');
            $this->User->saveField('last_logout', 'NOW()');
        }
        $this->Acl->flushcache();
        $this->DataAcl->flushcache();

        // on stocke qui à fait quoi quand
//        $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " s'est déconnecté de l'application le ".date('d/m/Y à H:i:s'), LOG_WARNING );

        /*if( $this->Session->read('Auth.User.Env.main') !== 'superadmin' ) {
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " s'est déconnecté à l'application le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info');
        }*/

        if (Configure::read('AuthManager.Authentification.use') && Configure::read('AuthManager.Authentification.type') === 'CAS') {
            $this->redirect('https://' . Configure::read('AuthManager.Cas.host'));
        } else {
            $this->redirect($this->Auth->logout());
        }
    }

    function casLogout() {
        $this->layout = 'login';
        $this->redirect('https://' . Configure::read('AuthManager.Cas.host'));
    }

    /**
     * Vérification de la disponibilitéde l'utilisateur
     *
     * @logical-group Utilsateurs
     * @logical-group Environnemnt
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function isAway() {
        $this->set('isAway', $this->User->isAway($this->Session->read('Auth.User.id')));
        $this->set('delegateToDesktopsContents', $this->User->getDelegateToDesktopsContents($this->Session->read('Auth.User.id')));
        $this->set('delegatedDesktopsContents', $this->User->getDelegatedDesktopsContents($this->Session->read('Auth.User.id')));
    }

    /**
     *
     */
//	public function importLdap() {
//		$services = array();
//
//		$conn = ldap_connect(Configure::read('ldap.host'), Configure::read('ldap.port')) or die("connexion impossible au serveur LDAP");
//		@ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
//		@ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0); // required for AD
//		// bind with appropriate dn to give update access
//		$r = ldap_bind($conn, Configure::read('ldap.login'), Configure::read('ldap.passwd'));
//		if (!$r)
//			die("ldap_bind failed<br>");
//		$result = ldap_search($conn, Configure::read('ldap.base_dn'), "(cn=*)") or die("Error in query");
//		$data = ldap_get_entries($conn, $result);
//
//		foreach ($data as $person) {
//			if ((isset($person['displayname'])) && ($person['displayname'][0] != "")) {
//				if (isset($person['cgservice'][0])) {
//					$service_id = substr($person['departmentnumber'][0], 1, strlen($person['departmentnumber'][0]));
//					$service['Service']['id'] = $service_id;
//					$service['Service']['libelle'] = utf8_decode($person['cgservice'][0]);
//					$this->Service->save($service);
//				}
//				$this->User->create();
//				$user['User']['id'] = '';
//				$user['User']['login'] = utf8_decode($person['cn'][0]);
//				$user['User']['profil_id'] = 0;
//				$user['User']['statut'] = 0;
//				$user['User']['nom'] = utf8_decode($person['sn'][0]);
//				$user['User']['prenom'] = utf8_decode($person['givenname'][0]);
//				$user['User']['accept_notif'] = 0;
//				$user['User']['password'] = md5('0');
//				$user['User']['password2'] = md5('0');
//				$user['User']['telmobile'] = '';
//				$user['User']['circuit_defaut_id'] = '';
//				$user['User']['note'] = '';
//				$user['Service']['Service'][0] = $service_id;
//
//				if (isset($person['mail'][0]))
//					$user['User']['email'] = $person['mail'][0];
//				else
//					$user['User']['email'] = "";
//
//				if (isset($person['telephonenumber'][0]))
//					$user['User']['telfixe'] = $person['telephonenumber'][0];
//				else
//					$user['User']['telfixe'] = "";
//
//				if (!$this->User->save($user)) {
//					die('Sauvegarde echouée' . $person['displayname'][0]);
//				}
//				/* else {
//				  $user_id = $this->User->getLastInsertId();
//				  $aro = new Aro();
//				  $aro->create($user_id, null, 'Utilisateur:'. utf8_decode($person['cn'][0]));
//				  $profilLibelle = $this->Profil->field('libelle', 'Profil.id = 1');
//				  $aro->setParent('Profil:'.$profilLibelle  ,$user_id);
//				  } */
//			}
//		}
//		$this->redirect('/users/index');
//	}

    /**
     * Fonction permettant la gestion du mot de passe oublié
     *
     * @logical-group Utilsateurs
     * @logical-group Environnemnt
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function mdplost($objet = NULL) {

        $this->layout = 'login';
        $this->loadModel('Collectivite');
        $conns = $this->Collectivite->find('list', array('fields' => array('Collectivite.conn', 'Collectivite.name'), 'conditions' => array('Collectivite.active' => true)));
        $this->set('conns', $conns);

		// Utilisé pour le paramétrage de la page d'accueil
		$error = false;
		$this->loadModel('Configuration');
		$config = $this->Configuration->find('first', array( 'contain' => false, 'recursive' => -1 ) );
		$this->set(compact('config') );

		foreach( $this->request->data as $value ) {
			$this->request->data['User']['username'] = trim( get_string_between($value, "data[User][username]\"", '--') );
			$this->request->data['User']['mail'] = trim( get_string_between($value, "data[User][mail]\"", '--') );
		}

        // Récupération du suffix permettant de connaître la collectivité sur laquelle se connecter
        $db_login_suffix = null;
        if (isset($this->request->data)) {
			if (Configure::read('Suffixe.connexion') == '') {
				$matches = array();
				if (preg_match('#(.*)@admin$#', $this->request->data['User']['username'], $matches)) {
					$this->request->data['User']['username'] = $matches[1];
					$db_login_suffix = 'default';
				} else if (preg_match('#(.*)@(.*)$#', $this->request->data['User']['username'], $matches)) {
					$this->request->data['User']['username'] = $matches[1];
					$db_login_suffix = $matches[2];
				}
			} else {
				if (preg_match('#(.*)@admin$#', $this->request->data['User']['username'], $matches)) {
					$this->request->data['User']['username'] = $matches[1];
					$db_login_suffix = 'default';
				}
				else {
					$db_login_suffix = Configure::read('Suffixe.connexion');
				}
			}
        }

        $isActive = null;
        //Si le suffix existe, vérification de la colectivité active
        if (!empty($db_login_suffix)) {
            $isActive = $this->Collectivite->find(
				'first',
				array(
					'conditions' => array(
						'Collectivite.active' => true,
						'Collectivite.login_suffix' => $db_login_suffix
					)
				)
            );

//$this->log($isActive);
            // Si la collectivité existe et est active, on travaille
            if (!empty($isActive) && !empty($this->request->data)  || $db_login_suffix == "default") {

                // Connexion à la collectivité en question pour rechercher l'utilsiateur en question
				$this->User->useDbConfig = $isActive['Collectivite']['conn'];
				if( $db_login_suffix == "default" ) {
					$this->User->useDbConfig = $db_login_suffix;
				}


                $this->User->begin();
                $user = $this->User->find(
                        'first', array(
                    'fields' => array(
                        'User.id',
                        'User.username',
                        'User.active',
                        'User.mail'
                    ),
                    'conditions' => array(
                        'User.username' => $this->request->data['User']['username'],
                        'User.mail' => $this->request->data['User']['mail']
                    ),
                    'contain' => false
                        )
                );

                // Si l'utilisateur existe, on vérifie le couple login/mot de passe
                if (empty($user) || ($user['User']['id'] < 0)) {
                    $this->Session->setFlash(__d('mdplost', 'save_error_couple'), 'growl', array('type' => 'erreur'));
                    $this->redirect($this->referer());
                    $this->User->rollback();
                }
                // Si l'utilsiateur existe, on vérifie qu'il est actif
                else if (!$user['User']['active']) {
                    $this->Session->setFlash(__d('mdplost', 'save_error_inactif'), 'growl', array('type' => 'important'));
                    $this->redirect($this->referer());
                    $this->User->rollback();
                }
                // Si tout est ok, on remplace son mot de passe et on l'envoi sur le mail renseigné
                else {
                    // génération du nouveau mot de passe
                    $newpass = $this->Password->generatePassword();

                    $this->request->data['User']['password'] = Security::hash($newpass, 'sha256', true);
                    $this->request->data['User']['pwdmodified'] = true;
                    $this->request->data['User']['id'] = $user['User']['id'];

                    $save = $this->User->save($this->request->data);
                    $success = !empty($save);
                    $errorMessage = null;

                    if ($success) {
                        // Paramètres mail
                        try {
                            $Email = new CakeEmail('motdepasseoublie');
                            if (Configure::read('debug') == 0) {
                                $Email->to($user['User']['mail']);
                            } else {
                                $Email->to($Email->from());
                            }
                            $Email->subject('Changement de mot de passe dans web-GFC');

                            $mailBody = "Bonjour,\nsuite à votre demande, veuillez trouver ci-dessous vos nouveaux identifiants: " . "\n" . "  Rappel de votre identifiant : " . $user['User']['username'] . "@" . $isActive['Collectivite']['login_suffix'] . "\n" . "  Votre nouveau mot de passe : " . $newpass . "\n\nA votre première connexion, il vous sera demandé de modifier ce mot de passe.";

                            $result = $Email->send($mailBody);

                            $success = !empty($result) && $success;
                        } catch (Exception $e) {
                            $this->log($e->getMessage(), LOG_ERROR);
                            $success = false;
                            $errorMessage = 'save_error_mail';
                        }
                    } else {
                        $errorMessage = 'save_error';
                    }

                    //Sauvegarde du nouveau mot de passe de l'utilisateur et envoi du mail
                    if ($success) {
                        $this->User->commit();
                        $this->redirect('/users/login/');
                        $this->Session->setFlash(__d('mdplost', 'save_success'), 'growl');
                    } else {
                        $this->User->rollback();
                        $this->Session->setFlash(__d('mdplost', $errorMessage), 'growl', array('type' => 'erreur'));
                    }
                }
            }
        } else if (empty($db_login_suffix) && !empty($this->request->data)) {
			$error = true;
            // Si l'identifnat ne comporte aps de suffix, ou que le suffix n'existe pas,
            // on affiche le message d'erreur
            $this->Session->setFlash(__d('mdplost', 'save_error_suffix'), 'growl', array('type' => 'erreur'));
        }

		$this->set(compact( 'error') );
        $this->set('title_for_layout', 'Demande de mot de passe');
    }

    /**
     * Chamgement de l'abnnement aux notifications
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un utilisateur
     * @return void
     */
    public function changenotif($id = null) {
        $actUser = $this->User->find('first', array('conditions' => array("User.id" => $id), "contain" => false));
        $this->set(compact('actUser'));


        if (empty($this->request->data)) {
            $this->request->data = $this->User->read(null, $id);
            if (empty($this->request->data)) {
                $this->Session->setFlash('Invalide id pour l\'utilisateur', 'growl');
                $this->redirect('/users/index');
            }
        } else {
            $this->Jsonmsg->init();
            if ($this->User->save($this->request->data)) {
                $this->Jsonmsg->valid();
//                $this->Session->setFlash('Enregistrement effectué', 'growl');
//                $this->redirect('/users/index');
            } else {
//                $this->Session->setFlash('Erreur lors de l\'enregistrement.', 'growl');
            }
            $this->Jsonmsg->send();
        }
        $typeabonnement = array(
            'quotidien' => 'Résumé journalier',
            'event' => 'Envoi à chaque événement'
        );
        $this->set(compact('typeabonnement'));
    }

    /**
     * Export des résultats au format CSV
     *
     * @logical-group Users
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function exportcsv() {
        $queryData = $this->User->search(Hash::expand($this->request->params['named'], '__'), null);

        unset($queryData['limit']);
        $results = $this->User->find('all', $queryData);

        foreach ($results as $key => $user) {
            // Permissions
            $user['right_edit'] = true;
            $user['right_delete'] = true;

            // Services liés au Desktop et au SecondaryDesktop
            $query = array(
                'fields' => array(
                    'DesktopsUser.id',
                    'DesktopsUser.user_id',
                    'DesktopsUser.desktop_id',
                    'DesktopsService.service_id',
                    'DesktopsService.id',
                    'DesktopsService.desktop_id',
                    'SecondaryDesktop.id',
                    'SecondaryDesktop.profil_id',
                ),
                'contain' => false,
                'joins' => array(
                    $this->User->join('DesktopsUser', array('type' => 'INNER')),
                    alias_querydata(
                            $this->User->DesktopsUser->join('Desktop', array('type' => 'INNER')), array('Desktop' => 'SecondaryDesktop')
                    ),
                    alias_querydata(
                            $this->User->DesktopsUser->Desktop->join('DesktopsService', array('type' => 'LEFT OUTER')), array('Desktop' => 'SecondaryDesktop')
                    ),
                ),
                'conditions' => array(
                    'User.id' => $user['User']['id']
                )
            );
            $allUsers = $this->User->find('all', $query);

            $services_ids = array_unique(
                    array_merge(
                            array_keys($this->User->Desktop->getServicesList($user['Desktop']['id']))
                    ), SORT_REGULAR
            );
            $user['Service'] = array();
            foreach ($services_ids as $service_id) {
                $user['Service'][] = array('id' => $service_id);
            }

            $groups_ids = array_unique(
                    array_merge(
                            array($user['Profil']['id']), Hash::extract($allUsers, '{n}.SecondaryDesktop.profil_id')
                    ), SORT_REGULAR
            );
            $user['Profil'] = array();
            foreach ($groups_ids as $group_id) {
                $user['Profil'][] = array('id' => $group_id);
            }

            $secondary_desktops_ids = array_unique(Hash::extract($allUsers, '{n}.SecondaryDesktop.id'), SORT_REGULAR);
            $user['SecondaryDesktop'] = array();
            foreach ($secondary_desktops_ids as $secondary_desktop_id) {
                $user['SecondaryDesktop'][] = array('id' => $secondary_desktop_id);
            }

            // Ajout des services associés à l'user dans le tableau de résultats
            $user['User']['services'] = array();
            $services_names = array_unique(
                    array_merge(
                            array_values($this->User->Desktop->getServicesList($user['Desktop']['id']))
                    ), SORT_REGULAR
            );
            foreach ($services_names as $serviceName) {
                $user['User']['services'][] = implode(' / ', array($serviceName));
            }
            if (!empty($user['User']['services'])) {
                $user['User']['services'] = implode(' / ', $user['User']['services']);
            } else {
                $user['User']['services'] = null;
            }

            // Ajout des profils associés à l'user dans le tableau de résultats
            $user['User']['profils'] = array();
            $groups_ids = Hash::extract($user, 'Profil.{n}.id');
            foreach ($groups_ids as $i => $groupId) {
                $groupName = $this->User->Desktop->Profil->find(
                        'first', array(
                    'fields' => array(
                        'Profil.name'
                    ),
                    'conditions' => array(
                        'Profil.id' => $groupId
                    ),
                    'recursive' => -1
                        )
                );
                $user['User']['profils'][] = Hash::get($groupName, 'Profil.name');
            }
            $user['User']['profils'] = implode(' / ', $user['User']['profils']);
            // On complète le résultat
            $results[$key] = $user;
        }

        $this->set('results', $results);

        $this->layout = '';
    }

    /**
     * Focniton permettant de supprimer l'utilsiateur à l'étape d'un circuit
     * @param type $id
     */
    public function cutCircuit($circuit_id, $desktopmanager_id) {

        $circuitInfo = $this->User->Desktop->Composition->Etape->Circuit->find(
                'first', array(
            'conditions' => array(
                'Circuit.id' => $circuit_id
            ),
            'contain' => array(
                'Etape' => array(
                    'Composition'
                )
            )
                )
        );

        $this->Jsonmsg->init();
        foreach ($circuitInfo['Etape'] as $i => $compositions) {
            foreach ($compositions['Composition'] as $c => $compo) {
                if ($compo['trigger_id'] == $desktopmanager_id) {
                    $this->User->Desktop->Composition->begin();

                    $success = $this->User->Desktop->Composition->delete($compo['id']);
                    if ($success) {
                        $this->User->Desktop->Composition->commit();
                        $this->Jsonmsg->valid();
                    } else {
                        $this->User->Desktop->Composition->rollback();
                    }
                }
            }
        }
        $this->Jsonmsg->send();
    }

    /**
     * @access public
     * @return type
     */
    public function casLogin() {
        if ($this->Auth->login()) {
            return $this->login();
        }

        return $this->redirect(array('admin' => false, 'prefix' => false, 'controller' => 'user', 'action' => 'login'));
    }

    /**
     * Fonction permettant de choisir sa collectivité si connexion CAS
     */
    public function chooseCollForCas() {

        $this->loadModel('Collectivite');
        $conns = $this->Collectivite->find(
                'list', array(
            'fields' => array(
                'Collectivite.conn',
                'Collectivite.name'
            ),
            'conditions' => array('Collectivite.active' => true)
                )
        );
        $this->set('conns', $conns);
//        Configure::write('conn', $conns);

        $headers = apache_request_headers();
        foreach ($headers as $header => $value) {
            if ($header == 'Auth-User') {
                if ($value == 'admin' || $value == 'superadmin') {
                    $this->request->data['User']['username'] = 'superadmin';
                } else {
                    $this->request->data['User']['username'] = $value;
                }
            }
        }

//        $headers['Auth-User'] = 'aauzolat@lannion';
        $this->request->data['User']['username'] = $headers['Auth-User'];

        $username = $this->request->data['User']['username'];
        $hasArrobase = strpos($this->request->data['User']['username'], '@');
        if ($hasArrobase) {
            $usernameWithoutSuffixe = strpos($this->request->data['User']['username'], '@');
            $username = substr($this->request->data['User']['username'], 0, $usernameWithoutSuffixe);
        }
        $this->set('username', $username);


        if ($hasArrobase) {
            $suffixConnexion = substr($this->request->data['User']['username'], strpos($this->request->data['User']['username'], '@') + strlen('@'));
            if (!empty($suffixConnexion)) {
                $coll = $this->Collectivite->find('first', array(
                    'conditions' => array(
                        'Collectivite.active' => true,
                        'Collectivite.login_suffix' => $suffixConnexion
                    ),
                    'contain' => false
                ));
                if (!empty($coll)) {
                    $collSuffixe = $coll['Collectivite']['login_suffix'];
                    $connectionColl = $coll['Collectivite']['conn'];
                    Configure::write('conn', $connectionColl);
                    $this->logincasafterchoosingcoll($username, $connectionColl, $collSuffixe);
                } else {
                    $this->Session->setFlash(__d('login', 'login_cas_error'), 'growl', array('type' => 'erreur'));
                }
            }
        } else {
            if (!empty($this->request->data['User']['collectivite'])) {
                $connectionColl = $this->request->data['User']['collectivite'];
                $coll = $this->Collectivite->find('first', array(
                    'conditions' => array(
                        'Collectivite.active' => true,
                        'Collectivite.conn' => $connectionColl
                    ),
                    'contain' => false
                ));
                $collSuffixe = $coll['Collectivite']['login_suffix'];
                Configure::write('conn', $connectionColl);
                $success = $this->logincasafterchoosingcoll($username, $connectionColl, $collSuffixe);
                if ($success) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        $this->layout = 'choose_coll_for_cas';
    }

    /**
     * Fonction permettant d'accéder au login suite à une connexion cAS
     * @param type $username
     * @param type $conn
     */
    public function logincasafterchoosingcoll($username, $conn, $db_login_suffix) {
//debug($username);
//debug($conn);
//debug($db_login_suffix);
//die();
//        $this->layout = 'login';
//        $this->request->data['User']['username'] = $username;
//        $this->Jsonmsg->init();
        if (!empty($db_login_suffix)) {
            $logged = false;
            $isActive = $this->Collectivite->find(
                    'first', array(
                'conditions' => array(
                    'Collectivite.active' => true,
                    'Collectivite.login_suffix' => $db_login_suffix
                )
                    )
            );

            if (!empty($isActive)) {
                Configure::write('conn', $conn);
                Configure::write('login_suffix', $db_login_suffix);

                if ($conn != 'default') {
//                    if ($this->Auth->login()) {
                    $userValid = $this->User->find(
                            'first', array(
                        'conditions' => array(
                            'User.active' => true,
                            'User.username' => $username
                        )
                            )
                    );
                    if (!empty($userValid)) {
                        $logged = true;
                        $this->Session->write('Auth', $userValid);
                        $this->Session->write('Auth.User.connType', 'Database');
                    } else {
                        $logged = false;




                        $this->Session->setFlash(__d('login', 'login_cas_error'), 'growl', array('type' => 'erreur'));
                        $this->Session->destroy();
                    }
//                    }
                } else {
                    // SI CAS actif, en tant qu'admin on peut se connecter à webgfc en tant que superadmin
                    $userValid = $this->User->find(
                            'first', array(
                        'conditions' => array(
                            'User.active' => true,
                            'User.username' => $this->request->data['User']['username']
                        )
                            )
                    );

                    if (!empty($userValid)) {
                        $this->Session->write('Auth', $userValid);
                        $this->Session->write('Auth.User.Env.main', 'superadmin');
                        $logged = true;
                    }
                }
            }

            if ($logged) {
                $this->Session->write('Auth.User.connName', $conn);
                $this->Session->write('Auth.User.login_suffix', $db_login_suffix);
                Configure::write('conn', $conn);
//debug($this->Session->read('Auth'));
                $this->_setConn($conn);
                $this->_getUserConf();
//debug($conn);
//debug($db_login_suffix);
                $this->User->id = $this->Session->read('Auth.User.id');
                $this->User->saveField('last_login', 'NOW()');
                $this->Session->setFlash(__d('default', 'welcome message', true), 'growl');
                $this->Linkeddocs->purge();
//echo '<pre>';var_dump($this->Session);
//die();
                if (!empty($userValid['User']['pwdmodified']) && $userValid['User']['pwdmodified'] == true) {
                    $this->redirect('setInfosPersos');
                } else {
                    $this->redirect(array('controller' => 'environnement', 'action' => 'index'));
                }
            } else /* if ($this->Session->read('Message.flash.message') != __d('default', 'good-bye')) */ {

//                $this->Jsonmsg->init(__d('login', 'login_cas_error'));
//                $this->Jsonmsg->send();
//                debug( $this->Session->read('Message.flash.message') );
//                $this->autoRender = false;
//                $this->Session->setFlash(__d('login', 'login_cas_error', true), 'growl');
//                $this->Jsonmsg->send();
//                $this->Session->setFlash('Les informations ne sont pas enregistrées', 'growl');
                $this->Session->setFlash(__d('login', 'login_cas_error'), 'growl', array('type' => 'erreur'));
            }
            $this->layout = 'login';
        }
    }

    /**
     * Gestion des habilitations d'un rôle (interface graphique)
     *
     * @logical-group Rôle
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du rôle
     * @param string $mode choix du type d'habilitations (func, func_classique, data, meta)
     * @return void
     */
    public function setRights($desktopId) {

//        $aro_id = $this->$desktopId;
        $success = false;
//        $datas = array( 'administration', 'aiguillage', 'creation', 'edition', 'validation', 'documentation', 'create_infos', 'get_infos', 'set_infos', 'get_meta', 'set_meta', 'get_taches', 'set_taches', 'get_affaire', 'set_affaire', 'mes_services', 'recherches', 'suppression_flux', 'carnet_adresse', 'statistiques', 'document_upload', 'document_suppr', 'document_setasdefault', 'relements', 'relements_upload', 'relements_suppr', 'ar_gen', 'envoi_cpy', 'envoi_wkf', 'envoi_ged', 'consult_scan' );
        $aro = $this->Aro->find('first', array('recursive' => -1, 'conditions' => array('foreign_key' => $desktopId)));

        $aro_id = $aro['Aro']['id'];
        $groupId = $aro['Aro']['parent_id'];

        $this->Right = ClassRegistry::init('Right');
        $rightsFromParent = $this->Right->find(
                'first', array(
            'conditions' => array(
                'Right.aro_id' => $groupId
            ),
            'contain' => false
                )
        );

        $computedRights = $this->Right->saveRightSets($aro_id, $this->Rights->parseRightsData($rightsFromParent['Right']), true);

        if (!empty($computedRights)) {
            $valid = array();
            $this->Aro->Permission->deleteAll(array('aro_id' => $aro['Aro']['id']));
            foreach ($computedRights as $key => $val) {
                if ($val) {
                    $valid[] = $this->Acl->allow($aro['Aro'], $key);
                } else {
                    $valid[] = $this->Acl->deny($aro['Aro'], $key);
                }
            }
            if (!in_array(false, $valid, true)) {
                $success = true;
                $this->Jsonmsg->valid();
            }
        }
//debug($valid);
        return $success;
    }

    public function habilPerson($id) {
        if ($id != null) {

            $delegatedDesktops = $this->User->getDelegatedDesktops($id);
            $this->set('delegatedDesktops', $delegatedDesktops);
            $delegateToDesktops = $this->User->getDelegateToDesktops($id);
            $this->set('delegateToDesktops', array_keys($delegateToDesktops));

            $qd = array(
                'conditions' => array(
                    'User.id' => $id
                ),
                'contain' => array(
                    $this->User->Desktop->alias => array(
						'conditions' => array(
							'Desktop.active' => true
						)
					),
                    $this->User->SecondaryDesktop->alias => array(
						'conditions' => array(
							'DesktopsUser.desktop_id IN (SELECT id FROM desktops WHERE active = true)'
						),
                        'order' => array(
                            'DesktopsUser.delegation'
                        )
                    )
                )
            );
            $user = $this->User->find('first', $qd);

            $this->request->data = $user;
            $this->set('user', $user);
        }
    }

	/**
	 * Ajout d'un utilisateur super admin
	 *
	 * @logical-group Utilisateurs
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function add_superadmin() {
		ClassRegistry::init(('User'));
		$this->User = new User();
		$this->User->setDataSource('default');
		if (!empty($this->request->data)) {

			$this->User->begin();
			$this->Jsonmsg->init();

			$password = Security::hash($this->request->data['User']['password'], 'sha256', true);
//			$password2 = Security::hash($this->request->data['User']['password2'], 'sha256', true);

		 	if ($this->User->validatesPassword($this->request->data)) {
				 $user = array(
					 'User' => array(
						 'username' => $this->request->data['User']['username'],
						 'password' => $password,
						 'nom' => $this->request->data['User']['nom'],
						 'prenom' => $this->request->data['User']['prenom'],
						 'mail' => $this->request->data['User']['mail'],
						 'apitoken' => $this->request->data['User']['apitoken']
					 )
				 );

				 $this->User->create($user);
		 	}

			if ($this->User->save()) {
				$this->User->commit();
				$this->Jsonmsg->valid();
			} else {
				$this->User->rollback();
			}

			$this->Jsonmsg->send();
		}
	}

	/**
	 * Liste des supers admin
	 *
	 * @logical-group Utilisateurs
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function index_superadmin() {
		ClassRegistry::init(('User'));
		$this->User = new User();
		$this->User->setDataSource('default');

		$users = $this->User->find(
			'all',
			array(
				'order' => array('User.nom ASC')
			)
		);
		$this->set('users', $users);
	}

	/**
	 * Récupération de la liste des superadmins (ajax)
	 *
	 * @logical-group Collectivité
	 * @user-profile Superadmin
	 *
	 * @access public
	 * @return void
	 */
	public function get_superadmin() {
		$users_tmp = $this->User->find("all", array('order' => 'User.nom'));
		$users = array();
		foreach ($users_tmp as $item) {
			$item['right_edit'] = true;
			$item['right_lock'] = true;
			$item['right_delete'] = true;
			$users[] = $item;
		}
		$this->set('users', $users);
	}


	/**
	 * Modification d'un utilisateur super admin
	 *
	 * @logical-group Utilisateurs
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function edit_superadmin( $user_id ) {
		ClassRegistry::init(('User'));
		$this->User = new User();
		$this->User->setDataSource('default');
		if (!empty($this->request->data)) {
			$this->User->begin();
			$this->Jsonmsg->init();

			if( !empty( $this->request->data['User']['password'])) {
				$this->request->data['User']['password'] = Security::hash($this->request->data['User']['password'], 'sha256', true);
			}

			if ($this->User->save($this->request->data)) {
				$this->User->commit();
				$this->Jsonmsg->valid();
			} else {
				$this->User->rollback();
			}

			$this->Jsonmsg->send();
		}
		else {
			$this->request->data = $this->User->find(
				'first',
				array(
					'conditions' => array(
						'User.id' => $user_id
					),
					'contain' => false,
					'recursive' => -1
				)
			);

		}
	}


	/**
	 * Modificaiton de smots de passe des superadmin
	 *
	 * @logical-group Utilisateurs
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function edit_password( $user_id ) {
		ClassRegistry::init(('User'));
		$this->User = new User();
		$this->User->setDataSource('default');
		if (!empty($this->request->data)) {
			$this->User->begin();
			$this->Jsonmsg->init();

			if (!empty($this->request->data['User']['oldpassword']) && !empty($this->request->data['User']['password']) && !empty($this->request->data['User']['password2'])) {

				$actUser = $this->User->find(
					'first', array(
						'fields' => array(
							'password',
							'id'
						),
						'conditions' => array(
							'User.id' => $user_id
						),
						'contain' => false
					)
				);

				if ($actUser['User']['password'] == Security::hash($this->request->data['User']['oldpassword'], 'sha256', true)) {
					$this->request->data['User']['password'] = Security::hash($this->request->data['User']['password'], 'sha256', true);
					$this->request->data['User']['password2'] = Security::hash($this->request->data['User']['password2'], 'sha256', true);
					if ($this->User->validatesPassword($this->request->data)) {
						if ($this->User->save($this->request->data) ) {
							$this->User->commit();
							$this->Jsonmsg->valid();
							$this->Session->setFlash('Le mot de passe a &eacute;t&eacute; modifi&eacute;', 'growl');
						} else {
							$this->User->rollback();
							$this->Session->setFlash('Erreur lors de la saisie des mots de passe.', 'growl');
						}
					} else {
						$this->User->rollback();
						$this->Session->setFlash('Erreur lors de la saisie des mots de passe.', 'growl');
					}
				} else {
					$this->User->rollback();
					$this->Session->setFlash('L ancien mot de passe n est pas correct, Veuillez reessayer', 'growl');
				}
			}
			else if (empty($this->request->data['User']['oldpassword']) && !empty($this->request->data['User']['password']) && !empty($this->request->data['User']['password2'])) {

				$actUser = $this->User->find(
					'first', array(
						'fields' => array(
							'password',
							'id'
						),
						'conditions' => array(
							'User.id' => $user_id
						),
						'contain' => false
					)
				);

				$this->request->data['User']['password'] = Security::hash($this->request->data['User']['password'], 'sha256', true);
				$this->request->data['User']['password2'] = Security::hash($this->request->data['User']['password2'], 'sha256', true);
				if ($this->User->validatesPassword($this->request->data)) {
					if ($this->User->save($this->request->data) ) {
						$this->User->commit();
						$this->Jsonmsg->valid();
						$this->Session->setFlash('Le mot de passe a &eacute;t&eacute; modifi&eacute;', 'growl');
					} else {
						$this->User->rollback();
						$this->Session->setFlash('Erreur lors de la saisie des mots de passe.', 'growl');
					}
				} else {
					$this->User->rollback();
					$this->Session->setFlash('Erreur lors de la saisie des mots de passe.', 'growl');
				}
			}

			$this->Jsonmsg->send();
		}
		else {

			$this->request->data = $this->User->find(
				'first',
				array(
					'conditions' => array(
						'User.id' => $user_id
					),
					'contain' => false,
					'recursive' => -1
				)
			);

		}

		$userIdConnected = CakeSession::read('Auth.User.id');
		$oldPasswordProvided = false;
		if( $user_id == $userIdConnected ) {
			$oldPasswordProvided = true;
		}
		$this->set('oldPasswordProvided', $oldPasswordProvided);

	}

	/**
     * Suppression d'un superadmin
     *
     * @logical-group Utilisateurs
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant d'un utilisateur
     * @throws NotFoundException
     * @return void
     */
    public function delete_superadmin($id = null)
	{
		ClassRegistry::init(('User'));
		$this->User = new User();
		$this->User->setDataSource('default');
		$user = $this->User->find(
			'first',
			array(
				'recursive' => -1,
				'contain' => false,
				'conditions' => array('User.id' => $id)
			)
		);
		if (empty($user)) {
			throw new NotFoundException();
		}
		$this->User->begin();
		$this->Jsonmsg->init();
		if ($this->User->delete($id)) {
			$this->User->commit();
			$this->Jsonmsg->valid(__d('default', 'delete.ok'));
		} else {
			$this->User->rollback();
		}
		$this->Jsonmsg->send();
	}
}

?>
