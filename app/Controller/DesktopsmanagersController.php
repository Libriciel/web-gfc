<?php

/**
 * Gestion des bureaux
 *
 *
 * DesktopsmanagersController controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller
 */
class DesktopsmanagersController extends AppController {

    /**
     * Controller uses
     *
     * @var array
     * @access public
     */
    public $uses = array('Desktopmanager');
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('export');
	}
    /**
     * Gestion des référentiels de voie
     *
     * @logical-group Carnet d'adresse
     * @user-profile Admin
     *
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'gestionBureaux', true)
        ));
    }

    /**
     * Récupération de la liste des bureaux
     *
     * @logical-group Circuits de traitements
     * @logical-group CakeFlow
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function get_bureaux() {

        $this->Desktopmanager->recursive = -1;
        $querydata = $this->Desktopmanager->search($this->request->data);
        $nbBureau = $this->Desktopmanager->find('count', $querydata);

        $desktopsmanagers_tmp = $this->Desktopmanager->find('all', $querydata);

        $bureauxChef = $this->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true), 'contain' => false));
        $desktopsmanagers = array();

        foreach ($desktopsmanagers_tmp as $i => $item) {
            $chef = '';
            if( isset($item['Desktopmanager']['parent_id']) && !empty( $item['Desktopmanager']['parent_id'] ) ) {
                $chef = $bureauxChef[$item['Desktopmanager']['parent_id']];
            }

            $item['Desktopmanager']['desktops'] = implode(' <br /> ', Hash::extract( $item, 'Desktop.{n}.name' ));
            $item['Desktopmanager']['isdispatch'] = ($item['Desktopmanager']['isdispatch'] === true) ? '<i class="fa fa-check" aria-hidden="true"></i>' : '';
            $item['Desktopmanager']['parent_id'] = $chef;
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $desktopsmanagers[] = $item;
        }
        $this->set('desktopsmanagers', $desktopsmanagers);
        $this->set('nbBureau', $nbBureau);
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Ajout d'un type
     *
     * @logical-group Types
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $this->Desktopmanager->begin();
            $this->Desktopmanager->create($this->request->data);
            if ($this->Desktopmanager->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Desktopmanager->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Desktopmanager->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }

        $bureauxChef = $this->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true), 'contain' => false));
        $this->set('bureauxChef', $bureauxChef);

		$profils = $this->Desktopmanager->Profil->find('list', array( 'conditions' => array('Profil.id' => [VAL_GID, VALEDIT_GID, INIT_GID, DISP_GID, ARCH_GID] ), 'order' => array('Profil.name ASC')));
		$this->set('profils', $profils);
	}

    /**
     * Edition d'un bureau
     *
     * @logical-group Bureaux
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant de la méta-donnée
     * @return void
     */
    public function edit($id = null) {

        if (!empty($this->request->data)) {
            // on regarde les anciens enregistrements entre les bureaux et les profils
            $records = $this->Desktopmanager->DesktopDesktopmanager->find(
                'list',
                array(
                    'fields' => array( "DesktopDesktopmanager.id", "DesktopDesktopmanager.desktop_id" ),
                    'conditions' => array(
                        "DesktopDesktopmanager.desktopmanager_id" => $id
                    )
                )
            );

            // On récupère la valeur des profils déjà associés au bureau
            $oldrecordsids = array_values( $records );

            // on regarde ce qui est retourné à l'enregistrement
            $nouveauxids = Hash::extract( $this->request->data, "Desktop.Desktop.{n}" );
            // si des différences existent on note les ids enlevés
            // Si un profil est ajouté
            $idsenmoins = array_diff( $oldrecordsids, $nouveauxids );
            // Si un profil est enlevé
            $idsenplus = array_diff( $nouveauxids, $oldrecordsids  );

            // on regarde la liste des bannettes contenant des flux pour chaque profil déjà associé au bureau
			if( Configure::read('Bancontenu.AddDesktop') ) {
				$conditions = array(
					'Bancontenu.desktop_id' => $oldrecordsids[0],
					'Bancontenu.etat <>' => 2,
					'Bancontenu.modified >=' => $this->request->data['Desktopmanager']['modified']
				);
				$bannetteAlreadyWithDesktopsIds = $this->Desktopmanager->Desktop->Bancontenu->find(
					'all',
					array(
						'conditions' => $conditions,
						'contain' => false
					)
				);

				// pour chacune de ces bannettes
				foreach ($bannetteAlreadyWithDesktopsIds as $bannette) {
					// si on a ajouté un profil
					if (!empty($idsenplus)) {
						// pour chaque profil ajouté
						foreach ($idsenplus as $dktId) {
							// on récupère les données des bannettes des profils déjà présents
							$datasBannettes = array(
								'bannette_id' => $bannette['Bancontenu']['bannette_id'],
								'courrier_id' => $bannette['Bancontenu']['courrier_id'],
								'etat' => $bannette['Bancontenu'] ['etat'],
								'desktop_id' => $dktId,
								'read' => $bannette['Bancontenu']['read'],
								'desktopmanager_id' => $id
							);
							// on crée des entrées dans la table bancontenus pour les nouvcaux profils avec les flux déjà associés aux autres profils du bureau
							$this->Desktopmanager->Desktop->Bancontenu->begin();
							$this->Desktopmanager->Desktop->Bancontenu->create($datasBannettes);
							if ($this->Desktopmanager->Desktop->Bancontenu->save()) {
								$this->Desktopmanager->Desktop->Bancontenu->commit();
								$this->Jsonmsg->valid();
							} else {
								$this->Desktopmanager->Bancontenu->rollback();
								$this->Jsonmsg->error('Erreur de sauvegarde lors de l\'association du profil au bureau');
							}
						}
					}

					// à contrario, si on a enlevé un profil du bureau
					if (!empty($idsenmoins)) {
						// on regarde les bannettes où il était associé
						$bannetteBeforeEnMoins = $this->Desktopmanager->Desktop->Bancontenu->find(
							'all',
							array(
								'conditions' => array(
									'Bancontenu.desktop_id' => $idsenmoins,
									'Bancontenu.desktopmanager_id' => $id
								),
								'contain' => false
							)
						);

						// pour chacune des bannettes on supprime la présence du flux dans l'environnement du profil dissocié
						foreach ($bannetteBeforeEnMoins as $bannetteToDelete) {
							$this->Desktopmanager->Desktop->Bancontenu->delete($bannetteToDelete['Bancontenu']['id']);
						}

					}
				}
			}

            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );
            $this->Desktopmanager->create();
            if ($this->Desktopmanager->save($this->request->data)) {
                $json['success'] = true;
                $json['message'] = __d('default', 'save.ok');
            }
            $this->Jsonmsg->sendJsonResponse($json);

            $querydata = array(
                'conditions' => array(
                    'Desktopmanager.id' => $id
                ),
                'contain' => false,
                'recursive' => -1
            );
            $desktopmanager = $this->Desktopmanager->find('first', $querydata);
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $journalmsg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le bureau ".$desktopmanager['Desktopmanager']['name']." le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $journalmsg, 'info');
        } else {
            $querydata = array(
                'conditions' => array(
                    'Desktopmanager.id' => $id
                ),
                'contain' => array(
                    'Desktop' => array(
						'Profil'
					)
                ),
//                'recursive' => -1
            );
            $this->request->data = $this->Desktopmanager->find('first', $querydata);

			$profilId = '';
			$profilName = '';
			foreach( $this->request->data['Desktop'] as $d => $desktop ) {
				$profilId = $desktop['profil_id'];
				$profilName = $desktop['Profil']['name'];
			}
			$this->set( 'profilId', $profilId );
			$this->set( 'profilName', $profilName );
            //
            $circuitsIds = $this->Desktopmanager->Desktop->User->Courrier->Traitement->Circuit->listeCircuitsParDesktopsmanagers($id);
            $circuitName = array();
            foreach ($circuitsIds as $circuitId) {
                $circuitName['name'][$circuitId] = $this->Desktopmanager->Desktop->User->Courrier->Traitement->Circuit->getLibelle($circuitId);
            }
            $this->set('circuitName', $circuitName);
            $this->set('id', $id);
        }


        $this->set('desktopmanagerId', $id);
        $intitulesagents = $this->Desktopmanager->Intituleagent->find('list', array('order' => array( 'Intituleagent.name ASC')));
        $this->set('intitulesagents', $intitulesagents);
        $bureauxChef = $this->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true, 'Desktopmanager.id <>' => $id), 'contain' => false));
        $this->set('bureauxChef', $bureauxChef);

		if(empty( $this->request->data['Desktop'] ) ) {
			$profils = $this->Desktopmanager->Profil->find('list', array( 'conditions' => array('Profil.id' => [VAL_GID, VALEDIT_GID, INIT_GID, DISP_GID, ARCH_GID] ), 'order' => array('Profil.name ASC')));
		}
		else {
			// On en regarde QUE le type de profil sélectionné (aiguilleur, initi	ateur ...) et les agents associés à ce profil
			$profils = [ $profilId => $profilName 	];
		}

		$this->set('profils', $profils);
		$desktopsmanagers = $this->Desktopmanager->Desktop->find('list', array('conditions' => array('Desktop.active' => true, 'Desktop.profil_id' => $profilId)));
		$this->set('desktopsmanagers', $desktopsmanagers);
    }

    /**
     * Suppression d'une base adresse
     *
     * @logical-group BAN
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant de la BAN
     * @return void
     */
    public function delete($id = null) {
        $json = array(
            'message' => __d('default', 'delete.error'),
            'success' => false
        );

        $this->Desktopmanager->begin();
        if ($this->Desktopmanager->delete($id)) {
            $json['success'] = true;
        }
        if ($json['success']) {
            $this->Desktopmanager->commit();
            $json['message'] = __d('default', 'delete.ok');
        } else {
            $this->Desktopmanager->rollback();
        }
        $this->Jsonmsg->sendJsonResponse($json);
    }

	/**
	 * Fonction permettant de retourner tous les profils actifs et hors parapheur selon leur type (valideur, Initiateur ...)
	 * Les données servent pour ne pas laisser la possibilité d'associer 2 profils différents dans un même bureau
	 * @param $profils
	 * @return jsonData
	 */
	public function getAllDesktopsByProfils($profils)
	{
		$this->autoRender = false;
		$ndesktops = [];
		if( !empty($profils) ) {
			$desktops = $this->Desktopmanager->Desktop->find(
				'list',
				array(
					'conditions' => array(
						'Desktop.profil_id' => $profils,
						'Desktop.id NOT IN' => ['-1','-3'],
						'Desktop.active' => true
					),
					'contain' => false
				)
			);

			foreach ($desktops as $key => $desktop) {
				$ndesktops[] = [
					'id' => $key,
					'name' => $desktop
				];
			}
		}
		return json_encode($ndesktops);


	}



	public function export() {
		$conditions = array();

		$querydata = array(
			'order' => array(
				'Desktopmanager.name'
			),
			'contain' => array(
				'Desktop' => array(
					'order' => array('Desktop.name ASC')
				),

			)
		);
		$results = $this->Desktopmanager->find('all', $querydata);
		$this->set('results', $results);

		$this->layout = '';
	}
}
