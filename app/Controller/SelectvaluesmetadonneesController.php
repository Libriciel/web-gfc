<?php

/**
 * Méta-données
 *
 * Selectvaluesmetadonnees controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class SelectvaluesmetadonneesController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Selectvaluesmetadonnees';

    /**
     * Ajout d'une méta-donnée
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function add($metadonnee_id) {
        if (!empty($this->request->data)) {

            $this->request->data['Selectvaluemetadonnee']['metadonnee_id'] = $metadonnee_id;

            $exit = $this->Selectvaluemetadonnee->find('count', array('conditions' => array('Selectvaluemetadonnee.metadonnee_id' => $metadonnee_id, 'Selectvaluemetadonnee.name' => $this->request->data['Selectvaluemetadonnee']['name'])));
            $this->Jsonmsg->init(__d('default', 'delete.error'));
            if (!$exit) {
                $this->Selectvaluemetadonnee->create($this->request->data);

                if ($this->Selectvaluemetadonnee->save()) {
                    $this->Jsonmsg->valid();
                }
            }
            $this->Jsonmsg->send();
        } else {
            $this->set('metadonneeId', $metadonnee_id);
            $this->set('selectvaluesmetadonnees', $this->Selectvaluemetadonnee->find('list'));
        }
    }

    /**
     * Edition d'un méta-donnée
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant de la méta-donnée
     * @return void
     */
    public function edit($id = null) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Selectvaluemetadonnee->create($this->request->data);
            if ($this->Selectvaluemetadonnee->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $querydata = array(
                'contain' => array(
                    $this->Selectvaluemetadonnee->Metadonnee->alias,
                ),
                'conditions' => array(
                    'Selectvaluemetadonnee.id' => $id
                )
            );
            $this->request->data = $this->Selectvaluemetadonnee->find('first', $querydata);
        }
        $this->set('metadonneeId', $this->request->data['Metadonnee']['id']);
        $this->set('selectvaluemetadonneeId', $id);
        $this->set('selectvaluesmetadonnees', $this->Selectvaluemetadonnee->find('list'));
    }

    /**
     * Suppression d'une méta-donnée (delete)
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant de la méta-donnée
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Selectvaluemetadonnee->begin();
        if ($this->Selectvaluemetadonnee->delete($id)) {
            $this->Selectvaluemetadonnee->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Selectvaluemetadonnee->rollback();
        }
        $this->Jsonmsg->send();
    }

}

?>
