<?php

/**
 * Profils
 *
 * Profils controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class ProfilsController extends AppController {

    /**
     * Controller name
     *
     * @var array
     * @access public
     */
    public $name = 'Profils';

    /**
     * Controller helpers
     *
     * @var array
     * @access public
     */
    public $helpers = array('DataAcl.DataAcl');

    /**
     * Controller components
     *
     * @access public
     * @var array
     */
    public $components = array('Rights', 'Ajaxformvalid' , 'LdapManager.GroupManager', 'AuthManager.AclManager' );

    /**
     * Controller index method
     *
     * @return void
     */
    //TODO: supprimer les fonctions commentées
//	public function index() {
//		$this->set('ariane', array(__d('menu', 'Administration', true), __d('menu', 'gestionProfils', true)));
//	}

    /**
     * Récupération de la liste des profils
     *
     * @logical-group Profils
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function getProfils() {
        $groups_tmp = $this->Profil->find("all", array('conditions' => array( 'Profil.name <>' => 'Superadmin'), 'order' => 'Profil.name'));
        $groups = array();
        foreach ($groups_tmp as $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = $this->Profil->isDeletable($item['Profil']['id']);
            $groups[] = $item;
        }

//         $this->ldebug($groups);

        $groups_base_tmp = $this->Profil->find("all", array('conditions' => array( 'Profil.name <>' => 'Superadmin'), 'order' => 'Profil.name'));
        $groups_base = array();
        foreach ($groups_base_tmp as $item) {
            $item['right_view'] = true;
            $item['right_edit'] = false;
            $item['right_delete'] = false;
            $groups_base[] = $item;
        }

        $this->set('groups', $groups);
        $this->set('groups_base', $groups_base);
        $this->set('nbItem', count($groups));
    }

    /**
     * Visualisation d'un profil de base
     *
     * @logical-group Profils
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du profil
     * @return void
     */
    public function view($id) {
        if ($id != null) {
            $this->set('id', $id);
            $group = $this->Profil->find(
                    'first', array(
                'fields' => array(
                    'Profil.id',
                    'Profil.name'
                ),
                'conditions' => array(
                    'Profil.id' => $id
                ),
                'contain' => false
                    )
            );
            $this->request->data = $group;
        }
    }

    /**
     * Ajouter un profil personnalisé
     *
     * @logical-group Profils
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            //creation du groupe
            $this->Profil->create();
            if ($this->Profil->save($this->request->data)) {
                //mise à jour de l'alias de l'aro
                //recuperation de l'aro correspondant au groupe nouvellement créé
                $aro = $this->Acl->Aro->find('first', array('conditions' => array('Aro.model' => 'Profil', 'Aro.foreign_key' => $this->Profil->id)));
                $aro['Aro']['alias'] = $this->Profil->field('name');
                $this->Acl->Aro->create($aro);
                $this->Acl->Aro->save();
                //duplication des droits du parent vers le groupe nouvellement créé
                $parent_group_id = $this->request->data['Profil']['profil_id'];
                //recuperation de l'aro correspondant au groupe nouvellement créé
                $aroParent = $this->Acl->Aro->find('first', array('conditions' => array('Aro.model' => 'Profil', 'Aro.foreign_key' => $parent_group_id)));
                //recuperation des droits du parent
                $rights = $this->Acl->Aro->Permission->find('all', array('conditions' => array('Permission.aro_id' => $aroParent['Aro']['id'])));
                //copie des droits
                $newRights = array();
                foreach ($rights as $kright => $right) {
                    $permission = $right['Permission'];
                    unset($permission['id']);
                    $permission['aro_id'] = $aro['Aro']['id'];
                    $newRights[$kright] = array('Permission' => $permission, 'Aro' => $aro['Aro'], 'Aco' => $right['Aco']);
                }

                $validCopy = $this->Acl->Aro->Permission->saveAll($newRights);
                if (!empty($validCopy)) {
                    $this->loadModel('Right');
                    $rights = $this->Right->find('first', array('conditions' => array('aro_id' => $this->request->data['Profil']['profil_id'])));
                    $rights['Right']['id'] = '';
                    $rights['Right']['aro_id'] = $aro['Aro']['id'];
                    $rights['Right']['model'] = $aro['Aro']['model'];
                    $rights['Right']['foreign_key'] = $aro['Aro']['foreign_key'];
                    $this->Right->create($rights);
                    $validRight = $this->Right->save();
                    if (!empty($validRight)) {
                        $this->Jsonmsg->valid();
                    }
                }
            }
            $this->Jsonmsg->send();
        }
        $qd = array(
            'conditions' => array(
                'NOT' => array(
                    'Profil.id' => array(SUPERADMIN_GID, ADMIN_GID, DISP_GID)
                ),
                'Profil.active' => true
            )
        );
        $this->set('groups', $this->Profil->find('list', $qd));
    }

    /**
     * Edition d'un profil personnalisé
     *
     * @logical-group Profils
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du profil
     * @return void
     */
    public function edit($id) {
        $this->loadModel('Connecteur');
        $connecteurLdap = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%LDAP%'
                ),
                'contain' => false
            )
        );

        $isLdap = false;
        if( !empty( $connecteurLdap ) ) {
            $isLdap = $connecteurLdap['Connecteur']['use_ldap'];
        }
        $this->set( 'isLdap', $isLdap);


        if( !empty($this->request->data)) {
            $this->Jsonmsg->init();


            $modelGroup = $this->request->data['Profil']['ModelGroup'];
//            unset($this->request->data['Profil']['ModelGroup']);
            if (isset($modelGroup)) {
                $this->GroupManager->setGroupsProfil($id, $modelGroup);
            }

            $this->Profil->begin();
            $this->Profil->create($this->request->data);
            $saved = $this->Profil->save();
            if (!empty($saved)) {
                $this->Profil->commit();
                $this->Jsonmsg->valid(__d('profil', 'profil.save.infos') . __d('default', 'save.ok'));
            }
            else {
                $this->Profil->rollback();
                $this->Jsonmsg->error(__d('profil', 'profil.save.notallowed'));
            }
            $this->Jsonmsg->send();

        }
        else if ($id != null) {
            $this->set('id', $id);

            $profil = $this->Profil->find(
                'first',
                array(
                    'conditions' => array(
                        'Profil.id' => $id
                    ),
                    'contain' => false,
                    'recrusive' => -1
                )
            );
            $this->request->data = $profil;

            $this->set( 'profil', $profil );

            $this->GroupManager->getGroupsProfil($id);
        }
    }

    /**
     * Edition d'un profil personnalisé
     *
     * @logical-group Profils
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du profil
     * @return void
     */
    public function setInfos($id = null) {

        if (!empty($this->request->data['Profil'])) {
            $this->Jsonmsg->init();
            $canSave = true;
            $this->Profil->create($this->request->data);
            //verification de l'autorisation de modification du groupe
            if (isset($this->request->data['Profil']['id']) && $this->request->data['Profil']['id'] != '') {
                $this->Profil->id = $this->request->data['Profil']['id'];
                $canSave = !$this->Profil->field('readonly');
            }
            if ($canSave) {
                $saved = $this->Profil->save();
                if (!empty($saved)) {
                    $this->Jsonmsg->valid(__d('profil', 'profil.save.infos') . __d('default', 'save.ok'));
                }
            } else {
                $this->Jsonmsg->init(__d('profil', 'profil.save.notallowed'));
            }
            $this->Jsonmsg->send();
        }
        $this->Profil->id = $id;
        $this->request->data = $this->Profil->read();
    }

    /**
     *
     * @param integer $id
     */
    //TODO: supprimer les fonctions commentées
//	public function getInfos($id = null) {
//		$this->Profil->id = $id;
//		$this->request->data = $this->Profil->read();
//	}

    /**
     * Suppression des droits des AROs dépendants
     *
     * @access private
     * @param integer $aroId indetifiant de l'ARO (ACL)
     * @return boolean
     */
    private function _flushDependantRights($aroId) {
        $this->loadModel('Right');
        $qdAros = array(
            'recursive' => -1,
            'fields' => array(
                'Aro.id'
            ),
            'conditions' => array(
                'Aro.parent_id' => $aroId
            )
        );
        $aros = Hash::flatten($this->Acl->Aro->find('all', $qdAros));
        return $this->Acl->Aro->Permission->deleteAll(array('aro_id' => $aros)) && $this->Right->deleteAll(array('aro_id' => $aros));
    }

    /**
     * Edition de la liste des habilitations de fonctionnalités d'un profil personnalisé
     *
     * @logical-group Profils
     * * @logical-group Gestion des habilitations
     *
     * @access public
     * @param integer $id identifiant du profil
     * @param string $mode détermine quel formulaire afficher (func, func_classique)
     * @return void
     */
    public function setDroitsFunc($id = null, $mode = 'func') {
        //mise a jour des droits sur les meta donnees
        if (!empty($this->request->data)) {
            $this->loadModel('Right');
            $this->Jsonmsg->init();
            $this->Acl->Aro->begin();
            $validGlobal = false;

            //Enregistrement en mode classique (a supprimer dans la prochaine version)
            if (!empty($this->request->data['Acl']['Type'])) {
                if ($this->request->data['Acl']['Type'] == 'Acl') {
                    if ($this->Acl->setRights($this->request->data['Requester'], $this->request->data['Rights'])) {
                        $qdAro = array(
                            'conditions' => array(
                                'model' => key($this->request->data['Requester']),
                                'foreign_key' => current($this->request->data['Requester'])
                            )
                        );
                        $aro = $this->Acl->Aro->find('first', $qdAro);
                        $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $aro['Aro']['id'])));
                        $rights['Right']['mode_classique'] = true;
                        $rights['Right']['model'] = 'Profil';
                        $rights['Right']['foreign_key'] = current($this->request->data['Requester']);
                        $this->Right->create($rights);
                        $saved = $this->Right->save();
                        if (!empty($saved) && $this->_flushDependantRights($aro['Aro']['id'])) {
                            $validGlobal = true;
                        }
                    }
                }
                //Enregistrement en mode simplifié
            } else if (!empty($this->request->data['Right'])) {
                $aro_id = $this->request->data['Right']['aro_id'];
                $aro = $this->Acl->Aro->find('first', array('recursive' => -1, 'conditions' => array('id' => $aro_id)));
                $computedRights = $this->Right->saveRightSets($aro_id, $this->Rights->parseRightsData($this->request->data['Right']), true);
                if (!empty($computedRights)) {
                    $valid = array();
                    $this->Acl->Aro->Permission->deleteAll(array('aro_id' => $aro_id));
                    foreach ($computedRights as $key => $val) {
                        if ($val) {
                            $valid[] = $this->Acl->allow($aro['Aro'], $key);
                        } else {
                            $valid[] = $this->Acl->deny($aro['Aro'], $key);
                        }
                    }
                    if (!in_array(false, $valid, true) && $this->_flushDependantRights($aro_id)) {
                        $validGlobal = true;
                    }
                }
            }

            if ($validGlobal) {
                $this->Jsonmsg->valid(__d('profil', 'profil.save.rights') . __d('default', 'save.ok'));
                $this->Acl->Aro->commit();
                $this->Acl->flushCache();
            } else {
                $this->Acl->Aro->rollback();
            }

            $this->Jsonmsg->send();
        } else {
            if ($mode == 'func') {
                $this->loadModel('Right');
                $aro = array('model' => 'Profil', 'foreign_key' => $id);
                $aro_id = $this->Acl->Aro->field('id', $aro);
                $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $aro_id)));
                $this->set('rights', $rights);
                $this->set('tabsForSets', json_encode($this->Right->getTabsForSets()));
                $this->set('rolePers',true);
                $profil=array('3'=>'creation',
                                    '4'=>'validation',
                                    '5'=>'edition',
                                    '6'=>'documentation',
                                    '7'=>'aiguillage'
                      );
                $parentProfilId=$this->Profil->field('profil_id',array('Profil.id' => $id));
                $this->set('profil',$profil[$parentProfilId]);
            } else if ($mode == 'func_classique') {
                $this->Profil->id = $id;
                $daco = $this->Profil->node();
                $rights = $this->Acl->getRightsGrid($daco[0], array(), array('actions' => array('read'), 'models' => array('Profil')));
                $this->set('rights', $rights);
                $this->set('id', $id);
            }
            $this->set('mode', $mode);
        }
    }

    /**
     * Vérificatoin de l'autorisation d'accès d'un ARO (profil, rôle) à un ACO (action d'un controlleur)
     *
     * @access private
     * @param array $aros tableau représentant un ARO (ACL)
     * @param string $aco alias d'un ACO (ACL) exemple:  "/controllers/controller/action"
     * @param mixed $action action CRUD ou "*"
     * @return boolean si l'ARO (profil, rôle) et autorisé à accéder à l'ACO (action d'un controlleur) alors on retourne true sinon on retourne false
     */
    private function _funcCheck($aros, $aco, $action = "*") {
        $checks = array();
        foreach ($aros as $aro) {
            $checks[] = $this->Acl->check($aro, $aco, $action);
        }
        return in_array(true, $checks, true);
    }

    /**
     * Récupération de la grille complète des droits pour une hiérarchie d'AROs (profils, rôles)	 *
     *
     * @param array $aros tableau contenants des ARO (profils, rôles) exemple:  $aros = array(array('model' => 'Profil', 'foreign_key' => 12), array('model' => 'Desktop', 'foreign_key' => 15))
     * @param array $acos tableau contenants des alias d'ACO (action d'un controlleur) exemple:  array('controllers/controller/action', 'controllers/controller/other_action')
     * @param array $options
     * @return array
     */
    private function _getFuncRightsGrid($aros, $acos = array(), $options = array('actions' => '*', 'alias' => '')) {
        if (empty($acos)) {
            $acos = $this->Acl->Aco->find('threaded');
        }
        $return = array();
        foreach ($acos as $aco) {
            $return[$aco['Aco']['id']] = $this->_getFuncRights($aros, $aco, $options);
        }
        return $return;
    }

    /**
     * Récupération des droits pour un hiérarchie d'AROs
     * cette méthode appelle récursivement _getFuncRightsGrid pour les enfants des AROs
     *
     * @param array $aros
     * @param string $aco
     * @param array $options
     * @return array
     */
    private function _getFuncRights($aros, $aco, $options = array('actions' => '*')) {
        $options['alias'] .= '/' . $aco['Aco']['alias'];
        $options['alias'] = ltrim($options['alias'], '/');

        $actions = $options['actions'];
        $checks = array();
        if ($actions == '*' || is_array($actions) && in_array('*', $actions)) {
            $chk = $this->_funcCheck($aros, $options['alias']);
            $checks['read'] = $chk;
        }
        if ($actions == 'create' || is_array($actions) && in_array('create', $actions)) {
            $checks['create'] = $this->_funcCheck($aros, $options['alias'], 'create');
        }
        if ($actions == 'read' || is_array($actions) && in_array('read', $actions)) {
            $checks['read'] = $this->_funcCheck($aros, $options['alias'], 'read');
        }
        if ($actions == 'update' || is_array($actions) && in_array('update', $actions)) {
            $checks['update'] = $this->_funcCheck($aros, $options['alias'], 'update');
        }
        if ($actions == 'delete' || is_array($actions) && in_array('delete', $actions)) {
            $checks['delete'] = $this->_funcCheck($aros, $options['alias'], 'delete');
        }

        $return = array(
            'checks' => $checks,
            'alias' => $options['alias']
        );
        if (!empty($aco['children'])) {
            $return['children'] = $this->_getFuncRightsGrid($aros, $aco['children'], $options);
            $this->_getFuncRightsGrid($aros, $aco['children']);
        }
        return $return;
    }

    /**
     * Récupération de la liste des habilitations de fonctionnalités (profils de base et personnalisés)
     *
     * @logical-group Profils
     * * @logical-group Gestion des habilitations
     *
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du rôle
     * @param string $mode détermine quel formulaire afficher (func, func_classique)
     * @return void
     */
    public function getDroitsFunc($id = null, $mode = 'func') {
        if ($mode == 'func') {
            $this->loadModel('Right');
            $aro = array('model' => 'Profil', 'foreign_key' => $id);
            $aro_id = $this->Acl->Aro->field('id', $aro);
            $rights = $this->Right->find('first', array('conditions' => array('Right.aro_id' => $aro_id)));
            $this->set('rights', $rights);
        } else if ($mode == 'func_classique') {
            $this->Profil->id = $id;
            $this->request->data = $this->Profil->read();
            $aros = array(array('model' => 'Profil', 'foreign_key' => $id));
            set_time_limit(120);
            $this->set('requester', array('model' => 'Profil', 'foreign_key' => $id));
            $this->set('rights', $this->_getFuncRightsGrid($aros));
        }
        $this->set('mode', $mode);
    }

    /**
     * Suppression d'un profil personnalisé
     *
     * @logical-user Profils
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du rôle
     * @return void
     */
    public function delete($id = null) {
        $group = $this->Profil->find('first', array('conditions' => array('Profil.id' => $id)));

        if (empty($group)) {
            throw new NotFoundException();
        }

        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Profil->begin();
        if ($this->Profil->delete($id, true)) {
            $this->Profil->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Profil->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     *
     * @param type $field
     * @param type $collectiviteId
     * @return type
     */
    public function ajaxformvalid($field = null) {
//         $this->ldebug($this->request->data);
        $this->Ajaxformvalid->valid(array('Profil'), $this->request->data);
    }

}

?>
