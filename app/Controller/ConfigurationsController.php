<?php

App::uses('ShellDispatcher', 'Console');

/**
 * Configurations
 *
 * Configurations controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

App::uses('AppController', 'Controller');
App::uses('File', 'Utility');
class ConfigurationsController extends AppController {

    /**
     *
     * Controller name
     *
     * @var array
     */
    public $name = 'Configurations';

    /**
     *
     * Controller uses
     *
     * @var array
     */
    public $uses = array('Configuration');
	/**
	 * Fonction de rappel beforeFilter
	 *
	 * @description  Paramétrage spécifique du composant Auth. Si on est connecté en tant que superadmin, on peut accèder à toutes les pages sans utiliser les Acl de CakePHP.
	 *
	 * @access public
	 * @return void
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->Auth->User('id') && Configure::read('conn') == "default") {
			$this->Auth->allow('*');
		}
	}
    /**
     * Gestion des collectivités (interface graphique)
     *
     * @logical-group Collectivité
     * @user-profile Superadmin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement">' . __d('menu', 'Administration') . '</a>',
            __d('menu', 'gestionCollectivites')));
    }


    /**
     * Fonction permettant de définir le paramètrage de la page d'accueil
     *
     */
    public function config() {
        $entreeExistante = $this->Configuration->find('first', array('contain' => false));
        if (!empty($this->request->data) ) {

            if( empty($entreeExistante) ) {
                $json = array(
                    'message' => __d('default', 'save.error'),
                    'success' => false
                );
                $this->request->data['showCustom'] = filter_var( $this->request->data['showCustom'], FILTER_VALIDATE_BOOLEAN );
                $this->request->data['showInformation'] = filter_var( $this->request->data['showInformation'], FILTER_VALIDATE_BOOLEAN );
                $this->request->data['informationText'] = str_replace("'", "&#39;", $this->request->data['informationText']);
                $this->request->data['subText'] = str_replace("'", "&#39;", $this->request->data['subText']);
                $configuration['Configuration']['config'] = json_encode( $this->request->data );

                $this->Configuration->begin();
                $this->Configuration->create($configuration);

                if ($this->Configuration->save()) {
                    $json['success'] = true;
                }
                if ($json['success']) {
                    $this->Configuration->commit();
                    $json['message'] = __d('default', 'save.ok');
                } else {
                    $this->Configuration->rollback();
                }
                $this->Jsonmsg->sendJsonResponse($json);
            }
            else {
                $json = array(
                    'message' => __d('default', 'save.error'),
                    'success' => false
                );
                $this->request->data['showCustom'] = filter_var( $this->request->data['showCustom'], FILTER_VALIDATE_BOOLEAN );
                $this->request->data['showInformation'] = filter_var( $this->request->data['showInformation'], FILTER_VALIDATE_BOOLEAN );
                $this->request->data['informationText'] = str_replace("'", "&#39;", $this->request->data['informationText']);
                $this->request->data['subText'] = str_replace("'", "&#39;", $this->request->data['subText']);
                $configuration['Configuration']['config'] = json_encode( $this->request->data );

                $this->Configuration->updateAll(
                    array( 'Configuration.config' => "'".$configuration['Configuration']['config']."'"),
                    array( 'Configuration.id' => $entreeExistante['Configuration']['id'])
                );

                $json['success'] = true;
                if ($json['success']) {
                    $this->Configuration->commit();
                    $json['message'] = __d('default', 'save.ok');
                } else {
                    $this->Configuration->rollback();
                }
                $this->Jsonmsg->sendJsonResponse($json);
                $entreeExistante['Configuration']['config'] = $configuration['Configuration']['config'];
            }
        }
        else {
            $entreeExistante = $this->Configuration->find('first', array('contain' => false));
            if( empty($entreeExistante) ) {
                $entreeExistante['Configuration']['config'] = '';
            }
        }
        $this->set('entreeExistante', $entreeExistante);
    }
}

?>
