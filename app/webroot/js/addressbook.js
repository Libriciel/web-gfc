/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    $('.id').hide();
    $('.nom').hide();
    $('.prenom').hide();
    $('.adbook_id').hide();
    $('.org_id').hide();

});

function addressbookDerepile(btn) {
    var add_id = btn.parents('tr').find('.id').text();
    if (btn.hasClass('depile')) {
        btn.attr('class', 'repile');
        btn.find('i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
        $('.organisme.parentA_' + add_id).show();
    } else {
        btn.attr('class', 'depile');
        btn.find('i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        $('.parentA_' + add_id + ':visible').hide();
        $('.parentA_' + add_id).find('i').each(function () {
            var icon = $(this);
            if (icon.hasClass('fa-chevron-down')) {
                icon.removeClass('fa-chevron-down').addClass('fa-chevron-right');
            }
        });
    }
}

function organismeDerepile(btn) {
    var add_id = btn.parents('tr').find('.id').text();
    if (btn.hasClass('depile')) {
        btn.attr('class', 'repile');
        btn.find('i').removeClass('fa-chevron-right').addClass('fa-chevron-down');
        $('.contact.parentO_' + add_id).show();
    } else {
        btn.attr('class', 'depile');
        btn.find('i').removeClass('fa-chevron-down').addClass('fa-chevron-right');
        $('.parentO_' + add_id).hide();
    }
}

$('.derepile.addressbookDR').click(function () {
    var btn = $(this);
    addressbookDerepile(btn);
});

$('.derepile.organismeDR').click(function () {
    var btn = $(this);
    organismeDerepile(btn);
});

//$('.viewAddressbook').click(function () {
//    var id = $(this).attr('id').substring(12);
//    var href = '/addressbooks/get_organisme/' + id;
//    var chargeZone = '#zone_organisme .panel-body';
//    $("#zone_carnet").removeAttr('class').addClass("col-sm-2 col-sm-2");
//    $("#zone_organisme").removeAttr('class').addClass("col-sm-8 col-sm-8");
//    $("#zone_contact").removeAttr('class').addClass("col-sm-2 col-sm-2");
//    viewItem(href, chargeZone);
//});
//$('.viewOrganisme').click(function () {
//    var id = $(this).attr('id').substring(10);
//    var href = '/organismes/get_contact/' + id;
//    var chargeZone = '#zone_contact .panel-body';
//    $("#zone_carnet").removeAttr('class').addClass("col-sm-2 col-sm-2");
//    $("#zone_organisme").removeAttr('class').addClass("col-sm-2 col-sm-2");
//    $("#zone_contact").removeAttr('class').addClass("col-sm-8 col-sm-8");
//    viewItem(href, chargeZone);
//});
//$('.viewContact').click(function () {
//    var id = $(this).attr('id').substring(8);
//    var href = '/contacts/view/' + id;
//    var chargeZone = '#infos .content';
//    viewItem(href, chargeZone);
//});
//$('.editAddressbook').click(function () {
//    var id = $(this).attr('id').substring(12);
//    var href = '/addressbooks/edit/' + id;
//    editItem(href);
//});
//$('.editOrganisme').click(function () {
//    var id = $(this).attr('id').substring(10);
//    var href = '/organismes/edit/' + id;
//    editItem(href);
//});
$('.editContact').click(function () {
    var id = $(this).attr('id').substring(8);
    var href = '/contacts/edit/' + id;
    var isSansContact = $(this).parent().parent().text();
    isSansContact.includes("Sans contact");
    if( !isSansContact.includes("Sans contact") ) {
        editItem(href);
    }
    else {
        var title = "Modifier le contact";
        var text = "Ce contact ne peut être modifié";
        swal(
            'Oops...',
            title + "<br>" + text,
            'warning'
        );
    }
});
$('.deleteAddressbook').click(function () {
    var id = $(this).attr('id').substring(12);
    var href = '/addressbooks/delete/' + id;
    var section = "addressbooks";
//    var actionAfter = loadAddressbooks();
    deleteItem(href, section, id);
//    loadAddressbooks();
});
$('.deleteOrganisme').click(function () {
    var id = $(this).attr('id').substring(10);
    var href = '/organismes/delete/' + id;
    var addressbookId = $(this).attr('addressbookId');
    var section = "organisme";
//    var actionAfter = refreshOrganisme(id);
    deleteItem(href, section, addressbookId);
//    refreshOrganisme(id);
});
$('.deleteContact').click(function () {
    var id = $(this).attr('id').substring(8);
    var href = '/contacts/delete/' + id;
    var organismeId = $(this).attr('organismeId');
    var section = "contact";
//    var actionAfter = refreshContact(id);
    deleteItem(href, section, organismeId);
//    refreshContact(id);
});

//$('.exportAddressbook').click(function () {
//    var id = $(this).attr('id').substring(12);
//    var href = '/addressbooks/export/' + id;
//    exportItem(href);
//});
//$('.exportOrganisme').click(function () {
//    var id = $(this).attr('id').substring(10);
//    var href = '/organismes/export/' + id;
//    exportItem(href);
//});
//$('.exportContact').click(function () {
//    var id = $(this).attr('id').substring(8);
//    var href = '/contacts/export/' + id;
//    exportItem(href);
//});

//function viewItem(href, chargeZone) {
//    gui.request({
//        url: href,
//        updateElement: $(chargeZone),
//        loader: true,
//        loaderMessage: gui.loaderMessage
//    });
//    if (chargeZone == "#infos .content") {
//        $('#infos .panel-title').empty();
//        $('#infos').modal('show');
//    }
//}

function editItem(href) {
    $('#infos').modal('show');
    gui.request({
        url: href,
        updateElement: $('#infos .content'),
        loader: true,
        loaderShowDiv: $('#liste .content'),
        loaderMessage: gui.loaderMessage
    });
}

function deleteItem(href, section, id) {
    swal({
                    showCloseButton: true,
        title: "Confirmation de suppression",
        text: "Voulez-vous supprimer cet élément ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
        cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
    }).then(function (data) {
        if (data) {
            gui.request({
                url: href,
                loader: true,
                loaderMessage: gui.loaderMessage
            }, function (data2) {
                switch (section) {
                    case 'contact':
                        console.log('111111');
						refreshContactFromOrganismeId(id);
                        break;
                    case 'organisme':
                        console.log('222222');
                        refreshOrganisme(id);
                        break;
                    case 'addressbooks':
                        console.log('333333');
                        loadAddressbooks();
                        break;
                }
                getJsonResponse(data2);
//                actionAfter;
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Annulé!",
                text: "Vous n'avez pas supprimé, ;) .",
                type: "error",

            });
        }
    });
    $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
}

//function exportItem(href) {
//    window.location.href = href;
//}
