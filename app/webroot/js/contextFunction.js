/*
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * @author JIN Yuzhu
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 */

//navigation Tab Context
function affichageTabContext(fluxId) {
//content main document de context

    if (typeof (mainDoc) != "undefined" && mainDoc != null) {
        if ($('#preview').length > 0 && $('#preview').is(':visible')) {
            var selectedImage = $('#mainDocTmp .ctxdoc_name img').clone();
            var selectedDoc = $('#mainDocTmp .filename').html();
            $('.document_preview .header .selected_document').html('');
            $('.document_preview .header .selected_document').append(selectedImage);
            var truncatedSelectedDoc = selectedDoc.substr(0, 40);
            if (truncatedSelectedDoc != selectedDoc) {
                truncatedSelectedDoc += "...";
            }
            $('.document_preview .header .selected_document').append(' ' + truncatedSelectedDoc);
            $('.document_preview .content').html(gui.loaderMessage);
            gui.request({
                url: '/Documents/getPreview/' + mainDocId,
                updateElement: $('.document_preview .content'),
                loader: true,
                loaderMessage: gui.loaderMessage
//                updateElement: $('#preview')
            }
            , function (data) {
                if ($('.document_preview .content').length != 0) {
                    $('.document_preview .content').html(data);
                } else {
                    $('#preview').html(data);
                }
            }
            );
        }
    }
//content lsit document de context
    if ($('#documents').length > 0 && $('#documents').is(':visible')) {
//        $('#preview .document_preview .content').empty();
        gui.request({
            url: '/courriers/getDocuments/' + fluxId,
            updateElement: $('#documents'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
    if ($('#relements').length > 0 && $('#relements').is(':visible')) {
//        $('#preview .document_preview .content').empty();
        gui.request({
            url: '/courriers/getRelements/' + fluxId,
            updateElement: $('#relements'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
//content Accusés de réception
    if ($('#ars').length > 0 && $('#ars').is(':visible')) {
//        $('#preview .document_preview .content').empty();
        gui.request({
            url: '/courriers/getArs/' + fluxId,
            updateElement: $('#ars'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
//content circuit
    if ($('#circuit').length > 0 && $('#circuit').is(':visible')) {
//        $('#preview .document_preview .content').empty();
        gui.request({
            url: '/courriers/getCircuit/' + fluxId,
            updateElement: $('#circuit'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
//content parent
    if ($('#parent').length > 0 && $('#parent').is(':visible')) {
//        $('#preview .document_preview .content').empty();
        gui.request({
            url: '/courriers/getParent/' + parentFluxId,
            updateElement: $('#parent'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
//content refus flux
    if ($('#fluxRefus').length > 0 && $('#fluxRefus').is(':visible')) {
//        $('#preview .document_preview .content').empty();
        gui.request({
            url: '/courriers/getParent/' + refusFluxId,
            updateElement: $('#fluxRefus'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

//content sms
	if ($('#sms').length > 0 && $('#sms').is(':visible')) {
		gui.request({
			url: '/courriers/getSms/' + fluxId,
			updateElement: $('#sms'),
			loader: true,
			loaderMessage: gui.loaderMessage
		});
	}
}
function initContextDocumentBttnActions() {
//button preview document
    $('.ctxdocBttnPreview').click(function () {
        var documentId = $(this).parent().parent().parent().attr('itemId');
        gui.formMessage({
            updateElement: $('body'),
            loader: true,
            width: '550px',
            loaderMessage: gui.loaderMessage,
            title: 'Document',
            url: "/documents/getFileListPreview/" + documentId,
            buttons: {
                '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                }
            }
        });
    });

    $('.ctxdocBttnPreviewArmodel').click(function () {
        var armodelId = $(this).parent().parent().parent().attr('itemId');
        gui.formMessage({
            updateElement: $('body'),
            loader: true,
            width: '550px',
            loaderMessage: gui.loaderMessage,
            title: 'Modèle Accusé de Réception',
            url: "/armodels/getPreview/" + armodelId,
            buttons: {
                '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    gui.request({
                        url: '/armodels/deletefile/' + armodelId
                    });
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                }
            }
        });
    });
    $('.ctxdocBttnPreviewAr').click(function () {
        var arId = $(this).parent().parent().parent().attr('itemId');
        gui.formMessage({
            updateElement: $('body'),
            loader: true,
            width: '550px',
            loaderMessage: gui.loaderMessage,
            title: 'Accusé de Réception',
            url: "/ars/getPreview/" + arId,
            buttons: {
                '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    gui.request({
                        url: '/ars/deletefile/' + arId
                    });
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                }
            }
        });
    });
    $('.ctxdocBttnPreviewRmodel').click(function () {
        var rmodelId = $(this).parent().parent().parent().attr('itemId');
        gui.formMessage({
            updateElement: $('body'),
            loader: true,
            width: '550px',
            loaderMessage: gui.loaderMessage,
            title: 'Accusé de Réception',
            url: "/rmodels/getPreview/" + rmodelId,
            buttons: {
                '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    gui.request({
                        url: '/rmodels/deletefile/' + rmodelId
                    });
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                }
            }
        });
    });

    $('.ctxdocBttnPreviewRelements').click(function () {
        var fileName = $(this).attr('fileName');
        gui.formMessage({
            updateElement: $('.table_fchier_scane'),
            loader: true,
            width: '550px',
            loaderMessage: gui.loaderMessage,
            url: '/documents/getFileScannePreview/' + fileName + '/relements',
            buttons: {
                '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                }
            }
        });
    });
    //button supprimer
    $('.ctxdocBttnDelete').click(function () {
        var updateElement = $('#flux_context').find('.active.in');
        var docId = $(this).parent().parent().parent().attr('itemId');
//                console.log(docId);
        var filename = $(this).parentsUntil('.relement_docs_list').find('.ctxdoc_name span.filename').html();
        var controllerName = $(this).parent().parent().parent().attr('controllerName');
//                console.log(controllerName);
        swal({
                    showCloseButton: true,
            title: titleModalSupp,
            text: msgModalSupp,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
            confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: '/' + controllerName + '/delete/' + docId
                }, function (data) {
                    getJsonResponse(data);
                    affichageTabContext(fluxId);
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n'avez pas supprimé.",
                    type: "error",

                });
            }
        });

        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    });
    //button set main document
    $('.ctxdocBttnSetMainDoc').click(function () {
        var updateElement = $('.context_accordion').find('.ui-state-active');
        var docId = $(this).parent().parent().parent().attr('itemId');
		var url = window.location.href;
		var isCreation = url.indexOf('add');

		if( isCreation == '-1' ) {
			gui.request({
				url: '/Documents/setMainDoc/' + docId
			}, function (data) {
				getJsonResponse(data);
				updateElement.removeAttr('loaded');
				affichageTabContext(fluxId);

				updateContextRight(fluxId);
			});
		}
    });


    $('.ctxdocBttnWebdavAdd').click(function () {
        var filename = $(this).parentsUntil('.relement_docs_list').find('.ctxdoc_name span.filename').attr('title');
        var updateElement = $('.context_accordion').find('.ui-state-active');
        gui.request({
            url: "/relements/addFromFile/" + fluxId + "/" + filename,
            data: $(this).serialize(),
        }, function (data) {
            getJsonResponse(data);
            updateElement.removeAttr('loaded');
            affichageTabContext(fluxId);
        });
    });

	//button permettant de dire que le doc doit être signé
	$('.ctxdocBttnASigner').click(function () {
		var updateElement = $('.context_accordion').find('.ui-state-active');
		var docId = $(this).parent().parent().parent().attr('itemId');
		gui.request({
			url: '/Documents/setASigner/' + docId
		}, function (data) {
			getJsonResponse(data);
			updateElement.removeAttr('loaded');
			affichageTabContext(fluxId);
		});
	});
	//button permettant de dire que le doc ne doit finalement pas être signé
	$('.ctxdocBttnDesigner').click(function () {
		var updateElement = $('.context_accordion').find('.ui-state-active');
		var docId = $(this).parent().parent().parent().attr('itemId');
		gui.request({
			url: '/Documents/setASigner/' + docId + '/1'
		}, function (data) {
			getJsonResponse(data);
			updateElement.removeAttr('loaded');
			affichageTabContext(fluxId);
		});
	});


    // Action d'envoi par mail du document
    if (fluxId != null) {
        $('.ctxdocBttnSendmail').click(function () {
            var docId = $(this).parent().parent().parent().attr('itemId');
            var ar = false;
            if ($(this).parent().parent().parent().hasClass('arCtxdoc')) {
                ar = true;
            }
            else {
                ar = false;
            }
            gui.formMessage({
                title: titleModalSendmail,
                url: '/courriers/sendmail/' + docId + '/' + fluxId + '/' + ar,
                buttons: {
                    '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    },
                    '<i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer': function () {
                        var form = $('#CourrierSendmailForm');

						if( $('#SendmailEmail').val() == ''  && $('#SendmailEmail2').val() == '') {
							$('#SendmailEmail').prop('required', true);
							$('#SendmailEmail2').prop('required', true);
						}

						if (form_validate(form)) {
                            gui.request({
                                url: '/courriers/sendmail/' + docId + '/' + fluxId + '/' + ar,
                                data: form.serialize()
                            }, function (data) {
                                getJsonResponse(data);
                            });
                            $(this).parents('.modal').modal('hide');
                            $(this).parents('.modal').empty();
                        }
						else {
							swal({
								showCloseButton: true,
								title: "Oops...",
								text: "Veuillez vérifier votre formulaire!",
								type: "error"
							});
						}
                    }
                }
            });
        });
    }
// Action d'envoi du document unitairement à la GED
    $('.ctxdocBttnSendDocged').click(function () {
        var docId = $(this).parent().parent().parent().attr('itemId');
        gui.message({
            title: titleModalGED,
            msg: msgModalGED,
            buttons: [{
                    text: buttonSubmit,
                    class: 'btn btn-info-webgfc',
                    click: function () {
                        $(this).parents('.modal').modal('hide');
                        gui.loader({
                            message: gui.loaderMessage,
                            element: $('#courrier_skel')
                        });
                        var updateElement = $(this);
                        gui.request({
                            url: "/courriers/sendDocumentToGed/" + docId,
                            updateElement: updateElement
                        }, function () {
                            updateElement.removeAttr('loaded');
//                            affichageTabContext(fluxId);
                            window.location.reload();
                        });
                        $(this).parents('.modal').empty();
                    }
                }, {
                    text: buttonCancel,
                    class: 'btn btn-info-webgfc',
                    click: function () {
                        $(this).parents('.modal').modal('hide');
                    }
                }]
        });
    });


}



