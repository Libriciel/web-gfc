/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (typeof actionsViewUrl !== 'undefined') {
    function viewElement(itemId) {
        if (typeof actionsViewFormMessage !== 'undefined' && actionsViewFormMessage != "") {
            gui.formMessage({
                updateElement: actionsViewUpdateElement,
                loader: true,
                loaderMessage: gui.loaderMessage,
                title: actionsViewTitle,
                url: actionsViewUrl + '/' + itemId,
                buttons: {
                    '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    }
                }
            });
        } else {
            if (typeof actionsViewUpdateElement !== 'undefined' && actionsViewUpdateElement != "") {
                gui.request({
                    url: actionsViewUrl + '/' + itemId,
                    updateElement: actionsViewUpdateElement,
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            } else {
                gui.request({
                    url: actionsViewUr + '/' + itemId
                });
            }
        }
    }
}

if (typeof actionsEditUrl !== 'undefined') {

    function editElement(itemId) {
        if (typeof actionsEditMessage !== 'undefined' && actionsEditMessage != "") {
            gui.formMessage({
                updateElement: actionsEditUpdateElement,
                loader: true,
                loaderMessage: gui.loaderMessage,
                title: actionsEditTitle,
                url: actionsEditUrl + '/' + itemId,
                buttons: {
//                    '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
                    '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                        $(this).parents(".modal").modal("hide");
                        $(this).parents(".modal").empty();
						if (typeof actionsEditRefreshAction !== 'undefined' && actionsEditRefreshAction != 'loadStructure();') { // Pour ne pas recharger la liste des utilisateurs lors de la modification de l'un d'eux
                            eval(actionsEditRefreshAction);
                        }
                    }
                }
            });
        } else if (typeof actionsEditFormMessage !== 'undefined' && actionsEditFormMessage != "") {
            if (typeof actionsEditRefreshAction !== 'undefined') {
                actionsEditRefreshAction = "";
            }
            gui.formMessage({
                updateElement: actionsEditUpdateElement,
                loader: true,
                width: 700,
                loaderMessage: gui.loaderMessage,
                url: actionsEditUrl + '/' + itemId,
                buttons: {
                    actionsBtnValide: function () {
                        var errorFormTabs = [];
                        var form = $(this).parents(".modal").find("form");
                        if (form.valide()) {
                            var submits = {};
                            var index = 0;
                            $(".ui-tabs-panel").each(function () {
                                if ($("form", this).attr("action") !== undefined) {
                                    submits[index] = {"url": $("form", this).attr("action"), "data": $("form", this).serialize()};
                                    index++;
                                }
                            });
                            submits[index] = "end";
                            gui.submitAll({
                                submits: submits,
                                index: 0,
                                endFunction: eval(actionsEditRefreshAction),
                                loader: true,
                                loaderElement: actionsEditUpdateElement,
                                loaderMessage: gui.loaderMessage
                            });
                            $(this).parents(".modal").modal("hide");
                            $(this).parents(".modal").empty();
                        } else {
                            $(".ui-dialog .ui-tabs").tabs("select", errorFormTabs[0]);
                        }
                    },
                    '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
                        $(this).parents(".modal").modal("hide");
                        $(this).parents(".modal").empty();
                    }
                }
            });
        } else {
            if (actionsEditUrl != "") {
                if (typeof actionsEditUpdateElement !== 'undefined') {
                    if (typeof actionsEditDivshow !== 'undefined') {
                        gui.request({
                            url: actionsEditUrl + '/' + itemId,
                            updateElement: actionsEditUpdateElement,
                            loader: true,
                            loaderMessage: gui.loaderMessage,
                            showdiv: actionsEditDivshow
                        });
                        if (typeof actionsEditDivhide !== 'undefined') {
                            $('#liste').hide();
                        }
                    } else {
                        $('#infos .content').empty();
//                        $('#infos .modal-content').empty();

                        gui.request({
                            url: actionsEditUrl + '/' + itemId,
                            updateElement: actionsEditUpdateElement,
                            loader: true,
                            loaderMessage: gui.loaderMessage
                        });
                        $('#infos').modal('show');
                    }

                } else {
                    gui.request({
                        url: actionsEditUrl + '/' + itemId
                    });
                }
            }
        }
    }
}

if (typeof actionsDeleteUrl !== 'undefined') {
    function deleteElement(itemId) {
        if (typeof actionsDeleteUpdateElement !== 'undefined' && actionsDeleteUpdateElement != "") {
            swal({
                    showCloseButton: true,
                title: actionsDeleteTitle,
                text: actionsDeleteText,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#3085d6",
                cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
                confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
            }).then(function (data) {
                if (data) {
                    gui.request({
                        url: actionsDeleteUrl + '/' + itemId,
                        updateElement: actionsDeleteUpdateElement,
                        loader: true,
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        getJsonResponse(data);
                        eval(actionsDeleteRefreshAction);
                    });
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n\'avez pas supprimé.",
                        type: "error",

                    });
                }
            });
            $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
        } else {
            gui.request({
                url: actionsDeleteUrl + '/' + itemId
            }, function (data) {
                getJsonResponse(data);
            });
        }
    }
}

if (typeof actionsCheckUrl !== 'undefined') {
    function checkElement(itemId) {
        gui.request({
            url: actionsCheckUrl + '/' + itemId,
            loader: true,
            updateElement: actionsCheckUpdateElement,
            loaderMessage: gui.loaderMessage
        }, function (data) {
            getJsonResponse(data);
        });
    }
}

if (typeof actionsLockUrl !== 'undefined') {

	function lockElement(itemId) {
		if (typeof actionsLockMessage !== 'undefined' && actionsLockMessage != "") {
			gui.formMessage({
				updateElement: actionsLockUpdateElement,
				loader: true,
				loaderMessage: gui.loaderMessage,
				title: actionsLockTitle,
				url: actionsLockUrl + '/' + itemId,
				buttons: {
					'<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
						$(this).parents(".modal").modal("hide");
						$(this).parents(".modal").empty();
						if (typeof actionsLockRefreshAction !== 'undefined' && actionsLockRefreshAction != 'loadUser();') { // Pour ne pas recharger la liste des utilisateurs lors de la modification de l'un d'eux
							eval(actionsLockRefreshAction);
						}
					}
				}
			});
		} else if (typeof actionsLockFormMessage !== 'undefined' && actionsLockFormMessage != "") {
			if (typeof actionsLockRefreshAction !== 'undefined') {
				actionsLockRefreshAction = "";
			}
			gui.formMessage({
				updateElement: actionsLockUpdateElement,
				loader: true,
				width: 700,
				loaderMessage: gui.loaderMessage,
				url: actionsLockUrl + '/' + itemId,
				buttons: {
					actionsBtnValide: function () {
						var errorFormTabs = [];
						var form = $(this).parents(".modal").find("form");
						if (form.valide()) {
							var submits = {};
							var index = 0;
							$(".ui-tabs-panel").each(function () {
								if ($("form", this).attr("action") !== undefined) {
									submits[index] = {"url": $("form", this).attr("action"), "data": $("form", this).serialize()};
									index++;
								}
							});
							submits[index] = "end";
							gui.submitAll({
								submits: submits,
								index: 0,
								endFunction: eval(actionsLockRefreshAction),
								loader: true,
								loaderElement: actionsLockUpdateElement,
								loaderMessage: gui.loaderMessage
							});
							$(this).parents(".modal").modal("hide");
							$(this).parents(".modal").empty();
						} else {
							$(".ui-dialog .ui-tabs").tabs("select", errorFormTabs[0]);
						}
					},
					'<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
						$(this).parents(".modal").modal("hide");
						$(this).parents(".modal").empty();
					}
				}
			});
		} else {
			if (actionsLockUrl != "") {
				if (typeof actionsLockUpdateElement !== 'undefined') {
					if (typeof actionsLockDivshow !== 'undefined') {
						gui.request({
							url: actionsLockUrl + '/' + itemId,
							updateElement: actionsLockUpdateElement,
							loader: true,
							loaderMessage: gui.loaderMessage,
							showdiv: actionsLockDivshow
						});
						if (typeof actionsLockDivhide !== 'undefined') {
							$('#liste').hide();
						}
					} else {
						$('#infos .content').empty();

						gui.request({
							url: actionsLockUrl + '/' + itemId,
							updateElement: actionsLockUpdateElement,
							loader: true,
							loaderMessage: gui.loaderMessage
						});
						$('#infos').modal('show');
					}

				} else {
					gui.request({
						url: actionsLockUrl + '/' + itemId
					});
				}
			}
		}
	}
}
if (typeof actionsSyncUrl !== 'undefined') {
    function syncElement(itemId) {
        gui.formMessage({
            updateElement: actionsSyncUpdateElement,
            loader: true,
            loaderMessage: gui.loaderMessage,
            title: 'Synchronisation',
            url: actionsSyncUrl + '/' + itemId,
            buttons: {
                '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    $(this).parents(".modal").modal("hide");
                    $(this).parents(".modal").empty();
                }
            }
        });


//        gui.request({
//            url: actionsSyncUrl + '/' + itemId,
//            loader: true,
//            updateElement: actionsSyncUpdateElement,
//            loaderMessage: gui.loaderMessage
//        }, function (data) {
//            getJsonResponse(data);
//        });
    }
}

if (typeof actionsCopieCircuitUrl !== 'undefined') {
    function copieElement(itemId) {
        if (typeof actionsCopieCircuitUpdateElement !== 'undefined') {
            swal({
                    showCloseButton: true,
                title: 'Validation de la copie',
                text: 'Etes-vous sur de vouloir copier ce circuit ?',
                type: "info",
                showCancelButton: true,
                // confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: actionsBtnValide,
                cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
            }).then(function (data) {
                if (data) {
                    $(this).parents(".modal").modal("hide");
                    console.log(actionsCopieCircuitUrl);
                    gui.request({
                        url: actionsCopieCircuitUrl + '/' + itemId,
                        updateElement: actionsCopieCircuitUpdateElement,
                        loader: true,
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        getJsonResponse(data);
                        eval(actionsCopieCircuitRefreshAction);
                    });
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n\'avez pas copié,;) .",
                        type: "error",

                    });
                }
            });





            /*
             gui.message({
             title: 'Validation de la copie',
             msg: 'Etes-vous sur de vouloir copier ce circuit ?',
             buttons: [{
             text: actionsBtnValide,
             class: "btn btn-info-webgfc",
             click: function () {
             $(this).parents(".modal").modal("hide");
             gui.request({
             url: actionsCopieCircuitUrl + '/' + itemId,
             updateElement: actionsCopieCircuitUpdateElement,
             loader: true,
             loaderMessage: gui.loaderMessage
             }, function (data) {
             getJsonResponse(data);
             eval(actionsCopieCircuitRefreshAction);
             });
             }
             }, {
             text: actionsBtnCancel,
             class: "btn btn-info-webgfc",
             click: function () {
             $(this).parents(".modal").modal("hide");
             }
             }
             ]
             });
             */
        } else {
            gui.request({
                url: actionsCopieCircuitUrl + '/' + itemId
            }, function (data) {
                getJsonResponse(data);
            });
        }
    }
}

if (typeof actionsExposrtUrl !== 'undefined') {
    function exportElement(itemId) {
        /*
         gui.request({
         url: actionsExposrtUrl + '/' + itemId,
         }, function (data) {
         console.log(data);
         getJsonResponse(data);
         });
         */
        window.location.href = actionsExposrtUrl + '/' + itemId;
    }
}

function apercuElement(reference) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: '/Courriers/getFluxPreview/' + reference,
        success: function (data) {
            var documentId = data.documentId;
            if (documentId != null) {
                /*
                 gui.formMessage({
                 title: 'Prévisualisation du document',
                 url: '/Documents/getPreview/' + documentId + '/modal',
                 buttons: {
                 "Fermer": function () {
                 $(this).parents(".modal").modal('hide');
                 $(this).parents(".modal").empty();
                 }
                 }
                 });
                 */

                gui.request({
                    url: '/Documents/getPreview/' + documentId + '/modal',
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                    updateElement: $('#traitementlot_page')
                }, function (data) {
                    $('#traitementlot_page .list-flux').addClass('col-sm-4');
                    $('#traitementlot_page .prevoir-document').addClass('col-sm-8').show();
                    $('#traitementlot_page .prevoir-document .content').html(data);

                    var length = $('#traitementlot_page .list-flux table#table_flux tr').length;
                    for( i=1; i< length; i++ ) {
                        if( $('#traitementlot_page .list-flux table#table_flux tr')[i].firstChild.innerHTML == reference ) {
                            $('#traitementlot_page .list-flux table#table_flux tr')[i].classList.add('surlignage');
                        }
                        else {
                            $('#traitementlot_page .list-flux table#table_flux tr')[i].classList.remove('surlignage');
                        }
                    }

                });
            } else {
                swal({
                    showCloseButton: true,
                    title: 'Oops...',
                    text: "Il n'existe pas de document associé à ce flux",
                    type: "error",

                });
            }
        }
    });
}

function downloadElement(itemId) {
    window.location.href = actionsDownloadUrl + '/' + itemId;
}
