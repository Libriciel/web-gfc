/**
 * web-GFC javascript debug tools
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */

function initDebugTools(){
	var spanRemoveLoader = $('<span></span>').html('Remove Loaders').css({
		'z-index': '6666666666',
		'position': 'fixed',
		'padding': '3px',
		'bottom': '-25px',
		'left': '5px',
		'cursor': 'pointer'
	}).addClass('ui-state-error').button().hover(function(){
		$(this).css('bottom', '0px');
	}, function(){
		$(this).css('bottom', '-25px');
	}).click(function(){
		$('.loader').remove();
	});
	$('body').append(spanRemoveLoader);


	var spanFireJGrowl = $('<span></span>').html('Fire jGrowl').css({
		'z-index': '6666666666',
		'position': 'fixed',
		'padding': '3px',
		'bottom': '-25px',
		'left': '320px',
		'cursor': 'pointer'
	}).addClass('ui-state-error').button().hover(function(){
		$(this).css('bottom', '0px');
	}, function(){
		$(this).css('bottom', '-25px');
	}).click(function(){

		var text = 'jQuery version : ' + $().jquery;
		text += '<br />jQuery UI version : ' + $.ui.version;

		layer.msg(text);
	});
	$('body').append(spanFireJGrowl);



	var spanFireJGrowl = $('<span></span>').html('Tab Reload Bttn').css({
		'z-index': '6666666666',
		'position': 'fixed',
		'padding': '3px',
		'bottom': '-25px',
		'left': '430px',
		'cursor': 'pointer'
	}).addClass('ui-state-error').button().hover(function(){
		$(this).css('bottom', '0px');
	}, function(){
		$(this).css('bottom', '-25px');
	}).click(function(){
		if($('.tabReloadBttn').length > 0){
			initTabsReloadBttns(true);
		} else {
			initTabsReloadBttns();
		}
	});
	$('body').append(spanFireJGrowl);


	var spanLogout = $('<span></span>').html('Logout').css({
		'z-index': '6666666666',
		'position': 'fixed',
		'padding': '3px',
		'bottom': '-25px',
		'right': '5px',
		'cursor': 'pointer'
	}).addClass('ui-state-error').button().hover(function(){
		$(this).css('bottom', '0px');
	}, function(){
		$(this).css('bottom', '-25px');
	}).click(function(){
		window.location.href = "/users/logout";
	});
	$('body').append(spanLogout);




	var spanShowUploadIFrames = $('<span></span>').html('Show Upload IFrames').css({
		'z-index': '6666666666',
		'position': 'fixed',
		'padding': '3px',
		'bottom': '-25px',
		'left': '150px',
		'cursor': 'pointer'
	}).addClass('ui-state-error').button().hover(function(){
		$(this).css('bottom', '0px');
	}, function(){
		$(this).css('bottom', '-25px');
	}).click(function(){
		var cpt = 0;
		$('iframe').each(function(){
			if ($(this).css('width') == '0px'){
				$(this).css({
					'position': 'fixed',
					'top':  ((cpt * 350) + 30).toString() + 'px',
					'height': '350px',
					'right': '5px',
					'width': '1024px',
					'border': '10px groove red',
					'background-color': 'white',
					'z-index': '6666666666'
				}).draggable().resizable();
			} else {
				$(this).css({
					'top':  '0',
					'right':  '0',
					'height': '0px',
					'width': '0px',
					'border-width': '0px'
				});
			}
			cpt++;
		});
	});
	$('body').append(spanShowUploadIFrames);




}

function initTabsReloadBttns(remove){
	if (typeof(remove) && remove){
		$('.tabReloadBttn').remove();
	} else {
		$('.ui-tabs').each(function(){
			var cpt = 0;
			$('.ui-tabs-nav li', $(this)).each(function(){
				var reloadBttn = $('<span></span>').attr('tabnum', cpt).html('reload').css({
					'float': 'right'
				}).addClass('tabReloadBttn').addClass('ui-state-error').button().click(function(){
					console.log($(this).attr('tabnum'));
					$(this).parents('.ui-tabs').tabs('load', $(this).attr('tabnum'));
				});
				$(this).append(reloadBttn);
				cpt++;
			});
		});
	}
}
