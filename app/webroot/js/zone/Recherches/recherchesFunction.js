/*
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * @author JIN Yuzhu
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 */
/*
 function setJSelect(select) {
 var id = select.attr('id');

 $('option', select).each(function () {
 $(this).addClass('item');
 $(this).attr('sId', $(this).val());
 });
 //        editItem( $('select#DesktopDesktop.JSelectMultiple'));

 var newList = $('<ul>');
 newList.attr('id', id + 'List');
 newList.addClass('jSelectList');

 var addAllBttn = $('<span title="Tout ajouter">');
 addAllBttn.html('<i class="fa fa-plus"></i>');
 addAllBttn.addClass('addAllBttn btn btn-info-webgfc');
 addAllBttn.button();
 $('span', $(addAllBttn)).css('padding', '3px');
 addAllBttn.click(function () {
 var typeChoix = new Array();
 $('option', select).removeAttr('selected').each(function () {
 if ($(this).css('display') == 'block') {
 $(this).attr('selected', 'selected');
 addItem(select);
 }
 if ($(this).parents('.panel-body').find('#TypeTypeList').length != 0) {
 typeChoix.push($(this).val());
 }
 });
 chargeSoustypes(typeChoix);
 });

 select.after(newList.before('<br />Liste des critères selectionnés : ')).after(addAllBttn.before(' '));

 var duplicatedSelect = select.clone(true);
 duplicatedSelect.attr('multiple', 'multiple');
 duplicatedSelect.attr('id', duplicatedSelect.attr('id') + "duplicated");
 $('option', duplicatedSelect).each(function () {
 if ($(this).val() == '' || $(this).val() == 'undefined') {
 $(this).remove();
 }
 });
 duplicatedSelect.css('display', 'none');
 select.before(duplicatedSelect);
 select.attr('name', '');
 select.removeAttr('multiple');
 select.addClass('form-control');
 //    select.css('width', '250px');
 //    select.css({
 //        width: '250px',
 //        position: 'relative',
 //        top: '-3px'
 //    });

 select.change(function () {
 addItem($(this));
 });


 }
 function chargeSoustypes(types) {
 $.ajax({
 type: 'post',
 url: '/recherches/charge_soustypes',
 data: {TypeChoix: types},
 success: function (data) {
 $('#SoustypeSoustype').html(data);
 $('#SoustypeSoustypeduplicated').html(data);
 }
 });
 }

 function delItem(e) {
 var select = $('#' + e.attr('parentSelect'));
 var list = e.parent().parent().parent();
 var listItem = e.parent().parent();
 var sId = e.attr('sId');
 var item;
 $('option', select).each(function () {
 if ($(this).attr('sId') == sId) {
 item = $(this);
 }
 });
 var realItem;
 $('#' + select.attr('id') + 'duplicated option').each(function () {
 if ($(this).attr('sId') == sId) {
 realItem = $(this);
 }
 });
 listItem.remove();
 if (item != null) {
 item.css('display', 'block');
 }
 if (realItem != null) {
 realItem.removeAttr('selected');
 }
 if ($('li', list).length == 0 && $('.delAllBttn[listId=' + list.attr('id') + ']').length != 0) {
 $('.delAllBttn[listId=' + list.attr('id') + ']').remove();
 }
 if (e.attr('parentSelect') == 'TypeType') {
 var typeChoix = new Array();
 list.find('li').each(function () {
 typeChoix.push($(this).attr('sid'));
 });

 //        if (typeChoix.length != 0) {
 chargeSoustypes(typeChoix);
 //        }
 }
 }

 function addItem(select) {
 var id = select.attr('id');
 var item = $('option:selected', select);
 var list = $('#' + id + 'List');
 var realItem;
 $('#' + id + 'duplicated option').each(function () {
 if ($(this).attr('sId') == item.attr('sId')) {
 realItem = $(this);
 }
 });

 if (item !== 'undefined' && item.html() !== 'undefined' && item.html() != "") {
 var li = $('<li>');
 li.attr('sId', item.val());
 li.addClass('duplicatedId' + item.val());

 var div = $('<div>');
 div.css({
 'padding': '5px'
 }).addClass('ui-corner-all ui-widget-content');
 //			div.hover(function() {
 //				$(this).addClass('ui-state-hover');
 //			}, function() {
 //				$(this).removeClass('ui-state-hover');
 //			});
 div.html(item.html());


 var delBttn = $('<span>');
 delBttn.html('<img src="/img/icons/cancel.png" alt="enlever" title="enlever" style="vertical-align:middle;"/>');
 delBttn.addClass('delBttn');
 delBttn.css({
 'float': 'right',
 'cursor': 'pointer'
 });
 delBttn.attr('parentSelect', id);
 delBttn.attr('sId', item.val());

 $('span', $(delBttn)).css('padding', '1px');
 delBttn.click(function () {
 delItem($(this));
 });

 list.append(li.append(div.append(delBttn)));
 item.css('display', 'none');
 $('option:selected', select).removeAttr('selected');
 realItem.attr('selected', 'selected');
 }

 if ($('li', list).length > 0 && $('.delAllBttn[listId=' + list.attr('id') + ']').length == 0) {
 var delAllBttn = $('<span title="Tout enlever">');
 delAllBttn.html('<i class="fa fa-minus"></i>');
 delAllBttn.addClass('delAllBttn btn btn-info-webgfc');
 delAllBttn.attr('listId', list.attr('id'));
 delAllBttn.button();
 $('span', $(delAllBttn)).css('padding', '3px');
 delAllBttn.click(function () {
 $('span.delBttn', list).each(function () {
 delItem($(this));
 });
 $(this).remove();
 });

 list.after(delAllBttn);
 }

 // Spécifique aux sous-valeurs des métadonnéees
 //         if( select[0].id == 'MetadonneeMetadonnee' ) {
 //             displayValueMeta(item.attr('sId'));
 //         }
 }
 function editItem(select) {
 var id = select.attr('id');
 var item = $('option:selected', select);
 var list = $('#' + id + 'List');
 var realItem = [];

 select.each(function () {
 if ($(this).is(':selected')) {
 realItem.push($(this));
 }
 });

 if (item !== 'undefined' && item.html() !== 'undefined' && item.html() != "") {

 var li = $('<li>');
 li.attr('sId', item.val());
 li.addClass('duplicatedId' + item.val());

 var div = $('<div>');
 div.css({
 'width': '350px',
 'padding': '5px'
 }).addClass('ui-corner-all ui-widget-content');
 div.hover(function () {
 $(this).addClass('ui-state-hover');
 }, function () {
 $(this).removeClass('ui-state-hover');
 });
 div.html(item.html());


 var delBttn = $('<span>');
 delBttn.html('<img src="/img/icons/cancel.png" alt="enlever" title="enlever" style="vertical-align:middle;"/>');
 delBttn.addClass('delBttn');
 delBttn.css({
 'float': 'right',
 'cursor': 'pointer'
 });
 delBttn.attr('parentSelect', id);
 delBttn.attr('sId', item.val());

 $('span', $(delBttn)).css('padding', '1px');
 delBttn.click(function () {
 delItem($(this));
 });

 list.append(li.append(div.append(delBttn)));
 item.css('display', 'none');
 $('option:selected', select).removeAttr('selected');

 for (var i = 0; i < realItem.length; i++) {
 realItem[i].attr('selected', 'selected');
 }

 }


 if ($('li', list).length > 0 && $('.delAllBttn[listId=' + list.attr('id') + ']').length == 0) {
 var delAllBttn = $('<span>');
 delAllBttn.html('Tout enlever');
 delAllBttn.addClass('delAllBttn');
 delAllBttn.attr('listId', list.attr('id'));
 delAllBttn.button();
 $('span', $(delAllBttn)).css('padding', '3px');
 delAllBttn.click(function () {
 $('span.delBttn', list).each(function () {
 delItem($(this));
 });
 $(this).remove();
 });
 list.after(delAllBttn);
 }
 }

 function resetJSelect(formId) {
 $(formId).find(':input').each(function () {
 switch (this.type) {
 case 'password':
 case 'select-multiple':
 case 'select-one':
 $('span.select2-chosen').empty();
 $('div.select2-container').addClass('select2-default');
 $('div.select2-container').removeClass('select2-allowclear');
 $('li.select2-search-choice').remove();
 case 'text':
 case 'textarea':
 $(this).val('');
 break;
 case 'checkbox':
 case 'radio':
 this.checked = false;
 }
 });

 $('.itemSelectList', $(formId)).empty();
 $('select option', $(formId)).removeAttr('selected');
 $('select option', $(formId)).css('display', 'block');
 $('.deleteAllBtn').remove();
 $('.table-list h3 span').remove();
 }
 */
function switchSave() {
    if ($('#RechercheSave').prop("disabled", false)) {
        $('#RechercheName').attr('disabled', 'disabled');
    } else {
        $('#RechercheName').removeAttr('disabled');
    }
}

$('#RechercheRecherchesEnregistrees').select2({allowClear:true, placeholder: "Veuillez sélectionner une recherche parmi celles déjà enregistrées"});
$('#RechercheRecherchesEnregistrees').change( function() {
	$(".formSearch :input").attr("disabled", true);
	$('.saveSearch :input').attr('disabled', true);
	$('.searcheSaveTrue :input').attr('disabled', false);

	$('#editSearch > a').removeClass('ui-state-disabled');
	$('#deleteSearch > a').removeClass('ui-state-disabled');

	if($('#RechercheRecherchesEnregistrees').val() != '') {

		gui.request({
			url: "/recherches/ajaxSavedSearch/" + $('#RechercheRecherchesEnregistrees').val(),
			data: $('#RechercheFormulaireForm').serialize()
		}, function (data) {
			var json = getJsonResponse(data);
			layer.msg("Les informations de la recherche <br \>ont bien été reprises");
			if (json != undefined) {

				$('#infoFlux').attr('checked', false);
				$('#serviceFlux').attr('checked', false);
				$('#ContactsFlux').attr('checked', false);
				$('#qualification').attr('checked', false);
				$('#metadonnees').attr('checked', false);
				$('#circuit').attr('checked', false);
				$('#dossierAffaire').attr('checked', false);
				$('#tache').attr('checked', false);
				$('#commentaire').attr('checked', false);
				$('#document').attr('checked', false);

// console.log(json);
// console.log(!isEmpty(json.Recherche.cocrdata ));
				if(
					!isEmpty(json.Recherche.cname)
					|| !isEmpty(json.Recherche.cdate)
					|| !isEmpty(json.Recherche.cdatefin)
					|| !isEmpty(json.Recherche.cdatereception)
					|| !isEmpty(json.Recherche.cdatereceptionfin)
					|| !isEmpty(json.Recherche.cobjet)
					|| !isEmpty(json.Recherche.contactname)
					|| !isEmpty(json.Recherche.organismename)
					|| !isEmpty(json.Recherche.creference)
					|| !isEmpty(json.Recherche.cretard)
					|| json.Desktop.length > 0
				) {
					fieldsForInfoFluxToDisplay(json.Recherche);

					if(json.Desktop.length != 0) {
						otherFieldsToDisplay(json.Desktop);
					}

					$('#infoFlux').attr('checked', true);
					$('#infoFlux').change();
				}


				if( json.Contact.length > 0 || json.Organisme.length > 0
					|| !isEmpty(json.Recherche.organismedepartement)
					|| !isEmpty(json.Recherche.contactdept)
				) {
					$('#ContactsFlux').attr('checked', true);
					$('#ContactsFlux').change();

					otherFieldsToDisplay(json.Contact );
					otherFieldsToDisplay(json.Organisme );
					fieldsForInfoFluxToDisplay(json.Recherche);
				}

				if(!isEmpty(json.Recherche.cocrdata ) ) {
					fieldsForInfoFluxToDisplay(json.Recherche);
					$('#document').attr('checked', true);
					$('#document').change();
				}

				if(!isEmpty(json.Recherche.ccomment ) ) {
					fieldsForInfoFluxToDisplay(json.Recherche);
					$('#commentaire').attr('checked', true);
					$('#commentaire').change();
				}

				if( json.Service.length >= 1 ) {
					otherFieldsToDisplay(json.Service );
					$('#serviceFlux').attr('checked', true);
					$('#serviceFlux').change();
				}
				if( json.Metadonnee.length > 0 ) {
					multipleFieldsToDisplay(json.Metadonnee, 'Metadonnee' );
					$('#metadonnees').attr('checked', true);
					$('#metadonnees').change();
				}

				if( json.RechercheSelectvaluemetadonnee.length >0 ) {
					fieldsForMetadonneeValues(json.RechercheSelectvaluemetadonnee );
				}
				if( json.Type.length > 0 ) {
					multipleFieldsToDisplay(json.Type, 'Type' );
					$('#qualification').attr('checked', true);
					$('#qualification').change();
				}
				if( json.Soustype.length > 0 ) {
					multipleFieldsToDisplay(json.Soustype, 'Soustype' );
				}

				if( json.Affaire.length > 0 || json.Dossier.length > 0  ) {
					multipleFieldsToDisplay(json.Affaire, 'Affaire' );
					multipleFieldsToDisplay(json.Dossier, 'Dossier' );
					$('#dossierAffaire').attr('checked', true);
					$('#dossierAffaire').change();
				}

				if( json.Circuit.length > 0 ) {
					multipleFieldsToDisplay(json.Circuit, 'Circuit' );
					$('#circuit').attr('checked', true);
					$('#circuit').change();
				}
				if( json.Tache.length > 0 ) {
					multipleFieldsToDisplay(json.Tache, 'Tache' );
					$('#tache').attr('checked', true);
					$('#tache').change();
				}

				$('#RechercheTosave').prop("checked", true);
				$('#RechercheTosave').change();
				$('#RechercheName').val(json.Recherche.name);

			}
		});
	}
	else {
		if( $('#infoFlux').prop('checked')) {
			$('#infoFlux').attr('checked', false);
			$('#infoFlux').change();
		}

		if( $('#serviceFlux').prop('checked')) {
			$('#serviceFlux').attr('checked', false);
			$('#serviceFlux').change();
		}

		if( $('#ContactsFlux').prop('checked')) {
			$('#ContactsFlux').attr('checked', false);
			$('#ContactsFlux').change();
		}

		if( $('#qualification').prop('checked')) {
			$('#qualification').attr('checked', false);
			$('#qualification').change();
		}

		if( $('#metadonnees').prop('checked')) {
			$('#metadonnees').attr('checked', false);
			$('#metadonnees').change();
		}

		if( $('#circuit').prop('checked')) {
			$('#circuit').attr('checked', false);
			$('#circuit').change();
		}

		if( $('#dossierAffaire').prop('checked')) {
			$('#dossierAffaire').attr('checked', false);
			$('#dossierAffaire').change();
		}

		if( $('#tache').prop('checked')) {
			$('#tache').attr('checked', false);
			$('#tache').change();
		}

		if( $('#commentaire').prop('checked')) {
			$('#commentaire').attr('checked', false);
			$('#commentaire').change();
		}

		if( $('#document').prop('checked')) {
			$('#document').attr('checked', false);
			$('#document').change();
		}

		$(".formSearch :input").attr("disabled", false);
		$('.saveSearch :input').attr('disabled', false);

		// On décoche et on referme la sauvegarde de la recherhce
		$('#RechercheTosave').prop("checked", false);
		$('#RechercheTosave').change();

		gui.disablebutton({
			element: $('#editSearch'),
			button: '<i class="fa fa-pencil" aria-hidden="true"></i> '
		});
		gui.disablebutton({
			element: $('#deleteSearch'),
			button: '<i class="fa fa-trash" aria-hidden="true"></i> '
		});
	}

});
$('#editSearch').css('margin', '-47px -70px auto auto');
$('#deleteSearch').css('float', 'right');
$('#deleteSearch').css('margin', '-28px 440px auto auto');


gui.addbuttons({
	element: $('#editSearch'),
	buttons: [
		{
			content: '<i class="fa fa-pencil" aria-hidden="true"></i> ',
			action: function () {
				$(".formSearch :input").attr("disabled", false);
				$('.saveSearch :input').attr('disabled', false);
				$('#detail_recherche :input').attr('disabled', false);
				$('#DesktopDesktop').attr('disabled', false);
			}
		}
	]
});

gui.disablebutton({
	element: $('#editSearch'),
	button: '<i class="fa fa-pencil" aria-hidden="true"></i> '
});



gui.addbuttons({
	element: $('#deleteSearch'),
	buttons: [
		{
			content: '<i class="fa fa-trash" aria-hidden="true"></i> ',
			class: "btn-danger",
			action: function () {
				var rechercheId = $('#RechercheRecherchesEnregistrees').val();
				var rechercheName = $('#RechercheRecherchesEnregistrees').find(':selected')[0].label ;
				if (rechercheId == 'undefined' || rechercheId == '' ) {
					swal({
						showCloseButton: true,
						title: 'Supprimer la recherche',
						text: 'Veuillez choisir au moins une recherche',

					});
				} else {
					swal({
						showCloseButton: true,
						title: 'Supprimer une recherche',
						text: 'Voulez-vous supprimer la recherche "' + rechercheName + '" ?',
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#d33",
						cancelButtonColor: "#3085d6",
						confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
						cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
					}).then(function (data) {
						if (data) {
							gui.request({
								url: '/recherches/deleteSearch/' + rechercheId,
								data: $('#RechercheFormulaireForm').serialize()
							}, function (data) {
								gui.request({
									url: '/recherches/deleteSearch/' + rechercheId,
									loader: true,
									updateElement: $('#infos .content'),
									loaderMessage: gui.loaderMessage
								});
								getJsonResponse(data);
								window.location.reload();
							});
						} else {
							swal({
								showCloseButton: true,
								title: "Annulé!",
								text: "Vous n\'avez pas supprimé.",
								type: "error",

							});
						}
					});
					$('.swal2-cancel').css('margin-left', '-320px');
					$('.swal2-cancel').css('background-color', 'transparent');
					$('.swal2-cancel').css('color', '#5397a7');
					$('.swal2-cancel').css('border-color', '#3C7582');
					$('.swal2-cancel').css('border', 'solid 1px');

					$('.swal2-cancel').hover(function () {
						$('.swal2-cancel').css('background-color', '#5397a7');
						$('.swal2-cancel').css('color', 'white');
						$('.swal2-cancel').css('border-color', '#5397a7');
					}, function () {
						$('.swal2-cancel').css('background-color', 'transparent');
						$('.swal2-cancel').css('color', '#5397a7');
						$('.swal2-cancel').css('border-color', '#3C7582');
					});
				}
			}
		}
	]
});

gui.disablebutton({
	element: $('#deleteSearch'),
	button: '<i class="fa fa-trash" aria-hidden="true"></i> '
});

$('#RechercheName').hide();
$('#RechercheName').parent().parent().hide();

$('#RechercheTosave').change(function () {
	if ($(this).prop('checked')) {
		$('#RechercheName').show();
		$('#RechercheName').parent().parent().show();
	}
	else {
		$('#RechercheName').hide();
		$('#RechercheName').parent().parent().hide();

	}
});

function multipleFieldsToDisplay( datas, checkName ) {
	let tabData = [];
	for( i in datas) {
		tabData.push(datas[i].id);
	}
	$('#' + checkName + checkName).val(tabData).change();
}
function fieldsForMetadonneeValues(datas) {
	for( i in datas ) {
		$('#SelectvaluemetadonneeSelectvaluemetadonnee' + datas[i].metadonnee_id ).val(datas[i].valeur).change();
		if( isEmpty(datas[i].valeur) ) {
			$('#SelectvaluemetadonneeSelectvaluemetadonnee' +  + datas[i].metadonnee_id ).val(datas[i].selectvaluemetadonnee_id).change();
		}
	}
}
function fieldsForInfoFluxToDisplay( datas ){
	for( i in datas ) {
		var dataToDisplay = i.substring(0, 1).toUpperCase() + i.substring(1);
		var fieldToCharge = '#Recherche'+dataToDisplay;
		$(fieldToCharge).val( datas[i] ).change();

		if( fieldToCharge === '#RechercheOrganismenomvoie' ) {
			$("#RechercheOrganismenomvoie").append($("<option value='" + datas[i] + "' selected='selected'>" + datas[i] + "</option>"));
			$("#RechercheOrganismenomvoie").val(datas[i]).change();
		}

		if( fieldToCharge === '#RechercheContactnomvoie' ) {
			$("#RechercheContactnomvoie").append($("<option value='" + datas[i] + "' selected='selected'>" + datas[i] + "</option>"));
			$("#RechercheContactnomvoie").val(datas[i]).change();
		}
	}
}

function otherFieldsToDisplay( datas ) {
	for( i in datas ) {
		if ( typeof datas[i].RechercheDesktop !== "undefined" ){
			$('#DesktopDesktop').val(datas[i].id).change();
		}
		if ( typeof datas[i].RechercheService !== "undefined" ){
			$('#ServiceService').val(datas[i].id).change();
		}
		if ( typeof datas[i].RechercheOrganisme !== "undefined" ){
			$('#OrganismeOrganisme').val(datas[i].id).change();
		}
		if ( typeof datas[i].RechercheContact !== "undefined" ){
			$('#ContactContact').val(datas[i].id).change();
		}
	}
}

function isEmpty(value) {
	return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
}
