/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function addslashes(ch) {
    ch = ch.replace(/\\/g, "\\\\")
    ch = ch.replace(/\'/g, "\\'")
    ch = ch.replace(/\"/g, "\\\"")
    return ch
}

function refreshOrganismes(orgInfos) {
    $("#CourrierOrganismeId").append($("<option value='" + orgInfos['id'] + "'>" + orgInfos['name'] + "</option>"));
    $('#CourrierOrganismeId').val(orgInfos['id']).change();
    if (typeof orgInfos['contacts']['id'] !== 'undefined') {
        refreshContact(orgInfos); //FIXME
        refreshContactInfos(orgInfos['contacts']['id']);
    }
}

function refreshContact(contactinfo) {
    $("#CourrierContactId").append($("<option value='" + contactinfo['contacts']['id'] + "'>" + contactinfo['contacts']['name'] + "</option>"));
    $('#CourrierContactId').val(contactinfo['contacts']['id']).change();
    refreshContactInfos(contactinfo['contacts']['id']);
}

function refreshContactInfos(contactId) {
    if (contactId != '') {
        $("[id^=contactInfoCard]").remove();
        gui.request({
            url: '/contacts/searchContactinfo/' + contactId
        }, function (data) {
            $('#CourrierContactId').val(contactId);
            var json = jQuery.parseJSON(data);
            var contactinfo = json['Contactinfo'];
            if (typeof contactinfo['id'] !== 'undefined') {
                $('#CourrierContactId').change(function () {
                    showContactinfo()
                }).parent().append(createContactinfo(contactinfo));
            }
            $('#contactInfoCard' + $('#CourrierContactId option:selected').val()).show();
        });
    } else {
        $("[id^=contactInfoCard]").hide();
    }
}

function refreshOrganismeInfos(organismeId) {
    gui.request({
        url: '/contacts/searchOrganismeinfo/' + organismeId
    }, function (data) {
//            $('.organismeInfoCard').remove();
        $('#CourrierOrganismeId').val(organismeId);
        var json = jQuery.parseJSON(data);
        var organismeinfo = json['Organisme'];
        if (typeof organismeinfo['id'] !== 'undefined') {
            $('#CourrierOrganismeId').change(function () {
                showOrganismeinfo();
            }).parent().append(createOrganismeinfo(organismeinfo));
        }
        $('#organismeInfoCard' + $('#CourrierOrganismeId option:selected').val()).show();
    });
}

function genPriorityImg() {
    var img = $('<img />').attr('id', 'priorityImg').css({
        width: '16px',
        height: '16px',
        'vertical-align': "middle",
        margin: "-31px -25px auto auto",
        float: "right"
    });
    var priorityLevel = $('#CourrierPriorite').val();
    if (priorityLevel == 0) {
        img.attr('src', '/img/priority_low.png');
    } else if (priorityLevel == 1) {
        img.attr('src', '/img/priority_mid.png');
    } else if (priorityLevel == 2) {
        img.attr('src', '/img/priority_high.png');
    }
    $('#CourrierPriorite').after(img);
}

function chargementOptionsContact(contactLists) {
    var courrierOrganismedIdVal = $("#CourrierOrganismeId").val();

    if (contactLists[courrierOrganismedIdVal] != undefined) {
        var tempContactOptions = [];
        var i = 0;
        $.each(contactLists[courrierOrganismedIdVal], function (index, value) {
            tempContactOptions[i] = {
                'key': index,
                'val': value
            };
            i++;
        });
        var ContactOptions = tempContactOptions.sort(function (a, b) {
            return a.val.localeCompare(b.val);
        });
        $('#CourrierContactId option').remove();
        $.each(ContactOptions, function (index, value) {
            $('#CourrierContactId').append($("<option value='" + value.key + "'>" + value.val + "</option>"));
        });
    }
}

function setFieldSetContact(stypeId) {
    if (stypeId == -1) {
        $('#fieldsetContact').css('display', 'none');
    } else if (stypeId == 0) {
        $('#fieldsetContact').hide('fast');
    } else {
        $('#fieldsetContact').show('slow');
        var sens;
        for (i in types) {
            for (j in types[i].soustypes) {
                if (types[i].soustypes[j].id == stypeId) {
                    sens = types[i].soustypes[j].entrant;
                }
            }
        }
        if (sens == 1 || sens == 2) {
            $('#fieldsetContact legend').html(fieldsetContactLegendExp);
            $('#fieldsetContact label[for=CourrierContactName]').html(fieldsetContactLabelCourrierContactNameExp);
            $('#fieldsetContact label[for=CourrierContact]').html(fieldsetContactLabelCourrierContactExp);
        } else if (sens == 0) {
            $('#fieldsetContact legend').html(fieldsetContactLegendDest);
            $('#fieldsetContact label[for=CourrierContactName]').html(fieldsetContactLabelCourrierContactNameDest);
            $('#fieldsetContact label[for=CourrierContact]').html(fieldsetContactLabelCourrierContactDest);
        }
    }
}


$("#CourrierOrganismeId").select2({allowClear: true, placeholder: "Sélectionner un organisme"});
$("#CourrierContactId").select2({allowClear: true, placeholder: "Sélectionner un contact"});
$('#PliconsultatifHeureHour').select2({allowClear: true});
$('#PliconsultatifHeureMin').select2({allowClear: true});
$("#CourrierAffairesuivieparId").select2({allowClear: true, placeholder: "Sélectionner une personne"});
$("#CourrierOriginefluxId").select2({allowClear: true, placeholder: "Sélectionner une origine"});
$("#CourrierPriorite").select2({allowClear: true, placeholder: "Sélectionner une priorité"});
// $("#CourrierDelaiUnite").select2({allowClear: true, placeholder: "Sélectionner une unité"});
// $('#s2id_CourrierDelaiUnite').css({
//     height: '0px',
//     width: '46%',
//     float: 'right',
//     position: 'relative',
//     top: '-52px'
// });
// $('#CourrierDelaiUnite').css({
//     top: '-44px'
// });
$("#TypesType").select2();
$("#CourrierSoustypeId").select2();

$("#CourrierContactId").change(function () {
    showContactinfo();
    if ($('#CourrierContactId').val() != '' && $('#CourrierContactId').val() != null) {
        refreshContactInfos($('#CourrierContactId').val());
    }
});

$("#CourrierOrganismeId").change(function () {
    showOrganismeinfo();
    if ($('#CourrierOrganismeId').val() != '' && $('#CourrierOrganismeId').val() != null) {
        refreshOrganismeInfos($('#CourrierOrganismeId').val());
    }
});

$('#CourrierPriorite').change(function () {
    $('#priorityImg').remove();
    genPriorityImg();
});


showContactinfo();
showOrganismeinfo();


//$('#CourrierSoustypeId').change(function () {
//    var dataString = 'id=' + $('#CourrierSoustypeId').val();
//    chargeSoustypeInfo(dataString);
//});
function chargeSoustypeInfo(dataString) {
    if ($('#CourrierSoustypeId').val() != null && $('#CourrierSoustypeId').val() != '') {
        $.ajax({
            type: 'post',
            url: '/Soustypes/getInformation/' + $('#CourrierSoustypeId').val(),
            data: dataString,
            success: function (data) {
                if (data != "") {
                    $('#tooltipImage').popover({content: data, container: $('#tooltipImage').parent()});
                } else {
                    $('#tooltipImage').popover({content: "Aucune information", container: $('#tooltipImage').parent()});
                }
            }
        });
    }
}

$('#s2id_PliconsultatifHeureHour').css({
    width: '48%',
    float: 'left'
});
$('#s2id_PliconsultatifHeureMin').css({
    width: '48%',
    float: 'right'
});
$('#CourrierDelaiNb').css({
    width: '50%'
});

$("document").ready(function () {
    $('.form_datetime input').datepicker({
        language: 'fr-FR',
        format: "dd/mm/yyyy",
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked'
    });
    $('#acteursTable').bootstrapTable({});


});


