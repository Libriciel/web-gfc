
/**
 * JavaScript helping functions
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */

//$.jGrowl.defaults.position = 'center';
//$.jGrowl.defaults.theme = 'ui-state-focus';
function strpos(haystack, needle, offset) {
    //  discuss at: http://phpjs.org/functions/strpos/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Onno Marsman
    // improved by: Brett Zamir (http://brett-zamir.me)
    // bugfixed by: Daniel Esteban
    //   example 1: strpos('Kevin van Zonneveld', 'e', 5);
    //   returns 1: 14

    var i = (haystack + '')
            .indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}
function processJsonFormCheck(data) {
    try {
        $('.errorJsonFormCheck').remove();
        var json = jQuery.parseJSON(data);

        for (i in json) {
            var model = i;
            for (j in json[i]) {
                var errmsg = json[i][j];
                //FIXME : trouver une technique pour unifier les validator cake et jquery
                if (errmsg != 'Champ obligatoire') {
                    var field = j.substr(0, 1).toUpperCase() + j.substr(1);

                    // ajout afin de gérer les dates et l'icone datepicker
                    if (strpos(j, 'date') !== false) {
                        $("input[name='data[" + model + "][" + j + "]']").addClass('ui-state-highlight').parent().after("<label class='errorJsonFormCheck alert alert-warning' style='width:390px;float:right' for='" + model + field + "' genrated='true'>" + errmsg + "</label>");
                    }
                    else {
                        $("input[name='data[" + model + "][" + j + "]']").addClass('ui-state-highlight').after("<label class='errorJsonFormCheck alert alert-warning' for='" + model + field + "' genrated='true'>" + errmsg + "</label>");
                    }
                    //$('#' + model + field).addClass('ui-state-highlight').after("<label class='errorJsonFormCheck alert alert-warning' for='" + model + field + "' genrated='true'>" + errmsg + "</label>");
                }
            }
        }

    }
    catch (e) {
//		$.jGrowl.defaults.theme = 'alert alert-warning';
        layer.msg('Erreur inattendue (Message standard non reçu)', {});
    }
}

function isInErrorJsonFormCheck(data) {
    try {
        var retval = false;
        var json = jQuery.parseJSON(data);
        for (i in json) {
            for (j in json[i]) {
                //FIXME : trouver une technique pour unifier les validator cake et jquery
                if (json[i][j] != 'Champ obligatoire') {
                    retval = true;
                }
            }
        }
        return retval;
    }
    catch (e) {
        alert('error');
    }
}



function getJsonResponse(data) {
    var json;
    try {
        json = jQuery.parseJSON(data);

        if (json.success === true) {
//			$.jGrowl.defaults.theme = 'ui-state-focus';
        } else {
//			$.jGrowl.defaults.theme = 'alert alert-warning';
        }

        if (json.message != '') {
            layer.msg(json.message, {offset: '70px'});
        } else {
//			$.jGrowl.defaults.theme = 'ui-state-highlight';
            layer.msg('Erreur inattendue (pas de message)', {offset: '70px'});
        }
    }
    catch (e) {
//		$.jGrowl.defaults.theme = 'ui-state-highlight';
        layer.msg('Erreur inattendue (Message standard non reçu)', {offset: '70px'});
    }
    return json;
}

function loadNotifications() {
    gui.request({
        url: "/environnement/getNotifications",
        updateElement: $('#nbNotifications'),
        noErrorMsg: true
    });
}

function initNotifications() {
    $('#nbNotifications').hover(function () {
        $("#divNotif").show();
    }, function () {
        $("#divNotif").hide();
    });
}

function loadTaches() {
    gui.request({
        url: "/taches/getTaches/",
        updateElement: $('#nbTaches'),
        noErrorMsg: true
    });
}

function initTaches() {
    $('#nbTaches').hover(function () {
        $("#divTache").show();
    }, function () {
        $("#divTache").hide();
    });
}

function loadDeleg() {
    gui.request({
        url: "/users/isAway",
        updateElement: $('#absences'),
        noErrorMsg: true
    });
}

function initDeleg() {
    $('#absences').hover(function () {
        $("#divAbsence").show();
    }, function () {
        $("#divAbsence").hide();
    });
}


/*****************************************/
/*
 function setJSelect(select) {
 var id = select.attr('id');

 $('option', select).each(function () {
 $(this).addClass('item');
 $(this).attr('sId', $(this).val());
 });

 var newList = $('<ul>');
 newList.attr('id', id + 'List');
 newList.addClass('jSelectList');

 var addAllBttn = $('<span class="btn btn-info-webgfc">');
 addAllBttn.html('Tout ajouter');
 addAllBttn.button();
 $('span', $(addAllBttn)).css('padding', '3px');
 addAllBttn.click(function () {
 $('option', select).removeAttr('selected').each(function () {
 if ($(this).css('display') == 'block') {
 $(this).attr('selected', 'selected');
 addItem(select);
 }
 });
 });

 select.after(newList.before('<br />Liste des critères selectionnés : ')).after(addAllBttn.before(' '));

 var duplicatedSelect = select.clone(true);
 duplicatedSelect.attr('multiple', 'multiple');
 duplicatedSelect.attr('id', duplicatedSelect.attr('id') + "duplicated");
 $('option', duplicatedSelect).each(function () {
 if ($(this).val() == '' || $(this).val() == 'undefined') {
 $(this).remove();
 }
 });
 duplicatedSelect.css('display', 'none');
 select.before(duplicatedSelect);
 select.attr('name', '');
 select.removeAttr('multiple');

 select.change(function () {
 addItem($(this));
 });
 }


 function delItem(e) {
 var select = $('#' + e.attr('parentSelect'));
 var list = e.parent().parent().parent();
 var listItem = e.parent().parent();
 var sId = e.attr('sId');
 var item;
 $('option', select).each(function () {
 if ($(this).attr('sId') == sId) {
 item = $(this);
 }
 });
 var realItem;
 $('#' + select.attr('id') + 'duplicated option').each(function () {
 if ($(this).attr('sId') == sId) {
 realItem = $(this);
 }
 });

 listItem.remove();
 item.css('display', 'block');
 realItem.removeAttr('selected');

 if ($('li', list).length == 0 && $('.delAllBttn[listId=' + list.attr('id') + ']').length != 0) {
 $('.delAllBttn[listId=' + list.attr('id') + ']').remove();
 }
 }

 function addItem(select) {
 var id = select.attr('id');
 var item = $('option:selected', select);
 var list = $('#' + id + 'List');
 var realItem;
 $('#' + id + 'duplicated option').each(function () {
 if ($(this).attr('sId') == item.attr('sId')) {
 realItem = $(this);
 }
 });

 if (item !== 'undefined' && item.html() !== 'undefined' && item.html() != "") {
 var li = $('<li>');
 li.attr('sId', item.val());
 li.addClass('duplicatedId' + item.val());

 var div = $('<div>');
 div.css({
 'width': '350px',
 'padding': '5px'
 }).addClass('ui-corner-all ui-widget-content');
 div.hover(function () {
 $(this).addClass('ui-state-hover');
 }, function () {
 $(this).removeClass('ui-state-hover');
 });
 div.html(item.html());


 var delBttn = $('<span>');
 delBttn.html("<img src='/img/icons/cancel.png'>");
 delBttn.addClass('delBttn');
 delBttn.css({
 'float': 'right',
 'cursor': 'pointer'
 });
 delBttn.attr('parentSelect', id);
 delBttn.attr('sId', item.val());

 $('span', $(delBttn)).css('padding', '1px');
 delBttn.click(function () {
 delItem($(this));
 });

 list.append(li.append(div.append(delBttn)));
 item.css('display', 'none');
 $('option:selected', select).removeAttr('selected');
 realItem.attr('selected', 'selected');
 }

 if ($('li', list).length > 0 && $('.delAllBttn[listId=' + list.attr('id') + ']').length == 0) {
 var delAllBttn = $('<span>');
 delAllBttn.html('Tout enlever');
 delAllBttn.addClass('delAllBttn');
 delAllBttn.attr('listId', list.attr('id'));
 delAllBttn.button();
 $('span', $(delAllBttn)).css('padding', '3px');
 delAllBttn.click(function () {
 $('span.delBttn', list).each(function () {
 delItem($(this));
 });
 $(this).remove();
 });
 list.after(delAllBttn);
 }
 }

 function resetJSelect(formId) {
 $(formId).find(':input').each(function () {
 switch (this.type) {
 case 'password':
 case 'select-multiple':
 case 'select-one':
 case 'text':
 case 'textarea':
 $(this).val('');
 break;
 case 'checkbox':
 case 'radio':
 this.checked = false;
 }
 });

 $('.jSelectList', $(formId)).empty();
 $('select option', $(formId)).removeAttr('selected');
 $('select option', $(formId)).css('display', 'block');
 $('.delAllBttn').remove();

 }
 */


function setJselectNew(select) {
    var idInput = select.attr('id');
    $("<div class='itemSelectDiv'><ul id='selectList_" + idInput + "' class='itemSelectList'></ul></div>").insertAfter(select);
//    $("<a class='btn btn-info-webgfc addAllBtn' href='#' id='addAll_" + idInput + "' onclick='addAllSelectBtn(" + idInput + ");'><i class='fa fa-plus' aria-hidden='true'></i></a>").insertAfter(select);
    var form = select.parents('form');
    var name = select.attr('name');
    select.change(function () {
        var selected = select.find('option:selected');
        var optionText = selected.text();
        var optionVal = selected.val();
        addItemToSelectList(optionText, optionVal, name, idInput);
    });

}

function addItemToSelectList(optionText, optionVal, selectedName, idInput) {
    if (optionText != "" && optionVal != "") {
        var optionValLiClass = optionVal.replace(/\./g, '_').replace(/\@/g, '_');
        var inputHidden = '<input name="' + selectedName + '[]" type="hidden" value="' + optionVal + '">';
        var deleteItemBtn = '<i class="fa fa-times-circle-o" aria-hidden="true" style="color:red;float:right" onclick="deleteItemSelectBtn($(this),' + idInput + '); "></i>';
        var newSelectdItem = "<li id='selectedLi_" + optionValLiClass + "' >" + optionText + inputHidden + deleteItemBtn + "</li>";

        if ($("#selectList_" + idInput + ":not(:has(#selectedLi_" + optionValLiClass + "))").length == 1) {
            $("#selectList_" + idInput).append(newSelectdItem);
        }

        if ($("#selectList_" + idInput).find('li').length > 0) {
            if ($("#deleteAll_" + idInput).length == 0) {
                $("#selectList_" + idInput).parent().append("<a title='Tout supprimer' class='btn btn-info-webgfc deleteAllBtn' href='#' id='deleteAll_" + idInput + "' onclick='deleteAllSelectBtn(" + idInput + "); ' ><i class='fa fa-minus' aria-hidden='true'></i></a>");
            }
        } else {
            $("#deleteAll_" + idInput).remove();
        }
    }
}

function deleteItemSelectBtn(button, idInput) {
    $(button).parents('li').remove();
    var id = $(idInput).attr('id');
    if ($('#selectList_' + id).find('li').length == 0) {
        $("#deleteAll_" + id).remove();
    }
}

function addAllSelectBtn(idInput) {
    var id = $(idInput).attr('id');
    var selectedName = $('#' + id).attr('name');
    $('#' + id + ' option').each(function () {
        var optionText = $(this).text();
        var optionVal = $(this).val();
        addItemToSelectList(optionText, optionVal, selectedName, id);
    });
}

function deleteAllSelectBtn(idInput) {
    var id = $(idInput).attr('id');
    $('#selectList_' + id).empty();
    $('#deleteAll_' + id).remove();
}
function resetJSelect(formId) {
    $(formId).find(':input').each(function () {
        switch (this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
                $('span.select2-chosen').empty();
                $('div.select2-container').addClass('select2-default');
                $('div.select2-container').removeClass('select2-allowclear');
                $('li.select2-search-choice').remove();
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });

    $('.itemSelectList', $(formId)).empty();
    $('select option', $(formId)).removeAttr('selected');
    $('select option', $(formId)).css('display', 'block');
    $('.deleteAllBtn').remove();
    $('.table-list h3 span').remove();
}
function getMousePos(event) {
    var e = event || window.event;
    return {'x': e.screenX, 'y': screenY}
}
/*****************************************/
function form_validate(form) {
    var result = true;
//    if (form.attr('id') == "CollectiviteAddForm") {
    if (form == "#CollectiviteAddForm") {
        if ($('#paramconn').val() != "" &&
                ( $("#CollectiviteName").val() != "" ||
                $("#CollectiviteName").val() != undefined ) &&
                ( $("#UserUsername").val() != "" ||
                $("#UserUsername").val() != undefined ) &&
                ( $("#UserNom").val() != "" ||
                $("#UserNom").val() != undefined ) &&
                ( $("#UserPrenom").val() != "" ||
                $("#UserPrenom").val() != undefined ) &&
                ( $("#UserMail").val() != "" ||
                $("#UserMail").val() != undefined ) &&
                ( $("#UserPassword").val() != "" ||
                $("#UserPassword").val() != undefined ) &&
                $("#UserPassword2").val() != "" &&
                $("#CollectiviteConn").val() != "" &&
                $("#CollectiviteLoginSuffix").val() != ""
                ) {
            result = true;
        } else {
            result = false;
        }
//    } else if (form.attr('id') == "CollectiviteEditForm") {
    } else if (form == "#CollectiviteEditForm") {
        if ($("#CollectiviteName").val() != "" &&
//                $("#CollectiviteSiren").val() != "" &&
//                $("#CollectiviteCodeinsee").val() != "" &&
//                $("#CollectiviteAdresse").val() != "" &&
//                $("#CollectiviteCodepostal").val() != "" &&
//                $("#CollectiviteVille").val() != "" &&
//                $("#CollectiviteTelephone").val() != "" &&
                $("#UserUsername").val() != "" &&
                $("#UserNom").val() != "" &&
                $("#UserPrenom").val() != "" &&
                $("#UserMail").val() != "" &&
                $("#CollectiviteConn").val() != "" &&
                $("#CollectiviteLoginSuffix").val() != ""
                ) {
            result = true;
        } else {
            result = false;
        }
//    } else if (form.attr('id') == "CollectiviteAdminForm") {
    } else if (form == "#CollectiviteAdminForm") {
        if ($("#CollectiviteName").val() != "" && $("#UserUsername").val() != "" &&
                $("#UserNom").val() != "" &&
                $("#UserPrenom").val() != "" &&
                $("#UserMail").val() != "" &&
                $("#UserPassword").val() != "" &&
                $("#UserPassword2").val() != "" &&
                $("#CollectiviteConn").val() != "" &&
                $("#CollectiviteLoginSuffix").val() != ""){
//                $("#CollectiviteSiren").val() != "" &&
//                $("#CollectiviteCodeinsee").val() != "" &&
//                $("#CollectiviteAdresse").val() != "" &&
//                $("#CollectiviteCodepostal").val() != "" &&
//                $("#CollectiviteVille").val() != "" &&
//                $("#CollectiviteTelephone").val() != "" &&
//                $("#CaffaireDefCompteur").val() != "" &&
//                $("#CaffaireDefReinit").val() != "" &&
//                $("#CdossierDefCompteur").val() != "" &&
//                $("#CdossierDefReinit").val() != "") {
            result = true;
        } else {
            result = false;
        }
    } else {
//    console.log(form.validator('validate'));
        if (!form.has('.has-error').length) {
            form.validator('destroy');
            if (form.validator('validate').has('.has-error').length) {
                result = false;
            }
        } else {
            result = false;
        }
    }

    return result;
}
