/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $("[class*=_pagination]").each(function () {
        var class_pagination = $(this).attr('class');
        var res = class_pagination.split("_");
        var tab_pane_id = res[0];
        $(this).hide();
        if ($("[class*=_pagination]").parents('.tab-pane').attr('id') == tab_pane_id) {
            var bootstrap_table = $("[class*=_pagination]").parents('#' + tab_pane_id).find('.bootstrap-table');
            $(this).insertAfter(bootstrap_table);
            $(this).show();
            $('body').on('click', '.pagination li span', function () {
                window.location.href = $(this).find('a').attr('href');
            });
        }
    });


    function nbInCircuit(fluxChoix) {
        var nbInCircuit;
        $.each(fluxChoix, function (i, fluxId) {
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/Courriers/isInTraite',
                data: 'fluxId=' + fluxId,
                success: function (data) {
                    console.log(data);
                    if (data['inCircuit'] == true) {
                        if ($.inArray(fluxId, inCircuitId) == -1) {
                            inCircuitId.push(fluxId);
                        }
                        nbInCircuit = inCircuitId.length;
                        console.log(nbInCircuit);
                        isButtonAiguillage(nbInCircuit);
                    }
                    if (data['fromMesServices'] == true) {
                        //isFluxMesServices(true);
                    }
                }
            });

        });
        return nbInCircuit;
    }





});

function detacheflux(fluxId, desktopId) {
    swal({
        showCloseButton: true,
        title: 'Détacher un flux',
        text: 'Vous voulez détacher ce flux depuis votre bannette de copie ? ',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        cancelButtonColor: "#3085d6",
        confirmButtonText: '<i class="fa fa-check" aria-hidden="true"></i> Valider',
        cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
    }).then(function (data) {
        if (data) {
            window.location.href = "/courriers/detache/" + fluxId + '/' + desktopId;
        } else {
            swal({
                showCloseButton: true,
                title: "Annulé!",
                text: "Vous n\'avez pas détaché.",
                type: "error",

            });
        }
    });
    $('.swal2-cancel').css('margin-left', '-320px');
    $('.swal2-cancel').css('background-color', 'transparent');
    $('.swal2-cancel').css('color', '#5397a7');
    $('.swal2-cancel').css('border-color', '#3C7582');
    $('.swal2-cancel').css('border', 'solid 1px');

    $('.swal2-cancel').hover(function () {
        $('.swal2-cancel').css('background-color', '#5397a7');
        $('.swal2-cancel').css('color', 'white');
        $('.swal2-cancel').css('border-color', '#5397a7');
    }, function () {
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
    });

}

function preveiwdocumentflux(reference) {
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: '/Courriers/getFluxPreview/' + reference,
        success: function (data) {
            var documentId = data.documentId;
            if (documentId != null) {
                console.log(documentId);
                gui.formMessage({
                    title: 'Prévisualisation du document',
                    url: '/Documents/getPreview/' + documentId + '/modal',
                    buttons: {
                        '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        }
                    }
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: 'Oops...',
                    text: "Il n'existe pas de document associé à ce flux",
                    type: "error",

                });
            }
        }
    });
}

function deletflux(fluxId) {
    swal({
        showCloseButton: true,
        title: supModalTitle,
        text: supModalContent,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        cancelButtonColor: "#5397a7",
        confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
        cancelButtonText: btnCancel,
    }).then(function (data) {
        if (data) {
            window.location.href = "/courriers/delete/" + fluxId;
        } else {
            swal({
                showCloseButton: true,
                title: "Annulé!",
                text: "Vous n\'avez pas supprimé.",
                type: "error",

            });
        }
    });
    $('.swal2-cancel').css('margin-left', '-320px');
    $('.swal2-cancel').css('background-color', 'transparent');
    $('.swal2-cancel').css('color', '#5397a7');
    $('.swal2-cancel').css('border-color', '#3C7582');
    $('.swal2-cancel').css('border', 'solid 1px');

    $('.swal2-cancel').hover(function () {
        $('.swal2-cancel').css('background-color', '#5397a7');
        $('.swal2-cancel').css('color', 'white');
        $('.swal2-cancel').css('border-color', '#5397a7');
    }, function () {
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
    });
}

function isButtonAiguillage(nbInCircuit) {
    if (nbInCircuit == 0) {
        gui.enablebutton({
            element: $('#bannetteControls'),
            button: '<i class="fa fa-backward" aria-hidden="true"></i>',
            action: function () {
                //vérifier tous les checkbox de tous les tabs
                var tabChecked = new Array();
                $('.selected').each(function () {
                    tabChecked.push($(this).find('.checkItem').attr('itemid'));
                });
                $('.checkItem').each(function () {
                    if ($(this).prop('checked')) {
                        tabChecked.push($(this).attr('itemId'));
                    }
                });
                if (tabChecked.length == 0) {
                    sweetAlert({
                        title: 'Oops...',
                        text: 'Veuillez choisir au moins un flux!',
                        type: 'error'
                    });
                } else {
                    var form = "<form id='formChecked'>";
                    for (i in tabChecked) {
                        form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
                    }
                    form += "</form>";
                    gui.formMessage({
                        title: 'Ré-aiguillage du flux',
                        url: "/courriers/sendBackToDisp",
                        data: $(form).serialize(),
                        buttons: {
                            '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
                                $(this).parents(".modal").modal('hide');
                                $(this).parents(".modal").empty();
                            },
                            '<i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer': function () {
                                gui.request({
                                    url: '/courriers/sendBackToDisp',
                                    data: $('#CourrierSendBackToDispForm').serialize()
                                }, function () {
                                    $('input[type="checkbox"]').prop('checked', false);
                                    window.location.reload();
                                    layer.msg('Les informations ont été enregistrées', {});
                                });
                                $(this).parents(".modal").modal('hide');
                                $(this).parents(".modal").empty();
                            }
                        }
                    });
                }
            }
        });
    } else {
        gui.disablebutton({
            element: $('#bannetteControls'),
            button: '<i class="fa fa-backward" aria-hidden="true"></i>',
            unbindAction: true
        });
    }
}

function dateSort(a, b) {
	a = a.split("/").reverse().join("/");
	b = b.split("/").reverse().join("/");
	a = new Date(a);
	b = new Date(b);
	console.log(a);
	if(a < b) {
		return 1;
	}
	else if( a > b) {
		return -1;
	}
	else {
		return 0;
	}
}
