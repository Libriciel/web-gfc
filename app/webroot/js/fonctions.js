/**
 * JavaScript helping functions
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */

function lister_services(params, url) {
	var urlb = url + params.value;
	document.location = urlb;
}

function lister_circuits(params, url) {
	$('.submit').hide();
	//var urlb=url+params.value+'/1';
	var urlb = url + params.value + '/1';
	//        alert(urlb);
	document.location = urlb;
}


function checkSelectedCircuit($id) {
	if ($id == "0") {
		alert('Vous devez d\'abord choisir un circuit');
	}
}
