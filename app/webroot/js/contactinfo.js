
/**
 * Contactinfo helpers
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
function showContactinfo() {
    $('.contactInfoCard').hide();
    $('#contactInfoCard' + $('#CourrierContactId option:selected').val()).show();
}

function createContactinfo(contactinfo) {
//console.log(contactinfo);
    var id = typeof contactinfo['id'] !== "undefined" ? contactinfo['id'] : contactinfo['Contactinfo.id'];
    var contactInfoCard = $('<table />').attr('id', 'contactInfoCard' + id).addClass('contactInfoCard');
    var cpt = 0, nbRows = 30, trs = [];
    for (var j in contactinfo) {
        if (j !== 'Contact.created' && j !== 'Contact.active' && j !== 'Contact.modified' && j !== 'Contact.addressbook_id' && j !== 'Contact.citoyen' && j !== 'Organisme' && j !== 'Contact.slug' && j !== 'name' && j !== 'id' && j !== 'Contact.organisme_id' && j !== 'organisme_id' && j !== 'Organisme asscocié') {
            if( j == 'Adresse complète' ||  j == 'N° de la voie'  ) {
                contactinfo[j] = stripslashes(contactinfo[j]);
            }
            if (cpt < nbRows) {
                trs[cpt] = $('<div class="col-sm-6" />').append($('<dt />').css({'font-weight': 'bold', 'color': '#5397a7'}).html(j)).append($('<dd />').html(contactinfo[j]));
            } else {
                trs[cpt - nbRows].append($('<dt />').css({'font-weight': 'bold', 'color': '#5397a7'}).html(j)).append($('<dd />').html(contactinfo[j]));
            }
            cpt++;
        }
    }
    for (var i = 0; i < 14; i++) {
        $(contactInfoCard).append(trs[i]);
    }
    return contactInfoCard;
}

function showOrganismeinfo() {
    $('.organismeInfoCard').hide();
    $('#organismeInfoCard' + $('#CourrierOrganismeId option:selected').val()).show();
}

function createOrganismeinfo(organisme) {
//console.log(organisme);
    var id = typeof organisme['id'] !== "undefined" ? organisme['id'] : organisme['Organisme.id'];
    var organismeInfoCard = $('<table />').attr('id', 'organismeInfoCard' + id).addClass('organismeInfoCard');
    var cpt = 0, nbRows = 30, trs = [];
    for (var j in organisme) {
        if (j !== 'Organisme.created' && j !== 'Organisme.active' && j !== 'Organisme.modified' && j !== 'id' && j !== 'Organisme.slug') {
            if( j == 'Adresse complète' ||  j == 'N° de la voie'  ) {
                organisme[j] = stripslashes(organisme[j]);
            }
            if (cpt < nbRows) {
                trs[cpt] = $('<div class="col-sm-6" />').append($('<dt />').css({'font-weight': 'bold', 'color': '#5397a7'}).html(j)).append($('<dd />').html(organisme[j]));
            } else {
                trs[cpt - nbRows].append($('<dt />').css({'font-weight': 'bold', 'color': '#5397a7'}).html(j)).append($('<dd />').html(organisme[j]));
            }
            cpt++;
        }
    }
    for (var i = 0; i < 14; i++) {
        $(organismeInfoCard).append(trs[i]);
    }

    $('#organismeInfoCard' + $('#CourrierOrganismeId option:selected').val()).css({'margin-bottom' : '15px'});
    return organismeInfoCard;
}
function stripslashes(str) {
    str = str.replace(/\\'/g, '\'');
    str = str.replace(/\\"/g, '"');
    str = str.replace(/\\0/g, '\0');
    str = str.replace(/\\\\/g, '\\');
    return str;
}
