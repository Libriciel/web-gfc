<?php
trait ControllerTestCaseAccessTrait
{
	/**
	 * Récupère les droits effectifs en base de données pour un profil au sein d'une entité.
	 */
	protected function _getListeDroitByRole($organisation_id, $role) {
		$roleDroit = ClassRegistry::init('Group');
		$query = [
			'fields' => ['Group.liste_droit_id'],
			'joins' => [
				$roleDroit->join('Role', ['type' => 'INNER'])
			],
			'conditions' => [
				'Role.libelle' => $role,
				'Role.organisation_id' => $organisation_id,
			]
		];
		return $roleDroit->find('list', $query);
	}

	// dans logout
	// debug($this->Session->read());die();
	protected function _setupUserSession($username) {
		// @todo: ajouter les utilisateurs manquants
		$sessions = [
			'Superadministrateur.superadmin' => [
				'Auth.User.id' => 1
			]
		];
		if ($username === null) {
			$session = [];//@todo: pour supprimer le contenu d'une session, voir WebcilUsersComponentTest::tearDown
		} else {
			// @todo: if not set
			$session = $sessions[$username];
		}

		CakeSession::write($session);
	}

	public function assertActionAccess($expectedStatus, $user, $url, $options = []) {
		$options += ['method' => 'GET', 'session' => []];
		$this->_setupUserSession($user);
		if (empty($options['session']) === false) {
			CakeSession::write($options['session']);
		}
		if (in_array($expectedStatus, [200, 301, 302], true)) {
			$this->testAction($url, $options);
			$this->assertSame($expectedStatus, $this->controller->response->statusCode());
		} else {
			// @see https://book.cakephp.org/2/en/development/exceptions.html#built-in-exceptions-for-cakephp
			$exceptions = [
				403 => 'ForbiddenException',
				404 => 'NotFoundException',
				405 => 'MethodNotAllowedException',
				500 => 'RuntimeException',
			];
			$this->setExpectedException($exceptions[$expectedStatus]);
			$this->testAction($url, $options);
		}
	}
}
