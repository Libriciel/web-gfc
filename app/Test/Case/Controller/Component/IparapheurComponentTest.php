<?php

/**
 * IParapheurComponentTest file
 *
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The AGPL v3 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @since       web-delib v4.3
 * @license     https://choosealicense.com/licenses/agpl-3.0/ AGPL v3 License
 */
App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('IparapheurComponent', 'Controller/Component');

/**
 * Classe IParapheurComponentTest.
 *
 * @version 3
 * @package app.Test.Case.Controller
 */
class IparapheurComponentTest extends CakeTestCase
{

	public $IparapheurComponent = null;
	public $Controller = null;

	public function setUp()
	{
		parent::setUp();
		// Configurer notre component et faire semblant de tester le controller
		$Collection = new ComponentCollection();
		$this->IparapheurComponent = new IparapheurComponent($Collection);
		$CakeRequest = new CakeRequest();
		$CakeResponse = new CakeResponse();
		$this->Controller = new TestIparapheurController($CakeRequest, $CakeResponse);
		$this->IparapheurComponent->startup($this->Controller);

		ClassRegistry::init(('Connecteur'));
		$this->Connecteur = new Connecteur();
		$this->Connecteur->setDataSource('test');
		$this->connecteur = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array('Connecteur.name ILIKE' => '%Parapheur%'),
				'contain' => false
			)
		);
	}

	/**
	 * Méthode exécutée avant chaque test.
	 *
	 * @return void
	 */
	public function tearDown()
	{
		parent::tearDown();
		unset($this->IparapheurComponent);
		unset($this->Controller);
	}

	/**
	 * Test GetVersion()
	 *
	 * @version 4.3
	 * @return void
	 */
	public function testEchoWebservice()
	{
		return ($this->IparapheurComponent->echoWebservice($this->connecteur));
	}

}

// Un faux controller pour tester against
class TestIparapheurController extends Controller
{

	public $paginate = null;

}
