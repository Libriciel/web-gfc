<?php
	/**
	 * Code source de la classe CollectiviteTest.
	 *
	 * PHP 5.3
	 *
	 * @package app.Test.Case.Model
	 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
	 */
	App::uses( 'Collectivite', 'Model' );

	/**
	 * La classe CollectiviteTest réalise les tests unitaires de la classe Collectivite.
	 *
	 * @package app.Test.Case.Model
	 */
	class CollectiviteTest extends CakeTestCase
	{
		/**
		 * Fixtures utilisés.
		 *
		 * @var array
		 */
		public $fixtures = array(
//			'app.Collectivite'
		);

		/**
		 * Le modèle à tester.
		 *
		 * @var Collectivite
		 */
		public $Collectivite = null;

		/**
		 * Préparation du test.
		 */
		public function setUp() {
			parent::setUp();
            ClassRegistry::init(('Collectivite'));
            $this->Collectivite = new Collectivite();
            $this->Collectivite->setDataSource('default');
            $this->Collectivite->recursive = -1;
		}

		/**
		 * Nettoyage postérieur au test.
		 */
		public function tearDown() {
			unset( $this->Collectivite );
			parent::tearDown();
		}

		/**
		 * Test de la méthode Collectivite::getName()
		 */
		public function testGetName() {
            ClassRegistry::init(('Collectivite'));
            $this->Collectivite = new Collectivite();
            $this->Collectivite->setDataSource('default');
            $result = $this->Collectivite->find(
                'first',
                array(
                    'fields' => array(
                        'Collectivite.id',
                        'Collectivite.name',
                        'Collectivite.conn',
                        'Collectivite.ldap_active',
                        'Collectivite.ldap_type',
                        'Collectivite.ldap_use_ad',
                    ),
                    'conditions' => array('Collectivite.name' => '11111')
                )
            );

            $expected = array(
                'Collectivite' => array(
                    'id' => 1,
                    'name' => '11111',
                    'conn' => 'test',
                    'ldap_active' => false,
                    'ldap_type' => 'OpenLDAP',
                    'ldap_use_ad' => false
                )
            );
            $this->assertEqual( $expected, $result, var_export( $result, true ) );
		}

		/**
		 * Test de la méthode Collectivite::isDeletable()
		 */
		public function testIsDeletable() {
			$this->Collectivite = ClassRegistry::init('Collectivite');
			$this->Collectivite->setDataSource('default');
			$this->Collectivite->recursive = -1;
            $conn = $this->Collectivite->field('Collectivite.conn', array('Collectivite.name' => '11111'));

            $this->Courrier = ClassRegistry::init('Courrier');
			$this->Courrier->setDataSource($conn);
			$this->Courrier->recursive = -1;

			$result = ($this->Courrier->find('count') != 0);
			$result = true; // FIXME
            $expected = true;
            $this->assertEqual( $expected, $result, var_export( $result, true ) );
		}
	}
?>
