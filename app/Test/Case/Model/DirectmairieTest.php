<?php
/**
 * Code source de la classe DirectmairieTest.
 *
 * PHP 5.3
 *
 * @package app.Test.Case.Model
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */
App::uses( 'Directmairie', 'Model' );

App::uses('Traitement', 'Cakeflow.Model');
App::uses('Circuit', 'Cakeflow.Model');
App::uses('CakeflowAppModel', 'Cakeflow.Model');

App::uses('SCComment', 'SimpleComment.Model');
App::uses('SCCommentReader', 'SimpleComment.Model');
/**
 * La classe DirectmairieTest réalise les tests unitaires de la classe Directmairie.
 *
 * @package app.Test.Case.Model
 */
class DirectmairieTest extends CakeTestCase
{

	public $fixtures = array(
		'app.ActiviteOrganisme',
		'app.ContactEvent',
		'app.ContactOperation',
		'app.RechercheContact',
		'app.Ar',
		'app.Bancontenu',
		'app.CourrierMetadonnee',
		'app.CourrierRepertoire',
		'app.DesktopTache',
		'app.RechercheTache',
		'app.Tache',
		'app.Sendmail',
		'app.Document',
		'app.Notifieddesktop',
		'app.Notifquotidien',
		'app.Pliconsultatif',
		'app.Relement',
		'app.Sendmail',
		'app.Plugin/Cakeflow/Signature',
		'app.Plugin/Cakeflow/Visa',
		'app.Plugin/Cakeflow/Traitement',
		'app.Plugin/Cakeflow/Composition',
		'app.Plugin/Cakeflow/Etape',
		'app.Plugin/SimpleComment/SCCommentReader',
		'app.Plugin/SimpleComment/SCComment',
		'app.Ordreservice',
		'app.Courrier',
		'app.Contactinfo',
		'app.Contact',
		'app.RechercheOrganisme',
		'app.OrganismeOperation',
		'app.Organisme',
		'app.Bancontenu',
	);

	public $name = 'Courrier';
	public $table = 'courriers';

	/**
	 * Le modèle à tester.
	 *
	 * @var Directmairie
	 */
	public $Courrier = null;

	/**
	 * Préparation du test.
	 */
	public function setUp() {
		parent::setUp();
		$this->Courrier = ClassRegistry::init('Courrier');
		$this->Courrier->setDataSource('test');
		$this->Courrier->recursive = -1;
	}

	/**
	 * Nettoyage postérieur au test.
	 */
//	public function tearDown() {
//		parent::tearDown();
//	}

	/**
	 * Test de la méthode Directmairie::add()
	 */
	public function testCreateCourrier() {
		$this->Courrier = ClassRegistry::init('Courrier');
		$this->Courrier->setDataSource('test');
		$this->Courrier->recursive = -1;

		$data = array(
			'Courrier' => array(
				'name' => '[DM] Premier flux',
				'objet' => 'Ceci est un flux issue de Direct Mairie',
				'date' => '2020-07-06',
				'datereception' => '2020-07-06',
				'reference' => '2099000001',
				'contact_id' => 1,
				'organisme_id' => 1,
				'created' => '2020-06-23 06:09:56.309561',
				'modified' => '2020-06-23 06:09:56.309561',
				'priorite' => '0',
				'mail_retard_envoye' => false,
		 		'mail_clos_envoye' => false,
		 		'mail_bloque_parapheur' => false,
				'pastell_etat' => '0',
				'signee' => false

			)
		);

		// la création se fait-elle ?
		$this->Courrier->create( $data );
		$this->assertTrue( $this->Courrier->beforeSave() );

		$result = $this->Courrier->data;

		// la sauvegarde se fait-elle ?
		$success = $this->Courrier->save( $data );
		$this->assertTrue( !empty( $success ) );

		$expected = array(
			'Courrier' => array(
				'name' => '[DM] Premier flux',
				'objet' => 'Ceci est un flux issue de Direct Mairie',
				'date' => '2020-07-06',
				'datereception' => '2020-07-06',
				'reference' => '2099000001',
				'contact_id' => 1,
				'organisme_id' => 1,
				'created' => '2020-06-23 06:09:56.309561',
				'modified' => '2020-06-23 06:09:56.309561',
				'priorite' => '0',
				'mail_retard_envoye' => false,
				'mail_clos_envoye' => false,
				'mail_bloque_parapheur' => false,
				'pastell_etat' => '0',
				'signee' => false
			),
		);
		// le résultat correspond à ce qui est attendu ?
		$this->assertEqual( $result, $expected, var_export( $result, true ) );

		$expected = array(
			'Courrier' => array(
				'name' => '[DM] Premier flux',
				'objet' => 'Ceci est un flux issue de Direct Mairie',
				'date' => '2020-07-06',
				'datereception' => '2020-07-06',
				'reference' => '2099000002',
				'contact_id' => 1,
				'organisme_id' => 1,
				'created' => '2020-06-23 06:09:56.309561',
				'modified' => '2020-06-23 06:09:56.309561',
				'priorite' => '0',
				'mail_retard_envoye' => false,
				'mail_clos_envoye' => false,
				'mail_bloque_parapheur' => false,
				'pastell_etat' => '0',
				'signee' => false
			),
		);
		// le résultat ne correspond pas à ce qui est attendu ?
		$this->assertNotEquals( $result, $expected, var_export( $result, true ) );


	}

}
?>
