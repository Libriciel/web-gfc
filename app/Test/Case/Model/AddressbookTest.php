<?php
	/**
	 * Code source de la classe AddressbookTest.
	 *
	 * PHP 5.3
	 *
	 * @package app.Test.Case.Model
	 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
	 */
	App::uses( 'Addressbook', 'Model' );

	/**
	 * La classe AddressbookTest réalise les tests unitaires de la classe Addressbook.
	 *
	 * @package app.Test.Case.Model
	 */
	class AddressbookTest extends CakeTestCase
	{
		/**
		 * Fixtures utilisés.
		 *
		 * @var array
		 */
		public $fixtures = array(
//			'app.Addressbook'
		);

		/**
		 * Le modèle à tester.
		 *
		 * @var Addressbook
		 */
		public $Addressbook = null;

		/**
		 * Préparation du test.
		 */
		public function setUp() {
			parent::setUp();
            $this->Addressbook = ClassRegistry::init('Addressbook');
            $this->Addressbook->setDataSource('test');
            $this->Addressbook->recursive = -1;
		}

		/**
		 * Nettoyage postérieur au test.
		 */
		public function tearDown() {
			unset( $this->Addressbook );
			parent::tearDown();
		}


		/**
		 * Test de la méthode Addressbook::add()
		 */
		public function testAdd() {
            $this->Addressbook = ClassRegistry::init('Addressbook');
            $this->Addressbook->setDataSource('test');
            $this->Addressbook->recursive = -1;

			$addressbookCarnetPrinicpal = $this->Addressbook->find(
				'first',
				array(
					'conditions' => array('Addressbook.name' => 'Carnet principal'),
					'contain'  => false
				)
			);

			if( empty($addressbookCarnetPrinicpal) ) {

				$data = array(
					'Addressbook' => array(
						'active' => true,
						'name' => 'Carnet principal',
						'description' => 'Carnet adresse principal',
						'private' => false
					)
				);
				$this->Addressbook->create($data);
				$this->assertTrue($this->Addressbook->beforeSave());

				$result = $this->Addressbook->data;

				$success = $this->Addressbook->save($data);
				$this->assertTrue(!empty($success));

				$expected = array(
					'Addressbook' => array(
						'active' => true,
						'name' => 'Carnet principal',
						'description' => 'Carnet adresse principal',
						'private' => false,
						'created' => 'now()',
						'modified' => 'now()'
					),
				);
			}
			else {
				$expected = array();
				$result = array();
			}
			$this->assertEqual( $result, $expected, var_export( $result, true ) );
		}
		/**
		 * Test de la méthode Addressbook::edit()
		 */
		public function testEdit() {
            $this->Addressbook = ClassRegistry::init('Addressbook');
            $this->Addressbook->setDataSource('test');
            $this->Addressbook->recursive = -1;

			$addressbookCitoyen = $this->Addressbook->find(
				'first',
				array(
					'conditions' => array('Addressbook.name' => 'Citoyen'),
					'contain'  => false
				)
			);
			if( empty($addressbookCitoyen) ) {
				// 0. Enregistrement du premier carnet d'adresse
				$data = array(
					'Addressbook' => array(
						'name' => 'Citoyen',
						'description' => 'Carnet des citoyens',
						'private' => true,
						'active' => true
					)
				);
				$this->Addressbook->create($data);
				$success = $this->Addressbook->save($data);
				$this->assertTrue(!empty($success));

				$id = $this->Addressbook->id;
			}
			else {
				$id = $addressbookCitoyen['Addressbook']['id'];
			}

			$addressbookElus = $this->Addressbook->find(
				'first',
				array(
					'conditions' => array('Addressbook.name' => 'Carnet principal'),
					'contain'  => false
				)
			);
			if( empty($addressbookElus) ) {
				// 1. Modification du carnet elus
				$data = array(
					'Addressbook' => array(
						'id' => $id,
						'name' => 'Carnet principal',
						'description' => 'Carnet adresse principal',
						'private' => true,
						'active' => true,
						'created' => 'now()',
						'modified' => 'now()'
					)
				);
				$this->Addressbook->create();
				$this->Addressbook->save($data);
				$this->assertTrue($this->Addressbook->beforeSave());

				$result = $this->Addressbook->data;

				$expected = array(
					'Addressbook' => array(
						'id' => $id,
						'active' => true,
						'name' => 'Carnet principal',
						'description' => 'Carnet adresse principal',
						'private' => true,
						'created' => 'now()',
						'modified' => 'now()'
					),
				);
			}
			else {
				$data =$result = $expected = array();
			}
			$this->assertEqual( $data, $expected, var_export( $result, true ) );
		}
		/**
		 * Test de la méthode Addressbook::delete()
		 */
		public function testDelete() {
			$this->Addressbook = ClassRegistry::init('Addressbook');
			$this->Addressbook->setDataSource('test');
			$this->Addressbook->recursive = -1;

			$addressbook1 = $this->Addressbook->find(
				'first',
				array(
					'conditions' => array('Addressbook.id' => 1),
					'contain'  => false
				)
			);
			if( !empty( $addressbook1 ) ) {
				$result = $this->Addressbook->delete( 1 );
				$this->assertEqual( $result, true, var_export( $result, true ) );
			}

			$addressbook2 = $this->Addressbook->find(
				'first',
				array(
					'conditions' => array('Addressbook.id' => 2),
					'contain'  => false
				)
			);
			if( !empty( $addressbook2 ) ) {
				$result2 = $this->Addressbook->delete(2);
				$this->assertEqual($result2, true, var_export($result, true));
			}


			$query = array(
				'fields' => array(
					'Addressbook.id',
					'Addressbook.name'
				),
				'conditions' => array(
					'Addressbook.id' => 1
				),
				'contain' => false,
				'recursive' => -1
			);
			$result = $this->Addressbook->find( 'first', $query );
			$this->assertEqual( $result, array(), var_export( $result, true ) );
		}
		/**
		 * Test de la méthode Addressbook::getName()
		 */
		public function testGetAddressbooks() {
            $this->Addressbook = ClassRegistry::init('Addressbook');
            $this->Addressbook->setDataSource('test');
            $this->Addressbook->recursive = -1;
            $toto = $this->Addressbook->find('all');


            $result = $this->Addressbook->find(
                'first',
                array(
                    'fields' => array(
                        'Addressbook.id',
                        'Addressbook.name',
                        'Addressbook.description',
                        'Addressbook.private',
                        'Addressbook.active'
                    ),
                    'conditions' => array('Addressbook.id' => 3), 'contain'  => false));

            $expected = array(
                'Addressbook' => array(
                    'id' => 3,
					'name' => 'Citoyen',
					'description' => 'Carnet des citoyens',
                    'private' => true,
                    'active' => true
                )
            );
			if( empty($result) ) {
				$expected = array();
			}
            $this->assertEqual( $expected, $result, var_export( $result, true ) );
		}


	}
?>

