<?php
	/**
	 * Code source de la classe TemplatemailTest.
	 *
	 * PHP 5.3
	 *
	 * @package app.Test.Case.Model
	 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
	 */
	App::uses( 'Templatemail', 'Model' );

	/**
	 * La classe TemplatemailTest réalise les tests unitaires de la classe Templatemail.
	 *
	 * @package app.Test.Case.Model
	 */
	class TemplatemailTest extends CakeTestCase
	{
		/**
		 * Fixtures utilisés.
		 *
		 * @var array
		 */
		public $fixtures = array(
//			'app.Templatemail'
		);

		/**
		 * Le modèle à tester.
		 *
		 * @var Templatemail
		 */
		public $Templatemail = null;

		/**
		 * Préparation du test.
		 */
		public function setUp() {
			parent::setUp();
            ClassRegistry::init(('Templatemail'));
            $this->Templatemail = new Templatemail();
            $this->Templatemail->setDataSource('test');
            $this->Templatemail->recursive = -1;
		}

		/**
		 * Nettoyage postérieur au test.
		 */
		public function tearDown() {
			unset( $this->Templatemail );
			parent::tearDown();
		}

		/**
		 * Test de la méthode Templatemail::add()
		 */
		public function testAdd() {
            ClassRegistry::init(('Templatemail'));
            $this->Templatemail = new Templatemail();
            $this->Templatemail->setDataSource('test');

            $data = array(
				'Templatemail' => array(
                    'active' => true,
					'name' => 'Premier mail de notif',
					'subject' => 'Ceci est un essai',
					'object' => 'Ceci est un objet'
				)
			);
			$this->Templatemail->create( $data );
			$this->assertTrue( $this->Templatemail->beforeSave() );

			$result = $this->Templatemail->data;

			$success = $this->Templatemail->save( $data );
			$this->assertTrue( !empty( $success ) );

			$expected = array(
				'Templatemail' => array(
                    'active' => true,
					'name' => 'Premier mail de notif',
					'subject' => 'Ceci est un essai',
					'object' => 'Ceci est un objet',
					'created' => 'now()',
					'modified' => 'now()'
				),
			);
			$this->assertEqual( $result, $expected, var_export( $result, true ) );
		}
		/**
		 * Test de la méthode Templatemail::edit()
		 */
		public function testEdit() {
            ClassRegistry::init(('Templatemail'));
            $this->Templatemail = new Templatemail();
            $this->Templatemail->setDataSource('test');

            // 0. Enregistrement de la première orientation
			$data = array(
				'Templatemail' => array(
					'name' => 'Deuxième mail de notif',
					'subject' => 'Ceci est un 2ème essai',
                    'object' => 'Objet',
                    'active' => true,
					'created' => 'now()',
					'modified' => 'now()'
				)
			);
			$this->Templatemail->create();
			$success = $this->Templatemail->save( $data );
			$this->assertTrue( !empty( $success ) );

			$id = $this->Templatemail->id;

			// 1. Modification du template mail
			$data = array(
				'Templatemail' => array(
					'id' => $id,
					'name' => 'Deuxième mail de notification',
					'subject' => 'Ceci est un 2ème essai de modification',
                    'object' => 'Objet',
                    'active' => true,
					'created' => 'now()',
					'modified' => 'now()'
				)
			);
			$this->Templatemail->create( $data );
			$this->assertTrue( $this->Templatemail->beforeSave() );

			$result = $this->Templatemail->data;

			$expected = array(
				'Templatemail' => array(
                    'id' => $id,
                    'active' => true,
					'name' => 'Deuxième mail de notification',
					'subject' => 'Ceci est un 2ème essai de modification',
                    'object' => 'Objet',
					'created' => 'now()',
					'modified' => 'now()'
				),
			);
			$this->assertEqual( $result, $expected, var_export( $result, true ) );
		}

		/**
		 * Test de la méthode Templatemail::getName()
		 */
		public function testGetTemplatemails() {
            ClassRegistry::init('Templatemail');
            $this->Templatemail = new Templatemail();
            $this->Templatemail->setDataSource('test');
            $result = $this->Templatemail->find('first', array('conditions' => array('Templatemail.id' => 1), 'contain'  => false));

            $expected = array(
                'Templatemail' => array(
                    'id' => 1,
                    'name' => 'Premier mail de notif',
					'subject' => 'Ceci est un essai',
					'object' => 'Ceci est un objet',
                    'active' => true,
					'created' => $result['Templatemail']['created'],
					'modified' => $result['Templatemail']['modified']
                )
            );
            $this->assertEqual( $expected, $result, var_export( $result, true ) );
		}
	}
?>
