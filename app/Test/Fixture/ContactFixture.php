<?php
/**
 * Code source de la classe CollectiviteFixture.
 *
 * PHP 5.3
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

/**
 * Classe ContactFixture.
 *
 * @package app.Test.Fixture
 */
class ContactFixture extends CakeTestFixture {

	public $useDbConfig = 'test';
	/**
	 * On importe la définition de la table, pas les enregistrements.
	 *
	 * @var array
	 */
	public $import = array('model' => 'Contact', 'records' => false, 'connection' => 'test');


	/**
	 * Définition des enregistrements.
	 *
	 * @var array
	 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Sans contact',
			'nom' => ' Sans contact',
			'organisme_id' => 1
		)
	);
}
?>
