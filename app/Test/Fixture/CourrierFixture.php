<?php

class CourrierFixture extends CakeTestFixture {
	var $name = 'Courrier';

	var $import = array('model' => 'Courrier', 'connection' => 'test', 'records' => true);

	var $records = array(
		array(
			'id' => 15,
			'name' => '[MAIL] Re_ web-DPO v2.0',
			'objet' => 'Bonjour Arnaud',
			'reference' => '2020000011'
		),
		array(
			'id' => 27,
			'name' => '20150512_Angouleme_GFC_2020-06-23_06:09:56',
			'objet' => 'null',
			'reference' => '2020000026'
		)
	);
}

?>
