<?php
	/**
	 * Code source de la classe CollectiviteFixture.
	 *
	 * PHP 5.3
	 *
	 * @package app.Test.Fixture
	 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
	 */

	/**
	 * Classe CollectiviteFixture.
	 *
	 * @package app.Test.Fixture
	 */
	class CollectiviteFixture
	{
		/**
		 * On importe la définition de la table, pas les enregistrements.
		 *
		 * @var array
		 */
		public $import = array(
			'model' => 'Collectivite',
			'records' => false
		);

		/**
		 * Définition des enregistrements.
		 *
		 * @var array
		 */
        public $records = array(
            array(
                'id' => 1,
                'name' => '11111',
                'conn' => 'test',
                'ldap_active' => false,
                'ldap_type' => 'OpenLDAP',
                'ldap_use_ad' => false/*,
                'ldap_host' => 'localhost',
                'ldap_port' => '389',
                'ldap_uniqueid' => 'UID',
                'ldap_base_dn' => null,
                'ldap_account_suffix' => null,
                'ldap_dn' => 'dn',
                'logo' => null,
                'siren' => '',
                'adresse' => '',
                'codeinsee' => '',
                'codepostal' => '',
                'ville' => '',
                'complementadresse' => '',
                'telephone' => '',
                'active' => true,
                'login_suffix' => 'a',
                'scan_active' => false,
                'scan_access_type' => null,
                'scan_local_path' => null,
                'scan_remote_type' => null,
                'scan_remote_url' => null,
                'scan_remote_port' => null,
                'scan_remote_path' => null,
                'scan_remote_user' => null,
                'scan_remote_pass' => null,
                'scan_imap_encryption_type' => null,
                'scan_imap_login_encryption_type' => null,
                'scan_purge_delay' => 7,
                'scan_imap_url' => null,
                'scan_imap_port' => null,
                'scan_imap_path' => null,
                'scan_imap_user' => null,
                'scan_imap_pass' => null,
                'scan_desktop_id' => null,
                'scanemail_id' => null,
                'created' => 'now()',
                'modified' => 'now()'*/
            )
        );
	}
?>
