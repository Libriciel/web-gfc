<?php
/**
 * Code source de la classe RechercheContactFixture.
 *
 * PHP 5.3
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

/**
 * Classe RechercheContactFixture.
 *
 * @package app.Test.Fixture
 */
class RechercheContactFixture extends CakeTestFixture {

	/**
	 * On importe la définition de la table, pas les enregistrements.
	 *
	 * @var array
	 */
	public $import = array('model' => 'RechercheContact', 'records' => false, 'connection' => 'test');
}
?>
