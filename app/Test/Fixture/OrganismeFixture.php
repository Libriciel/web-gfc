<?php
/**
 * Code source de la classe CollectiviteFixture.
 *
 * PHP 5.3
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

/**
 * Classe OrganismeFixture.
 *
 * @package app.Test.Fixture
 */
class OrganismeFixture extends CakeTestFixture {

//	public $useDbConfig = 'test';
	/**
	 * On importe la définition de la table, pas les enregistrements.
	 *
	 * @var array
	 */
	public $import = array('model' => 'Organisme', 'records' => true, 'connection' => 'test');

	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false)
	);

	/**
	 * Définition des enregistrements.
	 *
	 * @var array
	 */
	public $records = array(
		array(
			'name' => 'Sans Organisme'
		)
	);
}
?>
