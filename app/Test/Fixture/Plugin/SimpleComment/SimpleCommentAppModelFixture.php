<?php

/**
 * SimpleCommentAppModel test class.
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun., 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://ssampaio@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Model/Behavior/PasswordBehavior.php $
 * $Id: PasswordBehavior.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 *
 * @package			SimpleComment
 * @package       SimpleComment.Test.Fixture
 */
class SimpleCommentAppModelFixture extends CakeTestFixture {


}
