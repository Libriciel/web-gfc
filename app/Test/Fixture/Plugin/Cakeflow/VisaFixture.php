<?php
/**
 * Code source de la classe VisaFixture.
 *
 * PHP 7.4
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

class VisaFixture extends CakeTestFixture
{
	/**
	 * On importe la définition de la table et les enregistrements.
	 *
	 * @var array
	 */
	public $import = ['model' => 'Cakeflow.Visa', 'connection' => 'test',  'records' => false];
}
