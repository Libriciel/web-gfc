<?php
/**
 * Code source de la classe CompositionFixture.
 *
 * PHP 7.4
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

class CompositionFixture extends CakeTestFixture
{
	/**
	 * On importe la définition de la table et les enregistrements.
	 *
	 * @var array
	 */
	public $import = ['model' => 'Cakeflow.Composition', 'connection' => 'test',  'records' => false];
}
