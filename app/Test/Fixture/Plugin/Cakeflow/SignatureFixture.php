<?php
/**
 * Code source de la classe SignatureFixture.
 *
 * PHP 7.4
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

class SignatureFixture extends CakeTestFixture
{
	/**
	 * On importe la définition de la table et les enregistrements.
	 *
	 * @var array
	 */
	public $import = ['model' => 'Cakeflow.Signature', 'connection' => 'test',  'records' => false];
}
