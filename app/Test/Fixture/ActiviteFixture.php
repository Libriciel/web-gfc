<?php
	/**
	 * Code source de la classe ActiviteFixture.
	 *
	 * PHP 5.3
	 *
	 * @package app.Test.Fixture
	 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
	 */

	/**
	 * Classe ActiviteFixture.
	 *
	 * @package app.Test.Fixture
	 */
	class ActiviteFixture
	{
		/**
		 * On importe la définition de la table, pas les enregistrements.
		 *
		 * @var array
		 */
		public $import = array(
			'model' => 'Activite',
			'records' => false
		);

		/**
		 * Définition des enregistrements.
		 *
		 * @var array
		 */
        public $records = array(
            array(
                'id' => 1,
                'name' => 'Social',
                'description' => 'Association à but non lucratif',
                'active' => true
            ),
            array(
                'id' => 2,
                'name' => 'Bâtiment',
                'description' => 'Entreprise de BTP',
                'active' => true
            )
        );
	}
?>
