<?php
/**
 * Code source de la classe CollectiviteFixture.
 *
 * PHP 5.3
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

/**
 * Classe OrganismeOperationFixture.
 *
 * @package app.Test.Fixture
 */
class OrganismeOperationFixture extends CakeTestFixture {

	/**
	 * On importe la définition de la table, pas les enregistrements.
	 *
	 * @var array
	 */
	public $import = array('model' => 'OrganismeOperation', 'records' => false, 'connection' => 'test');
}
?>
