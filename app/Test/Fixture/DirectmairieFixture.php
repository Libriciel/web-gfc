<?php
class DirectmairieFixture extends CakeTestFixture {
	public $import = array('model' => 'Courrier', 'records' => false, 'connection' => 'test');

	// Optional.
	// Set this property to load fixtures to a different test datasource
	public $useDbConfig = 'test';
//	public $fields = array(
//		'id' => array('type' => 'integer', 'key' => 'primary'),
//		'reference' => array(
//			'type' => 'string',
//			'length' => 10,
//			'null' => false
//		),
//		'name' => array(
//			'type' => 'text'
//		),
//		'created' => 'datetime',
//		'modified' => 'datetime'
//	);

	public $records = array(
		array(
			'name' => 'Flux direct-mairie',
			'reference' => '2099000001',
			'courrier_id' => 1,
			'created' => '2020-03-18 10:39:23',
			'modified' => '2020-03-18 10:41:31'
		),
		array(
			'name' => 'Flux Direct-Mairie Bis',
			'reference' => '2099000002',
			'courrier_id' => 2,
			'created' => '2020-03-18 10:41:23',
			'modified' => '2020-03-18 10:43:31'
		)
	);
}
