<?php
class SmsFixture extends CakeTestFixture {
	public $import = array('model' => 'Sms', 'records' => true, 'connection' => 'test');

	// Optional.
	// Set this property to load fixtures to a different test datasource
	public $useDbConfig = 'test';
	public $fields = array(
		'id' => array('type' => 'integer', 'key' => 'primary'),
		'numero' => array(
			'type' => 'string',
			'length' => 12,
			'null' => false
		),
		'message' => array(
			'type' => 'text',
			'length' => 160
		),
		'courrier_id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime'
	);

	public $records = array(
		array(
			'id' => 1,
			'numero' => '0666666666',
			'message' => 'Premier SMS',
			'courrier_id' => 1,
			'created' => '2020-03-18 10:39:23',
			'modified' => '2020-03-18 10:41:31'
		),
		array(
			'id' => 2,
			'numero' => '0777777777',
			'message' => 'Deuxième SMS',
			'courrier_id' => 1,
			'created' => '2020-03-18 10:41:23',
			'modified' => '2020-03-18 10:43:31'
		)
	);
}
