<?php
/**
 * Code source de la classe CollectiviteFixture.
 *
 * PHP 5.3
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

/**
 * Classe BancontenuFixture.
 *
 * @package app.Test.Fixture
 */
class BancontenuFixture extends CakeTestFixture {

	public $useDbConfig = 'test';
	/**
	 * On importe la définition de la table, pas les enregistrements.
	 *
	 * @var array
	 */
	public $import = array('model' => 'Bancontenu', 'records' => false, 'connection' => 'test');


	/**
	 * Définition des enregistrements.
	 *
	 * @var array
	 */
//	public $records = array(
//		array(
//			'id' => 1,
//			'name' => 'Flux numéro 1',
//			'reference' => '2099000001'
//		),
//		array(
//			'id' => 2,
//			'name' => 'Flux numéro 2',
//			'reference' => '2099000002'
//		),
//		array(
//			'id' => 3,
//			'name' => 'Flux numéro 3',
//			'reference' => '2099000003'
//		)
//	);
}
?>
