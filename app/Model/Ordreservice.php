<?php

/**
 * Ordreservice model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Ordreservice extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Ordreservice';

    /**
     *
     * @var type
     */
    public $useTable = 'ordresservices';

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        'Marche' => array(
            'className' => 'Marche',
            'foreignKey' => 'marche_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'courrier_id',
            'type' => 'LEFT OUTER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Organisme' => array(
            'className' => 'Organisme',
            'foreignKey' => 'organisme_id',
            'type' => 'LEFT OUTER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );
//    public $validate = array(
////        'name' => array(
////            array(
////                'rule' => array('notBlank'),
////                'allowEmpty' => false,
////            )
////        ),
//        'numero' => array(
//            array(
//                'rule' => array('notBlank'),
//                'allowEmpty' => false,
//            )
//        )
//    );

     public function search( $params ) {
        $conditions = array();
        // seuls les flux sortants sont à prende en compte => direction = 0
		$joins = array(
//			$this->join( 'Courrier', array('type' => 'LEFT OUTER') ),
			$this->join( 'Courrier', array( 'conditions' => array('Courrier.direction' => 0 ),  'type' => 'LEFT OUTER') ),
			$this->Courrier->join( 'Organisme', array('type' => 'LEFT OUTER') ),
			$this->Courrier->join( 'Contact', array('type' => 'LEFT OUTER') ),
            $this->join('Marche', array('type' => 'INNER') ),
            $this->Marche->join('Operation', array('type' => 'INNER') )
		);
		$contain = false;
		$fields = array_merge(
			$this->fields(),
            $this->Courrier->fields(),
            $this->Courrier->Organisme->fields(),
            $this->Courrier->Contact->fields(),
            $this->Marche->fields(),
            $this->Marche->Operation->fields()
		);
        $group = array('Operation.name', 'Ordreservice.id', 'Courrier.id', 'Marche.id', 'Operation.id');

        $Metadonnee = ClassRegistry::init('Metadonnee');
        $metaDatas = $Metadonnee->find('first', array( 'conditions' => array('Metadonnee.id' => Configure::read('MetadonnneMontant.id')), 'recursive' => -1));
//debug($metaDatas );
        if(!empty($metaDatas['Metadonnee']['id'])) {
            $joins[] = $this->Courrier->join(
                'CourrierMetadonnee',
                array(
                    'type' => 'LEFT OUTER',
                    'conditions' => array(
                        'CourrierMetadonnee.metadonnee_id' => $metaDatas['Metadonnee']['id']
                    )
                )
            );
            $fields = array_merge( $fields, $this->Courrier->CourrierMetadonnee->fields() );


            $joins[] = $this->Courrier->CourrierMetadonnee->join(
                'Metadonnee',
                array(
                    'type' => 'LEFT OUTER'
                )
            );
            $fields = array_merge( $fields, $this->Courrier->CourrierMetadonnee->Metadonnee->fields() );
            $group = array_merge(
                    $group,
                    array('CourrierMetadonnee.id', 'Metadonnee.id')
            );
        }


        if (isset($params['Recherche']['cdatefilter']) && !empty($params['Recherche']['cdatefilter'])) {
            if (isset($params['Recherche']['cdatefilterfin']) && !empty($params['Recherche']['cdatefilterfin'])) {
                $conditions[] = array('AND' => array(array('Ordreservice.created >=' => $params['Recherche']['cdatefilter']), array('Ordreservice.created <=' => $params['Recherche']['cdatefilterfin'])));
            } else {
                $conditions[] = array('Ordreservice.created' => $params['Recherche']['cdatefilter']);
            }
        }

        if (isset($params['Recherche']['cnumerorar']) && !empty($params['Recherche']['cnumerorar'])) {
//            $joins[] = $this->Courrier->join(
//                'CourrierMetadonnee',
//                array(
//                    'type' => 'LEFT OUTER',
//                    'conditions' => array(
//                        'CourrierMetadonnee.id' => $params['Recherche']['cnumerorar']
//                    )
//                )
//            );
            $conditions[] = array("CourrierMetadonnee.id IN ( {$params['Recherche']['cnumerorar']} )");
        }
        if( isset($params['Recherche']['cnumeroop']) && !empty($params['Recherche']['cnumeroop']) ) {
            $conditions[] = array("Marche.operation_id IN ( {$params['Recherche']['cnumeroop']} )");
        }

        $query = array(
            'fields' => $fields,
            'recursive' => -1,
            'joins' => $joins,
            'conditions' => $conditions,
            'contain' => $contain,
            'order' => array('Operation.name ASC')
        );
// debug($this->sq( $query ) );
// die();
		return $query;

    }


    /**
	 *
	 * @param type $numero de l'OS
	 * @param type $marcheID ID du marché
	 * @return type
	 */
	public function getExistance($numero, $marcheId) {
		$return = array();
		if (!empty($numero)) {
			$qd = array(
				'recursive' => -1,
				'fields' => array(
					'Ordreservice.numero',
					'Ordreservice.marche_id',
					'Marche.name'
				),
				'contain' => false,
				'joins' => array(
					$this->join($this->Marche->alias)
				),
				'conditions' => array(
                    'Ordreservice.numero' => $numero,
                    'Ordreservice.marche_id' => $marcheId
                ),
				'order' => array(
					'Ordreservice.numero'
				)
			);
			$return = $this->find('all', $qd);
		}
		return $return;
	}

}

?>
