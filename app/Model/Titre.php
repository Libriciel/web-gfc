<?php

App::uses('AppModel', 'Model');

/**
 * Titre model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Titre extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'Titre';
    public $useTable = 'titres';

    /**
     * Validation rules
     *
     * @access public
     */
    public $validate = array(
        'name' => array(
            array(
                'rule' => 'notBlank',
            ),
        ),
    );

    /**
     *
     */
    public $hasMany = array(
        'Contact' => array(
            'className' => 'Contact',
            'foreignKey' => 'titre_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}

?>

