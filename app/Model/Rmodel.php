<?php

App::uses('AppModel', 'Model');

/**
 * Rmodel model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Rmodel extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';

	/**
	 *
	 * @var type
	 */
	public $actsAs = array('Linkeddocs');

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'size' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'soustype_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Soustype' => array(
			'className' => 'Soustype',
			'foreignKey' => 'soustype_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


    public $convertibleMimeTypes = array(
        "application/vnd.oasis.opendocument.text",
        "application/vnd.oasis.opendocument.spreadsheet",
        "image/bmp",
        "image/tiff",
        "text/plain",
        "application/msword",
        "application/x-octetstream",
        "application/msexcel",
        "application/xls",
        "application/vnd.ms-office",
        "application/vnd.ms-excel",
        "application/rtf",
        "application/binary",
        "OCTET-STREAM"
    );

	/**
	 *
	 * @param type $id
	 * @return type
	 * @throws NotFoundException
	 */
	public function generatePreview($id = null, $path = null) {
		if (!empty($id)) {
            $this->id = $id;
        }

        if (empty($this->id)) {
            throw new NotFoundException();
        }

        $pdfContent = '';
        $fileContent = '';
        $this->recursive = -1;
        $rmodel = $this->read();

        $return = __d('document', 'Document.preview.error');
        if (!empty($path)) {
			//Conversion en pdf via Gedooo
			require_once 'XML/RPC2/Client.php';

            $content = file_get_contents($path);
            $options = array(
                'uglyStructHack' => true
            );
            $url = 'http://' . Configure::read('CLOUDOOO_HOST') . ':' . Configure::read('CLOUDOOO_PORT');
            $cli = XML_RPC2_Client::create($url, $options);

            $result = $cli->convertFile(base64_encode($content), 'odt', 'pdf', false, true);
            $pdfContent = base64_decode($result);
		} else {
            //Conversion en pdf via Gedooo
            if (in_array($rmodel['Rmodel']['mime'], $this->convertibleMimeTypes)) {
                require_once 'XML/RPC2/Client.php';
                $content = $rmodel['Rmodel']['content'];
                $options = array(
                    'uglyStructHack' => true
                );
                $url = 'http://' . Configure::read('CLOUDOOO_HOST') . ':' . Configure::read('CLOUDOOO_PORT');
                $cli = XML_RPC2_Client::create($url, $options);

                $result = $cli->convertFile(base64_encode($content), 'odt', 'pdf', false, true);
                $pdfContent = base64_decode($result);



            } else if ($rmodel['Rmodel']['mime'] == 'application/pdf' || $rmodel['Rmodel']['mime'] == 'PDF') {

                $pdfContent = $this->field('Rmodel.content', array('Rmodel.id' => $id));
            } else if ($rmodel['Rmodel']['mime'] == 'image/png' || $rmodel['Rmodel']['mime'] == 'image/jpeg') {
                // Si c'est une image, on retourne le contenu directement
                $return = $this->field('Rmodel.content', array('Rmodel.id' => $id));
            } else {
                require_once 'XML/RPC2/Client.php';

                $content = $rmodel['Rmodel']['content'];
                $options = array(
                    'uglyStructHack' => true
                );
                $url = 'http://' . Configure::read('CLOUDOOO_HOST') . ':' . Configure::read('CLOUDOOO_PORT');
                $cli = XML_RPC2_Client::create($url, $options);
                $result = $cli->convertFile(base64_encode($content), 'odt', 'pdf', false, true);
                $pdfContent = base64_decode($result);

            }
        }
        //Conversion en flash
        if (!empty($pdfContent)) {
            $return = $pdfContent;
        }


		return $return;
	}

}
