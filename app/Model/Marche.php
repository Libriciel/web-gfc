<?php

/**
 * Marche model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Marche extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Marche';

    /**
     *
     * @var type
     */
    public $useTable = 'marches';


    /**
     * hasMany
     *
     * @access public
     */

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        'Operation' => array(
            'className' => 'Operation',
            'foreignKey' => 'operation_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Contractant' => array(
            'className' => 'Contractant',
            'foreignKey' => 'contractant_id',
            'type' => 'LEFT',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Uniterif' => array(
            'className' => 'Uniterif',
            'foreignKey' => 'uniterif_id',
            'type' => 'LEFT',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );

    /**
     *  hasMany
     *
     *  @access public
     */
    public $hasMany = array(
        'Ordreservice' => array(
            'className' => 'Ordreservice',
            'foreignKey' => 'marche_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        /* 'Pliconsultatif' => array(
          'className' => 'Pliconsultatif',
          'foreignKey' => 'marche_id',
          'dependent' => false,
          'conditions' => null,
          'fields' => null,
          'order' => null,
          'limit' => null,
          'offset' => null,
          'exclusive' => null,
          'finderQuery' => null,
          'counterQuery' => null
          ), */
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'marche_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        )
    );
    public $validate = array(
        'name' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'numero' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'operation_id' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'contractant_id' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'datenotification' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'titulaire' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        )
    );

    /**
     * Fonction permettant de rechercher les marchés
     *
     * @param type $criteres
     * @return array()
     */
    public function search($criteres) {

//debug($criteres);
        $conditions = array();
        if (isset($criteres['SearchMarche']['name']) && !empty($criteres['SearchMarche']['name'])) {
            $conditions[] = array('Marche.id' => $criteres['SearchMarche']['name']);
        }

        if (isset($criteres['SearchMarche']['numero']) && !empty($criteres['SearchMarche']['numero'])) {
            $conditions[] = 'Marche.numero ILIKE \'' . $this->wildcard('%' . $criteres['SearchMarche']['numero'] . '%') . '\'';
        }

        if (isset($criteres['SearchMarche']['operation_id']) && !empty($criteres['SearchMarche']['operation_id'])) {
            $conditions[] = array('Marche.operation_id' => $criteres['SearchMarche']['operation_id']);
        }
        if (isset($criteres['SearchMarche']['contractant_id']) && !empty($criteres['SearchMarche']['contractant_id'])) {
            $conditions[] = array('Marche.contractant_id' => $criteres['SearchMarche']['contractant_id']);
        }
        if (isset($criteres['SearchMarche']['uniterif_id']) && !empty($criteres['SearchMarche']['uniterif_id'])) {
            $conditions[] = array('Marche.uniterif_id' => $criteres['SearchMarche']['uniterif_id']);
        }
        if (isset($criteres['SearchMarche']['datenotificationdebut']) && !empty($criteres['SearchMarche']['datenotificationdebut'])) {
            if (isset($criteres['SearchMarche']['datenotificationfin']) && !empty($criteres['SearchMarche']['datenotificationfin'])) {
                $conditions[] = array('AND' => array(array('Marche.datenotification >=' => $criteres['SearchMarche']['datenotificationdebut']), array('Marche.datenotification <=' => $criteres['SearchMarche']['datenotificationfin'])));
            }
        }

        if (isset($criteres['SearchMarche']['active'])) {
            $active = @$criteres['SearchMarche']['active'];
            $conditions[] = array(
                'Marche.active' => $active
            );
        } else {
            $conditions[] = array(
                'Marche.active' => true
            );
        }

        // Partie pour les OPs
        if (isset($criteres['SearchMarche']['operationactive'])) {
            $operationactive = @$criteres['SearchMarche']['operationactive'];
            $conditions[] = array(
                'Operation.active' => $operationactive
            );
        } else {
            $conditions[] = array(
                'Operation.active' => true
            );
        }

        $query = array(
            'contain' => array(
                'Ordreservice',
                'Operation',
                'Contractant',
                'Uniterif'
            ),
            'limit' => 2000,
            'order' => 'Marche.name ASC',
            'conditions' => array(
                $conditions
            )
        );

//debug($query);
        return $query;
    }

}

?>
