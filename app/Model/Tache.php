<?php

App::uses('AppModel', 'Model');

/**
 * Tache model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Tache extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'Tache';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'courrier_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			),
		),
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Courrier' => array(
			'className' => 'Courrier',
			'foreignKey' => 'courrier_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasAndBelongsToMany = array(
		'Desktop' => array(
			'className' => 'Desktop',
			'joinTable' => 'desktops_taches',
			'foreignKey' => 'tache_id',
			'associationForeignKey' => 'desktop_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'DesktopTache'
		),
		'Recherche' => array(
			'className' => 'Recherche',
			'joinTable' => 'recherches_taches',
			'foreignKey' => 'tache_id',
			'associationForeignKey' => 'recherche_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheTache'
		)
	);

	/**
	 *
	 * @param type $desktop_id
	 * @param array $conditions
	 * @return type
	 * @throws BadMethodCallException
	 */
	public function getTaches($desktop_id, $conditions = array()) {
		if (!is_array($conditions)) {
			throw new BadMethodCallException();
		}

		$conditions['DesktopTache.desktop_id'] = $desktop_id;

		$findcond = array(
			'contain' => false,
			'fields' => array_merge($this->fields(), $this->Desktop->fields(), $this->Desktop->User->fields()),
			'joins' => array(
				$this->join($this->DesktopTache->alias),
				$this->DesktopTache->join($this->DesktopTache->Desktop->alias),
				$this->DesktopTache->Desktop->join($this->DesktopTache->Desktop->User->alias)
			),
            'recursive' => -1,
			'order' => array('Tache.created DESC'),
			'conditions' => $conditions
		);
		return $this->find('all', $findcond);
	}

	/**
	 *
	 * @param type $desktop_id
	 * @return type
	 */
	public function getTachesATraiter($desktop_id) {
		$taches_tmp = $this->getTaches($desktop_id, array('Tache.statut' => 0));
		$taches = array();
		foreach ($taches_tmp as $tache) {
			$countDesktops = $this->DesktopTache->find('count', array('conditions' => array('DesktopTache.tache_id' => $tache['Tache']['id']), 'recursive' => -1));
			$tache['multi'] = $countDesktops > 1 ? true : false;
			$taches[] = $tache;
		}
		return $taches;
	}

	/**
	 *
	 * @param type $desktop_id
	 * @return type
	 */
	public function getTachesEnCoursPerso($desktop_id) {
		return $this->getTaches($desktop_id, array('Tache.statut' => 1, 'Tache.act_desktop_id' => $desktop_id));
	}

}

?>
