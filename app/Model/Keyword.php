<?php

/**
 * Keyword model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 *
 * based on As@lae keywordlists / keywords (thesaurus)
 * @link https://adullact.net/projects/asalae/
 */
class Keyword extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Keyword';

	/**
	 *
	 * @var type
	 */
	public $actsAs = array('Tree');

	/**
	 *
	 * @var type
	 */
	public $displayField = 'libelle';

	/**
	 *
	 * @var type
	 */
	public $displayFields = array(
		'fields' => array(
			'libelle',
			'code'
		),
		'format' => '%s (%s)',
		'order' => 'libelle'
	);

	/**
	 *
	 * @var type
	 */
	public $belongsTo = array(
		'SurKeyword' => array(
			'className' => 'Keyword',
			'foreignKey' => 'parent_id'
		),
		'CreatedUser' => array(
			'className' => 'User',
			'foreignKey' => 'created_user_id'
		),
		'ModifiedUser' => array(
			'className' => 'User',
			'foreignKey' => 'modified_user_id'
		),
		'Keywordlist' => array(
			'className' => 'Keywordlist',
			'foreignKey' => 'keywordlist_id'
		),
	);

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'SousKeyword' => array(
			'className' => 'Keyword',
			'foreignKey' => 'parent_id')
	);

	/**
	 *
	 * @var type
	 */
	public $validate = array(
		'code' => array(
			'rule' => 'notBlank',
			'message' => "Vous devez saisir un code."
		),
		'libelle' => array(
			'rule' => 'notBlank',
			'message' => "Vous devez saisir un libellé."
		)
	);

	/**
	 * Retourne le libellé correspondant à l'état 'active'
	 *
	 * @param type $active
	 * @return type
	 */
	public function libelleActif($active) {
		return $active ? __('Oui', true) : __('Non', true);
	}

	/**
	 * Retourne le libellé correspondant à l'état 'acteur_seda'
	 *
	 * @param type $acteurSeda
	 * @return type
	 */
	public function libelleActeurSeda($acteurSeda) {
		return $acteurSeda ? __('Oui', true) : __('Non', true);
	}

	/**
	 * Détermine si une instance peut être supprimée tout en respectant l'intégrité référentielle
	 *
	 * @param type $id
	 * @return boolean
	 */
	public function isDeletable($id) {
		// Existence de l'instance en base
		if (!$this->find('count', array('recursive' => -1, 'conditions' => array('id' => $id))))
			return false;

		// Existence des dépendances hierarchiques
		if ($this->find('count', array('recursive' => -1, 'conditions' => array('parent_id' => $id))))
			return false;

		return true;
	}

	/**
	 * Retourne la liste des id des enfants du service $id lui inclus
	 *
	 * @param type $id
	 * @return type
	 */
	public function mesEnfantsEtMoiId($id) {
		$ret = null;
		$moi = $this->find('first', array(
			'conditions' => array('id' => $id),
			'recursive' => -1,
			'fields' => array('lft', 'rght')));
		$enfantsEtMoi = $this->find('all', array(
			'conditions' => array(
				'lft >=' => $moi[$this->name]['lft'],
				'rght <=' => $moi[$this->name]['rght']),
			'recursive' => -1,
			'fields' => array('id')));
		foreach ($enfantsEtMoi as $ele)
			$ret[] = $ele[$this->name]['id'];

		return $ret;
	}

	/**
	 *
	 * @param type $keywordlist_id
	 * @param type $version
	 * @return boolean
	 */
	public function tagVersion($keywordlist_id, $version) {
		$newVersion = $version + 1;
		$keywords = $this->find('all', array(
			'recursive' => -1,
			'conditions' => array('Keyword.keywordlist_id' => $keywordlist_id,
				'Keyword.version' => 0
				)));
		foreach ($keywords as $keyword) {
			$this->id = $keyword['Keyword']['id'];
			$this->saveField('version', $newVersion);
		}
		return true;
	}

	/**
	 *
	 * @param type $keywordlist_id
	 * @param type $lastVersion
	 * @return boolean
	 */
	public function createWorkVersion($keywordlist_id, $lastVersion) {
		$keywords = $this->find('all', array(
			'recursive' => -1,
			'conditions' => array('Keyword.keywordlist_id' => $keywordlist_id,
				'Keyword.version' => $lastVersion
				)));
		foreach ($keywords as $keyword) {
			$new = $keyword;
			$new['Keyword']['id'] = '';
			$new['Keyword']['version'] = 0;
			$this->save($new);
		}
		return true;
	}

	/**
	 *
	 * @param type $keywordlist_id
	 * @return type
	 */
	public function suppWorkVersion($keywordlist_id) {
		return $this->deleteAll(array('Keyword.keywordlist_id' => $keywordlist_id,
					'Keyword.version' => 0));
	}

	/**
	 *
	 * @param type $keywordId
	 * @return type
	 */
	public function getInfo($keywordId) {
		$keyword = $this->find('first', array(
			'fields' => array('code', 'libelle'),
			'recursive' => -1,
			'conditions' => array('id' => $keywordId)));

		return $keyword['Keyword'];
	}

}

?>
