<?php

App::uses('AppModel', 'Model');

/**
 * Compteur model class
 * Gestion des compteurs paramétrables.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 *
 * based on webdelib compteurs / sequences
 * @link https://adullact.net/projects/webdelib/
 */
class Compteur extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Compteur';

	/**
	 *
	 * @var type
	 */
	public $displayField = 'nom';

	/**
	 *
	 * @var type
	 */
	public $validate = array(
		'nom' => array(
			array(
				'rule' => 'notBlank',
				'message' => 'Entrer un nom pour le compteur'
			)/* ,
		  array(
		  'rule' => 'isUnique',
		  'message' => 'Entrer un autre nom, celui-ci est déjà utilisé.'
		  ) */
		),
		'def_compteur' => array(
			array(
				'rule' => 'notBlank',
				'message' => 'Entrer la définition du compteur'
			)
		),
		'sequence_id' => array(
			array(
				'rule' => 'notBlank',
				'message' => 'Selectionner une séquence'
			)
		)
	);
	public $belongsTo = 'Sequence';
//    public $hasMany = 'Typeseance';
	public $hasOne = array('Soustype',
		'CollectiviteAffaire' => array(
			'className' => 'Collectivite',
			'foreignKey' => 'compteur_affaire_id'
		),
		'CollectiviteDossier' => array(
			'className' => 'Collectivite',
			'foreignKey' => 'compteur_dossier_id'
		)
	);
	public $cacheQueries = false;

	/**
	 * Retourne la valeur suivante du compteur,
	 * enregistre la nouvelle valeur de la séquence et du critère de réinitialisation en base
	 *
	 * @param int $id Numéro de l'id du compteur
	 * @retourne string Valeur suivante du compteur
	 * @access public
	 */
	public function genereCompteur($id = null, $simulation = false) {
		/* initialisations */
		if (!$id) {
			$id = $this->id;
		}

		/* initialisation du tableau de recherche et de remplacement pour la date */
		$timestamp = time();
		$remplaceD = array("#AAAA#" => date("Y", $timestamp)
			, "#AA#" => date("y", $timestamp)
			, "#M#" => date("n", $timestamp)
			, "#MM#" => date("m", $timestamp)
			, "#J#" => date("j", $timestamp)
			, "#JJ#" => date("d", $timestamp)
		);

		/* lecture du compteur en base */
		$this->recursive = 1;
//    $cptEnBase = $this->read(null, $id);
		$cptEnBase = $this->find('first', array('conditions' => array($this->alias . '.id' => $id), 'contain' => array($this->Sequence->alias)));

		/* génération du critère de réinitialisation courant */
		$val_reinitCourant = str_replace(array_keys($remplaceD), array_values($remplaceD), $cptEnBase[$this->alias]['def_reinit']);

		/* traitement de la séquence */
		if ($val_reinitCourant != $cptEnBase[$this->alias]['val_reinit']) {
			$cptEnBase['Sequence']['num_sequence'] = 1;
			$cptEnBase[$this->alias]['val_reinit'] = $val_reinitCourant;
		} else {
			$cptEnBase['Sequence']['num_sequence']++;
		}

		/* initialisation du tableau de recherche et de remplacement pour la séquence */
		$strnseqS = sprintf("%'_10d", $cptEnBase['Sequence']['num_sequence']);
		$strnseqZ = sprintf("%010d", $cptEnBase['Sequence']['num_sequence']);

		$remplaceS = array("#s#" => $cptEnBase['Sequence']['num_sequence']
			, "#S#" => substr($strnseqS, -1, 1)
			, "#SS#" => substr($strnseqS, -2, 2)
			, "#SSS#" => substr($strnseqS, -3, 3)
			, "#SSSS#" => substr($strnseqS, -4, 4)
			, "#SSSSS#" => substr($strnseqS, -5, 5)
			, "#SSSSSS#" => substr($strnseqS, -6, 6)
			, "#SSSSSSS#" => substr($strnseqS, -7, 7)
			, "#SSSSSSSS#" => substr($strnseqS, -8, 8)
			, "#SSSSSSSSS#" => substr($strnseqS, -9, 9)
			, "#SSSSSSSSSS#" => $strnseqS
			, "#0#" => substr($strnseqZ, -1, 1)
			, "#00#" => substr($strnseqZ, -2, 2)
			, "#000#" => substr($strnseqZ, -3, 3)
			, "#0000#" => substr($strnseqZ, -4, 4)
			, "#00000#" => substr($strnseqZ, -5, 5)
			, "#000000#" => substr($strnseqZ, -6, 6)
			, "#0000000#" => substr($strnseqZ, -7, 7)
			, "#00000000#" => substr($strnseqZ, -8, 8)
			, "#000000000#" => substr($strnseqZ, -9, 9)
			, "#0000000000#" => $strnseqZ
		);

		/* génération de la valeur du compteur */
		$valCompteurD = str_replace(array_keys($remplaceD), array_values($remplaceD), $cptEnBase[$this->alias]['def_compteur']);
		$valCompteur = str_replace(array_keys($remplaceS), array_values($remplaceS), $valCompteurD);

		/* Sauvegarde du compteur et de la séquence */
		if (!$simulation) {
			$this->save($cptEnBase);
			$this->Sequence->save($cptEnBase);
		}

		/* retourne la valeur du compteur générée */
		return $valCompteur;
	}

}

?>
