<?php

/**
 * Keywordlist model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 *
 * based on As@lae keywordlists / keywords (thesaurus)
 * @link https://adullact.net/projects/asalae/
 */
class Keywordlist extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Keywordlist';

	/**
	 *
	 * @var type
	 */
	public $displayField = 'nom';

	/**
	 *
	 * @var type
	 */
	public $displayFields = array(
		'fields' => array(
			'nom',
			'description'
		),
		'format' => '%s - %s'
	);

	/**
	 *
	 * @var type
	 */
	public $belongsTo = array(
		'CreatedUser' => array(
			'className' => 'User',
			'foreignKey' => 'created_user_id'
		),
		'ModifiedUser' => array(
			'className' => 'User',
			'foreignKey' => 'modified_user_id'
		),
		'Keywordtype' => array(
			'className' => 'Keywordtype',
			'foreignKey' => 'keywordtype_id'
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Keyword'
	);

	/**
	 *
	 * @var type
	 */
	public $validate = array(
		'nom' => array(
			'rule' => 'notBlank',
			'message' => "Vous devez saisir un nom."
		)
	);

	/**
	 * Retourne le libellé correspondant à l'état 'active'
	 *
	 * @param type $active
	 * @return type
	 */
	public function libelleActif($active) {
		return $active ? __('Oui', true) : __('Non', true);
	}

	/**
	 * Détermine si une instance peut être supprimée tout en respectant l'intégrité référentielle
	 *
	 * @param type $id
	 * @return boolean
	 */
	public function isDeletable($id) {
		// Existence de l'instance en base
		if (!$this->find('count', array('recursive' => -1, 'conditions' => array('id' => $id))))
			return false;

		// Existence de mots clés liés
		if ($this->Keyword->find('count', array('recursive' => -1, 'conditions' => array('keywordlist_id' => $id))))
			return false;

		return true;
	}

	/**
	 *
	 * @param type $keywordlistId
	 * @return type
	 */
	public function getLastVersion($keywordlistId) {
		$version = $this->find('first', array(
			'recursive' => -1,
			'fields' => array('version'),
			'conditions' => array('Keywordlist.id' => $keywordlistId)
				));

		return $version['Keywordlist']['version'];
	}

	/**
	 *
	 * @param type $keywordlistId
	 * @return string
	 */
	public function getInfo($keywordlistId) {
		$keyWordList = $this->find('first', array(
			'recursive' => -1,
			'conditions' => array('Keywordlist.id' => $keywordlistId)));

		$ret['attributes'] = array(
			'schemeID' => $keyWordList['Keywordlist']['scheme_id'],
			'schemeName' => $keyWordList['Keywordlist']['scheme_name'],
			'schemeAgencyName' => $keyWordList['Keywordlist']['scheme_agency_name'],
			'schemeVersionID' => $keyWordList['Keywordlist']['scheme_version_id'],
			'schemeDataURI' => $keyWordList['Keywordlist']['scheme_data_uri'],
			'schemeURI' => $keyWordList['Keywordlist']['scheme_uri']
		);

		if (isset($keyWordList['Keywordlist']['keywordtype_id']))
			$ret['KeywordTypeCode'] = $this->Keywordtype->field('code', array('id' => $keyWordList['Keywordlist']['keywordtype_id']));
		else
			$ret['KeywordTypeCode'] = '';

		return $ret;
	}

}

?>
