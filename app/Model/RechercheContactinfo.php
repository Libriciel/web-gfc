<?php

App::uses('AppModel', 'Model');

/**
 * RechercheVcard model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class RechercheContactinfo extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'RechercheContactinfo';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'recherches_contactinfos';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'recherche_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'contactinfo_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		)
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Recherche' => array(
			'className' => 'Recherche',
			'foreignKey' => 'recherche_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Contactinfo' => array(
			'className' => 'Contactinfo',
			'foreignKey' => 'contactinfo_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

}

?>
