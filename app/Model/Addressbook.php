<?php

App::uses('Abaddressbook', 'Addressbook.Model');

/**
 * Addressbook model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Addressbook extends Abaddressbook {

    /**
     *
     * @var type
     */
    public $name = 'Addressbook';

    /**
     *
     * @var type
     */
    public $useTable = 'addressbooks';

    /**
     *
     * @var type
     */
//  public $belongsTo = array(
//      'Collectivite' => array(
//          'className' => 'Collectivite',
//          'foreignKey' => 'foreign_key',
//          'type' => 'INNER',
//          'conditions' => null,
//          'fields' => null,
//          'order' => null
//      )
//  );

    /**
     *
     * @var type
     */
    public $hasAndBelongsToMany = array(
        'Recherche' => array(
            'className' => 'Recherche',
            'joinTable' => 'recherches_addressbooks',
            'foreignKey' => 'addressbook_id',
            'associationForeignKey' => 'recherche_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'RechercheAddressbook'
        )
    );

    /**
     *
     * @var type
     */
    public $hasMany = array(
        'Contact' => array(
            'className' => 'Contact',
            'foreignKey' => 'addressbook_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Organisme' => array(
            'className' => 'Organisme',
            'foreignKey' => 'addressbook_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     *
     * @param type $lines
     * @return string
     */
    private function _convertVcards(&$lines) {
        $cards = array();
        $card = new VCard();

        while (@$card->parse($lines)) {
            $property = $card->getProperty('N');
            if (!$property) {
                return "";
            }
            $n = $property->getComponents();
            $tmp = array();
            if (isset($n[3]))
                $tmp[] = $n[3];   // Mr.
            if (isset($n[1]))
                $tmp[] = $n[1];   // John
            if (isset($n[2]))
                $tmp[] = $n[2];   // Quinlan
            if (isset($n[4]))
                $tmp[] = $n[4];   // Esq.
            $ret = array();
            if (isset($n[0]))
                $ret[] = $n[0];
            $tmp = join(" ", $tmp);
            if ($tmp)
                $ret[] = $tmp;
            $key = join(", ", $ret);
            $cards[$key] = $card;

            // MDH: Create new VCard to prevent overwriting previous one (PHP5)
            $card = new VCard();
        }
        ksort($cards);
        return $cards;
    }

    /**
     *
     * @param type $card
     * @return type
     */
    private function _checkVcard(&$card) {
        $checks = array(
            'fn' => false,
            'n' => false,
            'version' => false
        );

        if (isset($card->_map['FN']) && isset($card->_map['FN'][0]) && isset($card->_map['FN'][0]->value)) {
            $checks['fn'] = true;
        }
        if (isset($card->_map['N']) && isset($card->_map['N'][0]) && isset($card->_map['N'][0]->value)) {
            $checks['n'] = true;
        }
        if (isset($card->_map['VERSION']) && isset($card->_map['VERSION'][0]) && isset($card->_map['VERSION'][0]->value)) {
            $checks['version'] = true;
        }

        return !in_array(false, $checks, true);
    }

    /**
     *
     * @param type $card
     * @return type
     */
    private function _processVcard($card, $addressbook_id = null, $foreign_key = null) {
        //create
        $names = explode(';', $card->_map['N'][0]->value);
        $civiliteTransfereCode = array(
            "MISTER" => '1',
            "MONSIEUR" => '1',
            "MISS" => '2',
            "MRS" => '2',
            "MS" => '2',
            "MADAME" => '2',
            "OTHER" => '3',
            "MONSIEUR ET MADAME" => '3',
            "MADAME ET MONSIEUR" => '3',
            "MESSIEURS" => '4',
            "MESDAMES" => '5'
        );

        $contactinfo = array(
            'Contactinfo' => array(
                'name' => $card->_map['FN'][0]->value,
                'civilite' => isset($names[3]) ? $civiliteTransfereCode[strtoupper($names[3])] : '',
                'nom' => isset($names[0]) ? $names[0] : '',
                'prenom' => isset($names[1]) ? $names[1] : ''
            )
        );



        //email
        if (isset($card->_map['EMAIL']) && isset($card->_map['EMAIL'][0])) {
            $contactinfo['Contactinfo']['email'] = $card->_map['EMAIL'][0]->value;
        }

        //adress
        if (isset($card->_map['ADR']) && isset($card->_map['ADR'][0])) {
            $adresses = explode(';', $card->_map['ADR'][0]->value);
            $contactinfo['Contactinfo']['compl'] = isset($adresses[0]) ? $adresses[0] : '';
            $contactinfo['Contactinfo']['compl'] .= isset($adresses[1]) ? ' ' . $adresses[1] : '';
            $contactinfo['Contactinfo']['addresse'] = isset($adresses[2]) ? $adresses[2] : '';
            $contactinfo['Contactinfo']['ville'] = isset($adresses[3]) ? $adresses[3] : '';
            $contactinfo['Contactinfo']['region'] = isset($adresses[4]) ? $adresses[4] : '';
            $contactinfo['Contactinfo']['cp'] = isset($adresses[5]) ? $adresses[5] : '';
            $contactinfo['Contactinfo']['pays'] = isset($adresses[6]) ? $adresses[6] : '';
        }

        //tel
        if (isset($card->_map['TEL']) && isset($card->_map['TEL'][0])) {
            $contactinfo['Contactinfo']['tel'] = $card->_map['TEL'][0]->value;
        }

        //role
        if (isset($card->_map['ROLE']) && isset($card->_map['ROLE'][0])) {
            $contactinfo['Contactinfo']['role'] = $card->_map['ROLE'][0]->value;
        }
        //organisation
        if (isset($card->_map['ORG']) && isset($card->_map['ORG'][0])) {
            $contactinfo['Contactinfo']['organisation'] = $card->_map['ORG'][0]->value;
        }

        return $contactinfo;
    }

    /**
     *
     * @param type $filename
     * @param type $addressbook_id
     * @param type $foreign_key
     * @return boolean
     */
    public function importContactVcards($filename = null, $addressbook_id = null, $foreign_key = null, $organismeId = null) {

        if ($addressbook_id != null) {
            $return = array();
            if ($filename != null && is_file($filename) && is_readable($filename)) {
                $cards = $this->_convertVcards(file($filename));
                foreach ($cards as $kcard => $card) {
                    if ($this->_checkVcard($card)) {
                        $contactinfo = $this->_processVcard($card, $addressbook_id, $foreign_key);
                        $contact = array(
                            'Contact' => array(
                                'name' => $contactinfo['Contactinfo']['name'],
                                'nom' => $contactinfo['Contactinfo']['nom'],
                                'prenom' => $contactinfo['Contactinfo']['prenom'],
                                'civilite' => $contactinfo['Contactinfo']['civilite'],
                                'addressbook_id' => $addressbook_id,
                                'organisme_id' => $organismeId
                            )
                        );

                        //save
                        $this->Contact->begin();
                        $this->Contact->create();
                        $savedContact = $this->Contact->save($contact);
                        $contactinfo['Contactinfo']['contact_id'] = $savedContact['Contact']['id'];

                        $this->Contact->Contactinfo->create();
                        $savedContactinfo = $this->Contact->Contactinfo->save($contactinfo);

                        $saved = !empty($savedContact) && !empty($savedContactinfo);
                        if ($saved) {
                            $return[] = true;
                            $this->Contact->commit();
                        } else {
                            $return[] = false;
                            $this->Contact->rollback();
                        }
                    }
                }
            }
            return !in_array(false, $return, true);
        } else {
            $nb = 0;
            $div = '';
            if ($filename != null && is_file($filename) && is_readable($filename)) {
                $cards = $this->_convertVcards(file($filename));

                foreach ($cards as $kcard => $card) {
                    if ($this->_checkVcard($card)) {
                        $contactinfo = $this->_processVcard($card);
                        if ($contactinfo['Contactinfo']['name'] != '' && $contactinfo['Contactinfo']['nom'] != '')
                            $nb++;
                    }
                }
            }
            if ($nb == 0) {
                $div = false;
                if (is_file($filename)) {
                    unlink($filename);
                }
            } else {
                $div = 'Votre fichier importé est prêt. Vous avez ' . $nb . ' nouveaux contacts à ajouter.';
            }
            return $div;
        }
    }

    /*
     *
     *
     *
     */

    private function _processCsv($contactinfo, $langue) {
		$civiliteTransfereCode = array(
			"MISTER" => '1',
			"MONSIEUR" => '1',
			"MISS" => '2',
			"MRS" => '2',
			"MS" => '2',
			"MADAME" => '2',
			"OTHER" => '3',
			"MONSIEUR ET MADAME" => '3',
			"MADAME ET MONSIEUR" => '3'
		);
        if( isset( $contactinfo['CIVILITE']) ) {
            $infos = array(
                'name' => $contactinfo['PRENOM']." ".$contactinfo['NOM'],
                'civilite' => $civiliteTransfereCode[strtoupper($contactinfo['CIVILITE'])],
                'nom' => $contactinfo['NOM'],
                'prenom' => $contactinfo['PRENOM'],
                'adresse' => $contactinfo['ADRESSE'],
                'numvoie' => $contactinfo['NUMVOIE'],
                'nomvoie' => $contactinfo['NOM VOIE'],
                'compl' => $contactinfo['COMPL ADRESSE'],
                'cp' => $contactinfo['CODE POSTAL'],
                'ville' => $contactinfo['VILLE'],
                'telephone' => $contactinfo['TELEPHONE']
            );
            $email = array(
                'email' => $contactinfo['MAIL']
            );
            $tel = array(
                'tel' => $contactinfo['TELEPHONE']
            );
            $adresse['home'] = array(
                'adresse' => $contactinfo['ADRESSE'],
                'numvoie' => $contactinfo['NUMVOIE'],
                'nomvoie' => $contactinfo['NOM VOIE'],
                'compl' => $contactinfo['COMPL ADRESSE'],
                'cp' => $contactinfo['CODE POSTAL'],
                'ville' => $contactinfo['VILLE'],
                'telephone' => $contactinfo['TELEPHONE']
            );
            $role = $contactinfo['FONCTION'];
            $organisation = '';
        }
        else {
            //infos
            if ($langue == 'eng') {
                $nameEng = isset($contactinfo['Display Name']) ? $contactinfo['Display Name'] : $contactinfo['First Name'] . ' ' . $contactinfo['Last Name'];
            }
            $infos = array(
                'name' => ($langue == 'eng') ? $nameEng : $contactinfo['Nom à afficher'],
                'civilite' => ($langue == 'eng') ? $civiliteTransfereCode[strtoupper($contactinfo['Civility'])] : $civiliteTransfereCode[strtoupper($contactinfo['Civilité'])],
                'nom' => ($langue == 'eng') ? $contactinfo['Last Name'] : $contactinfo['Nom de famille'],
                'prenom' => ($langue == 'eng') ? $contactinfo['First Name'] : $contactinfo['Prénom']
            );

            //email
            $email = ($langue == 'eng') ? array($contactinfo['E-mail Address'], $contactinfo['E-mail 2 Address'], $contactinfo['E-mail 3 Address']) : array($contactinfo['Adresse électronique principale'], $contactinfo['Adresse électronique secondaire']);
            $email = array_values(array_filter($email));

            //tel
            if ($langue == 'eng') {
                if (isset($contactinfo['Business Phone']) && isset($contactinfo['Primary Phone'])) {
                    $telBussiniessEn = !empty($contactinfo['Business Phone']) ? $contactinfo['Business Phone'] : $contactinfo['Primary Phone'];
                }
                if (isset($contactinfo['Business Phone']) && !isset($contactinfo['Primary Phone'])) {
                    $telBussiniessEn = $contactinfo['Business Phone'];
                }
            }
            $tel = array(
                'home' => ($langue == 'eng') ? $contactinfo['Home Phone'] : $contactinfo['Tél. personnel'],
                'bussiniess' => ($langue == 'eng') ? $telBussiniessEn : $contactinfo['Tél. professionnel'],
                'mobile' => ($langue == 'eng') ? $contactinfo['Mobile Phone'] : $contactinfo['Portable']
            );
            $tel = array_filter($tel);

            //adresse
            $adresse = array();
            $home = array();
            if ($langue == 'eng') {
                $homeComplEng = isset($contactinfo['Home Address 2']) ? $contactinfo['Home Address 2'] : $contactinfo['Home Address'];
            }
            $home = array(
                'adresse' => ($langue == 'eng') ? $contactinfo['Home Street'] : $contactinfo['Adresse privée 2'],
                'compl' => ($langue == 'eng') ? $homeComplEng : $contactinfo['Adresse privée'],
                'cp' => ($langue == 'eng') ? $contactinfo['Home Postal Code'] : $contactinfo['Code postal'],
                'ville' => ($langue == 'eng') ? $contactinfo['Home City'] : $contactinfo['Ville'],
                'region' => ($langue == 'eng') ? $contactinfo['Home State'] : $contactinfo['Pays/Région (domicile)'],
                'pays' => ($langue == 'eng') ? $contactinfo['Home Country'] : $contactinfo['Pays/État']
            );
            $home = array_filter($home);
            $bussiniess = array();
            if ($langue == 'eng') {
                $bussiniessComplEng = isset($contactinfo['Business Address 2']) ? $contactinfo['Business Address 2'] : $contactinfo['Business Address'];
            }
            $bussiniess = array(
                'adresse' => ($langue == 'eng') ? $contactinfo['Business Address'] : $contactinfo['Adresse professionnelle 2'],
                'compl' => ($langue == 'eng') ? $bussiniessComplEng : $contactinfo['Adresse professionnelle'],
                'cp' => ($langue == 'eng') ? $contactinfo['Business Postal Code'] : $contactinfo['Code postal'],
                'ville' => ($langue == 'eng') ? $contactinfo['Business City'] : $contactinfo['Ville'],
                'region' => ($langue == 'eng') ? $contactinfo['Business State'] : $contactinfo['Pays/Région (bureau)'],
                'pays' => ($langue == 'eng') ? $contactinfo['Business Country'] : $contactinfo['Pays/État']
            );
            $bussiniess = array_filter($bussiniess);
            if (count($home) != 0) {
                $adresse['home'] = $home;
            }
            if (count($bussiniess) != 0) {
                $adresse['bussiniess'] = $bussiniess;
            }

            //role
            $role = ($langue == 'eng') ? $contactinfo['Job Title'] : $contactinfo['Profession'];

            //organisation
            if ($langue == 'eng') {
                $organisationEng = isset($contactinfo['Organization']) ? $contactinfo['Organization'] : $contactinfo['Company'];
            }
            $organisation = ($langue == 'eng') ? $organisationEng : $contactinfo['Société'];
        }

        $res = array(
            'infos' => $infos,
            'email' => $email,
            'tel' => $tel,
            'adresse' => $adresse,
            'role' => $role,
            'organisation' => $organisation
        );
        return $res;
    }

    /**
     *
     * @param type $filename
     * @param type $addressbook_id
     * @param type $foreign_key
     * @return boolean
     */
    public function importContactCsv($filename = null, $addressbook_id = null, $foreign_key = null, $organisme_id = null) {
        //check content file, return message
        if ($addressbook_id == null) {
            $nb = 0;
            if ($filename != null && is_file($filename) && is_readable($filename)) {
                if (($handle = fopen($filename, 'r')) !== FALSE) {
                    $contacts = array();
                    $header = NULL;
                    while (($row = fgetcsv($handle, 2000, ',')) !== FALSE) {
                        if (!$header) {
                            $header = $row;
                        } else {
                            $tmp = array();
                            for ($i = 0; $i < count($header); $i++) {
                                $tmp[$header[$i]] = $row[$i];
                            }
                            $contacts[] = $tmp;
                        }
                    }
                    fclose($handle);
                    $langue = "";
                    if (!empty($contacts[0]['First Name'])) {
                        $langue = "eng";
                    } else if (!empty($contacts[0]['Prénom'])) {
                        $langue = "fran";
                    }

                    $nb = 0;
                    foreach ($contacts as $contact) {
                        if (count($contact) == 1) {
                            $nb = 0;
                        } else {
                            $contactinfo = $this->_processCsv($contact, $langue);
                            $nbFicherContact = max(count($contactinfo['email']), count($contactinfo['adresse']), count($contactinfo['tel']));
                            if ($nbFicherContact != 0 && !empty($contactinfo['infos']['nom'])) {
                                $nb++;
                            }
                        }
                    }
                }
            }
            if ($nb != 0) {
                $div = 'Votre fichier importé est prêt. Vous avez ' . $nb . ' nouveaux contacts à ajouter.';
            } else {
                $div = false;
                if (is_file($filename)) {
                    unlink($filename);
                }
            }
            return $div;
            //save bbd
        } else {
            $return = array();
            if ($filename != null && is_file($filename) && is_readable($filename)) {
                if (($handle = fopen($filename, 'r')) !== FALSE) {
                    $contactinfos = array();
                    $header = NULL;

                    while (($row = fgetcsv($handle, 2000, ',')) !== FALSE) {
                        if (!$header) {
                            $header = $row;
                        } else {
                            $tmp = array();
                            for ($i = 0; $i < count($header); $i++) {
                                $tmp[$header[$i]] = $row[$i];
                            }
                            $contacts[] = $tmp;
                        }
                    }
                    fclose($handle);
                    $langue = "";
                    if (!empty($contacts[0]['First Name'])) {
                        $langue = "eng";
                    } else if (!empty($contacts[0]['Prénom'])) {
                        $langue = "fran";
                    }

                    foreach ($contacts as $contact) {
                        $contactinfo = $this->_processCsv($contact, $langue);
                        $nbFicherContact = max(count($contactinfo['email']), count($contactinfo['adresse']), count($contactinfo['tel']));
                        $contactModel = array();
                        $contactModel = array(
                            'Contact' => array(
                                'name' => $contactinfo['infos']['name'],
                                'nom' => $contactinfo['infos']['nom'],
                                'prenom' => $contactinfo['infos']['prenom'],
                                'civilite' => $contactinfo['infos']['civilite'],
                                'addressbook_id' => $addressbook_id,
                                'organisme_id' => $organisme_id
                            )
                        );
                        //un seul ficher contact, realiser model contactInfo
                        if ($nbFicherContact == 1) {
                            $contactINFO = array();
                            // infos
                            $contactINFO['Contactinfo'] = $contactinfo['infos'];
                            // email
                            if (count($contactinfo['email']) != 0) {
                                foreach ($contactinfo['email'] as $key => $value)
                                    $contactINFO['Contactinfo']['email'] = $value;
                            }
                            // adresse
                            if (count($contactinfo['adresse']) != 0) {
                                foreach ($contactinfo['adresse'] as $key => $value)
                                    foreach ($value as $k => $v)
                                        $contactINFO['Contactinfo'][$k] = $v;
                            }
                            // tel
                            if (count($contactinfo['tel']) != 0) {
                                foreach ($contactinfo['tel'] as $key => $value)
                                    $contactINFO['Contactinfo']['tel'] = $value;
                            }
                            //role
                            $contactINFO['Contactinfo']['role'] = $contactinfo['role'];
                            //organisation
                            $contactINFO['Contactinfo']['organisation'] = $contactinfo['organisation'];

                            $contactINFO['Contactinfo']['organisme_id'] = $organisme_id;
                            //plusieur ficher contacts
                        } else {
                            $contactINFO = array();
                            for ($i = 0; $i < $nbFicherContact; $i++) {
                                //infos
                                $contactINFO[$i]['Contactinfo'] = $contactinfo['infos'];

                                //email
                                if (count($contactinfo['email']) == $nbFicherContact) {
                                    $contactINFO[$i]['Contactinfo']['email'] = $contactinfo['email'][$i];
                                } else {
                                    if (count($contactinfo['email']) == 0) {
                                        $contactINFO[$i]['Contactinfo']['email'] = "";
                                    } else {

                                        $email = array_values($contactinfo['email']);
                                        if ($i >= count($contactinfo['email'])) {
                                            $contactINFO[$i]['Contactinfo']['email'] = $email[0];
                                        } else {
                                            $contactINFO[$i]['Contactinfo']['email'] = $email[$i];
                                        }
                                    }
                                }

                                //adresse tel
                                if (count($contactinfo['adresse']) == $nbFicherContact) {
                                    if ($i == 0) {
                                        $contactINFO[$i]['Contactinfo']['name'] .=' Pro.';
                                        foreach ($contactinfo['adresse']['bussiniess'] as $key => $val) {
                                            $contactINFO[$i]['Contactinfo'][$key] = $contactinfo['adresse']['bussiniess'][$key];
                                        }
                                        if (isset($contactinfo['tel']['bussiniess'])) {
                                            $contactINFO[$i]['Contactinfo']['tel'] = $contactinfo['tel']['bussiniess'];
                                        }
                                    } else {
                                        $contactINFO[$i]['Contactinfo']['name'] .=' Per.';
                                        foreach ($contactinfo['adresse']['home'] as $key => $val) {
                                            $contactINFO[$i]['Contactinfo'][$key] = $contactinfo['adresse']['home'][$key];
                                        }
                                        if (isset($contactinfo['tel']['home'])) {
                                            $contactINFO[$i]['Contactinfo']['tel'] = $contactinfo['tel']['home'];
                                        }
                                    }
                                } else if (count($contactinfo['tel']) == $nbFicherContact) {
                                    if ($i == 0) {
                                        $contactINFO[$i]['Contactinfo']['name'] .=' Pro.';
                                        if (isset($contactinfo['adresse']['bussiniess'])) {
                                            foreach ($contactinfo['adresse']['bussiniess'] as $key => $val) {
                                                $contactINFO[$i]['Contactinfo'][$key] = $contactinfo['adresse']['bussiniess'][$key];
                                            }
                                        }

                                        $contactINFO[$i]['Contactinfo']['tel'] = isset($contactinfo['tel']['bussiniess']) ? $contactinfo['tel']['bussiniess'] : '';
                                    } else if ($i == 1) {
                                        $contactINFO[$i]['Contactinfo']['name'] .=' Per.';
                                        if (isset($contactinfo['adresse']['home'])) {
                                            foreach ($contactinfo['adresse']['home'] as $key => $val) {
                                                $contactINFO[$i]['Contactinfo'][$key] = $contactinfo['adresse']['home'][$key];
                                            }
                                        }
                                        $contactINFO[$i]['Contactinfo']['tel'] = isset($contactinfo['tel']['home']) ? $contactinfo['tel']['home'] : '';
                                    } else {
                                        $contactINFO[$i]['Contactinfo']['name'] .=' Mobile.';
                                        if (isset($contactinfo['adresse']['bussiniess'])) {
                                            foreach ($contactinfo['adresse']['bussiniess'] as $key => $val) {
                                                $contactINFO[$i]['Contactinfo'][$key] = $contactinfo['adresse']['bussiniess'][$key];
                                            }
                                        }
                                        $contactINFO[$i]['Contactinfo']['tel'] = isset($contactinfo['tel']['bussiniess']) ? $contactinfo['tel']['bussiniess'] : '';
                                    }
                                } else {
                                    $adress = array_values($contactinfo['adresse']);
                                    if (count($contactinfo['adresse']) > 0) {
                                        if ($i >= count($contactinfo['adresse'])) {
                                            foreach ($adress[0] as $key => $val) {
                                                $contactINFO[$i]['Contactinfo'][$key] = $adress[$i - count($contactinfo['adresse'])][$key];
                                            }
                                        } else {
                                            foreach ($adress[$i] as $key => $val) {
                                                $contactINFO[$i]['Contactinfo'][$key] = $adress[$i][$key];
                                            }
                                        }
                                    }
                                    $tel = array_values($contactinfo['tel']);
                                    if (count($contactinfo['tel']) > 0) {
                                        if ($i >= count($contactinfo['tel'])) {
                                            $contactINFO[$i]['Contactinfo']['tel'] = $tel[0];
                                        } else {
                                            $contactINFO[$i]['Contactinfo']['tel'] = $tel[$i];
                                        }
                                    }
                                }

                                //role
                                $contactINFO[$i]['Contactinfo']['role'] = $contactinfo['role'];

                                //organisation
                                $contactINFO[$i]['Contactinfo']['organisation'] = $contactinfo['organisation'];

                                $contactINFO[$i]['Contactinfo']['organisme_id'] = $organisme_id;
                            }
                        }

//debug(count($contactINFO['Contactinfo']['nom']));
//debug(count(!empty($contactINFO['Contactinfo']['nom'] )));
                        //save
                        if (!empty($contactModel['Contact']['name'])) {
                            $contactModel['Contact'] = $contactINFO['Contactinfo'];
                            $this->Contact->begin();
                            $this->Contact->create();
                            $savedContact = $this->Contact->save($contactModel);
                            if (count($contactINFO) == 1) {
//                                if(!empty( $contactINFO['Contactinfo']['nom'] )) {
                                $contactINFO['Contactinfo']['contact_id'] = $savedContact['Contact']['id'];
//                                    $this->Contact->Contactinfo->create();
//                                    $savedContactinfo = $this->Contact->Contactinfo->save($contactINFO);
                                $saved = !empty($savedContact);
                                if ($saved) {
//                                        $return[] = true;
                                    $this->Contact->commit();
                                } else {
//                                        $return[] = false;
                                    $this->Contact->rollback();
                                }
//                                }
                            } else {
                                for ($j = 0; $j < count($contactINFO); $j++) {
//                                    if(!empty( $contactINFO[$j]['Contactinfo']['nom'] )) {
                                    $contactINFO[$j]['Contactinfo']['contact_id'] = $savedContact['Contact']['id'];
//                                        $this->Contact->Contactinfo->create();
//                                        $savedContactinfo[$j] = $this->Contact->Contactinfo->save($contactINFO[$j]);
                                    $saved = !empty($savedContact);
                                    if ($saved) {
                                        $return[] = true;
                                        $this->Contact->commit();
                                    } else {
                                        $return[] = false;
                                        $this->Contact->rollback();
                                    }
//                                    }
                                }
                            }
                        }
                    }
                }
            }
            return !in_array(false, $return, true);
        }
    }

    /**
     *
     */
    public function listTypesVoie() {
        return array(
            'Addressbook' => array(
                'typevoie' => array(
                    'ABE' => 'Abbaye',
                    'ACH' => 'Ancien chemin',
                    'AGL' => 'Agglomération',
                    'AIRE' => 'Aire',
                    'ALL' => 'Allée',
                    'ANSE' => 'Anse',
                    'ARC' => 'Arcade',
                    'ART' => 'Ancienne route',
                    'AUT' => 'Autoroute',
                    'AV' => 'Avenue',
                    'BAST' => 'Bastion',
                    'BCH' => 'Bas chemin',
                    'BCLE' => 'Boucle',
                    'BD' => 'Boulevard',
                    'BEGI' => 'Béguinage',
                    'BER' => 'Berge',
                    'BOIS' => 'Bois',
                    'BRE' => 'Barriere',
                    'BRG' => 'Bourg',
                    'BSTD' => 'Bastide',
                    'BUT' => 'Butte',
                    'CALE' => 'Cale',
                    'CAMP' => 'Camp',
                    'CAR' => 'Carrefour',
                    'CARE' => 'Carriere',
                    'CARR' => 'Carre',
                    'CAU' => 'Carreau',
                    'CAV' => 'Cavée',
                    'CGNE' => 'Campagne',
                    'CHE' => 'Chemin',
                    'CHEM' => 'Cheminement',
                    'CHEZ' => 'Chez',
                    'CHI' => 'Charmille',
                    'CHL' => 'Chalet',
                    'CHP' => 'Chapelle',
                    'CHS' => 'Chaussée',
                    'CHT' => 'Château',
                    'CHV' => 'Chemin vicinal',
                    'CITE' => 'Cité',
                    'CLOI' => 'Cloître',
                    'CLOS' => 'Clos',
                    'COL' => 'Col',
                    'COLI' => 'Colline',
                    'COR' => 'Corniche',
                    'COTE' => 'Côte(au)',
                    'COTT' => 'Cottage',
                    'COUR' => 'Cour',
                    'CPG' => 'Camping',
                    'CRS' => 'Cours',
                    'CST' => 'Castel',
                    'CTR' => 'Contour',
                    'CTRE' => 'Centre',
                    'DARS' => 'Darse',
                    'DEG' => 'Degré',
                    'DIG' => 'Digue',
                    'DOM' => 'Domaine',
                    'DSC' => 'Descente',
                    'ECL' => 'Ecluse',
                    'EGL' => 'Eglise',
                    'EN' => 'Enceinte',
                    'ENC' => 'Enclos',
                    'ENV' => 'Enclave',
                    'ESC' => 'Escalier',
                    'ESP' => 'Esplanade',
                    'ESPA' => 'Espace',
                    'ETNG' => 'Etang',
                    'FG' => 'Faubourg',
                    'FON' => 'Fontaine',
                    'FORM' => 'Forum',
                    'FORT' => 'Fort',
                    'FOS' => 'Fosse',
                    'FOYR' => 'Foyer',
                    'FRM' => 'Ferme',
                    'GAL' => 'Galerie',
                    'GARE' => 'Gare',
                    'GARN' => 'Garenne',
                    'GBD' => 'Grand boulevard',
                    'GDEN' => 'Grand ensemble',
                    'GPE' => 'Groupe',
                    'GPT' => 'Groupement',
                    'GR' => 'Grand(e) rue',
                    'GRI' => 'Grille',
                    'GRIM' => 'Grimpette',
                    'HAM' => 'Hameau',
                    'HCH' => 'Haut chemin',
                    'HIP' => 'Hippodrome',
                    'HLE' => 'Halle',
                    'HLM' => 'HLM',
                    'ILE' => 'Ile',
                    'IMM' => 'Immeuble',
                    'IMP' => 'Impasse',
                    'JARD' => 'Jardin',
                    'JTE' => 'Jetée',
                    'LD' => 'Lieu dit',
                    'LEVE' => 'Levée',
                    'LOT' => 'Lotissement',
                    'MAIL' => 'Mail',
                    'MAN' => 'Manoir',
                    'MAR' => 'Marche',
                    'MAS' => 'Mas',
                    'MET' => 'Métro',
                    'MF' => 'Maison forestiere',
                    'MLN' => 'Moulin',
                    'MTE' => 'Montée',
                    'MUS' => 'Musée',
                    'NTE' => 'Nouvelle route',
                    'PAE' => 'Petite avenue',
                    'PAL' => 'Palais',
                    'PARC' => 'Parc',
                    'PAS' => 'Passage',
                    'PASS' => 'Passe',
                    'PAT' => 'Patio',
                    'PAV' => 'Pavillon',
                    'PCH' => 'Porche - petit chemin',
                    'PERI' => 'Périphérique',
                    'PIM' => 'Petite impasse',
                    'PKG' => 'Parking',
                    'PL' => 'Place',
                    'PLAG' => 'Plage',
                    'PLAN' => 'Plan',
                    'PLCI' => 'Placis',
                    'PLE' => 'Passerelle',
                    'PLN' => 'Plaine',
                    'PLT' => 'Plateau(x)',
                    'PN' => 'Passage à niveau',
                    'PNT' => 'Pointe',
                    'PONT' => 'Pont(s)',
                    'PORQ' => 'Portique',
                    'PORT' => 'Port',
                    'POT' => 'Poterne',
                    'POUR' => 'Pourtour',
                    'PRE' => 'Pré',
                    'PROM' => 'Promenade',
                    'PRQ' => 'Presqu\'île',
                    'PRT' => 'Petite route',
                    'PRV' => 'Parvis',
                    'PSTY' => 'Peristyle',
                    'PTA' => 'Petite allée',
                    'PTE' => 'Porte',
                    'PTR' => 'Petite rue',
                    'QU' => 'Quai',
                    'QUA' => 'Quartier',
                    'R' => 'Rue',
                    'RAC' => 'Raccourci',
                    'RAID' => 'Raidillon',
                    'REM' => 'Rempart',
                    'RES' => 'Résidence',
                    'RLE' => 'Ruelle',
                    'ROC' => 'Rocade',
                    'ROQT' => 'Roquet',
                    'RPE' => 'Rampe',
                    'RPT' => 'Rond point',
                    'RTD' => 'Rotonde',
                    'RTE' => 'Route',
                    'SEN' => 'Sentier',
                    'SQ' => 'Square',
                    'STA' => 'Station',
                    'STDE' => 'Stade',
                    'TOUR' => 'Tour',
                    'TPL' => 'Terre plein',
                    'TRA' => 'Traverse',
                    'TRN' => 'Terrain',
                    'TRT' => 'Tertre(s)',
                    'TSSE' => 'Terrasse(s)',
                    'VAL' => 'Val(lée)(lon)',
                    'VCHE' => 'Vieux chemin',
                    'VEN' => 'Venelle',
                    'VGE' => 'Village',
                    'VIA' => 'Via',
                    'VLA' => 'Villa',
                    'VOI' => 'Voie',
                    'VTE' => 'Vieille route',
                    'ZA' => 'Zone artisanale',
                    'ZAC' => 'Zone d\'aménagement concerte',
                    'ZAD' => 'Zone d\'aménagement différé',
                    'ZI' => 'Zone industrielle',
                    'ZONE' => 'Zone',
                    'ZUP' => 'Zone à urbaniser en priorité'
                )
            )
        );
    }

    /**
     *
     * @param type $filename
     * @param type $addressbook_id
     * @param type $foreign_key
     * @return boolean
     */
    public function importOrganismeCsv($filename = null, $addressbook_id = null, $foreign_key = null) {
        if ($addressbook_id == null) {
            $nb = 0;
            if ($filename != null && is_file($filename) && is_readable($filename)) {
                if (($handle = fopen($filename, 'r')) !== FALSE) {
                    $orgs = array();
                    $header = NULL;
                    while (($row = fgetcsv($handle, 4000, ',')) !== FALSE) {
                        if (!$header) {
                            $header = $row;
                        } else {
                            $tmp = array();
                            for ($i = 0; $i < count($header); $i++) {
                                $tmp[$header[$i]] = $row[$i];
                            }
                            $orgs[] = $tmp;
                        }
                    }
                    fclose($handle);

                    $nb = 0;
                    foreach ($orgs as $org) {

                        $langue = "fran";
                        if (count($org) == 1) {
                            $nb = 0;
                        } else {
                            $orginfo = $this->_processCsv($org, $langue);
                            if (!empty($orginfo['infos']['name'])) {
                                $nb++;
                            }
                        }
                    }
//debug($nb);
                }
            }
            if ($nb != 0) {
                $div = 'Votre fichier importé est prêt. Vous avez ' . $nb . ' nouveaux organismes à ajouter.';
            } else {
                $div = false;
                if (is_file($filename)) {
                    unlink($filename);
                }
            }
            return $div;
            //save bbd
        } else {
            $return = array();
            if ($filename != null && is_file($filename) && is_readable($filename)) {
                if (($handle = fopen($filename, 'r')) !== FALSE) {
                    $orginfos = array();
                    $header = NULL;

                    while (($row = fgetcsv($handle, 4000, ',')) !== FALSE) {
                        if (!$header) {
                            $header = $row;
                        } else {
                            $tmp = array();
                            for ($i = 0; $i < count($header); $i++) {
                                $tmp[$header[$i]] = $row[$i];
                            }
                            $orgs[] = $tmp;
                        }
                    }
                    fclose($handle);
                    $langue = "fran";

                    foreach ($orgs as $org) {

                        $orginfo = $this->_processOrganismeCsv($org, $langue);
//    debug($orginfo);
//    die();
                        $orgModel = array();
                        $orgModel = array(
                            'Organisme' => array(
                                'name' => $orginfo['infos']['name'],
                                'numvoie' => $orginfo['adresse']['numvoie'],
                                'adressecomplete' => $orginfo['adresse']['adressecomplete'],
                                'compl' => $orginfo['adresse']['compl'],
                                'cp' => $orginfo['adresse']['cp'],
                                'email' => $orginfo['email']['email'],
                                'tel' => $orginfo['tel']['home'],
                                'portable' => $orginfo['tel']['mobile'],
                                'fax' => $orginfo['tel']['fax'],
                                'active' => true,
                                'ville' => $orginfo['adresse']['ville'],
                                'nomvoie' => $orginfo['adresse']['nomvoie'],
                                'pays' => $orginfo['adresse']['pays'],
                                'addressbook_id' => $addressbook_id
                            )
                        );
                        if (!empty($orginfo['infos']['nom'])) {
                            $nb++;
                        }
                        //debug(count($contactINFO['Contactinfo']['nom']));
                        //debug(count(!empty($contactINFO['Contactinfo']['nom'] )));
                        //save
                        if (!empty($orgModel['Organisme']['name'])) {
                            $this->Organisme->begin();
                            $this->Organisme->create();
                            $savedOrganisme = $this->Organisme->save($orgModel);
                            $saved = !empty($savedOrganisme);
                            if ($saved) {
                                $this->Organisme->commit();
                            } else {
                                $this->Organisme->rollback();
                            }
                        }
                    }
                }
            }
            return !in_array(false, $return, true);
        }
    }

    /*
     *
     *
     *
     */

    private function _processOrganismeCsv($orginfo) {

        $infos = array(
            'name' => $orginfo['Nom de famille']
        );

        //email
        $email = array('email' => $orginfo['Adresse électronique principale']);
//        $email = array_values(array_filter($email));
        //tel
        $tel = array(
            'home' => $orginfo['Tél. personnel'],
            'bussiniess' => $orginfo['Tél. professionnel'],
            'mobile' => $orginfo['Portable'],
            'fax' => $orginfo['Fax']
        );
        $tel = array_filter($tel);

        //adresse
        $adresse = array(
            'adresse' => $orginfo['Adresse privée 2'],
            'numvoie' => '',
            'nomvoie' => '',
            'compl' => $orginfo['Adresse privée'],
            'adressecomplete' => utf8_decode($orginfo['Adresse professionnelle']),
            'cp' => $orginfo['Code postal'],
            'ville' => $orginfo['Ville'],
            'region' => $orginfo['Pays/Région (domicile)'],
            'pays' => $orginfo['Pays/État']
        );
//        $adresse = array_filter($adresse);
//        $bussiniess = array();
//        $bussiniess = array(
//            'adresse' => $orginfo['Adresse professionnelle 2'],
//            'adressecomplete' => utf8_decode( $orginfo['Adresse professionnelle'] ),
//            'numvoie' => '',
//            'nomvoie' => '',
//            'compl' => $orginfo['Adresse privée'],
//            'cp' => $orginfo['Code postal'],
//            'ville' => $orginfo['Ville'],
//            'region' => $orginfo['Pays/Région (bureau)'],
//            'pays' => $orginfo['Pays/État']
//        );
//        $bussiniess = array_filter($bussiniess);
//        if (count($home) != 0) {
//            $adresse['home'] = $home;
//        }
//        if (count($bussiniess) != 0) {
//            $adresse['bussiniess'] = $bussiniess;
//        }
        //role
        $role = $orginfo['Profession'];

        //organisation
        $organisation = $orginfo['Société'];

        $res = array(
            'infos' => $infos,
            'email' => $email,
            'tel' => $tel,
            'adresse' => $adresse,
            'role' => $role,
            'organisation' => $organisation
        );
        return $res;
    }

}

?>
