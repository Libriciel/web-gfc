<?php

App::uses('AppModel', 'Model');

/**
 * Bannette model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Bannette extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Bannette';

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Bancontenu' => array(
			'className' => 'Bancontenu',
			'foreignKey' => 'bannette_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);

}

?>
