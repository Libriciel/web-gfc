<?php

/**
 * Intituleagent model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Intituleagent extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Intituleagent';

    /**
     *
     * @var type
     */
    public $useTable = 'intitulesagents';



    public $hasMany = array(
        'DesktopmanagerSousoperation' => array(
            'className' => 'DesktopmanagerSousoperation',
            'foreignKey' => 'intituleagent_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Desktopmanager' => array(
            'className' => 'Desktopmanager',
            'foreignKey' => 'intituleagent_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        )
    );

}

?>
