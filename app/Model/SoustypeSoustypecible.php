<?php

App::uses('AppModel', 'Model');

/**
 * FormatreponseSoustype model class.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class SoustypeSoustypecible extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'SoustypeSoustypecible';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'soustypecible_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'soustype_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Soustypecible' => array(
			'className' => 'Soustypecible',
			'foreignKey' => 'soustypecible_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Soustype' => array(
			'className' => 'Soustype',
			'foreignKey' => 'soustype_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

}

?>
