<?php

App::uses('Abcontactinfo', 'Addressbook.Model');

/**
 * Contactinfo model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Contactinfo extends Abcontactinfo {

	/**
	 *
	 * @var type
	 */
	public $name = 'Contactinfo';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'contactinfos';

	/**
	 *
	 * @var type
	 */
//	public $validate = array(
//		'' => ''
//	);

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Courrier' => array(
			'className' => 'Courrier',
			'foreignKey' => 'contactinfo_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);

	/**
	 *
	 * @var type
	 */
// 	public $hasAndBelongsToMany = array(
// 		'Recherche' => array(
// 			'className' => 'Recherche',
// 			'joinTable' => 'recherches_contactinfos',
// 			'foreignKey' => 'contactinfo_id',
// 			'associationForeignKey' => 'recherche_id',
// 			'unique' => null,
// 			'conditions' => null,
// 			'fields' => null,
// 			'order' => null,
// 			'limit' => null,
// 			'offset' => null,
// 			'finderQuery' => null,
// 			'deleteQuery' => null,
// 			'insertQuery' => null,
// 			'with' => 'RechercheContact'
// 		)
// 	);

	/**
	 *
	 * @var type
	 */
	public $belongsTo = array(
		'Contact' => array(
			'className' => 'Contact',
			'foreignKey' => 'contact_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'Ban' => array(
			'className' => 'Ban',
			'foreignKey' => 'ban_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Bancommune' => array(
			'className' => 'Bancommune',
			'foreignKey' => 'bancommune_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 *
	 * @param type $nom
	 * @param type $prenom
	 * @return type
	 */
	public function getHomonyms($nom, $prenom = "") {
		$return = array();
		if (!empty($nom)) {
			$slug = strtolower(Inflector::slug($nom . ' ' . $prenom));
			$qd = array(
				'recursive' => -1,
				'fields' => array(
					'Contactinfo.nom',
					'Contactinfo.prenom',
					'Contact.name',
					'Addressbook.name'
				),
				'contain' => false,
				'joins' => array(
					$this->join($this->Contact->alias),
					$this->Contact->join($this->Contact->Addressbook->alias)
				),
				'conditions' => array("levenshtein('" . $slug . "', Contactinfo.slug) <= " . Configure::read('levenshteinThreshold')),
				'order' => array(
					"levenshtein('" . $slug . "', Contactinfo.slug) DESC",
					'Contactinfo.nom',
					'Contactinfo.prenom'
				)
			);
			$return = $this->find('all', $qd);
		}
		return $return;
	}

	/**
	 *
	 * @param type $created
	 * @return type
	 */
	public function beforeSave($options = array()) {
		parent::beforeSave($options);
		if (!empty($this->data['Contactinfo']['nom']) && !empty($this->data['Contactinfo']['prenom'])) {
			$this->data['Contactinfo']['slug'] = strtolower(Inflector::slug($this->data['Contactinfo']['nom'] . ' ' . $this->data['Contactinfo']['prenom']));
		}
		return true;
	}

}

?>
