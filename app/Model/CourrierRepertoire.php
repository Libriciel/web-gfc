<?php

App::uses('AppModel', 'Model');

/**
 * CourrierRepertoire model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class CourrierRepertoire extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'CourrierRepertoire';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'courrier_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'repertoire_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Courrier' => array(
			'className' => 'Courrier',
			'foreignKey' => 'courrier_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Repertoire' => array(
			'className' => 'Repertoire',
			'foreignKey' => 'repertoire_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

}

?>
