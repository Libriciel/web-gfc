<?php


/**
 * Contactinfo model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Bancommune extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Bancommune';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'banscommunes';

	/**
	 *
	 * @var type
	 */
   public $validate = array(
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		)
    );

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Banadresse' => array(
			'className' => 'Banadresse',
			'foreignKey' => 'bancommune_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);


	/**
	 *
	 * @var type
	 */
	public $belongsTo = array(
		'Ban' => array(
			'className' => 'Ban',
			'foreignKey' => 'ban_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);



	/**
	 *
	 * @param type $created
	 * @return type
	 */
//	public function beforeSave($options = array()) {
//		parent::beforeSave($options);
//		if (!empty($this->data['Bancommune']['name']) && !empty($this->data['Contactinfo']['prenom'])) {
//			$this->data['Contactinfo']['slug'] = strtolower(Inflector::slug($this->data['Contactinfo']['nom'] . ' ' . $this->data['Contactinfo']['prenom']));
//		}
//		return true;
//	}

}

?>
