<?php

/**
 * Intituleagent model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Intituleagentbyoperation extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Intituleagentbyoperation';

    /**
     *
     * @var type
     */
    public $useTable = 'intituleagentbyoperations';
    public $belongsTo = array(
        'Desktopmanager' => array(
            'className' => 'Desktopmanager',
            'foreignKey' => 'desktopmanager_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Intituleagent' => array(
            'className' => 'Intituleagent',
            'foreignKey' => 'intituleagent_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Operation' => array(
            'className' => 'Operation',
            'foreignKey' => 'operation_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );

    /**
     *
     * @param type $params
     */
    public function search($params) {
        if (empty($params)) {
            $conditions = array();
        }
//debug($params);
        if (!empty($params)) {
            if (isset($params['Intituleagentbyoperation']['coperation_id']) && !empty($params['Intituleagentbyoperation']['coperation_id'])) {
                $conditions[] = array('Intituleagentbyoperation.operation_id' => $params['Intituleagentbyoperation']['coperation_id']);
            } else {
                $conditions = array();
            }
        }

        $querydata = array(
            'conditions' => array(
                $conditions
            ),
            'order' => array('Operation.name ASC'),
            'contain' => array(
                'Intituleagent',
                'Operation'
            ),
            'limit' => 2000
        );

        return $querydata;
    }

}

?>
