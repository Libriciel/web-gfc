<?php

App::uses('AppModel', 'Model');

/**
 * Configuration model class
 *
 * web-GFC : Gestion de Flux Citoyens
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://www.libriciel.fr
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Configuration extends AppModel {


	/**
	 *
	 * @var type
	 */
	public $name = 'Configuration';

	/**
	 *
	 * @param type $id
	 * @param type $table
	 * @param type $ds
	 */
	function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->setDataSource('default');
	}
}

?>
