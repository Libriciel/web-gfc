<?php

App::uses('AppModel', 'Model');

/**
 * Type model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Desktopmanager extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'Desktopmanager';

    /**
     * Validation rules
     *
     * @access public
     */
    public $validate = array(
        'name' => array(
            //TODO: remettre les regles d unicite
//          'rule' => 'isUnique',
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
        )
    );
    public $belongsTo = array(
        'Intituleagent' => array(
            'className' => 'Intituleagent',
            'foreignKey' => 'intituleagent_id',
            'type' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
		'Profil' => array(
            'className' => 'Profil',
            'foreignKey' => 'profil_id',
            'type' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );
    public $hasMany = array(
        'Composition' => array(
            'className' => 'Cakeflow.Composition',
            'foreignKey' => 'trigger_id'
        ),
        'Scanemail' => array(
            'className' => 'Scanemail',
            'foreignKey' => 'desktopmanager_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Bancontenu' => array(
            'className' => 'Bancontenu',
            'foreignKey' => 'desktopmanager_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     *
     * @var type
     */
    public $hasAndBelongsToMany = array(
        'Desktop' => array(
            'className' => 'Desktop',
            'joinTable' => 'desktops_desktopsmanagers',
            'foreignKey' => 'desktopmanager_id',
            'associationForeignKey' => 'desktop_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'DesktopDesktopmanager'
        ),
        /*
        'Sousoperation' => array(
            'className' => 'Sousoperation',
            'joinTable' => 'desktopsmanagers_sousoperations',
            'foreignKey' => 'desktopmanager_id',
            'associationForeignKey' => 'sousoperation_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'DesktopmanagerSousoperation'
        ),*/
        'Intituleagent' => array(
            'className' => 'Intituleagent',
            'joinTable' => 'intituleagentbyoperations',
            'foreignKey' => 'desktopmanager_id',
            'associationForeignKey' => 'intituleagent_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'Intituleagentbyoperation'
        )
    );

    /**
     *
     * @param type $id
     * @return type
     */
    public function getDesktops($id = null) {
        $return = array();
        if (empty($id)) {
            $desktopmanager = $this->find('list');
            $return = $desktopmanager;
        } else {
            $desktopmanager = $this->find(
                    'first', array(
                'conditions' => array(
                    'Desktopmanager.id' => $id
                ),
                'contain' => array(
                    'Desktop'
                ),
                'order' => array('Desktopmanager.id DESC')
                    )
            );

            if (!empty($desktopmanager['Desktop'])) {
                foreach ($desktopmanager['Desktop'] as $desktop) {
                    if (!in_array($desktop['id'], $return)) {
                        $return[] = $desktop['id'];
                    }
                }
            }
        }
        return $return;
    }

    /**
     * Retourne la liste des bureaux desktopsmanagers sélectionnables pour aiguillage et initialisation
     * @param type $id
     * @return type
     */
    public function getAllDesktopsByProfils($profils = array(), $iparapheur = false ) {
        $return = array();

		$newconditions = array('Desktopmanager.active' => true);
		if( !Configure::read('Display.AutocreatedDesktopmanager')) {
			$newconditions = array(
				'OR' => [
					'Desktopmanager.isautocreated' => false,
					'Desktopmanager.id IN' => ['-1', '-3']
				],
				'Desktopmanager.active' => true
			);
		}

        if( $iparapheur ) {
            $conditions = array(
                'Desktop.profil_id' => $profils,
                'Desktop.active' => 1
            );
        }
        else {
            $conditions = array(
                'Desktop.profil_id' => $profils,
                'Desktop.id NOT IN' => [-1],
                'Desktop.active' => 1
            );
        }
        if (!empty($profils)) {
            $desktopsmanagers = $this->find(
				'all',
				array(
					'conditions' => $newconditions,
					'contain' => array(
						'Desktop' => array(
							'conditions' => $conditions
						)
					),
					'order' => array('Desktopmanager.name ASC')
				)
            );
        } else {
            $desktopsmanagers = $this->find(
				'all',
				array(
					'conditions' => $newconditions,
					'contain' => array(
						'Desktop' => array(
							'conditions' => array(
								'Desktop.id <>' =>  '-1',
								'Desktop.active' => 1
							)
						)
					),
					'order' => array('Desktopmanager.name ASC')
				)
            );
        }

        foreach ($desktopsmanagers as $i => $desktopmanager) {
            if (!empty($desktopmanager['Desktop'])) {
                $return[$desktopmanager['Desktopmanager']['id']] = $desktopmanager['Desktopmanager']['name'];
            }
        }
        return $return;
    }

    /**
     * Retourne la liste des bureaux desktopsmanagers sélectionnables pour aiguillage et initialisation
     * @param type $id
     * @return type
     */
    public function getDesktopmanagerByService($profils = array()) {
        $return = array();

        if (!empty($profils)) {
            $desktopsmanagers = $this->find(
                    'all', array(
                'contain' => array(
                    'Desktop' => array(
                        'conditions' => array(
                            'Desktop.profil_id' => $profils,
                            'Desktop.id <>' => -1,
                            'Desktop.active' => 1
                        ),
                        'Service' => array(
                            'order' => array('Service.name ASC')
                        )
                    )
                ),
                'order' => array('Desktopmanager.name ASC')
                    )
            );
        } else {
            $desktopsmanagers = $this->find(
                    'all', array(
                'contain' => array(
                    'Desktop' => array(
                        'conditions' => array(
                            'Desktop.id <>' => -1,
                            'Desktop.active' => 1
                        ),
                        'Service'
                    )
                ),
                'order' => array('Desktopmanager.name ASC')
                    )
            );
        }
        foreach ($desktopsmanagers as $i => $desktopmanager) {
            if (!empty($desktopmanager['Desktop'])) {
                foreach ($desktopmanager['Desktop'] as $d => $desktops) {
                    if (!empty($desktops['Service'])) {
                        foreach ($desktops['Service'] as $s => $service) {
                            $optgroupUserName = $service['name'];
                            $return[$optgroupUserName][$desktopmanager['Desktopmanager']['id']] = $desktopmanager['Desktopmanager']['name'];
                        }
                    }
                }
            }
        }
        return $return;
    }

    /**
     *
     * @param type $params
     */
    public function search($params) {
        if (empty($params)) {
            $conditions = array(
                'Desktopmanager.active' => true,
                'Desktopmanager.isautocreated' => false
            );
        }

        if (!empty($params)) {
            if (isset($params['Desktopmanager']['name']) && !empty($params['Desktopmanager']['name'])) {
                $conditions[] = 'Desktopmanager.name ILIKE \'' . $this->wildcard('%' . $params['Desktopmanager']['name'] . '%') . '\'';
            }
            if (isset($params['Desktopmanager']['active'])) {
                $conditions[] = array('Desktopmanager.active' => $params['Desktopmanager']['active']);
            }
            if (isset($params['Desktopmanager']['isdispatch']) && in_array( $params['Desktopmanager']['isdispatch'], array('0', '1') )) {
                $conditions[] = array('Desktopmanager.isdispatch' => $params['Desktopmanager']['isdispatch']);
            }
			if (isset($params['Desktopmanager']['isautocreated']) ) {
                $conditions[] = array('Desktopmanager.isautocreated' => $params['Desktopmanager']['isautocreated']);
            }
        }

        $querydata = array(
            'conditions' => array(
                $conditions
            ),
            'order' => 'Desktopmanager.name',
            'contain' => array(
                'Desktop'
            ),
//            'limit' => 20
        );

        return $querydata;
    }


    /**
     * Fonction peremttant de retourner les informations des bureaux "Chefs de"
     * @param type $desktopmanagerId
     */
    public function getParentId( $desktopmanagerId) {

        $desktopmanager = $this->find(
            'first',
            array(
                'conditions' => array(
                    'Desktopmanager.id' => $desktopmanagerId
                ),
                'contain' => false,
                'order' => array('Desktopmanager.id DESC')
            )
        );
        $parentID = null;
        if( !empty($desktopmanager['Desktopmanager']['parent_id']) ) {
            $parentID = $desktopmanager['Desktopmanager']['parent_id'];
        }
        return $parentID;
    }
}

?>
