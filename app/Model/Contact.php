<?php

App::uses('Abcontact', 'Addressbook.Model');

/**
 * Contact model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Contact extends Abcontact {

    /**
     *
     * @var type
     */
    public $name = 'Contact';

    /**
     *
     * @var type
     */
    public $useTable = 'contacts';

    /**
     *
     * @var type
     */
    public $hasMany = array(
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'contact_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
    );

    /**
     *
     * @var type
     */
    public $hasAndBelongsToMany = array(
        'Recherche' => array(
            'className' => 'Recherche',
            'joinTable' => 'recherches_contacts',
            'foreignKey' => 'contact_id',
            'associationForeignKey' => 'recherche_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'RechercheContact'
        ),
        'Operation' => array(
            'className' => 'Operation',
            'joinTable' => 'contacts_operations',
            'foreignKey' => 'contact_id',
            'associationForeignKey' => 'operation_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'ContactOperation'
        ),
        'Event' => array(
            'className' => 'Event',
            'joinTable' => 'contacts_events',
            'foreignKey' => 'contact_id',
            'associationForeignKey' => 'event_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'ContactEvent'
        )
    );

    /**
     *
     * @var type
     */
    public $belongsTo = array(
        'Addressbook' => array(
            'className' => 'Addressbook',
            'foreignKey' => 'addressbook_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Organisme' => array(
            'className' => 'Organisme',
            'foreignKey' => 'organisme_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Ban' => array(
            'className' => 'Ban',
            'foreignKey' => 'ban_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Bancommune' => array(
            'className' => 'Bancommune',
            'foreignKey' => 'bancommune_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Fonction' => array(
            'className' => 'Fonction',
            'foreignKey' => 'fonction_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Titre' => array(
            'className' => 'Titre',
            'foreignKey' => 'titre_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     *
     * @param type $nom
     * @param type $prenom
     * @return type
     */
    public function getHomonyms($name) {
        $return = array();
        if (!empty($name)) {
            $slug = strtolower(Inflector::slug($name));
            $qd = array(
                'recursive' => -1,
                'fields' => array(
                    'Contact.name',
                    'Contact.adresse',
                    'Contact.cp',
                    'Contact.ville',
                    'Addressbook.name',
                    'Organisme.name'
                ),
                'contain' => false,
                'joins' => array(
                    $this->join($this->Addressbook->alias),
                    $this->join($this->Organisme->alias)
                ),
                'conditions' => array(
                    'OR' => array(
                        'Contact.name ILIKE' => '%' . $slug . '%',
                        "levenshtein('" . $slug . "', Contact.slug) < " . Configure::read('levenshteinThreshold')
                    )
                ),
                'order' => array(
                    "levenshtein('" . $slug . "', Contact.slug) DESC",
                    'Contact.name'
                )
            );
            $return = $this->find('all', $qd);
        }
        return $return;
    }

    /**
     *
     * @param type $created
     * @return type
     */
    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        if (!empty($this->data['Contact']['name'])) {
            $this->data['Contact']['slug'] = strtolower(Inflector::slug($this->data['Contact']['name']));
        }
        return true;
    }

    /**
     * Fonction permettant de rechercher les utilisateurs selon certains critères
     *
     * @param type $criteres
     * @return array()
     */
    public function search($criteres, $addressbookId, $orgId = null) {

        $conditions = array();
        /// Conditions de base
        if (!empty($addressbookId)) {
            $conditions[] = array(
                'Contact.addressbook_id' => $addressbookId
            );
        }

        if (!empty($orgId)) {
            $conditions[] = array(
                'Contact.organisme_id' => $orgId
            );
        }

        // Critères sur l'utilisateur
        foreach (array('name', 'nom', 'prenom') as $critereContact) {
            if (isset($criteres['SearchContact'][$critereContact]) && !empty($criteres['SearchContact'][$critereContact]) ) {
                $conditions[] = 'Contact.' . $critereContact . ' ILIKE \'' . $this->wildcard('%' . $criteres['SearchContact'][$critereContact] . '%') . '\'';
            }
            else if(isset($criteres['Contact'][$critereContact]) && !empty($criteres['Contact'][$critereContact] ) )  {
                $conditions[] = 'Contact.' . $critereContact . ' ILIKE \'' . $this->wildcard('%' . $criteres['Contact'][$critereContact] . '%') . '\'';
            }
        }

        if(isset($criteres['Contact']['organisme_id']) && !empty($criteres['Contact']['organisme_id']) )  {
            $conditions[] = array(
                'Contact.organisme_id' => $criteres['Contact']['organisme_id']
            );
        }

        if (isset($criteres['SearchContact']['active']) || isset($criteres['Organisme']['Contact']['active'])) {
            $active = @$criteres['SearchContact']['active'] || @$criteres['Organisme']['Contact']['active'];
            $conditions[] = array(
                'Contact.active' => $active
            );
        } else if(isset($criteres['Contact']['active']) )  {
            $active = $criteres['Contact']['active'];
            $conditions[] = array(
                'Contact.active' => $active
            );
        }
        else {
            $conditions[] = array(
                'Contact.active' => true
            );
        }

		if (!empty($criteres['Contact']['Fonction']['id']) ) {
			if (!empty($criteres['Contact']['fonction_id']) || !empty($criteres['Organisme']['Contact']['fonction_id'])) {
				if (!empty($criteres['Contact']['fonction_id'])) {
					$fonctionId = $criteres['Contact']['fonction_id'];
				}
				if (!empty($criteres['Organisme']['Contact']['fonction_id'])) {
					$fonctionId = $criteres['Organisme']['Contact']['fonction_id'];
				}
				$conditions[] = array(
					'Contact.fonction_id' => $fonctionId
				);
			}
			if (!empty($criteres['Contact']['Fonction']['id'])) {
				$conditions[] = array(
					'Contact.fonction_id' => $criteres['Contact']['Fonction']['id']
				);
			}
			if (!empty($criteres['Contact']['Fonction']['id'])) {
				$conditions[] = array(
					'Contact.fonction_id' => $criteres['Contact']['Fonction']['id']
				);
			}

		}

        if (Configure::read('Conf.SAERP')) {

            if( !empty($criteres['Contact'])) {
                $criteres['SearchContact'] = $criteres['Contact'];
            }
            // Filtre pour les events
            if (!empty($criteres['SearchContact']['Event']['id'])) {
                if (!empty($criteres['SearchContact']['Event']) || !empty($criteres['Organisme']['Contact']['Event'])) {
                    if (!empty($criteres['SearchContact']['Event'])) {
                        $eventsIds = $criteres['SearchContact']['Event'];
                    }
                    if (!empty($criteres['Organisme']['Contact']['Event'])) {
                        $eventsIds = $criteres['Organisme']['Contact']['Event'];
                    }

                    $contactsEvent = $this->ContactEvent->find(
                            'all', array(
                        'conditions' => array(
                            'ContactEvent.event_id' => $eventsIds
                        ),
                        'contain' => false
                            )
                    );
                    $contactsEventsIds = Hash::extract($contactsEvent, '{n}.ContactEvent.contact_id');
                    $conditions[] = array(
                        'Contact.id' => $contactsEventsIds
                    );
                }
            }

            // Filtre pour les OPs
            if (!empty($criteres['SearchContact']['Operation']['id'])) {
                if (!empty($criteres['SearchContact']['Operation'])) {
                    if (!empty($criteres['SearchContact']['Operation'])) {
                        $opsIds = $criteres['SearchContact']['Operation'];
                    }
                    $contactsOperation = $this->ContactOperation->find(
                            'all', array(
                        'conditions' => array(
                            'ContactOperation.operation_id' => $opsIds
                        ),
                        'contain' => false
                            )
                    );
                    if($contactsOperation) {
                        $contactsOpsIds = Hash::extract($contactsOperation, '{n}.ContactOperation.contact_id');
                        $conditions[] = array(
                            'Contact.id' => $contactsOpsIds
                        );
                    }
                    else {
                        $conditions[] = array(
                            'Contact.id' => null
                        );
                    }
                }
            }
        }

        if (Configure::read('Conf.SAERP')) {
            $contain = array(
                'Organisme',
                'Addressbook',
                'Operation',
                'Event'
            );
        } else {
            $contain = array(
                'Organisme',
                'Addressbook',
                'Fonction',
            );
        }
$this->log($conditions);
        $query = array(
            'contain' => $contain,
            'order' => 'Contact.nom ASC',
            'conditions' => array(
                $conditions
            )
        );

        return $query;
    }

    public function getNombreContactByOrganismeId($organisme_id) {
        $qd = array(
            'conditions' => array(
                'Contact.organisme_id' => $organisme_id
            )
        );

        $return = $this->find('count', $qd);
        return $return;
    }

    /**
     * Retourne l'ensemble des mails issus des services
     */
    public function getContactMails() {
        $emails = array();
        $mails = $this->find(
                'all', array(
            'fields' => array('Contact.email'),
            'conditions' => array(
                'Contact.active' => true
            ),
            'recursive' => -1,
            'order' => 'Contact.email ASC'
                )
        );
        foreach ($mails as $i => $mail) {
            if (!empty($mail['Contact']['email'])) {
                if (strpos($mail['Contact']['email'], '@') !== false) {
                    $emails[$mail['Contact']['email']] = $mail['Contact']['email'];
                }
            }
        }
        return $emails;
    }

    /**
     *
     * @param type $contact
     * @return type
     */
    public function contactTreated( $contact = array() ) {
        $cInfos = array();
        $civiliteOptions = CakeSession::read( 'Auth.Civilite');
		$fonctionOptions = CakeSession::read( 'Auth.Fonctions');
        foreach ($contact['Contact'] as $key => $val) {
            if ($val != '' && $val != 'active' && $val != 'addressbook_id' && $key != 'organisme_id' && $key != 'organisme_name' && $key != 'carnet_name' && $key != 'ban_id' && $key != 'bancommune_id' && $key != 'adressecomplete') {
                if ($key == 'civilite') {
                    $cInfos['Civilité'] = $civiliteOptions[$val];
                } else {
                    $cInfos[$key != 'id' && $key != 'name' ? __d('contact', 'Contact.' . $key) : $key] = $val;
                }

                if ($key == 'fonction_id') {
					$cInfos['Fonction'] = $fonctionOptions[$val];
				}
            }
        }

        $cbInfos = array();
        foreach ($contact['Ban'] as $key => $val) {
            if ($val != '' && $val != 'active' && $key != 'created' && $key != 'modified') {
                $cbInfos[$key != 'id' ? __d('ban', 'Ban.' . $key) : $key] = $val;
            }
        }
        $cbcInfos = array();
        foreach ($contact['Bancommune'] as $key => $val) {
            if ($val != '' && $val != 'active' && $key != 'created' && $key != 'modified' && $key != 'ban_id') {
                $cbcInfos[$key != 'id' ? __d('bancommune', 'Bancommune.' . $key) : $key] = $val;
            }
        }


        if (!empty($contact['Contact']['titre_id'])) {
            $titres = $this->Titre->find('list', array('order' => array('Titre.name ASC'), 'recursive' => -1));
            $cInfos['Titre'] = Hash::get($titres, $contact['Contact']['titre_id']);
        }

        return array( 'Contactinfo' => ( $cInfos + $cbInfos + $cbcInfos ) );
    }

    /**
     * Fonction permettant de vider le cache des contacts/organismes avant d'être mise à jour
     *
     */
    public function MiseajourData() {
        $this->Courrier = ClassRegistry::init('Courrier');

        CakeSession::write('Auth.OrganismesOptions', null );
        CakeSession::write('Auth.ContactsOptions', null );

        $addPrivateIds = $this->Courrier->Organisme->Addressbook->find('list', array('conditions' => array('Addressbook.private' => true)) );
        $privateAccess = CakeSession::read( 'Auth.User.addressbook_private');
        if($privateAccess) {
            $conditionsAddress = array(
                'Organisme.active' => true
            );
        }
        else {
            if(!empty($addPrivateIds) ) {
                $conditionsAddress = array(
                    'Organisme.active' => true,
                    'Organisme.addressbook_id NOT IN' => array_keys( $addPrivateIds)
                );
            }
            else {
                $conditionsAddress = array(
                    'Organisme.active' => true
                );
            }
        }

        $organismes = $this->Courrier->Organisme->find('all', array(
            'conditions' => $conditionsAddress,
            'order' => array('Organisme.name ASC'),
            'contain' => array(
                'Contact' => array(
                    'conditions' => array('Contact.active' => true),
                    'order' => 'Contact.nom ASC'
                )
            ),
            'recursive' => -1
        ));
        $newOrganismes = Hash::map($organismes, "{n}", array($this, '__getOrganisme'));
        $newContacts = Hash::map($organismes, "{n}", array($this, '__getContact'));
        foreach ($newOrganismes as $key => $value) {
            if( Configure::read('Conf.SAERP') || Configure::read('Conf.CD')  ) {
                $organismOptions[$value['id']] = $value['name'].' ('.$value['ville'].') ';
            }
            else {
                $organismOptions[$value['id']] = $value['name'];
            }

            if( $value['name'] == 'Sans organisme') {
                $organismOptions[$value['id']] = $value['name'];
            }
        }

        foreach ($newContacts as $key => $value) {
            if (isset($value) && !empty($value)) {
                $contact = array();
                foreach ($value as $keyContact => $valueContact) {
                    $contact[$valueContact['id']] = $valueContact['nom'] . ' ' . $valueContact['prenom'];
                    if( Configure::read('Conf.CD')  ) {
                        $contact[$valueContact['id']] = $valueContact['nom'] . ' ' . $valueContact['prenom'] . ' ('. $valueContact['ville'] .')';
                    }
                    if( $valueContact['nom'] == '0Sans contact') {
                        $contact[$valueContact['id']] = $valueContact['name'];
                    }
                }
                $contactOptions[$value[0]['organisme_id']] = $contact;
            }
        }
        CakeSession::write( 'Auth.OrganismesOptions', $organismOptions );
        CakeSession::write( 'Auth.ContactsOptions', $contactOptions );
    }


    function __getOrganisme($array) {
        return $array['Organisme'];
    }

    function __getContact($array) {
        return $array['Contact'];
    }

}

?>
