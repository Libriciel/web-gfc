<?php


/**
 * Templatemail model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Templatemail extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Templatemail';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'templatemails';

   public $validate = array(
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		),
		'subject' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		),
		'object' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		)
    );

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Sendmail' => array(
			'className' => 'Sendmail',
			'foreignKey' => 'templatemail_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);

}

?>
