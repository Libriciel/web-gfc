<?php

/**
 * Activite model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Authentification extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Authentification';

    /**
     *
     * @var type
     */
    public $useTable = 'authentifications';

    public $validate = array(
        'host' => array(
            array(
                'rule' => 'notBlank',
                'message' => 'Ce champ ne peut être vide',
                'allowEmpty' => true
            )
        ),
        'port' => array(
            array(
                'rule' => 'notBlank',
                'message' => 'Ce champ ne peut être vide',
                'allowEmpty' => true
            )
        ),
        'contexte' => array(
            array(
                'rule' => 'notBlank',
                'message' => 'Ce champ ne peut être vide',
                'allowEmpty' => true
            )
        ),
        'autcert' => array(
            array(
                'rule' => 'notBlank',
                'message' => 'Ce champ ne peut être vide',
                'allowEmpty' => true
            )
        ),
        'logpath' => array(
            array(
                'rule' => 'notBlank',
                'message' => 'Ce champ ne peut être vide',
                'allowEmpty' => true
            )
        )
    );

}

?>
