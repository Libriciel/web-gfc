<?php

App::uses('AppModel', 'Model');

/**
 * Rmodelgabarit model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Rmodelgabarit extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';

	/**
	 *
	 * @var type
	 */
	public $actsAs = array('Linkeddocs');

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'size' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'soustype_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Soustype' => array(
			'className' => 'Soustype',
			'foreignKey' => 'soustype_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
//		'Rmodel' => array(
//			'className' => 'Rmodel',
//			'foreignKey' => 'rmodel_id',
//			'conditions' => '',
//			'fields' => '',
//			'order' => ''
//		)
	);

	/**
	 *
	 * @param type $id
	 * @return type
	 * @throws NotFoundException
	 */
	public function generatePreview($id = null) {
		if (!empty($id)) {
			$this->id = $id;
		}

		if (empty($this->id)) {
			throw new NotFoundException();
		}

		$pdfContent = '';
		$this->recursive = -1;
		$rmodelgabarit = $this->read();

		//Conversion en pdf via Gedooo
		require_once 'XML/RPC.php';
		$content = base64_encode($rmodelgabarit['Rmodelgabarit']['content']);

		$params = array(new XML_RPC_Value($content, 'string'),
			new XML_RPC_Value($rmodelgabarit['Rmodelgabarit']['ext'], 'string'),
			new XML_RPC_Value('pdf', 'string'),
			new XML_RPC_Value(false, 'boolean'),
			new XML_RPC_Value(true, 'boolean'));

		$url = Configure::read('CLOUDOOO_HOST') . ":" . Configure::read('CLOUDOOO_PORT');

		$msg = new XML_RPC_Message('convertFile', $params);
		$cli = new XML_RPC_Client('/', $url);
		$resp = $cli->send($msg);
		$pdfContent = base64_decode($resp->xv->me['string']);

		$return = __d('document', 'Document.preview.error');

		//Conversion en flash
		if (!empty($pdfContent)) {
			$date_suffix = date('Ymd_His');
			$tmp_pdf_to_convert = TMP . "pdf_to_convert_" . $date_suffix;
			$tmp_pdf_converted = TMP . "pdf_converted_" . $date_suffix;

			file_put_contents($tmp_pdf_to_convert, $pdfContent);
			$cmd = "pdf2swf -i " . $tmp_pdf_to_convert . " -o " . $tmp_pdf_converted . " -f -T 9 -t -s storeallcharacters";
			$sys_result = exec($cmd, $output, $return_var);


			if ($return_var == 0) {
				$return = file_get_contents($tmp_pdf_converted);

				$rmodelgabarit['Rmodelgabarit']['preview'] = $return;
				$this->create();
				$this->save($rmodelgabarit);
			}

			unlink($tmp_pdf_converted);
			unlink($tmp_pdf_to_convert);
		}
		return $return;
	}

}
