<?php

App::uses('AppModel', 'Model');

/**
 * Type model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Type extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'Type';

    /**
     *
     * @var type
     */
    public $actsAs = array('DataAcl.DataAcl' => array('type' => 'controlled'));

    /**
     * Validation rules
     *
     * @access public
     */
    public $validate = array(
        'name' => array(
            //TODO: remettre les regles d unicite
//          'rule' => 'isUnique',
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
        )
    );

    /**
     * hasMany
     *
     * @access public
     */
    public $hasMany = array(
        'Soustype' => array(
            'className' => 'Soustype',
            'foreignKey' => 'type_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Operation' => array(
            'className' => 'Operation',
            'foreignKey' => 'type_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        )
    );

    /**
     *
     * @var type
     */
    public $hasAndBelongsToMany = array(
        'Recherche' => array(
            'className' => 'Recherche',
            'joinTable' => 'recherches_types',
            'foreignKey' => 'type_id',
            'associationForeignKey' => 'recherche_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'RechercheType'
        )
    );

    /**
     * parentNode override
     *
     * @return null
     */
    function parentNode() {
        return null;
    }

    /**
     *
     * @param type $id
     * @param type $alias
     * @return null
     */
    public function getDaco($id = null, $alias = false) {
        if ($id != null) {
            $this->id = $id;
        }

        if (!$this->id) {
            return null;
        }

        $daco = $this->Daco->node(array('model' => 'Type', 'foreign_key' => $this->id));

        if ($alias) {
            $return = $daco[0]['Daco']['alias'];
        } else {
            $return = $daco[0]['Daco'];
        }

        return $return;
    }

    /**
     *
     * @param type $alias
     * @return type
     */
    public function getDacos($alias = false) {
        $types = $this->find('list');
        $return = array();
        foreach ($types as $ktype => $type) {
            $this->id = $ktype;
            $return[] = $this->getDaco($ktype, $alias);
        }
        return !empty($return) ? $return : null;
    }

    /**
     * Fonciton permettant de rechercher les sous-types selon certains critères
     *
     * @param type $criteres
     * @return array()
     */
    public function search($criteres) {
         /// Conditions de base
        if (empty($criteres)) {
            $conditionsType = array(
                'Type.active' => true
            );
            $conditionsSousType  = array(
                'Soustype.active' => true
            );
        }

        if (!empty($criteres)) {
            if (isset($criteres['Type']['active'])) {
                $conditionsType[] = array('Type.active' => $criteres['Type']['active']);
            }
            if (isset($criteres['Type']['name']) && !empty($criteres['Type']['name'])) {
                $conditionsType[] = 'Type.name ILIKE \'' . $this->wildcard('%' . $criteres['Type']['name'] . '%') . '\'';
            }
            if (isset($criteres['Soustype']['active'])) {
                $conditionsSousType[] = array('Soustype.active' => $criteres['Soustype']['active']);
            }
            if (isset($criteres['Soustype']['name']) && !empty($criteres['Soustype']['name'])) {
                $conditionsSousType[] = 'Soustype.name ILIKE \'' . $this->wildcard('%' . $criteres['Soustype']['name'] . '%') . '\'';
            }
        }

        $querydata = array(
            'contain' => array(
               $this->Soustype->alias => array(
                    'order' => 'Soustype.name',
                    'conditions' => $conditionsSousType
                )
            ),
            'conditions' => $conditionsType,
            'order' => 'Type.name'
        );

        return $querydata;
    }

}

?>
