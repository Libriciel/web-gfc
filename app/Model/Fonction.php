<?php


/**
 * Fonction model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Fonction extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Fonction';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'fonctions';


    public $hasMany = array(
		'Contact' => array(
			'className' => 'Contact',
			'foreignKey' => 'fonction_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
    );


    public $validate = array(
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		)
    );

}

?>
