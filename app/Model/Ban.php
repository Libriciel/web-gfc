<?php


/**
 * Referentielfantoir model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Ban extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Ban';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'bans';

   public $validate = array(
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		)
    );

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Bancommune' => array(
			'className' => 'Bancommune',
			'foreignKey' => 'ban_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);


	/**
	 *
	 * @param type $filename
	 * @param type $departement_id
	 * @param type $foreign_key
	 * @return boolean
	 */
	/*public function importBan($filename = null, $departement_id = null, $libelledir = null, $foreign_key = null) {
		$return = array();
		if ($departement_id != null) {
			$dept = substr( $departement_id, 0, -1 );
			$listeDepartements = $this->listeDepartements();
			$libelledir = Hash::get($listeDepartements, $departement_id );


//INSERT INTO bans (name, ident, nom_voie, id_fantoir, numero, rep, code_insee, code_post, alias, nom_ld, nom_afnor, libelle_acheminement, x, y, lon, lat, nom_commune, active)  VALUES
//('HERAULT', 'ADRNIVX_0000000284159749', '', 'B058', '61', '', '34010', '34150', '', 'FAMOURETTE', '', 'ANIANE', '746569.8', '6286993.9', '3.57739382214376', '43.6804485223614', 'Aniane', '1');
			if ($filename != null && is_file($filename) && is_readable($filename)) {
                $ban['Ban']['name'] = strtoupper( $libelledir );

                $this->create();
                $banAd = $this->save($ban);

                //save
                $this->Bancommune->begin();
                $this->Bancommune->create();
                $commune = array(
                    'Bancommune' => array(
                        'ban_id' => $banAd['Ban']['id'],
                        'name' => 'Aniane',
                        'active' => 1
                    )
                );

                $savedCommune = $this->Bancommune->save($commune);

                $saved = !empty($savedCommune);

                if( $saved ) {
                    $adresse = array(
                        'Banadresse' => array(
                            'bancommune_id' => $savedCommune['Bancommune']['id'],
                            'name' => 'rue du mas de verchant',
                            'ident'=> '',
                            'nom_voie' => 'Nom de voie',
                            'id_fantoir' => 'ID fantoir',
                            'numero' => 'N°',
                            'rep' => 'Répertoir',
                            'code_insee' => 'code INSEE',
                            'code_post' => 'Code postal',
                            'alias' => 'ALIAS',
                            'nom_ld' => 'Nom LD',
                            'nom_afnor' => 'Nom AFNOR',
                            'x' => 'Coordonnée X',
                            'y' => 'Coordonnée Y',
                            'lon' => 'Longitude',
                            'lat' => 'Latitude',
                            'nom_commune' => 'Nom de la commune',
                            'active' => 1
                        )
                    );
                    $this->Bancommune->Banadresse->create();
                    $savedAdresse = $this->Bancommune->Banadresse->save($adresse);

                    $savedAdd = !empty( $savedAdresse );

                    if ($savedAdd) {
                        $return[] = true;
                        $this->commit();
                    } else {
                        $return[] = false;
                        $this->rollback();
                    }
                }

			}
		}
		return !in_array(false, $return, true);
	}*/


	/**
	*
	*
	*/

	public function listeDepartements() {
		return array(
			'010' => '01 - Ain',
			'020' => '02 - Aisne',
			'030' => '03 - Allier',
			'040' => '04 - Alpes de Hautes-Provence',
			'050' => '05 - Hautes-Alpes',
			'060' => '06 - Alpes-Maritimes',
			'070' => '07 - Ardèche',
			'080' => '08 - Ardennes',
			'090' => '09 - Ariège',
			'100' => '10 - Aube',
			'110' => '11 - Aude',
			'120' => '12 - Aveyron',
			'130' => '13 - Bouches-du-Rhône',
			'140' => '14 - Calvados',
			'150' => '15 - Cantal',
			'160' => '16 - Charente',
			'170' => '17 - Charente-Maritime',
			'180' => '18 - Cher',
			'190' => '19 - Corrèze',
			'2A0' => '2A - Corse-du-Sud',
			'2B0' => '2B - Haute-Corse',
			'210' => '21 - Côte-d\'Or',
			'220' => '22 - Côtes d\'Armor',
			'230' => '23 - Creuse',
			'240' => '24 - Dordogne',
			'250' => '25 - Doubs',
			'260' => '26 - Drôme',
			'270' => '27 - Eure',
			'280' => '28 - Eure-et-Loir',
			'290' => '29 - Finistère',
			'300' => '30 - Gard',
			'310' => '31 - Haute-Garonne',
			'320' => '32 - Gers',
			'330' => '33 - Gironde',
			'340' => '34 - Hérault',
			'350' => '35 - Ille-et-Vilaine',
			'360' => '36 - Indre',
			'370' => '37 - Indre-et-Loire',
			'380' => '38 - Isère',
			'390' => '39 - Jura',
			'400' => '40 - Landes',
			'410' => '41 - Loir-et-Cher',
			'420' => '42 - Loire',
			'430' => '43 - Haute-Loire',
			'440' => '44 - Loire-Atlantique',
			'450' => '45 - Loiret',
			'460' => '46 - Lot',
			'470' => '47 - Lot-et-Garonne',
			'480' => '48 - Lozère',
			'490' => '49 - Maine-et-Loire',
			'500' => '50 - Manche',
			'510' => '51 - Marne',
			'520' => '52 - Haute-Marne',
			'530' => '53 - Mayenne',
			'540' => '54 - Meurthe-et-Moselle',
			'550' => '55 - Meuse',
			'560' => '56 - Morbihan',
			'570' => '57 - Moselle',
			'580' => '58 - Nièvre',
			'590' => '59 - Nord',
			'600' => '60 - Oise',
			'610' => '61 - Orne',
			'620' => '62 - Pas-de-Calais',
			'630' => '63 - Puy-de-Dôme',
			'640' => '64 - Pyrénées-Atlantiques',
			'650' => '65 - Hautes-Pyrénées',
			'660' => '66 - Pyrénées-Orientales',
			'670' => '67 - Bas-Rhin',
			'680' => '68 - Haut-Rhin',
			'690' => '69 - Rhône',
			'700' => '70 - Haute-Saône',
			'710' => '71 - Saône-et-Loire',
			'720' => '72 - Sarthe',
			'730' => '73 - Savoie',
			'740' => '74 - Haute-Savoie',
			'750' => '75 - Paris',
			'760' => '76 - Seine-Maritime',
			'770' => '77 - Seine-et-Marne',
			'780' => '78 - Yvelines',
			'790' => '79 - Deux-Sèvres',
			'800' => '80 - Somme',
			'810' => '81 - Tarn',
			'820' => '82 - Tarn-et-Garonne',
			'830' => '83 - Var',
			'840' => '84 - Vaucluse',
			'850' => '85 - Vendée',
			'860' => '86 - Vienne',
			'870' => '87 - Haute-Vienne',
			'880' => '88 - Vosges',
			'890' => '89 - Yonne',
			'900' => '90 - Territoire-de-Belfort',
			'910' => '91 - Essonne',
			'920' => '92 - Hauts-de-Seine',
			'930' => '93 - Seine-Saint-Denis',
			'940' => '94 - Val-de-Marne',
			'950' => '95 - Val-d\'Oise',
			'970' => '970 - Saint-Barthélémy',
			'971' => '971 - Guadeloupe',
			'972' => '972 - Martinique',
			'973' => '973 - Guyane',
			'974' => '974 - Réunion',
			'975' => '975 - Saint-Pierre-et-Miquelon',
			'976' => '976 - Mayotte'
		);
	}



    /*
     * Fonction permettant d'intégrer le fichier BAN d'un département
     *
     *
     */

    private function _processCsv($ban) {
        //infos
        $infos = array(
            'ident' => $ban['id'],
            'name' => !empty( $ban['libelle_acheminement'] ) ? $ban['libelle_acheminement'] : $ban['nom_commune'],
            'nom_voie' => $ban['nom_voie'],
            'id_fantoir' => $ban['id_fantoir'],
            'numero' => $ban['numero'],
            'rep' => $ban['rep'],
            'code_insee' => $ban['code_insee'],
            'code_post' => $ban['code_post'],
            'alias' => $ban['alias'],
            'nom_ld' => $ban['nom_ld'],
            'nom_afnor' => $ban['nom_afnor'],
            'libelle_acheminement' => $ban['libelle_acheminement'],
            'x' => $ban['x'],
            'y' => $ban['y'],
            'lon' => $ban['lon'],
            'lat' => $ban['lat'],
            'nom_commune' => $ban['nom_commune'],
            'active' => 1

        );


        $res = array(
            'infos' => $infos
        );
        return $res;
    }


    /**
     *
     * @param type $filename
     * @param type $ban_id
     * @param type $foreign_key
     * @return boolean
     */
    public function importBanCsv($filename = null, $departement_id = null,  $foreign_key = null) {
        //check content file, return message
        if ($departement_id == null) {
            if ($filename != null && is_file($filename) && is_readable($filename)) {
                if (($handle = fopen($filename, 'r')) !== FALSE) {
                    $contacts = array();
                    $header = NULL;
                    while (($row = fgetcsv($handle, 2000, ',')) !== FALSE) {
                        if (!$header) {
                            $header = $row;
                        } else {
                            $tmp = array();
                            for ($i = 0; $i < count($header); $i++) {
                                $tmp[$header[$i]] = $row[$i];
                            }
                            $bansadresses[] = $tmp;
                        }
                    }
                    fclose($handle);

                    $nb = 0;
                    foreach ($bansadresses as $banadresse) {
                        if (count($banadresse) == 1) {
                            $nb = 0;
                        } else {
                            $banadresse = $this->_processCsv($ban, $langue);
                            if (!empty($ban['infos']['id'])) {
                                $nb++;
                            }
                        }
                    }
                }
            }
            if ($nb != 0) {
                $div = 'Votre fichier importé est prêt. Vous avez ' . $nb . ' nouveaux contacts à ajouter.';
            } else {
                $div = false;
                if (is_file($filename)) {
                    unlink($filename);
                }
            }
            return $div;
            //save bbd
        } else {
            $return = array();
            if ($filename != null && is_file($filename) && is_readable($filename)) {
                if (($handle = fopen($filename, 'r')) !== FALSE) {

                    //création du BAN selon le département
                    $dept = substr( $departement_id, 0, -1 );
                    $listeDepartements = $this->listeDepartements();
                    $libelledir = Hash::get($listeDepartements, $departement_id );
                    $ban['Ban']['name'] = strtoupper( $libelledir );
                    $this->create();
                    $banAd = $this->save($ban);


                    $banscommunes = array();
                    $header = NULL;

                    while (($row = fgetcsv($handle, 1000000, ';')) !== FALSE) {
                        if (!$header) {
                            $header = $row;
                        } else {
                            $tmp = array();
                            for ($i = 0; $i < count($header); $i++) {
                                $tmp[$header[$i]] = $row[$i];
                            }
                            $banscommunes[] = $tmp;
                        }
                    }
                    fclose($handle);
//debug($banscommunes);
//die();

                    foreach ($banscommunes as $bancommune) {
                        $adresseINFO = $this->_processCsv($bancommune);

                        // Sauvegarde des communes
                        $communeINFO = array();
                        $communeINFO['Bancommune']['name'] = $adresseINFO['infos']['nom_commune'];
                        $communeINFO['Bancommune']['ban_id'] = $banAd['Ban']['id'];

                        $commune = $this->Bancommune->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Bancommune.name' => $communeINFO['Bancommune']['name']
                                ),
                                'recursive' => -1
                            )
                        );
                        if( !empty( $commune ) ) {
                            $bancommuneId = $commune['Bancommune']['id'];
                        }
                        else {
                            $this->Bancommune->create();
                            $savedCommune = $this->Bancommune->save($communeINFO);
                            $bancommuneId = $this->Bancommune->id;
                        }



                        // Sauvegarde des adresses
                        if(!empty( $bancommuneId )) {

                            //un seul ficher contact, realiser model contactInfo
                            $adresse = array();
                            // infos
                            $adresse['Banadresse'] = $adresseINFO['infos'];
//                            $adresseINFO['Banadresse']['name'] = $bancommuneId;
                            $adresse['Banadresse']['bancommune_id'] = $bancommuneId;
                            $this->Bancommune->Banadresse->create();
                            $savedAdresse = $this->Bancommune->Banadresse->save($adresse);
                            $saved = !empty($savedAdresse);
                            if ($saved) {
                                $return[] = true;
                                $this->Bancommune->Banadresse->commit();
                            } else {
                                $return[] = false;
                                $this->Bancommune->Banadresse->rollback();
                            }
                        }
                    }
                }
            }
            return !in_array(false, $return, true);
        }
    }
}

?>
