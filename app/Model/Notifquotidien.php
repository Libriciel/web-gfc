    <?php

App::uses('AppModel', 'Model');

/**
 * Notifieddesktop model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Notifquotidien extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'Notifquotidien';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'courrier_id' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)
		),
		'user_id' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)
		)
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Desktop' => array(
			'className' => 'Desktop',
			'foreignKey' => 'desktop_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Notification' => array(
			'className' => 'Notification',
			'foreignKey' => 'notification_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Courrier' => array(
			'className' => 'Courrier',
			'foreignKey' => 'courrier_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

}

?>
