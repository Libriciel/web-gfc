<?php

/**
 * Pliconsultatif model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Pliconsultatif extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Pliconsultatif';

    /**
     *
     * @var type
     */
    public $useTable = 'plisconsultatifs';

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        'Consultation' => array(
            'className' => 'Consultation',
            'foreignKey' => 'consultation_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Origineflux' => array(
            'className' => 'Origineflux',
            'foreignKey' => 'origineflux_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'courrier_id',
            'type' => 'LEFT OUTER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );
    public $validate = array(
        'name' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'numero' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'date' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'heure' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        )
    );

    public function search( $params ) {
        $conditions = array();

		$joins = array(
			$this->join( 'Courrier', array('type' => 'LEFT OUTER') ),
			$this->Courrier->join( 'Organisme', array('type' => 'LEFT OUTER') ),
			$this->Courrier->join( 'Contact', array('type' => 'LEFT OUTER') ),
            $this->join( 'Origineflux', array('type' => 'LEFT OUTER') ),
            $this->join('Consultation', array('type' => 'INNER') ),
            $this->Consultation->join('Operation', array('type' => 'INNER') )
		);
		$contain = false;
		$fields = array_merge(
			$this->fields(),
            $this->Courrier->fields(),
            $this->Courrier->Organisme->fields(),
            $this->Courrier->Contact->fields(),
            $this->Origineflux->fields(),
            $this->Consultation->fields(),
            $this->Consultation->Operation->fields()
		);
        $group = array('Operation.name', 'Pliconsultatif.id', 'Origineflux.id', 'Courrier.id', 'Consultation.id', 'Operation.id', 'Organisme.id', 'Contact.id');

        $Metadonnee = ClassRegistry::init('Metadonnee');
        $metaDatas = $Metadonnee->find('first', array( 'conditions' => array('Metadonnee.id' => Configure::read('MetadonnneMontant.id')), 'recursive' => -1));

        if( !empty($metaDatas) ) {
            $joins[] = $this->Courrier->join(
                'CourrierMetadonnee',
                array(
                    'type' => 'LEFT OUTER',
                    'conditions' => array(
                        'CourrierMetadonnee.metadonnee_id' => $metaDatas['Metadonnee']['id']
                    )
                )
            );
            $fields = array_merge( $fields, $this->Courrier->CourrierMetadonnee->fields() );

            $joins[] = $this->Courrier->CourrierMetadonnee->join(
                'Metadonnee',
                array(
                    'type' => 'LEFT OUTER'
                )
            );
            $fields = array_merge( $fields, $this->Courrier->CourrierMetadonnee->Metadonnee->fields() );
            $group = array_merge(
                    $group,
                    array('CourrierMetadonnee.id', 'Metadonnee.id')
            );
        }

        if (isset($params['Recherche']['cdatefilter']) && !empty($params['Recherche']['cdatefilter'])) {
            if (isset($params['Recherche']['cdatefilterfin']) && !empty($params['Recherche']['cdatefilterfin'])) {
                $conditions[] = array('AND' => array(array('Pliconsultatif.date >=' => $params['Recherche']['cdatefilter']), array('Pliconsultatif.date <=' => $params['Recherche']['cdatefilterfin'])));
            } else {
                $conditions[] = array('Pliconsultatif.date' => $params['Recherche']['cdatefilter']);
            }
        }

        $query = array(
            'fields' => $fields,
            'recursive' => -1,
            'joins' => $joins,
            'conditions' => $conditions,
            'contain' => $contain,
            'order' => array('Operation.name ASC'),
            'group' => $group
        );
// debug($this->sq( $query ) );
// die();
		return $query;

    }
}

?>
