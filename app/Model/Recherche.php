<?php

App::uses('AppModel', 'Model');

/**
 * Recherche model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Recherche extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Recherche';

	/**
	 *
	 * @var type
	 */
	public $validate = array();

	/**
	 *
	 * @var type
	 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);

	/**
	 *
	 */
	public $hasMany = array(
		'RechercheSelectvaluemetadonnee' => array(
			'className' => 'RechercheSelectvaluemetadonnee',
			'foreignKey' => 'recherche_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasAndBelongsToMany = array(
		'Metadonnee' => array(
			'className' => 'Metadonnee',
			'joinTable' => 'recherches_metadonnees',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'metadonnee_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheMetadonnee'
		),
		'Circuit' => array(
			'className' => 'Cakeflow.Circuit',
			'joinTable' => 'recherches_circuits',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'circuit_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheCircuit'
		),
		'Dossier' => array(
			'className' => 'Dossier',
			'joinTable' => 'recherches_dossiers',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'dossier_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheDossier'
		),
		'Affaire' => array(
			'className' => 'Affaire',
			'joinTable' => 'recherches_affaires',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'affaire_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheAffaire'
		),
		'Type' => array(
			'className' => 'Type',
			'joinTable' => 'recherches_types',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'type_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheType'
		),
		'Soustype' => array(
			'className' => 'Soustype',
			'joinTable' => 'recherches_soustypes',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'soustype_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheSoustype'
		),
		'Tache' => array(
			'className' => 'Tache',
			'joinTable' => 'recherches_taches',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'tache_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheTache'
		),
// 		'Vcard' => array(
// 			'className' => 'Vcard',
// 			'joinTable' => 'recherches_vcards',
// 			'foreignKey' => 'recherche_id',
// 			'associationForeignKey' => 'vcard_id',
// 			'unique' => null,
// 			'conditions' => null,
// 			'fields' => null,
// 			'order' => null,
// 			'limit' => null,
// 			'offset' => null,
// 			'finderQuery' => null,
// 			'deleteQuery' => null,
// 			'insertQuery' => null,
// 			'with' => 'RechercheVcard'
// 		),
		'Contact' => array(
			'className' => 'Contact',
			'joinTable' => 'recherches_contacts',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'contact_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheContact'
		),
		'Addressbook' => array(
			'className' => 'Addressbook',
			'joinTable' => 'recherches_addressbooks',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'addressbook_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheAddressbook'
		),
		'Desktop' => array(
			'className' => 'Desktop',
			'joinTable' => 'recherches_desktops',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'desktop_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheDesktop'
		),
		'Organisme' => array(
			'className' => 'Organisme',
			'joinTable' => 'recherches_organismes',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'organisme_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheOrganisme'
		),
		'Service' => array(
			'className' => 'Service',
			'joinTable' => 'recherches_services',
			'foreignKey' => 'recherche_id',
			'associationForeignKey' => 'service_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheService'
		)
	);

	/**
	 * Recherche de flux
	 */
	public function search($params, $id = null, $datasAuthorizedType = false) {
		$conditions = array();
		$Courrier = ClassRegistry::init('Courrier');


		$bureaux = Hash::extract(CakeSession::read('Auth.User.SecondaryDesktops'), '{n}.Profil.name');
		$isAdmin = CakeSession::read('Auth.User.Desktop.Profil.name') == 'Admin' || in_array('Admin', $bureaux);

		$joins = array(
			$Courrier->join('Contact', array('type' => 'LEFT OUTER')),
			$Courrier->join('Organisme', array('type' => 'LEFT OUTER')),
			$Courrier->join('Soustype', array('type' => 'LEFT OUTER')),
			$Courrier->Soustype->join('Type', array('type' => 'LEFT OUTER')),
			$Courrier->join('Traitement', array('type' => 'LEFT OUTER')),
			$Courrier->Traitement->join('Circuit', array('type' => 'LEFT OUTER')),
			$Courrier->join('Affaire', array('type' => 'LEFT OUTER')),
			$Courrier->Affaire->join('Dossier', array('type' => 'LEFT OUTER')),
			$Courrier->join('Desktop', array('type' => 'LEFT OUTER')),
			$Courrier->join('Origineflux', array('type' => 'LEFT OUTER')),
			$Courrier->join('Affairesuivie', array('type' => 'LEFT OUTER')),
			$Courrier->Affairesuivie->join('User', array('type' => 'LEFT OUTER')),
		);
		$contain = false;
		$order = array(
			'Courrier.date',
			'Courrier.datereception',
			'Courrier.name'
		);
		$fields = array_merge(
			$Courrier->fields(),
			$Courrier->Organisme->fields(),
			$Courrier->Contact->fields(),
			$Courrier->Soustype->fields(),
			$Courrier->Soustype->Type->fields(),
			$Courrier->Traitement->fields(),
			$Courrier->Traitement->Circuit->fields(),
			$Courrier->Affaire->fields(),
			$Courrier->Affaire->Dossier->fields(),
			array(
				'Desktop.name'
			),
			$Courrier->Origineflux->fields(),
			$Courrier->Affairesuivie->User->fields()
		);

		$sqBancontenu = $Courrier->Bancontenu->sqDerniereBannetteRecherche('Courrier.id');

		$joinBancontenu = false;
		//critères Recherches
		if (!empty($params['Recherche']['cname'])) {
			$conditions[] = 'NOACCENTS_UPPER(Courrier.name) LIKE \'' . $this->wildcard('%' . noaccents_upper( $params['Recherche']['cname'] ) . '%') . '\'';
		}

		// Objet du courrier
		if (!empty($params['Recherche']['cobjet'])) {
			$conditions[] = array('Courrier.objet ILIKE' => $Courrier->wildcard("*{$params['Recherche']['cobjet']}*"));
		}

		// Commentaire
		if (isset($params['Recherche']['ccomment']) && !empty($params['Recherche']['ccomment'])) {

			//'SELECT comment.target_id FROM comments WHERE comments.slug '
			$sq = $Courrier->Comment->sq(
				array(
					'fields' => array('comments.target_id'),
					'alias' => 'comments',
					'conditions' => array(
						'NOACCENTS_UPPER(comments.objet) LIKE \'' . $this->wildcard('%' . noaccents_upper( $params['Recherche']['ccomment'] ) . '%') . '\''
					),
					'contain' => false
				)
			);
			$conditions[] = array("Courrier.id IN ( {$sq} )");
		}
		//critères Recherches
		if (!empty($params['Recherche']['creference']) && !Configure::read('Recherche.FluxCopie') ) {
			foreach( $joins as $j => $join ){
				if( $join['alias'] == 'Bancontenu') {
					$joinBancontenu = true;
				}
			}

			if( !$joinBancontenu) {
				$joins[] = $Courrier->join(
					'Bancontenu', array(
						'type' => 'INNER',
						'conditions' => array(
							"Bancontenu.id IN ( {$sqBancontenu} )"
						)
					)
				);
				$fields = array_merge(
					$fields,
					$Courrier->Bancontenu->fields()
				);
				$joinBancontenu = true;
			}
			if( strpos( $params['Recherche']['creference'], ',' ) ) {
				$references = explode( ',', str_replace( ' ', '', $params['Recherche']['creference']) );

				$refconditions = [];
				foreach( $references as $reference ) {
					$courrier = ClassRegistry::init('Courrier')->find(
						'first',
						array(
							'fields' => array('Courrier.id', 'Courrier.parent_id'),
							'conditions' => array(
								'Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*")
							),
							'contain' => false,
							'recursive' => -1
						)
					);
					$courrierReponse = array();
					if (!empty($courrier)) {
						$courrierReponse = ClassRegistry::init('Courrier')->getSon($courrier['Courrier']['id']);
					}

					if (empty($courrier)) {
						$refconditions[] = array('Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*"));
					}
					if (!empty($courrier) && empty($courrier['Courrier']['parent_id']) && empty($courrierReponse)) {
						$refconditions[] = array('Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*"));
					}
					if (!empty($courrier) && !empty($courrier['Courrier']['parent_id']) || !empty($courrier) && !empty($courrierReponse)) {
						if (!empty($courrier) && !empty($courrier['Courrier']['parent_id'])) {
							$courrierOrig = ClassRegistry::init('Courrier')->find(
								'first',
								array(
									'fields' => array('Courrier.reference'),
									'conditions' => array(
										'Courrier.id' => $courrier['Courrier']['parent_id']
									),
									'contain' => false,
									'recursive' => -1
								)
							);
							$refconditions[] = array(
								'OR' => array(
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*")
									),
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$courrierOrig['Courrier']['reference']}*")
									)
								)
							);
						}
						if (!empty($courrier) && !empty($courrierReponse)) {
							$refconditions[] = array(
								'OR' => array(
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*")
									),
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$courrierReponse['Courrier']['reference']}*")
									)
								)
							);
						}
					}

				}
				$conditions[] = array('OR' => $refconditions);
			}
			else {

				$courriers = ClassRegistry::init('Courrier')->find(
					'all',
					array(
						'fields' => array('Courrier.id', 'Courrier.parent_id'),
						'conditions' => array(
							'Courrier.reference ILIKE' => $Courrier->wildcard("*{$params['Recherche']['creference']}*")
						),
						'contain' => false,
						'recursive' => -1
					)
				);
				if (!empty($courriers)) {
					$multiotherconditions = [];
					foreach ($courriers as $courrier) {

						$courrierReponse = ClassRegistry::init('Courrier')->getSon($courrier['Courrier']['id']);

						if (empty($courrier['Courrier']['parent_id']) && empty($courrierReponse)) {
							$multiotherconditions[] = array(
								'Courrier.reference ILIKE' => $Courrier->wildcard("*{$params['Recherche']['creference']}*")
							);
						}
						if (!empty($courrierReponse)) {
							$multiotherconditions[] = array(
								'OR' => array(
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$params['Recherche']['creference']}*")
									),
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$courrierReponse['Courrier']['reference']}*")
									)
								)
							);
						} else {
							$courrierOrig = ClassRegistry::init('Courrier')->find(
								'first',
								array(
									'fields' => array('Courrier.reference'),
									'conditions' => array(
										'Courrier.id' => $courrier['Courrier']['parent_id']
									),
									'contain' => false,
									'recursive' => -1
								)
							);
							$multiotherconditions[] = array(
								'OR' => array(
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$params['Recherche']['creference']}*")
									),
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$courrierOrig['Courrier']['reference']}*")
									)
								)
							);
						}
					}
					$conditions[] = array('OR' => $multiotherconditions);
				}
				if (empty($courriers)) {
					$conditions[] = array('Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*"));
				}
			}
		}

		//critères Recherches
		if (!empty($params['Recherche']['coriginefluxid'])) {
			$conditions[] = array('Courrier.origineflux_id' => $params['Recherche']['coriginefluxid']);
		}
		if (isset($params['Recherche']['cpriorite']) && !empty($params['Recherche']['cpriorite'])) {
			$conditions[] = array('Courrier.priorite' => $params['Recherche']['cpriorite']);
		}
		else if( isset($params['Recherche']['cpriorite']) && !empty($params['Recherche']['cpriorite'] ) || ( isset($params['Recherche']['cpriorite']) && $params['Recherche']['cpriorite'] === '0' ) ) {
			$conditions[] = array('Courrier.priorite' => $params['Recherche']['cpriorite']);
		}

		//Critères sur les dates d'entrée en collectivité
		if (isset($params['Recherche']['cdate']) && !empty($params['Recherche']['cdate'])) {
			if (isset($params['Recherche']['cdatefin']) && !empty($params['Recherche']['cdatefin'])) {
				$conditions[] = array(
					'AND' => array(
						array(
							'Courrier.date >=' => $params['Recherche']['cdate']
						),
						array(
							'Courrier.date <=' => $params['Recherche']['cdatefin']
						)
					)
				);
			} else {
				$conditions[] = array('Courrier.date' => $params['Recherche']['cdate']);
			}
		}

		//Critères sur les dates de réception par la collectivité
		if (isset($params['Recherche']['cdatereception']) && !empty($params['Recherche']['cdatereception'])) {
			if (isset($params['Recherche']['cdatereceptionfin']) && !empty($params['Recherche']['cdatereceptionfin'])) {
				$conditions[] = array('AND' => array(array('Courrier.datereception >=' => $params['Recherche']['cdatereception']), array('Courrier.datereception <=' => $params['Recherche']['cdatereceptionfin'])));
			} else {
				$conditions[] = array('Courrier.datereception' => $params['Recherche']['cdatereception']);
			}
		}

		//critères Affaire suviie par
		if (!empty($params['Desktop']['Desktop'])) {
			$conditions[] = array('Courrier.affairesuiviepar_id' => $params['Desktop']['Desktop']);
		}

		//critères Organisme
		if (!empty($params['Recherche']['organismename'])) {
			$conditions[] = 'NOACCENTS_UPPER(Organisme.name) LIKE \'' . $this->wildcard('%' . noaccents_upper( $params['Recherche']['organismename'] ) . '%') . '\'';
		} else if (!empty($params['Organisme']['Organisme'])) {
			$organisme_conditions = array();
			foreach ($params['Organisme']['Organisme'] as $organisme_id) {
				$organisme_conditions[] = array('Courrier.organisme_id' => $organisme_id);
			}
			$conditions[] = array('OR' => $organisme_conditions);
		} else if ($id != null && !empty($params['Organisme'])) {
			$organisme_conditions = array();
			foreach ($params['Organisme'] as $organisme) {
				$organisme_conditions[] = array('Courrier.organisme_id' => $organisme['id']);
			}
			$conditions[] = array('OR' => $organisme_conditions);
		}

		//critères Contact
		if (!empty($params['Recherche']['contactname'])) {
			$conditions[] = 'NOACCENTS_UPPER(Contact.name) ILIKE \'' . $this->wildcard('%' . str_replace("'", "'", $params['Recherche']['contactname'] ) . '%') . '\'';
		} else if (!empty($params['Contact']['Contact'])) {
			$contact_conditions = array();
			foreach ($params['Contact']['Contact'] as $contact_id) {
				$contact_conditions[] = array('Courrier.contact_id' => $contact_id);
			}
			$conditions[] = array('OR' => $contact_conditions);
		} else if ($id != null && !empty($params['Contact'])) {
			$contact_conditions = array();
			foreach ($params['Contact'] as $contact) {
				$contact_conditions[] = array('Courrier.contact_id' => $contact['id']);
			}
			$conditions[] = array('OR' => $contact_conditions);
		}

		//critères Type
		if (!empty($params['Type']['Type'])) {
			$type_conditions = array();
			foreach ($params['Type']['Type'] as $type_id) {
				$type_conditions[] = array('Soustype.type_id' => $type_id);
			}
			$conditions[] = array('OR' => $type_conditions);
		} else if ($id != null && !empty($params['Type'])) {
			$type_conditions = array();
			foreach ($params['Type'] as $type) {
				$type_conditions[] = array('Soustype.type_id' => $type['id']);
			}
			$conditions[] = array('OR' => $type_conditions);
		}
		// Si on cherche les flux en copie, on ne regarde pas les habilitations de sous-types
		else if(!empty($params['Recherche']['ccopie']) &&  $params['Recherche']['ccopie'] == 'Oui') {
			$type_conditions = [];
		}
		else {
			if ($datasAuthorizedType) {
				$soustypeConditions  = [
					'Soustype.id' => array_keys(CakeSession::read('Auth.Soustypes'))
				];
				if( $isAdmin || Configure::read('DisplaySearch.FluxWithoutSoustype') ) {
					$soustypeConditions  = [
						'OR' => [
							'Soustype.id IS NULL',
							'Soustype.id' => array_keys(CakeSession::read('Auth.Soustypes'))
						]
					];
				}
				if( Configure::read('SoustypeVide.UserDesktopCreatorOnly')) {
					$type_conditions[] = array(
						'OR' => array(
							'AND' => array(
								'OR' => array(
									'Courrier.user_creator_id' => CakeSession::read('Auth.User.id'),
									'Courrier.desktop_creator_id' => array_keys(CakeSession::read('Auth.UserDesktops'))
								)
							),
							$soustypeConditions
						)
					);
				}
				else {
					$allAuthorizedTypes = [];
					foreach( CakeSession::read('Auth.TypesAll') as $typeId => $type) {
						$allAuthorizedTypes[$type['Type']['id']] = $type;
					}
					$type_conditions[] = array(
						'OR' => array(
							'Soustype.id IS NULL',
							'AND' => [
								'Soustype.id' => array_keys(CakeSession::read('Auth.Soustypes') ),
								'Soustype.type_id' => array_keys($allAuthorizedTypes)
							]
						)
					);
				}
				$conditions[] = array('OR' => $type_conditions);
			}
		}

		//critères Soustype
		if (!empty($params['Soustype']['Soustype'])) {
			$soustype_conditions = array();
			foreach ($params['Soustype']['Soustype'] as $soustype_id) {
				$soustype_conditions[] = array('Courrier.soustype_id' => $soustype_id);
			}
			$conditions[] = array('OR' => $soustype_conditions);
		} else if ($id != null && !empty($params['Soustype'])) {
			$soustype_conditions = array();
			foreach ($params['Soustype'] as $soustype) {
				$soustype_conditions[] = array('Courrier.soustype_id' => $soustype['id']);
			}
			$conditions[] = array('OR' => $soustype_conditions);
		}

		//critères Metadonnee
		$meta_conditions = array();
		$joinmeta = false;

// debug($params);
		// Recherche sur les valeurs présentes pour les métadonnées
		$useMetaCondition = false;
		if (!empty($params['Selectvaluemetadonnee']['Selectvaluemetadonnee'])) {
			foreach ($params['Selectvaluemetadonnee']['Selectvaluemetadonnee'] as $kmeta => $meta) {
				if ($meta != '') {
					$sqMeta = $Courrier->CourrierMetadonnee->sq(
						array(
							'fields' => array('courriers_metadonnees.courrier_id'),
							'alias' => 'courriers_metadonnees',
							'conditions' => array(
								'courriers_metadonnees.valeur' => $meta
							),
							'contain' => false
						)
					);
					$meta_conditions[] = array("Courrier.id IN ( {$sqMeta} )");
				}
				else {
					$meta_conditions[] = array();
				}
			}
			if (!empty($meta_conditions)) {
				foreach($meta_conditions as $metaId => $value ) {
					if( !empty( $value ) ) {
						$useMetaCondition = true;
					}
				}
				if($useMetaCondition) {
					$conditions[] = $meta_conditions;
				}
			}
		}
//$this->log($meta_conditions);
		if (!empty($params['Metadonnee']['Metadonnee'])) {
			foreach ($params['Metadonnee']['Metadonnee'] as $kmeta => $meta) {
				if ($meta != '') {
					if (!$joinmeta) {
						$joins[] = $Courrier->join($Courrier->CourrierMetadonnee->alias);
						$joinmeta = true;
					}
					$meta_conditions[] = array(
						array('CourrierMetadonnee.metadonnee_id' => $meta)
					);
				}
			}
			if (!empty($meta_conditions)) {
				$conditions[] = array('AND' => $meta_conditions);
			}
		}


		//critères Circuit
		if (!empty($params['Circuit']['Circuit'])) {
			$circuit_conditions = array();
			foreach ($params['Circuit']['Circuit'] as $circuit_id) {
				$circuit_conditions[] = array('Circuit.id' => $circuit_id);
			}
			$conditions[] = array('OR' => $circuit_conditions);
		} else if ($id != null && !empty($params['Circuit'])) {
			$circuit_conditions = array();
			foreach ($params['Circuit'] as $circuit) {
				$circuit_conditions[] = array('Circuit.id' => $circuit['id']);
			}
			$conditions[] = array('OR' => $circuit_conditions);
		}

		//critères Dossier
		if (!empty($params['Dossier']['Dossier'])) {
			$dossier_conditions = array();
			foreach ($params['Dossier']['Dossier'] as $dossier_id) {
				$dossier_conditions[] = array('Dossier.id' => $dossier_id);
			}
			$conditions[] = array('OR' => $dossier_conditions);
		} else if ($id != null && !empty($params['Dossier'])) {
			$dossier_conditions = array();
			foreach ($params['Dossier'] as $dossier) {
				$dossier_conditions[] = array('Dossier.id' => $dossier['id']);
			}
			$conditions[] = array('OR' => $dossier_conditions);
		}

		//critères Affaire
		if (!empty($params['Affaire']['Affaire'])) {
			$affaire_conditions = array();
			foreach ($params['Affaire']['Affaire'] as $affaire_id) {
				$affaire_conditions[] = array('Courrier.affaire_id' => $affaire_id);
			}
			$conditions[] = array('OR' => $affaire_conditions);
		} else if ($id != null && !empty($params['Affaire'])) {
			$affaire_conditions = array();
			foreach ($params['Affaire'] as $affaire) {
				$affaire_conditions[] = array('Courrier.affaire_id' => $affaire['id']);
			}
			$conditions[] = array('OR' => $affaire_conditions);
		}

		//critères Tache
		if (isset($params['Tache']['Tache']) && !empty($params['Tache']['Tache'])) {
			$joins[] = $Courrier->join(
				'Tache', array(
					'type' => 'INNER',
					'conditions' => array(
						"Tache.courrier_id = Courrier.id"
					)
				)
			);
			$sqTache = $Courrier->Tache->sq(
				array(
					'fields' => array('taches.courrier_id'),
					'alias' => 'taches',
					'conditions' => array(
						'taches.id' => $params['Tache']['Tache']
					),
					'contain' => false
				)
			);
			$conditions[] = array("Courrier.id IN ( {$sqTache} )");
		}

		if (!empty($params['Tache']['Tache'])) {
			$type_conditions = array();
			foreach ($params['Tache']['Tache'] as $tache_id) {
				$tache_conditions[] = array('Tache.id' => $tache_id);
			}
			$conditions[] = array('OR' => $tache_conditions);
		} else if ($id != null && !empty($params['Tache'])) {
			$type_conditions = array();
			foreach ($params['Tache'] as $tache) {
				$tache_conditions[] = array('Tache.id' => $tache['id']);
			}
			$conditions[] = array('OR' => $tache_conditions);
		}


		//critères Recherches sur les adresses
		if (!empty($params['Recherche']['contactadresse'])) {
			$conditions[] = array('Contact.banadresse' => $params['Recherche']['contactadresse']);
		}
		if (!empty($params['Recherche']['contactdept'])) {
			$conditions[] = array('Contact.ban_id' => $params['Recherche']['contactdept']);
		}

		if (!empty($params['Recherche']['contactcommune'])) {
			$conditions[] = array('Contact.bancommune_id' => $params['Recherche']['contactcommune']);
		}

		if (!empty($params['Recherche']['contactnomvoie'])) {
			$conditions[] = array('Contact.banadresse LIKE' => $Courrier->Contact->wildcard("*{$params['Recherche']['contactnomvoie']}*"));
		}

		if (!empty($params['Recherche']['contactcanton'])) {
			$conditions[] = array('Contact.canton ILIKE' => $Courrier->Contact->wildcard("*{$params['Recherche']['contactcanton']}*"));
		}

		if (!empty($params['Recherche']['contacttitreid'])) {
			$conditions[] = array('Contact.titre_id' => $params['Recherche']['contacttitreid'] );
		}

		//critères Recherches sur les adresses
		if (!empty($params['Recherche']['organismedepartement'])) {
			$conditions[] = array('Organisme.ban_id' => $params['Recherche']['organismedepartement']);
		}
		if (!empty($params['Recherche']['organismeadresse'])) {
			$conditions[] = array('Organisme.banadresse' => $params['Recherche']['organismeadresse']);
		}

		if (!empty($params['Recherche']['organismecommune'])) {
			$conditions[] = array('Organisme.bancommune_id' => $params['Recherche']['organismecommune']);
		}

		if (!empty($params['Recherche']['organismenomvoie'])) {
			$conditions[] = array('Organisme.banadresse LIKE' => $Courrier->Contact->wildcard("*{$params['Recherche']['organismenomvoie']}*"));
		}

		if (!empty($params['Recherche']['organismecanton'])) {
			$conditions[] = array('Organisme.canton ILIKE' => $Courrier->Contact->wildcard("*{$params['Recherche']['organismecanton']}*"));
		}

		//debug($params['Recherche']['cetat']);
		if (isset($params['Recherche']['cetat']) && ($params['Recherche']['cetat'] != '' )) {
			$etat = $params['Recherche']['cetat'];
			foreach( $joins as $join ){
				if( $join['alias'] == 'Bancontenu') {
					$joinBancontenu = true;
				}
			}
			if( !$joinBancontenu) {
				$joins[] = $Courrier->join(
					'Bancontenu', array(
						'type' => 'INNER',
						'conditions' => array(
							"Bancontenu.id IN ( {$sqBancontenu} )"
						)
					)
				);
				$fields = array_merge(
					$fields,
					$Courrier->Bancontenu->fields()
				);
				$joinBancontenu = true;
			}
			if( $etat == '-1' ) {
				$conditions[] = array(
					'Bancontenu.etat' => 1,
					'Bancontenu.bannette_id' => 2
				);
			}
			else {
				$conditions[] = array('Bancontenu.etat' => $params['Recherche']['cetat']);
			}
			$fields = array_merge(
				$fields,
				$Courrier->Bancontenu->fields()
			);
		}

// critères Service
		if (!empty($params['Service']['Service'])) {
//			$joins[] = $Courrier->Bancontenu->Desktop->join(
//				'DesktopsService', array(
//					'type' => 'LEFT OUTER'
//				)
//			);
//			$joins[] = $Courrier->Bancontenu->Desktop->DesktopsService->join(
//				'Service', array(
//					'type' => 'LEFT OUTER'
//				)
//			);
//			$fields = array_merge( $fields, $Courrier->Bancontenu->Desktop->DesktopsService->Service->fields() );
//
//			$conditions[] = array('OR' => array('DesktopsService.service_id' => $params['Service']['Service']));


			foreach( $joins as $join ){
				if( $join['alias'] == 'Bancontenu') {
					$joinBancontenu = true;
				}
			}

			if( !$joinBancontenu) {
				$desktopsIds = $this->Service->getDesktops($params['Service']['Service']);
				$sqAllBancontenus = $Courrier->Bancontenu->sqBannettesCourrierForDesktops('Courrier.id', $desktopsIds);
				$joins[] = $Courrier->join(
					'Bancontenu', array(
						'type' => 'INNER',
						'conditions' => array(
							"Bancontenu.id IN ( {$sqAllBancontenus} )"
						)
					)
				);
				$fields = array_merge(
					$fields,
					$Courrier->Bancontenu->fields()
				);
				$joinBancontenu = true;
			}
			$conditions[] = array('OR' => array('Bancontenu.desktop_id' => $desktopsIds));

		} else {
			$joins[] = $Courrier->join(
				'Service', array(
					'type' => 'LEFT OUTER'
				)
			);
			$fields = array_merge( $fields, $Courrier->Service->fields() );
		}
//debug($params);
//debug($conditions);


		// Filtre pour les activités
		if( isset($params['Activite']['Activite']) && !empty($params['Activite']['Activite']) ) {
			foreach( $joins as $j => $join ){
				if( $join['alias'] == 'Bancontenu') {
					$joinBancontenu = true;
				}
			}

			if( !$joinBancontenu) {
				$joins[] = $Courrier->join(
					'Bancontenu', array(
						'type' => 'INNER',
						'conditions' => array(
							"Bancontenu.id IN ( {$sqBancontenu} )"
						)
					)
				);
				$fields = array_merge(
					$fields,
					$Courrier->Bancontenu->fields()
				);
				$joinBancontenu = true;
			}
			$joins[] = $Courrier->Organisme->join(
				'ActiviteOrganisme',
				array(
					'type' => 'LEFT OUTER',
					'conditions' => array(
						'ActiviteOrganisme.activite_id' => $params['Activite']['Activite']
					)
				)
			);
			$fields = array_merge( $fields, $Courrier->Organisme->ActiviteOrganisme->fields() );

			$activitesOrgs = $Courrier->Organisme->ActiviteOrganisme->find(
				'all',
				array(
					'conditions' => array(
						'ActiviteOrganisme.activite_id' => $params['Activite']['Activite']
					),
					'recursive' => -1
				)
			);
			$orgsIds = Hash::extract( $activitesOrgs, '{n}.ActiviteOrganisme.organisme_id');
			$conditions[] = array(
				'Courrier.organisme_id' => $orgsIds,
				'Courrier.organisme_id IS NOT NULL'
			);
		}

		if (!empty($params['Recherche']['creference']) && Configure::read('Recherche.FluxCopie') ) {
			foreach( $joins as $j => $join ){
				if( $join['alias'] == 'Bancontenu') {
					$joinBancontenu = true;
				}
			}

			if( !$joinBancontenu) {
				$joins[] = $Courrier->join(
					'Bancontenu', array(
						'type' => 'INNER',
						'conditions' => array(
							"Bancontenu.id IN ( {$sqBancontenu} )"
						)
					)
				);
				$fields = array_merge(
					$fields,
					$Courrier->Bancontenu->fields()
				);
				$joinBancontenu = true;
			}

			if( strpos( $params['Recherche']['creference'], ',' ) ) {
				$references = explode( ',', str_replace( ' ', '', $params['Recherche']['creference']) );

				$refconditions = [];
				foreach( $references as $reference ) {
					$courrier = ClassRegistry::init('Courrier')->find(
						'first',
						array(
							'fields' => array('Courrier.id', 'Courrier.parent_id'),
							'conditions' => array(
								'Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*")
							),
							'contain' => false,
							'recursive' => -1
						)
					);
					$courrierReponse = array();
					if (!empty($courrier)) {
						$courrierReponse = ClassRegistry::init('Courrier')->getSon($courrier['Courrier']['id']);
					}

					if (empty($courrier)) {
						$refconditions[] = array('Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*"));
					}
					if (!empty($courrier) && empty($courrier['Courrier']['parent_id']) && empty($courrierReponse)) {
						$refconditions[] = array('Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*"));
					}
					if (!empty($courrier) && !empty($courrier['Courrier']['parent_id']) || !empty($courrier) && !empty($courrierReponse)) {
						if (!empty($courrier) && !empty($courrier['Courrier']['parent_id'])) {
							$courrierOrig = ClassRegistry::init('Courrier')->find(
								'first',
								array(
									'fields' => array('Courrier.reference'),
									'conditions' => array(
										'Courrier.id' => $courrier['Courrier']['parent_id']
									),
									'contain' => false,
									'recursive' => -1
								)
							);
							$refconditions[] = array(
								'OR' => array(
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*")
									),
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$courrierOrig['Courrier']['reference']}*")
									)
								)
							);
						}
						if (!empty($courrier) && !empty($courrierReponse)) {
							$refconditions[] = array(
								'OR' => array(
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$reference}*")
									),
									array(
										'Courrier.reference ILIKE' => $Courrier->wildcard("*{$courrierReponse['Courrier']['reference']}*")
									)
								)
							);
						}
					}

				}
				$conditions[] = array('OR' => $refconditions);

			}
			else {
				$courriers = ClassRegistry::init('Courrier')->find(
					'all',
					array(
						'fields' => array('Courrier.id', 'Courrier.parent_id'),
						'conditions' => array(
							'Courrier.reference ILIKE' => $Courrier->wildcard("*{$params['Recherche']['creference']}*")
						),
						'contain' => false,
						'recursive' => -1
					)
				);

				$multiconditions = [];
				foreach($courriers as $courrier) {

					$courrierReponse = array();
					if (!empty($courrier)) {
						$courrierReponse = ClassRegistry::init('Courrier')->getSon($courrier['Courrier']['id']);
					}

					if (empty($courrier)) {
						$multiconditions[] = array('Courrier.reference ILIKE' => $Courrier->wildcard("*{$params['Recherche']['creference']}*"));
					}
					if (!empty($courrier) && empty($courrierReponse)) {
						$multiconditions[] = array('Courrier.reference ILIKE' => $Courrier->wildcard("*{$params['Recherche']['creference']}*"));
					}
					if (!empty($courrier) && !empty($courrierReponse)) {
						$multiconditions[] = array(
							'OR' => array(
								array(
									'Courrier.reference ILIKE' => $Courrier->wildcard("*{$params['Recherche']['creference']}*")
								),
								array(
									'Courrier.reference ILIKE' => $Courrier->wildcard("*{$courrierReponse['Courrier']['reference']}*")
								)
							)
						);
					}
					if(!empty($courrier) && !empty($courrier['Courrier']['parent_id'])){
						$courrierOrig = ClassRegistry::init('Courrier')->find(
							'first',
							array(
								'fields' => array('Courrier.reference'),
								'conditions' => array(
									'Courrier.id' => $courrier['Courrier']['parent_id']
								),
								'contain' => false,
								'recursive' => -1
							)
						);
						$multiconditions[] = array(
							'OR' => array(
								array(
									'Courrier.reference ILIKE' => $Courrier->wildcard("*{$params['Recherche']['creference']}*")
								),
								array(
									'Courrier.reference ILIKE' => $Courrier->wildcard("*{$courrierOrig['Courrier']['reference']}*")
								)
							)
						);
					}
				}
				$conditions[] = array('OR' => $multiconditions);
			}
		}

		$autreconditions = array();
		$bureaux = Hash::extract(CakeSession::read('Auth.User.SecondaryDesktops'), '{n}.Profil.name');
		if ( CakeSession::read('Auth.User.Desktop.Profil.name') != 'Admin' && !in_array('Admin', $bureaux)  && !empty( $params['Recherche']['creference'] ) ) {
			$arraySecDesktop = array();
			foreach (CakeSession::read('Auth.User.SecondaryDesktops') as $d => $desktop) {
				$arraySecDesktop[] = $desktop['id'];
			}
			$arraySecDesktop[] = CakeSession::read('Auth.User.Desktop.id');
			$courrierIdsBancontenu = $Courrier->Bancontenu->find(
				'all',
				array(
					'conditions' => array(
						'Bancontenu.desktop_id IN' => $arraySecDesktop
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			$idsToCheck = Hash::extract( $courrierIdsBancontenu, '{n}.Bancontenu.id' );
			$idsCourriersToCheck = Hash::extract( $courrierIdsBancontenu, '{n}.Bancontenu.courrier_id' );
			if (!empty($params['Recherche']['creference'])) {
				$courriern = ClassRegistry::init('Courrier')->find(
					'first',
					array(
						'fields' => array('Courrier.id'),
						'conditions' => array(
							'Courrier.reference ILIKE' => $Courrier->wildcard("*{$params['Recherche']['creference']}*")
						),
						'contain' => false,
						'recursive' => -1
					)
				);
				if( !empty($courriern ) ) {
					$autreconditions[] = array(
						'Courrier.reference' => $params['Recherche']['creference'],
						'Bancontenu.courrier_id' => $courriern['Courrier']['id'],
						'Bancontenu.courrier_id' => $idsCourriersToCheck // FIXME : à tester
//                        'Bancontenu.id' => $idsToCheck
					);
				}
				else {
					$autreconditions[] = array();
				}
			}
			else {
				$autreconditions[] = array(
					'OR' => array(
						'Bancontenu.id' => $idsToCheck,
						'Bancontenu.courrier_id' => $idsCourriersToCheck
					)
				);
			}
		}


		if (isset($params['Recherche']['cretard'])) {
			if ($params['Recherche']['cretard'] == 'Oui') {
				foreach( $joins as $j => $join ){
					if( $join['alias'] == 'Bancontenu') {
						$joinBancontenu = true;
					}
				}

				if( !$joinBancontenu) {
					$joins[] = $Courrier->join(
						'Bancontenu', array(
							'type' => 'INNER',
							'conditions' => array(
								"Bancontenu.id IN ( {$sqBancontenu} )"
							)
						)
					);
					$fields = array_merge(
						$fields,
						$Courrier->Bancontenu->fields()
					);
					$joinBancontenu = true;
				}
				$conditions[] = array('Bancontenu.etat NOT IN (-1, 2)');
				//            $conditions[] = array('"Courrier__retard"' => true );
				$conditions[] = array(
					'( CASE
                        WHEN "Courrier"."delai_unite" = \'0\' THEN ( ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 day\' ) ) < NOW() )
                        WHEN "Courrier"."delai_unite" = \'1\' THEN ( ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 week\' ) ) < NOW() )
                        WHEN "Courrier"."delai_unite" = \'2\'  THEN ( ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 month\' ) ) < NOW() )
                        ELSE false
                    END )'
				);
			} else if ($params['Recherche']['cretard'] == 'Non') {
				$conditions[] = array(
					'( CASE
                        WHEN "Courrier"."delai_unite" = \'0\' THEN ( ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 day\' ) ) >= NOW() )
                        WHEN "Courrier"."delai_unite" = \'1\' THEN ( ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 week\' ) ) >= NOW() )
                        WHEN "Courrier"."delai_unite" = \'2\' THEN ( ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 month\' ) ) >= NOW() )
                        ELSE false
                    END )'
				);
			}
		}

		// Sens du flux
		if (isset($params['Recherche']['cdirection']) && ($params['Recherche']['cdirection'] != '' )) {
			$conditions[] = array('Courrier.direction' => $params['Recherche']['cdirection']);
		}

		// Agent possédant le flux
		if (!empty($params['Recherche']['bancontenudesktopid'])) {
			$sqAllBancontenus = $Courrier->Bancontenu->sqDernieresBannettesRecherche('Courrier.id');
//			$joinBancontenu = true;
			foreach( $joins as $join ){
				if( $join['alias'] == 'Bancontenu') {
					$joinBancontenu = true;
				}
			}

			if( !$joinBancontenu) {
				$joins[] = $Courrier->join(
					'Bancontenu', array(
						'type' => 'INNER',
						'conditions' => array(
							"Bancontenu.id IN ( {$sqAllBancontenus} )"
						)
					)
				);
				$fields = array_merge(
					$fields,
					$Courrier->Bancontenu->fields()
				);
				$joinBancontenu = true;
			}
			$conditions[] = array('OR' => array('Bancontenu.desktop_id' => $params['Recherche']['bancontenudesktopid']));
		}

		//critères Sur les données Océrisées du document scanné
		if (isset($params['Recherche']['cocrdata']) && !empty($params['Recherche']['cocrdata'])) {
			$joins[] = $Courrier->join(
				'Document', array(
					'type' => 'INNER',
					'conditions' => array(
						"Document.courrier_id = Courrier.id"
					)
				)
			);
			$ocrData = $params['Recherche']['cocrdata'];
			$sqDocument = $Courrier->Document->sq(
				array(
					'fields' => array('documents.courrier_id'),
					'alias' => 'documents',
					'conditions' => array(
						"documents.ocr_data LIKE '%$ocrData%'"
					),
					'contain' => false
				)
			);
			$conditions[] = array("Courrier.id IN ( {$sqDocument} )");
		}


		// Réposne ou non ?
		if (isset($params['Recherche']['creponse']) && ($params['Recherche']['creponse'] != '' )) {
			if( $params['Recherche']['creponse'] == '1' ) {
				$sqCourrierRe = $Courrier->sq(
					array(
						'fields' => array('courriers.parent_id'),
						'alias' => 'courriers',
						'conditions' => array(
							'courriers.parent_id is not null'
						),
						'contain' => false
					)
				);
				$conditions[] = array("Courrier.id IN ( {$sqCourrierRe} )");
			}
			else if( $params['Recherche']['creponse'] == '0' ) {
				$sqCourrierRe = $Courrier->sq(
					array(
						'fields' => array('courriers.parent_id'),
						'alias' => 'courriers',
						'conditions' => array(
							'courriers.parent_id is not null'
						),
						'contain' => false
					)
				);
				$conditions[] = array(
					"Courrier.id NOT IN ( {$sqCourrierRe} )"
				);
			}
		}


		// Agent étant intervneu sur le flux quelque soit le moment
		if (!empty($params['Recherche']['bancontenuuserid'])) {
//			$joinBancontenu = true;
			foreach( $joins as $join ){
				if( $join['alias'] == 'Bancontenu') {
					$joinBancontenu = true;
				}
			}

			if( !$joinBancontenu) {
				$desktopsIds = $this->User->getDesktops($params['Recherche']['bancontenuuserid']);
				$sqAllBancontenus = $Courrier->Bancontenu->sqBannettesCourrierForDesktops('Courrier.id', $desktopsIds);
				$joins[] = $Courrier->join(
					'Bancontenu', array(
						'type' => 'INNER',
						'conditions' => array(
							"Bancontenu.id IN ( {$sqAllBancontenus} )"
						)
					)
				);
				$fields = array_merge(
					$fields,
					$Courrier->Bancontenu->fields()
				);
				$joinBancontenu = true;
			}
			$conditions[] = array('OR' => array('Bancontenu.desktop_id' => $desktopsIds));
		}

		// Retourne uniquement des flux adressés en copie
		$copies = [];
		if(!empty($params['Recherche']['ccopie']) &&  $params['Recherche']['ccopie'] == 'Oui') {
			foreach( $joins as $j => $join ){
				if( $join['alias'] == 'Bancontenu') {
					$joinBancontenu = true;
				}
			}

			if( !$joinBancontenu) {
				$joins[] = $Courrier->join(
					'Bancontenu', array(
						'type' => 'INNER',
					)
				);
				$fields = array_merge(
					$fields,
					$Courrier->Bancontenu->fields()
				);
				$joinBancontenu = true;
			}
			$arraySecDesktop = array();
			foreach (CakeSession::read('Auth.User.SecondaryDesktops') as $d => $desktop) {
				$arraySecDesktop[] = $desktop['id'];
			}
			$arraySecDesktop[] = CakeSession::read('Auth.User.Desktop.id');
			$courrierIdsBancontenu = $Courrier->Bancontenu->find(
				'all',
				array(
					'conditions' => array(
						'Bancontenu.desktop_id IN' => $arraySecDesktop,
						'Bancontenu.bannette_id' => BAN_COPY
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			$idsToCheck = Hash::extract( $courrierIdsBancontenu, '{n}.Bancontenu.id' );

			$copies[] = array(
				'OR' => array(
					'Bancontenu.id' => $idsToCheck
				)
			);
		}

		$query = array(
			'fields' => $fields,
			'recursive' => -1,
			'joins' => $joins,
			'conditions' => array(
				'OR' => array(
					$conditions,
					$copies,
					$autreconditions
				)
			),
			'order' => $order,
			'contain' => $contain
		);
//$this->log($this->sq( $query ) );
		return $query;
	}

}

?>
