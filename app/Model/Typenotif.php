<?php

App::uses('AppModel', 'Model');

/**
 * Typenotif model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * Les différents types de notifications sont les suivants :
 *
 * wkf_new : nouveau flux arrivant à une étape
 * wkf_wait : attente de validation de la part des autres compositions d une etape
 * wkf_done : validation effectuée par une des composition d une étape
 * ban_xnew : plusieurs nouveaux flux dans la bannette docs
 * ban_cancel : flux refusé
 * ban_response : réponse à un flux
 * ban_info : nouveau flux dans la bannette "pour info"
 * sys_rights : changement des droits
 * dkt_delegto : delegation de bureau
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Typenotif extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $codes = array(
		'wkf_new' => '1',
		'wkf_wait' => '2',
		'wkf_done' => '3',
		'ban_xnew' => '4',
		'ban_cancel' => '5',
		'ban_response' => '6',
		'ban_info' => '7',
		'sys_rights' => '8',
		'dkt_delegto' => '9',
		'wkf_jmp' => '10',
		'wkf_jbk' => '11',
		'ban_copy' => '12'
	);

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'Typenotif';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'name' => array(
			'rule' => 'isUnique',
			array(
				'allowEmpty' => false
			)
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'typenotifs_users',
			'foreignKey' => 'typenotif_id',
			'associationForeignKey' => 'user_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'TypenotifUser'
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Notification' => array(
			'className' => 'Notification',
			'foreignKey' => 'notification_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

?>
