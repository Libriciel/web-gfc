<?php


/**
 * Consultation model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Consultation extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Consultation';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'consultations';


    /**
	 * hasMany
	 *
	 * @access public
	 */
	/**
	 * belongsTo
	 *
	 * @access public
	 */

    public $belongsTo = array(
		'Operation' => array(
			'className' => 'Operation',
			'foreignKey' => 'operation_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);


    /**
     *  hasMany
     *
     *  @access public
     */
    public $hasMany = array(
		'Pliconsultatif' => array(
			'className' => 'Pliconsultatif',
			'foreignKey' => 'consultation_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Courrier' => array(
			'className' => 'Courrier',
			'foreignKey' => 'consultation_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);

    public $validate = array(
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		),
		'numero' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		),
		'operation_id' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		),
		'datelimite' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)
		),
		'heurelimite' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		)
    );



    /**
     * Fonction permettant de rechercher les marchés
     *
     * @param type $criteres
     * @return array()
     */
    public function search( $criteres ) {

//debug($criteres);
        $conditions = array();
        if( isset( $criteres['SearchConsultation']['name'] ) && !empty( $criteres['SearchConsultation']['name'] ) ) {
            $conditions[] = array( 'Consultation.id' => $criteres['SearchConsultation']['name'] );
        }

        if( isset( $criteres['SearchConsultation']['numero'] ) && !empty( $criteres['SearchConsultation']['numero'] ) ) {
            $conditions[] = 'Consultation.numero ILIKE \''.$this->wildcard( '%'.$criteres['SearchConsultation']['numero'].'%' ).'\'';
        }

        if( isset( $criteres['SearchConsultation']['operation_id'] ) && !empty( $criteres['SearchConsultation']['operation_id'] ) ) {
            $conditions[] = array( 'Consultation.operation_id' => $criteres['SearchConsultation']['operation_id'] );
        }

        //
        if (isset($criteres['SearchConsultation']['datelimitedebut']) && !empty($criteres['SearchConsultation']['datelimitedebut'])) {
            if (isset($criteres['SearchConsultation']['datelimitefin']) && !empty($criteres['SearchConsultation']['datelimitefin'])) {
                $conditions[] = array('AND' => array(array('Consultation.datelimite >=' => $criteres['SearchConsultation']['datelimitedebut']), array('Consultation.datelimite <=' => $criteres['SearchConsultation']['datelimitefin'])));
            }
        }

        if( isset($criteres['SearchConsultation']['active']) ) {
            $active = @$criteres['SearchConsultation']['active'];
            $conditions[] = array(
                'Consultation.active' => $active
            );
        }
        else {
            $conditions[] = array(
                'Consultation.active' => true
            );
        }

        // Partie pour les OPs
        if( isset($criteres['SearchConsultation']['operationactive']) ) {
            $operationactive = @$criteres['SearchConsultation']['operationactive'];
            $conditions[] = array(
                'Operation.active' => $operationactive
            );
        }
        else {
            $conditions[] = array(
                'Operation.active' => true
            );
        }


        $joins = array(
			$this->join( 'Courrier', array('type' => 'LEFT OUTER') ),
			$this->Courrier->join( 'Organisme', array('type' => 'LEFT OUTER') ),
			$this->Courrier->join( 'Contact', array('type' => 'LEFT OUTER') ),
            $this->Courrier->join( 'Origineflux', array('type' => 'LEFT OUTER') ),
//            $this->join('Pliconsultatif', array('type' => 'LEFT OUTER','limit' => 1) ),
            $this->join('Operation', array('type' => 'LEFT OUTER') )
		);
		$contain = false;
		$fields = array_merge(
			$this->fields(),
            $this->Courrier->fields(),
            $this->Courrier->Organisme->fields(),
            $this->Courrier->Contact->fields(),
            $this->Courrier->Origineflux->fields(),
//            $this->Pliconsultatif->fields(),
            $this->Operation->fields()
		);
        $group = array('Operation.name', 'Origineflux.id', 'Courrier.id', 'Consultation.id', 'Operation.id', 'Organisme.id', 'Contact.id');

         if (isset($criteres['Recherche']['ctypedocument'] ) &&  $criteres['Recherche']['ctypedocument'] == 'PLI'){
            $joins[] = $this->join('Pliconsultatif', array('type' => 'LEFT OUTER') );
            $fields = array_merge( $fields, $this->Pliconsultatif->fields() );
            $group = array_merge(
                $group,
                array('Pliconsultatif.id')
            );
         }

        $Metadonnee = ClassRegistry::init('Metadonnee');
        $metaDatas = $Metadonnee->find('first', array( 'conditions' => array('Metadonnee.id' => Configure::read('MetadonnneMontant.id')), 'recursive' => -1));
        if( !empty($metaDatas) ) {
            $joins[] = $this->Courrier->join(
                'CourrierMetadonnee',
                array(
                    'type' => 'LEFT OUTER',
                    'conditions' => array(
                        'CourrierMetadonnee.metadonnee_id' => $metaDatas['Metadonnee']['id']
                    )
                )
            );
            $fields = array_merge( $fields, $this->Courrier->CourrierMetadonnee->fields() );

            $joins[] = $this->Courrier->CourrierMetadonnee->join(
                'Metadonnee',
                array(
                    'type' => 'LEFT OUTER'
                )
            );
            $fields = array_merge( $fields, $this->Courrier->CourrierMetadonnee->Metadonnee->fields() );
            $group = array_merge(
                    $group,
                    array('CourrierMetadonnee.id', 'Metadonnee.id')
            );
        }

        if (isset($criteres['Recherche']['cdatefilter']) && !empty($criteres['Recherche']['cdatefilter'])) {
            if (isset($criteres['Recherche']['cdatefilterfin']) && !empty($criteres['Recherche']['cdatefilterfin'])) {
                $conditions[] = array('AND' => array(array('Pliconsultatif.date >=' => $criteres['Recherche']['cdatefilter']), array('Pliconsultatif.date <=' => $criteres['Recherche']['cdatefilterfin'])));
            } else {
                $conditions[] = array('Pliconsultatif.date' => $criteres['Recherche']['cdatefilter']);
            }
        }

//        $query = array(
//            'contain' => array(
//                'Pliconsultatif',
//                'Operation'
//            ),
//            'limit' => 20,
//            'order' => 'Consultation.name ASC',
//            'conditions' => array(
//                $conditions
//            )
//        );
         $query = array(
            'fields' => $fields,
            'recursive' => -1,
            'joins' => $joins,
            'conditions' => $conditions,
            'contain' => $contain,
            'order' => array('Operation.name ASC'),
            'group' => $group
        );

//debug($query);
        return $query;
    }
}

?>
