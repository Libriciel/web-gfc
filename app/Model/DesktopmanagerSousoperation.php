<?php

App::uses('AppModel', 'Model');

/**
 * RechercheDesktop model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class DesktopmanagerSousoperation extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'DesktopmanagerSousoperation';

    /**
     *
     * @var type
     */
    public $useTable = 'desktopsmanagers_sousoperations';

    /**
     * Validation rules
     *
     * @access public
     */
    public $validate = array(
        'desktopmanager_id' => array(
            array(
                'rule' => array('numeric'),
                'allowEmpty' => true,
            ),
        ),
        'sousoperation_id' => array(
            array(
                'rule' => array('numeric'),
                'allowEmpty' => true,
            ),
        )
    );

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        /* 'Desktopmanager' => array(
          'className' => 'Desktopmanager',
          'foreignKey' => 'desktopmanager_id',
          'type' => 'INNER',
          'conditions' => null,
          'fields' => null,
          'order' => null
          ), */
        'Sousoperation' => array(
            'className' => 'Sousoperation',
            'foreignKey' => 'sousoperation_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Intituleagent' => array(
            'className' => 'Intituleagent',
            'foreignKey' => 'intituleagent_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );

}

?>
