<?php

App::uses('AppModel', 'Model');

/**
 * Formatreponse model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Soustypecible extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Soustypecible';

    /**
     *
     * @var type
     */
    public $useTable = 'soustypes';
	/**
	 *
	 * @var type
	 */
	public $displayField = "name";

	/**
	 *
	 * @var type
	 */
	public $hasAndBelongsToMany = array(
		'Soustype' => array(
			'className' => 'Soustype',
			'joinTable' => 'soustypes_soustypescibles',
			'foreignKey' => 'soustypecible_id',
			'associationForeignKey' => 'soustype_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => '',
			'with' => 'SoustypeSoustypecible'
		)
	);

}

?>
