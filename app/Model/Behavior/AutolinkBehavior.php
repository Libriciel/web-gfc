<?php

/**
 * Autolink behavior class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Christian Buffin
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model.Behavior
 */
class AutolinkBehavior extends AppBehavior {

	/**
	 *
	 */
	protected function _uniqueSq($schema, $table, $column) {
		return "( SELECT
								COUNT(c.oid)
							FROM
								pg_catalog.pg_class c,
								pg_catalog.pg_class c2,
								pg_catalog.pg_index i
							WHERE
								c.oid = (
									SELECT
											c.oid
										FROM
											pg_catalog.pg_class c
											LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
										WHERE
											c.relname = {$table}
											AND pg_catalog.pg_table_is_visible(c.oid)
											AND n.nspname ~ '^({$schema})$' -- FIXME
								)
								AND c.oid = i.indrelid
								AND i.indexrelid = c2.oid
								AND i.indisunique = true
								AND ( pg_catalog.pg_get_indexdef(i.indexrelid, 0, true) LIKE '%(' || {$column} || ')' )
						) > 0";
	}

	/**
	 *
	 */
	protected function _foreignKeys(&$model, $from = true) {
		$ds = $model->getDataSource($model->useDbConfig);
		$table = $ds->fullTableName($model, false);

		if ($from) {
			$condition = "kcu.table_name = '{$table}'";
		} else {
			$condition = "ccu.table_name = '{$table}'";
		}

		$sql = "SELECT
						kcu.table_name AS \"From__table\",
						kcu.column_name AS \"From__column\",
						" . $this->_uniqueSq('public', 'kcu.table_name', 'kcu.column_name') . " AS \"From__unique\",
						ccu.table_name AS \"To__table\",
						ccu.column_name AS \"To__column\",
						" . $this->_uniqueSq('public', 'ccu.table_name', 'ccu.column_name') . " AS \"To__unique\"
					FROM information_schema.table_constraints tc
						LEFT JOIN information_schema.key_column_usage kcu ON (
							tc.constraint_catalog = kcu.constraint_catalog
							AND tc.constraint_schema = kcu.constraint_schema
							AND tc.constraint_name = kcu.constraint_name
						)
						LEFT JOIN information_schema.referential_constraints rc ON (
							tc.constraint_catalog = rc.constraint_catalog
							AND tc.constraint_schema = rc.constraint_schema
							AND tc.constraint_name = rc.constraint_name
						)
						LEFT JOIN information_schema.constraint_column_usage ccu ON (
							rc.unique_constraint_catalog = ccu.constraint_catalog
							AND rc.unique_constraint_schema = ccu.constraint_schema
							AND rc.unique_constraint_name = ccu.constraint_name
						)
					WHERE {$condition}
						AND tc.constraint_type = 'FOREIGN KEY';";

		$foreignKeys = $model->query($sql);

		return $foreignKeys;
	}

	/**
	 * TODO:
	 * 	- vérifier
	 * 	- dependant ?
	 * 	- belongsTo -> IIF not unique From.column ?
	 * 	- vérifier HABTM
	 */
	public function getAssociations(&$model) {
		$belongsTo = array();
		$hasMany = array();
		$hasOne = array();
		$hasAndBelongsToMany = array();

		// belongsTo
		foreach ($this->_foreignKeys($model, true) as $Assoc) {
			$toClass = Inflector::classify($Assoc['To']['table']);
			$fromColumnInfos = $model->schema($Assoc['From']['column']);
			$belongsTo[$toClass] = array(
				'className' => $toClass,
				'foreignKey' => $Assoc['From']['column'],
				'conditions' => null,
				'type' => ( $fromColumnInfos['null'] == false ? 'INNER' : null ),
				'fields' => null,
				'order' => null,
				'counterCache' => null,
				'counterScope' => null
			);
		}

		foreach ($this->_foreignKeys($model, false) as $Assoc) {
			// hasAndBelongsToMany
			if (strpos($Assoc['From']['table'], '_')) {
				$modelTable = $model->getDataSource($model->useDbConfig)->fullTableName($model, false);

				$fromTable = preg_replace("/(^{$modelTable}_|_{$modelTable}$)/", '', $Assoc['From']['table']);
				$fromClass = Inflector::classify($fromTable);

				$foreignKey = Inflector::singularize($modelTable) . '_id';
				$associationForeignKey = Inflector::singularize($fromTable) . '_id';

				$unique = null;
				foreach ($model->getDataSource()->index($Assoc['From']['table']) as $key => $index) {
					$uniqueIndex = (
							is_array($index['column'])
							&& count($index['column']) == 2
							&& in_array($foreignKey, $index)
							&& in_array($associationForeignKey, $index)
							);

					if ($uniqueIndex) {
						$unique = true;
					}
				}

				$hasAndBelongsToMany[$fromClass] = array(
					'className' => $fromClass,
					'joinTable' => $Assoc['From']['table'],
					'foreignKey' => $foreignKey,
					'associationForeignKey' => $associationForeignKey,
					/// INFO: if false, will not try to check if already inserted
					'unique' => $unique,
					'conditions' => null,
					'fields' => null,
					'order' => null,
					'limit' => null,
					'offset' => null,
					'finderQuery' => null,
					'deleteQuery' => null,
					'insertQuery' => null,
					'with' => Inflector::classify($Assoc['From']['table'])
				);
			}
			// hasMany - IIF not unique From.column
			// @see http://book.cakephp.org/view/82/hasMany
			else if (!$Assoc['From']['unique']) {
				$fromClass = Inflector::classify($Assoc['From']['table']);
				$hasMany[$fromClass] = array(
					'className' => $fromClass,
					'foreignKey' => $Assoc['From']['column'],
					'conditions' => null,
					'fields' => null,
					'order' => null,
					'limit' => null,
					'offset' => null,
					/// 1.2, 1.3: When dependent is set to true, recursive model deletion is possible. In this example, Comment records will be deleted when their associated User record has been deleted.
					/// -> IIF ON DELETE CASCADE ?
					'dependent' => false,
					/// 1.2, 1.3: When exclusive is set to true, recursive model deletion does the delete with a deleteAll() call, instead of deleting each entity separately. This greatly improves performance, but may not be ideal for all circumstances.
					/// -> IIF ON DELETE CASCADE ?
					'exclusive' => false,
					'finderQuery' => null,
					'counterQuery' => null
				);
			}
			// hasOne - IIF unique From.column
			// @see http://book.cakephp.org/view/1041/hasOne
			else {
				$fromClass = Inflector::classify($Assoc['From']['table']);
				$hasOne[$fromClass] = array(
					'className' => $fromClass,
					'foreignKey' => $Assoc['From']['column'],
					'conditions' => null,
					'fields' => null,
					'order' => null,
					'dependent' => false,
				);
			}
		}

		return array(
			'belongsTo' => $belongsTo,
			'hasOne' => $hasOne,
			'hasMany' => $hasMany,
			'hasAndBelongsToMany' => $hasAndBelongsToMany,
		);
	}

	/**
	 * Setup this behavior with the specified configuration settings.
	 *
	 * @param object &$model Model using this behavior
	 * @param array $settings Configuration settings for $model
	 * @access public
	 */
	public function setup(Model $model, $settings) {
		parent::setup($model, $settings);

// 			$model->bindModel( $this->getAssociations( $model ), false );
	}

	/**
	 * TODO: basics lib
	 */
	public function exportAssociations(&$model) {
		$export = '';

		foreach ($this->getAssociations($model) as $key => $assocs) {
			$exportTmp = "/**\n*\n*/\n\npublic \$$key = " . var_export($assocs, true) . "\n";
			$exportTmp = preg_replace('/array \(/', 'array(', $exportTmp);
			$exportTmp = preg_replace('/=>\W+array\(/', '=> array(', $exportTmp);
			$exportTmp = preg_replace('/\([\w\n]*\)$/', '()', $exportTmp);
			$exportTmp = preg_replace('/\)$/', ");\n", $exportTmp);
			$exportTmp = preg_replace('/  /', "\t", $exportTmp);
			$exportTmp = preg_replace('/NULL/', 'null', $exportTmp);

			$export .= $exportTmp;
		}

		return $export;
	}

}

?>
