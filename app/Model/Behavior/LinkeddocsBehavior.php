<?php

/**
 * Linkeddocs behavior class.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		app.Model.Behavior
 *
 */
class LinkeddocsBehavior extends ModelBehavior {

	/**
	 * Définition des champs "lourds" (contenus de fichier)
	 *
	 * @access public
	 * @var array
	 */
	private $_heavyFields = array(
		'content',
		'preview'
	);

	/**
	 * Initialization du behavior
	 *
	 * @access public
	 * @param Model $model
	 * @param array $config
	 * @return void
	 */
	public function setup(Model $model, $config = array()) {
		parent::setup($model, $config);
	}

	/**
	 * Récupération des champs "légers" (sans les champs "lourds")
	 *
	 * @access public
	 * @param Model $model
	 * @return array
	 */
	public function getLightFields(&$model) {
		$heavyFields = array();
		foreach ($this->_heavyFields as $field) {
			$heavyFields[] = $model->alias . '.' . $field;
		}
		return array_diff($model->fields(), $heavyFields);
	}

	/**
	 *
	 * @return boolean
	 */
	private function _checkGedAccess() {
		//TODO: mettre en place le fonctionnement avec une GED
		return false;
	}

	/**
	 *
	 * @return string
	 */
	private function _sendToGed($tmpFile) {
		$path = '';
		//TODO: mettre en place le fonctionnement avec une GED
		$path = "-- fonctionnalité en cours de développement --";
		return $path;
	}

	/**
	 * CakePHP callback beforeSave
	 * Permet d'enregistrer le contenu d'un fichier en base ou via une GED
	 *
	 * @access public
	 * @param array $options
	 * @return boolean
	 */
	public function beforeSave(Model $model, $options = Array()) {
		parent::beforeSave($model, $options);
		if (!empty($_FILES['myfile'])) {
			if (empty($model->data[$model->alias]['name'])) {
				$model->data[$model->alias]['name'] = /* Inflector::slug( */$_FILES['myfile']['name']/* ) */;
			}
			if ($this->_checkGedAccess()) {
				//TODO: mettre en place le fonctionnement avec une GED
				$model->data[$model->alias]['gedpath'] = $this->_sendToGed($_FILES['myfile']['tmp_name']);
				$model->data[$model->alias]['content'] = null;
			} else if( in_array( $model->alias, array('Armodel', 'Rmodel', 'Gabaritdocument') ) ) {
                $model->data[$model->alias]['content'] = file_get_contents($_FILES['myfile']['tmp_name']);
			} else {
				$model->data[$model->alias]['gedpath'] = null;
//				$model->data[$model->alias]['content'] = file_get_contents($_FILES['myfile']['tmp_name']);
			}
		}
		return true;
	}

	/**
	 *
	 * @param type $path
	 * @return string
	 */
	private function _getFileContentFromGed($path) {
		$filecontent = '';
		//TODO: mettre en place le fonctionnement avec une GED
		$filecontent = "-- fonctionnalité en cours de développement --";
		return $filecontent;
	}

	/**
	 * Récupération du contenu d'un fichier stocké en base ou dans une GED
	 *
	 * @access public
	 * @param integer $id identifiant du document
	 * @throws NotFoundException
	 * @return string
	 */
	public function getFileContent(&$model, $id) {
		if (empty($id)) {
			throw new NotFoundException();
		}

		$filecontent = '';
		if ($this->_checkGedAccess()) {
			//TODO: mettre en place le fonctionnement avec une GED
			$doc = $model->find('first', array('recursive' => -1, 'fields' => array($model->alias . '.gedpath', $model->alias . '.id'), 'conditions' => array($model->alias . '.id' => $id)));
			$filecontent = $this->_getFileContentFromGed($doc[$model->alias]['gedpath']);
		} else {
            if($model->alias == 'Armodel') {
                $doc = $model->find('first', array('recursive' => -1, 'fields' => array($model->alias . '.content', $model->alias . '.id'), 'conditions' => array($model->alias . '.id' => $id)));
            }
            else if($model->alias == 'Rmodel') {
                $doc = $model->find('first', array('recursive' => -1, 'fields' => array($model->alias . '.content', $model->alias . '.id'), 'conditions' => array($model->alias . '.id' => $id)));
            }
            else {
                $doc = $model->find(
                    'first',
                    array(
                        'recursive' => -1,
                        'fields' => array(
                            $model->alias . '.path',
                            $model->alias . '.id'
                        ),
                        'conditions' => array(
                            $model->alias . '.id' => $id
                        )
                    )
                );
                $filecontent = $doc[$model->alias]['path'];
            }
		}
		return $filecontent;
	}

}

?>
