<?php


/**
 * Referentielfantoir model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Referentielfantoir extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Referentielfantoir';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'referentielsfantoir';

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Referentielfantoirdir' => array(
			'className' => 'Referentielfantoirdir',
			'foreignKey' => 'referentielfantoir_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Referentielfantoircom' => array(
			'className' => 'Referentielfantoircom',
			'foreignKey' => 'referentielfantoir_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Referentielfantoirvoie' => array(
			'className' => 'Referentielfantoirvoie',
			'foreignKey' => 'referentielfantoir_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasAndBelongsToMany = array(
		'Recherche' => array(
			'className' => 'Recherche',
			'joinTable' => 'recherches_addressbooks',
			'foreignKey' => 'addressbook_id',
			'associationForeignKey' => 'recherche_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheAddressbook'
		)
	);


	/**
	 *
	 * @param type $filename
	 * @param type $departement_id
	 * @param type $foreign_key
	 * @return boolean
	 */
	public function importReferentiel($filename = null, $departement_id = null, $libelledir = null, $foreign_key = null) {
		$return = array();
		if ($departement_id != null) {
			$dept = substr( $departement_id, 0, -1 );
			$listeDepartements = $this->listeDepartements();
			$libelledir = Hash::get($listeDepartements, $departement_id );



			if ($filename != null && is_file($filename) && is_readable($filename)) {
// 				$cards = $this->_convertVcards(file($filename));
// 				foreach ($cards as $kcard => $card) {
// 					if ($this->_checkVcard($card)) {


                        $referentiel['Referentielfantoir']['name'] = strtoupper( $libelledir );

                        $this->create();
                        $fantoir = $this->save($referentiel);

						//save
						$this->Referentielfantoirdir->begin();
						$this->Referentielfantoirdir->create();
                        $direction = array(
                            'Referentielfantoirdir' => array(
                                'referentielfantoir_id' => $fantoir['Referentielfantoir']['id'],
                                'name' => strtoupper( $libelledir ),
                                'codedpt' => $dept,
                                'codedir' => '0',
                                'libelledir' => strtoupper( $libelledir ),
                                'active' => 1
                            )
                        );

						$savedRefDir = $this->Referentielfantoirdir->save($direction);

						$saved = !empty($savedRefDir);

						if( $saved ) {
							$commune = array(
								'Referentielfantoircom' => array(
									'referentielfantoirdir_id' => $this->Referentielfantoirdir->id,
									'referentielfantoir_id' => $fantoir['Referentielfantoir']['id'],
									'codecom' => '001',
									'clerivoli' => 'P',
									'libellecom' => 'ABZAC',
									'typecom' => 'N',
									'caracrur' => '3',
									'caracpop' => '',
									'popreelle' => '0000523',
									'popapart' => '0000000',
									'popfictive' => '0000000',
									'caracannul' => '',
									'dateannul' => '0000000',
									'datecreation' => '1987001',
									'active' => 1
								)
							);
                            $commune['Referentielfantoircom']['name'] = $commune['Referentielfantoircom']['codecom'].$commune['Referentielfantoircom']['clerivoli'];
							$this->Referentielfantoircom->create();
							$savedRefCom = $this->Referentielfantoircom->save($commune);

							$savedCom = !empty( $savedRefCom );

							$voie = array(
								'Referentielfantoirvoie' => array(
									'referentielfantoircom_id' => $this->Referentielfantoircom->id,
                                    'referentielfantoir_id' => $fantoir['Referentielfantoir']['id'],
									'identvoie' => '1',
									'clerivoli' => '1',
									'codevoie' => '',
									'libellevoie' => '',
									'typecom' => 'N',
									'caracrur' => '',
									'caracvoie' => '1',
									'caracpop' => '',
									'popapart' => '',
									'popfictive' => '',
									'caracannul' => '',
									'dateannul' => '',
									'datecreation' => '',
									'codemajic' => '',
									'typevoie' => '1',
									'caraclieudit' => '',
									'libentiervoie' => '',
									'active' => ''
								)
							);
                            $voie['Referentielfantoirvoie']['name'] = $voie['Referentielfantoirvoie']['identvoie'].$voie['Referentielfantoirvoie']['clerivoli'];
							$this->Referentielfantoirvoie->create();
							$savedRefVoie = $this->Referentielfantoirvoie->save($voie);


							if ($savedRefVoie) {
								$return[] = true;
								$this->Referentielfantoirdir->commit();
							} else {
								$return[] = false;
								$this->Referentielfantoirdir->rollback();
							}
						}

// 					}
// 				}
			}
		}
		return !in_array(false, $return, true);
	}


	/**
	*
	*
	*/

	public function listeDepartements() {
		return array(
			'010' => 'Ain',
			'020' => 'Aisne',
			'030' => 'Allier',
			'040' => 'Alpes de Hautes-Provence',
			'050' => 'Hautes-Alpes',
			'060' => 'Alpes-Maritimes',
			'070' => 'Ardèche',
			'080' => 'Ardennes',
			'090' => 'Ariège',
			'100' => 'Aube',
			'110' => 'Aude',
			'120' => 'Aveyron',
			'130' => 'Bouches-du-Rhône',
			'140' => 'Calvados',
			'150' => 'Cantal',
			'160' => 'Charente',
			'170' => 'Charente-Maritime',
			'180' => 'Cher',
			'190' => 'Corrèze',
			'2A0' => 'Corse-du-Sud',
			'2B0' => 'Haute-Corse',
			'210' => 'Côte-d\'Or',
			'220' => 'Côtes d\'Armor',
			'230' => 'Creuse',
			'240' => 'Dordogne',
			'250' => 'Doubs',
			'260' => 'Drôme',
			'270' => 'Eure',
			'280' => 'Eure-et-Loir',
			'290' => 'Finistère',
			'300' => 'Gard',
			'310' => 'Haute-Garonne',
			'320' => 'Gers',
			'330' => 'Gironde',
			'340' => 'Hérault',
			'350' => 'Ille-et-Vilaine',
			'360' => 'Indre',
			'370' => 'Indre-et-Loire',
			'380' => 'Isère',
			'390' => 'Jura',
			'400' => 'Landes',
			'410' => 'Loir-et-Cher',
			'420' => 'Loire',
			'430' => 'Haute-Loire',
			'440' => 'Loire-Atlantique',
			'450' => 'Loiret',
			'460' => 'Lot',
			'470' => 'Lot-et-Garonne',
			'480' => 'Lozère',
			'490' => 'Maine-et-Loire',
			'500' => 'Manche',
			'510' => 'Marne',
			'520' => 'Haute-Marne',
			'530' => 'Mayenne',
			'540' => 'Meurthe-et-Moselle',
			'550' => 'Meuse',
			'560' => 'Morbihan',
			'570' => 'Moselle',
			'580' => 'Nièvre',
			'590' => 'Nord',
			'600' => 'Oise',
			'610' => 'Orne',
			'620' => 'Pas-de-Calais',
			'630' => 'Puy-de-Dôme',
			'640' => 'Pyrénées-Atlantiques',
			'650' => 'Hautes-Pyrénées',
			'660' => 'Pyrénées-Orientales',
			'670' => 'Bas-Rhin',
			'680' => 'Haut-Rhin',
			'690' => 'Rhône',
			'700' => 'Haute-Saône',
			'710' => 'Saône-et-Loire',
			'720' => 'Sarthe',
			'730' => 'Savoie',
			'740' => 'Haute-Savoie',
			'750' => 'Paris',
			'760' => 'Seine-Maritime',
			'770' => 'Seine-et-Marne',
			'780' => 'Yvelines',
			'790' => 'Deux-Sèvres',
			'800' => 'Somme',
			'810' => 'Tarn',
			'820' => 'Tarn-et-Garonne',
			'830' => 'Var',
			'840' => 'Vaucluse',
			'850' => 'Vendée',
			'860' => 'Vienne',
			'870' => 'Haute-Vienne',
			'880' => 'Vosges',
			'890' => 'Yonne',
			'900' => 'Territoire-de-Belfort',
			'910' => 'Essonne',
			'920' => 'Hauts-de-Seine',
			'930' => 'Seine-Saint-Denis',
			'940' => 'Val-de-Marne',
			'950' => 'Val-d\'Oise'
		);
	}

}

?>
