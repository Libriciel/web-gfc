<?php

App::uses('AppModel', 'Model');

/**
 * DesktopsService model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class DesktopsService extends AppModel {

	public $actsAs = array(
		'DataAcl.DataAcl' => array('type' => 'requester')
	);

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'Service_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'desktop_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Service' => array(
			'className' => 'Service',
			'foreignKey' => 'service_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Desktop' => array(
			'className' => 'Desktop',
			'foreignKey' => 'desktop_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 *
	 * @return null
	 */
	public function parentNode() {
		if (!$this->id && empty($this->data)) {
			return null;
		}
		if (isset($this->data['Service']['service_id'])) {
			$ServiceId = $this->data['Service']['service_id'];
		} else {
			$ServiceId = $this->field('service_id');
		}
		if (!$ServiceId) {
			return null;
		} else {
			return array('Service' => array('id' => $ServiceId));
		}
	}

	/**
	 *
	 * @param type $id
	 * @return null
	 */
	public function getDataRights($id = null) {
		$retval = null;
		if ($id != null) {

		}
		return $retval;
	}

}
