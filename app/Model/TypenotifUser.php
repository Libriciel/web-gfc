<?php

App::uses('AppModel', 'Model');

/**
 * TypenotifUser model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 *
 */
class TypenotifUser extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'TypenotifUser';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'typenotif_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'user_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		)
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Typenotif' => array(
			'className' => 'Typenotif',
			'foreignKey' => 'typenotif_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

}

?>
