<?php

/**
 * Sousoperation model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Sousoperation extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Sousoperation';

    /**
     *
     * @var type
     */
    public $useTable = 'sousoperations';


    /**
     * hasMany
     *
     * @access public
     */

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        'Operation' => array(
            'className' => 'Operation',
            'foreignKey' => 'operation_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Soustype' => array(
            'className' => 'Soustype',
            'foreignKey' => 'soustype_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );

    /**
     * hasAndBelongsToMany
     *
     * @var mixed
     * @access public
     */
    public $hasAndBelongsToMany = array(
        'Desktopmanager' => array(
            'className' => 'Desktopmanager',
            'joinTable' => 'desktopsmanagers_sousoperations',
            'foreignKey' => 'sousoperation_id',
            'associationForeignKey' => 'desktopmanager_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'DesktopmanagerSousoperation'
        ),
        'Intituleagent' => array(
            'className' => 'Intituleagent',
            'joinTable' => 'desktopsmanagers_sousoperations',
            'foreignKey' => 'sousoperation_id',
            'associationForeignKey' => 'intituleagent_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'DesktopmanagerSousoperation'
        )
    );
    public $validate = array(
        'name' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        )
    );

}

?>
