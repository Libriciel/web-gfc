<?php

App::uses('AppModel', 'Model');

/**
 * Sousactivite model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Sousactivite extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'Sousactivite';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'activite_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Activite' => array(
			'className' => 'Activite',
			'foreignKey' => 'activite_id',
			'type' => 'LEFT OUTER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

}

?>
