<?php

/**
 * Sendmail model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Sendmail extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Sendmail';

    /**
     *
     * @var type
     */
    public $useTable = 'sendsmails';
    public $validate = array(
        'email' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'desktop_id' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'courrier_id' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        )
    );

    /**
     *
     * @var type
     */
    public $belongsTo = array(
        'Desktop' => array(
            'className' => 'Desktop',
            'foreignKey' => 'desktop_id',
            'type' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Document' => array(
            'className' => 'Document',
            'foreignKey' => 'document_id',
            'type' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'courrier_id',
            'type' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Templatemail' => array(
            'className' => 'Templatemail',
            'foreignKey' => 'templatemail_id',
            'type' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );

}

?>
