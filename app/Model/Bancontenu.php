<?php

App::uses('AppModel', 'Model');

/**
 * Bancontenu model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Bancontenu extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Bancontenu';

    /**
     *
     * @var type
     */
    public $belongsTo = array(
        'Desktop' => array(
            'className' => 'Desktop',
            'foreignKey' => 'desktop_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Desktopmanager' => array(
            'className' => 'Desktopmanager',
            'foreignKey' => 'desktop_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
//      'User' => array(
//          'className' => 'User',
//          'foreignKey' => 'user_id',
//          'type' => 'INNER',
//          'conditions' => null,
//          'fields' => null,
//          'order' => null
//      ),
        'Bannette' => array(
            'className' => 'Bannette',
            'foreignKey' => 'bannette_id',
            'type' => 'LEFT OUTER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'courrier_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
		'Bancontenuuser' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'type' => 'LEFT OUTER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
    );

    /**
     *
     *
     * etat :
     *      -1 refus
     *      0 validé
     *      1 à traiter
     *      2 terminé
     *
     * @param type $desktop_id
     * @param type $bannette_id
     * @param type $courrier_id
     * @return type
     */
    public function add($desktop_id = null, $bannette_id = null, $courrier_id = null, $read = false, $userId = null) {
        $ret = false;
        if ($desktop_id != null && $bannette_id != null && $courrier_id != null) {
                $content = $this->create();
                $content['Bancontenu']['desktop_id'] = $desktop_id;
                $content['Bancontenu']['bannette_id'] = $bannette_id;
                $content['Bancontenu']['courrier_id'] = $courrier_id;
//                $content['Bancontenu']['user_id'] = $userId;
                $content['Bancontenu']['read'] = $read;
                $ret = $this->save($content);
        }
        return !empty($ret);
    }

    /**
     *
     * @param type $desktop_id
     * @param type $bannette_id
     * @param type $courrier_id
     * @return type
     */
    public function end($desktop_id = null, $bannette_id = null, $courrier_id = null, $userId = null) {
        $ret = false;
        $item2 = $this->find(
            'first',
            array(
                'conditions' => array(
                    'Bancontenu.desktop_id' => $desktop_id,
                    'Bancontenu.bannette_id' => $bannette_id,
                    'Bancontenu.courrier_id' => $courrier_id
                ),
                'contain' => false,
                'order' => array('Bancontenu.created DESC')
            )
        );
        if (!empty($item2)) {
            $item2['Bancontenu']['etat'] = 0;
            $item2['Bancontenu']['user_id'] = $userId;
            $ret = $this->save($item2);
        }
        return !empty($ret);
    }

    /**
     *
     * @param type $courrier_id
     */
    public function fullend($courrier_id = null, $userId = null) {
        $retval = false;
        if ($courrier_id != null) {

            if( Configure::read('Webservice.GRC') ) {
                $courriergrcId = null;
                $flux = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $courrier_id), 'contain' => false, 'recursive' => -1) );
                if( !empty( $flux['Courrier']['courriergrc_id'] ) ) {
                    $courriergrcId = $flux['Courrier']['courriergrc_id'];

                    // On vérifie si la requête existe toujours dans la GRC
                    $this->Connecteur = ClassRegistry::init('Connecteur');
                    $hasGrcActif = $this->Connecteur->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Connecteur.name ILIKE' => '%GRC%',
                                'Connecteur.use_grc' => true
                            ),
                            'contain' => false
                        )
                    );

                    if( !empty( $hasGrcActif )) {
                        $localeo = new LocaleoComponent;
                        $note = 'Le flux a été clos dans web-GFC';
                        $this->Courrier->updateAll(
                            array( 'Courrier.statutgrc' => '50'),
                            array( 'Courrier.id' => $courrier_id)
                        );
                        $doc = array();
						$msg['message'] = '';
						$conn = CakeSession::read('Auth.Collectivite.conn');
                        $attachement = $this->Courrier->Document->find('first', array('conditions' => array('Document.courrier_id' =>  $courrier_id, 'Document.main_doc' => true), 'contain' => false));
                        if( !empty($attachement) ) {
                            $path = $attachement['Document']['path'];
                            if( empty( $path ) ) {
                                $file = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'] . DS . "{$attachement['Document']['name']}";
                                file_put_contents( $file, $attachement['Document']['content'] );
                                $path = $file;
                            }
                        }
                        $retour = $localeo->sendDocGRC( $flux, $path, '50', $note );
                        if( isset( $retour->err_msg ) && !empty($retour->err_msg)) {
                            $msg['message'] .= '<br />Aucun retour disponible.';
                        }
                    }
                }
            }

			$bannetteFin = $this->find(
				'first',
				array(
					'conditions' => array(
						'Bancontenu.etat' => 1,
						'Bancontenu.bannette_id' => 4,
						'Bancontenu.courrier_id' => $courrier_id
					),
					'contain' => false,
					'order' => array('Bancontenu.created DESC')
				)
			);
			if (!empty($bannetteFin)) {
				$this->updateAll(
					array('Bancontenu.user_id' => $userId),
					array(
						'Bancontenu.courrier_id' => $courrier_id,
						'Bancontenu.bannette_id' => 4,
						'Bancontenu.etat' => 1
					)
				);
			}

			if(Configure::read('Courrier.SaveDateCloture')){
				$courrierMetadonnee = [
					'CourrierMetadonnee' => [
						'valeur' => date('d/m/Y'),
						'courrier_id' => $courrier_id,
						'metadonnee_id' => '-1'
					]
				];
				$retval = $this->Courrier->Metadonnee->CourrierMetadonnee->save($courrierMetadonnee);
			}

            if(Configure::read('Vidage.Bannettecopie')) {
                $retval = $this->updateAll(array('Bancontenu.etat' => 2), array('Bancontenu.courrier_id' => $courrier_id));
            }
            else {
                $retval = $this->updateAll(
                    array('Bancontenu.etat' => 2),
                    array(
                        'Bancontenu.courrier_id' => $courrier_id,
                        'Bancontenu.bannette_id' => array(1,2,3,4,5,6) // Toutes sauf la bannette des flux en copie
                    )
                );
            }
        }
        return $retval;
    }

    /**
     *
     * @param type $courrier_id
     */
    public function cancel($courrier_id = null) {
        if ($courrier_id != null) {
            $this->query('update bancontenus set etat = -1 where bannette_id != '. BAN_COPY .' and courrier_id = ' . $courrier_id . ';');
        }
    }

    public function setRead($fluxId, $desktopId) {
        /* $qd = array(
          'recursive' => -1,
          'fields' => array(
          'Bancontenu.id',
          'Bancontenu.read'
          ),
          'conditions' => array(
          'Bancontenu.courrier_id' => $fluxId,
          'Bancontenu.desktop_id' => $desktopId
          )
          );
          $bancontenu = $this->find('first', $qd);

          $this->create();
          $bancontenu['Bancontenu']['read'] = true;

          $this->save($bancontenu); */
        $this->Courrier->Bancontenu->updateAll(
                array('Bancontenu.read' => true), array(
            'Bancontenu.courrier_id' => $fluxId,
            'Bancontenu.desktop_id' => $desktopId
                )
        );
    }

    /**
     * Retourne une sous-requête permettant de cibler la dernière bannette
     * d'un courrier pour une année donnée.
     * * etat :
     *      2 terminé
     * @param type $annee
     * @return type
     */
    public function sqDerniereBannette($courrierIdFied, $etat = null) {
        $courrierIdFied = 'Courrier.id';

        $conditions = array(
            "bancontenus.courrier_id = {$courrierIdFied}"
        );
        if (isset($etat)) {
            if (is_array($etat)) {
                $etat = implode(' ,', $etat);
                $conditions[] = array("bancontenus.etat IN ( {$etat} )");
            } else {
                $conditions[] = array("bancontenus.etat = {$etat}");
            }
        }
        $dbo = $this->getDataSource($this->useDbConfig);
        $table = $dbo->fullTableName($this, false, false);

        return "
                    SELECT {$table}.id
                            FROM {$table}
                            WHERE
                                    {$table}.courrier_id = " . $courrierIdFied . "
                                    AND {$table}.etat != 3
                                    AND {$table}.bannette_id != '7'
                            ORDER BY {$table}.created DESC
                            LIMIT 1
            ";

//        return $this->sq(
//            array(
//                'fields' => array(
//                    'bancontenus.id'
//                ),
//                'alias' => 'bancontenus',
//                'conditions' => $conditions,
//                'order' => array( 'bancontenus.created DESC' ),
//                'limit' => 1
//            )
//        );
    }

    /**
     * Retourne une sous-requête permettant de cibler la dernière bannette
     * d'un courrier pour une année donnée.
     * * etat :
     *      2 terminé
     * @param type $annee
     * @return type
     */
    public function sqDerniereBannetteRecherche($courrierIdFied) {
        $courrierIdFied = 'Courrier.id';

        $dbo = $this->getDataSource($this->useDbConfig);
        $table = $dbo->fullTableName($this, false, false);

        return "
                SELECT {$table}.id
                        FROM {$table}
                        WHERE
                                {$table}.courrier_id = " . $courrierIdFied . "
                        ORDER BY {$table}.created DESC
                        LIMIT 1
        ";

    }

    /**
     *
     * @param type $courrier_id
     */
    public function refus($desktop_id, $courrier_id = null, $user_id = null) {
		if( empty($user_id) ) {
			$user_id = '-1';
		}
        if ($desktop_id != null && $courrier_id != null) {
            $this->query('INSERT INTO bancontenus (courrier_id, desktop_id, etat, read, created, modified, user_id) VALUES ( ' . $courrier_id . ', ' . $desktop_id . ', -1, true, NOW(), NOW(), ' . $user_id. ' )');
        }
    }

    public function getBannetteIdByCourrierIdDesktopId($courrier_id, $desktop_id) {
        if ($courrier_id != null) {
            $bannetteId = '';
            $bancontenu = $this->find('first', array('conditions' => array('Bancontenu.courrier_id' => $courrier_id, 'Bancontenu.desktop_id' => $desktop_id, 'Bancontenu.etat' => 1), 'contain' => false));
            if( !empty($bancontenu) ) {
                $bannetteId = $bancontenu['Bancontenu']['bannette_id'];
            }
            return $bannetteId;
        }
    }


	/**
	 * Retourne une sous-requête permettant de cibler la dernière bannette
	 * d'un courrier pour un état donné.
	 * * etat :
	 *      2 terminé
	 * @param type $annee
	 * @return type
	 */
	public function sqDerniereBannetteWithStateValue($courrierIdFied, $etat ) {
		$courrierIdFied = 'Courrier.id';

		$conditions = array(
			"bancontenus.courrier_id = {$courrierIdFied}",
			"bancontenus.bannette_id != 7",
			"bancontenus.etat != 3"
		);
		if (isset($etat)) {
			if (is_array($etat)) {
				$etat = implode(' ,', $etat);
				$conditions[] = "bancontenus.etat NOT IN ( {$etat} )";
			} else {
				$conditions[] = "bancontenus.etat != {$etat}";
			}
		}
		$dbo = $this->getDataSource($this->useDbConfig);
		$table = $dbo->fullTableName($this, false, false);
//$this->log( $conditions );
		return $this->sq(
            array(
                'fields' => array(
                    'bancontenus.id'
                ),
                'alias' => 'bancontenus',
                'conditions' => $conditions,
                'order' => array( 'bancontenus.created DESC' ),
                'limit' => 1
            )
        );
	}

	/**
	 * Retourne une sous-requête permettant de cibler les dernières bannettes d'un flux
	 * @param string 'Courrier.id'
	 * @return request
	 */
	public function sqDernieresBannettesRecherche($courrierIdFied) {
		$courrierIdFied = 'Courrier.id';

		$dbo = $this->getDataSource($this->useDbConfig);
		$table = $dbo->fullTableName($this, false, false);

		return "
                SELECT {$table}.id
                        FROM {$table}
                        WHERE
                                {$table}.courrier_id = " . $courrierIdFied . "
                                AND {$table}.etat = 1
                                AND {$table}.bannette_id != 7
                                AND {$table}.bannette_id IS NOT NULL
                        ORDER BY {$table}.created DESC
        ";

	}

	/**
	 * Retourne une sous-requête permettant de cibler les dernières bannettes d'un flux pour un profil donné
	 * @param string 'Courrier.id'
	 * @param array $desktopId
	 * @return request
	 */
	public function sqBannettesCourrierForDesktops($courrierIdFied, $desktopsIds) {
		$courrierIdFied = 'Courrier.id';

		$dbo = $this->getDataSource($this->useDbConfig);
		$table = $dbo->fullTableName($this, false, false);

		$conditions = array(
			"bancontenus.courrier_id = {$courrierIdFied}",
			"bancontenus.bannette_id != ".BAN_COPY,
			"bancontenus.bannette_id IS NOT NULL"
		);

		if (isset($desktopsIds)) {
			if (is_array($desktopsIds)) {
				$desktopsIds = implode(' ,', $desktopsIds);
				$conditions[] = "bancontenus.desktop_id IN ( {$desktopsIds} )";
			} else {
				$conditions[] = "bancontenus.desktop_id = {$desktopsIds}";
			}
		}

		return $this->sq(
			array(
				'fields' => array(
					'bancontenus.id'
				),
				'alias' => 'bancontenus',
				'conditions' => $conditions,
				'order' => array( 'bancontenus.created DESC' ),
				'limit' => 1
			)
		);

	}

	/**
	 * Retourne une sous-requête permettant de cibler la dernière bannette
	 * d'un courrier pour une année donnée.
	 * * etat :
	 *      2 terminé
	 * @param type $annee
	 * @return type
	 */
	public function sqDerniereBannetteClos() {
		$courrierIdFied = 'Courrier.id';

		$dbo = $this->getDataSource($this->useDbConfig);
		$table = $dbo->fullTableName($this, false, false);

		return "
                SELECT {$table}.id
                        FROM {$table}
                        WHERE
                                {$table}.courrier_id = " . $courrierIdFied . "
                                AND {$table}.etat = 2
                        ORDER BY {$table}.created DESC
                        LIMIT 1
        ";

	}
}

?>
