<?php

/**
 * Operation model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Operation extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Operation';

    /**
     *
     * @var type
     */
    public $useTable = 'operations';


    /**
     * hasMany
     *
     * @access public
     */

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        'Type' => array(
            'className' => 'Type',
            'foreignKey' => 'type_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );
    public $hasMany = array(
        'Marche' => array(
            'className' => 'Marche',
            'foreignKey' => 'operation_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Consultation' => array(
            'className' => 'Consultation',
            'foreignKey' => 'operation_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Sousoperation' => array(
            'className' => 'Sousoperation',
            'foreignKey' => 'operation_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Intituleagentbyoperation' => array(
            'className' => 'Intituleagentbyoperation',
            'foreignKey' => 'operation_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        )
    );
    public $hasAndBelongsToMany = array(
        'Contact' => array(
            'className' => 'Contact',
            'joinTable' => 'contacts_operations',
            'foreignKey' => 'operation_id',
            'associationForeignKey' => 'contact_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'ContactOperation'
        ),
        'Organisme' => array(
            'className' => 'Organisme',
            'joinTable' => 'organismes_operations',
            'foreignKey' => 'operation_id',
            'associationForeignKey' => 'organisme_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'OrganismeOperation'
        )
    );
    public $validate = array(
        'name' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'subject' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'object' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        )
    );

    /**
     *
     * @param type $params
     */
    public function search($params) {
        if (empty($params)) {
            $conditions = array(
                'Operation.active' => true
            );
        }
//debug($params);
        if (!empty($params)) {
            if (isset($params['Operation']['id']) && !empty($params['Operation']['id'])) {
                $conditions[] = array('Operation.id' => $params['Operation']['id']);
            }
            if (isset($params['Operation']['active'])) {
                $conditions[] = array('Operation.active' => $params['Operation']['active']);
            }
        }

        $querydata = array(
            'conditions' => array(
                $conditions
            ),
            'order' => 'Operation.name ASC',
            'contain' => array(
                'Sousoperation'
            ),
            'limit' => 20000
        );

        return $querydata;
    }

}

?>
