<?php


/**
 * Referentielfantoir model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Referentielfantoirdir extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Referentielfantoirdir';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'referentielsfantoirdir';

	/**
	 *
	 * @var type
	 */
	public $belongsTo = array(
		'Referentielfantoir' => array(
			'className' => 'Referentielfantoir',
			'foreignKey' => 'referentielfantoir_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);



}

?>
