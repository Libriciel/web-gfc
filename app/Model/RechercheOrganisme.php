<?php

App::uses('AppModel', 'Model');

/**
 * RechercheOrganisme model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class RechercheOrganisme extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'RechercheOrganisme';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'recherches_organismes';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'recherche_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'organisme_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		)
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Recherche' => array(
			'className' => 'Recherche',
			'foreignKey' => 'recherche_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Organisme' => array(
			'className' => 'Organisme',
			'foreignKey' => 'organisme_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

}

?>
