<?php

App::uses('AppModel', 'Model');

/**
 * InitServices model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class InitsService extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'InitsService';

    /**
     * Validation rules
     *
     * @access public
     */
    public $validate = array(
        'desktop_id' => array(
            array(
                'rule' => array('numeric'),
                'allowEmpty' => true,
            ),
        ),
        'service_id' => array(
            array(
                'rule' => array('numeric'),
                'allowEmpty' => true,
            ),
        ),
    );

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        'Desktop' => array(
            'className' => 'Desktop',
            'foreignKey' => 'desktop_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Service' => array(
            'className' => 'Service',
            'foreignKey' => 'service_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );

}

?>
