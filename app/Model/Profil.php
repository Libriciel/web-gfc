<?php

/**
 * Profil model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Profil extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Profil';

    /**
     *
     * @var type
     */
    public $actsAs = array('Acl' => array('type' => 'requester'));

    /**
     * Validation rules
     *
     * @var mixed
     * @access public
     */
    public $validate = array(
        'name' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
            array(
                'rule' => array('isunique'),
                'allowEmpty' => true
            )
        ),
    );

    /**
     * hasMany
     *
     * @var mixed
     * @access public
     */
    public $hasMany = array(
        'Desktop' => array(
            'className' => 'Desktop',
            'foreignKey' => 'profil_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        )
    );

    /**
     * parentNode override
     *
     * @return null
     */
    public function parentNode() {
        return null;
    }

    /**
     * Check if the group contains users
     *
     * @param integer $id
     * @return boolean
     */
    public function hasDesktops($id) {
        $count = $this->Desktop->find('count', array('conditions' => array('Desktop.profil_id' => $id)));
        return $count > 0;
    }

    /**
     * Check if the group can be deleted (if empty)
     *
     * @param integer $id
     * @return boolean
     */
    public function isDeletable($id) {
        return !$this->hasDesktops($id);
    }

    /**
     * CakePHP callback afterSave
     * Mise à jour de l'alias des aros
     *
     * @access public
     * @param type $created
     * @return void
     */
    public function afterSave($created, $options = Array()) {
        parent::afterSave($created);
        //mise à jour des AROs
        $aro = $this->Aro->find('first', array('conditions' => array('model' => 'Profil', 'foreign_key' => $this->id)));

        $aro['Aro']['alias'] = $this->field('name');
        $this->Aro->create($aro);
        $this->Aro->save();
    }

    public function getNbProfils() {
        return $this->find('count', array('conditions' => array( 'Profil.name <>' => 'Superadmin')));
    }

}

?>
