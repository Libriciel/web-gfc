<?php

App::uses('AppModel', 'Model');

/**
 * Service model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Service extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'Service';

    /**
     *
     * @var type
     */
//  public $actsAs = array('Tree');
    public $actsAs = array('DataAcl.DataAcl' => array('type' => 'requester'), 'Tree');

    /**
     * Validation rules
     *
     * @access public
     */
    public $validate = array(
        'name' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
        ),
        'email' => array(
            array(
                'rule' => array('email'),
                'allowEmpty' => true
            )
        ),
        'parent_id' => array(
            'rule' => 'checkParadox',
            'on' => 'update',
            'message' => "Un service ne peut pas dépendre de lui même.",
            'allowEmpty' => true, //update
        )
    );

    /**
     * hasAndBelongsToMany
     *
     * @access public
     */
    public $hasAndBelongsToMany = array(
        'Init' => array(
            'className' => 'Desktop',
            'joinTable' => 'inits_services',
            'foreignKey' => 'service_id',
            'associationForeignKey' => 'desktop_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'InitsService'
        ),
        'Desktop' => array(
            'className' => 'Desktop',
            'joinTable' => 'desktops_services',
            'foreignKey' => 'service_id',
            'associationForeignKey' => 'desktop_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'DesktopsService'
        ),
        'Recherche' => array(
            'className' => 'Recherche',
            'joinTable' => 'recherches_services',
            'foreignKey' => 'service_id',
            'associationForeignKey' => 'recherche_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'RechercheService'
        )
    );
    public $hasMany = array(
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'service_creator_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
    );

    /**
     *
     * @var type
     */
//  private $_defaultSaveMsg = array(
//      'state' => 'add',
//      'serviceAdded' => false,
//      'serviceDaroAdded' => false,
//      'serviceDaroRights' => false,
//      'debug' => ''
//  );

    /**
     *
     * @var type
     */
//  public $saveMsg = array();

    /**
     *
     */
//  private function _initSaveMsg() {
//    $this->saveMsg = $this->_defaultSaveMsg;
//  }

    /**
     *
     * @return boolean
     */
//  public function beforeSave() {
//    $this->_initSaveMsg();
//    return true;
//  }

    /**
     *
     * @param type $created
     */
	public function afterSave($created, $options= [])
	{
		parent::afterSave($created);

		$id = $this->id;
		if( !empty( $this->data['Service']['id']) ) {
			$id = $this->data['Service']['id'];
		}

		if (!empty($this->data['Desktop']['Desktop'])) {
			foreach ($this->data['Desktop']['Desktop'] as $i => $desktopId) {
				$DesktopsService = $this->DesktopsService->find('first', array('conditions' => array('DesktopsService.desktop_id' => $desktopId, 'DesktopsService.service_id' => $id)));

				if (empty($DesktopsService)) {
					$newDesktopsService = array(
						'DesktopsService' => array(
							'service_id' => $id,
							'desktop_id' => $desktopId
						)
					);
					$this->DesktopsService->create($newDesktopsService);
					$DesktopsService = $this->DesktopsService->save();
				}


				if (!empty($DesktopsService)) {
					$parent = $this->DesktopsService->Daro->find('first', array('conditions' => array('model' => 'Service', 'foreign_key' => $id)));
					$data = array(
						'parent_id' => isset($parent['Daro']['id']) ? $parent['Daro']['id'] : null,
						'model' => $this->DesktopsService->name,
						'foreign_key' => $DesktopsService['DesktopsService']['id']
					);
					$data['alias'] = $data['model'] . '.' . $data['foreign_key'] . (isset($parent['Daro']['id']) ? '.' . $parent['Daro']['id'] : '');

					$existingDaro = $this->DesktopsService->Daro->find('first', array('conditions' => Set::flatten(array('Daro' => $data))));

					if (!empty($existingDaro)) {
						$data['id'] = $existingDaro['Daro']['id'];
					}
					$this->DesktopsService->Daro->create();
					$this->DesktopsService->Daro->save(array('Daro' => $data));
				}
			}
		}
	}
    /**
     *
     * @param type $data
     * @return boolean
     */
    public function checkParadox($data) {
        if (isset($this->data[$this->alias]['id'])) {
            return $data['parent_id'] != $this->data[$this->alias]['id'];
        }
        return true;
    }

    /**
     * parentNode override
     *
     * @return null
     */
    function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['Service']['parent_id'])) {
            $serviceId = $this->data['Service']['parent_id'];
        } else {
            $serviceId = $this->field('parent_id');
        }
        if (!$serviceId) {
            return null;
        } else {
            return array('Service' => array('id' => $serviceId));
        }
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function getDesktops($id = null) {
        $return = array();
        $services = $this->find('first', array('conditions' => array('Service.id' => $id)));
        foreach ($services['Desktop'] as $desktop) {
            if (!in_array($desktop['id'], $return)) {
                $return[] = $desktop['id'];
            }
        }
        return $return;
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function getUsers($id = null) {
        $users = array();
        $desktops = $this->getDesktops($id);
        foreach ($desktops as $desktop) {
            $desktopUsers = $this->Desktop->getUsers($desktop);
            foreach ($desktopUsers as $user) {
                if (!in_array($user, $users)) {
                    $users[] = $user;
                }
            }
        }
        return $users;
    }

    public function getNbServices() {
        return $this->find('count');
    }

    /**
     * Retourne l'ensemble des mails issus des services
     */
    public function getServiceMails() {
        $emails = array();
        $mails = $this->find(
            'all',
            array(
                'fields' => array('Service.email'),
                'conditions' => array(
                    'Service.active' => true
                ),
                'recursive' => -1,
                'order' => 'Service.email ASC'
            )
        );
        foreach ($mails as $i => $mail) {
            if( !empty($mail['Service']['email'])) {
                $emails[$mail['Service']['email']] = $mail['Service']['email'];
            }
        }
        return $emails;
    }
     /**
     * Retourne l'ensemble des enfants d'un service donné
     * @params : id du service cible
     * return array liste des enfants (cle => nom du service)
     */
    public function getServiceEnfants($id) {
        $servicesEnfants = $this->children($id);
        $enfants = Hash::extract( $servicesEnfants, '{n}.Service.name');
        return $enfants;
    }
}

?>
