<?php

App::uses('AppModel', 'Model');

/**
 * Sequence model class
 * Gestion des compteurs paramétrables.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 *
 * based on webdelib compteurs / sequences
 * @link https://adullact.net/projects/webdelib/
 */
class Sequence extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Sequence';

	/**
	 *
	 * @var type
	 */
	public $displayField = "nom";

	/**
	 *
	 * @var type
	 */
	public $validate = array(
		'nom' => array(
			array(
				'rule' => 'notBlank',
				'message' => 'Entrer un nom pour la séquence'
			)/* ,
		  array(
		  'rule' => 'isUnique',
		  'message' => 'Entrer un autre nom, celui-ci est déjà utilisé.'
		  ) */
		),
		'num_sequence' => array(
			array(
				'rule' => 'numeric',
				'allowEmpty' => true,
				'message' => 'Le numéro de séquence doit être un nombre.'
			)
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasMany = 'Compteur';

	/**
	 *
	 * @var type
	 */
	public $cacheQueries = false;

}

?>
