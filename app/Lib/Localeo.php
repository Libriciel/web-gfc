<?php

/**
 * @author Arnaud AUZOLAT <arnaud.auzolat@libriciel.coop>
 * @editor Libriciel SCOP
 *
 * @created 19 sept 2018
 *
 *
 */
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('Deliberation', 'Model');
App::uses('Collectivite', 'Model');
App::uses('Courrier', 'Model'); // WebGFC
App::uses('LocaleoComponent', 'Controller/Component');

class Localeo  {

    /**
     * @var Composant IparapheurComponent
     */
    private $Localeo;


    /**
     * Appelée lors de l'initialisation de la librairie
     * Charge le bon protocol de signature et initialise le composant correspondant
     */
    function __construct($connecteur) {
        $this->collection = new ComponentCollection();

        if(!empty($connecteur))
            $this->connecteur = $connecteur;
        else $this->connecteur = NULL;

        $this->Courrier = new Courrier;
    }

    /**
     * @param array $flux
     * @param int|string $circuit_id
     * @param string $document contenu du fichier du document principal
     * @param array $annexes (content, filename, mimetype)
     * @return bool|int false si echec sinon identifiant iparapheur
     */
    public function echoTest($flux, $circuit_id, $document, $annexes = array()) {
        $this->Connecteur = ClassRegistry::init('Connecteur');
        $hasParapheurActif = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true,
					'Connecteur.signature_protocol'=> 'IPARAPHEUR'
                ),
                'contain' => false
            )
        );

        if (is_numeric($circuit_id)) {
            $circuits = $this->listCircuitsIparapheur();
            $libelleSousType = $circuits[$circuit_id];
        } else {
            $libelleSousType = $circuit_id;
        }
        $targetName = $flux[CAKEFLOW_TARGET_MODEL]['name'];
        $date_limite = null;

        $ret = $this->Iparapheur->echoTest(
            $targetName,
            $this->parapheur_type,
            $libelleSousType,
            $this->visibility,
            $document,
            $annexes,
            $date_limite,
            $hasParapheurActif
        );

        return $ret;
    }

    /**
     * @param int $id
     */
    public function createRequestGRC($data) {
echo 'toto';
        $this->Localeo->createRequestGRC($data);
    }


}
