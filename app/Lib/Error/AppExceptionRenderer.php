<?php

App::uses('ExceptionRenderer', 'Error');

/**
 *
 * AppException Renderer class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 */
class AppExceptionRenderer extends ExceptionRenderer {

	/**
	 *
	 * @param CakeException $error
	 */
	public function error400($error) {
		$message = __d('cake', $error->getMessage());
		if (Configure::read('debug') == 0 && $error instanceof CakeException) {
			$message = __d('cake', 'Not Found');
		}
		$url = $this->controller->request->here();
		$this->controller->response->statusCode($error->getCode());
		$this->controller->set(array(
			'name' => $message,
			'url' => h($url),
			'error' => $error,
			'_serialize' => array('name', 'url')
		));
		$this->_outputMessage('error400');
	}

	/**
	 *
	 * @param type $template
	 */
	function _outputMessage($template) {
		$this->controller->layout = 'error';
		parent::_outputMessage($template);
	}

}

?>
