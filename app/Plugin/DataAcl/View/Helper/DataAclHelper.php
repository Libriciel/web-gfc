<?php

/**
 *
 * DataAcl helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		DataAcl.app
 * @subpackage	DataAcl.view.helper
 */
class DataAclHelper extends Helper {

    /**
     *
     * @var type
     */
    private $_level;

    /**
     *
     * @var type
     */
    private $_cssParentClasses;

    /**
     *
     * @var type
     */
    private $_defaultSettings = array(
        'fields' => array(
            'ressource' => 'Ressource',
            'create' => 'C',
            'read' => 'R',
            'update' => 'U',
            'delete' => 'D'
        ),
        'trOptions' => array(),
        'thOptions' => array('style' => 'text-align: center;'),
        'tableLevelPadding' => 2,
        'yes' => '<img src="/DataAcl/img/test-pass-icon.png" alt="yes" style="width: 16px;" />',
        'no' => '<img src="/DataAcl/img/test-fail-icon.png" alt="no" style="width: 16px;" />',
        'edit' => false,
        'requester' => array(),
        'aclType' => '',
        'formName' => '',
        'levelMarks' => array(
        /*
          '&therefore; ',
          '&bull; ',
          '&loz; ',
          '&diams; ',
          '&rsaquo; ', */
        ),
        'parentNodeMarkOpen' => '<img src="/DataAcl/img/parent_node_mark_close.png" alt="+ " style="width: 16px;vertical-align: middle;" />',
        'parentNodeMarkClose' => '<img src="/DataAcl/img/parent_node_mark_open.png" alt="- " style="width: 16px;vertical-align: middle;" />',
        'tableRootStyle' => '',
        'tableRootClass' => '',
        'fieldsetStyle' => '',
        'fieldsetClass' => '',
        'fieldsetLegend' => '',
        'legendClass' => '',
        'legendStyle' => '',
    );

    /**
     *
     * @var type
     */
    private $_settings;

    /**
     *
     * @var type
     */
    public $helpers = array('Html', 'Form');

    /**
     *
     * @param type $rights
     * @param type $externRoot
     * @param type $options
     * @return type
     */
    public function drawTable($rights = array(), $externRoot = false, $options = array()) {
        if (empty($rights)) {
            return __d('cake.dev', 'No right defined.');
        }

        $this->_level = 0;
        $this->_cssParentClasses = array();
        $this->_settings = Set::merge($this->_defaultSettings, $options);

        $tmp = array_values($rights);
        $CRUDkeys = array_keys($tmp[0]['checks']);
        if (!in_array('create', $CRUDkeys)) {
            unset($this->_settings['fields']['create']);
        }
        if (!in_array('read', $CRUDkeys)) {
            unset($this->_settings['fields']['read']);
        }
        if (!in_array('update', $CRUDkeys)) {
            unset($this->_settings['fields']['update']);
        }
        if (!in_array('delete', $CRUDkeys)) {
            unset($this->_settings['fields']['delete']);
        }

        if ($this->_settings['edit'] && !empty($this->_settings['requester']['model'])) {
            echo $this->Form->create($this->_settings['formName']);
        }

        echo '<fieldset' . (!empty($this->_settings['fieldsetStyle']) ? ' style="' . $this->_settings['fieldsetStyle'] . '"' : '') . (!empty($this->_settings['fieldsetClass']) ? ' class="' . $this->_settings['fieldsetClass'] . '"' : '') . '>';
        if (!empty($this->_settings['fieldsetLegend'])) {
            echo '<legend>&nbsp;' . $this->_settings['fieldsetLegend'] . '&nbsp;</legend>';
        }

        if ($this->_settings['edit'] && !empty($this->_settings['requester']['model'])) {
            echo $this->Form->input('Acl.Type', array('type' => 'hidden', 'label' => array('class' => 'control-label  col-sm-5'), 'value' => $this->_settings['aclType']));
            echo $this->Form->input('Requester.' . $this->_settings['requester']['model'], array('type' => 'hidden', 'value' => $this->_settings['requester']['foreign_key']));
        }

        $content = $this->Html->tableHeaders($this->_settings['fields'], $this->_settings['trOptions'], $this->_settings['thOptions']);
        $content .= $this->_drawRows($rights, $this->_settings['edit'], $externRoot);
        echo $this->Html->tag('table', $content, array('style' => 'margin-right: 0;'));

        if ($this->_settings['edit'] && !empty($this->_settings['requester']['model']) && empty($this->_settings['noSubmit'])) {
            echo $this->Form->end(__('submit'));
        }
        echo '</fieldset>';
    }

    /**
     *
     * @param type $line
     * @return type
     */
    private function _drawRows($rights = array(), $edit = false, $extern = false) {
        if (empty($rights)) {
            return __d('cake.dev', 'No right defined.');
        }

        $content = '';
        foreach ($rights as $kline => $line) {
            if ($extern) {
                $this->_drawTableRoot($line, $edit);
            } else {
                $printName = '';
                if (!empty($line['children'])) {
                    $printName .= $this->Html->tag('span', $this->_settings['parentNodeMarkClose'], array('class' => 'CloseMark', 'status' => 'opened', 'style' => 'display: none;'));
                    $printName .= $this->Html->tag('span', $this->_settings['parentNodeMarkOpen'], array('class' => 'OpenMark', 'status' => 'opened', 'style' => 'display: none;'));
                }
                $printName .= $this->Html->tag('span', /* $this->_level > 4 ? $this->_settings['levelMarks'][4] : $this->_settings['levelMarks'][$this->_level] */ '', array('class' => 'levelMark', 'level' => $this->_level));


                $printName .=!empty($line['object']) ? $line['object'] : $line['alias'];
                if (preg_match("#^controllers.*/(.*)$#", $printName, $matches)) {
                    $printName = $matches[1];
                }
                $actions = $edit ? $this->_drawFormActions($line) : $this->_drawActions($line);

                $classes = '';
//				$classes = 'rowlevel' . $this->_level . ' row row_' . $kline;
                $classes .=!empty($line['children']) ? ' parent' : '';
                foreach ($this->_cssParentClasses as $parentClass) {
                    $classes .= ' parent_' . $parentClass;
                }
                $content .= $this->Html->tag('tr', $this->Html->tag('td', __d('data_acl', $printName), array('style' => 'padding-left: ' . (string) ($this->_level * $this->_settings['tableLevelPadding']) . 'em;')) . $actions, array('parent' => end($this->_cssParentClasses), 'row' => $kline, 'class' => $classes));
            }
            if (!empty($line['children'])) {
                if (!$extern) {
                    $this->_level++;
                    $this->_cssParentClasses[] = $kline;
                }
                $content .= $this->_drawRows($line['children'], $edit);
                if (!$extern) {
                    $this->_level--;
                    array_pop($this->_cssParentClasses);
                }
            }
        }

        return $content;
    }

    /**
     *
     * @param type $line
     */
    private function _drawTableRoot($line, $edit = false) {
        $printName = isset($line['object']) && !empty($line['object']) ? $line['object'] : $line['alias'];
        echo $this->Html->tag('div', __d('data_acl', $printName) . ($edit ? $this->_drawFormControls($line) : $this->_drawControls($line)), array('style' => 'margin-right: 0;' . (!empty($this->_settings['tableRootStyle']) ? ' ' . $this->_settings['tableRootStyle'] : ''), 'class' => 'tableRoot' . (!empty($this->_settings['tableRootClass']) ? ' ' . $this->_settings['tableRootClass'] : '' )));
    }

    /**
     *
     * @param type $line
     * @return type
     */
    private function _drawControls($line) {
        $chks = $line['checks'];
        $content = '';
        foreach ($chks as $kChk => $chk) {
            $value = $this->_settings['fields'][$kChk];
            $value .= $chk . ' ' . ($chk ? $this->_settings['yes'] : $this->_settings['no']);
            $content .= $this->Html->tag('span', $value, array('style' => 'padding: 0.6em;', 'class' => 'action'));
        }
        return $this->Html->tag('span', $content, array('style' => 'float: right;'));
    }

    /**
     *
     * @param type $line
     * @return type
     */
    private function _drawFormControls($line) {
        $chks = $line['checks'];
        $content = '';
        foreach ($chks as $kChk => $chk) {
            $content .= $this->Html->tag('span', $this->_settings['fields'][$kChk] . ' ' . $this->_genChkbox($line, $kChk, $chk), array('style' => 'padding: 0.6em;', 'class' => 'action'));
        }
        return $this->Html->tag('span', $content, array('style' => 'float: right;'));
    }

    /**
     *
     * @param type $line
     * @return type
     */
    private function _drawActions($line) {
        $chks = $line['checks'];
        $content = '';
        foreach ($chks as $kChk => $chk) {
            $content .= $this->Html->tag('td', $chk ? $this->_settings['yes'] : $this->_settings['no'], array('style' => 'text-align: center;width: 6em', 'class' => 'action'));
        }
        return $content;
    }

    /**
     *
     * @param type $line
     * @return type
     */
    private function _drawFormActions($line) {
        $chks = $line['checks'];
        $content = '';
        foreach ($chks as $kChk => $chk) {
            $content .= $this->Html->tag('td', $this->_genChkbox($line, $kChk, $chk), array('style' => 'text-align: center;width: 6em', 'class' => 'action'));
        }
        return $content;
    }

    /**
     *
     * @param type $line
     * @param type $key
     * @return string
     */
    private function _genActionName($line, $key) {
        $chkboxFormName = implode('__', explode('/', $line['alias']));
        if (!empty($line['model']) && !empty($line['foreign_key'])) {
            $chkboxFormName = $line['model'] . '.' . $line['foreign_key'];
        }
        $chkboxFormName .= '.' . $key;
        return $chkboxFormName;
    }

    /**
     *
     * @param type $line
     * @param type $key
     * @param type $checked
     * @return type
     */
    private function _genChkbox($line, $key, $checked) {
        $name = $this->_genActionName($line, $key);
        $options = array(
            'type' => 'checkbox',
            'div' => false,
            'label' => false,
            'checked' => $checked === true ? 'checked' : '',
            'style' => 'text-align: center;width: 6em;clear: none;float: none;',
            'key' => $key,
            'verif' => $checked === true
        );

        return $this->Form->input('Rights.' . $name, $options);
    }

}

?>
