/**
 * DataAcl helping functions
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */

/**
 *
 * @param {type} e
 * @returns {undefined}
 */
var avoidEvent = function(e) {
	e.stopPropagation();
	e.preventDefault();
};

/**
 *
 * @param {type} element
 * @returns {undefined}
 */
function setCheckBoxInactive(element) {
	element.bind('click', avoidEvent);
	element.attr('data-active', '0');

	$('img', $(element).parent()).remove();
	$(element).after($('<img src="/DataAcl/img/unlock.png" alt="unlock" title="unlock" />').addClass('acl_table_unlock').click(function() {
		setCheckBoxActive($('input[type=checkbox]', $(this).parent()));
	}));

}

/**
 *
 * @param {type} element
 * @returns {undefined}
 */
function setCheckBoxActive(element) {
	element.unbind('click', avoidEvent);
	$('img', $(element).parent()).remove();
	element.attr('data-active', '1');
}

/**
 * Apply checked/unchecked state from an element's checkbox to his children's checkboxes
 *
 * @param {type} element
 * @param {type} tableElement
 * @returns {undefined}
 */
function applyToChildren(element, tableElement) {
	var line = $(element).parent().parent();
	var row = line.attr('row');
	var key = $(element).attr('key');
	var chk = $(element).attr('checked');

	if (typeof $(element).attr('data-active') !== 'undefined' && $(element).attr('data-active') === '1') {
		$('.parent_' + row + ' .action input[type=checkbox]', tableElement).each(function() {
			if ($(this).attr('key') === key) {
				if (chk === 'checked') {
					$(this).attr('checked', 'checked');
				} else {
					$(this).removeAttr('checked');
				}
			}
		});
	}
}

/**
 * Apply checked/unchecked state from root element's checkbox to all children's checkboxes
 *
 * @param {type} element
 * @param {type} tableElement
 * @returns {undefined}
 */
function rootApplyToChildren(element, tableElement) {
	var key = $(element).attr('key');
	var chk = $(element).attr('checked');

	if (typeof $(element).attr('data-active') !== 'undefined' && $(element).attr('data-active') === '1') {
		$('tr .action input[type=checkbox]', tableElement).each(function() {
			if ($(this).attr('key') === key) {
				setCheckBoxActive($(this));
				if (chk === 'checked') {
					$(this).attr('checked', 'checked');
				} else {
					$(this).removeAttr('checked');
				}
			}
		});
	}
}

/**
 *
 * @param {type} element
 * @param {type} tableElement
 * @returns {undefined}
 */
function applyToParent(element, tableElement) {
	var line = $(element).parent().parent();
	var parentRow = line.attr('parent');
	var key = $(element).attr('key');

	var values = new Array();
	$('tr.parent_' + parentRow + ' .action input[type=checkbox]', $(tableElement)).each(function() {
		if ($(this).attr('key') === key) {
			values.push($(this).attr('checked'));
		}
	});

	var allChecked = true;
	var allUnChecked = true;
	for (var i in values) {
		if (values[i] !== 'checked') {
			allChecked = false;
		}
		if (values[i] === 'checked') {
			allUnChecked = false;
		}
	}

	if (!allChecked && !allUnChecked) {
		$('tr.row_' + parentRow + ' .action input[type=checkbox]', $(tableElement)).each(function() {
			if ($(this).attr('key') === key) {
				setCheckBoxInactive($(this));
				$(this).attr('checked', 'checked');
				applyToParent($(this), tableElement);
			}
		});
	} else if (allChecked) {
		$('tr.row_' + parentRow + ' .action input[type=checkbox]', $(tableElement)).each(function() {
			if ($(this).attr('key') === key) {
				setCheckBoxActive($(this));
				$(this).attr('checked', 'checked');
				applyToParent($(this), tableElement);
			}
		});
	} else if (allUnChecked) {
		$('tr.row_' + parentRow + ' .action input[type=checkbox]', $(tableElement)).each(function() {
			if ($(this).attr('key') === key) {
				setCheckBoxActive($(this));
				applyToParent($(this), tableElement);
			}
		});
	}
}

/**
 *
 * @param {type} tableElement
 * @param {type} key
 * @returns {undefined}
 */
function rootApplyToParent(tableElement, key) {
	var keys = new Array();
	if (key === 'all') {
		$('.tableRoot input[type=checkbox]').each(function() {
			keys.push($(this).attr('key'));
		});
	} else if (key !== '') {
		keys.push(key);
	}

	for (k in keys) {
		var rootChk = $('.tableRoot input[key=' + keys[k] + ']', $(tableElement).parent());
		var values = new Array();
		$('input[key=' + keys[k] + ']', $(tableElement)).each(function() {
			values.push($(this).attr('checked'));
		});

		var allChecked = true;
		var allUnChecked = true;
		for (i in values) {
			if (values[i] !== 'checked') {
				allChecked = false;
			}
			if (values[i] === 'checked') {
				allUnChecked = false;
			}
		}

		if (!allChecked && !allUnChecked) {
			setCheckBoxInactive($(rootChk));
			$(rootChk).attr('checked', 'checked');
		} else if (allChecked) {
			setCheckBoxActive($(rootChk));
			$(rootChk).attr('checked', 'checked');
		} else if (allUnChecked) {
			setCheckBoxActive($(rootChk));
			$(rootChk).removeAttr('checked');
		}
	}
}

/**
 *
 * @param {type} tableElement
 * @returns {undefined}
 */
function initCheck(tableElement) {
	if ($('input[type=checkbox]', tableElement).length > 0) {
		$('.parent .action input[type=checkbox]', $(tableElement)).click(function() {
			applyToChildren($(this), tableElement);
		});

		$('tr:not(.parent) .action input[type=checkbox]', $(tableElement)).each(function() {
			applyToParent($(this), tableElement);
		}).click(function() {
			applyToParent($(this), tableElement);
		});

		rootApplyToParent(tableElement, 'all');
		$('.row .action input[type=checkbox]', $(tableElement)).click(function() {
			rootApplyToParent(tableElement, $(this).attr('key'));
		});

		$('.tableRoot input[type=checkbox]', $(tableElement).parent()).click(function() {
			rootApplyToChildren($(this), tableElement);
		});
	}
}

/**
 * Fold children elements
 *
 * @param {type} row
 * @returns {undefined}
 */
function fold(row) {
	var id = $(row).attr('row');
	var table = $(row).parents('table');
	$('.parent_' + id, table).show();
	$('.CloseMark', $(row)).show();
	$('.OpenMark', $(row)).hide();
	$('table .parent .levelMark').hide();
	$('td:first', row).unbind('click').click(function() {
		unfold(row);
	});
}

/**
 *	Unfold children elements
 *
 * @param {type} row
 * @returns {undefined}
 */
function unfold(row) {
	var id = $(row).attr('row');
	var table = $(row).parents('table');
	$('.parent_' + id, table).hide();
	$('.CloseMark', $(row)).hide();
	$('.OpenMark', $(row)).show();
	$('table .parent .levelMark').hide();
	$('td:first', row).unbind('click').click(function() {
		fold(row);
	});
}

/**
 *
 * @param {type} tableElement
 * @param {type} open
 * @returns {undefined}
 */
function initFolding(tableElement, open) {
	if ($('.parent', tableElement).length > 0) {
		$('.tableRoot').hover(function() {
			$(this).addClass('ui-state-focus');
			$('tr.row', tableElement).addClass('ui-state-highlight');
		}, function() {
			$(this).removeClass('ui-state-focus');
			$('tr.row', tableElement).removeClass('ui-state-highlight');
		});

		$('tr.row', $(tableElement)).hover(function() {
			$(this).addClass('ui-state-focus');
			$('.parent_' + $(this).attr('row'), tableElement).addClass('ui-state-highlight');
			$('.row_' + $(this).attr('parent'), tableElement).addClass('ui-state-highlight');
		}, function() {
			$(this).removeClass('ui-state-focus');
			$('.parent_' + $(this).attr('row'), tableElement).removeClass('ui-state-highlight');
			$('.row_' + $(this).attr('parent'), tableElement).removeClass('ui-state-highlight');
		});

		$('.parent td:first', $(tableElement)).css('cursor', 'pointer');
		$('.parent', $(tableElement)).each(function() {
			if (open) {
				unfold(this);
			} else {
				fold(this);
			}
		});

		tableElement.before($('<span></span>').html($('.CloseMark:first').html()).click(function() {
			$('tr.parent', tableElement).each(function() {
				unfold($(this));
			});
		})).before($('<span>aze</span>').html($('.OpenMark:first').html()).click(function() {
			$('tr.parent', tableElement).each(function() {
				fold($(this));
			});
		}));
	} else {
		$('tr.row', $(tableElement)).hover(function() {
			$(this).addClass('ui-state-focus');
		}, function() {
			$(this).removeClass('ui-state-focus');
		});
	}
}

/**
 *
 * @param {type} fold
 * @returns {undefined} */
function initTables(fold) {
	$('table').addClass('acl_table').each(function() {
		if ($(this).attr('data-init') !== "true") {
			initFolding($(this), fold);
			initCheck($(this));
			$(this).attr('data-init', 'true');
		}
	});
}
