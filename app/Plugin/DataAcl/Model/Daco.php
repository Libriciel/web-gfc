<?php

/**
  * DataAcl Daco model class
 *
 * DataAcl : CakePHP Data Acl plugin
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		DataAcl
 * @subpackage		DataAcl.Model
 */
App::uses('AppModel', 'Model');
App::uses('AclNode', 'Model');

/**
 * Access Control Object
 *
 * @package       Cake.Model
 */
class Daco extends DataAclNode {

	/**
	 * Model name
	 *
	 * @var string
	 */
	public $name = 'Daco';

	/**
	 *
	 * @var type
	 */
	public $useTable = "dacos";

	/**
	 *
	 * @var type
	 */
	public $alias = "Daco";

	/**
	 * Binds to ARO nodes through permissions settings
	 *
	 * @var array
	 */
	public $hasAndBelongsToMany = array('Daro' => array('className' => 'DataAcl.Daro', 'with' => 'DataAcl.Dpermission'));

	/**
	 *
	 * @param type $dacoId
	 * @return null
	 */
	public function initRights($dacoId = null) {
		$defaultValue = Configure::read('DataAcl.defaultAccessValue.Daco');
		if ($defaultValue == null) {
			$defaultValue = 1;
		}

		if ($dacoId != null) {
			$this->id = $dacoId;
		}

		if (!$this->id) {
			return null;
		}

		$parent_id = $this->field('parent_id');
		$result = array();


		if (empty($parent_id)) {
			$daros = $this->Daro->find('list', array('conditions' => array('parent_id' => null)));
			foreach ($daros as $daro_id) {
				$data = array(
					'DarosDaco' => array(
						'daro_id' => $daro_id,
						'daco_id' => $this->id,
						'_create' => $defaultValue,
						'_read' => $defaultValue,
						'_update' => $defaultValue,
						'_delete' => $defaultValue,
					)
				);
				$this->Dpermission->create();
				$this->Dpermission->save($data);
			}
		}
		return !in_array(false, $result, true);
	}

}
