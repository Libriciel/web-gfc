<?php

App::uses('AppModel', 'Model');
App::uses('AclNode', 'Model');

/**
 * DataAcl Node model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * DataAcl : CakePHP Data Acl plugin
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		DataAcl
 * @subpackage		DataAcl.Model
 */
class DataAclNode extends AclNode {

	/**
	 * Constructor
	 *
	 */
	public $actsAs = array('Tree' => array('type' => 'nested'));

	/**
	 *
	 */
	public function __construct() {
		$config = Configure::read('DataAcl.database');
		if (isset($config)) {
			$this->setDataSource($config);
		}
		parent::__construct();
	}

	/**
	 *
	 * @param type $ref
	 * @return type
	 */
	public function node($ref = null) {
		return parent::node($ref);
	}

}
