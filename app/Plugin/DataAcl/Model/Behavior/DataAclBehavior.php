<?php

App::uses('AclNode', 'Model');
App::uses('AclBehavior', 'Model/Behavior');

/**
 * DataAcl behavior class
 *
 * DataAcl : CakePHP Data Acl plugin
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		DataAcl
 * @subpackage		DataAcl.Model.Behavior
 */
class DataAclBehavior extends AclBehavior {

	/**
	 * Sets up the configuration for the model, and loads ACL models if they haven't been already
	 *
	 * @param Model $model
	 * @param array $config
	 * @return void
	 */
	public function setup(Model $model, $config = Array()) {
		if (isset($config[0])) {
			$config['type'] = $config[0];
			unset($config[0]);
		}
		$this->settings[$model->name] = array_merge(array('type' => 'controlled'), $config);
		$this->settings[$model->name]['type'] = strtolower($this->settings[$model->name]['type']);

		$types = $this->_typeMaps[$this->settings[$model->name]['type']];

		if (!is_array($types)) {
			$types = array($types);
		}
		foreach ($types as $type) {
			list($plugin, $name) = pluginSplit($type);
			$model->{$name} = ClassRegistry::init($type);
		}
		if (!method_exists($model, 'parentNode')) {
			trigger_error(__d('cake_dev', 'Callback parentNode() not defined in %s', $model->alias), E_USER_WARNING);
		}
	}

	/**
	 * Maps DataAcl type options to DataAcl models
	 *
	 * @var array
	 */
	protected $_typeMaps = array('requester' => 'DataAcl.Daro', 'controlled' => 'DataAcl.Daco', 'both' => array('DataAcl.Daro', 'DataAcl.Daco'));

	/**
	 *
	 * @param type $model
	 * @param type $ref
	 * @param type $type
	 * @return null
	 */
	public function node(Model $model, $ref = null, $type = null) {
		if (empty($type)) {
			$type = $this->_typeMaps[$this->settings[$model->name]['type']];
			if (is_array($type)) {
				trigger_error(__d('cake_dev', 'AclBehavior is setup with more then one type, please specify type parameter for node()'), E_USER_WARNING);
				return null;
			}
		}
		if (empty($ref)) {
			$ref = array('model' => $model->name, 'foreign_key' => $model->id);
		}
		list( $pluginName, $type ) = pluginSplit($type);
		return $model->{$type}->node($ref);
	}

	/**
	 * Creates a new ARO/ACO node bound to this record
	 *
	 * @param Model $model
	 * @param boolean $created True if this is a new record
	 * @return void
	 */
	public function afterSave(Model $model, $created, $options= Array()) {
		$types = $this->_typeMaps[$this->settings[$model->name]['type']];
		if (!is_array($types)) {
			$types = array($types);
		}
		foreach ($types as $type) {
			list ($pluginName, $alias) = pluginSplit($type);
			$parent = $model->parentNode();
			if (!empty($parent)) {
				$parent = $this->node($model, $parent, $type);
			}

			$data = array(
				'parent_id' => isset($parent[0][$alias]['id']) ? $parent[0][$alias]['id'] : null,
				'model' => $model->name,
				'foreign_key' => $model->id
			);
			$data['alias'] = $this->genAlias($model, $data);

			if (!$created) {
				$node = $this->node($model, null, $type);
				$data['id'] = isset($node[0][$alias]['id']) ? $node[0][$alias]['id'] : null;
			}
			$model->{$alias}->create();
			$model->{$alias}->save($data);
			if ($created) {
				$model->{$alias}->initRights($model->{$alias}->id);
			}
		}
	}

	/**
	 * Destroys the ARO/ACO node bound to the deleted record
	 *
	 * @param Model $model
	 * @return void
	 */
	public function afterDelete(Model $model) {
		$types = $this->_typeMaps[$this->settings[$model->name]['type']];
		if (!is_array($types)) {
			$types = array($types);
		}
		foreach ($types as $type) {
			list ($pluginName, $type) = pluginSplit($type);
			$node = Set::extract($this->node($model, null, $type), "0.{$type}.id");
			if (!empty($node)) {
				$model->{$type}->delete($node);
			}
		}
	}

	/**
	 *
	 * @param type $model
	 * @param type $node
	 * @return null
	 */
	public function genAlias($model, $node = array()) {

		if (isset($node[0]['Daro']) && !empty($node[0]['Daro'])) {
			$node = $node[0]['Daro'];
		} else if (isset($node[0]['Daco']) && !empty($node[0]['Daco'])) {
			$node = $node[0]['Daco'];
		}

		if (empty($node) || !isset($node['foreign_key'])) {
			return null;
		}

		$nameParts = array(
			'name' => Inflector::slug(isset($model->displayField) ? $model->field($model->displayField, array($model->alias . '.' . $model->primaryKey => $node['foreign_key'])) : ($model && $model->hasField('name', true)) ? $model->field('name', array($model->alias . '.' . $model->primaryKey => $node['foreign_key'])) : $model->alias),
			'id' => $node['foreign_key'],
			'parent_id' => (!empty($node['parent_id']) ? $node['parent_id'] : '')
		);
		return trim(implode(".", $nameParts), '.');
	}

}
