<?php

App::uses('AppController', 'Controller');

/**
 * CakePHP Data Acl plugin
 * DataAcl controller app class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		DataAcl
 * @subpackage		DataAcl.Controller
 */
class DataAclAppController extends AppController {

	/**
	 *
	 * @param type $var
	 */
	public function dbg($var) {
		if (Configure::read('debug') > 2) {
			$this->log(var_export($var, true), 'debug');
		}
	}

}

