<?php

App::uses('Component', 'Controller');
App::uses('AclComponent', 'Controller/Component');
App::uses('AclInterface', 'Controller/Component');

/**
 * DataAcl component app class
 *
 * DataAcl : CakePHP Data Acl plugin
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		DataAcl
 * @subpackage		DataAcl.Controller.Component
 */
class DataAclComponent extends AclComponent {

    /**
     * Instance of an ACL class
     *
     * @var AclInterface
     */
    protected $_Instance = null;

    /**
     * Daro object.
     *
     * @var string
     */
    public $Daro;

    /**
     * Daco object
     *
     * @var string
     */
    public $Daco;

    /**
     * Constructor. Will return an instance of the correct ACL class as defined in `Configure::read('DataAcl.classname')`
     *
     * @param ComponentCollection $collection
     * @param array $settings
     * @throws CakeException when DataAcl.classname could not be loaded.
     */
    public function __construct(ComponentCollection $collection, $settings = array()) {
        parent::__construct($collection, $settings);
        $name = Configure::read('DataAcl.classname');
        $this->adapter($name);
    }

    /**
     * Sets or gets the Adapter object currently in the AclComponent.
     *
     * `$this->Acl->adapter();` will get the current adapter class while
     * `$this->Acl->adapter($obj);` will set the adapter class
     *
     * Will call the initialize method on the adapter if setting a new one.
     *
     * @param mixed $adapter Instance of AclBase or a string name of the class to use. (optional)
     * @return mixed either null, or instance of AclBase
     * @throws CakeException when the given class is not an AclBase
     */
    public function adapter($adapter = null) {
        if ($adapter) {
            if (is_string($adapter)) {
                $adapter = new $adapter();
            }
            if (!$adapter instanceof AclInterface) {
                throw new CakeException(__d('cake_dev', 'AclComponent adapters must implement AclInterface'));
            }
            $this->_Instance = $adapter;
            $this->_Instance->initialize($this);
            return;
        }
        return $this->_Instance;
    }

    /**
     * Pass-thru function for ACL check instance.  Check methods
     * are used to check whether or not an ARO can access an ACO
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success
     */
    public function check($daro, $daco, $action = "*") {
        return $this->_Instance->check($daro, $daco, $action);
    }

    /**
     * Pass-thru function for ACL allow instance. Allow methods
     * are used to grant an ARO access to an ACO.
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success
     */
    public function allow($daro, $daco, $action = "*") {
        return $this->_Instance->allow($daro, $daco, $action);
    }

    /**
     * Pass-thru function for ACL deny instance. Deny methods
     * are used to remove permission from an ARO to access an ACO.
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success
     */
    public function deny($daro, $daco, $action = "*") {
        return $this->_Instance->deny($daro, $daco, $action);
    }

    /**
     * Pass-thru function for ACL inherit instance. Inherit methods
     * modify the permission for an ARO to be that of its parent object.
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success
     */
    public function inherit($daro, $daco, $action = "*") {
        return $this->_Instance->inherit($daro, $daco, $action);
    }

    /**
     * Pass-thru function for ACL grant instance. An alias for AclComponent::allow()
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success
     * @deprecated
     */
    public function grant($daro, $daco, $action = "*") {
        trigger_error(__d('cake_dev', 'AclComponent::grant() is deprecated, use allow() instead'), E_USER_WARNING);
        return $this->_Instance->allow($daro, $daco, $action);
    }

    /**
     * Pass-thru function for ACL grant instance. An alias for AclComponent::deny()
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success
     * @deprecated
     */
    public function revoke($daro, $daco, $action = "*") {
        trigger_error(__d('cake_dev', 'AclComponent::revoke() is deprecated, use deny() instead'), E_USER_WARNING);
        return $this->_Instance->deny($daro, $daco, $action);
    }

    /**
     *
     * @param type $daro
     * @return type
     */
    public function getRightsGrid($daro, $dacos = array(), $options = array('actions' => '*')) {
        if (empty($dacos)) {
//			$dacos = $this->_Instance->Daco->find('threaded');
            $dacos = $this->_Instance->Daco->find('threaded', array('order' => array('Daco.alias ASC')));
        }
        $return = array();
        foreach ($dacos as $daco) {
            if (!empty($options['models']) && in_array($daco['Daco']['model'], $options['models']) || empty($options['models'])) {
                $return[$daco['Daco']['id']] = $this->getRights($daro, $daco, $options);
                if (!empty($daco['children'])) {
                    $return[$daco['Daco']['id']]['children'] = $this->getRightsGrid($daro, $daco['children'], $options);
                }
            }
        }
        return $return;
    }

    /**
     *
     * Recursive check
     *
     * @param type $daro
     * @param type $daco
     * @return type
     */
    public function getRights($daro, $daco, $options = array('actions' => '*')) {
        $conditions = array('id' => $daco['Daco']['foreign_key']);
        $this->model = ClassRegistry::init($daco['Daco']['model']);
        if (isset($this->model->displayField)) {
            $object = $this->model->field($this->model->displayField, $conditions);
        } else {
            $object = $this->model->field('name', $conditions);
        }

        $actions = $options['actions'];
        $checks = array();
        if ($actions == '*' || is_array($actions) && in_array('*', $actions)) {
            $chk = $this->check($daro['Daro'], $daco['Daco']);
            $checks['create'] = $chk;
            $checks['read'] = $chk;
            $checks['update'] = $chk;
            $checks['delete'] = $chk;
        }
        if ($actions == 'create' || is_array($actions) && in_array('create', $actions)) {
            $checks['create'] = $this->check($daro['Daro'], $daco['Daco'], 'create');
        }
        if ($actions == 'read' || is_array($actions) && in_array('read', $actions)) {
            $checks['read'] = $this->check($daro['Daro'], $daco['Daco'], 'read');
        }
        if ($actions == 'update' || is_array($actions) && in_array('update', $actions)) {
            $checks['update'] = $this->check($daro['Daro'], $daco['Daco'], 'update');
        }
        if ($actions == 'delete' || is_array($actions) && in_array('delete', $actions)) {
            $checks['delete'] = $this->check($daro['Daro'], $daco['Daco'], 'delete');
        }

        $return = array(
            'checks' => $checks,
            'model' => $daco['Daco']['model'],
            'foreign_key' => $daco['Daco']['foreign_key'],
            'alias' => $daco['Daco']['alias'],
            'object' => $object
        );
        return $return;
    }

    /**
     *
     * @param type $requester
     * @param type $rightsGrid
     */
    public function setRights($requester, $rightsGrid) {
        $querydata = array(
            'fields' => array(
                'Daco.id',
                'Daco.alias',
                'Daco.model',
                'Daco.foreign_key',
                'Daco.parent_id'
            ),
            'conditions' => array(
                'OR' => array()
            )
        );
        foreach ($rightsGrid as $key => $val) {
            $querydata['conditions']['OR'][] = array('Daco.model' => $key);
        }

        $arbo = $this->_Instance->Daco->find('threaded', $querydata);
        $keys = array_keys(current(current($rightsGrid)));


        $saved = array();
        foreach ($keys as $key) {
            $dacos = array();
            foreach ($arbo as $item) {
                $dacos[] = $this->_normalizeDacos($item, $rightsGrid, $key);
            }

            $dacos = $this->_processDacos($requester, $dacos);

            $daro = array('model' => key($requester), 'foreign_key' => current($requester));
            $saved[] = $this->_saveDacos($daro, $dacos, $key);
        }
        $this->Session->delete('DataAcl');

        $return = !in_array(false, $saved, true);
        return $return;
    }

    /**
     *
     * @param type $daco
     * @param type $rights
     * @param type $key
     * @return type
     */
    private function _normalizeDacos($daco, $rights, $key) {
        $children = array();
        if (!empty($daco['children'])) {
            foreach ($daco['children'] as $child) {
                $children[] = $this->_normalizeDacos($child, $rights, $key);
            }
        }

        $daco['Daco']['value'] = $rights[$daco['Daco']['model']][$daco['Daco']['foreign_key']][$key];
        $daco['children'] = $children;
        return $daco;
    }

    /**
     *
     * @param type $requester
     * @param type $dacos
     * @param type $parentValue
     * @return type
     */
    private function _processDacos($requester, $dacos, $parentValue = 0) {
        foreach ($dacos as $key => $daco) {
            if (!empty($daco['children'])) {
                $daco['children'] = $this->_processDacos($requester, $daco['children'], $daco['Daco']['value']);
            }
            $dacos[$key] = $this->_processDaco($daco, $parentValue);
        }
        return $dacos;
    }

    /**
     *
     * @param type $daco
     * @param type $parentValue
     * @return boolean
     */
    private function _processDaco($daco, $parentValue = 0) {
        $daco['Daco']['inherit'] = false;
        if (!empty($daco['children'])) {
            //recuperation des valeurs des enfants
            $values = array();
            foreach ($daco['children'] as $child) {
                $values[] = intval($child['Daco']['value']);
            }
            if (array_sum($values) == count($values) || (array_sum($values) == 0 && $daco['Daco']['value'] == 1)) {
                $daco['Daco']['value'] = 1;
                for ($i = 0; $i < count($daco['children']); $i++) {
                    $daco['children'][$i]['Daco']['inherit'] = true;
                }
            }
        }
        return $daco;
    }

    /**
     *
     * @param type $aro
     * @param type $acos
     * @param type $first
     */
    private function _saveDacos($daro, $dacos, $key) {
        $return = array();
        foreach ($dacos as $daco) {
            if (!empty($daco['children'])) {
                $this->_saveDacos($daro, $daco['children'], $key, false);
            }

            $daroId = $this->_Instance->Daro->field('Daro.id', $daro);
            $dacoId = $this->_Instance->Daco->field('Daco.id', array('model' => $daco['Daco']['model'], 'foreign_key' => $daco['Daco']['foreign_key']));
            $exists = $this->_Instance->Daco->Dpermission->query('select id from daros_dacos where daro_id = ' . $daroId . ' and daco_id = ' . $dacoId . ';');

            $value = ($daco['Daco']['value'] == 1 ? 1 : -1);
//            if ($daco['Daco']['inherit'] == true) {
//                $value = 0;
//            }

            $valueToUse = array(
            	'create' => 'create',
				'delete' => 'delete',
				'read' => 'read',
				'update' => 'update'
			);

			if (!empty($exists)) {
				foreach( $valueToUse as $key => $name ) {
					$query = "UPDATE daros_dacos SET _" . $key . " = " . $value . " WHERE id=" . $exists[0][0]['id'] . ";";
					$return[] = $this->_Instance->Daco->Dpermission->query($query);
				}
			} else {
//				$query = "INSERT INTO daros_dacos (daro_id, daco_id, _" . $key . ") VALUES (" . $daroId . ", " . $dacoId . ", " . $value . ");";
				$query = "INSERT INTO daros_dacos (daro_id, daco_id, _create, _read, _delete, _update) VALUES (" . $daroId . ", " . $dacoId . ", " . $value . ", " . $value . ", " . $value . ", " . $value . ");";
			}
//$this->log($query);
			$return[] = $this->_Instance->Daco->Dpermission->query($query);
            /*if (!empty($exists)) {
                $query = "UPDATE daros_dacos SET _" . $key . " = " . $value . " WHERE id=" . $exists[0][0]['id'] . ";";
            } else {
                $query = "INSERT INTO daros_dacos (daro_id, daco_id, _" . $key . ") VALUES (" . $daroId . ", " . $dacoId . ", " . $value . ");";
            }

            $return[] = $this->_Instance->Daco->Dpermission->query($query);*/
        }

        return !in_array(false, $return, true);
    }

}

/**
 *
 */
class DbDataAcl extends CakeObject implements AclInterface {

    /**
     * Constructor
     *
     */
    public function __construct() {
        parent::__construct();
        App::uses('DataAclNode', 'DataAcl.Model');
        $this->Daro = ClassRegistry::init(array('class' => 'DataAcl.Daro', 'alias' => 'Daro'));
        $this->Daco = ClassRegistry::init(array('class' => 'DataAcl.Daco', 'alias' => 'Daco'));
    }

    /**
     * Initializes the containing component and sets the Daro/Daco objects to it.
     *
     * @param AclComponent $component
     * @return void
     */
    public function initialize(Component $component) {
        $component->Daro = $this->Daro;
        $component->Daco = $this->Daco;
    }

    /**
     * Checks if the given $daro has access to action $action in $daco
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success (true if ARO has access to action in ACO, false otherwise)
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/access-control-lists.html#checking-permissions-the-acl-component
     */
    public function check($daro, $daco, $action = "*") {
        if ($daro == null || $daco == null) {
            return false;
        }

        $permKeys = $this->_getAcoKeys($this->Daro->Dpermission->schema());
        $aroPath = $this->Daro->node($daro);
        $acoPath = $this->Daco->node($daco);

        if (empty($aroPath) || empty($acoPath)) {
            trigger_error(__d('cake_dev', "DbDataAcl::check() - Failed DARO/DACO node lookup in permissions check.  Node references:\nDaro: ") . print_r($daro, true) . "\nDaco: " . print_r($daco, true), E_USER_WARNING);
            return false;
        }

        if ($acoPath == null || $acoPath == array()) {
            trigger_error(__d('cake_dev', "DbDataAcl::check() - Failed DACO node lookup in permissions check.  Node references:\nDaro: ") . print_r($daro, true) . "\nDaco: " . print_r($daco, true), E_USER_WARNING);
            return false;
        }

        if ($action != '*' && !in_array('_' . $action, $permKeys)) {
            trigger_error(__d('cake_dev', "ACO permissions key %s does not exist in DbDataAcl::check()", $action), E_USER_NOTICE);
            return false;
        }

        $inherited = array();
        $acoIDs = Set::extract($acoPath, '{n}.' . $this->Daco->alias . '.id');

        $count = count($aroPath);
        for ($i = 0; $i < $count; $i++) {
            $permAlias = $this->Daro->Dpermission->alias;

            $perms = $this->Daro->Dpermission->find('all', array(
                'conditions' => array(
                    "{$permAlias}.daro_id" => $aroPath[$i][$this->Daro->alias]['id'],
                    "{$permAlias}.daco_id" => $acoIDs
                ),
                'order' => array($this->Daco->alias . '.lft' => 'desc'),
                'recursive' => 0
            ));

            if (empty($perms)) {
                continue;
            } else {
                $perms = Set::extract($perms, '{n}.' . $this->Daro->Dpermission->alias);
                foreach ($perms as $perm) {
                    if ($action == '*') {

                        foreach ($permKeys as $key) {
                            if (!empty($perm)) {
                                if ($perm[$key] == -1) {
                                    return false;
                                } elseif ($perm[$key] == 1) {
                                    $inherited[$key] = 1;
                                }
                            }
                        }

                        if (count($inherited) === count($permKeys)) {
                            return true;
                        }
                    } else {
                        switch ($perm['_' . $action]) {
                            case -1:
                                return false;
                            case 0:
                                continue 2;
                                break;
                            case 1:
                                return true;
                                break;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Allow $daro to have access to action $actions in $daco
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $actions Action (defaults to *)
     * @param integer $value Value to indicate access type (1 to give access, -1 to deny, 0 to inherit)
     * @return boolean Success
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/access-control-lists.html#assigning-permissions
     */
    public function allow($daro, $daco, $actions = "*", $value = 1) {
        $perms = $this->getAclLink($daro, $daco);
        $permKeys = $this->_getAcoKeys($this->Daro->Dpermission->schema());
        $save = array();

        if ($perms == false) {
            trigger_error(__d('cake_dev', 'DbDataAcl::allow() - Invalid node'), E_USER_WARNING);
            return false;
        }
        if (isset($perms[0])) {
            $save = $perms[0][$this->Daro->Dpermission->alias];
        }

        if ($actions == "*") {
            $permKeys = $this->_getAcoKeys($this->Daro->Dpermission->schema());
            $save = array_combine($permKeys, array_pad(array(), count($permKeys), $value));
        } else {
            if (!is_array($actions)) {
                $actions = array('_' . $actions);
            }
            if (is_array($actions)) {
                foreach ($actions as $action) {
                    if ($action{0} != '_') {
                        $action = '_' . $action;
                    }
                    if (in_array($action, $permKeys)) {
                        $save[$action] = $value;
                    }
                }
            }
        }
        list($save['daro_id'], $save['daco_id']) = array($perms['daro'], $perms['daco']);

        if ($perms['link'] != null && !empty($perms['link'])) {
            $save['id'] = $perms['link'][0][$this->Daro->Dpermission->alias]['id'];
        } else {
            unset($save['id']);
            $this->Daro->Dpermission->id = null;
        }
        return ($this->Daro->Dpermission->save($save) !== false);
    }

    /**
     * Deny access for $daro to action $action in $daco
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/access-control-lists.html#assigning-permissions
     */
    public function deny($daro, $daco, $action = "*") {
        return $this->allow($daro, $daco, $action, -1);
    }

    /**
     * Let access for $daro to action $action in $daco be inherited
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success
     */
    public function inherit($daro, $daco, $action = "*") {
        return $this->allow($daro, $daco, $action, 0);
    }

    /**
     * Allow $daro to have access to action $actions in $daco
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success
     * @see allow()
     */
    public function grant($daro, $daco, $action = "*") {
        return $this->allow($daro, $daco, $action);
    }

    /**
     * Deny access for $daro to action $action in $daco
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @param string $action Action (defaults to *)
     * @return boolean Success
     * @see deny()
     */
    public function revoke($daro, $daco, $action = "*") {
        return $this->deny($daro, $daco, $action);
    }

    /**
     * Get an array of access-control links between the given Daro and Daco
     *
     * @param string $daro ARO The requesting object identifier.
     * @param string $daco ACO The controlled object identifier.
     * @return array Indexed array with: 'daro', 'daco' and 'link'
     */
    public function getAclLink($daro, $daco) {
        $obj = array();
        $obj['Daro'] = $this->Daro->node($daro);
        $obj['Daco'] = $this->Daco->node($daco);

        if (empty($obj['Daro']) || empty($obj['Daco'])) {
            return false;
        }

        return array(
            'daro' => Set::extract($obj, 'Daro.0.' . $this->Daro->alias . '.id'),
            'daco' => Set::extract($obj, 'Daco.0.' . $this->Daco->alias . '.id'),
            'link' => $this->Daro->Dpermission->find('all', array('conditions' => array(
                    $this->Daro->Dpermission->alias . '.daro_id' => Set::extract($obj, 'Daro.0.' . $this->Daro->alias . '.id'),
                    $this->Daro->Dpermission->alias . '.daco_id' => Set::extract($obj, 'Daco.0.' . $this->Daco->alias . '.id')
        )))
        );
    }

    /**
     * Get the keys used in an ACO
     *
     * @param array $keys Permission model info
     * @return array ACO keys
     */
    protected function _getAcoKeys($keys) {
        $newKeys = array();
        $keys = array_keys($keys);
        foreach ($keys as $key) {
            if (!in_array($key, array('id', 'daro_id', 'daco_id'))) {
                $newKeys[] = $key;
            }
        }
        return $newKeys;
    }

}

?>
