<?php

App::uses('DataAclComponent', 'DataAcl.Controller/Component');

/**
 * SessionDataAcl component class
 *
 * DataAcl : CakePHP Data Acl plugin
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		DataAcl
 * @subpackage		DataAcl.Controller.Component
 *
 * based on macduy SessionAcl Component
 * @link http://bakery.cakephp.org/fre/%20articles/view/4cb560cb-60e0-4aa5-b7e1-4b4ed13e7814
 */
class SessionDataAclComponent extends DataAclComponent {

	/**
	 * Component components
	 *
	 * @access public
	 * @var array
	 */
	public $components = array('Session');

	/**
	 * Component initialization
	 *
	 * @access public
	 * @param Controller $controller
	 * @retun void
	 */
	public function initialize(Controller $controller) {
		parent::initialize($controller);
		$controller->DataAcl = $this;
	}

	/**
	 * Vérification d'autorisation d'accès à un DACO pour un DARO
	 *
	 * @access public
	 * @param array $daro
	 * @param array $daco
	 * @param mixed $action
	 * @return boolean
	 */
	public function check($daro, $daco, $action = "*") {
		$path = $this->__cachePath($daro, $daco, $action);
		if ($this->Session->check($path)) {
			$check = $this->Session->read($path);
		} else {
			$check = parent::check($daro, $daco, $action);
			$this->Session->write($path, $check);
		}
		return $check;
	}

	/**
	 * Autoriser un DARO à accéder à un DACO
	 *
	 * @access public
	 * @param array $daro
	 * @param mixed $daco
	 * @param mixed $action
	 * @return void
	 */
	public function allow($daro, $daco, $action = "*") {
		$return = parent::allow($daro, $daco, $action);
		$this->__delete($daro, $daco, $action);
		return $return;
	}

	/**
	 * Interdire un DARO à accéder à un DACO
	 *
	 * @access public
	 * @param array $daro
	 * @param mixed $daco
	 * @param mixed $action
	 * @return void
	 */
	public function deny($daro, $daco, $action = "*") {
		$return = parent::deny($daro, $daco, $action);
		$this->__delete($daro, $daco, $action);
		return $return;
	}

	/**
	 * Inherit method.
	 *
	 * This method overrides and uses the original
	 * method. It only adds cache to it.
	 *
	 * @param array $daro DARO
	 * @param array $daco DACO
	 * @param string $action Action (defaults to *)
	 * @access public
	 * @return void
	 */
	public function inherit($daro, $daco, $action = "*") {
		$return = parent::inherit($daro, $daco, $action);
		$this->__delete($daro, $daco, $action);
		return $return;
	}

	/**
	 * Grant method.
	 *
	 * This method overrides and uses the original
	 * method. It only adds cache to it.
	 *
	 * @param array $daro DARO
	 * @param array $daco DACO
	 * @param string $action Action (defaults to *)
	 * @access public
	 * @return void
	 */
	public function grant($daro, $daco, $action = "*") {
		$return = parent::grant($daro, $daco, $action);
		$this->__delete($daro, $daco, $action);
		return $return;
	}

	/**
	 * Revoke method.
	 *
	 * This method overrides and uses the original
	 * method. It only adds cache to it.
	 *
	 * @param array $daro DARO
	 * @param array $daco DACO
	 * @param string $action Action (defaults to *)
	 * @access public
	 * @return void
	 */
	public function revoke($daro, $daco, $action = "*") {
		$return = parent::revoke($daro, $daco, $action);
		$this->__delete($daro, $daco, $action);
		return $return;
	}

	/**
	 * Returns a unique, dot separated path to use as the cache key. Copied from CachedAcl.
	 *
	 * @param array $daro DARO
	 * @param array $daco DACO
	 * @param boolean $acoPath Boolean to return only the path to the ACO or the full path to the permission.
	 * @access private
	 * @return void
	 */
	public function __cachePath($daro, $daco, $action, $dacoPath = false) {
		if (is_array($daco) && !empty($daco['alias'])) {
			$dacoAlias = $daco['alias'];
		} else if (is_string($daco)) {
			$dacoAlias = $daco;
		}

		if ($action != "*") {
			$dacoAlias .= '.' . $action;
		}
		$path = Inflector::slug($dacoAlias);
//    if (!$dacoPath) {
		if (is_array($daro) && !empty($daro['alias'])) {
			$daroAlias = $daro['alias'];
		} else if (is_string($daro)) {
			$daroAlias = $daro;
		}
		$path .= '.' . Inflector::slug($daroAlias);
//    }
		return "DataAcl." . $path;
	}

	/**
	 * Suppression d'un droit
	 *
	 * @access public
	 * @param array $daro
	 * @param array $daco
	 * @param mixed $action
	 * @return void
	 */
	public function __delete($daro, $daco, $action) {
		$key = $this->__cachePath($daro, $daco, $action, true);
		if ($this->Session->check($key)) {
			$this->Session->delete($key);
		}
	}

	/**
	 * Suppression des données en cache (session PHP)
	 *
	 * @access public
	 * @return void
	 */
	public function flushCache() {
		$this->Session->delete('DataAcl');
	}

	/**
	 * Checks that all given pairs of aco-action is satisfied
	 *
	 * @access public
	 * @param array $daro
	 * @param array $pairs
	 * @return boolean
	 */
	public function all($daro, $pairs) {
		foreach ($pairs as $daco => $action) {
			if (!$this->check($daro, $daco, $action)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks that AT LEAST ONE of given pairs of aco-action is satisfied
	 *
	 * @access public
	 * @param array $daro
	 * @param array $pairs
	 * @return boolean
	 */
	public function one($daro, $pairs) {
		foreach ($pairs as $daco => $action) {
			if ($this->check($daro, $daco, $action)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns an array of booleans for each $aco-$aro pair
	 *
	 * @access public
	 * @param array $daro
	 * @param array $pairs
	 * @return array
	 */
	public function can($daro, $pairs) {
		$can = array();
		$i = 0;
		foreach ($pairs as $daco => $action) {
			$can[$i] = $this->check($daro, $daco, $action);
			$i++;
		}
		return $can;
	}

	/**
	 * Application des droits
	 *
	 * @access public
	 * @param array $requester
	 * @param array $rigthsGrid
	 * @return array
	 */
	public function setRights($requester, $rigthsGrid) {
		$return = parent::setRights($requester, $rigthsGrid);
		$this->log("ok", 'debug');
		$this->flushCache();
		return $return;
	}

}

?>
