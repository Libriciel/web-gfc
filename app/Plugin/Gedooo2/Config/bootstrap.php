<?php
	/**
	 * Boostrap du plugin Gedooo2.
	 *
	 * PHP 5.3
	 *
	 * @package Gedooo2
	 * @subpackage Config
	 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
	 */
	if( !defined( 'GEDOOO_PLUGIN_DIR' ) ) {
		define( 'GEDOOO_PLUGIN_DIR', dirname( __FILE__ ).DS.'..'.DS );
	}

	if( !defined( 'GEDOOO_WSDL' ) ) {
		define( 'GEDOOO_WSDL', Configure::read( 'Gedooo.wsdl' ) );
	}

	if( !defined( 'GEDOOO_TEST_FILE' ) ) {
		define( 'GEDOOO_TEST_FILE', GEDOOO_PLUGIN_DIR.'Vendor'.DS.'modelesodt'.DS.'test_gedooo.odt' );
	}

	if( !defined( 'PHPGEDOOO_DIR' ) ) {
		switch( Configure::read( 'Gedooo.method' ) ) {
			case 'classic':
				define( 'PHPGEDOOO_DIR', GEDOOO_PLUGIN_DIR.'Vendor'.DS.'phpgedooo_ancien'.DS );
				break;
			case 'cloudooo':
			case 'unoconv':
			default:
				define( 'PHPGEDOOO_DIR', GEDOOO_PLUGIN_DIR.'Vendor'.DS.'phpgedooo_nouveau'.DS );
		}
	}

	require_once( PHPGEDOOO_DIR.'GDO_Utility.class' );
	require_once( PHPGEDOOO_DIR.'GDO_FieldType.class' );
	require_once( PHPGEDOOO_DIR.'GDO_ContentType.class' );
	require_once( PHPGEDOOO_DIR.'GDO_IterationType.class' );
	require_once( PHPGEDOOO_DIR.'GDO_PartType.class' );
	require_once( PHPGEDOOO_DIR.'GDO_FusionType.class' );
	require_once( PHPGEDOOO_DIR.'GDO_MatrixType.class' );
	require_once( PHPGEDOOO_DIR.'GDO_MatrixRowType.class' );
	require_once( PHPGEDOOO_DIR.'GDO_AxisTitleType.class' );
?>
