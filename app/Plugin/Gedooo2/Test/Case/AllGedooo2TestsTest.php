<?php
	/**
	 * AllGedooo2Tests file
	 *
	 * PHP 5.3
	 *
	 * @package Gedooo2
	 * @subpackage Test.Case
	 */
	CakePlugin::load( 'Gedooo2', array( 'bootstrap' => true ) );

	/**
	 * AllGedooo2Tests class
	 *
	 * This test group will run all tests.
	 *
	 * @package Gedooo2
	 * @subpackage Test.Case
	 */
	class AllGedooo2Tests extends PHPUnit_Framework_TestSuite
	{
		/**
		 * Test suite with all test case files.
		 *
		 * @return void
		 */
		public static function suite() {
			$suite = new CakeTestSuite( 'All Gedooo2 tests' );
			$suite->addTestDirectoryRecursive( dirname( __FILE__ ).DS.'..'.DS.'Case'.DS );
			return $suite;
		}
	}
?>