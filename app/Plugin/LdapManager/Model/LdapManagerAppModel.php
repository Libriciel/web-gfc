<?php

App::uses('AppModel', 'Model');

class LdapManagerAppModel extends AppModel {

    public $actsAs = array( 'Containable');


    public function __construct($id = false, $table = null, $ds = null)
    {
        if ($this->useDbConfig == 'ldap' && !array_key_exists('ldap', ConnectionManager::enumConnectionObjects())) {
            ClassRegistry::init(('Connecteur'));
			$this->Connecteur = new Connecteur();
			$this->Connecteur->setDataSource(Configure::read('LdapManager.Ldap.conn'));
            $ldap = $this->Connecteur->find(
                'first',
                array(
                    'conditions' => array(
                        'Connecteur.name ILIKE' => '%LDAP%'
                    ),
                    'contain' => false
                )
            );
            if( isset($ldap) && !empty( $ldap ) ) {
                if (!$ldap['Connecteur']['use_ldap']) {
                    trigger_error(__d('ldap_manager', '(LdapManagerAppModel::__construct) Unable to build ConnectionManager because LDAP not use.'), E_USER_ERROR);
                }

                if($ldap['Connecteur']['ldap_type'] == 'ActiveDirectory') {
                    $fields = array(
                        'User' => array (
                            'username' => Configure::read('Ldap.UsernameLdapAttribute'),
                            'note' => 'info',
                            'nom' => 'sn',
                            'prenom' => 'givenname',
                            'mail' => 'mail',
                            'numtel' => 'telephonenumber',
                            'active' => ''
                        )
                    );
                }
                else {
                    $fields = array(
                        'User' => array (
							'username' => Configure::read('Ldap.UsernameLdapAttribute'),
                            'note' => 'info',
                            'nom' => 'sn',
                            'prenom' => 'givenname',
                            'mail' => 'mail',
                            'numtel' => 'telephonenumber',
                            'active' => ''
                        )
                    );
                }

                $settings = array (
                    'datasource' => 'LdapManager.Ldap',
                    'type' => $ldap['Connecteur']['ldap_type'],
                    'host' => array(
                        $ldap['Connecteur']['ldap_host'],
                        $ldap['Connecteur']['ldap_has_fall_over']
                    ),
                    'port' => $ldap['Connecteur']['ldap_port'],
                    'basedn' => $ldap['Connecteur']['ldap_base_dn'],
                    'login' => $ldap['Connecteur']['ldap_login'],
                    'password' => $ldap['Connecteur']['ldap_password'],
                    'database' => '',
                    'tls'      => $ldap['Connecteur']['ldap_tls'],
                    'version'  => $ldap['Connecteur']['ldap_version'],
                    'username' => $fields['User']['username'],
                    'account_suffix' => $ldap['Connecteur']['ldap_account_suffix'],
                    'fields' => $fields,
                    'certificat' => $ldap['Connecteur']['ldaps_cert'],
					'conn' => Configure::read('LdapManager.Ldap.conn')
                );
                if (is_null(ConnectionManager::create('ldap', $settings))) {
                    trigger_error(__d('ldap_manager', '(LdapManagerAppModel::__construct) Unable to build ConnectionManager for $settings data.'), E_USER_ERROR);
                }
            }
            else {
                return false;
            }
        }
        parent::__construct($id, $table, $ds);
    }


}
