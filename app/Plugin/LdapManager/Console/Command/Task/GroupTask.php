<?php

class GroupTask extends Shell {

    public $uses = array('LdapManager.LdapGroup', 'LdapManager.Group');

    public function execute() { }

    public function update() {

//        $dataSource = $this->Group->getDataSource();
//        $dataSource->useNestedTransactions = false;
        $this->Group->setDataSource(Configure::read('LdapManager.Ldap.conn'));
        try
        {
            //Liste groups ldap
            $ldapGroups= $this->LdapGroup->find('all',
                array(
                    'fields' => array($this->LdapGroup->getKeyWord('cn')),
                    'conditions' => array(
                            'OR' =>  array(
                                $this->LdapGroup->getKeyWord('objectClass') => array(
                                    $this->LdapGroup->getKeyWord('groupOfNames'),
                                    $this->LdapGroup->getKeyWord('groupOfUniqueNames'))
                                )),
                    'recursive'=> -1,
                    'order'=> $this->LdapGroup->getKeyWord('cn')
                )
            );
//            $dataSource->begin();
            $this->Group->begin();

        if(!empty($ldapGroups))
            foreach($ldapGroups as $ldapGroup)
            {
                $this->Group->setDataSource(Configure::read('LdapManager.Ldap.conn'));
                $this->Group->recursive=-1;
                $GroupId=$this->Group->findByName($ldapGroup['Group'][$this->LdapGroup->getKeyWord('cn')], array('id'));
                if(empty($GroupId)){
                    $this->Group->create();
                    $data = array();
                    $data['Group']['name'] = $ldapGroup['Group'][$this->LdapGroup->getKeyWord('cn')];
                    $data['Group']['dn'] = utf8_encode($ldapGroup['Group']['dn']);
                    $this->Group->save($data);
                    Shell::err('Création du groupe '.$data['Group']['name'] . ' réalisée.');
                }

            }

            $this->Group->commit();
            return true;
        }
        catch (ErrorException $e)
        {
            $this->Group->rollback();
            $this->out($e->getMessage());
        }

        return false;
    }

}
