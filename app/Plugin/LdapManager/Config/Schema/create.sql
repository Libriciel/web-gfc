DROP TABLE IF EXISTS ldapm_groups;
DROP SEQUENCE IF EXISTS ldapm_groups_id_seq;

CREATE TABLE ldapm_groups
(
    id serial,
    parent_id integer,
    lft integer,
    rght integer,
    "name" character varying(255),  
    dn character varying(255),  
    CONSTRAINT ldapm_groups_pkey PRIMARY KEY (id)
);

DROP TABLE IF EXISTS ldapm_models_groups;
DROP SEQUENCE IF EXISTS ldapm_models_groups_id_seq;

CREATE TABLE ldapm_models_groups
(
    id serial,
    ldapm_groups_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    foreign_key integer,
    CONSTRAINT ldapm_models_groups_pkey PRIMARY KEY (id)
);