<?php

/**
 * DataSource base class
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Model.Datasource
 * @since         CakePHP(tm) v 0.10.5.1790
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 *
 * Create a datasource in your config/database.php
  public $config = array(
  'datasource' => 'ImapSource',
  'database' => '',
  'server' => '',
  'port' => null,
  'login' => '',
  'password' => '',
  'encryption' => '', // could be : nothing ( '' ), ssl/tls ('ssl'), starttls ('tls'),
  'login_encryption' => '' // currently, it could only be '' for text/plain
  );
 *
 *
 * Note : please compile php with imap support or install php imap module (Ubuntu : sudo apt-get install php5-imap)
 *
 *
 */
App::uses('DataSource', 'Model/Datasource');

/**
 * DataSource base class
 *
 * @package       Cake.Model.Datasource
 */
class ImapSource extends DataSource {

	/**
	 * An optional description of your datasource
	 */
	public $description = 'IMAP reader';

	/**
	 *
	 * @var type
	 */
	public $mbox = null;

	/**
	 *
	 * @var type
	 */
	public $connected = true;

	/**
	 *
	 * @var type
	 */
	public $config = array(
		'datasource' => 'ImapSource',
		'database' => '',
		'server' => '',
		'port' => null,
		'login' => '',
		'password' => '',
		'encryption' => '', // could be : nothing ( '' ), ssl/tls ('ssl'), starttls ('tls'),
		'login_encryption' => '' // currently, it could only be '' for text/plain
	);

	/**
	 * Créons notre HttpSocket et gérons any config tweaks.
	 */
	public function __construct($config) {
		parent::__construct($config);
//		$this->Http = new HttpSocket();
	}

	/**
	 * Since datasources normally connect to a database there are a few things
	 * we must change to get them to work without a database.
	 */

	/**
	 * listSources() is for caching. You'll likely want to implement caching in
	 * your own way with a custom datasource. So just ``return null``.
	 */
	public function listSources($data = null) {
		parent::listSources($data);
	}

	/**
	 * describe() tells the model your schema for ``Model::save()``.
	 *
	 * You may want a different schema for each model but still use a single
	 * datasource. If this is your case then set a ``schema`` property on your
	 * models and simply return ``$Model->schema`` here instead.
	 */
	public function describe($model) {
		parent::describe($model);
	}

	/**
	 *
	 * @throws FatalErrorException
	 */
	private function _setConnection() {

		$encrypt = '';
		if ($this->config['encryption'] == 'ssl') {
			$encrypt = '/ssl/novalidate-cert';
		} else if ($this->config['encryption'] == 'tls') {
			$encrypt = '/tls/novalidate-cert';
		}

		$ServerName = "{" . $this->config['server'] . "/imap" . $encrypt . ":" . $this->config['port'] . "}INBOX"; // For a IMAP connection    (PORT 143)
		$this->mbox = imap_open($ServerName, $this->config['login'], $this->config['password'], OP_READONLY) or die("Could not open Mailbox - try again later!");
		return $this->mbox != null;
	}

	/**
	 *
	 * @return type
	 */
	private function _closeConnection() {
		return imap_close($this->mbox);
	}

	/**
	 * Implement the R in CRUD. Calls to ``Model::find()`` arrive here.
	 *
	 *
	 *
	 * @param Model $model
	 * @param type $queryData
	 * @param type $recursive
	 * @return type
	 * @throws FatalErrorException
	 */
	public function read(Model $model, $queryData = array(), $recursive = null) {
		//prise en compte du querydata
		//si l'on veut la liste des message lus / non lus
		$seen = null;
		if (!empty($queryData['conditions']) && isset($queryData['conditions']['seen'])) {
			$seen = $queryData['conditions']['seen'];
		}

		//si l'on veut un message en fonction de son msgno
		$msgno = null;
		if (!empty($queryData['conditions']) && isset($queryData['conditions']['msgno'])) {
			$msgno = $queryData['conditions']['msgno'];
		}

		//si l'on veut recupérer le contenu d'une partie d'un message
		$part = null;
		if (!empty($queryData['conditions']) && isset($queryData['conditions']['part'])) {
			$part = $queryData['conditions']['part'];
		}

		//initialisation du retour
		$result = array(
			'try_conn' => false,
			'setConnection' => false,
			'mails' => array(),
			'mail' => array(),
			'part' => array(),
			'closeConnection' => false
		);

		try {
			$result['try_conn'] = true;
			//connexion imap
			$result['setConnection'] = $this->_setConnection();

			//verification de la validité de la boîte aux lettres courrante
			$hdr = imap_check($this->mbox);
			if (empty($hdr)) {
				$result['closeConnection'] = $this->_closeConnection();
				throw new FatalErrorException(__d('imap', 'check.error'));
			}

			//prise en compte du querydata
			if (!is_null($msgno)) {
				if (!is_null($part)) { // si on veut récpérer le contenu d'une partie du message (pour les pièces jointes notament)
					$result['part'] = $this->_getPartInfo($msgno, $part);
				} else { // si on veut récupérer les informations d'un message en particulier
					$result['mail'] = $this->_getMailInfo($msgno);
				}
			} else {
				$result['mails'] = $this->_getOverviews($seen);
			}

			//deconnexion imap
			$result['closeConnection'] = $this->_closeConnection();
		} catch (Exception $exc) {
			throw new FatalErrorException($exc);
		}
		return $result;
	}

	/**
	 *
	 * @param type $msgno
	 * @param type $partno
	 * @return type
	 */
	private function _getPartInfo($msgno, $partno) {
		$structure = imap_fetchstructure($this->mbox, $msgno);
		$structurePart = $structure->parts[$partno];
		if ($structurePart->ifdisposition && strtolower($structurePart->disposition) == 'attachment') { // si la partie à récupérer est une pièce jointe
			$return = $this->_getPartInfoFile($msgno, $partno, $structurePart);
		} else { // si on veut récupérer le contenu du message (pas un pièce jointe)
			$return = $this->_getPartInfoText($msgno, $partno, $structurePart);
		}
		return $return;
	}

	/**
	 *
	 * @param type $msgno
	 * @param type $part
	 * @param type $structure
	 * @return type
	 */
	private function _getPartInfoFile($msgno, $partno, $structurePart) {
		$filename = '';
		if ($structurePart->ifdparameters) {
			foreach ($structurePart->dparameters as $dparameter) {
				if (strtolower($dparameter->attribute) == 'filename') {
					$filename = $this->_convertAndDecodeMailHeader($dparameter->value);
				}
			}
		} else if ($structurePart->ifparameters) {
			foreach ($structurePart->parameters as $parameter) {
				if (strtolower($parameter->attribute) == 'name') {
					$filename = $this->_convertAndDecodeMailHeader($parameter->value);
				}
			}
		}

		$return = array(
			'msgno' => $msgno,
			'part' => $partno,
			'filename' => html_entity_decode($filename),
			'size' => $structurePart->bytes,
			'attachment' => $this->_translate_imap_body(imap_fetchbody($this->mbox, $msgno, $partno + 1), $structurePart->encoding)
		);
		return $return;
	}

	/**
	 *
	 * @param type $msgno
	 * @param type $part
	 * @param type $structure
	 * @return type
	 */
	private function _getPartInfoText($msgno, $partno, $structurePart) {
		return array(
			'msgno' => $msgno,
			'part' => $partno,
			'message' => $this->_translate_imap_body(imap_fetchbody($this->mbox, $msgno, $partno + 1), $structurePart->encoding)
		);
	}

	/**
	 *
	 * @param type $msgno
	 * @return type
	 */
	private function _getMailInfo($msgno) {
		$overviews = imap_fetch_overview($this->mbox, $msgno, 0);

		return array(
			'header' => $this->_convertAndDecodeMail($overviews[0]),
			'structure' => $this->_convertPartFileName(Set::reverse(imap_fetchstructure($this->mbox, $msgno)))
		);
	}

	/**
	 *
	 * @param type $structure
	 * @return type
	 */
	private function _convertPartFileName($structure) {
		if (isset($structure['parts'])) {
			for ($i = 0; $i < count($structure['parts']); $i++) {
				if ($structure['parts'][$i]['ifdisposition'] && strtolower($structure['parts'][$i]['disposition']) == 'attachment') {
					if ($structure['parts'][$i]['ifparameters']) {
						for ($j = 0; $j < count($structure['parts'][$i]['parameters']); $j++) {
							if (strtolower($structure['parts'][$i]['parameters'][$j]['attribute']) == 'name') {
								$structure['parts'][$i]['parameters'][$j]['value'] = $this->_convertAndDecodeMailHeader($structure['parts'][$i]['parameters'][$j]['value']);
							}
						}
					}

					if ($structure['parts'][$i]['ifdparameters']) {
						for ($j = 0; $j < count($structure['parts'][$i]['dparameters']); $j++) {
							if (strtolower($structure['parts'][$i]['dparameters'][$j]['attribute']) == 'filename') {
								$structure['parts'][$i]['dparameters'][$j]['value'] = $this->_convertAndDecodeMailHeader($structure['parts'][$i]['dparameters'][$j]['value']);
							}
						}
					}
				}
			}
		}
		return $structure;
	}

	/**
	 *
	 * @param type $seen
	 * @return type
	 */
	private function _getOverviews($seen = null, $reverse = true) {
		$overviews = array();
		$return = array();
		if (!is_null($seen)) { //si on filtre les mails sur le status "seen"
			$search = imap_search($this->mbox, $seen == 0 ? 'UNSEEN' : 'SEEN'); //liste des messages lus / non lus
			if (!empty($search)) {
				$overviews = imap_fetch_overview($this->mbox, implode(',', $search), 0); //on récupère le sommaire des en-têtes de messages recherchés
			}
		} else { // sinon on récupère le sommaire des en-têtes de tous les messages
			$overviews = imap_fetch_overview($this->mbox, "1: $hdr->Nmsgs", 0);
		}

		//parcours du sommaire des en-têtes de messages
		foreach ($overviews as $overview) {
			$return[] = $this->_convertAndDecodeMail($overview);
		}
		return $reverse ? array_reverse($return) : $return;
	}

	/**
	 *
	 * @param type $body
	 * @param type $encoding
	 * @return type
	 */
	private function _translate_imap_body($body, $encoding) {
		$result = '';
		if ($encoding == 0) {
			$result = $body;
		}if ($encoding == 1) {
			$result = $body;
		}if ($encoding == 2) {
			$result = imap_binary($body);
		}if ($encoding == 3) {
			$result = imap_base64($body);
		}if ($encoding == 4) {
			$result = quoted_printable_decode($body);
		}if ($encoding == 5) {
			$result = $body;
		}
		return $result;
	}

	/**
	 *
	 * @param type $oMail
	 * @return type
	 */
	private function _convertAndDecodeMail($oMail) {
		$aMail = Set::reverse($oMail);
		$return = array();
		foreach ($aMail as $key => $val) {
			$return[$key] = $this->_convertAndDecodeMailHeader($val);
		}
		return $return;
	}

	/**
	 *
	 * @param type $header
	 * @return type
	 */
	private function _convertAndDecodeMailHeader($header) {
		$return = '';
		$elements = imap_mime_header_decode($header);

		if (is_string($header)) { // si c'est une chaîne on decode
			$fullText = '';
			for ($i = 0; $i < count($elements); $i++) {
				$charset = $elements[$i]->charset;
				$text = $elements[$i]->text;
				$fullText .= ' ' . $text;
			}
			$return = htmlentities(trim($fullText));
		} else { //si ce n'est pas une chaîne, on rempli directement le tableau de retour
			$return = $header;
		}
//		$this->ldebug('--------------------------------');
//		$this->ldebug($header . ' <=> ' . $return);
		return $return;
	}

	/**
	 *
	 * @param type $structure
	 * @return string
	 */
	private function _getMimeType(&$structure) {
		$return = 'TEXT/PLAIN';
		$primary_mime_type = array('TEXT', 'MULTIPART', 'MESSAGE', 'APPLICATION', 'AUDIO', 'IMAGE', 'VIDEO', 'OTHER');
		if ($structure->subtype) {
			$return = $primary_mime_type[(int) $structure->type] . '/' . $structure->subtype;
		}
		return $return;
	}

	/**
	 *
	 * @param type $stream
	 * @param type $msg_number
	 * @param type $mime_type
	 * @param type $structure
	 * @param string $part_number
	 * @return boolean
	 */
	private function _getPart($stream, $msg_number, $mime_type, $structure = false, $part_number = false) {
		if (!$structure) {
			$structure = imap_fetchstructure($stream, $msg_number);
		}
		if ($structure) {
			if ($mime_type == $this->_getMimeType($structure)) {
				if (!$part_number) {
					$part_number = "1";
				}
				$text = imap_fetchbody($stream, $msg_number, $part_number);
				if ($structure->encoding == 3) {
					return imap_base64($text);
				} else if ($structure->encoding == 4) {
					return imap_qprint($text);
				} else {
					return $text;
				}
			}

			if ($structure->type == 1) /* multipart */ {
				while (list($index, $sub_structure) = each($structure->parts)) {
					if ($part_number) {
						$prefix = $part_number . '.';
					}
					$data = $this->_getPart($stream, $msg_number, $mime_type, $sub_structure, $prefix . ($index + 1));
					if ($data) {
						return $data;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Implement the C in CRUD. Calls to ``Model::save()`` without $Model->id
	 * set arrive here.
	 */
	public function create(Model $Model, $fields = array(), $values = array()) {
		return false;
	}

	/**
	 * Implement the U in CRUD. Calls to ``Model::save()`` with $Model->id
	 * set arrive here. Depending on the remote source you can just call
	 * ``$this->create()``.
	 */
	public function update(Model $model, $fields = null, $values = null, $conditions = null) {
		return false;
	}

	/**
	 * Implement the D in CRUD. Calls to ``Model::delete()`` arrive here.
	 */
	public function delete(Model $Model, $conditions = null) {
		return false;
	}

	/**
	 *
	 * @param type $var
	 */
	public function ldebug($var) {
		Cakelog::write('debug', var_export($var, true));
	}

}
