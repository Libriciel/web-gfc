<?php

/**
 * Imap App model class
 *
 * Imap : CakePHP Imap plugin
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		Imap
 * @subpackage		Imap.Model
 */
class ImapAppModel extends AppModel {

}

