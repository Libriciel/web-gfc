<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ImapsController extends ImapAppController {

	/**
	 * Controller name
	 *
	 * @var string
	 */
	public $name = 'Imaps';

	/**
	 *
	 * @var array
	 */
	public $uses = array('Imap.Imap');

	/**
	 *
	 * @var array
	 */
	public $components = array('Jsonmsg.Jsonmsg');

	/**
	 *
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Imap->useDbConfig = 'defaultimap';
//		$this->Imap->useDbConfig = 'imapperso';
	}

	/**
	 *
	 */
	public function index() {
		$conn = $this->Imap->getDatasource();
		$this->set(compact('conn'));
	}

	/**
	 * Displays a view
	 *
	 * @param mixed What page to display
	 * @return void
	 */
	public function retrieve() {
		if ($this->request->is('ajax')) {
			$this->Jsonmsg->init(__d('imap', 'connection.error'));
			$qd = array(
				'conditions' => array(
					'seen' => '0'
//					'seen' => '1'
				)
			);
			$result = $this->Imap->find('all', $qd);
			if (!empty($result)) {
				$this->Jsonmsg->json['return'] = $result;
				$this->Jsonmsg->valid(__d('imap', 'connection.success'));
			}
			$this->Jsonmsg->serialize();
		} else {
			throw new BadRequestException(__d('default', 'ajax.only'));
		}
	}

	/**
	 *
	 * @throws BadRequestException
	 */
	public function getMail() {
		if ($this->request->is('ajax')) {
			if (empty($this->request->data['msgno'])) {
				throw new BadRequestException();
			}
			$this->Jsonmsg->init(__d('imap', 'connection.error'));
			$qd = array(
				'conditions' => array(
					'msgno' => $this->request->data['msgno']
				)
			);
			$result = $this->Imap->find('all', $qd);
			if (!empty($result)) {
				$this->Jsonmsg->json['return'] = $result;
				$this->Jsonmsg->valid(__d('imap', 'connection.success'));
			}
			$this->Jsonmsg->serialize();
		} else {
			throw new BadRequestException(__d('default', 'ajax.only'));
		}
	}

	/**
	 *
	 * @param type $msgno
	 * @param type $part
	 * @throws BadRequestException
	 * @throws BadMethodCallException
	 */
	public function getMailPart($msgno = null, $part = null) {
		if (!$this->request->is('ajax') && $this->request->is('get')) {
			if (!isset($msgno) || !isset($part)) {
				throw new BadRequestException(__d('imap', 'mailpart.get.params.error'));
			}
			$this->_getMailPartFile($msgno, $part);
		} else if ($this->request->is('ajax') && $this->request->is('post')) {
			if (!isset($this->request->data['msgno']) || !isset($this->request->data['part'])) {
				throw new BadRequestException(__d('imap', 'mailpart.ajax.post.error'));
			}
			$this->_getMailPartText($this->request->data['msgno'], $this->request->data['part']);
		} else {
			throw new BadMethodCallException(__d('imap', 'mailpart.error'));
		}
	}

	/**
	 *
	 * @param type $msgno
	 * @param type $part
	 * @throws BadMethodCallException
	 */
	private function _getMailPartFile($msgno, $part) {
		$qd = array(
			'conditions' => array(
				'msgno' => $msgno,
				'part' => $part
			)
		);
		$result = $this->Imap->find('all', $qd);
		if (!empty($result)) {
			if (isset($result['part']['filename'])) { //fichier
				$this->response->type(substr(strrchr($result['part']['filename'], '.'), 1));
				$this->response->download($result['part']['filename']);
				$this->response->body($result['part']['attachment']);
				$this->response->send();
			} else { //message
				throw new BadMethodCallException(__d('imap', 'mailpart.file.error'));
			}
		}
	}

	/**
	 *
	 * @param type $msgno
	 * @param type $part
	 * @throws BadMethodCallException
	 */
	private function _getMailPartText($msgno, $part) {
		$this->Jsonmsg->init(__d('imap', 'connection.error'));
		$qd = array(
			'conditions' => array(
				'msgno' => $msgno,
				'part' => $part
			)
		);
		$result = $this->Imap->find('all', $qd);
		if (!empty($result)) {
			if (isset($result['part']['filename'])) { //fichier
				throw new BadMethodCallException(__d('imap', 'mailpart.text.error'));
			} else { //message
				$result['part']['message'] = nl2br(htmlentities(utf8_encode($result['part']['message'])));
				$this->Jsonmsg->json['return'] = $result;
				$this->Jsonmsg->valid(__d('imap', 'connection.success'));
			}
		}
		$this->Jsonmsg->serialize();
	}

}

