<?php

App::uses('SimpleCommentAppModel', 'SimpleComment.Model');

/**
 * Carnet d'adresse
 *
 * SimpleComment Plugin
 * SimpleComment Comment model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package             SimpleComment
 * @subpackage          SimpleComment.Model
 *
 * @property Reader $Reader
 * @property Target $Target
 */
class SCCommentReader extends SimpleCommentAppModel {

    /**
     *
     * @var type
     */
    public $useTable = 'comments_readers';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'reader_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'comment_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
    public $belongsTo = array(
        'Reader' => array(
            'className' => 'SimpleComment.SCReader',
            'foreignKey' => 'reader_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Comment' => array(
            'className' => 'SimpleComment.SCComment',
            'foreignKey' => 'comment_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );


	/**
	 * Fonction pêrmettant d emarquer les messages de lecture de comentaire comme lu
	 * Commentaire privé uniquement
	 * @param $fluxId
	 * @param $desktopId
	 */
	public function setRead($commentIds = array(), $desktopId) {
		$this->updateAll(
			array(
				'SCCommentReader.read' => 'true'
			),
			array(
				'SCCommentReader.comment_id' => $commentIds,
				'SCCommentReader.reader_id' => $desktopId
			)
		);
	}

}
