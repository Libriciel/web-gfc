<?php

/**
 * SimpleComment Target behavior class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package			SimpleComment
 * @subpackage		SimpleComment.Model.Behavior
 */
class SCTargetBehavior extends ModelBehavior {

	/**
	 *
	 * @var type
	 */
	public $Comment;

	/**
	 *
	 * @param type $model
	 * @param type $config
	 * @throws BadMethodCallException
	 */
	public function setup(Model $model, $config = Array()) {

		//verifier la config des alias

		$bindModelCommentParams = array(
			'hasMany' => array(
				'Comment' => array(
					'className' => 'SimpleComment.SCComment',
					'foreignKey' => 'target_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
				)
			)
		);
		$model->bindModel($bindModelCommentParams, false);

		$bindModelTargetParams = array(
			'belongsTo' => array(
				'Target' => array(
					'className' => $model->name,
					'foreignKey' => 'target_id',
					'conditions' => '',
					'fields' => '',
					'order' => '',
                    'counterCache' => ''
				)
			)
		);
		$model->Comment->bindModel($bindModelTargetParams, false);
	}

}

?>
