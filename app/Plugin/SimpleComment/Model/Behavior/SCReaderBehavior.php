<?php

/**
 * SimpleComment Reader behavior class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package			SimpleComment
 * @subpackage		SimpleComment.Model.Behavior
 */
class SCReaderBehavior extends ModelBehavior {

    /**
     *
     * @var type
     */
    public $Comment;

    /**
     *
     * @var type
     */
    public $CommentReader;

    /**
     *
     * @param type $model
     * @param type $config
     */
    public function setup(Model $model, $config = Array()) {
//debug($model);
        //verfier la definition des alias

        $bindModelCommentParams = array(
            'hasAndBelongsToMany' => array(
                'Comment' => array(
                    'className' => 'SimpleComment.SCComment',
                    'joinTable' => 'comments_readers',
                    'foreignKey' => 'reader_id',
                    'associationForeignKey' => 'comment_id',
                    'unique' => 'keepExisting',
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'finderQuery' => '',
                    'deleteQuery' => '',
                    'insertQuery' => '',
                    'with' => 'SimpleComment.SCCommentReader'
                )
            )
        );
        $model->bindModel($bindModelCommentParams, false);

        $bindModelReaderParams = array(
            'hasAndBelongsToMany' => array(
                'Reader' => array(
                    'className' => $model->name,
//                    'className' => 'SimpleComment.SCReader',
                    'joinTable' => 'comments_readers',
                    'foreignKey' => 'comment_id',
                    'associationForeignKey' => 'reader_id',
                    'unique' => 'keepExisting',
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'finderQuery' => '',
                    'deleteQuery' => '',
                    'insertQuery' => '',
                    'with' => 'SimpleComment.SCCommentReader'
                )
            )
        );
        $model->Comment->bindModel($bindModelReaderParams, false);
    }

}

?>
