<?php

/**
 * SCOwnerBehavior test class.
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun., 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://ssampaio@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Model/Behavior/PasswordBehavior.php $
 * $Id: PasswordBehavior.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 *
 * @package			SimpleComment
 * @package       SimpleComment.Test.Fixture
 */
class SCOwnerFixture extends CakeTestFixture {

    /**
     * name property
     *
     * @var string 'SCOwner'
     */
    public $name = 'SCOwner';

    /**
     * fields property
     *
     * @var array
     */
    public $fields = array(
        'id' => array('type' => 'integer', 'key' => 'primary'),
        'profil_id' => array('type' => 'integer', 'null' => true),
        'name' => array('type' => 'string', 'length' => 255, 'null' => false),
        'created' => 'datetime',
        'modified' => 'datetime'
    );

    /**
     * records property
     *
     * @var array
     */
    public $records = array(
        array('profil_id' => 2, 'name' => 'Init SCOwner 1', 'created' => '2006-11-22 10:38:58', 'modified' => '2006-12-01 13:31:26'),
        array('profil_id' => 1, 'name' => 'Disp SCOwner', 'created' => '2006-11-22 10:43:13', 'modified' => '2006-11-30 18:38:10'),
        array('profil_id' => 2, 'name' => 'Val Edit', 'created' => '2006-12-25 05:13:36', 'modified' => '2006-12-25 05:23:24'),
        array('profil_id' => 2, 'name' => 'Test Name', 'created' => '2006-12-25 05:23:36', 'modified' => '2006-12-25 05:23:36'),
        array('profil_id' => 4, 'name' => 'My new SCOwner', 'created' => '2006-12-25 05:29:39', 'modified' => '2006-12-25 05:29:39')
    );

}
