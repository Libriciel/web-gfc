<?php

/**
 * SimpleComment Reader behavior class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package			SimpleComment
 * @subpackage		SimpleComment.Model.Behavior
 */
App::uses('SCComment', 'SimpleComment.Model');
App::uses('SCOwnerBehavior', 'SimpleComment.Model/Behavior');

/**
 * Classe SCOwnerBehaviorTest.
 *
 * @package app.Test.Case.Model.Behavior
 */
class SCOwnerBehaviorTest extends CakeTestCase {

    /**
     * Modèle Owner utilisé par ce test.
     *
     * @var Model
     */
    public $Owner = null;

    /**
     *
     * @var type
     */
    public $Comment = null;

    /**
     * Fixtures utilisés par ces tests unitaires.
     *
     * @var array
     */
    public $fixtures = array(
        'plugin.SimpleComment.SCOwner'
    );

    /**
     * PrÃ©paration du test.
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->Owner = ClassRegistry::init('Owner');
        $this->Owner->Behaviors->attach('SimpleComment.SCOwner');
    }

    /**
     * Nettoyage postérieur au test.
     *
     * @return void
     */
    public function tearDown() {
        unset($this->Owner);
        parent::tearDown();
    }

    /**
     *
     */
    public function testSetup() {
        //$bindModelCommentParams
        $bindModelCommentParams = array(
            'hasMany' => array(
                'Comment' => array(
                    'className' => 'SimpleComment.SCComment',
                    'foreignKey' => 'owner_id',
                    'dependent' => false,
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
                )
            )
        );

        //!empty hasMany
        $checkHasManyKey = !empty($this->Owner->hasMany);
        $this->assertEqual($checkHasManyKey, true, var_export($this->Owner, true));
        //equals
        $this->assertEqual($this->Owner->hasMany['Comment'], $bindModelCommentParams['hasMany']['Comment'], var_export($this->Owner->hasMany['Comment'], true));




        //$bindModelOwnerParams
        $bindModelOwnerParams = array(
            'belongsTo' => array(
                'Owner' => array(
                    'className' => $this->Owner->name,
                    'foreignKey' => 'owner_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'counterCache' => ''
                )
            )
        );

        //!empty belongsTo
        $checkBelongsToKey = !empty($this->Owner->Comment->belongsTo);
        $this->assertEqual($checkBelongsToKey, true, var_export($this->Owner->Comment->belongsTo, true));

        //equals
        $this->assertEqual($this->Owner->Comment->belongsTo['Owner'], $bindModelOwnerParams['belongsTo']['Owner'], var_export($this->Owner->Comment->belongsTo['Owner'], true));
    }

}

?>
