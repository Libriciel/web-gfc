<?php

/**
 * SimpleComment Reader behavior class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package			SimpleComment
 * @subpackage		SimpleComment.Model.Behavior
 */
App::uses('SCComment', 'SimpleComment.Model');
App::uses('SCTargetBehavior', 'SimpleComment.Model/Behavior');

/**
 * Classe SCTargetBehaviorTest.
 *
 * @package app.Test.Case.Model.Behavior
 */
class SCTargetBehaviorTest extends CakeTestCase {

    /**
     * Modèle Target utilisé par ce test.
     *
     * @var Model
     */
    public $Target = null;

    /**
     *
     * @var type
     */
    public $Comment = null;

    /**
     * Fixtures utilisés par ces tests unitaires.
     *
     * @var array
     */
    public $fixtures = array(
        'plugin.SimpleComment.SCTarget'
    );

    /**
     * PrÃ©paration du test.
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->Target = ClassRegistry::init('Target');
        $this->Target->Behaviors->attach('SimpleComment.SCTarget');
    }

    /**
     * Nettoyage postérieur au test.
     *
     * @return void
     */
    public function tearDown() {
        unset($this->Target);
        parent::tearDown();
    }

    /**
     *
     */
    public function testSetup() {

        $bindModelCommentParams = array(
            'hasMany' => array(
                'Comment' => array(
                    'className' => 'SimpleComment.SCComment',
                    'foreignKey' => 'target_id',
                    'dependent' => false,
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
                )
            )
        );

        //!empty hasMany
        $checkHasManyKey = !empty($this->Target->hasMany);
        $this->assertEqual($checkHasManyKey, true, var_export($this->Target, true));
        //equals
        $this->assertEqual($this->Target->hasMany['Comment'], $bindModelCommentParams['hasMany']['Comment'], var_export($this->Target->hasMany['Comment'], true));




        //$bindModelTargetParams
        $bindModelTargetParams = array(
            'belongsTo' => array(
                'Target' => array(
                    'className' => $this->Target->name,
                    'foreignKey' => 'target_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'counterCache' => ''
                )
            )
        );

        //!empty belongsTo
        $checkBelongsToKey = !empty($this->Target->Comment->belongsTo);
        $this->assertEqual($checkBelongsToKey, true, var_export($this->Target->Comment->belongsTo, true));

        //equals
        $this->assertEqual($this->Target->Comment->belongsTo['Target'], $bindModelTargetParams['belongsTo']['Target'], var_export($this->Target->Comment->belongsTo['Target'], true));
    }

}

?>
