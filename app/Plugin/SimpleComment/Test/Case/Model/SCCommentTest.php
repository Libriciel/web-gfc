<?php

/**
 * SimpleComment Reader behavior class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package			SimpleComment
 * @subpackage		SimpleComment.Model.Behavior
 */
App::uses('SCComment', 'SimpleComment.Model');

/**
 * Group Test Case
 *
 */
class SCCommentTest extends CakeTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = array('plugin.SimpleComment.SCComment');

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->SCComment = ClassRegistry::init('SimpleComment.SCComment');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->SCComment);
        parent::tearDown();
    }

    /**
     *
     */
    public function testBeforeSave() {
        $this->SCComment->data = array(
            'SCComment' => array(
                'slug' => '',
                'content' => 'ceci est un test',
                'private' => true,
                'owner_id' => 1,
                'target_id' => 1
            )
        );
        $expected = Hash::merge($this->SCComment->data, array('SCComment' => array('slug' => 'ceci_est_un_test')));

        $this->SCComment->beforeSave();
        $result = $this->SCComment->data;

        //vérification de la longueur de la chaïne slug
        $this->assertEqual(strlen($result['SCComment']['slug']) <= 255, true, var_export($result, true));

        //vérification de la transformation de la chaïne slug
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    public function testBeforeSaveWithEmptyDataSet() {
        //empty data test
        $this->SCComment->data = array();
        $this->SCComment->beforeSave();
        $expected = array();
        $result = $this->SCComment->data;
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    public function testBeforeSaveWithEmptyFields() {
        //empty fields test
        $this->SCComment->data = array(
            'SCComment' => array(
                'slug' => '',
                'content' => '',
                'private' => true,
                'owner_id' => 1,
                'target_id' => 1
            )
        );
        $this->SCComment->beforeSave();
        $result = $this->SCComment->data;
        $this->assertEqual(empty($this->SCComment->data['SCComment']['slug']), true, var_export($result, true));
    }

}
