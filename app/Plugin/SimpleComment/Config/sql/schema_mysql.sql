create table comments(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`slug` varchar(255),
	`content` blog,
	`active` varchar(2) not null default '0',
	`owner_id` int(11),
	`target_id` int(11),
	`created` datetime,
	`modified` datetime,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table comments_readers(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`reader_id` int(11),
	`comment_id` int(11) not null references comments(id) on update cascade on delete cascade,
	`created` datetime,
	`modified` datetime,
	PRIMARY KEY (`id`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;