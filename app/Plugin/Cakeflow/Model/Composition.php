<?php

/**
 * Modele des cmpositions
 */
class Composition extends CakeflowAppModel {

  public $name = 'Composition';
  public $tablePrefix = 'wkf_';

  /**   * *******************************************************************
   * 	Associations
   * ** ****************************************************************** */
  public $belongsTo = array(
      'Cakeflow.Etape',
      CAKEFLOW_TRIGGER_MODEL => array(
          'className' => CAKEFLOW_TRIGGER_MODEL,
          'foreignKey' => 'trigger_id'
      )
  );

  /**   * *******************************************************************
   * 	Règles de validation
   * ** ****************************************************************** */
  public $validate = array(
      'etape_id' => array(
          array(
              'rule' => 'notBlank',
              'required' => true,
              'message' => 'Champ obligatoire'
          )
      ),
      'trigger_id' => array(
          array(
              'rule' => 'notBlank',
              'required' => true,
              'message' => 'Champ obligatoire'
          )
      ),
      'type_validation' => array(
          array(
              'rule' => 'notBlank',
              'required' => true,
              'message' => 'Champ obligatoire'
          )
      ),
      'type_composition' => array(
            array(
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            )
        )

  );

  /**
   * Retourne true si la validation de la composition est de type Signature
   * @param integer $etapeId id de l'étape de la composition
   * @param integer $userIr id de l'utilisateur de la composition
   * @return boolean true si signature, false dans le cas contraire
   */
  public function validationParSignature($etapeId, $userId) {
    $data = $this->find('first', array(
        'recursive' => -1,
        'fields' => array('type_validation'),
        'conditions' => array(
            'etape_id' => $etapeId,
            'trigger_id' => $userId)));
    return ($data['Composition']['type_validation'] == 'S');
  }

  /**
   * Retourne la liste des types de validation
   * @return array code, libelle de la liste des type de composition
   */
  public function listeTypeValidation() {
    return array(
        'V' => __('Visa', true),
        'S' => __('Signature', true),
        'D' => __('Délégation de validation', true),
        'P' => __('Délégation à PASTELL', true));
  }

  /**
   * Retourne le libellé du type de validation
   * @param string $code_type lettre S ou V
   */
  public function libelleTypeValidation($code_type) {
    $typesValid = $this->listeTypeValidation();
    return $typesValid[$code_type];
  }

      /**
     * Retourne la liste des types de composition
     * @return array code, libelle de la liste des types de composition
     */
    function listeTypes()
    {
        $ret = array(
            'USER' => __('Utilisateur de ', true) .' '. CAKEFLOW_APP
        );

		// Pour i-Parapheur
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$hasParapheurActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true
				),
				'contain' => false
			)
		);
		$protocole = $hasParapheurActif['Connecteur']['signature_protocol'];
		$this->set('hasParapheurActif', $hasParapheurActif);
		if (!empty($hasParapheurActif) && $protocole == 'IPARAPHEUR') {
            $ret['PARAPHEUR'] = __('Parapheur électronique', true);
        }
        // Pour PASTELL
		$hasPastellActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%PASTELL%',
					'Connecteur.use_pastell'=> true
				),
				'contain' => false
			)
		);
		$this->set('hasPastellActif', $hasPastellActif);
		if (!empty($hasPastellActif)) {
			$ret['PASTELL'] = __('PASTELL', true);
		}
        return $ret;
    }

}

?>
