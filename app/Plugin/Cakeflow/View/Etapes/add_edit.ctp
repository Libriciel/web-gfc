<?php

echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
    $formEtape = array(
        'name' => 'Etape',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-5',
        'input' => array(
            'Etape.circuit_id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            ),
            'Etape.nom' => array(
                'labelText' => __('Nom', true),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
            'Etape.description' => array(
                'labelText' =>__('Description', true),
                'inputType' => 'textarea',
                'items'=>array(
                    'type'=>'textarea'
                )
            ),
            'Etape.type' => array(
                'labelText' =>__('Type', true),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'options' => $types,
                    'required'=>true,
                    'title' => "- Type d'étape - \nSimple: accord requis \nConcurrent: l'accord d'un seul suffit \nCollaboratif: accord de tous requis"
                )
            ),
            'Etape.cpt_retard' => array(
                'labelText' =>__('Nombre de jours avant retard', true),
                'inputType' => 'number',
                'items'=>array(
                    'type'=>'number',
                    'options' => $types,
                    'title' => 'Nombre de jours avant la date de fin de traitement de l\'étape pour déclencher l\'alerte de retard',
                    'min' => '0',
                    'max' => $retard_max
                )
            )
        )
    );
    if ($this->action == 'edit') {
        $formEtape['input']['Etape.id'] = array(
            'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
        );
        $formEtape['input']['Etape.ordre'] = array(
            'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value'=>$this->request->data['Etape']['ordre']
                )
        );
        $formEtape['input']['retardInf'] = array(
            'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
        );
    }
?>
<div class="panel panel-default" id="addEtape">
    <div class="panel-heading">
        Ajouter une étape
    </div>
    <div class="panel-body">
        <?php
        echo $this->Formulaire->createForm($formEtape);
        if (!empty($retard_max)){
            echo $this->Html->tag('p',"* Maximum : $retard_max jours", array('title'=>'La valeur pour cette étape ne peut pas dépasser celle des étapes précédentes'));
        }
        echo $this->Form->end();
        ?>
    </div>
    <div class="panel-footer" role="group" style="width: 100%">
    </div>
</div>

<?php
echo $this->Html->script('Cakeflow.etapes');
?>
<script type="text/javascript">
    var circuitId = $('#EtapeCircuitId').val();

    function chargerTab() {
        if ($('#circuit_tabs_2').length > 0 && $('#circuit_tabs_2').is(':visible')) {
            gui.request({
                url: "<?php echo "/workflows/setSchema/"; ?>" + circuitId,
                updateElement: $('#circuit_tabs_2'),
            });
        }
        if ($('#circuit_tabs_3').length > 0 && $('#circuit_tabs_3').is(':visible')) {
            gui.request({
                url: "<?php echo "/workflows/setSchema/"; ?>" + circuitId,
                updateElement: $('#circuit_tabs_3'),
            });
        }
    }

    gui.addbuttons({
        element: $('#addEtape .panel-footer'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse btn-secondary",
                action: function () {
                    $('#addEtape').remove();
                    chargerTab();
					$('div.panel-heading div.controls').show();
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success btn-secondary",
                action: function () {
                    var form = $(this).parents('#addEtape').find('form');
                    var url = '/cakeflow/Etapes/add/' + circuitId;
                    <?php if ($this->action == 'edit'): ?>
                    url = form.attr('action');
                    <?php endif; ?>
                    // $('#addEtape').remove();
                    if (form_validate(form)) {
                        gui.request({
                            url: url,
                            data: form.serialize()
                        }, function () {
                            chargerTab();
                        });
						$('div.controls').show();
                    } else {
                        sweetAlert(
                                'Oops...',
                                'Veuillez vérifier votre formulaire!',
                                'error'
                                )
                        $('#infos #tabs').tabs('select', errorFormTabs[0]);
                    }
                }
            }
        ]
    });
    $('#EtapeType').select2();

    $('.control-group + p').css({'float': 'left', 'margin-top': '5px', 'color': 'red'});

	$('div.panel-heading div.controls').hide();
</script>
