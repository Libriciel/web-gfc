<?php

/*
 * Created on 24 janv. 09
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class MyhtmlHelper extends HtmlHelper {
    
    var $helpers = array('Html');
    

    function bouton($url, $title = null, $confirmMessage = false, $path = null, $height = "24", $width = "24") {
        /* Initialisation du title si il est vide */
        if (empty($title) && !empty($url['action'])) {
            if ($url['action'] == 'view')
                $title = __('Visualiser', true);
            elseif ($url['action'] == 'add')
                $title = __('Ajouter', true);
            elseif ($url['action'] == 'edit')
                $title = __('Modifier', true);
            elseif ($url['action'] == 'delete')
                $title = __('Supprimer', true);
        }
        /* Initialisation du path de l'icone si il est vide */
        if (empty($path) && !empty($url['action'])) {
            if ($url['action'] == 'view')
                $path = '/img/icons/visualiser.png';
            elseif ($url['action'] == 'add')
                $path = '/img/icons/ajouter.png';
            elseif ($url['action'] == 'edit')
                $path = '/img/icons/modifier.png';
            elseif ($url['action'] == 'delete')
                $path = '/img/icons/supprimer.png';
        }

        return $this->Html->link(
                $this->Html->image($path, array('border' => "0", 'height' => $height, 'width' => $width, 'alt' => $title)),
                $url,
                array('title' => $title, 'escape' => false),
                $confirmMessage
        );
    }

}

?>
