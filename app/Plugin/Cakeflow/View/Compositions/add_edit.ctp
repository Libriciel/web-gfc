<?php

$formCompositions = array(
    'name' => 'Composition',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-5',
    'form_url' => Router::url(null, false),
    'input' => array(
        'Composition.etape_id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Composition.type_composition' => array(
            'labelText' =>'Type de composition',
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'required'=> true,
                'empty' => ( $canAddParapheur || $canAddPastell ),
                'options' =>$typeCompositions
            )
        ),
        'Composition.trigger_id' => array(
            'labelText' =>CAKEFLOW_TRIGGER_TITLE,
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'empty' => true,
                'required' => true,
                'options' =>$triggers
            )
        )
    )
);

if ($this->action == 'edit'){
    $formCompositions['input']['Composition.id'] =array(
        'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
    );
}

if ($canAddParapheur){
    $formCompositions['input']['soustype'] =array(
        'labelText' => __( 'Sous-Types de "'.$hasParapheurActif['Connecteur']['type'].'"', true ),
        'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
				'required'=> true,
                'empty' => true,
                'options' => isset( $soustypes['soustype'] ) ? $soustypes['soustype'] : $soustypes
            )
    );
}

if ($canAddPastell){
	$formCompositions['input']['type_document'] =array(
			'labelText' => __( 'Cheminements Pastell disponibles', true ),
			'inputType' => 'select',
			'items'=>array(
					'type'=>'select',
					'empty' => true,
					'required'=> true,
					'options' => $documents
			)
	);
	$formCompositions['input']['inforequired_type_document'] =array(
		'labelText' => __( 'Sous-type Parapheur', true ),
		'inputType' => 'select',
		'items'=>array(
			'type'=>'select',
			'empty' => true,
			'required'=> true,
			'options' => @$listSoustypeIp['soustype']
		)
	);
}
if(CAKEFLOW_GERE_SIGNATURE){
    $formCompositions['input']['Composition.type_validation'] =array(
        'labelText' => 'Type de validation',
        'inputType' => 'radio',
            'items'=>array(
                'type'=>'radio',
                'options'=>$typeValidations
            )
    );
}else{
    $formCompositions['input']['Composition.type_validation'] =array(
        'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value'=>'V'
            )
    );
}
?>

<div class="panel panel-default" id="composition_add">
    <div class="panel-heading">
        Ajouter un utilisateur
    </div>
    <div class="panel-body">
        <?php
        echo $this->Formulaire->createForm($formCompositions);
        echo $this->Form->end();
        ?>
    </div>
    <div class="panel-footer" role="group" style="width:100%">
    </div>
</div>

<script type="text/javascript">
    var circuitId = "<?php echo $circuit_id; ?>";
    function chargerTab() {
        if ($('#circuit_tabs_3').length > 0 && $('#circuit_tabs_3').is(':visible')) {
            gui.request({
                url: "<?php echo "/workflows/setSchema/"; ?>" + circuitId,
                updateElement: $('#circuit_tabs_3'),
            });
        }
    }

    gui.addbuttons({
        element: $('#composition_add .panel-footer'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse btn-secondary",
                action: function () {
                    $('#composition_add').remove();
                    chargerTab();
					$('div.controls').show();
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success btn-secondary",
                action: function () {

                    var form = $(this).parents('#composition_add').find('form');
                    // $('#composition_add').remove();
                    if (form_validate(form)) {
                        gui.request({
                            url: form.attr('action'),
                            data: form.serialize()
                        }, function (data) {
                            chargerTab();
                        });

						$('div.panel-heading div.controls').show();
                    } else {
                        sweetAlert(
                                'Oops...',
                                'Veuillez vérifier votre formulaire!',
                                'error'
                                )
                    }
                }
            }
        ]
    });

    $(document).ready(function () {
    $('#CompositionTriggerId').select2({allowClear: true, placeholder: "Sélectionner un bureau"});


    $('#CompositionTypeComposition').select2({allowClear: true, placeholder: "Sélectionner un type de composition"});
    $('#CompositionSoustype').select2({allowClear: true, placeholder: "Sélectionner un sous-type"});
    $('#CompositionTypeDocument').select2({allowClear: true, placeholder: "Sélectionner un cheminement"});
    $('#CompositionInforequiredTypeDocument').select2({allowClear: true, placeholder: "Sélectionner un sous-type"});
    $('#selectUser').select2();
        $("<div id='userDiv'>").insertBefore($('#CompositionTypeComposition').parents('.form-group'));
        $("#userDiv").append($('#CompositionTypeComposition').parents('.form-group'));
        $("<div id='selectUser'>").insertBefore($('#CompositionTriggerId').parents('.form-group'));
        $("#selectUser").append($('#CompositionTriggerId').parents('.form-group'));
        $("<div id='soustype'>").insertBefore($('#CompositionSoustype').parents('.form-group'));
        $("#soustype").append($('#CompositionSoustype').parents('.form-group'));
		$("<div id='type_document'>").insertBefore($('#CompositionTypeDocument').parents('.form-group'));
		$("#type_document").append($('#CompositionTypeDocument').parents('.form-group'));
		$("<div id='inforequired_type_document'>").insertBefore($('#CompositionInforequiredTypeDocument').parents('.form-group'));
		$("#inforequired_type_document").append($('#CompositionInforequiredTypeDocument').parents('.form-group'));

        $("#boutonValider").hide();
        <?php
        if (!$canAddParapheur){
            echo "$(\"option[value='PARAPHEUR']\").hide();";
        }
		if (!$canAddPastell){
			echo "$(\"option[value='PASTELL']\").hide();";
		}
        if (CAKEFLOW_GERE_SIGNATURE){
            echo '$("#typeValidation").hide();';
            echo '$("#CompositionTypeValidationD").hide();';
            echo '$("label[for=\'CompositionTypeValidationD\']").hide();';
        }
        ?>
        $('#CompositionTypeComposition').on('change', onChangeAction);
        onChangeAction();



    });


    function onChangeAction() {
        var selectedOption = $('#CompositionTypeComposition').val();
        $('#selectUser').hide();
        $('#soustype').hide();
        $('#type_document').hide();
        $('#inforequired_type_document').hide();
        $('#tmp_parapheur').remove();
        if (selectedOption == '') {
            $('#boutonValider').hide();
        } else {
            if (selectedOption == 'USER') {
                $('#selectUser').show();
                $("input[name='data[Composition][type_validation]']").val('V');
                <?php
                if (CAKEFLOW_GERE_SIGNATURE) {
                    echo '$("#typeValidation").show();';
                }
                ?>
            } else if (selectedOption == 'PARAPHEUR') {
                $('#CompositionTriggerId').append('<option value="-1" id="tmp_parapheur">Parapheur</option>');
                $('#CompositionTriggerId').val("-1");
                $("input[name='data[Composition][type_validation]']").val('D');
                $('#soustype').show();
                $('#selectUser').hide();
                $('#type_document').hide();
                <?php
                if (CAKEFLOW_GERE_SIGNATURE) {
                    echo '$("#typeValidation").hide();';
                }
                ?>
            } else if (selectedOption == 'PASTELL') {
                $('#CompositionTriggerId').append('<option value="-3" id="tmp_parapheur">PASTELL</option>');
                $('#CompositionTriggerId').val("-3");
                $("input[name='data[Composition][type_validation]']").val('P');
                $('#soustype').hide();
                $('#selectUser').hide();
                $('#type_document').show();
                <?php
                if (CAKEFLOW_GERE_SIGNATURE) {
                    echo '$("#typeValidation").hide();';
                }
                ?>
            }
            $('#boutonValider').show();
        }
    }

	$('#CompositionTypeDocument').change(function () {
		if (
			$('#CompositionTypeDocument').val() == 'document-a-signer'
			|| $('#CompositionTypeDocument').val() == 'pdf-generique'
			|| $('#CompositionTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName').'-visa'; ?>'
			|| $('#CompositionTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName').'-mailsec'; ?>'
		) {
			$('#inforequired_type_document').show();
			$('#CompositionInforequiredTypeDocument').addClass('required');

		}
		if ($('#CompositionTypeDocument').val() == 'mailsec' || $('#CompositionTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>') {
			$("#inforequired_type_document").hide();
			$('#CompositionInforequiredTypeDocument').parent().removeClass('required');
		}
	});
	if ($('#CompositionTypeDocument').val() == 'document-a-signer'
		|| $('#CompositionTypeDocument').val() == 'pdf-generique'
		|| $('#CompositionTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName').'-visa'; ?>'
		|| $('#CompositionTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName').'-mailsec'; ?>'
	) {
		$('#inforequired_type_document').show();
		$('#CompositionInforequiredTypeDocument').parent().addClass('required');
	}
	if ($('#CompositionTypeDocument').val() == 'mailsec' || $('#CompositionTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>') {
		$("#inforequired_type_document").hide();
		$('#CompositionInforequiredTypeDocument').parent().removeClass('required');
	}

	$('div.panel-heading div.controls').hide();
</script>
