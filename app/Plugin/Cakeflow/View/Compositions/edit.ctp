<?php

echo $this->Form->create('Composition', array('url' => Router::url(null, false)));
echo $this->Html->tag('div', $this->Form->input('Composition.id', array('type' => 'hidden')));
echo $this->Html->tag('div', $this->Form->input('Composition.etape_id', array('type' => 'hidden')));
echo $this->Form->input('Composition.trigger_id', array('type' => 'select', 'empty' => true, 'required' => true, 'label' => CAKEFLOW_TRIGGER_TITLE));
if (CAKEFLOW_GERE_SIGNATURE) {
  echo $this->Form->input('Composition.type_validation', array('type' => 'radio', 'label' => 'Type de validation'));
} else {
  echo $this->Html->tag('div', $this->Form->input('Composition.type_validation', array('type' => 'hidden', 'value' => 'V')));
}
?>
