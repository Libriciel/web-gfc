<?php

if (!empty($compositions)) {
    $this->Paginator->options(array('url' => $this->passedArgs));
    $cells = '';

    foreach ($compositions as $rownum => $composition) {
        $rows = Set::extract($composition, 'Composition');
        $triggerLibelle = '<td>' . $rows['triggerLibelle'] . '</td>';
        if (empty($parapheur) && $composition['Composition']['trigger_id'] == -1){
            $triggerLibelle = "<td><span style='cursor: help; border-bottom-color: #999; border-bottom-style: dotted; border-bottom-width: 1px;' title='Attention : Cette délégation peut poser problème. \nSolution : Activer le parapheur dans les connecteurs ou modifier/supprimer la composition'>
                <i class='fa fa-warning'  ></i> " . $rows['triggerLibelle'] . "</span></td>";
        }
        $cells .= '<tr>'
					. $triggerLibelle .
					'<td>' . $rows['typeValidationLibelle'] . '</td>
				<td style="text-align:center">
					' . $this->Myhtml->bouton(array('action' => 'view', $composition['Composition']['id']), 'Visualiser', false) . '
					' . $this->Myhtml->bouton(array('action' => 'edit', $composition['Composition']['id']), 'Modifier', false) . '
					' . $this->Myhtml->bouton(array('action' => 'delete', $composition['Composition']['id']), 'Supprimer', sprintf('Réellement supprimer la composition %s ?', $rows['triggerLibelle'])) . '
				</td>
			</tr>';
    }

    $headers = $this->Html->tableHeaders(
            array(
                CAKEFLOW_TRIGGER_TITLE,
                __('Type de validation', true),
                __('Actions', true)
            )
    );

    echo $this->element('indexPageCourante');
    echo $this->Html->tag(
            'table', $this->Html->tag('thead', $headers) . $this->Html->tag('tbody', $cells), array('class' => 'table table-striped')
    );
    echo $this->element('indexPageNavigation');
}
?>

<div class="actions">
    <ul>
        <?php
        if ($canAdd) {
//            echo $this->Html->tag('li', $this->Html->link(__('Ajouter une composition', true), array('action' => 'add', $this->params['pass'][0])));
            ?>
        <script type="text/javascript">
            $('#compoUserEtape').show();
        </script>
            <?php
        } else {
            ?>
        <script type="text/javascript">
            $('#compoUserEtape').hide();
        </script>
            <?php
        }
        ?>

    </ul>
</div>

