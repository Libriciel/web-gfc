<div class="elementBordered2">
<?php
//echo $this->Html->css( 'Cakeflow.design.css'); //FIXME: à voir
// Affichage du titre de la vue
if (!empty($contenuVue['titreVue']))
	echo '<h2>'.$contenuVue['titreVue'].'</h2>';

// Affichage des sections principales
foreach($contenuVue['sections'] as $section) {
	// affichage du titre de la section
	if (!empty($section['titreSection']))
		echo '<h4>'.$section['titreSection'].'</h4>';
	echo '<dl>';
	// Parcours des lignes de la section
	foreach($section['lignes'] as $iLigne => $ligne) {
		echo $this->element('viewLigne', array('ligne'=>$ligne, 'altrow'=>($iLigne & 1)));
	}
	echo '</dl>';
}

// Affichage du lien de retour
echo '<div class=\'actions\'>';
	echo '<ul>';
		echo '<li>';
			echo $this->Html->link('retour aux circuits', $contenuVue['lienRetour']['url'], array(), false, false);
		echo '</li>';
	echo '</ul>';
echo '</div>';
?>
</div>
