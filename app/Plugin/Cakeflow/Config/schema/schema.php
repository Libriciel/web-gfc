<?php 
/* cakeflow schema generated on: 2011-11-09 17:24:51 : 1320855891*/
class cakeflowSchema extends CakeSchema {
	var $name = 'cakeflow';

	function before($event = array()) {
		return true;
	}

	function after($event = array()) {
	}

	var $acos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true),
		'model' => array('type' => 'string', 'null' => true, 'default' => NULL),
		'foreign_key' => array('type' => 'integer', 'null' => true),
		'alias' => array('type' => 'string', 'null' => true, 'default' => NULL),
		'lft' => array('type' => 'integer', 'null' => true),
		'rght' => array('type' => 'integer', 'null' => true),
		'indexes' => array('PRIMARY' => array('unique' => true, 'column' => 'id'), 'acos_alias_idx' => array('unique' => false, 'column' => 'alias'), 'acos_lft_idx' => array('unique' => false, 'column' => 'lft'), 'acos_parent_id_idx' => array('unique' => false, 'column' => 'parent_id'), 'acos_rght_idx' => array('unique' => false, 'column' => 'rght')),
		'tableParameters' => array()
	);
	var $aros = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
		'parent_id' => array('type' => 'integer', 'null' => true),
		'model' => array('type' => 'string', 'null' => true, 'default' => NULL),
		'foreign_key' => array('type' => 'integer', 'null' => true),
		'alias' => array('type' => 'string', 'null' => true, 'default' => NULL),
		'lft' => array('type' => 'integer', 'null' => true),
		'rght' => array('type' => 'integer', 'null' => true),
		'indexes' => array('PRIMARY' => array('unique' => true, 'column' => 'id'), 'aros_model_alias_idx' => array('unique' => true, 'column' => array('model', 'alias')), 'aros_alias_idx' => array('unique' => false, 'column' => 'alias'), 'aros_foreign_key_idx' => array('unique' => false, 'column' => 'foreign_key'), 'aros_lft_idx' => array('unique' => false, 'column' => 'lft'), 'aros_model_idx' => array('unique' => false, 'column' => 'model'), 'aros_parent_id_idx' => array('unique' => false, 'column' => 'parent_id'), 'aros_rght_idx' => array('unique' => false, 'column' => 'rght')),
		'tableParameters' => array()
	);
	var $aros_acos = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
		'aro_id' => array('type' => 'integer', 'null' => false),
		'aco_id' => array('type' => 'integer', 'null' => false),
		'_create' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2),
		'_read' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2),
		'_update' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2),
		'_delete' => array('type' => 'string', 'null' => false, 'default' => '0', 'length' => 2),
		'indexes' => array('PRIMARY' => array('unique' => true, 'column' => 'id'), 'aro_aco_key' => array('unique' => true, 'column' => array('aro_id', 'aco_id')), 'aros_aros_aro_id_aco_id_idx' => array('unique' => true, 'column' => array('aro_id', 'aco_id')), 'aros_acos_aco_id_idx' => array('unique' => false, 'column' => 'aco_id'), 'aros_acos_aro_id_idx' => array('unique' => false, 'column' => 'aro_id'), 'aros_aros_aco_id_idx' => array('unique' => false, 'column' => 'aco_id'), 'aros_aros_aro_id_idx' => array('unique' => false, 'column' => 'aro_id')),
		'tableParameters' => array()
	);
	var $wkf_circuits = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
		'nom' => array('type' => 'string', 'null' => false, 'length' => 250),
		'description' => array('type' => 'binary', 'null' => true),
		'actif' => array('type' => 'integer', 'null' => false, 'default' => '1'),
		'defaut' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'created_user_id' => array('type' => 'integer', 'null' => false),
		'modified_user_id' => array('type' => 'integer', 'null' => true),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'collectivite_id' => array('type' => 'integer', 'null' => true),
		'indexes' => array('PRIMARY' => array('unique' => true, 'column' => 'id'), 'wkf_cicruits_unique_nom' => array('unique' => true, 'column' => 'nom')),
		'tableParameters' => array()
	);
	var $wkf_compositions = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
		'etape_id' => array('type' => 'integer', 'null' => false),
		'type_validation' => array('type' => 'string', 'null' => false, 'length' => 1),
		'created_user_id' => array('type' => 'integer', 'null' => true),
		'modified_user_id' => array('type' => 'integer', 'null' => true),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'trigger_id' => array('type' => 'integer', 'null' => true),
		'indexes' => array('PRIMARY' => array('unique' => true, 'column' => 'id')),
		'tableParameters' => array()
	);
	var $wkf_etapes = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
		'circuit_id' => array('type' => 'integer', 'null' => false),
		'nom' => array('type' => 'string', 'null' => false, 'length' => 250),
		'description' => array('type' => 'binary', 'null' => true),
		'type' => array('type' => 'integer', 'null' => false),
		'ordre' => array('type' => 'integer', 'null' => false),
		'created_user_id' => array('type' => 'integer', 'null' => false),
		'modified_user_id' => array('type' => 'integer', 'null' => true),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'indexes' => array('PRIMARY' => array('unique' => true, 'column' => 'id')),
		'tableParameters' => array()
	);
	var $wkf_signatures = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
		'type_signature' => array('type' => 'string', 'null' => false, 'length' => 100),
		'signature' => array('type' => 'binary', 'null' => false),
		'indexes' => array('PRIMARY' => array('unique' => true, 'column' => 'id')),
		'tableParameters' => array()
	);
	var $wkf_traitements = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
		'circuit_id' => array('type' => 'integer', 'null' => false),
		'target_id' => array('type' => 'integer', 'null' => false),
		'numero_traitement' => array('type' => 'integer', 'null' => false, 'default' => '1'),
		'created_user_id' => array('type' => 'integer', 'null' => true),
		'modified_user_id' => array('type' => 'integer', 'null' => true),
		'created' => array('type' => 'datetime', 'null' => true),
		'modified' => array('type' => 'datetime', 'null' => true),
		'treated' => array('type' => 'boolean', 'null' => true),
		'indexes' => array('PRIMARY' => array('unique' => true, 'column' => 'id'), 'wkf_traitements_target_id_key' => array('unique' => false, 'column' => 'target_id')),
		'tableParameters' => array()
	);
	var $wkf_visas = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 11, 'key' => 'primary'),
		'traitement_id' => array('type' => 'integer', 'null' => false),
		'trigger_id' => array('type' => 'integer', 'null' => false),
		'signature_id' => array('type' => 'integer', 'null' => true),
		'etape_nom' => array('type' => 'string', 'null' => true, 'length' => 250),
		'etape_type' => array('type' => 'integer', 'null' => false),
		'action' => array('type' => 'string', 'null' => false, 'length' => 2),
		'commentaire' => array('type' => 'string', 'null' => true, 'length' => 500),
		'date' => array('type' => 'datetime', 'null' => true),
		'type_validation' => array('type' => 'string', 'null' => false, 'length' => 1),
		'numero_traitement' => array('type' => 'integer', 'null' => false),
		'indexes' => array('PRIMARY' => array('unique' => true, 'column' => 'id'), 'wkf_visas_traitement_id_key' => array('unique' => false, 'column' => 'traitement_id'), 'wkf_visas_trigger_id_key' => array('unique' => false, 'column' => 'trigger_id')),
		'tableParameters' => array()
	);
}
?>