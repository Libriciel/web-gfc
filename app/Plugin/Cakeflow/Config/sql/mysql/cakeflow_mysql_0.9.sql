-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny4
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Jeu 02 Septembre 2010 à 16:35
-- Version du serveur: 5.0.51
-- Version de PHP: 5.2.6-1+lenny9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `webdelib`
--

-- --------------------------------------------------------

--
-- Structure de la table `wkf_circuits`
--

CREATE TABLE IF NOT EXISTS `wkf_circuits` (
  `id` int(11) NOT NULL auto_increment,
  `nom` varchar(250) collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci,
  `actif` tinyint(1) NOT NULL default '1',
  `defaut` tinyint(1) NOT NULL default '0',
  `created_user_id` int(11) NOT NULL,
  `modified_user_id` int(11) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `nom` (`nom`),
  KEY `created_user_id` (`created_user_id`),
  KEY `modified_user_id` (`modified_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `wkf_circuits`
--

INSERT INTO `wkf_circuits` (`id`, `nom`, `description`, `actif`, `defaut`, `created_user_id`, `modified_user_id`, `created`, `modified`) VALUES
(1, 'Validation', '', 1, 0, 0, NULL, '2010-08-18 10:15:09', '2010-08-18 10:15:09'),
(2, 'Collaboratif', '', 1, 0, 0, NULL, '2010-08-19 16:11:03', '2010-08-19 16:11:03'),
(3, 'Circuit 1', 'Ce circuit est destiné au service Transport', 1, 1, 0, NULL, '2010-08-31 16:00:01', '2010-08-31 16:00:01');

-- --------------------------------------------------------

--
-- Structure de la table `wkf_compositions`
--

CREATE TABLE IF NOT EXISTS `wkf_compositions` (
  `id` int(11) NOT NULL auto_increment,
  `etape_id` int(11) NOT NULL,
  `type_validation` varchar(1) collate utf8_unicode_ci NOT NULL,
  `user_id` int(10) default NULL,
  PRIMARY KEY  (`id`),
  KEY `etape_id` (`etape_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Contenu de la table `wkf_compositions`
--

INSERT INTO `wkf_compositions` (`id`, `etape_id`, `type_validation`, `user_id`) VALUES
(1, 1, 'V', 2),
(2, 2, 'V', 3),
(3, 2, 'V', 4),
(4, 3, 'V', 5),
(5, 3, 'V', 6),
(6, 4, 'V', 7),
(7, 3, 'V', 9),
(9, 5, 'V', 5),
(10, 6, 'V', 7),
(11, 5, 'V', 6),
(12, 7, 'V', 8),
(13, 8, 'V', 3),
(14, 9, 'V', 4),
(15, 10, 'V', 6),
(16, 10, 'V', 7),
(17, 11, 'V', 10),
(18, 11, 'V', 11);

-- --------------------------------------------------------

--
-- Structure de la table `wkf_etapes`
--

CREATE TABLE IF NOT EXISTS `wkf_etapes` (
  `id` int(11) NOT NULL auto_increment,
  `circuit_id` int(11) NOT NULL,
  `nom` varchar(250) collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci,
  `type` int(10) NOT NULL,
  `ordre` int(11) NOT NULL,
  `created_user_id` int(11) NOT NULL,
  `modified_user_id` int(11) default NULL,
  `created` datetime default NULL,
  `modified` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `circuit_id` (`circuit_id`),
  KEY `nom` (`nom`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Contenu de la table `wkf_etapes`
--

INSERT INTO `wkf_etapes` (`id`, `circuit_id`, `nom`, `description`, `type`, `ordre`, `created_user_id`, `modified_user_id`, `created`, `modified`) VALUES
(1, 1, 'Première étape', '', 1, 1, 0, NULL, '2010-08-18 10:15:21', '2010-08-18 10:15:21'),
(2, 1, 'Deuxième étape', '', 2, 2, 0, NULL, '2010-08-18 10:15:31', '2010-08-18 10:15:58'),
(3, 1, 'Troisième étape', '', 3, 3, 0, NULL, '2010-08-18 10:15:46', '2010-08-18 10:16:05'),
(4, 1, 'Quatrième et fin', '', 1, 4, 0, NULL, '2010-08-18 10:16:19', '2010-08-18 10:16:19'),
(5, 2, 'fisrt', '', 3, 1, 0, NULL, '2010-08-19 16:11:18', '2010-08-19 16:11:18'),
(6, 2, 'last', '', 1, 2, 0, NULL, '2010-08-19 16:11:27', '2010-08-19 16:11:27'),
(8, 3, 'nom de l''étape 1 du circuit 1', '', 1, 1, 0, NULL, '2010-08-31 16:06:55', '2010-08-31 16:06:55'),
(9, 3, 'Etape 2 du circuit 1', '', 1, 2, 0, NULL, '2010-09-01 17:33:07', '2010-09-01 17:33:07'),
(10, 3, 'Etape 3 du circuit 1', '', 2, 3, 0, NULL, '2010-09-01 17:34:40', '2010-09-01 17:34:40'),
(11, 3, 'Etape 4 du circuit 1', '', 3, 4, 0, NULL, '2010-09-01 17:36:31', '2010-09-01 17:36:31');

-- --------------------------------------------------------

--
-- Structure de la table `wkf_signatures`
--

CREATE TABLE IF NOT EXISTS `wkf_signatures` (
  `id` int(11) NOT NULL auto_increment,
  `type_signature` varchar(100) collate utf8_unicode_ci NOT NULL,
  `signature` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `wkf_signatures`
--


-- --------------------------------------------------------

--
-- Structure de la table `wkf_traitements`
--

CREATE TABLE IF NOT EXISTS `wkf_traitements` (
  `id` int(11) NOT NULL auto_increment,
  `circuit_id` int(11) NOT NULL,
  `etape_id` int(11) NOT NULL,
  `user_id` int(10) default NULL,
  `archive_id` int(11) NOT NULL,
  `numero_traitement` int(11) NOT NULL default '1',
  `created` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `wkf_traitements`
--


-- --------------------------------------------------------

--
-- Structure de la table `wkf_visas`
--

CREATE TABLE IF NOT EXISTS `wkf_visas` (
  `id` int(11) NOT NULL auto_increment,
  `traitement_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `composition_id` int(11) NOT NULL,
  `etape_id` int(11) NOT NULL,
  `signature_id` int(11) NOT NULL,
  `action` varchar(1) collate utf8_unicode_ci NOT NULL,
  `commentaire` varchar(500) collate utf8_unicode_ci NOT NULL,
  `numero_traitement` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `wkf_visas`
--


