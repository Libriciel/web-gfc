-- --------------------------------------------------------
--
-- Table `wkf_visas`
--
ALTER TABLE `wkf_visas` CHANGE `action` `action` VARCHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

UPDATE `webdelib`.`wkf_visas` SET `action` = 'RI' WHERE `wkf_visas`.`action` = 'I';
UPDATE `webdelib`.`wkf_visas` SET `action` = 'OK' WHERE `wkf_visas`.`action` = 'A';
UPDATE `webdelib`.`wkf_visas` SET `action` = 'KO' WHERE `wkf_visas`.`action` = 'R';
UPDATE `webdelib`.`wkf_visas` SET `action` = 'IL' WHERE `wkf_visas`.`action` = 'L';
UPDATE `webdelib`.`wkf_visas` SET `action` = 'IP' WHERE `wkf_visas`.`action` = 'P';

ALTER TABLE `webdelib`.`wkf_visas` ADD INDEX `traitement_id` ( `traitement_id` );
ALTER TABLE `webdelib`.`wkf_visas` ADD INDEX `trigger_id` ( `trigger_id` );


-- --------------------------------------------------------
--
-- Table `wkf_traitements`
--
ALTER TABLE `webdelib`.`wkf_traitements` ADD INDEX `target_id` ( `target_id` );
