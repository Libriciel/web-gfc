<?php

class EtapesController extends CakeflowAppController {

  public $name = 'Etapes';

  public $components = array('Cakeflow.VueDetaillee', 'Paginator');
  public $helpers = array('Text', 'Cakeflow.Myhtml');
   public $uses = array('Cakeflow.Circuit', 'Cakeflow.Etape', 'Cakeflow.Visa', 'Cakeflow.Composition', 'Cakeflow.Traitement');


  // Gestion des droits
//  public $commeDroit = array('index' => 'Circuits:index',
//      'add' => 'Circuits:index',
//      'delete' => 'Circuits:index',
//      'edit' => 'Circuits:index',
//      'view' => 'Circuits:index',
//      'moveUp' => 'Circuits:index',
//      'moveDown' => 'Circuits:index');

  /**   * *******************************************************************
   *
   * ** ****************************************************************** */
    public function index($circuit_id=null) { // FIXME: Circuit.service_id
        $this->set('ariane', array('Administration', 'Gestion des circuits', 'Etapes'));
        $this->Etape->Circuit->id = $circuit_id;
        if ($this->Etape->Circuit->exists($circuit_id)) {
            $this->set('circuit', $this->Etape->Circuit->field('nom'));
            $this->Etape->Behaviors->attach('Containable');
            $this->paginate = array(
                'fields' => array(
                    'Etape.ordre',
                    'Etape.nom',
                    'Etape.description',
                    'Etape.type',
                    'Etape.soustype',
                    'Etape.cpt_retard'
                ),
                'contain' => array('Composition.trigger_id'),
                'order' => array('Etape.ordre' => 'ASC')
            );
            $etapes = $this->Paginator->paginate('Etape', array('Etape.circuit_id' => $circuit_id));
            //Si le circuit est vide, rediriger vers la vue d'ajout d'étape
            if (empty($etapes) && stripos($this->previous, 'etapes/add') === false)
                $this->redirect(array('action' => 'add', $circuit_id));

            // Mise en forme pour chaque ligne
            foreach ($etapes as &$etape) {
                $etape['ListeActions']['delete'] = $this->Etape->isDeletable($etape['Etape']['id']);
                $etape['Etape']['libelleType'] = $this->Etape->types[$etape['Etape']['type']];
                $etape['Etape']['libelleSousType'] = '';
                foreach ($etape['Composition'] as &$composition) {
                    $composition['libelleTrigger'] = $this->formatLinkedModel('Trigger', $composition['trigger_id']);
                }
            }
            $nbrEtapes = $this->Etape->find('count', array('conditions' => array('Etape.circuit_id' => $circuit_id)));
            $this->set(compact('etapes', 'nbrEtapes'));
        }else{
            $this->Session->setFlash('Circuit introuvable, id erroné', 'growl');
//            return $this->redirect(array('controller'=>'circuit', 'action'=>'index'));
        }
    }

  /**   * *******************************************************************
   *
   * ** ****************************************************************** */
  public function view($id = null) {
    $this->set('ariane', array('Administration', 'Gestion des circuits', 'Etapes', 'Vue détaillée de l\'étape'));
    
    $this->request->data = $this->{$this->modelClass}->find('first', array(
        'conditions' => array('id' => $id),
        'recursive' => -1));
    if (empty($this->request->data)) {
      $this->Session->setFlash(__('Invalide id pour l\'', true) . ' ' . __('étape', true) . ' : ' . __('affichage de la vue impossible.', true), 'growl', array('type' => 'important'));
//      $this->redirect(array('action' => 'index'));
    } else {
      $this->pageTitle = Configure::read('appName') . ' : ' . __('Etapes des circuits de traitement', true) . ' : ' . __('vue détaillée', true);

      // lecture du circuit de l'étape
      $circuit = $this->{$this->modelClass}->Circuit->find('first', array(
          'recursive' => -1,
          'fields' => array('id', 'nom'),
          'conditions' => array('id' => $this->request->data[$this->modelClass]['circuit_id'])));

      // préparation des informations à afficher dans la vue détaillée
      $maVue = new $this->VueDetaillee(
                      __('Vue détaillée de l\'etape \'', true) . $this->request->data[$this->modelClass]['nom'] . '\' du circuit \'' . $circuit['Circuit']['nom'] . '\'',
                      __('Retour à la liste des étapes', true),
                      array('action' => 'index', $circuit['Circuit']['id']));
      $maVue->ajouteSection(__('Informations principales', true));
      $maVue->ajouteLigne(__('Identifiant interne (id)', true), $this->request->data[$this->modelClass]['id']);
      $maVue->ajouteLigne(__('Nom', true), $this->request->data[$this->modelClass]['nom']);
      $maVue->ajouteLigne(__('Description', true), $this->request->data[$this->modelClass]['description']);
      $maVue->ajouteLigne(__('Ordre', true), $this->request->data[$this->modelClass]['ordre']);
      $maVue->ajouteLigne(__('Type', true), $this->{$this->modelClass}->libelleType($this->request->data[$this->modelClass]['type']));
      $maVue->ajouteSection(__('Création / Modification', true));
      $maVue->ajouteLigne(__('Date de cr&eacute;ation', true), $this->request->data[$this->modelClass]['created']);
      $maVue->ajouteElement(__('Par', true), $this->formatUser($this->request->data[$this->modelClass]['created_user_id']));
      $maVue->ajouteLigne(__('Date de dernière modification', true), $this->request->data[$this->modelClass]['modified']);
      $maVue->ajouteElement(__('Par', true), $this->formatUser($this->request->data[$this->modelClass]['modified_user_id']));

      // Affichage des compositions
      $compositions = $this->{$this->modelClass}->Composition->find('all', array(
          'conditions' => array('Composition.etape_id' => $id),
          'fields' => array('Composition.trigger_id', 'Composition.type_validation')
              ));
      If (!empty($compositions)) {
        $maVue->ajouteSection(__('Composition de l\'étape', true));
        foreach ($compositions as $composition) {
          $maVue->ajouteLigne($this->formatLinkedModel('Trigger', $composition['Composition']['trigger_id']), $this->Etape->Composition->libelleTypeValidation($composition['Composition']['type_validation']));
        }
      }

      $this->set('contenuVue', $maVue->getContenuVue());
    }
  }

  /**   * *******************************************************************
   *
   * ** ****************************************************************** */
  public function add($circuit_id = null) { // FIXME: !empty find + service_id
    $this->set('ariane', array('Administration', 'Gestion des circuits', 'Etapes', 'Nouvelle étape'));
    
    if (empty($circuit_id) || !$this->Etape->Circuit->exists($circuit_id)){
        $this->Session->setFlash('Circuit introuvable.', 'growl');
//        return $this->redirect(array('controller'=>'circuits', 'action' => 'index'));
    }

    $nbrEtapes = $this->Etape->find('count', array('conditions' => array('Etape.circuit_id' => $circuit_id)));
    $etapeMinCptRetard = $this->Etape->find('first', array(
        'conditions' => array('Etape.circuit_id' => $circuit_id),
        'fields' => array('MIN(Etape.cpt_retard) as retardmax')
    ));
    $retardMax = $etapeMinCptRetard[0]['retardmax'];
    $this->set('retard_max', $retardMax);
    if (!empty($this->data)) {
        //Vérification cpt_retard valide
        if (!empty($retardMax) && $this->request->data['Etape']['cpt_retard'] > $retardMax){
            $this->Session->setFlash('Erreur lors de l\'enregistrement : le compteur de retard a une valeur supèrieure à celui d\'une étape précédente', 'growl');
        } else {
            $this->request->data['Etape']['ordre'] = $nbrEtapes + 1;
            $this->Etape->create();
            $this->setCreatedModifiedUser($this->request->data, 'Etape');
            if ($this->Etape->save($this->request->data)) {
                $this->Session->setFlash('Enregistrement effectuée.', 'growl');
                return $this->redirect(array('action' => 'index', $circuit_id));
            } else {
                $this->Session->setFlash('Erreur lors de l\'enregistrement.', 'growl');
            }
        }
    }else{
        $this->request->data['Etape']['circuit_id'] = $circuit_id;
    }
    $this->set('types', $this->Etape->types);
    $this->render('add_edit');

//    if (!empty($this->request->data)) {
//      $nbrEtapes = $this->Etape->find('count', array('conditions' => array('Etape.circuit_id' => Set::extract($this->request->data, 'Etape.circuit_id'))));
//      $this->request->data['Etape']['ordre'] = $nbrEtapes + 1;
//      $this->request->data['Etape']['circuit_id'] = $id;
//
//      $this->setCreatedModifiedUser($this->request->data);
//      $this->Etape->create($this->request->data);
//      if ($this->Etape->validates($this->request->data)) {
//        if ($this->Etape->save()) {
//          $this->set('reloadWkf', $this->request->data['Etape']['circuit_id']);
//        }
//      } else {
//        debug('error');
//      }
//    } else {
//      $this->request->data['Etape']['circuit_id'] = $id;
//    }
//    $this->set('types', $this->Etape->listeType());
  }

  /**   * *******************************************************************
   *
   * ** ****************************************************************** */
  public function edit($id = null) { // FIXME: !empty find + service_id
    $this->set('ariane', array('Administration', 'Gestion des circuits', 'Etapes', 'Edition d\'une étape'));
    
     if (empty($id) || !$this->Etape->exists($id)){
        $this->Session->setFlash('Etape introuvable.', 'growl');
        return $this->redirect(array('controller'=>'circuits', 'action' => 'index'));
    }

    $etape = $this->Etape->find('first', array(
        //'recursive' => -1,
        'fields' => array_merge(
            $this->Etape->fields()
            //$this->Etape->Composition->fields()
        ),
        'contain' => array(
            'Composition'
        ),
        'conditions' => array('Etape.id'=>$id)
     ));

    if(count($etape['Composition'])<2){
        $types = $this->Etape->types;
        $this->set('types', $types);
    }else{
        $types = array_slice($this->Etape->types,1,2,true);
        $this->set('types', $types);
        $this->set('mess',"noTypeSimple");
        
    }
    $recordRetardMax = $this->Etape->find('first', array(
        'conditions' => array(
            'Etape.circuit_id' => $etape['Etape']['circuit_id'],
            'Etape.ordre <' => $etape['Etape']['ordre'],
            'Etape.cpt_retard >=' => 0
        ),
        'fields' => array('MIN(Etape.cpt_retard) as retardmax')
    ));
    $this->set('retard_max', $recordRetardMax[0]['retardmax']);
    $recordRetardInf = $this->Etape->find('first', array(
        'conditions' => array(
            'Etape.circuit_id' => $etape['Etape']['circuit_id'],
            'Etape.ordre <' => $etape['Etape']['ordre'],
            'Etape.cpt_retard !=' => 0
        ),
        'fields' => array('MAX(Etape.cpt_retard) as retardinf')
    ));
    $this->set('retard_inf', $recordRetardInf[0]['retardinf']);
    
     if (!empty($this->data)) {
        //Vérification cpt_retard valide
        if ($recordRetardMax[0]['retardmax'] != null && $this->request->data['Etape']['cpt_retard'] > $recordRetardMax[0]['retardmax']){
            $this->Session->setFlash('Erreur lors de l\'enregistrement : le compteur de retard a une valeur supèrieure à celui d\'une étape précédente', 'growl');
        } else {
            $this->Etape->begin();
            try{
                // Si nouvelle affectation ou diminution de cpt_retard
                if ( $etape['Etape']['cpt_retard'] > $this->request->data['Etape']['cpt_retard']
                    || (empty($etape['Etape']['cpt_retard']) && !empty($this->request->data['Etape']['cpt_retard']))){
                    $etapeSuivante = $this->Etape->find('first', array(
                        'recursive' => -1,
                        'fields' => array('Etape.cpt_retard'),
                        'conditions' => array(
                            'Etape.circuit_id' => $this->request->data['Etape']['circuit_id'],
                            'Etape.ordre' => $this->request->data['Etape']['ordre']+1
                        )
                    ));
                    if (!empty($etapeSuivante)){
                        $decalage = $etapeSuivante['Etape']['cpt_retard'] - $this->request->data['Etape']['cpt_retard'];
                        if ($decalage > 0){
                            $etapesSuivante = $this->Etape->find('all', array(
                                'recursive' => -1,
                                'conditions' => array(
                                    'Etape.circuit_id' => $this->request->data['Etape']['circuit_id'],
                                    'Etape.ordre >' => $this->request->data['Etape']['ordre']
                                ),
                                'order' => 'Etape.ordre ASC'
                            ));
                            foreach($etapesSuivante as $etapeSuivante){
                                if (!empty($etapeSuivante['Etape']['cpt_retard'])){
                                    $etapeSuivante['Etape']['cpt_retard'] -= $decalage;
                                    if ($etapeSuivante['Etape']['cpt_retard'] < 0)
                                        $etapeSuivante['Etape']['cpt_retard'] = 0;
                                    $this->Etape->id = $etapeSuivante['Etape']['id'];
                                    $this->setCreatedModifiedUser($etapeSuivante, 'Etape');
                                    if (!$this->Etape->save($etapeSuivante)){
                                        throw new Exception('Erreur lors de la modification du compteur des étapes suivante.');
                                    }
                                }
                            }
                        }
                    }
                }
                $this->Etape->id = $id;
                $this->setCreatedModifiedUser($this->request->data, 'Etape');
                if ($this->Etape->save($this->request->data)) {
                    $this->Etape->commit();
                    $this->Session->setFlash('Enregistrement effectuée.', 'growl');
                    return $this->redirect(array('action' => 'index', $etape['Etape']['circuit_id']));
                } else {
                    throw new Exception('Erreur lors de l\'enregistrement de l\étape.');
                }
            }catch (Exception $e){
                $this->Etape->rollback();
                $this->Session->setFlash($e->getMessage(), 'growl');
            }
        }
    }else{
        $this->request->data = $etape;
    }
    $this->set('etape',$etape);
    //$this->set('types', $this->Etape->types);
    $this->render('add_edit');


//    if (!empty($this->request->data)) {
//      $this->setCreatedModifiedUser($this->request->data);
//      $this->Etape->create($this->request->data);
//      if ($this->Etape->validates($this->request->data)) {
//        if ($this->Etape->save()) {
//          $this->set('reloadWkf', $this->request->data['Etape']['circuit_id']);
//        }
//      } else {
//        debug('error');
//      }
//    } else {
//      $this->Etape->id = $id;
//      $this->request->data = $this->Etape->read();
////      $this->request->data = $this->Etape->findById($id, null, null, -1);
//    }
//    $this->set('types', $this->Etape->listeType());
  }

  /**   * *******************************************************************
   *
   * ** ****************************************************************** */
//  public function add_edit($id = null) { // FIXME: transaction pour l'ordre et la sauvegarde
//    if (!empty($this->request->data)) {
//      if ($this->action == 'add') {
//        $nbrEtapes = $this->Etape->find('count', array('conditions' => array('Etape.circuit_id' => Set::extract($this->request->data, 'Etape.circuit_id'))));
//        $this->request->data['Etape']['ordre'] = $nbrEtapes + 1;
//        $this->request->data['Etape']['circuit_id'] = $id;
//      }
//
//      $this->setCreatedModifiedUser($this->request->data);
//      $this->Etape->create($this->request->data);
//      if ($this->Etape->validates($this->request->data)) {
//        if ($this->Etape->save()) {
//          $this->set('reloadWkf', $this->request->data['Etape']['circuit_id']);
//        }
//      } else {
//        debug('error');
//      }
//    } else if ($this->action == 'edit') {
//      $this->request->data = $this->Etape->findById($id, null, null, -1);
//      // $this->assert( !empty( $this->request->data ) );
//    } else if ($this->action == 'add') {
//      $this->request->data['Etape']['circuit_id'] = $id;
//    }
//
//    $this->set('types', $this->Etape->listeType());
//
//    $this->render($this->action, null, 'add_edit');
//  }

  /**   * *******************************************************************
   *
   * ** ****************************************************************** */
  public function delete($id = null) {  // FIXME: !empty find + service_id
    $etape = $this->Etape->find('first', array(
        'conditions' => array('Etape.id' => $id),
        'recursive' => -1,
        'fields' => array('Etape.id', 'Etape.circuit_id', 'Etape.ordre')));

    if ($this->Etape->delete($id)) {
        $etapes = $this->Etape->find('all', array(
            'conditions' => array(
                'Etape.circuit_id' => $etape['Etape']['circuit_id'],
                'Etape.ordre >' => $etape['Etape']['ordre']),
            'fields' => array('Etape.id', 'Etape.ordre'),
            'recursive' => -1));
        foreach ($etapes as $etape) {
            $this->Etape->id = $etape['Etape']['id'];
            $this->Etape->saveField('ordre', $etape['Etape']['ordre'] - 1);
        }
        
        $this->loadModel('Journalevent');
        $datasSession = $this->Session->read('Auth.User');
        $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé l'étape ".$etape['Etape']['id']. " du circuit ". $etape['Etape']['circuit_id'] ." le ".date('d/m/Y à H:i:s');
        $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', array());
        
        $this->Session->setFlash('Suppression effectuée', 'growl');
    } else {
        $this->Session->setFlash('Erreur lors de la suppression', 'growl');
    }
//    return $this->redirect($this->referer());
      
      
//      if ($this->Etape->delete($id)) {
//      $this->Session->setFlash('Suppression effectuée', 'growl');
//    } else {
//      $this->Session->setFlash('Erreur lors de la suppression', 'growl');
//    }
    $this->autoRender = false;

  }

  /**   * *******************************************************************
   *
   * ** ****************************************************************** */
  public function moveUp($id = null) {  // FIXME: !empty find + service_id
    if (!$this->Etape->moveUp($id)) {
      $this->Session->setFlash('Impossible de changer l\'ordre des étapes', 'growl');
    }
    $this->autoRender = false;
//        $this->redirect($this->referer());
  }

  /**   * *******************************************************************
   *
   * ** ****************************************************************** */
  public function moveDown($id = null) {  // FIXME: !empty find + service_id
    if (!$this->Etape->moveDown($id)) {
      $this->Session->setFlash('Impossible de changer l\'ordre des étapes', 'growl');
    }
    $this->autoRender = false;
//        $this->redirect($this->referer());
  }

}

?>
