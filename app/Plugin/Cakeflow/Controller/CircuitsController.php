<?php

class CircuitsController extends CakeflowAppController {

    public $name = 'Circuits';
    public $components = array('VueDetaillee', 'Paginator');
    public $helpers = array('Text');
    public $uses = array('Cakeflow.Circuit', 'Cakeflow.Etape', 'Cakeflow.Visa', 'Cakeflow.Composition', 'Cakeflow.Traitement');
    // Gestion des droits
    public $aucunDroit = array('visuCircuit');
    public $libelleControleurDroit = 'Circuits';
    public $libellesActionsDroit = array(
        'index' => 'Gestion des circuits',
    );
    public $commeDroit = array('add' => 'Circuits:index',
        'edit' => 'Circuits:index',
        'view' => 'Circuits:index',
        'delete' => 'Circuits:index',
        'reorder' => 'Circuits:index');

    /**
     * Liste des circuits de traitement
     */
    public function index() {
        $this->set('ariane', array(__d('menu', 'Administration', true), __d('menu', 'gestionCircuit', true)));
        $this->pageTitle = Configure::read('appName') . ' : ' . __('Circuits de traitement', true) . ' : ' . __('liste', true);
        $this->paginate = array(
            'recursive' => -1,
            'page' => 1,
            'order' => array('Circuit.nom' => 'asc'),
            'fields' => array('id', 'nom', 'description', 'actif', 'defaut'),
        );
        $this->request->data = $this->Paginator->paginate($this->modelClass);

        // lecture des étapes dans l'odre 'ordre'
        foreach ($this->request->data as &$circuit) {
            $circuit['Etape'] = $this->{$this->modelClass}->Etape->find('all', array(
                'recursive' => -1,
                'conditions' => array('circuit_id' => $circuit[$this->modelClass]['id']),
                'fields' => array('nom', 'type'),
                'order' => array('ordre')));
        }

        // mise en forme pour la vue
        foreach ($this->data as $i => $data) {
            $this->request->data[$i]['ListeActions']['view'] = true;
            $this->request->data[$i]['ListeActions']['visuCircuit'] = true;
            $this->request->data[$i]['ListeActions']['edit'] = true;
            $this->request->data[$i]['ListeActions']['delete'] = $this->{$this->modelClass}->isDeletable($data[$this->modelClass]['id']);
            $this->request->data[$i][$this->modelClass]['actifLibelle'] = $this->{$this->modelClass}->boolToString($data[$this->modelClass]['actif']);
            $this->request->data[$i][$this->modelClass]['defautLibelle'] = $this->{$this->modelClass}->boolToString($data[$this->modelClass]['defaut']);
        }

        // types des étapes
//    $this->set('listeType', $this->{$this->modelClass}->Etape->listeType());
        $this->set('listeType', $this->{$this->modelClass}->Etape->types);
    }

    /**
     * Vue détaillée des circuits de traitement
     */
    public function view($id = null) {
        $this->set('ariane', array('Administration', 'Gestion des circuits', 'Vue détaillée du circuit'));
        $this->data = $this->{$this->modelClass}->find('first', array(
            'conditions' => array('Circuit.id' => $id),
            'recursive' => -1));
        if (empty($this->data)) {
            $this->Session->setFlash(__('Invalide id pour le', true) . ' ' . __('circuit de traitement', true) . ' : ' . __('affichage de la vue impossible.', true), 'growl', array('type' => 'important'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->pageTitle = Configure::read('appName') . ' : ' . __('Circuits de traitement', true) . ' : ' . __('vue détaillée', true);

            // préparation des informations à afficher dans la vue détaillée
            $maVue = new $this->VueDetaillee(
                    __('Vue d&eacute;taill&eacute;e du circuit', true) . ' : ' . $this->data[$this->modelClass]['nom'], __('Retour &agrave; la liste des circuits de traitement', true));
            $maVue->ajouteSection(__('Informations principales', true));
            $maVue->ajouteLigne(__('Identifiant interne (id)', true), $this->data[$this->modelClass]['id']);
            $maVue->ajouteLigne(__('Nom', true), $this->data[$this->modelClass]['nom']);
            $maVue->ajouteLigne(__('Description', true), $this->data[$this->modelClass]['description']);
            if (CAKEFLOW_GERE_DEFAUT)
                $maVue->ajouteLigne(__('Défaut', true), $this->{$this->modelClass}->boolToString($this->data[$this->modelClass]['defaut']));
            $maVue->ajouteLigne(__('Actif', true), $this->{$this->modelClass}->boolToString($this->data[$this->modelClass]['actif']));
            $maVue->ajouteSection(__('Création / Modification', true));
            $maVue->ajouteLigne(__('Date de cr&eacute;ation', true), $this->data[$this->modelClass]['created']);
            $maVue->ajouteElement(__('Par', true), $this->formatUser($this->data[$this->modelClass]['created_user_id']));
            $maVue->ajouteLigne(__('Date de derni&egrave;re modification', true), $this->data[$this->modelClass]['modified']);
            $maVue->ajouteElement(__('Par', true), $this->formatUser($this->data[$this->modelClass]['modified_user_id']));

            // Affichage des étapes liées
            $this->{$this->modelClass}->Etape->Behaviors->attach('Containable');
            $etapes = $this->{$this->modelClass}->Etape->find('all', array(
                'conditions' => array('Etape.circuit_id' => $id),
                'contain' => array('Composition.type_validation', 'Composition.trigger_id'),
                'fields' => array('Etape.nom', 'Etape.type'),
                'order' => array('Etape.ordre'),
            ));
            If (!empty($etapes)) {
                $maVue->ajouteSection(__('Etapes du circuit', true));
                foreach ($etapes as $etape) {
                    $maVue->ajouteLigne($etape['Etape']['nom'] . ' (' . $this->{$this->modelClass}->Etape->types[$etape['Etape']['type']] . ')', '', 'viewEtapes');
                    // Affichage des utilisateurs
                    foreach ($etape['Composition'] as $i => $composition)
                        $maVue->ajouteLigne('&nbsp;', 'Composition ' . ($i + 1) . ' : ' . $this->formatLinkedModel('Trigger', $composition['trigger_id']) . ' par ' . $this->Etape->Composition->libelleTypeValidation($composition['type_validation']));
                }
            }

            $this->set('contenuVue', $maVue->getContenuVue());
        }
    }

    /**     * *******************************************************************
     *
     * ** ****************************************************************** */
    public function add() {
        $this->set('ariane', array('Administration', 'Gestion des circuits', 'Nouveau circuit'));
        $this->_add_edit();
    }

    /**     * *******************************************************************
     *
     * ** ****************************************************************** */
    public function edit($id = null) {
        $this->set('ariane', array('Administration', 'Gestion des circuits', 'Edition du circuit'));
        $this->_add_edit($id);
    }

    /**     * *******************************************************************
     *
     * ** ****************************************************************** */
    private function _add_edit($id = null) {
        if (!empty($this->request->data)) {
            $this->setCreatedModifiedUser($this->request->data);
//            debug($this->Session->read('Auth.User.id'));
//            if ($this->action == 'add') {
//                $this->data["Circuit"]["created_user_id"] = $this->Session->read('user.User.id');
//            }
//            $this->data["Circuit"]["modified_user_id"] = $this->Session->read('user.User.id');
//
//      if ($this->action == 'add') {
//        $this->data["Circuit"]["collectivite_id"] = $id;
//      }


            $this->Circuit->create($this->data);
            if ($this->Circuit->validates($this->data)) {
                if ($this->Circuit->save()) {
                    if (empty($id))
                        $this->Session->setFlash(__('Le circuit', true) . ' \'' . $this->data[$this->modelClass]['nom'] . '\' ' . __('a &eacute;t&eacute; ajout&eacute;.', true), 'growl');
                    else
                        $this->Session->setFlash(__('Le circuit', true) . ' \'' . $this->data[$this->modelClass]['nom'] . '\' ' . __('a &eacute;t&eacute; modifi&eacute;.', true), 'growl');
//                    $this->redirect(array('action' => 'index'));
                }
                else {
                    $this->Session->setFlash('Erreur lors de l\'enregistrement.', 'growl', array('type' => 'erreur'));
                }
            } else
                $this->Session->setFlash(__('Veuillez corriger les erreurs du formulaire.', true), 'growl', array('type' => 'erreur'));
        }
        else if ($this->request->action == 'add') {
            $this->request->data['Circuit']['actif'] = true;
        } else if ($this->request->action == 'edit') {
            $this->request->data = $this->Circuit->read(null, $id);
            // $this->assert( !empty( $this->data ) );
        }

//    if ($this->action == 'add') {
//      $this->set('collectivite_id', $id);
//    }
//    $this->render($this->action, null, 'add_edit');
        $this->render('add_edit');
    }

    /**
     * Suppression d'un circuit de traitement
     */
    public function delete($id = null) {
//        $eleASupprimer = $this->{$this->modelClass}->find('first', array(
//                    'conditions' => array('Circuit.id' => $id),
//                    'recursive' => 0));
//        if (empty($eleASupprimer))
//            $this->Session->setFlash(__('Invalide id pour le', true) . ' ' . __('circuit de traitement', true) . ' : ' . __('suppression impossible.', true), 'growl', array('type' => 'important'));
//        elseif (!$this->{$this->modelClass}->isDeletable($id))
//            $this->Session->setFlash(__('Le circuit de traitement', true) . ' \'' . $eleASupprimer[$this->modelClass]['nom'] . '\' ' . __('ne peut pas &ecirc;tre supprim&eacute;.', true), 'growl');
//        elseif (!$this->{$this->modelClass}->delete($id, true))
//            $this->Session->setFlash(__('Une erreur est survenue pendant la suppression', true), 'growl', array('type' => 'erreur'));
//        else {
//            $this->Session->setFlash(__('Le circuit de traitement', true) . ' \'' . $eleASupprimer[$this->modelClass]['nom'] . '\' ' . __('a &eacute;t&eacute; supprim&eacute;.', true), 'growl');
//        }


        if ($id != null && $this->Circuit->isDeletable($id)) {
            $this->Circuit->delete($id);
        }


//        $this->redirect(array('action' => 'index'));
    }

    /**
     * Affiche graphique un circuit de validation
     * Paramètre : id
     */
    public function visuCircuit($circuit_id, $archive_id = null) {
        $this->Connecteur = ClassRegistry::init('Connecteur');
        $parapheur = $this->Connecteur->find(
                'first', array(
            'conditions' => array(
                'Connecteur.name ILIKE' => '%Parapheur%',
				'Connecteur.use_signature'=> true
            ),
            'contain' => false
                )
        );
        $this->set('ariane', array('Administration', 'Gestion des circuits', 'Visualiser le circuit'));
        // lecture des étapes du circuit
        $this->{$this->modelClass}->Etape->Behaviors->attach('Containable');
        $etapes = $this->Circuit->Etape->find('all', array(
            'fields' => array('Etape.nom', 'Etape.ordre', 'Etape.type', 'Etape.soustype'),
            'contain' => array(
                'Composition.type_validation', 'Composition.trigger_id', 'Composition.soustype'),
            'conditions' => array('Etape.circuit_id' => $circuit_id),
            'order' => array('Etape.ordre ASC')));
        foreach ($etapes as &$etape) {
            $etape['Etape']['libelleType'] = $this->{$this->modelClass}->Etape->types[$etape['Etape']['type']];
            foreach ($etape['Composition'] as &$composition) {
                $composition['libelleTypeValidation'] = $this->{$this->modelClass}->Etape->Composition->libelleTypeValidation($composition['type_validation']);
                if ($composition['type_validation'] == 'D' && $composition['soustype'] !== null) {
                    try {
//                $tooltip = Configure::read('IPARAPHEUR_TYPE')." / ".$this->{$this->modelClass}->Etape->libelleSousType($composition['soustype']);
                        $tooltip = $parapheur['Connecteur']['type'] . " / " . $this->{$this->modelClass}->Etape->libelleSousType($composition['soustype'], $parapheur);
                        $composition['libelleTrigger'] = '<a class="infobulle" data-placement="right" data-toggle="tooltip" title="' . $tooltip . '">' . $this->formatLinkedModel('Trigger', $composition['trigger_id']) . "</a>";
                    } catch (Exception $e) {
                        $tooltip = $e->getMessage();
                        $composition['libelleTrigger'] = '<a class="infobulle" data-placement="right" data-toggle="tooltip" title="' . $tooltip . '"><i class="fa fa-warning"  aria-hidden="true"></i> Erreur</a>';
                        $composition['libelleTrigger'] .= '<input type="hidden" class="parapheur_error" value="true" />';
                        $this->Session->setFlash("Problème de connexion au parapheur", 'growl');
                    }
                } else {
                    $composition['libelleTrigger'] = $this->formatLinkedModel('Trigger', $composition['trigger_id']);
                }
//        $composition['libelleTrigger'] = $this->formatLinkedModel('Trigger', $composition['trigger_id']);
            }
        }
        $this->set('etapes', $etapes);
    }

    public function reorder() {
        $ids = explode(',', $this->request->params['pass'][0]);
        for ($i = 0; $i < count($ids); $i++) {
            file_put_contents('/tmp/cakeflow.log', $ids[$i], FILE_APPEND);
            $etape = $this->Etape->read(null, $ids[$i]);
            $etape['Etape']['ordre'] = $i + 1;
            $this->Etape->save($etape['Etape']);
        }
    }

}

?>
