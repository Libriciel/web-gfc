-- schema_mysql.sql script
-- Création des tables nécessaires au plugin Adressbook (MySQL)
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3


create table `addressbooks`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255),
  `description` blob,
  `created` datetime,
  `modified` datetime,
  `foreign_key` int(11),
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table `contacts`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `civilite` varchar(255),
  `name` varchar(255) not null,
  `nom` varchar(255) not null,
  `prenom` varchar(255),
  `created` datetime,
  `modified` datetime,
  `addressbook_id` int(11) not null references addressbooks(id) on update cascade on delete cascade,
  `active` varchar(2) not null default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;



create table `contactinfos`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `civilite` varchar(255),
  `name` varchar(255) not null,
  `nom` varchar(255) not null,
  `prenom` varchar(255),
  `email` varchar(255),
  `adresse` varchar(255),
  `compl` varchar(255),
  `cp` varchar(5),
  `ville` varchar(255),
  `region` varchar(255),
  `pays` varchar(255),
  `tel` varchar(20),
  `role` varchar(255),
  `organisation` varchar(255),
  `created` datetime,
  `modified` datetime,
  `contact_id` int(11) not null references contacts(id) on update cascade on delete cascade,
  `active` varchar(2) not null default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;
