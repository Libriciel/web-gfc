-- schema_mysql.sql script
-- Création des tables nécessaires au plugin Adressbook (PostgreSQL)
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

create table addressbooks(
  id serial not null primary key,
  name character varying(255) unique,
  description bytea,
  created timestamp without time zone default now(),
  modified timestamp without time zone default now(),
  foreign_key integer,
  active boolean not null default true
);

create table contacts(
  id serial not null primary key,
  name character varying(255) not null,
  civilite character varying(255),
  nom character varying(255) not null,
  prenom character varying(255),
  created timestamp without time zone default now(),
  modified timestamp without time zone default now(),
  addressbook_id integer not null references addressbooks(id) on update cascade on delete cascade,
  active boolean not null default true
);

create table contactinfos (
	id serial not null primary key,
	name character varying(255) not null,
	civilite character varying(255),
	nom character varying(255) not null,
	prenom character varying(255),
	email character varying(255),
	adresse character varying(255),
	compl character varying(255),
	cp character varying(5),
	ville character varying(255),
	region character varying(255),
	pays character varying(255),
	tel character varying(20),
	role character varying(255),
	organisation character varying(255),
	active boolean not null default true,
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now(),
	contact_id integer not null references contacts(id) on update cascade on delete cascade
);
