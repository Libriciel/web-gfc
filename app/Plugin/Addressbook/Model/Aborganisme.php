<?php

App::uses('AddressbookAppModel', 'Addressbook.Model');

/**
 * Contact
 *
 * Addressbook Plugin
 * Addressbook Abcontact model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		Addressbook
 * @subpackage	Addressbook.Model
 */
class Aborganisme extends AddressbookAppModel {

	/**
	 * Model useTable
	 *
	 * @access public
	 * @var string
	 */
	public $useTable = 'organismes';

	/**
	 * Validation rules
	 *
	 * @access public
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'addressbook_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed



    public $hasMany = array(
		'Abcontact' => array(
			'className' => 'Abcontact',
			'foreignKey' => 'organisme_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);
	/**
	 *
	 * @var type
	 */
	public $belongsTo = array(
		'Abaddressbook' => array(
			'className' => 'Addressbook',
			'foreignKey' => 'addressbook_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
