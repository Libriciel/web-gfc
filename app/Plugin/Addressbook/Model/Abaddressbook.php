<?php

App::uses('AddressbookAppModel', 'Addressbook.Model');

/**
 * Carnet d'adresse
 *
 * Addressbook Plugin
 * Addressbook Abadressbook model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		Addressbook
 * @subpackage		Addressbook.Model
 */
class Abaddressbook extends AddressbookAppModel {

	/**
	 * Model useTable
	 *
	 * @access public
	 * @var string
	 */
	public $useTable = 'addressbooks';

	/**
	 * hasMany associations
	 *
	 * @access public
	 * @var array
	 */
	public $hasMany = array(
		'Abcontact' => array(
			'className' => 'Abcontact',
			'foreignKey' => 'addressbook_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	//Fonctionnalite d'import obsolète conservée pour info
//	/**
//	 *
//	 * @param type $lines
//	 * @return string
//	 */
//	private function _convertVcards(&$lines) {
//		$cards = array();
//		$card = new VCard();
//
//		while (@$card->parse($lines)) {
//			$property = $card->getProperty('N');
//			if (!$property) {
//				return "";
//			}
//			$n = $property->getComponents();
//			$tmp = array();
//			if (isset($n[3]))
//				$tmp[] = $n[3];   // Mr.
//			if (isset($n[1]))
//				$tmp[] = $n[1];   // John
//			if (isset($n[2]))
//				$tmp[] = $n[2];   // Quinlan
//			if (isset($n[4]))
//				$tmp[] = $n[4];   // Esq.
//			$ret = array();
//			if (isset($n[0]))
//				$ret[] = $n[0];
//			$tmp = join(" ", $tmp);
//			if ($tmp)
//				$ret[] = $tmp;
//			$key = join(", ", $ret);
//			$cards[$key] = $card;
//
//			// MDH: Create new VCard to prevent overwriting previous one (PHP5)
//			$card = new VCard();
//		}
//		ksort($cards);
//		return $cards;
//	}
//
//	/**
//	 *
//	 * @param type $card
//	 * @return type
//	 */
//	private function _checkVcard(&$card) {
//		$checks = array(
//			'fn' => false,
//			'n' => false,
//			'version' => false
//		);
//
//		if (isset($card->_map['FN']) && isset($card->_map['FN'][0]) && isset($card->_map['FN'][0]->value)) {
//			$checks['fn'] = true;
//		}
//		if (isset($card->_map['N']) && isset($card->_map['N'][0]) && isset($card->_map['N'][0]->value)) {
//			$checks['n'] = true;
//		}
//		if (isset($card->_map['VERSION']) && isset($card->_map['VERSION'][0]) && isset($card->_map['VERSION'][0]->value)) {
//			$checks['version'] = true;
//		}
//
//		return !in_array(false, $checks, true);
//	}
//
//	/**
//	 *
//	 * @param type $card
//	 * @return type
//	 */
//	private function _processVcard($card, $addressbook_id, $foreign_key) {
//		//create
//		$contact = array();
//
//		//formatted name
//		$contact['Abcontact']['name'] = $card->_map['FN'][0]->value;
//
//		//names
//		$names = explode(';', $card->_map['N'][0]->value);
//		$contact['Abcontact']['civilite'] = isset($names[3]) ? $names[3] : '';
//		$contact['Abcontact']['nom'] = isset($names[0]) ? $names[0] : '';
//		$contact['Abcontact']['prenom'] = isset($names[1]) ? $names[1] : '';
//
//		//email
//		if (isset($card->_map['EMAIL']) && isset($card->_map['EMAIL'][0])) {
//			$contact['Abcontact']['email'] = $card->_map['EMAIL'][0]->value;
//		}
//
//		//adress
//		if (isset($card->_map['ADR']) && isset($card->_map['ADR'][0])) {
//			$adresses = explode(';', $card->_map['ADR'][0]->value);
//			$contact['Abcontact']['compl'] = isset($adresses[0]) ? $adresses[0] : '';
//			$contact['Abcontact']['compl'] .= isset($adresses[1]) ? ' ' . $adresses[1] : '';
//			$contact['Abcontact']['addresse'] = isset($adresses[2]) ? $adresses[2] : '';
//			$contact['Abcontact']['ville'] = isset($adresses[3]) ? $adresses[3] : '';
//			$contact['Abcontact']['region'] = isset($adresses[4]) ? $adresses[4] : '';
//			$contact['Abcontact']['cp'] = isset($adresses[5]) ? $adresses[5] : '';
//			$contact['Abcontact']['pays'] = isset($adresses[6]) ? $adresses[6] : '';
//		}
//
//		//tel
//		if (isset($card->_map['TEL']) && isset($card->_map['TEL'][0])) {
//			$contact['Abcontact']['tel'] = $card->_map['TEL'][0]->value;
//		}
//
//		//role
//		if (isset($card->_map['ROLE']) && isset($card->_map['ROLE'][0])) {
//			$contact['Abcontact']['role'] = $card->_map['ROLE'][0]->value;
//		}
//		//organisation
//		if (isset($card->_map['ORG']) && isset($card->_map['ORG'][0])) {
//			$contact['Abcontact']['organisation'] = $card->_map['ORG'][0]->value;
//		}
//
//		//addressbook
//		$contact['Abcontact']['addressbook_id'] = $addressbook_id;
//
//		//foreign_key
//		if ($foreign_key != null) {
//			$contact['Abcontact']['foreign_key'] = $foreign_key;
//		}
//
//		return $contact;
//	}
//
//	/**
//	 *
//	 * @param type $filename
//	 * @param type $addressbook_id
//	 * @param type $foreign_key
//	 * @return boolean
//	 */
//	public function importContactVcards($filename = null, $addressbook_id = null, $foreign_key = null) {
//		$return = array();
//		if ($addressbook_id != null) {
//			if ($filename != null && is_file($filename) && is_readable($filename)) {
//				$cards = $this->_convertVcards(file($filename));
//				foreach ($cards as $kcard => $card) {
//					if ($this->_checkVcard($card)) {
//						$contact = $this->_processVcard($card, $addressbook_id, $foreign_key);
//						//save
//						$this->Abcontact->create($contact);
//						$saved = $this->Abcontact->save();
//						$return[] = !empty($saved);
//					}
//				}
//			}
//		}
//		return !in_array(false, $return, true);
//	}
}
