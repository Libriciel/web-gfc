<?php

App::uses('HtmlHelper', 'View/Helper');
App::uses('Inflector', 'Utility');
App::import('Component', 'Acl');

/**
 *
 * Helper CakePHP to create Twitter Bootstrap elements
 * @author AWL
 *
 */
class PermissionsHelper extends AppHelper {
    
    var $helpers = array('Session');
    
    private $Acl = null;
    
    public function __construct(View $view, $settings = array()) {
        $collection = new ComponentCollection();
        $this->Acl = new AclComponent($collection);
        parent::__construct($view, $settings);
    }
    
    function check($path, $permission='*'){
        
        return $this->Acl->check(array( 'model' => 'User', 
                                        'foreign_key' => AuthComponent::user('id')
                ), 
               /* 'controllers/'.*/$path, 
                $permission);
    }
}