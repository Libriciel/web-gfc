<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The AGPL v3 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     https://choosealicense.com/licenses/agpl-3.0/ AGPL v3 License
 */

Configure::write('Pastell', [
    'documents-a-signer' => [
        'field' => [
            'acte_nature' => 'acte_nature',
            'numero_de_lacte' => 'numero_de_lacte',
            'objet' => 'objet',
            'date_de_lacte' => 'date_de_lacte',
            'document_papier' => 'document_papier',
            'arrete' => 'arrete',
            'autre_document_attache' => 'autre_document_attache',
            'classification' => 'classification',
            'type_acte' => 'type_acte',
            'type_pj' => 'type_pj',
            'envoi_signature' => 'envoi_signature',
            'envoi_signature_check' => 'envoi_signature_check',
            'envoi_tdt' => 'envoi_tdt',
            'envoi_ged' => 'envoi_ged',
            'envoi_sae' => 'envoi_sae',
            'iparapheur_type' => 'iparapheur_type',
            'iparapheur_sous_type' => 'iparapheur_sous_type',
            'has_signature' => 'has_signature',
            'is_pades' => 'is_pades',
            'signature' => 'signature',
            'document_signe' => 'document_signe',
            'has_bordereau' => 'has_bordereau',
            'bordereau' => 'bordereau',
            'aractes' => 'aractes',
            'acte_tamponne' => 'acte_tamponne',
            'annexes_tamponnees' => 'annexes_tamponnees'
        ],
        'action' => [
            'send-tdt' => 'send-tdt',
            'verif-tdt' => 'verif-reponse-tdt',
            'verif-reponse-tdt' => 'verif-reponse-tdt',
            'send-iparapheur' => 'send-iparapheur',
            'verif-iparapheur' => 'verif-iparapheur',
            'send-ged' => 'send-ged',
            'send-archive' => 'send-archive',
            'verif-sae' => 'verif-sae',
            'supression' => 'supression'
        ]
    ]
]);

