Bonjour #PRENOM# #NOM#,

Un flux dans votre bannette "Mes flux à traiter" est en retard et attend votre validation.

Id : #IDENTIFIANT_FLUX#
Référence : #REFERENCE_FLUX#
Objet : #OBJET_FLUX#
Circuit : #LIBELLE_CIRCUIT#

Vous pouvez consulter ce flux à l'adresse : #ADRESSE_A_TRAITER#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.