Bonjour #PRENOM# #NOM#,

  Un nouveau commentaire sur le flux #REFERENCE_FLUX# vient de vous être adressé

  Id : #IDENTIFIANT_FLUX#
  Référence : #REFERENCE_FLUX#
  Objet : #OBJET_FLUX#
  Circuit : #LIBELLE_CIRCUIT#

  Vous pouvez consulter ce flux à l'adresse : #ADRESSE_A_TRAITER#

  Très cordialement.

  web-GFC

  Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.