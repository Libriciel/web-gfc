-- 231_patch_1.8.1.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************


ALTER TABLE courriers ALTER COLUMN ref_ancien TYPE INTEGER;

SELECT add_missing_table_field ('public', 'soustypes', 'active', 'BOOLEAN');
ALTER TABLE soustypes ALTER COLUMN active SET DEFAULT '1';
UPDATE soustypes SET active = '1';
SELECT add_missing_table_field ('public', 'types', 'active', 'BOOLEAN');
ALTER TABLE types ALTER COLUMN active SET DEFAULT '1';
UPDATE types SET active = '1';

DROP INDEX IF EXISTS ordresservices_marche_id_numero_idx;

INSERT INTO contacts (organisme_id, name, nom, addressbook_id, numvoie, nomvoie,  adressecomplete, cp, ville, ban_id, bancommune_id, banadresse, compl)
SELECT id, ' Sans contact', '0Sans contact', addressbook_id, numvoie, nomvoie, adressecomplete, cp, ville, ban_id, bancommune_id, banadresse, compl FROM organismes WHERE organismes.id not in (select organisme_id from contacts where name ilike '%Sans contact%');

ALTER TABLE plisconsultatifs ALTER COLUMN numero TYPE integer USING (trim(numero)::integer);
ALTER TABLE ordresservices ALTER COLUMN numero TYPE integer USING (trim(numero)::integer);

SELECT add_missing_table_field ('public', 'users', 'typeabonnement', 'VARCHAR(255)');
ALTER TABLE users ALTER COLUMN typeabonnement SET DEFAULT 'event';
UPDATE users SET typeabonnement = 'event';

-- Table pour les notifs quotidiennes
DROP TABLE IF EXISTS notifquotidiens CASCADE;
CREATE TABLE notifquotidiens (
    id SERIAL NOT NULL PRIMARY KEY,
    notification_id integer  references notifications(id) on update cascade on delete cascade,
    courrier_id integer NOT NULL references courriers(id) on update cascade on delete cascade,
    desktop_id integer references desktops(id) on update cascade on delete cascade,
    user_id integer NOT NULL references users(id) on update cascade on delete cascade,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE notifquotidiens IS 'Table permettant de stocker les notifications quotidiennes';


INSERT INTO templatenotifications VALUES (9, 'quotidien', '[web-GFC]  Récapitulatif journalier de vos flux', 'Bonjour #PRENOM# #NOM#,

Voici la liste des flux vous concernant pour la date du #DATERECEPTION_FLUX#.

Flux :
#REFERENCE_FLUX#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );
COMMIT;
