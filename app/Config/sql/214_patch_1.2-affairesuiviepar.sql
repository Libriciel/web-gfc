-- 214_patch_1.2-affairesuiciepar.sql script
-- Patch de montée de version 1.1 vers 1.2
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

-- *****************************************************************************
-- Nouvelle version de la fonction public.add_missing_constraint qui prend en compte
--		la longueur maximale de 63 caractères pour le nom de la contrainte.
-- Il est à présent possible de passer un 6ème paramètre à false pour avoir SET NULL
--		au lieu de CASCADE lors d'une suppression.
-- ***************************************************************************************

CREATE OR REPLACE FUNCTION public.add_missing_constraint (text, text, text, text, text, bool) RETURNS bool AS
$$
	DECLARE
		p_namespace 		alias for $1;
		p_table     		alias for $2;
		p_constraintname	alias for $3;
		p_foreigntable		alias for $4;
		p_foreignkeyname	alias for $5;
		p_deletecascade		alias for $6;
		v_row       		record;
		v_query     		text;
	BEGIN
		SELECT 1 INTO v_row
		FROM information_schema.table_constraints tc
			LEFT JOIN information_schema.key_column_usage kcu ON (
				tc.constraint_catalog = kcu.constraint_catalog
				AND tc.constraint_schema = kcu.constraint_schema
				AND tc.constraint_name = kcu.constraint_name
			)
			LEFT JOIN information_schema.referential_constraints rc ON (
				tc.constraint_catalog = rc.constraint_catalog
				AND tc.constraint_schema = rc.constraint_schema
				AND tc.constraint_name = rc.constraint_name
			)
			LEFT JOIN information_schema.constraint_column_usage ccu ON (
				rc.unique_constraint_catalog = ccu.constraint_catalog
				AND rc.unique_constraint_schema = ccu.constraint_schema
				AND rc.unique_constraint_name = ccu.constraint_name
			)
		WHERE
			tc.table_schema = p_namespace
			AND tc.table_name = p_table
			AND tc.constraint_type = 'FOREIGN KEY'
			AND tc.constraint_name = substring( p_constraintname from 1 for 63 ) -- INFO: les noms sont juste tronqués, pas de chiffre à la fin -> ça ne devrait pas poser de problème
			AND kcu.column_name = p_foreignkeyname
			AND ccu.table_name = p_foreigntable
			AND ccu.column_name = 'id';

		IF NOT FOUND THEN
			RAISE NOTICE 'Upgrade table %.% - add constraint %', p_namespace, p_table, p_constraintname;
			v_query := 'alter table ' || p_namespace || '.' || p_table || ' add constraint ';
			v_query := v_query || p_constraintname || ' FOREIGN KEY (' || p_foreignkeyname || ') REFERENCES ' || p_foreigntable || '(id)';

			IF p_deletecascade THEN
				v_query := v_query || ' ON DELETE CASCADE ON UPDATE CASCADE;';
			ELSE
				v_query := v_query || ' ON DELETE SET NULL ON UPDATE CASCADE;';
			END IF;

			EXECUTE v_query;
			RETURN 't';
		ELSE
			RETURN 'f';
		END IF;
	END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.add_missing_constraint (text, text, text, text, text, bool) IS 'Add a constraint to a table if it is missing';


--------------------------------------------------------------------------------
-- 20140625: ajout du champa ffairesuiviepar_id pointant sur la table users
--------------------------------------------------------------------------------
SELECT add_missing_table_field ('public', 'courriers', 'affairesuiviepar_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'courriers', 'courriers_affairesuiviepar_id_fkey', 'desktops', 'affairesuiviepar_id', false );

-- *****************************************************************************
COMMIT;
-- *****************************************************************************


