-- 301_patch_2.0.1.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

ALTER INDEX typologiesflux_pkey RENAME TO originesflux_pkey;

ALTER TABLE plisconsultatifs RENAME CONSTRAINT plisconsultatifs_typologieflux_id_fkey TO plisconsultatifs_origineflux_id_fkey;

ALTER SEQUENCE origineflux_id_seq RENAME TO originesflux_id_seq;

COMMIT;
