-- 310_patch_3.0.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

BEGIN;

--- Ajout de champs pour la notion de Chef de
SELECT add_missing_table_field( 'public', 'desktopsmanagers', 'parent_id', 'INTEGER' );

--- Ajout pour le novueau connecteur PASTELL
INSERT INTO connecteurs (name, created, modified) VALUES ( 'PASTELL', '2019-10-21 11:58:03.398832', '2019-10-21 11:58:03.398832');

SELECT add_missing_table_field( 'public', 'connecteurs', 'use_pastell', 'BOOLEAN' );

SELECT add_missing_table_field( 'public', 'courriers', 'pastell_id', 'INTEGER' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'pastell_parapheur_type', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'id_entity', 'INTEGER' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'json_pastell', 'TEXT' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'type_doc_pastell', 'VARCHAR(255)' );

UPDATE bansadresses SET nom_commune = UPPER(nom_commune);
UPDATE banscommunes SET name = UPPER(name);

---- suppression des traitements qui ne sont associés à aucun flux car ce dernier a été refusé
DELETE FROM wkf_traitements WHERE target_id NOT IN (SELECT id FROM courriers);
DELETE FROM comments WHERE target_id NOT IN (SELECT id FROM courriers);
-- et ajou d'une contrainte pour s'assurer que tout est ok pour plus tard
SELECT add_missing_constraint ( 'public', 'wkf_traitements', 'wkf_traitements_target_id_fkey', 'courriers', 'target_id', true );
SELECT add_missing_constraint ( 'public', 'comments', 'comments_target_id_fkey', 'courriers', 'target_id', true );

SELECT add_missing_table_field( 'public', 'documents', 'ocr_data', 'BYTEA' );

--- Ajout pour le nouveau connecteur mail sécurisé (PASTELL)
INSERT INTO connecteurs (name, type_doc_pastell, created, modified) VALUES ( 'Mail Sécurisé', 'mailsec', '2020-03-25 08:58:03.398832', '2020-03-25 08:58:03.398832');

SELECT add_missing_table_field( 'public', 'connecteurs', 'use_mailsec', 'BOOLEAN' );

--- Ajout pour le nouveau connecteur demarches simplifiées (PASTELL)
INSERT INTO connecteurs (name, host, created, modified) VALUES ( 'Démarches-Simplifiées', 'https://www.demarches-simplifiees.fr/api/v1/', '2020-04-03 08:58:03.398832', '2020-04-03 08:58:03.398832');
SELECT add_missing_table_field( 'public', 'connecteurs', 'use_ds', 'BOOLEAN' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'ds_active', 'BOOLEAN' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'ds_apikey', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'ds_desktopmanager_id', 'INTEGER' );

SELECT add_missing_table_field( 'public', 'courriers', 'ds_procedure_id', 'INTEGER' );
SELECT add_missing_table_field( 'public', 'courriers', 'ds_dossier_id', 'INTEGER' );

-- ajout d'un champ dans la table document pour savoir quel document doit être envoyé à la signature
SELECT add_missing_table_field( 'public', 'documents', 'asigner', 'BOOLEAN' );
ALTER TABLE documents ALTER COLUMN asigner SET DEFAULT false;
UPDATE documents set asigner=false where asigner is null;


SELECT add_missing_table_field( 'public', 'connecteurs', 'ldaps_cert', 'BYTEA' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'ldaps_nom_cert', 'VARCHAR(255)' );


-- Table pour la politique de confidentialit
DROP TABLE IF EXISTS rgpds CASCADE;
CREATE TABLE rgpds(
    id SERIAL NOT NULL PRIMARY KEY,
    collectivite_id INTEGER NOT NULL,
    conn VARCHAR(255) NOT NULL,
    nomhebergeur VARCHAR(255) NOT NULL,
    adressecompletehebergeur VARCHAR(255) NOT NULL,
    representanthebergeur VARCHAR(255),
    qualiterepresentanthebergeur VARCHAR(255),
    sirethebergeur VARCHAR(255),
    codeapehebergeur VARCHAR(255),
    numtelhebergeur VARCHAR(14),
    mailhebergeur VARCHAR(255),
    maildpo VARCHAR (255),
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE rgpds IS 'Table permettant de stocker les politiques de confidentialité par collectivités (RGPD)';
ALTER TABLE rgpds OWNER TO webgfc;


--- Ajout pour le nouveau connecteur LsMessage (SMS)
INSERT INTO connecteurs (name, host, created, modified) VALUES ( 'LsMessage (SMS)', '', '2020-06-11 08:58:03.398832', '2020-06-11 08:58:03.398832');
SELECT add_missing_table_field( 'public', 'connecteurs', 'use_sms', 'BOOLEAN' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'sms_active', 'BOOLEAN' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'sms_apikey', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'sms_expediteur', 'VARCHAR(11)' );


-- Table pour les SMS
DROP TABLE IF EXISTS sms CASCADE;
CREATE TABLE sms(
    id SERIAL NOT NULL PRIMARY KEY,
    courrier_id integer NOT NULL,
    numero VARCHAR(12) NOT NULL,
    message VARCHAR (160) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE sms IS 'Table permettant de stocker les SMS envoyés';
ALTER TABLE sms OWNER TO webgfc;


--- Ajout pour le nouveau connecteur DirectMairie (DM)
INSERT INTO connecteurs (name, host, created, modified) VALUES ( 'Direct-Mairie (DM)', '', '2020-06-18 08:58:03.398832', '2020-06-18 08:58:03.398832');
SELECT add_missing_table_field( 'public', 'connecteurs', 'use_dm', 'BOOLEAN' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'dm_active', 'BOOLEAN' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'dm_apikey', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'dm_usermail', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'dm_userpassword', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'connecteurs', 'dm_desktopmanager_id', 'INTEGER' );

SELECT add_missing_table_field( 'public', 'courriers', 'dm_issue_id', 'INTEGER' );

-- ajout des droits sur l'ajout/édition des contacts dans un flux
SELECT add_missing_table_field( 'public', 'rights', 'action_carnet_adresse_flux', 'BOOLEAN' );
UPDATE rights set action_carnet_adresse_flux = true;

-- Colonne pour stocker si on a adressé une notification pur les flux bloqués dans IP ou pas
SELECT add_missing_table_field( 'public', 'courriers', 'mail_bloque_parapheur', 'BOOLEAN' );
ALTER TABLE courriers ALTER COLUMN mail_bloque_parapheur SET DEFAULT false;
UPDATE courriers set mail_bloque_parapheur = false;

SELECT add_missing_table_field( 'public', 'users', 'mail_bloque_parapheur', 'BOOLEAN' );
ALTER TABLE users ALTER COLUMN mail_bloque_parapheur SET DEFAULT true;
UPDATE users set mail_bloque_parapheur = true;

INSERT INTO templatenotifications VALUES ( 11, 'bloque_parapheur', 'Flux non traité présent dans le i-Parapheur', 'Bonjour #PRENOM# #NOM#,

Un flux est présent dans le i-Parapheur depuis + d''1 semaine.

Objet : #OBJET_FLUX#
Référence : #REFERENCE_FLUX#
Commentaire: #COMMENTAIRE_FLUX#

Le flux peut être consulté à cette adresse : #ADRESSE_A_MODIFIER#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );


ALTER TABLE courriers ALTER COLUMN pastell_id TYPE VARCHAR (7);

-- pour mieux gérer les sous-types issus du iParapheur
ALTER TABLE wkf_compositions ALTER COLUMN soustype TYPE VARCHAR(255);
ALTER TABLE wkf_etapes ALTER COLUMN soustype TYPE VARCHAR(255);
ALTER TABLE wkf_visas ALTER COLUMN soustype TYPE VARCHAR(255);

SELECT add_missing_table_field( 'public', 'desktopsmanagers', 'profil_id', 'INTEGER' );

INSERT INTO types (id, name, created, modified) VALUES ('-1', 'Flux sans traitement', NOW(), NOW());
INSERT INTO soustypes (id, type_id, name, created, modified) VALUES ('-1', '-1', 'Clôture automatique', NOW(), NOW());

INSERT INTO dacos (id, parent_id, model, foreign_key, alias, lft, rght) VALUES ( '-1', null, 'Type', '-1', 'Flux_sans_traitement.-1', '3', '6');
INSERT INTO dacos (id, parent_id, model, foreign_key, alias, lft, rght) VALUES ( '-2', '-1', 'Soustype', '-1', 'Cloture_automatique.-2', '4', '5');


--
-- Name: documents_name_path_courrier_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace:
--

-- CREATE UNIQUE INDEX documents_name_ext_path_courrier_id_idx ON documents USING btree (name, ext, path, courrier_id);


SELECT add_missing_table_field( 'public', 'taches', 'creator_desktop_id', 'INTEGER' );
COMMIT;
