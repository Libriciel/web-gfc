-- 203_patch_1.1.sql script
-- Patch de montée de version 1.0.4 vers 1.1
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

alter table bancontenus add column read boolean not null default false;
update bancontenus set read = true where etat != 0;


--------------------------------------------------------------------------------
-- 20140108: Ajout d'une contrainte d'unicité sur la table courriers_repertoires
-- afin de ne pas pouvoir associer un même courrier dans un même répertoire + d'1 fois
--------------------------------------------------------------------------------
CREATE UNIQUE INDEX courriers_repertoires_courrier_id_repertoire_id_idx ON courriers_repertoires(courrier_id,repertoire_id);

--------------------------------------------------------------------------------
-- 20140113: Ajout du délai de traitement dans la table courriers
--------------------------------------------------------------------------------
ALTER TABLE courriers ADD COLUMN delai_nb SMALLINT;
ALTER TABLE courriers ADD COLUMN delai_unite SMALLINT;

UPDATE courriers SET delai_nb = (SELECT delai_nb FROM soustypes WHERE courriers.soustype_id = soustypes.id);
UPDATE courriers SET delai_unite = (SELECT delai_unite FROM soustypes WHERE courriers.soustype_id = soustypes.id);
-- *****************************************************************************
COMMIT;
-- *****************************************************************************
