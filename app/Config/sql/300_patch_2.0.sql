-- 230_patch_1.8.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

-- Renommage de la table typologiesflux en originesflux
ALTER TABLE typologiesflux RENAME TO originesflux;

ALTER  SEQUENCE typologiesflux_id_seq RENAME TO origineflux_id_seq;
SELECT pg_catalog.setval('origineflux_id_seq', ( SELECT max(originesflux.id) + 1 FROM originesflux ), false);

ALTER TABLE courriers RENAME COLUMN typologieflux_id TO origineflux_id;

ALTER TABLE plisconsultatifs RENAME COLUMN typologieflux_id TO origineflux_id;
DROP INDEX IF EXISTS plisconsultatifs_typologieflux_id_idx;
CREATE INDEX plisconsultatifs_origineflux_id_idx ON plisconsultatifs( origineflux_id );

UPDATE documents SET mime='application/pdf' WHERE mime='application/x-octetstream';
UPDATE ars SET mime='application/pdf' WHERE mime='application/x-octetstream';
UPDATE armodels SET mime='application/pdf' WHERE mime='application/x-octetstream';
UPDATE relements SET mime='application/pdf' WHERE mime='application/x-octetstream';
UPDATE gabaritsdocuments SET mime='application/pdf' WHERE mime='application/x-octetstream';

-- Ajout des droits dispenv pour tous les aiguilleurs
INSERT INTO aros_acos (aro_id, aco_id, _create, _read, _update, _delete) (
 SELECT id, (SELECT id FROM acos WHERE alias ILIKE '%dispenv%'), 1, 1, 1, 1 FROM aros where id = 7 );

-- ajout de l'email pour les services
SELECT add_missing_table_field ('public', 'services', 'email', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'services', 'active', 'BOOLEAN');
ALTER TABLE services ALTER COLUMN active SET DEFAULT '1';
UPDATE services SET active = '1';


update contacts set civilite = '1' where civilite='Monsieur';
update contacts set civilite = '2' where civilite='Madame';
update contacts set civilite = '3' where civilite='Monsieur et Madame';



INSERT INTO templatenotifications VALUES (10, 'clos', '[web-GFC]  Un flux dont vous êtes l''initiateur a été clos', 'Bonjour #PRENOM# #NOM#,

Le flux #REFERENCE_FLUX#, dont vous êtes l''initiateur vient d''être clos

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );

SELECT add_missing_table_field ('public', 'users', 'mail_clos', 'BOOLEAN');
ALTER TABLE users ALTER COLUMN mail_clos SET DEFAULT 'true';
UPDATE users SET mail_clos = true;


SELECT add_missing_table_field ('public', 'courriers', 'mail_clos_envoye', 'BOOLEAN');
ALTER TABLE courriers ALTER COLUMN mail_clos_envoye SET DEFAULT 'false';
UPDATE courriers SET mail_clos_envoye = false;


COMMIT;
