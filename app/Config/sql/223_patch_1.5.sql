-- 223_patch_1.5.sql script
-- Patch permettant la mise en place des référentiels FANTOIR
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

DROP TABLE IF EXISTS desktopsmanagers CASCADE;
CREATE TABLE desktopsmanagers (
    id  SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);

DROP TABLE IF EXISTS desktops_desktopsmanagers CASCADE;
CREATE TABLE desktops_desktopsmanagers (
    id  SERIAL NOT NULL PRIMARY KEY,
    desktop_id INTEGER REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE SET NULL,
    desktopmanager_id INTEGER REFERENCES desktopsmanagers(id) ON UPDATE CASCADE ON DELETE SET NULL,
    created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);
DROP INDEX IF EXISTS desktops_desktopsmanagers_desktop_id_idx;
CREATE INDEX desktops_desktopsmanagers_desktop_id_idx ON desktops_desktopsmanagers( desktop_id );

DROP INDEX IF EXISTS desktops_desktopsmanagers_desktopmanager_id_idx;
CREATE INDEX desktops_desktopsmanagers_desktopmanager_id_idx ON desktops_desktopsmanagers( desktopmanager_id );

DROP INDEX IF EXISTS desktops_desktopsmanagers_desktop_id_desktopmanager_id_idx;
CREATE UNIQUE INDEX desktops_desktopsmanagers_desktop_id_desktopmanager_id_idx ON desktops_desktopsmanagers(desktop_id,desktopmanager_id);

-- On insère dans la nouvelle table les données liées aux rôles existants.
-- Pour un rôle donné on crée un bureau uniquement si le rôle n'est pas admin, disp ou init
INSERT INTO desktopsmanagers ( id, name, active )  (SELECT id, name, active FROM desktops where group_id NOT IN (2,3,7));
-- On met à jour le nom du bureau en ajoutant la chaîne de caractère Bureau devant
UPDATE desktopsmanagers SET name=concat( 'Bureau ', name);
-- On associe le rôle de l'utilisateur avec le bureau nouvellement créé
INSERT INTO desktops_desktopsmanagers (desktop_id, desktopmanager_id ) (SELECT id, id FROM desktopsmanagers );


-- Création d'une table pour stocker les mails envoyés individuellement
DROP TABLE IF EXISTS sendsmails CASCADE;
CREATE TABLE sendsmails(
    id  SERIAL NOT NULL PRIMARY KEY,
    email VARCHAR(255) NOT NULL,
    sujet VARCHAR(255),
    message TEXT,
    desktop_id INTEGER REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE SET NULL,
    courrier_id INTEGER REFERENCES courriers(id) ON UPDATE CASCADE ON DELETE SET NULL,
    document_id INTEGER REFERENCES documents(id) ON UPDATE CASCADE ON DELETE SET NULL,
    created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    modified TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);

DROP INDEX IF EXISTS sendsmails_desktop_id_idx;
CREATE INDEX sendsmails_desktop_id_idx ON sendsmails(desktop_id);

DROP INDEX IF EXISTS sendsmails_courrier_id_idx;
CREATE INDEX sendsmails_courrier_id_idx ON sendsmails(courrier_id);
DROP INDEX IF EXISTS sendsmails_document_id_idx;
CREATE INDEX sendsmails_document_id_idx ON sendsmails(document_id);


DROP TABLE IF EXISTS rmodelgabarits;
CREATE TABLE rmodelgabarits (
    id SERIAL NOT NULL PRIMARY KEY,
    name character varying(255) NOT NULL,
    gedpath text,
    content bytea,
    size integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    soustype_id INTEGER NOT NULL REFERENCES soustypes(id) ON UPDATE CASCADE ON DELETE SET NULL,
    mime character varying(255),
    preview bytea,
    ext character varying(10),
    path character varying(255)
);
DROP INDEX IF EXISTS rmodelgabarits_soustype_id_idx;
CREATE INDEX rmodelgabarits_soustype_id_idx ON rmodelgabarits(soustype_id);


SELECT add_missing_table_field ('public', 'sendsmails', 'ccmail', 'VARCHAR(255)');

/*
    Pour intégration BAN : augmenter le ini_set("memory_limit","3600M");
*/
ALTER TABLE banscommunes ALTER COLUMN name TYPE VARCHAR(255);
ALTER TABLE bansadresses ALTER COLUMN name TYPE VARCHAR(255);

SELECT add_missing_table_field ('public', 'contactinfos', 'portable', 'VARCHAR(20)');
COMMIT;
