-- 315_patch_3.1.11.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

BEGIN;

-- Valeurs ajoutées pour stocker le ratio appliqué pour le délai de retard selon l'origine du flux
SELECT add_missing_table_field( 'public', 'originesflux', 'ratio', 'NUMERIC' );
SELECT add_missing_table_field( 'public', 'scanemails', 'authentication', 'VARCHAR(10)' );
UPDATE scanemails SET authentication = 'imap';
SELECT add_missing_table_field( 'public', 'scanemails', 'grant_type', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'scanemails', 'scope', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'scanemails', 'client_secret', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'scanemails', 'client_id', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'scanemails', 'access_token', 'TEXT' );
SELECT add_missing_table_field( 'public', 'scanemails', 'url_api', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'scanemails', 'expires_in', 'TIMESTAMP WITHOUT TIME ZONE' );
COMMIT;
