--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: fuzzystrmatch; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS fuzzystrmatch WITH SCHEMA public;


--
-- Name: EXTENSION fuzzystrmatch; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION fuzzystrmatch IS 'determine similarities and distance between strings';


SET search_path = public, pg_catalog;

--
-- Name: add_missing_constraint(text, text, text, text, text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION add_missing_constraint(text, text, text, text, text, boolean) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
p_namespace   alias for $1;
p_table       alias for $2;
p_constraintname alias for $3;
p_foreigntable  alias for $4;
p_foreignkeyname alias for $5;
p_deletecascade  alias for $6;
v_row         record;
v_query       text;
BEGIN
SELECT 1 INTO v_row
FROM information_schema.table_constraints tc
LEFT JOIN information_schema.key_column_usage kcu ON (
tc.constraint_catalog = kcu.constraint_catalog
AND tc.constraint_schema = kcu.constraint_schema
AND tc.constraint_name = kcu.constraint_name
)
LEFT JOIN information_schema.referential_constraints rc ON (
tc.constraint_catalog = rc.constraint_catalog
AND tc.constraint_schema = rc.constraint_schema
AND tc.constraint_name = rc.constraint_name
)
LEFT JOIN information_schema.constraint_column_usage ccu ON (
rc.unique_constraint_catalog = ccu.constraint_catalog
AND rc.unique_constraint_schema = ccu.constraint_schema
AND rc.unique_constraint_name = ccu.constraint_name
)
WHERE
tc.table_schema = p_namespace
AND tc.table_name = p_table
AND tc.constraint_type = 'FOREIGN KEY'
AND tc.constraint_name = substring( p_constraintname from 1 for 63 ) -- INFO: les noms sont juste tronqués, pas de chiffre à la fin -> ça ne devrait pas poser de problème
AND kcu.column_name = p_foreignkeyname
AND ccu.table_name = p_foreigntable
AND ccu.column_name = 'id';

IF NOT FOUND THEN
RAISE NOTICE 'Upgrade table %.% - add constraint %', p_namespace, p_table, p_constraintname;
v_query := 'alter table ' || p_namespace || '.' || p_table || ' add constraint ';
v_query := v_query || p_constraintname || ' FOREIGN KEY (' || p_foreignkeyname || ') REFERENCES ' || p_foreigntable || '(id)';

IF p_deletecascade THEN
v_query := v_query || ' ON DELETE CASCADE ON UPDATE CASCADE;';
ELSE
v_query := v_query || ' ON DELETE SET NULL ON UPDATE CASCADE;';
END IF;

EXECUTE v_query;
RETURN 't';
ELSE
RETURN 'f';
END IF;
END;
$_$;


ALTER FUNCTION public.add_missing_constraint(text, text, text, text, text, boolean) OWNER TO webgfc;

--
-- Name: FUNCTION add_missing_constraint(text, text, text, text, text, boolean); Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON FUNCTION add_missing_constraint(text, text, text, text, text, boolean) IS 'Add a constraint to a table if it is missing';


--
-- Name: add_missing_table_field(text, text, text, text); Type: FUNCTION; Schema: public; Owner: webgfc
--

CREATE FUNCTION add_missing_table_field(text, text, text, text) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
  p_namespace alias for $1;
  p_table     alias for $2;
  p_field     alias for $3;
  p_type      alias for $4;
  v_row       record;
  v_query     text;
BEGIN
  select 1 into v_row from pg_namespace n, pg_class c, pg_attribute a
     where
         --public.slon_quote_brute(n.nspname) = p_namespace and
         n.nspname = p_namespace and
         c.relnamespace = n.oid and
         --public.slon_quote_brute(c.relname) = p_table and
         c.relname = p_table and
         a.attrelid = c.oid and
         --public.slon_quote_brute(a.attname) = p_field;
         a.attname = p_field;
  if not found then
    raise notice 'Upgrade table %.% - add field %', p_namespace, p_table, p_field;
    v_query := 'alter table ' || p_namespace || '.' || p_table || ' add column ';
    v_query := v_query || p_field || ' ' || p_type || ';';
    execute v_query;
    return 't';
  else
    return 'f';
  end if;
END;$_$;


ALTER FUNCTION public.add_missing_table_field(text, text, text, text) OWNER TO webgfc;

--
-- Name: FUNCTION add_missing_table_field(text, text, text, text); Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON FUNCTION add_missing_table_field(text, text, text, text) IS 'Drops a column from a table if it exists.';


--
-- Name: alter_table_drop_column_if_exists(text, text, text); Type: FUNCTION; Schema: public; Owner: webgfc
--

CREATE FUNCTION alter_table_drop_column_if_exists(text, text, text) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
	DECLARE
		p_namespace alias for $1;
		p_table     alias for $2;
		p_field     alias for $3;
		v_row       record;
		v_query     text;
	BEGIN
		SELECT 1 INTO v_row FROM pg_namespace n, pg_class c, pg_attribute a
			WHERE
				n.nspname = p_namespace
				AND c.relnamespace = n.oid
				AND c.relname = p_table
				AND a.attrelid = c.oid
				AND a.attname = p_field;
		IF FOUND THEN
			RAISE NOTICE 'Upgrade table %.% - drop field %', p_namespace, p_table, p_field;
			v_query := 'ALTER TABLE ' || p_namespace || '.' || p_table || ' DROP column ' || p_field || ';';
			EXECUTE v_query;
			RETURN 't';
		ELSE
			RETURN 'f';
		END IF;
	END;
$_$;


ALTER FUNCTION public.alter_table_drop_column_if_exists(text, text, text) OWNER TO webgfc;

--
-- Name: alter_table_drop_constraint_if_exists(text, text, text); Type: FUNCTION; Schema: public; Owner: webgfc
--

CREATE FUNCTION alter_table_drop_constraint_if_exists(text, text, text) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
	DECLARE
		p_namespace			alias for $1;
		p_table				alias for $2;
		p_constraintname    alias for $3;
		v_row				record;
		v_query				text;
	BEGIN
		SELECT
				1 INTO v_row
			FROM pg_constraint
				INNER JOIN pg_namespace ON ( pg_constraint.connamespace = pg_namespace.oid )
				INNER JOIN pg_class ON ( pg_constraint.conrelid = pg_class.oid )
			WHERE
				pg_namespace.nspname = p_namespace
				AND pg_class.relname = p_table
				AND pg_constraint.conname = p_constraintname;
		IF FOUND THEN
			RAISE NOTICE 'Alter table %.% - drop constraint %', p_namespace, p_table, p_constraintname;
			v_query := 'ALTER TABLE ' || p_namespace || '.' || p_table || ' DROP constraint ' || p_constraintname || ';';
			EXECUTE v_query;
			RETURN 't';
		ELSE
			RETURN 'f';
		END IF;
	END;
$_$;


ALTER FUNCTION public.alter_table_drop_constraint_if_exists(text, text, text) OWNER TO webgfc;

--
-- Name: FUNCTION alter_table_drop_constraint_if_exists(text, text, text); Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON FUNCTION alter_table_drop_constraint_if_exists(text, text, text) IS 'Équivalent de la fonctionnalité ALTER TABLE <table> DROP CONSTRAINT <name> disponible à partir de PostgreSQl 9.1';


--
-- Name: cakephp_validate_in_list(text, text[]); Type: FUNCTION; Schema: public; Owner: webgfc
--

CREATE FUNCTION cakephp_validate_in_list(text, text[]) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $_$
	SELECT $1 IS NULL OR ( ARRAY[CAST($1 AS TEXT)] <@ CAST($2 AS TEXT[]) );
$_$;


ALTER FUNCTION public.cakephp_validate_in_list(text, text[]) OWNER TO webgfc;

--
-- Name: FUNCTION cakephp_validate_in_list(text, text[]); Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON FUNCTION cakephp_validate_in_list(text, text[]) IS '@see http://api.cakephp.org/class/validation#method-ValidationinList';


--
-- Name: noaccents_upper(text); Type: FUNCTION; Schema: public; Owner: webgfc
--

CREATE FUNCTION noaccents_upper(text) RETURNS text
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $_$
    DECLARE
        st text;

    BEGIN
        -- On transforme les caractèes accentués et on passe en majuscule
        st:=translate($1,'aàäâeéèêëiïîoôöuùûücçñAÀÄÂEÉÈÊËIÏÎOÔÖUÙÛÜCÇÑ','AAAAEEEEEIIIOOOUUUUCCNAAAAEEEEEIIIOOOUUUUCCN');
        st:=upper(st);

        return st;
    END;
$_$;


ALTER FUNCTION public.noaccents_upper(text) OWNER TO webgfc;

--
-- Name: rebuilt_sequences(); Type: FUNCTION; Schema: public; Owner: webgfc
--

CREATE FUNCTION rebuilt_sequences() RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE sequencedefs RECORD; c integer ;
  BEGIN
    FOR sequencedefs IN Select
      constraint_column_usage.table_name as tablename,
      constraint_column_usage.table_name as tablename,
      constraint_column_usage.column_name as columnname,
      replace(replace(columns.column_default,'''::regclass)',''),'nextval(''','') as sequencename
      from information_schema.constraint_column_usage, information_schema.columns
      where constraint_column_usage.table_schema ='public' AND
      columns.table_schema = 'public' AND columns.table_name=constraint_column_usage.table_name
      AND constraint_column_usage.column_name = columns.column_name
      AND columns.column_default is not null
   LOOP
      EXECUTE 'select max('||sequencedefs.columnname||') from ' || sequencedefs.tablename INTO c;
      IF c is null THEN c = 0; END IF;
      IF c is not null THEN c = c+ 1; END IF;
      EXECUTE 'alter sequence ' || sequencedefs.sequencename ||' restart  with ' || c;
   END LOOP;

   RETURN 1; END;
$$;


ALTER FUNCTION public.rebuilt_sequences() OWNER TO webgfc;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acos; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE acos (
    id integer NOT NULL,
    parent_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    foreign_key integer,
    alias character varying(255) DEFAULT NULL::character varying,
    lft integer,
    rght integer
);


ALTER TABLE public.acos OWNER TO webgfc;

--
-- Name: acos_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE acos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.acos_id_seq OWNER TO webgfc;

--
-- Name: acos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE acos_id_seq OWNED BY acos.id;


--
-- Name: addressbooks; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE addressbooks (
    id integer NOT NULL,
    name character varying(255),
    description bytea,
    created timestamp without time zone DEFAULT now(),
    modified timestamp without time zone DEFAULT now(),
    foreign_key integer,
    active boolean DEFAULT true NOT NULL
);


ALTER TABLE public.addressbooks OWNER TO webgfc;

--
-- Name: addressbooks_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE addressbooks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.addressbooks_id_seq OWNER TO webgfc;

--
-- Name: addressbooks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE addressbooks_id_seq OWNED BY addressbooks.id;


--
-- Name: affaires; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE affaires (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    comment bytea,
    dossier_id integer,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.affaires OWNER TO webgfc;

--
-- Name: affaires_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE affaires_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.affaires_id_seq OWNER TO webgfc;

--
-- Name: affaires_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE affaires_id_seq OWNED BY affaires.id;


--
-- Name: armodels; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE armodels (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    gedpath text,
    content bytea,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    soustype_id integer NOT NULL,
    sms_content character varying(160),
    email_content bytea,
    email_title character varying(255),
    format character varying(50),
    mime character varying(255),
    ext character varying(10),
    size integer,
    preview bytea
);


ALTER TABLE public.armodels OWNER TO webgfc;

--
-- Name: armodels_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE armodels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.armodels_id_seq OWNER TO webgfc;

--
-- Name: armodels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE armodels_id_seq OWNED BY armodels.id;


--
-- Name: aros; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE aros (
    id integer NOT NULL,
    parent_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    foreign_key integer,
    alias character varying(255) DEFAULT NULL::character varying,
    lft integer,
    rght integer
);


ALTER TABLE public.aros OWNER TO webgfc;

--
-- Name: aros_acos; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE aros_acos (
    id integer NOT NULL,
    aro_id integer NOT NULL,
    aco_id integer NOT NULL,
    _create character varying(2) DEFAULT '0'::character varying(255) NOT NULL,
    _read character varying(2) DEFAULT '0'::character varying(255) NOT NULL,
    _update character varying(2) DEFAULT '0'::character varying(255) NOT NULL,
    _delete character varying(2) DEFAULT '0'::character varying(255) NOT NULL
);


ALTER TABLE public.aros_acos OWNER TO webgfc;

--
-- Name: aros_acos_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE aros_acos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aros_acos_id_seq OWNER TO webgfc;

--
-- Name: aros_acos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE aros_acos_id_seq OWNED BY aros_acos.id;


--
-- Name: aros_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE aros_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aros_id_seq OWNER TO webgfc;

--
-- Name: aros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE aros_id_seq OWNED BY aros.id;


--
-- Name: ars; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE ars (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    gedpath text,
    content bytea,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    courrier_id integer NOT NULL,
    format character varying(50),
    sms_dest character varying(20),
    sms_content character varying(160),
    email_title character varying(255),
    email_content bytea,
    ext character varying(10),
    mime character varying(255),
    used boolean DEFAULT false NOT NULL,
    size integer,
    email_dest character varying(255),
    preview bytea
);


ALTER TABLE public.ars OWNER TO webgfc;

--
-- Name: ars_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE ars_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ars_id_seq OWNER TO webgfc;

--
-- Name: ars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE ars_id_seq OWNED BY ars.id;


--
-- Name: bancontenus; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE bancontenus (
    id integer NOT NULL,
    bannette_id integer,
    courrier_id integer,
    created timestamp without time zone,
    modified timestamp without time zone,
    etat integer DEFAULT 1 NOT NULL,
    desktop_id integer,
    read boolean DEFAULT false NOT NULL
);


ALTER TABLE public.bancontenus OWNER TO webgfc;

--
-- Name: bancontenus_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE bancontenus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bancontenus_id_seq OWNER TO webgfc;

--
-- Name: bancontenus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE bancontenus_id_seq OWNED BY bancontenus.id;


--
-- Name: bannettes; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE bannettes (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.bannettes OWNER TO webgfc;

--
-- Name: bannettes_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE bannettes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bannettes_id_seq OWNER TO webgfc;

--
-- Name: bannettes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE bannettes_id_seq OWNED BY bannettes.id;


--
-- Name: bans; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE bans (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    active boolean DEFAULT true,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.bans OWNER TO webgfc;

--
-- Name: TABLE bans; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE bans IS 'Table permettant le stockage des Bases Adresse Nationale';


--
-- Name: bans_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE bans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bans_id_seq OWNER TO webgfc;

--
-- Name: bans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE bans_id_seq OWNED BY bans.id;


--
-- Name: bansadresses; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE bansadresses (
    id integer NOT NULL,
    bancommune_id integer NOT NULL,
    name character varying(100) NOT NULL,
    ident character varying(30) NOT NULL,
    nom_voie character varying(255),
    id_fantoir character varying(255),
    numero character varying(255),
    rep character varying(255),
    code_insee character varying(255),
    code_post character varying(255),
    alias character varying(255),
    nom_ld character varying(255),
    nom_afnor character varying(255),
    x character varying(255),
    y character varying(255),
    lon character varying(255),
    lat character varying(255),
    nom_commune character varying(255),
    active boolean DEFAULT true,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.bansadresses OWNER TO webgfc;

--
-- Name: TABLE bansadresses; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE bansadresses IS 'Table permettant le stockage des adresses par commune de la Base Adresse Nationale';


--
-- Name: bansadresses_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE bansadresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bansadresses_id_seq OWNER TO webgfc;

--
-- Name: bansadresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE bansadresses_id_seq OWNED BY bansadresses.id;


--
-- Name: banscommunes; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE banscommunes (
    id integer NOT NULL,
    ban_id integer NOT NULL,
    name character varying(100) NOT NULL,
    active boolean DEFAULT true,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.banscommunes OWNER TO webgfc;

--
-- Name: TABLE banscommunes; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE banscommunes IS 'Table permettant le stockage des Communes par Base Adresse Nationale';


--
-- Name: banscommunes_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE banscommunes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banscommunes_id_seq OWNER TO webgfc;

--
-- Name: banscommunes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE banscommunes_id_seq OWNED BY banscommunes.id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE comments (
    id integer NOT NULL,
    slug character varying(255),
    content bytea,
    private boolean DEFAULT false,
    owner_id integer NOT NULL,
    target_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.comments OWNER TO webgfc;

--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comments_id_seq OWNER TO webgfc;

--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE comments_id_seq OWNED BY comments.id;


--
-- Name: comments_readers; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE comments_readers (
    id integer NOT NULL,
    reader_id integer NOT NULL,
    comment_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.comments_readers OWNER TO webgfc;

--
-- Name: comments_readers_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE comments_readers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comments_readers_id_seq OWNER TO webgfc;

--
-- Name: comments_readers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE comments_readers_id_seq OWNED BY comments_readers.id;


--
-- Name: compteurs; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE compteurs (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    commentaire character varying(255) NOT NULL,
    def_compteur character varying(255) NOT NULL,
    sequence_id integer NOT NULL,
    def_reinit character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    val_reinit character varying(255)
);


ALTER TABLE public.compteurs OWNER TO webgfc;

--
-- Name: compteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE compteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compteurs_id_seq OWNER TO webgfc;

--
-- Name: compteurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE compteurs_id_seq OWNED BY compteurs.id;


--
-- Name: connecteurs; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE connecteurs (
    id integer NOT NULL,
    name character varying(250) DEFAULT NULL::character varying,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.connecteurs OWNER TO webgfc;

--
-- Name: TABLE connecteurs; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE connecteurs IS 'Table permettant la définition des différents connecteurs présents';


--
-- Name: connecteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE connecteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.connecteurs_id_seq OWNER TO webgfc;

--
-- Name: connecteurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE connecteurs_id_seq OWNED BY connecteurs.id;


--
-- Name: contactinfos; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE contactinfos (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    civilite character varying(255),
    nom character varying(255) NOT NULL,
    prenom character varying(255),
    email character varying(255),
    adresse character varying(255),
    compl character varying(255),
    cp character varying(5),
    ville character varying(255),
    region character varying(255),
    pays character varying(255),
    tel character varying(20),
    role character varying(255),
    organisation character varying(255),
    active boolean DEFAULT true NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    contact_id integer NOT NULL,
    slug character varying,
    numvoie character varying(20),
    typevoie character varying(4),
    nomvoie character varying(25),
    adressecomplete character varying(255)
);


ALTER TABLE public.contactinfos OWNER TO webgfc;

--
-- Name: contactinfos_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE contactinfos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contactinfos_id_seq OWNER TO webgfc;

--
-- Name: contactinfos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE contactinfos_id_seq OWNED BY contactinfos.id;


--
-- Name: contacts; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE contacts (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    civilite character varying(255),
    nom character varying(255) NOT NULL,
    prenom character varying(255),
    created timestamp without time zone DEFAULT now(),
    modified timestamp without time zone DEFAULT now(),
    addressbook_id integer NOT NULL,
    foreign_key integer,
    active boolean DEFAULT true NOT NULL,
    citoyen boolean DEFAULT true NOT NULL,
    slug character varying
);


ALTER TABLE public.contacts OWNER TO webgfc;

--
-- Name: contacts_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE contacts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contacts_id_seq OWNER TO webgfc;

--
-- Name: contacts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE contacts_id_seq OWNED BY contacts.id;


SET default_with_oids = true;

--
-- Name: courriers; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE courriers (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    fichier_associe character varying(255),
    objet text,
    ref_ancien smallint,
    soustype_id integer,
    lft integer,
    rght integer,
    parent_id integer,
    affaire_id integer,
    adressemail character varying(255),
    wav_associe character varying(255),
    video_associe character varying(255),
    numtelsms character varying(15),
    messagesms character varying(160),
    reference character varying(255),
    fichier_genere character varying(255),
    priorite integer DEFAULT 0 NOT NULL,
    user_creator_id integer,
    desktop_creator_id integer,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    date date DEFAULT now() NOT NULL,
    datereception date DEFAULT now() NOT NULL,
    direction integer,
    contactinfo_id integer,
    service_creator_id integer,
    delai_nb smallint,
    delai_unite smallint,
    affairesuiviepar_id integer,
    parapheur_etat integer,
    parapheur_id character varying(50),
    parapheur_commentaire text,
    parapheur_cible character varying(250),
    signee boolean,
    motifrefus text,
    dateenvoiparapheur timestamp without time zone,
    mail_retard_envoye boolean DEFAULT false,
    numero_depot integer,
    origineflux_id integer
);


ALTER TABLE public.courriers OWNER TO webgfc;

--
-- Name: courriers_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE courriers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courriers_id_seq OWNER TO webgfc;

--
-- Name: courriers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE courriers_id_seq OWNED BY courriers.id;


SET default_with_oids = false;

--
-- Name: courriers_metadonnees; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE courriers_metadonnees (
    id integer NOT NULL,
    valeur character varying(255),
    created timestamp without time zone,
    modified timestamp without time zone,
    courrier_id integer NOT NULL,
    metadonnee_id integer NOT NULL
);


ALTER TABLE public.courriers_metadonnees OWNER TO webgfc;

--
-- Name: courriers_metadonnees_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE courriers_metadonnees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courriers_metadonnees_id_seq OWNER TO webgfc;

--
-- Name: courriers_metadonnees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE courriers_metadonnees_id_seq OWNED BY courriers_metadonnees.id;


--
-- Name: courriers_reference_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE courriers_reference_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courriers_reference_seq OWNER TO webgfc;

--
-- Name: courriers_repertoires; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE courriers_repertoires (
    id integer NOT NULL,
    courrier_id integer,
    repertoire_id integer
);


ALTER TABLE public.courriers_repertoires OWNER TO webgfc;

--
-- Name: courriers_repertoires_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE courriers_repertoires_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courriers_repertoires_id_seq OWNER TO webgfc;

--
-- Name: courriers_repertoires_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE courriers_repertoires_id_seq OWNED BY courriers_repertoires.id;


--
-- Name: dacos; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE dacos (
    id integer NOT NULL,
    parent_id integer,
    model character varying(255),
    foreign_key integer,
    alias character varying(255),
    lft integer,
    rght integer
);


ALTER TABLE public.dacos OWNER TO webgfc;

--
-- Name: dacos_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE dacos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dacos_id_seq OWNER TO webgfc;

--
-- Name: dacos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE dacos_id_seq OWNED BY dacos.id;


--
-- Name: daros; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE daros (
    id integer NOT NULL,
    parent_id integer,
    model character varying(255),
    foreign_key integer,
    alias character varying(255),
    lft integer,
    rght integer
);


ALTER TABLE public.daros OWNER TO webgfc;

--
-- Name: daros_dacos; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE daros_dacos (
    id integer NOT NULL,
    daro_id integer NOT NULL,
    daco_id integer NOT NULL,
    _create character(2) DEFAULT 0 NOT NULL,
    _read character(2) DEFAULT 0 NOT NULL,
    _update character(2) DEFAULT 0 NOT NULL,
    _delete character(2) DEFAULT 0 NOT NULL
);


ALTER TABLE public.daros_dacos OWNER TO webgfc;

--
-- Name: daros_dacos_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE daros_dacos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.daros_dacos_id_seq OWNER TO webgfc;

--
-- Name: daros_dacos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE daros_dacos_id_seq OWNED BY daros_dacos.id;


--
-- Name: daros_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE daros_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.daros_id_seq OWNER TO webgfc;

--
-- Name: daros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE daros_id_seq OWNED BY daros.id;


--
-- Name: desktops; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE desktops (
    name character varying(255) NOT NULL,
    group_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    id integer NOT NULL,
    active boolean DEFAULT true NOT NULL
);


ALTER TABLE public.desktops OWNER TO webgfc;

--
-- Name: desktops_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE desktops_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.desktops_id_seq OWNER TO webgfc;

--
-- Name: desktops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE desktops_id_seq OWNED BY desktops.id;


--
-- Name: desktops_services; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE desktops_services (
    id integer NOT NULL,
    service_id integer NOT NULL,
    desktop_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.desktops_services OWNER TO webgfc;

--
-- Name: desktops_services_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE desktops_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.desktops_services_id_seq OWNER TO webgfc;

--
-- Name: desktops_services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE desktops_services_id_seq OWNED BY desktops_services.id;


--
-- Name: desktops_taches; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE desktops_taches (
    id integer NOT NULL,
    desktop_id integer NOT NULL,
    tache_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.desktops_taches OWNER TO webgfc;

--
-- Name: desktops_taches_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE desktops_taches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.desktops_taches_id_seq OWNER TO webgfc;

--
-- Name: desktops_taches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE desktops_taches_id_seq OWNED BY desktops_taches.id;


--
-- Name: desktops_users; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE desktops_users (
    id integer NOT NULL,
    user_id integer NOT NULL,
    desktop_id integer NOT NULL,
    delegation boolean DEFAULT false,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.desktops_users OWNER TO webgfc;

--
-- Name: desktops_users_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE desktops_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.desktops_users_id_seq OWNER TO webgfc;

--
-- Name: desktops_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE desktops_users_id_seq OWNED BY desktops_users.id;


--
-- Name: documents; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE documents (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    gedpath text,
    content bytea,
    size integer NOT NULL,
    mime character varying(255),
    preview bytea,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    courrier_id integer NOT NULL,
    ext character varying(10),
    main_doc boolean DEFAULT false NOT NULL,
    parapheur_bordereau bytea,
    courrier_pdf bytea,
    signee boolean,
    signature bytea,
    desktop_creator_id integer
);


ALTER TABLE public.documents OWNER TO webgfc;

--
-- Name: documents_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documents_id_seq OWNER TO webgfc;

--
-- Name: documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE documents_id_seq OWNED BY documents.id;


--
-- Name: documents_name_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE documents_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documents_name_seq OWNER TO webgfc;

--
-- Name: dossiers; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE dossiers (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    comment bytea,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.dossiers OWNER TO webgfc;

--
-- Name: dossiers_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE dossiers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dossiers_id_seq OWNER TO webgfc;

--
-- Name: dossiers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE dossiers_id_seq OWNED BY dossiers.id;


--
-- Name: formatsreponse; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE formatsreponse (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.formatsreponse OWNER TO webgfc;

--
-- Name: TABLE formatsreponse; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE formatsreponse IS 'Table pour les formats de réponse';


--
-- Name: formatsreponse_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE formatsreponse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.formatsreponse_id_seq OWNER TO webgfc;

--
-- Name: formatsreponse_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE formatsreponse_id_seq OWNED BY formatsreponse.id;


--
-- Name: formatsreponse_soustypes; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE formatsreponse_soustypes (
    id integer NOT NULL,
    formatreponse_id integer NOT NULL,
    soustype_id integer NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.formatsreponse_soustypes OWNER TO webgfc;

--
-- Name: TABLE formatsreponse_soustypes; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE formatsreponse_soustypes IS 'Table pour lier les sous-types aux différents formats de réponse';


--
-- Name: formatsreponse_soustypes_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE formatsreponse_soustypes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.formatsreponse_soustypes_id_seq OWNER TO webgfc;

--
-- Name: formatsreponse_soustypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE formatsreponse_soustypes_id_seq OWNED BY formatsreponse_soustypes.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE groups (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    readonly boolean DEFAULT false NOT NULL,
    active boolean DEFAULT true NOT NULL,
    group_id integer
);


ALTER TABLE public.groups OWNER TO webgfc;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO webgfc;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: inits_services; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE inits_services (
    id integer NOT NULL,
    desktop_id integer NOT NULL,
    service_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.inits_services OWNER TO webgfc;

--
-- Name: inits_services_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE inits_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inits_services_id_seq OWNER TO webgfc;

--
-- Name: inits_services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE inits_services_id_seq OWNED BY inits_services.id;


--
-- Name: keywordlists; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE keywordlists (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description bytea,
    version integer NOT NULL,
    active boolean DEFAULT true NOT NULL,
    keywordtype_id integer NOT NULL,
    scheme_id character varying(1024),
    scheme_name character varying(1024),
    scheme_agency_name character varying(1024),
    scheme_version_id character varying(1024),
    scheme_data_uri character varying(1024),
    scheme_uri character varying(1024),
    created_user_id integer,
    modified_user_id integer,
    created timestamp without time zone DEFAULT now(),
    modified timestamp without time zone DEFAULT now()
);


ALTER TABLE public.keywordlists OWNER TO webgfc;

--
-- Name: COLUMN keywordlists.scheme_id; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON COLUMN keywordlists.scheme_id IS 'Champ correpondant aux normes SEDA';


--
-- Name: COLUMN keywordlists.scheme_name; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON COLUMN keywordlists.scheme_name IS 'Champ correpondant aux normes SEDA';


--
-- Name: COLUMN keywordlists.scheme_agency_name; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON COLUMN keywordlists.scheme_agency_name IS 'Champ correpondant aux normes SEDA';


--
-- Name: COLUMN keywordlists.scheme_version_id; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON COLUMN keywordlists.scheme_version_id IS 'Champ correpondant aux normes SEDA';


--
-- Name: COLUMN keywordlists.scheme_data_uri; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON COLUMN keywordlists.scheme_data_uri IS 'Champ correpondant aux normes SEDA';


--
-- Name: COLUMN keywordlists.scheme_uri; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON COLUMN keywordlists.scheme_uri IS 'Champ correpondant aux normes SEDA';


--
-- Name: keywordlists_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE keywordlists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.keywordlists_id_seq OWNER TO webgfc;

--
-- Name: keywordlists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE keywordlists_id_seq OWNED BY keywordlists.id;


--
-- Name: keywords; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE keywords (
    id integer NOT NULL,
    version integer NOT NULL,
    code character varying(255) NOT NULL,
    libelle bytea,
    parent_id integer,
    lft integer,
    rght integer,
    keywordlist_id integer NOT NULL,
    created_user_id integer,
    modified_user_id integer,
    created timestamp without time zone DEFAULT now(),
    modified timestamp without time zone DEFAULT now()
);


ALTER TABLE public.keywords OWNER TO webgfc;

--
-- Name: keywords_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE keywords_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.keywords_id_seq OWNER TO webgfc;

--
-- Name: keywords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE keywords_id_seq OWNED BY keywords.id;


--
-- Name: keywordtypes; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE keywordtypes (
    id integer NOT NULL,
    code character varying(10) NOT NULL,
    documentation character varying(250) NOT NULL,
    created timestamp without time zone DEFAULT now(),
    modified timestamp without time zone DEFAULT now()
);


ALTER TABLE public.keywordtypes OWNER TO webgfc;

--
-- Name: keywordtypes_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE keywordtypes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.keywordtypes_id_seq OWNER TO webgfc;

--
-- Name: keywordtypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE keywordtypes_id_seq OWNED BY keywordtypes.id;


--
-- Name: metadonnees; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE metadonnees (
    id integer NOT NULL,
    name character varying(255),
    typemetadonnee_id integer NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.metadonnees OWNER TO webgfc;

--
-- Name: metadonnees_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE metadonnees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.metadonnees_id_seq OWNER TO webgfc;

--
-- Name: metadonnees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE metadonnees_id_seq OWNED BY metadonnees.id;


--
-- Name: metadonnees_soustypes; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE metadonnees_soustypes (
    id integer NOT NULL,
    metadonnee_id integer NOT NULL,
    soustype_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    obligatoire boolean
);


ALTER TABLE public.metadonnees_soustypes OWNER TO webgfc;

--
-- Name: TABLE metadonnees_soustypes; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE metadonnees_soustypes IS 'Table de liaison entre les métadonnées et les sous types';


--
-- Name: metadonnees_soustypes_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE metadonnees_soustypes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.metadonnees_soustypes_id_seq OWNER TO webgfc;

--
-- Name: metadonnees_soustypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE metadonnees_soustypes_id_seq OWNED BY metadonnees_soustypes.id;


--
-- Name: notifications; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE notifications (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description bytea,
    typenotif_id integer NOT NULL,
    desktop_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.notifications OWNER TO webgfc;

--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notifications_id_seq OWNER TO webgfc;

--
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE notifications_id_seq OWNED BY notifications.id;


--
-- Name: notifieddesktops; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE notifieddesktops (
    id integer NOT NULL,
    courrier_id integer,
    notification_id integer NOT NULL,
    desktop_id integer NOT NULL,
    read boolean DEFAULT false NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.notifieddesktops OWNER TO webgfc;

--
-- Name: notifieddesktops_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE notifieddesktops_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notifieddesktops_id_seq OWNER TO webgfc;

--
-- Name: notifieddesktops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE notifieddesktops_id_seq OWNED BY notifieddesktops.id;


--
-- Name: plandelegations; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE plandelegations (
    id integer NOT NULL,
    user_id integer NOT NULL,
    desktop_id integer NOT NULL,
    date_start timestamp without time zone NOT NULL,
    date_end timestamp without time zone NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.plandelegations OWNER TO webgfc;

--
-- Name: plandelegations_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE plandelegations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plandelegations_id_seq OWNER TO webgfc;

--
-- Name: plandelegations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE plandelegations_id_seq OWNED BY plandelegations.id;


--
-- Name: recheche_contactinfos; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recheche_contactinfos (
    id integer NOT NULL,
    recherche_id integer NOT NULL,
    contactinfo_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.recheche_contactinfos OWNER TO webgfc;

--
-- Name: recheche_contactinfos_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recheche_contactinfos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recheche_contactinfos_id_seq OWNER TO webgfc;

--
-- Name: recheche_contactinfos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recheche_contactinfos_id_seq OWNED BY recheche_contactinfos.id;


--
-- Name: recherches; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches (
    id integer NOT NULL,
    name character varying(255),
    cname character varying(255),
    cdate date,
    cdatereception date,
    user_id integer,
    created timestamp without time zone,
    modified timestamp without time zone,
    cdatefin date,
    cdatereceptionfin date,
    contactname character varying(255),
    cobjet character varying(255),
    creference character varying(255),
    ccomment character varying(255)
);


ALTER TABLE public.recherches OWNER TO webgfc;

--
-- Name: recherches_addressbooks; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_addressbooks (
    id integer NOT NULL,
    addressbook_id integer NOT NULL,
    recherche_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    modified timestamp without time zone DEFAULT now()
);


ALTER TABLE public.recherches_addressbooks OWNER TO webgfc;

--
-- Name: recherches_addressbooks_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_addressbooks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_addressbooks_id_seq OWNER TO webgfc;

--
-- Name: recherches_addressbooks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_addressbooks_id_seq OWNED BY recherches_addressbooks.id;


--
-- Name: recherches_affaires; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_affaires (
    id integer NOT NULL,
    affaire_id integer NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    recherche_id integer NOT NULL
);


ALTER TABLE public.recherches_affaires OWNER TO webgfc;

--
-- Name: recherches_affaires_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_affaires_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_affaires_id_seq OWNER TO webgfc;

--
-- Name: recherches_affaires_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_affaires_id_seq OWNED BY recherches_affaires.id;


--
-- Name: recherches_circuits; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_circuits (
    id integer NOT NULL,
    recherche_id integer,
    circuit_id integer,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.recherches_circuits OWNER TO webgfc;

--
-- Name: recherches_circuits_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_circuits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_circuits_id_seq OWNER TO webgfc;

--
-- Name: recherches_circuits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_circuits_id_seq OWNED BY recherches_circuits.id;


--
-- Name: recherches_contacts; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_contacts (
    id integer NOT NULL,
    contact_id integer NOT NULL,
    recherche_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    modified timestamp without time zone DEFAULT now()
);


ALTER TABLE public.recherches_contacts OWNER TO webgfc;

--
-- Name: recherches_contacts_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_contacts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_contacts_id_seq OWNER TO webgfc;

--
-- Name: recherches_contacts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_contacts_id_seq OWNED BY recherches_contacts.id;


--
-- Name: recherches_desktops; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_desktops (
    id integer NOT NULL,
    recherche_id integer NOT NULL,
    desktop_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.recherches_desktops OWNER TO webgfc;

--
-- Name: TABLE recherches_desktops; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE recherches_desktops IS 'Table de liaison entre les recherches et les desktops';


--
-- Name: recherches_desktops_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_desktops_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_desktops_id_seq OWNER TO webgfc;

--
-- Name: recherches_desktops_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_desktops_id_seq OWNED BY recherches_desktops.id;


--
-- Name: recherches_dossiers; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_dossiers (
    id integer NOT NULL,
    dossier_id integer NOT NULL,
    recherche_id integer NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.recherches_dossiers OWNER TO webgfc;

--
-- Name: recherches_dossiers_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_dossiers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_dossiers_id_seq OWNER TO webgfc;

--
-- Name: recherches_dossiers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_dossiers_id_seq OWNED BY recherches_dossiers.id;


--
-- Name: recherches_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_id_seq OWNER TO webgfc;

--
-- Name: recherches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_id_seq OWNED BY recherches.id;


--
-- Name: recherches_metadonnees; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_metadonnees (
    id integer NOT NULL,
    recherche_id integer,
    metadonnee_id integer,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.recherches_metadonnees OWNER TO webgfc;

--
-- Name: recherches_metadonnees_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_metadonnees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_metadonnees_id_seq OWNER TO webgfc;

--
-- Name: recherches_metadonnees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_metadonnees_id_seq OWNED BY recherches_metadonnees.id;


--
-- Name: recherches_selectvaluesmetadonnees; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_selectvaluesmetadonnees (
    id integer NOT NULL,
    recherche_id integer NOT NULL,
    selectvaluemetadonnee_id integer,
    metadonnee_id integer,
    valeur character varying(255),
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.recherches_selectvaluesmetadonnees OWNER TO webgfc;

--
-- Name: TABLE recherches_selectvaluesmetadonnees; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE recherches_selectvaluesmetadonnees IS 'Table de liaison entre les recherches et les selectvaluesmetadonnees';


--
-- Name: recherches_selectvaluesmetadonnees_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_selectvaluesmetadonnees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_selectvaluesmetadonnees_id_seq OWNER TO webgfc;

--
-- Name: recherches_selectvaluesmetadonnees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_selectvaluesmetadonnees_id_seq OWNED BY recherches_selectvaluesmetadonnees.id;


--
-- Name: recherches_soustypes; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_soustypes (
    id integer NOT NULL,
    soustype_id integer,
    recherche_id integer,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.recherches_soustypes OWNER TO webgfc;

--
-- Name: recherches_soustypes_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_soustypes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_soustypes_id_seq OWNER TO webgfc;

--
-- Name: recherches_soustypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_soustypes_id_seq OWNED BY recherches_soustypes.id;


--
-- Name: recherches_taches; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_taches (
    id integer NOT NULL,
    tache_id integer NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    recherche_id integer NOT NULL
);


ALTER TABLE public.recherches_taches OWNER TO webgfc;

--
-- Name: recherches_taches_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_taches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_taches_id_seq OWNER TO webgfc;

--
-- Name: recherches_taches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_taches_id_seq OWNED BY recherches_taches.id;


--
-- Name: recherches_types; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE recherches_types (
    id integer NOT NULL,
    type_id integer,
    recherche_id integer,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.recherches_types OWNER TO webgfc;

--
-- Name: recherches_types_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE recherches_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recherches_types_id_seq OWNER TO webgfc;

--
-- Name: recherches_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE recherches_types_id_seq OWNED BY recherches_types.id;


--
-- Name: referentielsfantoir; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE referentielsfantoir (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    active boolean DEFAULT true,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.referentielsfantoir OWNER TO webgfc;

--
-- Name: TABLE referentielsfantoir; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE referentielsfantoir IS 'Table permettant le stockage des référentiels FANTOIR';


--
-- Name: referentielsfantoir_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE referentielsfantoir_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.referentielsfantoir_id_seq OWNER TO webgfc;

--
-- Name: referentielsfantoir_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE referentielsfantoir_id_seq OWNED BY referentielsfantoir.id;


--
-- Name: referentielsfantoircom; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE referentielsfantoircom (
    id integer NOT NULL,
    referentielfantoir_id integer NOT NULL,
    referentielfantoirdir_id integer NOT NULL,
    name character varying(255) NOT NULL,
    codecom character varying(3) NOT NULL,
    clerivoli character varying(1) NOT NULL,
    libellecom character varying(30) NOT NULL,
    typecom character varying(1) NOT NULL,
    caracrur character varying(1) DEFAULT NULL::character varying,
    caracpop character varying(1) DEFAULT NULL::character varying,
    popreelle character varying(7) DEFAULT NULL::character varying,
    popapart character varying(7) DEFAULT NULL::character varying,
    popfictive character varying(7) DEFAULT NULL::character varying,
    caracannul character varying(1) DEFAULT NULL::character varying,
    dateannul character varying(7) DEFAULT NULL::character varying,
    datecreation character varying(7) DEFAULT NULL::character varying,
    active boolean DEFAULT true,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT referentielsfantoircom_typecom_in_list_chk CHECK (cakephp_validate_in_list((typecom)::text, ARRAY['N'::text, 'R'::text]))
);


ALTER TABLE public.referentielsfantoircom OWNER TO webgfc;

--
-- Name: TABLE referentielsfantoircom; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE referentielsfantoircom IS 'Table permettant le stockage des référentiels FANTOIR de commune';


--
-- Name: referentielsfantoircom_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE referentielsfantoircom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.referentielsfantoircom_id_seq OWNER TO webgfc;

--
-- Name: referentielsfantoircom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE referentielsfantoircom_id_seq OWNED BY referentielsfantoircom.id;


--
-- Name: referentielsfantoirdir; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE referentielsfantoirdir (
    id integer NOT NULL,
    referentielfantoir_id integer NOT NULL,
    name character varying(255) NOT NULL,
    codedpt character varying(2) NOT NULL,
    codedir character varying(1) NOT NULL,
    libelledir character varying(30) NOT NULL,
    active boolean DEFAULT true,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.referentielsfantoirdir OWNER TO webgfc;

--
-- Name: TABLE referentielsfantoirdir; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE referentielsfantoirdir IS 'Table permettant le stockage des référentiels FANTOIR de direction';


--
-- Name: referentielsfantoirdir_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE referentielsfantoirdir_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.referentielsfantoirdir_id_seq OWNER TO webgfc;

--
-- Name: referentielsfantoirdir_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE referentielsfantoirdir_id_seq OWNED BY referentielsfantoirdir.id;


--
-- Name: referentielsfantoirvoie; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE referentielsfantoirvoie (
    id integer NOT NULL,
    referentielfantoir_id integer NOT NULL,
    referentielfantoircom_id integer NOT NULL,
    name character varying(255) NOT NULL,
    identvoie character varying(4) NOT NULL,
    clerivoli character varying(1) NOT NULL,
    codevoie character varying(4) DEFAULT NULL::character varying,
    libellevoie character varying(26) NOT NULL,
    typecom character varying(1) NOT NULL,
    caracrur character varying(1) DEFAULT NULL::character varying,
    caracvoie character varying(1) NOT NULL,
    caracpop character varying(1) DEFAULT NULL::character varying,
    popapart character varying(7) DEFAULT NULL::character varying,
    popfictive character varying(7) DEFAULT NULL::character varying,
    caracannul character varying(1) DEFAULT NULL::character varying,
    dateannul character varying(7) DEFAULT NULL::character varying,
    datecreation character varying(7) DEFAULT NULL::character varying,
    codemajic character varying(5) NOT NULL,
    typevoie character varying(1) NOT NULL,
    caraclieudit character varying(1) DEFAULT NULL::character varying,
    libentiervoie character varying(8) NOT NULL,
    active boolean DEFAULT true,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT referentielsfantoirvoie_caracvoie_in_list_chk CHECK (cakephp_validate_in_list((caracvoie)::text, ARRAY['1'::text, '0'::text])),
    CONSTRAINT referentielsfantoirvoie_typecom_in_list_chk CHECK (cakephp_validate_in_list((typecom)::text, ARRAY['N'::text, 'R'::text])),
    CONSTRAINT referentielsfantoirvoie_typevoie_in_list_chk CHECK (cakephp_validate_in_list((typevoie)::text, ARRAY['1'::text, '2'::text, '3'::text, '4'::text, '5'::text]))
);


ALTER TABLE public.referentielsfantoirvoie OWNER TO webgfc;

--
-- Name: TABLE referentielsfantoirvoie; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE referentielsfantoirvoie IS 'Table permettant le stockage des référentiels FANTOIR de voie';


--
-- Name: referentielsfantoirvoie_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE referentielsfantoirvoie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.referentielsfantoirvoie_id_seq OWNER TO webgfc;

--
-- Name: referentielsfantoirvoie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE referentielsfantoirvoie_id_seq OWNED BY referentielsfantoirvoie.id;


--
-- Name: relements; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE relements (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    courrier_id integer NOT NULL
);


ALTER TABLE public.relements OWNER TO webgfc;

--
-- Name: relements_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE relements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relements_id_seq OWNER TO webgfc;

--
-- Name: relements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE relements_id_seq OWNED BY relements.id;


--
-- Name: repertoires; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE repertoires (
    id integer NOT NULL,
    name character varying(255),
    user_id integer,
    lft integer,
    rght integer,
    parent_id integer
);


ALTER TABLE public.repertoires OWNER TO webgfc;

--
-- Name: repertoires_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE repertoires_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.repertoires_id_seq OWNER TO webgfc;

--
-- Name: repertoires_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE repertoires_id_seq OWNED BY repertoires.id;


--
-- Name: rights; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE rights (
    id integer NOT NULL,
    get_infos boolean DEFAULT false NOT NULL,
    get_meta boolean DEFAULT false NOT NULL,
    get_taches boolean DEFAULT false NOT NULL,
    get_affaire boolean DEFAULT false NOT NULL,
    create_infos boolean DEFAULT false NOT NULL,
    set_infos boolean DEFAULT false NOT NULL,
    set_meta boolean DEFAULT false NOT NULL,
    set_taches boolean DEFAULT false NOT NULL,
    set_affaire boolean DEFAULT false NOT NULL,
    administration boolean DEFAULT false NOT NULL,
    aiguillage boolean DEFAULT false NOT NULL,
    creation boolean DEFAULT false NOT NULL,
    validation boolean DEFAULT false NOT NULL,
    edition boolean DEFAULT false NOT NULL,
    documentation boolean DEFAULT false NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    mode_classique boolean DEFAULT false NOT NULL,
    aro_id integer,
    envoi_cpy boolean DEFAULT false NOT NULL,
    envoi_wkf boolean DEFAULT false NOT NULL,
    document_upload boolean DEFAULT false NOT NULL,
    document_suppr boolean DEFAULT false NOT NULL,
    document_setasdefault boolean DEFAULT false NOT NULL,
    relements boolean DEFAULT false NOT NULL,
    relements_upload boolean DEFAULT false NOT NULL,
    relements_suppr boolean DEFAULT false NOT NULL,
    ar_gen boolean DEFAULT false NOT NULL,
    mes_services boolean DEFAULT false NOT NULL,
    recherches boolean DEFAULT false NOT NULL,
    suppression_flux boolean DEFAULT false NOT NULL,
    model character varying(255),
    foreign_key integer,
    envoi_ged boolean,
    consult_scan boolean
);


ALTER TABLE public.rights OWNER TO webgfc;

--
-- Name: rights_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE rights_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rights_id_seq OWNER TO webgfc;

--
-- Name: rights_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE rights_id_seq OWNED BY rights.id;


--
-- Name: rmodels; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE rmodels (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    gedpath text,
    content bytea,
    size integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    soustype_id integer NOT NULL,
    mime character varying(255),
    preview bytea,
    ext character varying(10)
);


ALTER TABLE public.rmodels OWNER TO webgfc;

--
-- Name: rmodels_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE rmodels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rmodels_id_seq OWNER TO webgfc;

--
-- Name: rmodels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE rmodels_id_seq OWNED BY rmodels.id;


--
-- Name: scanemails; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE scanemails (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    mail character varying(255),
    hostname character varying(255) NOT NULL,
    port character varying(24) DEFAULT '143'::character varying,
    password character varying(255) NOT NULL,
    desktop_id integer,
    type_id integer,
    soustype_id integer,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.scanemails OWNER TO webgfc;

--
-- Name: TABLE scanemails; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE scanemails IS 'Table permettant de stocker les emails et destinataires pour chaque collectivité';


--
-- Name: scanemails_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE scanemails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scanemails_id_seq OWNER TO webgfc;

--
-- Name: scanemails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE scanemails_id_seq OWNED BY scanemails.id;


--
-- Name: selectvaluesmetadonnees; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE selectvaluesmetadonnees (
    id integer NOT NULL,
    metadonnee_id integer NOT NULL,
    name character varying(250) DEFAULT NULL::character varying,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.selectvaluesmetadonnees OWNER TO webgfc;

--
-- Name: selectvaluesmetadonnees_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE selectvaluesmetadonnees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.selectvaluesmetadonnees_id_seq OWNER TO webgfc;

--
-- Name: selectvaluesmetadonnees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE selectvaluesmetadonnees_id_seq OWNED BY selectvaluesmetadonnees.id;


--
-- Name: sequences; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE sequences (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    commentaire character varying(255) NOT NULL,
    num_sequence integer NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.sequences OWNER TO webgfc;

--
-- Name: sequences_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE sequences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sequences_id_seq OWNER TO webgfc;

--
-- Name: sequences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE sequences_id_seq OWNED BY sequences.id;


--
-- Name: services; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE services (
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    parent_id smallint,
    lft smallint,
    rght smallint,
    id integer NOT NULL
);


ALTER TABLE public.services OWNER TO webgfc;

--
-- Name: services_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.services_id_seq OWNER TO webgfc;

--
-- Name: services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE services_id_seq OWNED BY services.id;


--
-- Name: soustypes; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE soustypes (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    type_id integer NOT NULL,
    circuit_id integer,
    entrant smallint DEFAULT 1 NOT NULL,
    delai_nb smallint,
    delai_unite smallint,
    lft integer,
    rght integer,
    parent_id integer,
    valeuremail bytea,
    compteur_id integer
);


ALTER TABLE public.soustypes OWNER TO webgfc;

--
-- Name: soustypes_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE soustypes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.soustypes_id_seq OWNER TO webgfc;

--
-- Name: soustypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE soustypes_id_seq OWNED BY soustypes.id;


SET default_with_oids = true;

--
-- Name: taches; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE taches (
    id integer NOT NULL,
    courrier_id integer NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    name character varying(255) NOT NULL,
    delai_nb integer,
    delai_unite integer,
    statut integer DEFAULT 0 NOT NULL,
    act_desktop_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.taches OWNER TO webgfc;

--
-- Name: taches_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE taches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taches_id_seq OWNER TO webgfc;

--
-- Name: taches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE taches_id_seq OWNED BY taches.id;


SET default_with_oids = false;

--
-- Name: typemetadonnees; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE typemetadonnees (
    id integer NOT NULL,
    name character varying(255),
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.typemetadonnees OWNER TO webgfc;

--
-- Name: typemetadonnees_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE typemetadonnees_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.typemetadonnees_id_seq OWNER TO webgfc;

--
-- Name: typemetadonnees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE typemetadonnees_id_seq OWNED BY typemetadonnees.id;


--
-- Name: typenotifs; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE typenotifs (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.typenotifs OWNER TO webgfc;

--
-- Name: typenotifs_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE typenotifs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.typenotifs_id_seq OWNER TO webgfc;

--
-- Name: typenotifs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE typenotifs_id_seq OWNED BY typenotifs.id;


--
-- Name: typenotifs_users; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE typenotifs_users (
    id integer NOT NULL,
    typenotif_id integer NOT NULL,
    user_id integer NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.typenotifs_users OWNER TO webgfc;

--
-- Name: typenotifs_users_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE typenotifs_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.typenotifs_users_id_seq OWNER TO webgfc;

--
-- Name: typenotifs_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE typenotifs_users_id_seq OWNED BY typenotifs_users.id;


SET default_with_oids = true;

--
-- Name: types; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE types (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.types OWNER TO webgfc;

--
-- Name: types_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.types_id_seq OWNER TO webgfc;

--
-- Name: types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE types_id_seq OWNED BY types.id;


SET default_with_oids = false;

--
-- Name: originesflux; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE originesflux (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    active boolean DEFAULT true,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.originesflux OWNER TO webgfc;

--
-- Name: TABLE originesflux; Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON TABLE originesflux IS 'Table permettant le stockage des origines de flux';


--
-- Name: origineflux_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE origineflux_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.origineflux_id_seq OWNER TO webgfc;

--
-- Name: origineflux_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE origineflux_id_seq OWNED BY originesflux.id;


SET default_with_oids = true;

--
-- Name: users; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    prenom character varying(255) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    mail character varying(255),
    desktop_id integer,
    last_login timestamp without time zone,
    last_logout timestamp without time zone,
    active boolean DEFAULT true NOT NULL,
    pwdmodified boolean DEFAULT false NOT NULL,
    numtel character varying(15),
    accept_notif boolean,
    mail_insertion boolean,
    mail_traitement boolean,
    mail_refus boolean,
    mail_retard_validation boolean,
    mail_copy boolean,
    mail_insertion_reponse boolean,
    mail_aiguillage boolean
);


ALTER TABLE public.users OWNER TO webgfc;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO webgfc;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


SET default_with_oids = false;

--
-- Name: wkf_circuits; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE wkf_circuits (
    id integer NOT NULL,
    nom character varying(250) NOT NULL,
    description bytea,
    actif integer DEFAULT 1 NOT NULL,
    defaut integer DEFAULT 0 NOT NULL,
    created_user_id integer NOT NULL,
    modified_user_id integer,
    created timestamp without time zone,
    modified timestamp without time zone
);


ALTER TABLE public.wkf_circuits OWNER TO webgfc;

--
-- Name: wkf_circuits_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE wkf_circuits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wkf_circuits_id_seq OWNER TO webgfc;

--
-- Name: wkf_circuits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE wkf_circuits_id_seq OWNED BY wkf_circuits.id;


--
-- Name: wkf_compositions; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE wkf_compositions (
    id integer NOT NULL,
    etape_id integer NOT NULL,
    type_validation character varying(1) NOT NULL,
    created_user_id integer,
    modified_user_id integer,
    created timestamp without time zone,
    modified timestamp without time zone,
    trigger_id integer,
    soustype integer,
    type_composition character varying(20) DEFAULT 'USER'::character varying
);


ALTER TABLE public.wkf_compositions OWNER TO webgfc;

--
-- Name: wkf_compositions_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE wkf_compositions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wkf_compositions_id_seq OWNER TO webgfc;

--
-- Name: wkf_compositions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE wkf_compositions_id_seq OWNED BY wkf_compositions.id;


--
-- Name: wkf_etapes; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE wkf_etapes (
    id integer NOT NULL,
    circuit_id integer NOT NULL,
    nom character varying(250) NOT NULL,
    description bytea,
    type integer NOT NULL,
    ordre integer NOT NULL,
    created_user_id integer NOT NULL,
    modified_user_id integer,
    created timestamp without time zone,
    modified timestamp without time zone,
    soustype integer,
    cpt_retard integer
);


ALTER TABLE public.wkf_etapes OWNER TO webgfc;

--
-- Name: wkf_etapes_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE wkf_etapes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wkf_etapes_id_seq OWNER TO webgfc;

--
-- Name: wkf_etapes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE wkf_etapes_id_seq OWNED BY wkf_etapes.id;


--
-- Name: wkf_signatures; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE wkf_signatures (
    id integer NOT NULL,
    type_signature character varying(100) NOT NULL,
    signature bytea NOT NULL,
    visa_id integer
);


ALTER TABLE public.wkf_signatures OWNER TO webgfc;

--
-- Name: wkf_signatures_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE wkf_signatures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wkf_signatures_id_seq OWNER TO webgfc;

--
-- Name: wkf_signatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE wkf_signatures_id_seq OWNED BY wkf_signatures.id;


--
-- Name: wkf_traitements; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE wkf_traitements (
    id integer NOT NULL,
    circuit_id integer NOT NULL,
    target_id integer NOT NULL,
    numero_traitement integer DEFAULT 1 NOT NULL,
    created_user_id integer,
    modified_user_id integer,
    created timestamp without time zone,
    modified timestamp without time zone,
    treated boolean
);


ALTER TABLE public.wkf_traitements OWNER TO webgfc;

--
-- Name: wkf_traitements_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE wkf_traitements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wkf_traitements_id_seq OWNER TO webgfc;

--
-- Name: wkf_traitements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE wkf_traitements_id_seq OWNED BY wkf_traitements.id;


--
-- Name: wkf_visas; Type: TABLE; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE TABLE wkf_visas (
    id integer NOT NULL,
    traitement_id integer NOT NULL,
    trigger_id integer NOT NULL,
    signature_id integer,
    etape_nom character varying(250),
    etape_type integer NOT NULL,
    action character varying(2) NOT NULL,
    commentaire text,
    date timestamp without time zone,
    type_validation character varying(1) NOT NULL,
    numero_traitement integer NOT NULL,
    etape_id integer,
    date_retard timestamp without time zone
);


ALTER TABLE public.wkf_visas OWNER TO webgfc;

--
-- Name: wkf_visas_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE wkf_visas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wkf_visas_id_seq OWNER TO webgfc;

--
-- Name: wkf_visas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE wkf_visas_id_seq OWNED BY wkf_visas.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY acos ALTER COLUMN id SET DEFAULT nextval('acos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY addressbooks ALTER COLUMN id SET DEFAULT nextval('addressbooks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY affaires ALTER COLUMN id SET DEFAULT nextval('affaires_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY armodels ALTER COLUMN id SET DEFAULT nextval('armodels_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY aros ALTER COLUMN id SET DEFAULT nextval('aros_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY aros_acos ALTER COLUMN id SET DEFAULT nextval('aros_acos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY ars ALTER COLUMN id SET DEFAULT nextval('ars_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY bancontenus ALTER COLUMN id SET DEFAULT nextval('bancontenus_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY bannettes ALTER COLUMN id SET DEFAULT nextval('bannettes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY bans ALTER COLUMN id SET DEFAULT nextval('bans_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY bansadresses ALTER COLUMN id SET DEFAULT nextval('bansadresses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY banscommunes ALTER COLUMN id SET DEFAULT nextval('banscommunes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY comments ALTER COLUMN id SET DEFAULT nextval('comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY comments_readers ALTER COLUMN id SET DEFAULT nextval('comments_readers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY compteurs ALTER COLUMN id SET DEFAULT nextval('compteurs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY connecteurs ALTER COLUMN id SET DEFAULT nextval('connecteurs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY contactinfos ALTER COLUMN id SET DEFAULT nextval('contactinfos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY contacts ALTER COLUMN id SET DEFAULT nextval('contacts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers ALTER COLUMN id SET DEFAULT nextval('courriers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers_metadonnees ALTER COLUMN id SET DEFAULT nextval('courriers_metadonnees_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers_repertoires ALTER COLUMN id SET DEFAULT nextval('courriers_repertoires_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY dacos ALTER COLUMN id SET DEFAULT nextval('dacos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY daros ALTER COLUMN id SET DEFAULT nextval('daros_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY daros_dacos ALTER COLUMN id SET DEFAULT nextval('daros_dacos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops ALTER COLUMN id SET DEFAULT nextval('desktops_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops_services ALTER COLUMN id SET DEFAULT nextval('desktops_services_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops_taches ALTER COLUMN id SET DEFAULT nextval('desktops_taches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops_users ALTER COLUMN id SET DEFAULT nextval('desktops_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY documents ALTER COLUMN id SET DEFAULT nextval('documents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY dossiers ALTER COLUMN id SET DEFAULT nextval('dossiers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY formatsreponse ALTER COLUMN id SET DEFAULT nextval('formatsreponse_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY formatsreponse_soustypes ALTER COLUMN id SET DEFAULT nextval('formatsreponse_soustypes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY inits_services ALTER COLUMN id SET DEFAULT nextval('inits_services_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY keywordlists ALTER COLUMN id SET DEFAULT nextval('keywordlists_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY keywords ALTER COLUMN id SET DEFAULT nextval('keywords_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY keywordtypes ALTER COLUMN id SET DEFAULT nextval('keywordtypes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY metadonnees ALTER COLUMN id SET DEFAULT nextval('metadonnees_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY metadonnees_soustypes ALTER COLUMN id SET DEFAULT nextval('metadonnees_soustypes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY notifications ALTER COLUMN id SET DEFAULT nextval('notifications_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY notifieddesktops ALTER COLUMN id SET DEFAULT nextval('notifieddesktops_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY plandelegations ALTER COLUMN id SET DEFAULT nextval('plandelegations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recheche_contactinfos ALTER COLUMN id SET DEFAULT nextval('recheche_contactinfos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches ALTER COLUMN id SET DEFAULT nextval('recherches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_addressbooks ALTER COLUMN id SET DEFAULT nextval('recherches_addressbooks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_affaires ALTER COLUMN id SET DEFAULT nextval('recherches_affaires_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_circuits ALTER COLUMN id SET DEFAULT nextval('recherches_circuits_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_contacts ALTER COLUMN id SET DEFAULT nextval('recherches_contacts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_desktops ALTER COLUMN id SET DEFAULT nextval('recherches_desktops_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_dossiers ALTER COLUMN id SET DEFAULT nextval('recherches_dossiers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_metadonnees ALTER COLUMN id SET DEFAULT nextval('recherches_metadonnees_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_selectvaluesmetadonnees ALTER COLUMN id SET DEFAULT nextval('recherches_selectvaluesmetadonnees_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_soustypes ALTER COLUMN id SET DEFAULT nextval('recherches_soustypes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_taches ALTER COLUMN id SET DEFAULT nextval('recherches_taches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_types ALTER COLUMN id SET DEFAULT nextval('recherches_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY referentielsfantoir ALTER COLUMN id SET DEFAULT nextval('referentielsfantoir_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY referentielsfantoircom ALTER COLUMN id SET DEFAULT nextval('referentielsfantoircom_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY referentielsfantoirdir ALTER COLUMN id SET DEFAULT nextval('referentielsfantoirdir_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY referentielsfantoirvoie ALTER COLUMN id SET DEFAULT nextval('referentielsfantoirvoie_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY relements ALTER COLUMN id SET DEFAULT nextval('relements_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY repertoires ALTER COLUMN id SET DEFAULT nextval('repertoires_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY rights ALTER COLUMN id SET DEFAULT nextval('rights_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY rmodels ALTER COLUMN id SET DEFAULT nextval('rmodels_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY scanemails ALTER COLUMN id SET DEFAULT nextval('scanemails_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY selectvaluesmetadonnees ALTER COLUMN id SET DEFAULT nextval('selectvaluesmetadonnees_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY sequences ALTER COLUMN id SET DEFAULT nextval('sequences_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY services ALTER COLUMN id SET DEFAULT nextval('services_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY soustypes ALTER COLUMN id SET DEFAULT nextval('soustypes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY taches ALTER COLUMN id SET DEFAULT nextval('taches_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY typemetadonnees ALTER COLUMN id SET DEFAULT nextval('typemetadonnees_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY typenotifs ALTER COLUMN id SET DEFAULT nextval('typenotifs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY typenotifs_users ALTER COLUMN id SET DEFAULT nextval('typenotifs_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY types ALTER COLUMN id SET DEFAULT nextval('types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY originesflux ALTER COLUMN id SET DEFAULT nextval('origineflux_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_circuits ALTER COLUMN id SET DEFAULT nextval('wkf_circuits_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_compositions ALTER COLUMN id SET DEFAULT nextval('wkf_compositions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_etapes ALTER COLUMN id SET DEFAULT nextval('wkf_etapes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_signatures ALTER COLUMN id SET DEFAULT nextval('wkf_signatures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_traitements ALTER COLUMN id SET DEFAULT nextval('wkf_traitements_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_visas ALTER COLUMN id SET DEFAULT nextval('wkf_visas_id_seq'::regclass);


--
-- Name: acos_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY acos
    ADD CONSTRAINT acos_pkey PRIMARY KEY (id);


--
-- Name: addressbooks_name_key; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY addressbooks
    ADD CONSTRAINT addressbooks_name_key UNIQUE (name);


--
-- Name: addressbooks_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY addressbooks
    ADD CONSTRAINT addressbooks_pkey PRIMARY KEY (id);


--
-- Name: affaires_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY affaires
    ADD CONSTRAINT affaires_pkey PRIMARY KEY (id);


--
-- Name: armodels_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY armodels
    ADD CONSTRAINT armodels_pkey PRIMARY KEY (id);


--
-- Name: aros_acos_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY aros_acos
    ADD CONSTRAINT aros_acos_pkey PRIMARY KEY (id);


--
-- Name: aros_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY aros
    ADD CONSTRAINT aros_pkey PRIMARY KEY (id);


--
-- Name: ars_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY ars
    ADD CONSTRAINT ars_pkey PRIMARY KEY (id);


--
-- Name: bancontenus_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY bancontenus
    ADD CONSTRAINT bancontenus_pkey PRIMARY KEY (id);


--
-- Name: bannettes_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY bannettes
    ADD CONSTRAINT bannettes_pkey PRIMARY KEY (id);


--
-- Name: bans_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY bans
    ADD CONSTRAINT bans_pkey PRIMARY KEY (id);


--
-- Name: bansadresses_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY bansadresses
    ADD CONSTRAINT bansadresses_pkey PRIMARY KEY (id);


--
-- Name: banscommunes_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY banscommunes
    ADD CONSTRAINT banscommunes_pkey PRIMARY KEY (id);


--
-- Name: comments_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: comments_readers_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY comments_readers
    ADD CONSTRAINT comments_readers_pkey PRIMARY KEY (id);


--
-- Name: compteurs_nom_key; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY compteurs
    ADD CONSTRAINT compteurs_nom_key UNIQUE (nom);


--
-- Name: compteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY compteurs
    ADD CONSTRAINT compteurs_pkey PRIMARY KEY (id);


--
-- Name: connecteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY connecteurs
    ADD CONSTRAINT connecteurs_pkey PRIMARY KEY (id);


--
-- Name: contactinfos_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY contactinfos
    ADD CONSTRAINT contactinfos_pkey PRIMARY KEY (id);


--
-- Name: contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY contacts
    ADD CONSTRAINT contacts_pkey PRIMARY KEY (id);


--
-- Name: courriers_metadonnees_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY courriers_metadonnees
    ADD CONSTRAINT courriers_metadonnees_pkey PRIMARY KEY (id);


--
-- Name: courriers_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY courriers
    ADD CONSTRAINT courriers_pkey PRIMARY KEY (id);


--
-- Name: courriers_repertoires_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY courriers_repertoires
    ADD CONSTRAINT courriers_repertoires_pkey PRIMARY KEY (id);


--
-- Name: dacos_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY dacos
    ADD CONSTRAINT dacos_pkey PRIMARY KEY (id);


--
-- Name: daros_dacos_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY daros_dacos
    ADD CONSTRAINT daros_dacos_pkey PRIMARY KEY (id);


--
-- Name: daros_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY daros
    ADD CONSTRAINT daros_pkey PRIMARY KEY (id);


--
-- Name: desktops_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY desktops
    ADD CONSTRAINT desktops_pkey PRIMARY KEY (id);


--
-- Name: desktops_services_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY desktops_services
    ADD CONSTRAINT desktops_services_pkey PRIMARY KEY (id);


--
-- Name: desktops_taches_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY desktops_taches
    ADD CONSTRAINT desktops_taches_pkey PRIMARY KEY (id);


--
-- Name: desktops_users_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY desktops_users
    ADD CONSTRAINT desktops_users_pkey PRIMARY KEY (id);


--
-- Name: documents_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: dossiers_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY dossiers
    ADD CONSTRAINT dossiers_pkey PRIMARY KEY (id);


--
-- Name: formatsreponse_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY formatsreponse
    ADD CONSTRAINT formatsreponse_pkey PRIMARY KEY (id);


--
-- Name: formatsreponse_soustypes_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY formatsreponse_soustypes
    ADD CONSTRAINT formatsreponse_soustypes_pkey PRIMARY KEY (id);


--
-- Name: groups_name_unique; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_name_unique UNIQUE (name);


--
-- Name: groups_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: inits_services_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY inits_services
    ADD CONSTRAINT inits_services_pkey PRIMARY KEY (id);


--
-- Name: keywordlists_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY keywordlists
    ADD CONSTRAINT keywordlists_pkey PRIMARY KEY (id);


--
-- Name: keywords_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY keywords
    ADD CONSTRAINT keywords_pkey PRIMARY KEY (id);


--
-- Name: keywordtypes_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY keywordtypes
    ADD CONSTRAINT keywordtypes_pkey PRIMARY KEY (id);


--
-- Name: mail_unique; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT mail_unique UNIQUE (mail);


--
-- Name: metadonnees_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY metadonnees
    ADD CONSTRAINT metadonnees_pkey PRIMARY KEY (id);


--
-- Name: metadonnees_soustypes_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY metadonnees_soustypes
    ADD CONSTRAINT metadonnees_soustypes_pkey PRIMARY KEY (id);


--
-- Name: notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: notifieddesktops_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY notifieddesktops
    ADD CONSTRAINT notifieddesktops_pkey PRIMARY KEY (id);


--
-- Name: plandelegations_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY plandelegations
    ADD CONSTRAINT plandelegations_pkey PRIMARY KEY (id);


--
-- Name: recheche_contactinfos_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY recheche_contactinfos
    ADD CONSTRAINT recheche_contactinfos_pkey PRIMARY KEY (id);


--
-- Name: recherches_addressbooks_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY recherches_addressbooks
    ADD CONSTRAINT recherches_addressbooks_pkey PRIMARY KEY (id);


--
-- Name: recherches_circuits_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY recherches_circuits
    ADD CONSTRAINT recherches_circuits_pkey PRIMARY KEY (id);


--
-- Name: recherches_contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY recherches_contacts
    ADD CONSTRAINT recherches_contacts_pkey PRIMARY KEY (id);


--
-- Name: recherches_desktops_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY recherches_desktops
    ADD CONSTRAINT recherches_desktops_pkey PRIMARY KEY (id);


--
-- Name: recherches_metadonnees_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY recherches_metadonnees
    ADD CONSTRAINT recherches_metadonnees_pkey PRIMARY KEY (id);


--
-- Name: recherches_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY recherches
    ADD CONSTRAINT recherches_pkey PRIMARY KEY (id);


--
-- Name: recherches_selectvaluesmetadonnees_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY recherches_selectvaluesmetadonnees
    ADD CONSTRAINT recherches_selectvaluesmetadonnees_pkey PRIMARY KEY (id);


--
-- Name: referentielsfantoir_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY referentielsfantoir
    ADD CONSTRAINT referentielsfantoir_pkey PRIMARY KEY (id);


--
-- Name: referentielsfantoircom_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY referentielsfantoircom
    ADD CONSTRAINT referentielsfantoircom_pkey PRIMARY KEY (id);


--
-- Name: referentielsfantoirdir_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY referentielsfantoirdir
    ADD CONSTRAINT referentielsfantoirdir_pkey PRIMARY KEY (id);


--
-- Name: referentielsfantoirvoie_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY referentielsfantoirvoie
    ADD CONSTRAINT referentielsfantoirvoie_pkey PRIMARY KEY (id);


--
-- Name: relements_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY relements
    ADD CONSTRAINT relements_pkey PRIMARY KEY (id);


--
-- Name: repertoires_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY repertoires
    ADD CONSTRAINT repertoires_pkey PRIMARY KEY (id);


--
-- Name: rights_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY rights
    ADD CONSTRAINT rights_pkey PRIMARY KEY (id);


--
-- Name: rmodels_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY rmodels
    ADD CONSTRAINT rmodels_pkey PRIMARY KEY (id);


--
-- Name: scanemails_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY scanemails
    ADD CONSTRAINT scanemails_pkey PRIMARY KEY (id);


--
-- Name: selectvaluesmetadonnees_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY selectvaluesmetadonnees
    ADD CONSTRAINT selectvaluesmetadonnees_pkey PRIMARY KEY (id);


--
-- Name: sequences_nom_key; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY sequences
    ADD CONSTRAINT sequences_nom_key UNIQUE (nom);


--
-- Name: sequences_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY sequences
    ADD CONSTRAINT sequences_pkey PRIMARY KEY (id);


--
-- Name: services_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: soustypes_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY soustypes
    ADD CONSTRAINT soustypes_pkey PRIMARY KEY (id);


--
-- Name: taches_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY taches
    ADD CONSTRAINT taches_pkey PRIMARY KEY (id);


--
-- Name: typemetadonnees_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY typemetadonnees
    ADD CONSTRAINT typemetadonnees_pkey PRIMARY KEY (id);


--
-- Name: typenotifs_name_key; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY typenotifs
    ADD CONSTRAINT typenotifs_name_key UNIQUE (name);


--
-- Name: typenotifs_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY typenotifs
    ADD CONSTRAINT typenotifs_pkey PRIMARY KEY (id);


--
-- Name: typenotifs_users_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY typenotifs_users
    ADD CONSTRAINT typenotifs_users_pkey PRIMARY KEY (id);


--
-- Name: types_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_pkey PRIMARY KEY (id);


--
-- Name: originesflux_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY originesflux
    ADD CONSTRAINT originesflux_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: wkf_circuits_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY wkf_circuits
    ADD CONSTRAINT wkf_circuits_pkey PRIMARY KEY (id);


--
-- Name: wkf_compositions_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY wkf_compositions
    ADD CONSTRAINT wkf_compositions_pkey PRIMARY KEY (id);


--
-- Name: wkf_etapes_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY wkf_etapes
    ADD CONSTRAINT wkf_etapes_pkey PRIMARY KEY (id);


--
-- Name: wkf_signatures_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY wkf_signatures
    ADD CONSTRAINT wkf_signatures_pkey PRIMARY KEY (id);


--
-- Name: wkf_traitements_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY wkf_traitements
    ADD CONSTRAINT wkf_traitements_pkey PRIMARY KEY (id);


--
-- Name: wkf_visas_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace: 
--

ALTER TABLE ONLY wkf_visas
    ADD CONSTRAINT wkf_visas_pkey PRIMARY KEY (id);


--
-- Name: acos_alias_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX acos_alias_idx ON acos USING btree (alias);


--
-- Name: acos_lft_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX acos_lft_idx ON acos USING btree (lft);


--
-- Name: acos_parent_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX acos_parent_id_idx ON acos USING btree (parent_id);


--
-- Name: acos_rght_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX acos_rght_idx ON acos USING btree (rght);


--
-- Name: affaires_dossier_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX affaires_dossier_id_idx ON affaires USING btree (dossier_id);


--
-- Name: aro_aco_key; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX aro_aco_key ON aros_acos USING btree (aro_id, aco_id);


--
-- Name: aros_acos_aco_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX aros_acos_aco_id_idx ON aros_acos USING btree (aco_id);


--
-- Name: aros_acos_aro_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX aros_acos_aro_id_idx ON aros_acos USING btree (aro_id);


--
-- Name: aros_alias_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX aros_alias_idx ON aros USING btree (alias);


--
-- Name: aros_aros_aco_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX aros_aros_aco_id_idx ON aros_acos USING btree (aco_id);


--
-- Name: aros_aros_aro_id_aco_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX aros_aros_aro_id_aco_id_idx ON aros_acos USING btree (aro_id, aco_id);


--
-- Name: aros_aros_aro_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX aros_aros_aro_id_idx ON aros_acos USING btree (aro_id);


--
-- Name: aros_foreign_key_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX aros_foreign_key_idx ON aros USING btree (foreign_key);


--
-- Name: aros_lft_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX aros_lft_idx ON aros USING btree (lft);


--
-- Name: aros_model_alias_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX aros_model_alias_idx ON aros USING btree (model, alias);


--
-- Name: aros_model_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX aros_model_idx ON aros USING btree (model);


--
-- Name: aros_parent_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX aros_parent_id_idx ON aros USING btree (parent_id);


--
-- Name: aros_rght_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX aros_rght_idx ON aros USING btree (rght);


--
-- Name: bancontenus_bannette_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX bancontenus_bannette_id_idx ON bancontenus USING btree (bannette_id);


--
-- Name: bancontenus_courrier_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX bancontenus_courrier_id_idx ON bancontenus USING btree (courrier_id);


--
-- Name: bancontenus_etat_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX bancontenus_etat_id_idx ON bancontenus USING btree (etat);


--
-- Name: bans_name_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX bans_name_idx ON bans USING btree (name);


--
-- Name: banscommunes_name_ban_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX banscommunes_name_ban_id_idx ON banscommunes USING btree (name, ban_id);


--
-- Name: comments_slug_noaccents_upper; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX comments_slug_noaccents_upper ON comments USING btree (noaccents_upper((slug)::text));


--
-- Name: compositions_etape_id_trigger_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX compositions_etape_id_trigger_id_idx ON wkf_compositions USING btree (etape_id, trigger_id);


--
-- Name: compteurs_sequence_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX compteurs_sequence_id_idx ON compteurs USING btree (sequence_id);


--
-- Name: courriers_affaire_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX courriers_affaire_id_idx ON courriers USING btree (affaire_id);


--
-- Name: courriers_metadonnees_courrier_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX courriers_metadonnees_courrier_id_idx ON courriers_metadonnees USING btree (courrier_id);


--
-- Name: courriers_metadonnees_courrier_id_metadonnee_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX courriers_metadonnees_courrier_id_metadonnee_id_idx ON courriers_metadonnees USING btree (courrier_id, metadonnee_id);


--
-- Name: courriers_metadonnees_metadonnee_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX courriers_metadonnees_metadonnee_id_idx ON courriers_metadonnees USING btree (metadonnee_id);


--
-- Name: courriers_parent_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX courriers_parent_id_idx ON courriers USING btree (parent_id);


--
-- Name: courriers_reference_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX courriers_reference_idx ON courriers USING btree (reference);


--
-- Name: courriers_repertoires_courrier_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX courriers_repertoires_courrier_id_idx ON courriers_repertoires USING btree (courrier_id);


--
-- Name: courriers_repertoires_courrier_id_repertoire_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX courriers_repertoires_courrier_id_repertoire_id_idx ON courriers_repertoires USING btree (courrier_id, repertoire_id);


--
-- Name: courriers_repertoires_repertoire_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX courriers_repertoires_repertoire_id_idx ON courriers_repertoires USING btree (repertoire_id);


--
-- Name: formatsreponse_soustypes_formatreponse_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX formatsreponse_soustypes_formatreponse_id_idx ON formatsreponse_soustypes USING btree (formatreponse_id);


--
-- Name: formatsreponse_soustypes_soustype_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX formatsreponse_soustypes_soustype_id_idx ON formatsreponse_soustypes USING btree (soustype_id);


--
-- Name: metadonnees_soustypes_metadonnee_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX metadonnees_soustypes_metadonnee_id_idx ON metadonnees_soustypes USING btree (metadonnee_id);


--
-- Name: metadonnees_soustypes_metadonnee_id_soustype_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX metadonnees_soustypes_metadonnee_id_soustype_id_idx ON metadonnees_soustypes USING btree (metadonnee_id, soustype_id);


--
-- Name: metadonnees_soustypes_soustype_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX metadonnees_soustypes_soustype_id_idx ON metadonnees_soustypes USING btree (soustype_id);


--
-- Name: metadonnees_typemetadonnee_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX metadonnees_typemetadonnee_id_idx ON metadonnees USING btree (typemetadonnee_id);


--
-- Name: recherches_affaires_affaire_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_affaires_affaire_id_idx ON recherches_affaires USING btree (affaire_id);


--
-- Name: recherches_affaires_recherche_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_affaires_recherche_id_idx ON recherches_affaires USING btree (recherche_id);


--
-- Name: recherches_circuits_circuit_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_circuits_circuit_id_idx ON recherches_circuits USING btree (circuit_id);


--
-- Name: recherches_circuits_recherche_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_circuits_recherche_id_idx ON recherches_circuits USING btree (recherche_id);


--
-- Name: recherches_desktops_desktop_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_desktops_desktop_id_idx ON recherches_desktops USING btree (desktop_id);


--
-- Name: recherches_desktops_recherche_id_desktop_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX recherches_desktops_recherche_id_desktop_id_idx ON recherches_desktops USING btree (recherche_id, desktop_id);


--
-- Name: recherches_desktops_recherche_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_desktops_recherche_id_idx ON recherches_desktops USING btree (recherche_id);


--
-- Name: recherches_dossiers_dossier_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_dossiers_dossier_id_idx ON recherches_dossiers USING btree (dossier_id);


--
-- Name: recherches_dossiers_recherche_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_dossiers_recherche_id_idx ON recherches_dossiers USING btree (recherche_id);


--
-- Name: recherches_metadonnees_metadonnee_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_metadonnees_metadonnee_id_idx ON recherches_metadonnees USING btree (metadonnee_id);


--
-- Name: recherches_metadonnees_recherche_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_metadonnees_recherche_id_idx ON recherches_metadonnees USING btree (recherche_id);


--
-- Name: recherches_selectvaluesmetadonnees_recherche_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_selectvaluesmetadonnees_recherche_id_idx ON recherches_selectvaluesmetadonnees USING btree (recherche_id);


--
-- Name: recherches_selectvaluesmetadonnees_recherche_id_selectvaluemeta; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX recherches_selectvaluesmetadonnees_recherche_id_selectvaluemeta ON recherches_selectvaluesmetadonnees USING btree (recherche_id, selectvaluemetadonnee_id);


--
-- Name: recherches_soustypes_recherche_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_soustypes_recherche_id_idx ON recherches_soustypes USING btree (recherche_id);


--
-- Name: recherches_soustypes_soustype_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_soustypes_soustype_id_idx ON recherches_soustypes USING btree (soustype_id);


--
-- Name: recherches_taches_recherche_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_taches_recherche_id_idx ON recherches_taches USING btree (recherche_id);


--
-- Name: recherches_taches_tache_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_taches_tache_id_idx ON recherches_taches USING btree (tache_id);


--
-- Name: recherches_types_recherche_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_types_recherche_id_idx ON recherches_types USING btree (recherche_id);


--
-- Name: recherches_types_type_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_types_type_id_idx ON recherches_types USING btree (type_id);


--
-- Name: recherches_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX recherches_user_id_idx ON recherches USING btree (user_id);


--
-- Name: repertoires_parent_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX repertoires_parent_id_idx ON repertoires USING btree (parent_id);


--
-- Name: repertoires_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX repertoires_user_id_idx ON repertoires USING btree (user_id);


--
-- Name: scanemails_desktop_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX scanemails_desktop_id_idx ON scanemails USING btree (desktop_id);


--
-- Name: scanemails_name_desktop_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX scanemails_name_desktop_id_idx ON scanemails USING btree (name, desktop_id);


--
-- Name: scanemails_soustype_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX scanemails_soustype_id_idx ON scanemails USING btree (soustype_id);


--
-- Name: scanemails_type_id_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX scanemails_type_id_idx ON scanemails USING btree (type_id);


--
-- Name: services_parent_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX services_parent_id_idx ON services USING btree (parent_id);


--
-- Name: soustypes_circuit_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX soustypes_circuit_id_idx ON soustypes USING btree (circuit_id);


--
-- Name: soustypes_compteur_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX soustypes_compteur_id_idx ON soustypes USING btree (compteur_id);


--
-- Name: soustypes_parent_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX soustypes_parent_id_idx ON soustypes USING btree (parent_id);


--
-- Name: soustypes_type_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX soustypes_type_id_idx ON soustypes USING btree (type_id);


--
-- Name: taches_act_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX taches_act_user_id_idx ON taches USING btree (act_desktop_id);


--
-- Name: taches_courrier_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX taches_courrier_id_idx ON taches USING btree (courrier_id);


--
-- Name: wkf_cicruits_unique_nom; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE UNIQUE INDEX wkf_cicruits_unique_nom ON wkf_circuits USING btree (nom);


--
-- Name: wkf_circuits_created_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_circuits_created_user_id_idx ON wkf_circuits USING btree (created_user_id);


--
-- Name: wkf_circuits_modified_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_circuits_modified_user_id_idx ON wkf_circuits USING btree (modified_user_id);


--
-- Name: wkf_compositions_created_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_compositions_created_user_id_idx ON wkf_compositions USING btree (created_user_id);


--
-- Name: wkf_compositions_etape_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_compositions_etape_id_idx ON wkf_compositions USING btree (etape_id);


--
-- Name: wkf_compositions_modified_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_compositions_modified_user_id_idx ON wkf_compositions USING btree (modified_user_id);


--
-- Name: wkf_compositions_trigger_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_compositions_trigger_id_idx ON wkf_compositions USING btree (trigger_id);


--
-- Name: wkf_etapes_circuit_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_etapes_circuit_id_idx ON wkf_etapes USING btree (circuit_id);


--
-- Name: wkf_etapes_created_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_etapes_created_user_id_idx ON wkf_etapes USING btree (created_user_id);


--
-- Name: wkf_etapes_modified_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_etapes_modified_user_id_idx ON wkf_etapes USING btree (modified_user_id);


--
-- Name: wkf_traitements_circuit_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_traitements_circuit_id_idx ON wkf_traitements USING btree (circuit_id);


--
-- Name: wkf_traitements_created_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_traitements_created_user_id_idx ON wkf_traitements USING btree (created_user_id);


--
-- Name: wkf_traitements_modified_user_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_traitements_modified_user_id_idx ON wkf_traitements USING btree (modified_user_id);


--
-- Name: wkf_traitements_target_id_key; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_traitements_target_id_key ON wkf_traitements USING btree (target_id);


--
-- Name: wkf_visas_signature_id_idx; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_visas_signature_id_idx ON wkf_visas USING btree (signature_id);


--
-- Name: wkf_visas_traitement_id_key; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_visas_traitement_id_key ON wkf_visas USING btree (traitement_id);


--
-- Name: wkf_visas_trigger_id_key; Type: INDEX; Schema: public; Owner: webgfc; Tablespace: 
--

CREATE INDEX wkf_visas_trigger_id_key ON wkf_visas USING btree (trigger_id);


--
-- Name: aco_fk; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY aros_acos
    ADD CONSTRAINT aco_fk FOREIGN KEY (aco_id) REFERENCES acos(id);


--
-- Name: affaires_dossiers_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY affaires
    ADD CONSTRAINT affaires_dossiers_fkey FOREIGN KEY (dossier_id) REFERENCES dossiers(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: armodels_soustype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY armodels
    ADD CONSTRAINT armodels_soustype_id_fkey FOREIGN KEY (soustype_id) REFERENCES soustypes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aro_fk; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY aros_acos
    ADD CONSTRAINT aro_fk FOREIGN KEY (aro_id) REFERENCES aros(id);


--
-- Name: ars_courrier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY ars
    ADD CONSTRAINT ars_courrier_id_fkey FOREIGN KEY (courrier_id) REFERENCES courriers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: bancontenus_bannettes_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY bancontenus
    ADD CONSTRAINT bancontenus_bannettes_fkey FOREIGN KEY (bannette_id) REFERENCES bannettes(id);


--
-- Name: bancontenus_courriers_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY bancontenus
    ADD CONSTRAINT bancontenus_courriers_fkey FOREIGN KEY (courrier_id) REFERENCES courriers(id);


--
-- Name: bancontenus_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY bancontenus
    ADD CONSTRAINT bancontenus_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: bansadresses_bancommune_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY bansadresses
    ADD CONSTRAINT bansadresses_bancommune_id_fkey FOREIGN KEY (bancommune_id) REFERENCES banscommunes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: banscommunes_ban_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY banscommunes
    ADD CONSTRAINT banscommunes_ban_id_fkey FOREIGN KEY (ban_id) REFERENCES bans(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comments_readers_comment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY comments_readers
    ADD CONSTRAINT comments_readers_comment_id_fkey FOREIGN KEY (comment_id) REFERENCES comments(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: compteurs_sequence_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY compteurs
    ADD CONSTRAINT compteurs_sequence_id_fkey FOREIGN KEY (sequence_id) REFERENCES sequences(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contactinfos_contact_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY contactinfos
    ADD CONSTRAINT contactinfos_contact_id_fkey FOREIGN KEY (contact_id) REFERENCES contacts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contacts_addressbook_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY contacts
    ADD CONSTRAINT contacts_addressbook_id_fkey FOREIGN KEY (addressbook_id) REFERENCES addressbooks(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: couriers_affaires_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers
    ADD CONSTRAINT couriers_affaires_fkey FOREIGN KEY (affaire_id) REFERENCES affaires(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: courriers_affairesuiviepar_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers
    ADD CONSTRAINT courriers_affairesuiviepar_id_fkey FOREIGN KEY (affairesuiviepar_id) REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: courriers_contactinfo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers
    ADD CONSTRAINT courriers_contactinfo_id_fkey FOREIGN KEY (contactinfo_id) REFERENCES contactinfos(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: courriers_desktop_creator_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers
    ADD CONSTRAINT courriers_desktop_creator_id_fkey FOREIGN KEY (desktop_creator_id) REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: courriers_metadonnees_courriers_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers_metadonnees
    ADD CONSTRAINT courriers_metadonnees_courriers_fkey FOREIGN KEY (courrier_id) REFERENCES courriers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: courriers_metadonnees_metadonnees_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers_metadonnees
    ADD CONSTRAINT courriers_metadonnees_metadonnees_fkey FOREIGN KEY (metadonnee_id) REFERENCES metadonnees(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: courriers_repertoires_courriers_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers_repertoires
    ADD CONSTRAINT courriers_repertoires_courriers_fkey FOREIGN KEY (courrier_id) REFERENCES courriers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: courriers_repertoires_repertoires_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers_repertoires
    ADD CONSTRAINT courriers_repertoires_repertoires_fkey FOREIGN KEY (repertoire_id) REFERENCES repertoires(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: courriers_service_creator_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers
    ADD CONSTRAINT courriers_service_creator_id_fkey FOREIGN KEY (service_creator_id) REFERENCES services(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: courriers_soustypes_soustype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers
    ADD CONSTRAINT courriers_soustypes_soustype_id_fkey FOREIGN KEY (soustype_id) REFERENCES soustypes(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: courriers_taches_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY taches
    ADD CONSTRAINT courriers_taches_fkey FOREIGN KEY (courrier_id) REFERENCES courriers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: courriers_user_creator_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY courriers
    ADD CONSTRAINT courriers_user_creator_id_fkey FOREIGN KEY (user_creator_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: desktops_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops
    ADD CONSTRAINT desktops_group_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- Name: desktops_services_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops_services
    ADD CONSTRAINT desktops_services_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id);


--
-- Name: desktops_services_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops_services
    ADD CONSTRAINT desktops_services_service_id_fkey FOREIGN KEY (service_id) REFERENCES services(id);


--
-- Name: desktops_taches_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops_taches
    ADD CONSTRAINT desktops_taches_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: desktops_taches_tache_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops_taches
    ADD CONSTRAINT desktops_taches_tache_id_fkey FOREIGN KEY (tache_id) REFERENCES taches(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: desktops_users_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops_users
    ADD CONSTRAINT desktops_users_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id);


--
-- Name: desktops_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY desktops_users
    ADD CONSTRAINT desktops_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: documents_courrier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_courrier_id_fkey FOREIGN KEY (courrier_id) REFERENCES courriers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: formatsreponse_soustypes_formatreponse_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY formatsreponse_soustypes
    ADD CONSTRAINT formatsreponse_soustypes_formatreponse_id_fkey FOREIGN KEY (formatreponse_id) REFERENCES formatsreponse(id);


--
-- Name: formatsreponse_soustypes_soustype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY formatsreponse_soustypes
    ADD CONSTRAINT formatsreponse_soustypes_soustype_id_fkey FOREIGN KEY (soustype_id) REFERENCES soustypes(id);


--
-- Name: groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: inits_services_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY inits_services
    ADD CONSTRAINT inits_services_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inits_services_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY inits_services
    ADD CONSTRAINT inits_services_service_id_fkey FOREIGN KEY (service_id) REFERENCES services(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: keywordlists_keywodtype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY keywordlists
    ADD CONSTRAINT keywordlists_keywodtype_id_fkey FOREIGN KEY (keywordtype_id) REFERENCES keywordtypes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: keywords_keywordlist_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY keywords
    ADD CONSTRAINT keywords_keywordlist_id_fkey FOREIGN KEY (keywordlist_id) REFERENCES keywordlists(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: metadonnees_soustypes_metadonnee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY metadonnees_soustypes
    ADD CONSTRAINT metadonnees_soustypes_metadonnee_id_fkey FOREIGN KEY (metadonnee_id) REFERENCES metadonnees(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: metadonnees_soustypes_soustype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY metadonnees_soustypes
    ADD CONSTRAINT metadonnees_soustypes_soustype_id_fkey FOREIGN KEY (soustype_id) REFERENCES soustypes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: metadonnees_typemetadonnees_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY metadonnees
    ADD CONSTRAINT metadonnees_typemetadonnees_fkey FOREIGN KEY (typemetadonnee_id) REFERENCES typemetadonnees(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: notifications_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id);


--
-- Name: notifications_typenotif_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY notifications
    ADD CONSTRAINT notifications_typenotif_id_fkey FOREIGN KEY (typenotif_id) REFERENCES typenotifs(id);


--
-- Name: notifieddesktops_courrier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY notifieddesktops
    ADD CONSTRAINT notifieddesktops_courrier_id_fkey FOREIGN KEY (courrier_id) REFERENCES courriers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: notifieddesktops_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY notifieddesktops
    ADD CONSTRAINT notifieddesktops_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: notifieddesktops_notification_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY notifieddesktops
    ADD CONSTRAINT notifieddesktops_notification_id_fkey FOREIGN KEY (notification_id) REFERENCES notifications(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: plandelegations_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY plandelegations
    ADD CONSTRAINT plandelegations_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: plandelegations_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY plandelegations
    ADD CONSTRAINT plandelegations_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reacherches_affaires_recherche_id; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_affaires
    ADD CONSTRAINT reacherches_affaires_recherche_id FOREIGN KEY (recherche_id) REFERENCES recherches(id);


--
-- Name: recheche_contactinfos_contactinfo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recheche_contactinfos
    ADD CONSTRAINT recheche_contactinfos_contactinfo_id_fkey FOREIGN KEY (contactinfo_id) REFERENCES recherches(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recheche_contactinfos_recherche_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recheche_contactinfos
    ADD CONSTRAINT recheche_contactinfos_recherche_id_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_addressbooks_addressbook_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_addressbooks
    ADD CONSTRAINT recherches_addressbooks_addressbook_id_fkey FOREIGN KEY (addressbook_id) REFERENCES addressbooks(id);


--
-- Name: recherches_addressbooks_recherche_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_addressbooks
    ADD CONSTRAINT recherches_addressbooks_recherche_id_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id);


--
-- Name: recherches_affaires_affaire_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_affaires
    ADD CONSTRAINT recherches_affaires_affaire_id_fkey FOREIGN KEY (affaire_id) REFERENCES affaires(id);


--
-- Name: recherches_circuits_circuits_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_circuits
    ADD CONSTRAINT recherches_circuits_circuits_fkey FOREIGN KEY (circuit_id) REFERENCES wkf_circuits(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_circuits_recherches_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_circuits
    ADD CONSTRAINT recherches_circuits_recherches_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_contacts_contact_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_contacts
    ADD CONSTRAINT recherches_contacts_contact_id_fkey FOREIGN KEY (contact_id) REFERENCES contacts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_contacts_recherche_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_contacts
    ADD CONSTRAINT recherches_contacts_recherche_id_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_desktops_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_desktops
    ADD CONSTRAINT recherches_desktops_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_desktops_recherche_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_desktops
    ADD CONSTRAINT recherches_desktops_recherche_id_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_dossiers_dossier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_dossiers
    ADD CONSTRAINT recherches_dossiers_dossier_id_fkey FOREIGN KEY (dossier_id) REFERENCES dossiers(id);


--
-- Name: recherches_dossiers_recherche_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_dossiers
    ADD CONSTRAINT recherches_dossiers_recherche_id_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id);


--
-- Name: recherches_metadonnees_metadonnees_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_metadonnees
    ADD CONSTRAINT recherches_metadonnees_metadonnees_fkey FOREIGN KEY (metadonnee_id) REFERENCES metadonnees(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_metadonnees_recherches_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_metadonnees
    ADD CONSTRAINT recherches_metadonnees_recherches_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_selectvaluesmetadonnee_selectvaluemetadonnee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_selectvaluesmetadonnees
    ADD CONSTRAINT recherches_selectvaluesmetadonnee_selectvaluemetadonnee_id_fkey FOREIGN KEY (selectvaluemetadonnee_id) REFERENCES selectvaluesmetadonnees(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_selectvaluesmetadonnees_metadonnee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_selectvaluesmetadonnees
    ADD CONSTRAINT recherches_selectvaluesmetadonnees_metadonnee_id_fkey FOREIGN KEY (metadonnee_id) REFERENCES metadonnees(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_selectvaluesmetadonnees_recherche_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_selectvaluesmetadonnees
    ADD CONSTRAINT recherches_selectvaluesmetadonnees_recherche_id_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: recherches_soustypes_recherche_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_soustypes
    ADD CONSTRAINT recherches_soustypes_recherche_id_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id);


--
-- Name: recherches_soustypes_soustype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_soustypes
    ADD CONSTRAINT recherches_soustypes_soustype_id_fkey FOREIGN KEY (soustype_id) REFERENCES soustypes(id);


--
-- Name: recherches_taches_recherche_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_taches
    ADD CONSTRAINT recherches_taches_recherche_id_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id);


--
-- Name: recherches_taches_tache_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_taches
    ADD CONSTRAINT recherches_taches_tache_id_fkey FOREIGN KEY (tache_id) REFERENCES taches(id);


--
-- Name: recherches_types_recherche_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_types
    ADD CONSTRAINT recherches_types_recherche_id_fkey FOREIGN KEY (recherche_id) REFERENCES recherches(id);


--
-- Name: recherches_types_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches_types
    ADD CONSTRAINT recherches_types_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id);


--
-- Name: recherches_users_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY recherches
    ADD CONSTRAINT recherches_users_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: referentielsfantoircom_referentielfantoir_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY referentielsfantoircom
    ADD CONSTRAINT referentielsfantoircom_referentielfantoir_id_fkey FOREIGN KEY (referentielfantoir_id) REFERENCES referentielsfantoir(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: referentielsfantoircom_referentielfantoirdir_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY referentielsfantoircom
    ADD CONSTRAINT referentielsfantoircom_referentielfantoirdir_id_fkey FOREIGN KEY (referentielfantoirdir_id) REFERENCES referentielsfantoirdir(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: referentielsfantoirdir_referentielfantoir_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY referentielsfantoirdir
    ADD CONSTRAINT referentielsfantoirdir_referentielfantoir_id_fkey FOREIGN KEY (referentielfantoir_id) REFERENCES referentielsfantoir(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: referentielsfantoirvoie_referentielfantoir_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY referentielsfantoirvoie
    ADD CONSTRAINT referentielsfantoirvoie_referentielfantoir_id_fkey FOREIGN KEY (referentielfantoir_id) REFERENCES referentielsfantoir(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: referentielsfantoirvoie_referentielfantoircom_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY referentielsfantoirvoie
    ADD CONSTRAINT referentielsfantoirvoie_referentielfantoircom_id_fkey FOREIGN KEY (referentielfantoircom_id) REFERENCES referentielsfantoircom(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: relements_courrier_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY relements
    ADD CONSTRAINT relements_courrier_id_fkey FOREIGN KEY (courrier_id) REFERENCES courriers(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: repertoires_users_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY repertoires
    ADD CONSTRAINT repertoires_users_fkey FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rmodels_soustype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY rmodels
    ADD CONSTRAINT rmodels_soustype_id_fkey FOREIGN KEY (soustype_id) REFERENCES soustypes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: scanemails_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY scanemails
    ADD CONSTRAINT scanemails_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: scanemails_soustype_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY scanemails
    ADD CONSTRAINT scanemails_soustype_id_fkey FOREIGN KEY (soustype_id) REFERENCES soustypes(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: scanemails_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY scanemails
    ADD CONSTRAINT scanemails_type_id_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: selectvaluesmetadonnees_metadonnee_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY selectvaluesmetadonnees
    ADD CONSTRAINT selectvaluesmetadonnees_metadonnee_id_fkey FOREIGN KEY (metadonnee_id) REFERENCES metadonnees(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: soustypes_compteur_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY soustypes
    ADD CONSTRAINT soustypes_compteur_id_fkey FOREIGN KEY (compteur_id) REFERENCES compteurs(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: soustypes_types_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY soustypes
    ADD CONSTRAINT soustypes_types_fkey FOREIGN KEY (type_id) REFERENCES types(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: typenotifs_users_typenotif_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY typenotifs_users
    ADD CONSTRAINT typenotifs_users_typenotif_id_fkey FOREIGN KEY (typenotif_id) REFERENCES typenotifs(id);


--
-- Name: typenotifs_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY typenotifs_users
    ADD CONSTRAINT typenotifs_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: users_desktop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_desktop_id_fkey FOREIGN KEY (desktop_id) REFERENCES desktops(id) ON UPDATE CASCADE;


--
-- Name: users_wkf_circuits_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_circuits
    ADD CONSTRAINT users_wkf_circuits_fkey FOREIGN KEY (created_user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users_wkf_circuits_fkey2; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_circuits
    ADD CONSTRAINT users_wkf_circuits_fkey2 FOREIGN KEY (modified_user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wkf_circuits_wkf_compositions_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_etapes
    ADD CONSTRAINT wkf_circuits_wkf_compositions_fkey FOREIGN KEY (circuit_id) REFERENCES wkf_circuits(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wkf_compositions_etape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_compositions
    ADD CONSTRAINT wkf_compositions_etape_id_fkey FOREIGN KEY (etape_id) REFERENCES wkf_etapes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wkf_signatures_visa_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_signatures
    ADD CONSTRAINT wkf_signatures_visa_id_fkey FOREIGN KEY (visa_id) REFERENCES wkf_visas(id);


--
-- Name: wkf_traitements_circuit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_traitements
    ADD CONSTRAINT wkf_traitements_circuit_id_fkey FOREIGN KEY (circuit_id) REFERENCES wkf_circuits(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wkf_traitements_wkf_visas_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_visas
    ADD CONSTRAINT wkf_traitements_wkf_visas_fkey FOREIGN KEY (traitement_id) REFERENCES wkf_traitements(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wkf_visas_etape_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY wkf_visas
    ADD CONSTRAINT wkf_visas_etape_id_fkey FOREIGN KEY (etape_id) REFERENCES wkf_etapes(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM webgfc;
GRANT ALL ON SCHEMA public TO webgfc;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

