-- 319_patch_3.1.21.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

BEGIN;

SELECT add_missing_table_field ('public', 'contacts', 'compl2', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'organismes', 'compl2', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'contacts', 'cedex', 'VARCHAR(15)');
SELECT add_missing_table_field ('public', 'organismes', 'cedex', 'VARCHAR(15)');

INSERT INTO metadonnees (id, name, typemetadonnee_id, champfusion, created, modified, active, isrepercutable) VALUES ('-1', 'Date de clôture', 1, 'dateclos', now(), now(), true, true);
INSERT INTO dacos (id, model, foreign_key, alias, lft, rght) VALUES ( -3, 'Metadonnee', '-1', 'Date_de_clôture.-1', (SELECT max(dacos.lft) + 1 FROM dacos), (SELECT max(dacos.rght) + 1 FROM dacos));

COMMIT;
