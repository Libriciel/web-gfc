-- 306_patch_2.2.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

BEGIN;

--- Ajout de champs pour le nouveau système de LDAP
SELECT add_missing_table_field ('public', 'connecteurs', 'use_grc', 'BOOLEAN');
ALTER TABLE connecteurs ALTER COLUMN use_grc SET DEFAULT false;
UPDATE connecteurs SET use_grc = false;

SELECT add_missing_table_field ('public', 'connecteurs', 'grc_active', 'BOOLEAN');
ALTER TABLE connecteurs ALTER COLUMN grc_active SET DEFAULT false;
UPDATE connecteurs SET grc_active = false;

SELECT add_missing_table_field ('public', 'connecteurs', 'grc_apikey', 'VARCHAR(255)');
UPDATE connecteurs SET grc_apikey = '7a1182abe571c90584c2541ecs3a0f54';

SELECT add_missing_table_field ('public', 'connecteurs', 'grc_appname', 'VARCHAR(255)');
UPDATE connecteurs SET grc_appname = 'Webgfc';

SELECT add_missing_table_field ('public', 'connecteurs', 'grc_host', 'VARCHAR(255)');
UPDATE connecteurs SET grc_host = 'https://grc28.localeo.fr/public/webgfc/';

SELECT add_missing_table_field ('public', 'connecteurs', 'grc_cityid', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'connecteurs', 'grc_canalid', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'connecteurs', 'grc_servid', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'connecteurs', 'grc_sqrtid', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'connecteurs', 'grc_qrtid', 'VARCHAR(255)');

INSERT INTO connecteurs ( name, grc_apikey, grc_appname, grc_host, grc_cityid, grc_canalid, grc_servid, grc_sqrtid, grc_qrtid, created, modified ) VALUES
( 'GRC Localeo', '7a1182abe571c90584c2541ecs3a0f54', 'Webgfc', 'https://grc28.localeo.fr/public/webgfc/', '73', '4', '4128', '12177', '125', NOW(), NOW() );

SELECT add_missing_table_field ('public', 'types', 'grc_servid', 'VARCHAR(255)');

UPDATE courriers_metadonnees SET valeur ='Oui' WHERE valeur='1' AND metadonnee_id IN (SELECT id FROM metadonnees WHERE typemetadonnee_id = 3 );
UPDATE courriers_metadonnees SET valeur ='Non' WHERE valeur='0' AND metadonnee_id IN (SELECT id FROM metadonnees WHERE typemetadonnee_id = 3 );
SELECT add_missing_table_field( 'public', 'courriers', 'statutgrc', 'VARCHAR(10)' );

SELECT add_missing_table_field( 'public', 'services', 'scan_active', 'BOOLEAN' );
SELECT add_missing_table_field( 'public', 'services', 'scan_local_path', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'services', 'scan_service_name', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'services', 'scan_desktopmanager_id', 'INTEGER' );
SELECT add_missing_constraint ( 'public', 'services', 'services_scan_desktopmanager_id_fkey', 'desktopsmanagers', 'scan_desktopmanager_id', false );
SELECT add_missing_table_field( 'public', 'services', 'scan_purge_delay', 'INTEGER' );


SELECT alter_table_drop_constraint_if_exists( 'public', 'notifications', 'notifications_desktop_id_fkey' );
SELECT add_missing_constraint ( 'public', 'notifications', 'notifications_desktop_id_fkey', 'desktops', 'desktop_id', true );

SELECT alter_table_drop_constraint_if_exists( 'public', 'wkf_visas', 'wkf_visas_etape_id_fkey' );
ALTER TABLE wkf_visas ADD CONSTRAINT wkf_visas_etape_id_fkey FOREIGN KEY (etape_id) REFERENCES wkf_etapes(id) ON UPDATE CASCADE ON DELETE SET NULL;


SELECT add_missing_table_field ('public', 'metadonnees', 'active', 'BOOLEAN');
ALTER TABLE metadonnees ALTER COLUMN active SET DEFAULT true;
UPDATE metadonnees SET active = true;

SELECT add_missing_table_field ('public', 'selectvaluesmetadonnees', 'active', 'BOOLEAN');
ALTER TABLE selectvaluesmetadonnees ALTER COLUMN active SET DEFAULT true;
UPDATE selectvaluesmetadonnees SET active = true;

SELECT add_missing_table_field( 'public', 'bansadresses', 'code_insee_ancienne_commune', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'bansadresses', 'libelle_acheminement', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'bansadresses', 'nom_ancienne_commune', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'bansadresses', 'source_position', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'bansadresses', 'source_nom_voie', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'bansadresses', 'date_der_maj', 'TIMESTAMP WITHOUT TIME ZONE' );
SELECT add_missing_table_field( 'public', 'banscommunes', 'date_der_maj', 'TIMESTAMP WITHOUT TIME ZONE' );


SELECT add_missing_table_field( 'public', 'scanemails', 'configuration', 'VARCHAR(255)' );
ALTER TABLE scanemails ALTER COLUMN configuration SET DEFAULT '/novalidate-cert';

COMMIT;
