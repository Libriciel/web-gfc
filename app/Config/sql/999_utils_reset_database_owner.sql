-- 999_utils_reset_database_owner .sql script
-- Prépare les requêtes permettant de redéfinir l'utilisateur webgfc
-- comme propriétaire des tables et séquences d'une base de données
-- d'une collectivité
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SELECT 'ALTER TABLE ' || n.nspname || '.' ||c.relname || ' OWNER TO webgfc;' FROM pg_catalog.pg_class AS c LEFT JOIN pg_catalog.pg_namespace AS n ON n.oid = c.relnamespace WHERE relkind = 'r' AND n.nspname NOT IN ('pg_catalog', 'pg_toast') AND pg_catalog.pg_table_is_visible(c.oid);
SELECT 'ALTER SEQUENCE ' || c.relname || ' OWNER TO webgfc;' FROM pg_class c WHERE c.relkind = 'S';

