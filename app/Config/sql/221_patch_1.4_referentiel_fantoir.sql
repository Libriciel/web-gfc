-- 218_patch_1.4_referentiel_fantoir.sql script
-- Patch permettant la mise en place des référentiels FANTOIR
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

DROP TABLE IF EXISTS referentielsfantoir CASCADE;
CREATE TABLE referentielsfantoir (
    id serial not null primary key,
    name	VARCHAR(50) NOT NULL,
	active BOOLEAN DEFAULT '1',
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
COMMENT ON TABLE referentielsfantoir IS 'Table permettant le stockage des référentiels FANTOIR';

/** Ajout pour le référentiel FANTOIR **/
DROP TABLE IF EXISTS referentielsfantoirdir CASCADE;
CREATE TABLE referentielsfantoirdir (
    id serial not null primary key,
    referentielfantoir_id INTEGER NOT NULL REFERENCES referentielsfantoir(id) on update cascade on delete cascade,
    name        VARCHAR(255) NOT NULL,
    codedpt    VARCHAR(2) NOT NULL,
    codedir    VARCHAR(1) NOT NULL,
	libelledir    VARCHAR(30) NOT NULL,
	active BOOLEAN DEFAULT '1',
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
COMMENT ON TABLE referentielsfantoirdir IS 'Table permettant le stockage des référentiels FANTOIR de direction';


DROP TABLE IF EXISTS referentielsfantoircom CASCADE;
CREATE TABLE referentielsfantoircom (
    id serial not null primary key,
    referentielfantoir_id INTEGER NOT NULL REFERENCES referentielsfantoir(id) on update cascade on delete cascade,
    referentielfantoirdir_id INTEGER NOT NULL REFERENCES referentielsfantoirdir(id) on update cascade on delete cascade,
    name        VARCHAR(255) NOT NULL,
    codecom 	VARCHAR(3) NOT NULL,
    clerivoli    VARCHAR(1) NOT NULL,
	libellecom	VARCHAR(30) NOT NULL,
	typecom		VARCHAR(1) NOT NULL,
	caracrur	VARCHAR(1) DEFAULT NULL,
	caracpop	VARCHAR(1) DEFAULT NULL,
	popreelle	VARCHAR(7) DEFAULT NULL,
	popapart	VARCHAR(7) DEFAULT NULL,
	popfictive	VARCHAR(7) DEFAULT NULL,
	caracannul	VARCHAR(1) DEFAULT NULL,
	dateannul	VARCHAR(7) DEFAULT NULL,
	datecreation	VARCHAR(7) DEFAULT NULL,
	active BOOLEAN DEFAULT '1',
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
COMMENT ON TABLE referentielsfantoircom IS 'Table permettant le stockage des référentiels FANTOIR de commune';

ALTER TABLE referentielsfantoircom ADD CONSTRAINT referentielsfantoircom_typecom_in_list_chk CHECK (cakephp_validate_in_list(typecom::text, ARRAY['N'::text, 'R'::text]));


DROP TABLE IF EXISTS referentielsfantoirvoie CASCADE;
CREATE TABLE referentielsfantoirvoie (
    id serial not null primary key,
    referentielfantoir_id INTEGER NOT NULL REFERENCES referentielsfantoir(id) on update cascade on delete cascade,
    referentielfantoircom_id INTEGER NOT NULL REFERENCES referentielsfantoircom(id) on update cascade on delete cascade,
    name        VARCHAR(255) NOT NULL,
    identvoie 	VARCHAR(4) NOT NULL,
    clerivoli    VARCHAR(1) NOT NULL,
    codevoie	VARCHAR(4) DEFAULT NULL,
	libellevoie	VARCHAR(26) NOT NULL,
	typecom		VARCHAR(1) NOT NULL,
	caracrur	VARCHAR(1) DEFAULT NULL,
	caracvoie	VARCHAR(1) NOT NULL,
	caracpop	VARCHAR(1) DEFAULT NULL,
	popapart	VARCHAR(7) DEFAULT NULL,
	popfictive	VARCHAR(7) DEFAULT NULL,
	caracannul	VARCHAR(1) DEFAULT NULL,
	dateannul	VARCHAR(7) DEFAULT NULL,
	datecreation	VARCHAR(7) DEFAULT NULL,
	codemajic	VARCHAR(5) NOT NULL,
	typevoie	VARCHAR(1) NOT NULL,
	caraclieudit	VARCHAR(1) DEFAULT NULL,
	libentiervoie	VARCHAR(8) NOT NULL,
	active BOOLEAN DEFAULT '1',
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
COMMENT ON TABLE referentielsfantoirvoie IS 'Table permettant le stockage des référentiels FANTOIR de voie';

ALTER TABLE referentielsfantoirvoie ADD CONSTRAINT referentielsfantoirvoie_typecom_in_list_chk CHECK (cakephp_validate_in_list(typecom::text, ARRAY['N'::text, 'R'::text]));
ALTER TABLE referentielsfantoirvoie ADD CONSTRAINT referentielsfantoirvoie_caracvoie_in_list_chk CHECK (cakephp_validate_in_list(caracvoie::text, ARRAY['1'::text, '0'::text]));
ALTER TABLE referentielsfantoirvoie ADD CONSTRAINT referentielsfantoirvoie_typevoie_in_list_chk CHECK (cakephp_validate_in_list(typevoie::text, ARRAY['1'::text, '2'::text,'3'::text,'4'::text,'5'::text]));




/* Ajout d'une table pour stocker les origines de flux */

DROP TABLE IF EXISTS typologiesflux CASCADE;
DROP TABLE IF EXISTS originesflux CASCADE;
CREATE TABLE originesflux (
    id serial not null primary key,
    name	VARCHAR(100) NOT NULL,
	active BOOLEAN DEFAULT '1',
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
COMMENT ON TABLE originesflux IS 'Table permettant le stockage des origines de flux';

/* Ajout du champ origineflux_id dans la table courriers*/
SELECT add_missing_table_field ('public', 'courriers', 'origineflux_id', 'INTEGER');

UPDATE aros_acos SET (_create, _read, _update, _delete) = (1,1,1,1) WHERE aro_id = '2';

/* Remise ne place des droit sur la fonction getContext pour avoir les droits sur la consultation des flux */
UPDATE aros_acos SET (_create, _read, _update, _delete) = (1,1,1,1) WHERE aco_id = '258';

/* Ajout des droits en tant qu'administration sur la consultaton des scans */
SELECT add_missing_table_field ('public', 'rights', 'consult_scan', 'BOOLEAN');

/** BAN **/
-- begin;
DROP TABLE IF EXISTS bans CASCADE;
CREATE TABLE bans (
    id serial not null primary key,
    name        VARCHAR(100) NOT NULL,
	active BOOLEAN DEFAULT '1',
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
COMMENT ON TABLE bans IS 'Table permettant le stockage des Bases Adresse Nationale';
DROP INDEX IF EXISTS bans_name_idx;
CREATE UNIQUE INDEX bans_name_idx ON bans( name );
/*
DROP TABLE IF EXISTS banscommunes CASCADE;
CREATE TABLE banscommunes (
    id serial not null primary key,
    ban_id      INTEGER NOT NULL REFERENCES bans(id) on update cascade on delete cascade,
    name        VARCHAR(100) NOT NULL,
	ident       VARCHAR(30) NOT NULL,
    nom_voie    VARCHAR(255),
    id_fantoir    VARCHAR(255),
    numero    VARCHAR(255),
    rep    VARCHAR(255),
    code_insee    VARCHAR(255),
    code_post    VARCHAR(255),
    alias    VARCHAR(255),
    nom_ld    VARCHAR(255),
    nom_afnor    VARCHAR(255),
    x    VARCHAR(255),
    y    VARCHAR(255),
    lon    VARCHAR(255),
    lat    VARCHAR(255),
    nom_commune    VARCHAR(255),
	active BOOLEAN DEFAULT '1',
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
COMMENT ON TABLE banscommunes IS 'Table permettant le stockage des Communes par Base Adresse Nationale';

DROP INDEX IF EXISTS banscommunes_name_idx;
CREATE INDEX banscommunes_name_idx ON banscommunes( name );
DROP INDEX IF EXISTS banscommunes_name_ban_id_idx;
CREATE INDEX banscommunes_name_ban_id_idx ON banscommunes( name, ban_id );
*/

DROP TABLE IF EXISTS banscommunes CASCADE;
CREATE TABLE banscommunes (
    id serial not null primary key,
    ban_id      INTEGER NOT NULL REFERENCES bans(id) on update cascade on delete cascade,
    name        VARCHAR(100) NOT NULL,
	active BOOLEAN DEFAULT '1',
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
COMMENT ON TABLE banscommunes IS 'Table permettant le stockage des Communes par Base Adresse Nationale';

DROP INDEX IF EXISTS banscommunes_name_ban_id_idx;
CREATE UNIQUE INDEX banscommunes_name_ban_id_idx ON banscommunes( name, ban_id );



DROP TABLE IF EXISTS bansadresses CASCADE;
CREATE TABLE bansadresses (
    id serial not null primary key,
    bancommune_id      INTEGER NOT NULL REFERENCES banscommunes(id) on update cascade on delete cascade,
    name        VARCHAR(100) NOT NULL,
    ident       VARCHAR(30) NOT NULL,
    nom_voie    VARCHAR(255),
    id_fantoir    VARCHAR(255),
    numero    VARCHAR(255),
    rep    VARCHAR(255),
    code_insee    VARCHAR(255),
    code_post    VARCHAR(255),
    alias    VARCHAR(255),
    nom_ld    VARCHAR(255),
    nom_afnor    VARCHAR(255),
    x    VARCHAR(255),
    y    VARCHAR(255),
    lon    VARCHAR(255),
    lat    VARCHAR(255),
    nom_commune    VARCHAR(255),
	active BOOLEAN DEFAULT '1',
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
COMMENT ON TABLE bansadresses IS 'Table permettant le stockage des adresses par commune de la Base Adresse Nationale';
-- DROP INDEX IF EXISTS bansadresses_name_bancommune_id_idx;
-- CREATE UNIQUE INDEX bansadresses_name_bancommune_id_idx ON bansadresses( name, bancommune_id );

SELECT add_missing_table_field ('public', 'contactinfos', 'adressecomplete', 'VARCHAR(255)');

-- Permat de supprimer une étape si une entrée est tout de même présente dans la table wkf_visas
ALTER TABLE wkf_visas DROP CONSTRAINT "wkf_visas_etape_id_fkey";
ALTER TABLE wkf_visas ADD FOREIGN KEY(etape_id) REFERENCES wkf_etapes(id) ON DELETE CASCADE ON UPDATE CASCADE;

SELECT add_missing_table_field ('public', 'metadonnees_soustypes', 'obligatoire', 'BOOLEAN');
COMMIT;
