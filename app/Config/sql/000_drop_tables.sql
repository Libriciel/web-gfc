-- 000_drop_tables.sql script
-- Préparation des requêtes de suppression de toutes les tables et sequences 'dune base de données PostgreSQL
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SELECT 'DROP TABLE ' || n.nspname || '.' ||c.relname || ' CASCADE;' FROM pg_catalog.pg_class AS c LEFT JOIN pg_catalog.pg_namespace AS n ON n.oid = c.relnamespace WHERE relkind = 'r' AND n.nspname NOT IN ('pg_catalog', 'pg_toast') AND pg_catalog.pg_table_is_visible(c.oid);
SELECT 'DROP SEQUENCE ' || c.relname || ' CASCADE;' FROM pg_class c WHERE c.relkind = 'S';

