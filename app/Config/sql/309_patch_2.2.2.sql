-- 309_patch_2.2.2.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

BEGIN;

SELECT alter_table_drop_constraint_if_exists( 'public', 'contacts', 'contacts_titre_id_fkey' );

SELECT add_missing_constraint ( 'public', 'contacts', 'contacts_titre_id_fkey', 'titres', 'titre_id', false );

COMMIT;
