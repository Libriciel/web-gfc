-- 304_patch_2.1.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

BEGIN;

DROP TABLE IF EXISTS ldapm_groups CASCADE;
DROP SEQUENCE IF EXISTS ldapm_groups_id_seq;
CREATE TABLE ldapm_groups
(
    id serial,
    parent_id integer,
    lft integer,
    rght integer,
    name character varying(500),
    readonly BOOLEAN,
    active BOOLEAN,
    dn TEXT,
    CONSTRAINT ldapm_groups_pkey PRIMARY KEY (id)
);

DROP TABLE IF EXISTS ldapm_models_groups CASCADE;
DROP SEQUENCE IF EXISTS ldapm_models_groups_id_seq;
CREATE TABLE ldapm_models_groups
(
    id serial,
    ldapm_groups_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    foreign_key integer,
    CONSTRAINT ldapm_models_groups_pkey PRIMARY KEY (id)
);


--- Ajout de champs pour le nouveau système de LDAP
SELECT add_missing_table_field ('public', 'connecteurs', 'use_ldap', 'BOOLEAN');
ALTER TABLE connecteurs ALTER COLUMN use_ldap SET DEFAULT false;
UPDATE connecteurs SET use_ldap = false;
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_active', 'BOOLEAN');
ALTER TABLE connecteurs ALTER COLUMN ldap_active SET DEFAULT false;
UPDATE connecteurs SET ldap_active = false;
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_type', 'VARCHAR(50)');
ALTER TABLE connecteurs ALTER COLUMN ldap_type SET DEFAULT 'OpenLDAP';
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_host', 'VARCHAR(255)');
ALTER TABLE connecteurs ALTER COLUMN ldap_host SET DEFAULT 'localhost';
UPDATE connecteurs SET ldap_host = 'localhost';
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_port', 'VARCHAR(5)');
ALTER TABLE connecteurs ALTER COLUMN ldap_port SET DEFAULT '389';
UPDATE connecteurs SET ldap_port = '389';
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_uniqueid', 'VARCHAR(255)');
ALTER TABLE connecteurs ALTER COLUMN ldap_uniqueid SET DEFAULT 'UID';
UPDATE connecteurs SET ldap_uniqueid = 'UID';
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_base_dn', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_account_suffix', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_dn', 'VARCHAR(255)');
ALTER TABLE connecteurs ALTER COLUMN ldap_dn SET DEFAULT 'dn';
UPDATE connecteurs SET ldap_dn = 'dn';
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_has_fall_over', 'TEXT');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_login', 'TEXT');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_password', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_tls', 'BOOLEAN');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_version', 'TEXT');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_username', 'VARCHAR(100)');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_note', 'VARCHAR(100)');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_nom', 'VARCHAR(100)');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_prenom', 'VARCHAR(100)');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_mail', 'VARCHAR(100)');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_numtel', 'VARCHAR(100)');
SELECT add_missing_table_field ('public', 'connecteurs', 'ldap_portable', 'VARCHAR(100)');


INSERT INTO connecteurs ( name, ldap_note, ldap_username, ldap_nom, ldap_prenom, ldap_mail, ldap_numtel, ldap_portable, created, modified ) VALUES ( 'Annuaire LDAP', 'info', 'samaccountname', 'sn', 'givenname', 'mail', 'telephonenumber', 'mobile', NOW(), NOW() );


--- On rempalce troutes les occurences Group, group_id, groups par Profil, profil_id,
ALTER TABLE groups RENAME TO profils;
ALTER TABLE profils RENAME COLUMN group_id TO profil_id;
ALTER TABLE profils DROP CONSTRAINT groups_group_id_fkey;
SELECT add_missing_constraint ( 'public', 'profils', 'profils_profil_id_fkey', 'profils', 'profil_id', false );

ALTER TABLE desktops RENAME COLUMN group_id TO profil_id;
ALTER TABLE desktops DROP CONSTRAINT desktops_group_id_fkey;
SELECT add_missing_constraint ( 'public', 'desktops', 'desktops_profil_id_fkey', 'profils', 'profil_id', false );

update aros set model='Profil' WHERE model='Group';

update acos set alias='Profils' WHERE alias='Groups';
update acos set alias='getProfils' WHERE alias='getGroups';
update acos set alias='linkProfil' WHERE alias='linkGroup';
update aros set alias='unlinkProfil' WHERE alias='unlinkGroup';
update rights set model='Profil' where model='Group';

SELECT add_missing_constraint ( 'public', 'ldapm_models_groups', 'ldapm_models_groups_ldapm_groups_id_fkey', 'ldapm_groups', 'ldapm_groups_id', false );


ALTER SEQUENCE groups_id_seq RENAME TO profils_id_seq;

ALTER TABLE profils DROP CONSTRAINT groups_pkey CASCADE;
ALTER TABLE profils DROP CONSTRAINT groups_name_unique CASCADE;
ALTER TABLE ONLY public.profils ADD CONSTRAINT profils_name_unique UNIQUE (name);
ALTER TABLE ONLY public.profils ADD CONSTRAINT profils_pkey PRIMARY KEY (id);



SELECT add_missing_table_field( 'public', 'wkf_visas', 'soustype', 'INTEGER' );

-- En lien avec Localeo
SELECT add_missing_table_field( 'public', 'courriers', 'contactgrc_id', 'INTEGER' );
SELECT add_missing_table_field( 'public', 'courriers', 'courriergrc_id', 'INTEGER' );
SELECT add_missing_table_field( 'public', 'contacts', 'contactgrc_id', 'INTEGER' );
SELECT add_missing_table_field( 'public', 'organismes', 'contactgrc_id', 'INTEGER' );
SELECT add_missing_table_field( 'public', 'organismes', 'organismegrc_id', 'INTEGER' );

-- Ajout d'une table permettant de stocker les soustypes cibles liés à une réponse
DROP TABLE IF EXISTS soustypes_soustypescibles;
CREATE TABLE soustypes_soustypescibles(
    id SERIAL NOT NULL PRIMARY KEY,
    soustype_id integer not null references soustypes(id) on update cascade on delete cascade,
    soustypecible_id integer not null references soustypes(id) on update cascade on delete cascade,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE soustypes_soustypescibles IS 'Table de liaison entre un sous-type et se(s) sous-type(s) de référence pour une réponse';

DROP INDEX IF EXISTS soustypes_soustypescibles_soustype_id_idx;
CREATE INDEX soustypes_soustypescibles_soustype_id_idx ON soustypes_soustypescibles( soustype_id );

DROP INDEX IF EXISTS soustypes_soustypescibles_soustypecible_id_idx;
CREATE INDEX soustypes_soustypescibles_soustypecible_id_idx ON soustypes_soustypescibles( soustypecible_id );

DROP INDEX IF EXISTS soustypes_soustypescibles_soustype_id_soustypecible_id_idx;
CREATE INDEX soustypes_soustypescibles_soustype_id_soustypecible_id_idx ON soustypes_soustypescibles(soustype_id,soustypecible_id);

-- On bascule les entrées de la table soustypes possédant un parent_id vers la novuelle table soustypes_soustypescilbes
INSERT INTO soustypes_soustypescibles (soustype_id, soustypecible_id) ( SELECT id, parent_id FROM soustypes where parent_id IS NOT NULL);
COMMIT;
