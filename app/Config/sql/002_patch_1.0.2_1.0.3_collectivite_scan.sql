-- 002_patch_1.0.2-1.0.3.sql script
-- Patch de montée de version 1.0.1 vers 1.0.2 pour la basea d'administration
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************


create type t_scan_access_type as ENUM ('local', 'remote', 'imap');
create type t_scan_remote_type as enum ('ftp', 'ssh', 'smb');
create type t_scan_imap_encryption_type as enum('none', 'ssl', 'tls');
create type t_scan_imap_login_encryption_type as enum('plain');

alter table collectivites add column scan_active boolean not null default false;
alter table collectivites add column scan_access_type t_scan_access_type;

alter table collectivites add column scan_local_path character varying(255);

alter table collectivites add column scan_remote_type t_scan_remote_type;
alter table collectivites add column scan_remote_url character varying(255);
alter table collectivites add column scan_remote_port int;
alter table collectivites add column scan_remote_path character varying(255);
alter table collectivites add column scan_remote_user character varying(255);
alter table collectivites add column scan_remote_pass character varying(255);

alter table collectivites add column scan_imap_url character varying(255);
alter table collectivites add column scan_imap_port int;
alter table collectivites add column scan_imap_path character varying(255);
alter table collectivites add column scan_imap_user character varying(255);
alter table collectivites add column scan_imap_pass character varying(255);
alter table collectivites add column scan_imap_encryption_type t_scan_imap_encryption_type;
alter table collectivites add column scan_imap_login_encryption_type t_scan_imap_login_encryption_type;

alter table collectivites add column scan_purge_delay integer not null default 7;
alter table collectivites add column scan_desktop_id integer;



-- *****************************************************************************
COMMIT;
-- *****************************************************************************
