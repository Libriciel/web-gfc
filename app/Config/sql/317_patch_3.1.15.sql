-- 316_patch_3.1.13.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

BEGIN;

SELECT add_missing_table_field ('public', 'users', 'mail_commentaire', 'BOOLEAN');

INSERT INTO templatenotifications VALUES (11, 'commentaire', '[web-GFC]  Nouveau commentaire arrivé', 'Bonjour #PRENOM# #NOM#,

  Un nouveau commentaire sur le flux #REFERENCE_FLUX# vient de vous être adressé

  Id : #IDENTIFIANT_FLUX#
  Référence : #REFERENCE_FLUX#
  Objet : #OBJET_FLUX#
  Circuit : #LIBELLE_CIRCUIT#

  Vous pouvez consulter ce flux à l''adresse : #ADRESSE_A_TRAITER#

  Très cordialement.

  web-GFC

  Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );

SELECT add_missing_table_field ('public', 'soustypes', 'mailservice', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'gabaritsdocuments', 'path', 'TEXT');
SELECT add_missing_table_field ('public', 'scanemails', 'archive_folder', 'VARCHAR(255)');

COMMIT;
