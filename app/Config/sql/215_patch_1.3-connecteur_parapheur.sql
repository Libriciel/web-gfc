-- 215_patch_1.3-connecteur_parapheur.sql script
-- Patch de montée de version 1.2 vers 1.3
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************
--------------------------------------------------------------------------------
-- 20140805: création d'une table pour les nouveaux connecteurs paramétrables dans GFC
--------------------------------------------------------------------------------

DROP TABLE IF EXISTS connecteurs CASCADE;
CREATE TABLE connecteurs (
    id serial not null primary key,
    name    VARCHAR(250) DEFAULT NULL,
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
COMMENT ON TABLE connecteurs IS 'Table permettant la définition des différents connecteurs présents';

INSERT INTO connecteurs ( name, created, modified ) VALUES ( 'Parapheur électronique (Signature)', NOW(), NOW() );



SELECT add_missing_table_field ('public', 'courriers', 'parapheur_etat', 'INTEGER');
SELECT add_missing_table_field ('public', 'courriers', 'parapheur_id', 'VARCHAR(50)');
SELECT add_missing_table_field ('public', 'courriers', 'parapheur_commentaire', 'TEXT');
SELECT add_missing_table_field ('public', 'courriers', 'parapheur_cible', 'VARCHAR(250)');
SELECT add_missing_table_field ('public', 'courriers', 'signee', 'BOOLEAN');
SELECT add_missing_table_field ('public', 'documents', 'parapheur_bordereau', 'BYTEA');
SELECT add_missing_table_field ('public', 'documents', 'courrier_pdf', 'BYTEA');
SELECT add_missing_table_field ('public', 'documents', 'signature', 'BYTEA');

INSERT INTO desktops (name, group_id, created, modified, id, active) VALUES ( 'Parapheur', '5', NOW(), NOW(), '-1',TRUE);

SELECT add_missing_table_field ('public', 'courriers', 'motifrefus', 'TEXT');

--------------------------------------------------------------------------------
-- 20141021: ajout du champ permettant de stocker la date d'envoi au parapheur
--------------------------------------------------------------------------------
SELECT add_missing_table_field ('public', 'courriers', 'dateenvoiparapheur', 'TIMESTAMP WITHOUT TIME ZONE');
-- *****************************************************************************
COMMIT;
-- *****************************************************************************



