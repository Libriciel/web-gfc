-- 213_patch_1.2-new_typemetadonnees.sql script
-- Patch de montée de version 1.1 vers 1.2
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

--------------------------------------------------------------------------------
-- 20140516: Ajout d'un nouveau type de métadonnées
--------------------------------------------------------------------------------
INSERT INTO typemetadonnees ( name, created, modified ) VALUES ( 'select', NOW(), NOW() );

DROP TABLE IF EXISTS selectvaluesmetadonnees CASCADE;
CREATE TABLE selectvaluesmetadonnees (
    id serial not null primary key,
	metadonnee_id INTEGER NOT NULL REFERENCES metadonnees(id) on update cascade on delete cascade,
    name    VARCHAR(250) DEFAULT NULL,
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);

-- *****************************************************************************
COMMIT;
-- *****************************************************************************

