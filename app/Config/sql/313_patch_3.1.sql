-- 313_patch_3.1.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

BEGIN;

-- Valeurs ajoutées pour stocker qui a fait le travail pour qui (délégation)
SELECT add_missing_table_field( 'public', 'bancontenus', 'user_id', 'INTEGER' );

-- Valeurs ajoutées pour définir si la métadonnée sera répercutée dans le flux sortant
SELECT add_missing_table_field( 'public', 'metadonnees', 'isrepercutable', 'BOOLEAN' );


-- Le profil administratuer créé apr défaut ne possède pas de bureau associé à son profil
insert into desktopsmanagers (id, name) values ('-2', 'Bureau administrateur');
insert into desktops_desktopsmanagers (desktop_id, desktopmanager_id)  (select id, '-2' from desktops where name ='Bureau administrateur');

-- Ajout d'une information pour savoir si les commentaires ont été lus ou non
SELECT add_missing_table_field( 'public', 'comments', 'read', 'BOOLEAN' );
ALTER TABLE comments ALTER COLUMN read SET DEFAULT 'false';

-- Valeurs ajoutées pour stocker les types de documents utilisés à l'étape PASTELL dans un circuit
SELECT add_missing_table_field( 'public', 'wkf_compositions', 'type_document', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'wkf_etapes', 'type_document', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'wkf_visas', 'type_document', 'VARCHAR(255)' );

SELECT add_missing_table_field( 'public', 'wkf_compositions', 'inforequired_type_document', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'wkf_etapes', 'inforequired_type_document', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'wkf_visas', 'inforequired_type_document', 'VARCHAR(255)' );


-- Ajout du profil/bureau et la liaison pour la partie PASTELL
INSERT INTO desktops (name, profil_id, created, modified, id, active) VALUES ( 'Pastell', '5', NOW(), NOW(), '-3',TRUE);
INSERT INTO desktopsmanagers (name, created, modified, id, active) VALUES ( 'Bureau Pastell', NOW(), NOW(), '-3',TRUE);
insert into desktops_desktopsmanagers (desktop_id, desktopmanager_id)  VALUES ('-3', '-3');

SELECT add_missing_table_field( 'public', 'courriers', 'dateenvoipastell', 'TIMESTAMP WITHOUT TIME ZONE' );

SELECT add_missing_table_field( 'public', 'courriers', 'deletedpastell_id', 'VARCHAR(7)' );

ALTER TABLE bansadresses ALTER COLUMN ident TYPE VARCHAR(255);

-- Les bureaux créés automatiquement par la synchronisation LDAP seront masqués par défaut dans de la gestion des bureaux
SELECT add_missing_table_field( 'public', 'desktopsmanagers', 'isautocreated', 'BOOLEAN' );
ALTER TABLE desktopsmanagers ALTER COLUMN isautocreated SET DEFAULT 'false';
UPDATE desktopsmanagers SET isautocreated = false;

SELECT add_missing_table_field( 'public', 'soustypes', 'envoi_signature', 'BOOLEAN' );
ALTER TABLE soustypes ALTER COLUMN envoi_signature SET DEFAULT 'false';
UPDATE soustypes SET envoi_signature = false;

SELECT add_missing_table_field( 'public', 'soustypes', 'envoi_mailsec', 'BOOLEAN' );
ALTER TABLE soustypes ALTER COLUMN envoi_mailsec SET DEFAULT 'false';
UPDATE soustypes SET envoi_mailsec = false;

SELECT add_missing_table_field( 'public', 'soustypes', 'envoi_ged', 'BOOLEAN' );
ALTER TABLE soustypes ALTER COLUMN envoi_ged SET DEFAULT 'false';
UPDATE soustypes SET envoi_ged = false;

SELECT add_missing_table_field( 'public', 'soustypes', 'envoi_sae', 'BOOLEAN' );
ALTER TABLE soustypes ALTER COLUMN envoi_sae SET DEFAULT 'false';
UPDATE soustypes SET envoi_sae = false;

SELECT add_missing_table_field( 'public', 'courriers', 'pastell_etat', 'INTEGER' );
ALTER TABLE courriers ALTER COLUMN pastell_etat SET DEFAULT 0;
UPDATE courriers SET pastell_etat = 0;

SELECT add_missing_table_field( 'public', 'courriers', 'pastell_typedoc', 'VARCHAR(255)' );

SELECT add_missing_table_field( 'public', 'comments_readers', 'read', 'BOOLEAN' );
ALTER TABLE comments_readers ALTER COLUMN read SET DEFAULT false;
UPDATE comments_readers SET read = true;

CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA public;

ALTER TABLE courriers ALTER COLUMN signee SET DEFAULT 'false';

SELECT add_missing_table_field( 'public', 'soustypes', 'soustype_parapheur', 'VARCHAR(255)' );

DELETE FROM connecteurs WHERE name = 'Mail Sécurisé';

SELECT add_missing_table_field( 'public', 'bancontenus', 'pastell_id', 'VARCHAR(7)' );
COMMIT;
