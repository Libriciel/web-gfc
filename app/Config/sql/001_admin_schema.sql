BEGIN;
--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: t_scan_access_type; Type: TYPE; Schema: public; Owner: webgfc
--

CREATE TYPE t_scan_access_type AS ENUM (
    'local',
    'remote',
    'imap'
);


ALTER TYPE public.t_scan_access_type OWNER TO webgfc;

--
-- Name: t_scan_imap_encryption_type; Type: TYPE; Schema: public; Owner: webgfc
--

CREATE TYPE t_scan_imap_encryption_type AS ENUM (
    'none',
    'ssl',
    'tls'
);


ALTER TYPE public.t_scan_imap_encryption_type OWNER TO webgfc;

--
-- Name: t_scan_imap_login_encryption_type; Type: TYPE; Schema: public; Owner: webgfc
--

CREATE TYPE t_scan_imap_login_encryption_type AS ENUM (
    'plain'
);


ALTER TYPE public.t_scan_imap_login_encryption_type OWNER TO webgfc;

--
-- Name: t_scan_remote_type; Type: TYPE; Schema: public; Owner: webgfc
--

CREATE TYPE t_scan_remote_type AS ENUM (
    'ftp',
    'ssh',
    'smb'
);


ALTER TYPE public.t_scan_remote_type OWNER TO webgfc;

--
-- Name: add_missing_table_field(text, text, text, text); Type: FUNCTION; Schema: public; Owner: webgfc
--

CREATE FUNCTION add_missing_table_field(text, text, text, text) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
  p_namespace alias for $1;
  p_table     alias for $2;
  p_field     alias for $3;
  p_type      alias for $4;
  v_row       record;
  v_query     text;
BEGIN
  select 1 into v_row from pg_namespace n, pg_class c, pg_attribute a
     where
         --public.slon_quote_brute(n.nspname) = p_namespace and
         n.nspname = p_namespace and
         c.relnamespace = n.oid and
         --public.slon_quote_brute(c.relname) = p_table and
         c.relname = p_table and
         a.attrelid = c.oid and
         --public.slon_quote_brute(a.attname) = p_field;
         a.attname = p_field;
  if not found then
    raise notice 'Upgrade table %.% - add field %', p_namespace, p_table, p_field;
    v_query := 'alter table ' || p_namespace || '.' || p_table || ' add column ';
    v_query := v_query || p_field || ' ' || p_type || ';';
    execute v_query;
    return 't';
  else
    return 'f';
  end if;
END;$_$;


ALTER FUNCTION public.add_missing_table_field(text, text, text, text) OWNER TO webgfc;

--
-- Name: FUNCTION add_missing_table_field(text, text, text, text); Type: COMMENT; Schema: public; Owner: webgfc
--

COMMENT ON FUNCTION add_missing_table_field(text, text, text, text) IS 'Drops a column from a table if it exists.';


--
-- Name: rebuilt_sequences(); Type: FUNCTION; Schema: public; Owner: webgfc
--

CREATE FUNCTION rebuilt_sequences() RETURNS integer
    LANGUAGE plpgsql
    AS $$
  DECLARE sequencedefs RECORD; c integer ;
  BEGIN
    FOR sequencedefs IN Select
      constraint_column_usage.table_name as tablename,
      constraint_column_usage.table_name as tablename,
      constraint_column_usage.column_name as columnname,
      replace(replace(columns.column_default,'''::regclass)',''),'nextval(''','') as sequencename
      from information_schema.constraint_column_usage, information_schema.columns
      where constraint_column_usage.table_schema ='public' AND
      columns.table_schema = 'public' AND columns.table_name=constraint_column_usage.table_name
      AND constraint_column_usage.column_name = columns.column_name
      AND columns.column_default is not null
   LOOP
      EXECUTE 'select max('||sequencedefs.columnname||') from ' || sequencedefs.tablename INTO c;
      IF c is null THEN c = 0; END IF;
      IF c is not null THEN c = c+ 1; END IF;
      EXECUTE 'alter sequence ' || sequencedefs.sequencename ||' restart  with ' || c;
   END LOOP;

   RETURN 1; END;
$$;


ALTER FUNCTION public.rebuilt_sequences() OWNER TO webgfc;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: collectivites; Type: TABLE; Schema: public; Owner: webgfc; Tablespace:
--

CREATE TABLE collectivites (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    conn character varying(255) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    ldap_active boolean DEFAULT false NOT NULL,
    ldap_type character varying(50) DEFAULT 'OpenLDAP'::character varying,
    ldap_host character varying(255) DEFAULT 'localhost'::character varying NOT NULL,
    ldap_port character varying (10) DEFAULT '389'::character varying NOT NULL,
    ldap_uniqueid character varying(255) DEFAULT 'UID'::character varying NOT NULL,
    ldap_base_dn character varying(255),
    ldap_account_suffix character varying(255),
    ldap_dn character varying(255) DEFAULT 'dn'::character varying NOT NULL,
    ldap_use_ad boolean,
    logo character varying(255),
    siren character varying(9),
    adresse character varying(255),
    codeinsee character varying(10),
    codepostal character varying(5),
    ville character varying(255),
    complementadresse character varying(255),
    telephone character varying(20),
    active boolean DEFAULT true NOT NULL,
    login_suffix character varying(255) NOT NULL,
    scan_active boolean DEFAULT false NOT NULL,
    scan_access_type t_scan_access_type,
    scan_local_path character varying(255),
    scan_remote_type t_scan_remote_type,
    scan_remote_url character varying(255),
    scan_remote_port integer,
    scan_remote_path character varying(255),
    scan_remote_user character varying(255),
    scan_remote_pass character varying(255),
    scan_imap_encryption_type t_scan_imap_encryption_type,
    scan_imap_login_encryption_type t_scan_imap_login_encryption_type,
    scan_purge_delay integer DEFAULT 7 NOT NULL,
    scan_imap_url character varying(255),
    scan_imap_port integer,
    scan_imap_path character varying(255),
    scan_imap_user character varying(255),
    scan_imap_pass character varying(255),
    scan_desktop_id integer,
    scanemail_id integer,
    apitoken BYTEA
);


ALTER TABLE public.collectivites OWNER TO webgfc;

--
-- Name: collectivites_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE collectivites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.collectivites_id_seq OWNER TO webgfc;

--
-- Name: collectivites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE collectivites_id_seq OWNED BY collectivites.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: webgfc; Tablespace:
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    prenom character varying(255) NOT NULL,
    mail character varying(255) NOT NULL,
    last_login timestamp without time zone,
    last_logout timestamp without time zone,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL,
    desktop_id integer,
    active boolean DEFAULT true NOT NULL,
    apitoken BYTEA
);


ALTER TABLE public.users OWNER TO webgfc;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO webgfc;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: authentifications; Type: TABLE; Schema: public; Owner: webgfc; Tablespace:
--

CREATE TABLE authentifications(
    id integer NOT NULL,
    host VARCHAR(255) NOT NULL,
    port VARCHAR(5) NOT NULL,
    contexte VARCHAR(255) NOT NULL,
    autcert BYTEA NOT NULL,
    proxy TEXT,
    logpath TEXT NOT NULL,
    use_cas BOOLEAN NOT NULL DEFAULT 'true',
    nom_cert VARCHAR(255),
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.authentifications OWNER TO webgfc;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--

CREATE SEQUENCE authentifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.authentifications_id_seq OWNER TO webgfc;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE authentifications_id_seq OWNED BY authentifications.id;
--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

CREATE TABLE public.configurations(
    id SERIAL NOT NULL,
    config TEXT NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.configurations IS 'Table permettant de paramétrer la page de connexion de web-GFC';

ALTER TABLE public.configurations OWNER TO webgfc;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: webgfc
--
--
-- CREATE SEQUENCE configurations_id_seq
--     START WITH 1
--     INCREMENT BY 1
--     NO MINVALUE
--     NO MAXVALUE
--     CACHE 1;


ALTER TABLE public.configurations_id_seq OWNER TO webgfc;

--
-- Name: configurations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: webgfc
--

ALTER SEQUENCE configurations_id_seq OWNED BY configurations.id;






ALTER TABLE ONLY collectivites ALTER COLUMN id SET DEFAULT nextval('collectivites_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: webgfc
--

ALTER TABLE ONLY authentifications ALTER COLUMN id SET DEFAULT nextval('authentifications_id_seq'::regclass);



ALTER TABLE ONLY configurations ALTER COLUMN id SET DEFAULT nextval('configurations_id_seq'::regclass);

--
-- Name: collectivites_conn_key; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace:
--

ALTER TABLE ONLY collectivites
    ADD CONSTRAINT collectivites_conn_key UNIQUE (conn);


--
-- Name: collectivites_name_key; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace:
--

ALTER TABLE ONLY collectivites
    ADD CONSTRAINT collectivites_name_key UNIQUE (name);


--
-- Name: collectivites_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace:
--

ALTER TABLE ONLY collectivites
    ADD CONSTRAINT collectivites_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace:
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_username_key; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace:
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_key UNIQUE (username);

--
-- Name: authentificationss_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace:
--

ALTER TABLE ONLY authentifications
    ADD CONSTRAINT authentifications_pkey PRIMARY KEY (id);


-- Name: authentificationss_pkey; Type: CONSTRAINT; Schema: public; Owner: webgfc; Tablespace:
--

ALTER TABLE ONLY configurations
    ADD CONSTRAINT configurations_pkey PRIMARY KEY (id);
--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM webgfc;
GRANT ALL ON SCHEMA public TO webgfc;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

COMMIT;
