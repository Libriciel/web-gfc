-- 212_patch_1.2-groups_name_unique.sql script
-- Patch de montée de version 1.0.4 vers 1.1
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

--------------------------------------------------------------------------------
-- 20140131: Gestion des groupes : ajout d'une contrainte unique evitant les
-- doublons de nom
--------------------------------------------------------------------------------

alter table groups add constraint groups_name_unique UNIQUE (name);


ALTER TABLE soustypes DROP CONSTRAINT soustypes_compteur_id_fkey;
ALTER TABLE soustypes ADD CONSTRAINT soustypes_compteur_id_fkey FOREIGN KEY (compteur_id) REFERENCES compteurs(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE compteurs DROP CONSTRAINT compteurs_sequence_id_fkey;
ALTER TABLE compteurs ADD CONSTRAINT compteurs_sequence_id_fkey FOREIGN KEY (sequence_id) REFERENCES sequences(id) ON UPDATE CASCADE ON DELETE CASCADE;



-- *****************************************************************************
COMMIT;
-- *****************************************************************************
