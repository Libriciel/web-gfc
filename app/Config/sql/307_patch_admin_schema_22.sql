-- 307_patch_admin_schema_22.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- /!\ CE PATCH DOIT ÊTRE BASÉ SUR LA BASE DE DONNÉES GÉNÉRALE CONTENANT TOUTES LES COLLECTIVITÉS  /!\ ---

-- *****************************************************************************
BEGIN;
-- *****************************************************************************


DROP TABLE IF EXISTS configurations CASCADE;
CREATE TABLE configurations(
    id SERIAL NOT NULL PRIMARY KEY,
    config TEXT NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE configurations IS 'Table permettant de paramétrer la page de connexion de web-GFC';



COMMIT;
