-- 316_patch_3.1.13.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

BEGIN;

INSERT INTO templatenotifications VALUES (12, 'bloque_parapheur', 'Flux non traité présent dans le i-Parapheur', 'Bonjour #PRENOM# #NOM#,

Un flux est présent dans le i-Parapheur depuis + d''1 semaine.

Objet : #OBJET_FLUX#
Référence : #REFERENCE_FLUX#
Commentaire: #COMMENTAIRE_FLUX#

Le flux peut être consulté à cette adresse : #ADRESSE_A_MODIFIER#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );

SELECT add_missing_table_field ('public', 'recherches', 'coriginefluxid', 'INTEGER');
SELECT add_missing_table_field ('public', 'recherches', 'cpriorite', 'VARCHAR(10)');
SELECT add_missing_table_field ('public', 'recherches', 'cetat', 'VARCHAR(10)');
SELECT add_missing_table_field ('public', 'recherches', 'cdirection', 'VARCHAR(10)');
SELECT add_missing_table_field ('public', 'recherches', 'creponse', 'VARCHAR(10)');
SELECT add_missing_table_field ('public', 'recherches', 'organismename', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'organismeadresse', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'organismedepartement', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'organismecommune', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'organismenomvoie', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'contactadresse', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'contactdept', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'contactcommune', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'contactnomvoie', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'contacttitreid', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'cocrdata', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'bancontenudesktopid', 'INTEGER');
SELECT add_missing_table_field ('public', 'recherches', 'bancontenuuserid', 'INTEGER');

COMMIT;
