-- 207_patch_1.2-commentaires.sql script
-- Patch de montée de version 1.0.4 vers 1.1
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

--------------------------------------------------------------------------------
-- 20140116: Gestion des commentaires. Un commentaire est lié à un flux et au
-- bureau de la personne qui l'a saisi, et est public par défaut.
-- Si il est privé, il peut-être lié à un ou plusieurs bureaux qui y auront
-- exclusivement accès.
--------------------------------------------------------------------------------
DROP TABLE  IF  EXISTS comments CASCADE;
create table comments(
	id serial not null primary key,
	slug character varying(255),
	content bytea,
	private boolean default false,
	owner_id integer not null,
	target_id integer not null,
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
DROP TABLE  IF  EXISTS comments_readers CASCADE;
create table comments_readers(
	id serial not null primary key,
	reader_id integer not null,
	comment_id integer not null references comments(id) on update cascade on delete cascade,
	created timestamp without time zone not null default now(),
	modified timestamp without time zone not null default now()
);
-- *****************************************************************************
COMMIT;
-- *****************************************************************************
