-- 222_patch_1.4.01.sql script
-- Patch permettant la mise en place des référentiels FANTOIR
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************
UPDATE bansadresses SET nom_voie=initcap(lower(nom_voie));
UPDATE bansadresses SET name=initcap(lower(name));



/* Table pour stocker les valeurs de emails à scruter pour la génération des flux */
DROP TABLE IF EXISTS scanemails CASCADE; -- du schéma de la base principale gérant les collectivités
CREATE TABLE scanemails(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    mail VARCHAR(255),
    hostname VARCHAR(255) NOT NULL,
    port VARCHAR(24) DEFAULT '143',
    password VARCHAR(255) NOT NULL,
    desktop_id integer references desktops(id) on update cascade on delete set null,
    type_id  integer references types(id) on update cascade on delete set null,
    soustype_id  integer references soustypes(id) on update cascade on delete set null,
    created timestamp without time zone not null default now(),
    modified timestamp without time zone not null default now()
);
COMMENT ON TABLE scanemails IS 'Table permettant de stocker les emails et destinataires pour chaque collectivité';

DROP INDEX IF EXISTS scanemails_desktop_id_idx;
CREATE INDEX scanemails_desktop_id_idx ON scanemails( desktop_id );

DROP INDEX IF EXISTS scanemails_type_id_idx;
CREATE INDEX scanemails_type_id_idx ON scanemails( type_id );

DROP INDEX IF EXISTS scanemails_soustype_id_idx;
CREATE INDEX scanemails_soustype_id_idx ON scanemails( soustype_id );

DROP INDEX IF EXISTS scanemails_name_desktop_id_idx;
CREATE UNIQUE INDEX scanemails_name_desktop_id_idx ON scanemails(name,desktop_id);

UPDATE rights SET set_affaire=true where aro_id='2';
UPDATE rights SET get_affaire=true where aro_id='2';

ALTER TABLE wkf_traitements DROP CONSTRAINT wkf_traitements_circuit_id_fkey;
ALTER TABLE wkf_traitements ADD CONSTRAINT wkf_traitements_circuit_id_fkey FOREIGN KEY (circuit_id) REFERENCES wkf_circuits(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE wkf_compositions DROP CONSTRAINT wkf_compositions_etape_id_fkey;
ALTER TABLE wkf_compositions ADD CONSTRAINT wkf_compositions_etape_id_fkey FOREIGN KEY (etape_id) REFERENCES wkf_etapes(id) ON UPDATE CASCADE ON DELETE CASCADE;


COMMIT;
