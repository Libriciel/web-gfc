-- 230_patch_1.8.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************
-- Ajout d'une table permettant de paramétrer des opérations (propre à la SAERP)
DROP TABLE IF EXISTS operations CASCADE;
CREATE TABLE operations(
    id SERIAL NOT NULL PRIMARY KEY,
    type_id integer references types(id) on update cascade on delete cascade,
    name VARCHAR(4) NOT NULL,
    nomoperation VARCHAR(255) NOT NULL,
    ville VARCHAR(255) NOT NULL,
    numvoie VARCHAR(20),
    nomvoie VARCHAR(255),
    compl VARCHAR(255),
    codepostal VARCHAR(5),
    commune VARCHAR(255),
    numconvention VARCHAR(255),
    nature VARCHAR(255),
    lieu VARCHAR(255),
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE operations IS 'Table permettant de définir les opérations (SAERP)';

DROP INDEX IF EXISTS operations_type_id_idx;
CREATE INDEX operations_type_id_idx ON operations( type_id );

-- Ajout d'une table permettant de paramétrer les intitulés d'agents (RO, AO, ...) (propre à la SAERP)
DROP TABLE IF EXISTS intitulesagents CASCADE;
CREATE TABLE intitulesagents(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(4) NOT NULL,
    description VARCHAR(255),
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE intitulesagents IS 'Table permettant de définir les intitulés d''agents (SAERP)';

-- -- Ajout d'une table permettant de stocker les agents ne lien avec des opérations (propre à la SAERP)
-- DROP TABLE IF EXISTS desktopsmanagers_operations CASCADE;
-- CREATE TABLE desktopsmanagers_operations(
--     id SERIAL NOT NULL PRIMARY KEY,
--     desktopmanager_id integer not null references desktopsmanagers(id) on update cascade on delete cascade,
--     operation_id integer not null references operations(id) on update cascade on delete cascade,
--     ordre VARCHAR(10) NOT NULL,
--     intituleagent_id integer not null references intitulesagents(id) on update cascade on delete cascade,
--     active BOOLEAN NOT NULL DEFAULT 'true',
--     created timestamp without time zone DEFAULT now() NOT NULL,
--     modified timestamp without time zone DEFAULT now() NOT NULL
-- );
-- COMMENT ON TABLE desktopsmanagers_operations IS 'Table de liaison entre les bureaux et les opérations (SAERP)';
--
-- DROP INDEX IF EXISTS desktopsmanagers_operations_desktopmanager_id_idx;
-- CREATE INDEX desktopsmanagers_operations_desktopmanager_id_idx ON desktopsmanagers_operations( desktopmanager_id );
--
-- DROP INDEX IF EXISTS desktopsmanagers_operations_intituleagent_id_idx;
-- CREATE INDEX desktopsmanagers_operations_intituleagent_id_idx ON desktopsmanagers_operations( intituleagent_id );
--
-- DROP INDEX IF EXISTS desktopsmanagers_operations_operation_id_idx;
-- CREATE INDEX desktopsmanagers_operations_operation_id_idx ON desktopsmanagers_operations( operation_id );
--
-- DROP INDEX IF EXISTS desktopsmanagers_operations_desktopmanager_id_operation_id_idx;
-- CREATE UNIQUE INDEX desktopsmanagers_operations_desktopmanager_id_operation_id_idx ON desktopsmanagers_operations(desktopmanager_id,operation_id);
--
-- DROP INDEX IF EXISTS desktopsmanagers_operations_operation_id_ordre_idx;
-- CREATE UNIQUE INDEX desktopsmanagers_operations_operation_id_ordre_idx ON desktopsmanagers_operations(operation_id,ordre);


SELECT add_missing_table_field ('public', 'desktopsmanagers', 'intituleagent_id', 'INTEGER');
-- SELECT add_missing_constraint ( 'public', 'desktopsmanagers', 'desktopsmanagers_intituleagent_id_fkey', 'intitulesagents', 'intituleagent_id', false );

UPDATE aros_acos SET (_create, _read, _delete, _update)=(1,1,1,1) WHERE aro_id IN (SELECT id FROM aros WHERE id in (3,7)) AND aco_id IN (SELECT id FROM acos WHERE alias ILIKE '%send%');
UPDATE aros_acos SET (_create, _read, _delete, _update)=(1,1,1,1) WHERE aro_id IN (SELECT id FROM aros WHERE parent_id in (3,7)) AND aco_id IN (SELECT id FROM acos WHERE alias ILIKE '%sendcpy%');


-- Ajout d'une table permettant de paramétrer les intitulés d'agents (RO, AO, ...) (propre à la SAERP)
DROP TABLE IF EXISTS contractants CASCADE;
CREATE TABLE contractants(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE contractants IS 'Table permettant de définir les types de contractant pour les marchés (SAERP)';

-- Ajout d'une table permettant de paramétrer les intitulés d'agents (RO, AO, ...) (propre à la SAERP)
DROP TABLE IF EXISTS uniterifs CASCADE;
CREATE TABLE uniterifs(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE uniterifs IS 'Table permettant de définir les Unités RIF (SAERP)';

-- Ajout d'une table permettant de stocker les marchés (propre à la SAERP)
DROP TABLE IF EXISTS marches CASCADE;
CREATE TABLE marches(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    numero VARCHAR(10) NOT NULL,
    objet VARCHAR(255),
    operation_id INTEGER NOT NULL REFERENCES operations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    contractant_id INTEGER NOT NULL REFERENCES contractants(id) ON UPDATE CASCADE ON DELETE CASCADE,
    uniterif_id INTEGER NOT NULL REFERENCES uniterifs(id) ON UPDATE CASCADE ON DELETE CASCADE,
    chrono VARCHAR(50),
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE marches IS 'Table permettant de stocker les informations en lien avec les marchés (SAERP)';

DROP INDEX IF EXISTS marches_operation_id_idx;
CREATE INDEX marches_operation_id_idx ON marches( operation_id );
DROP INDEX IF EXISTS marches_contractant_id_idx;
CREATE INDEX marches_contractant_id_idx ON marches( contractant_id );
DROP INDEX IF EXISTS marches_uniterif_id_idx;
CREATE INDEX marches_uniterif_id_idx ON marches( uniterif_id );

DROP INDEX IF EXISTS marches_name_idx;
CREATE UNIQUE INDEX marches_name_idx ON marches (name);

-- Ajout d'une table permettant de stocker les marchés (propre à la SAERP)
DROP TABLE IF EXISTS ordresservices CASCADE;
CREATE TABLE ordresservices(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    marche_id INTEGER NOT NULL REFERENCES marches(id) ON UPDATE CASCADE ON DELETE CASCADE,
    courrier_id INTEGER REFERENCES courriers(id) ON UPDATE CASCADE ON DELETE CASCADE,
    numero VARCHAR(10) NOT NULL,
    objet VARCHAR(255),
    montant VARCHAR(10),
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE ordresservices IS 'Table permettant de stocker les ordres de service (SAERP)';

DROP INDEX IF EXISTS ordresservices_marche_id_idx;
CREATE INDEX ordresservices_marche_id_idx ON ordresservices( marche_id );

DROP INDEX IF EXISTS ordresservices_marche_id_numero_idx;
CREATE UNIQUE INDEX ordresservices_marche_id_numero_idx ON ordresservices( marche_id, numero );


-- Ajout d'un lien entre la table courriers et la table marches
SELECT add_missing_table_field ('public', 'courriers', 'marche_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'courriers', 'courriers_marche_id_fkey', 'marches', 'marche_id', false );

SELECT add_missing_table_field ('public', 'courriers', 'typedocument', 'VARCHAR(50)');

UPDATE aros_acos SET (_create, _read, _delete, _update)=(1,1,1,1) WHERE aro_id IN (SELECT id FROM aros WHERE parent_id=3 and parent_id=7) AND aco_id IN (SELECT id FROM acos WHERE alias ILIKE '%send%');
-- ajout des drits sur la focntion detachelot
-- INSERT INTO aros_acos (aro_id, aco_id, _create, _read, _update, _delete) (
-- SELECT id, (SELECT id FROM acos WHERE alias ILIKE '%detachelot%'), 1, 1, 1, 1 FROM aros where id > 7 );


-- Ajout d'une table permettant de stocker les gabarits de documents (propre à la SAERP)
DROP TABLE IF EXISTS gabaritsdocuments CASCADE;
CREATE TABLE gabaritsdocuments(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    content BYTEA,
    format VARCHAR(50),
    mime VARCHAR(255),
    ext VARCHAR(255),
    size VARCHAR(255),
    preview BYTEA,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE gabaritsdocuments IS 'Table permettant de stocker les gabarits de documents (SAERP)';






-- Ajout d'une table permettant de stocker les agents ne lien avec des opérations (propre à la SAERP)
DROP TABLE IF EXISTS sousoperations CASCADE;
CREATE TABLE sousoperations(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    soustype_id integer references soustypes(id) on update cascade on delete cascade,
    operation_id integer not null references operations(id) on update cascade on delete cascade,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE sousoperations IS 'Table de liaison entre les bureaux et les opérations (SAERP)';
DROP INDEX IF EXISTS sousoperations_soustype_id_idx;
CREATE INDEX sousoperations_soustype_id_idx ON sousoperations( soustype_id );
DROP INDEX IF EXISTS sousoperations_operation_id_idx;
CREATE INDEX sousoperations_operation_id_idx ON sousoperations( operation_id );



-- Ajout d'une table permettant de stocker les agents ne lien avec des opérations (propre à la SAERP)
DROP TABLE IF EXISTS desktopsmanagers_sousoperations CASCADE;
CREATE TABLE desktopsmanagers_sousoperations(
    id SERIAL NOT NULL PRIMARY KEY,
    desktopmanager_id integer references desktopsmanagers(id) on update cascade on delete cascade,
    sousoperation_id integer not null references sousoperations(id) on update cascade on delete cascade,
    ordre VARCHAR(10) NOT NULL,
    intituleagent_id integer not null references intitulesagents(id) on update cascade on delete cascade,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE desktopsmanagers_sousoperations IS 'Table de liaison entre les bureaux et les opérations (SAERP)';

DROP INDEX IF EXISTS desktopsmanagers_sousoperations_desktopmanager_id_idx;
CREATE INDEX desktopsmanagers_sousoperations_desktopmanager_id_idx ON desktopsmanagers_sousoperations( desktopmanager_id );

DROP INDEX IF EXISTS desktopsmanagers_sousoperations_intituleagent_id_idx;
CREATE INDEX desktopsmanagers_sousoperations_intituleagent_id_idx ON desktopsmanagers_sousoperations( intituleagent_id );

DROP INDEX IF EXISTS desktopsmanagers_sousoperations_sousoperation_id_idx;
CREATE INDEX desktopsmanagers_sousoperations_sousoperation_id_idx ON desktopsmanagers_sousoperations( sousoperation_id );

DROP INDEX IF EXISTS desktopsmanagers_sousoperations_desktopmanager_id_sousoperation_id_idx;
CREATE UNIQUE INDEX desktopsmanagers_sousoperations_desktopmanager_id_sousoperation_id_idx ON desktopsmanagers_sousoperations(desktopmanager_id,sousoperation_id);

DROP INDEX IF EXISTS desktopsmanagers_sousoperations_sousoperation_id_ordre_idx;
CREATE UNIQUE INDEX desktopsmanagers_sousoperations_sousoperation_id_ordre_idx ON desktopsmanagers_sousoperations(sousoperation_id,ordre);

SELECT add_missing_table_field ('public', 'ordresservices', 'courrier_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'ordresservices', 'ordresservices_courrier_id_fkey', 'courriers', 'courrier_id', false );



DROP TABLE IF EXISTS intituleagentbyoperations CASCADE;
CREATE TABLE intituleagentbyoperations(
    id SERIAL NOT NULL PRIMARY KEY,
    intituleagent_id integer not null references intitulesagents(id) on update cascade on delete cascade,
    operation_id integer not null references operations(id) on update cascade on delete cascade,
    desktopmanager_id integer not null references desktopsmanagers(id) on update cascade on delete cascade,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE intituleagentbyoperations IS 'Table permettant de définir les bureaux pour un rôle donné dans une OP (SAERP)';

DROP INDEX IF EXISTS intituleagentbyoperations_desktopmanager_id_idx;
CREATE INDEX intituleagentbyoperations_desktopmanager_id_idx ON intituleagentbyoperations( desktopmanager_id );

DROP INDEX IF EXISTS intituleagentbyoperations_intituleagent_id_idx;
CREATE INDEX intituleagentbyoperations_intituleagent_id_idx ON intituleagentbyoperations( intituleagent_id );

DROP INDEX IF EXISTS intituleagentbyoperations_operation_id_idx;
CREATE INDEX intituleagentbyoperations_operation_id_idx ON intituleagentbyoperations( operation_id );

DROP INDEX IF EXISTS intituleagentbyoperations_operation_id_intituleagent_id_idx;
CREATE UNIQUE INDEX intituleagentbyoperations_operation_id_intituleagent_id_idx ON intituleagentbyoperations( intituleagent_id, operation_id );


alter table desktopsmanagers_sousoperations alter column  desktopmanager_id drop not null;


UPDATE aros_acos SET (_create, _read, _delete, _update)=(1,1,1,1) WHERE aro_id IN (SELECT id FROM aros WHERE parent_id=7) AND aco_id IN (SELECT id FROM acos WHERE alias ILIKE '%merge%');

SELECT add_missing_table_field ('public', 'desktopsmanagers_sousoperations', 'typeetape', 'INTEGER');

drop index desktopsmanagers_sousoperations_sousoperation_id_ordre_idx;

DROP TABLE IF EXISTS consultations CASCADE;
CREATE TABLE consultations(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    numero VARCHAR(10) NOT NULL,
    objet VARCHAR(255),
    datelimite DATE NOT NULL,
    heurelimite TIME NOT NULL,
    operation_id INTEGER NOT NULL REFERENCES operations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE consultations IS 'Table permettant de stocker les informations en lien avec les consultations (SAERP)';

DROP INDEX IF EXISTS consultations_operation_id_idx;
CREATE INDEX consultations_operation_id_idx ON consultations( operation_id );

DROP INDEX IF EXISTS consultations_name_idx;
CREATE UNIQUE INDEX consultations_name_idx ON consultations (name);




-- Ajout d'une table permettant de stocker les marchés (propre à la SAERP)
DROP TABLE IF EXISTS plisconsultatifs CASCADE;
CREATE TABLE plisconsultatifs(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    consultation_id INTEGER NOT NULL REFERENCES consultations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    typologieflux_id INTEGER REFERENCES typologiesflux(id) ON UPDATE CASCADE ON DELETE CASCADE,
    courrier_id INTEGER REFERENCES courriers(id) ON UPDATE CASCADE ON DELETE CASCADE,
    numero VARCHAR(10) NOT NULL,
    date DATE NOT NULL,
    heure TIME NOT NULL,
    objet VARCHAR(255),
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE plisconsultatifs IS 'Table permettant de stocker les plis consultatifs (SAERP)';

DROP INDEX IF EXISTS plisconsultatifs_consultation_id_idx;
CREATE INDEX plisconsultatifs_consultation_id_idx ON plisconsultatifs( consultation_id );

DROP INDEX IF EXISTS plisconsultatifs_typologieflux_id_idx;
CREATE INDEX plisconsultatifs_typologieflux_id_idx ON plisconsultatifs( typologieflux_id );

SELECT add_missing_table_field ('public', 'marches', 'typecontractant', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'marches', 'datenotification', 'DATE');
SELECT add_missing_table_field ('public', 'marches', 'premiermois', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'marches', 'uniterif', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'marches', 'titulaire', 'VARCHAR(255)');

SELECT add_missing_table_field ('public', 'plisconsultatifs', 'lot', 'VARCHAR(10)');

SELECT add_missing_table_field ('public', 'courriers', 'consultation_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'courriers', 'courriers_consultation_id_fkey', 'consultations', 'consultation_id', false );
SELECT add_missing_table_field ('public', 'courriers', 'marcheconsult', 'VARCHAR(20)');

SELECT add_missing_table_field ('public', 'ordresservices', 'organisme_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'ordresservices', 'ordresservices_organisme_id_fkey', 'organismes', 'organisme_id', false );


/** BASE CONTACTS SAERP **/
DROP TABLE IF EXISTS activites CASCADE;
CREATE TABLE activites(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NULL,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE activites IS 'Table permettant de stocker les activités liées aux contacts (SAERP)';

DROP TABLE IF EXISTS sousactivites CASCADE;
CREATE TABLE sousactivites(
    id SERIAL NOT NULL PRIMARY KEY,
    activite_id  INTEGER NOT NULL REFERENCES activites(id) ON UPDATE CASCADE ON DELETE CASCADE,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NULL,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE sousactivites IS 'Table permettant de stocker les sous-activités liées aux contacts (SAERP)';

DROP TABLE IF EXISTS events CASCADE;
CREATE TABLE events(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NULL,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE events IS 'Table permettant de stocker les événements liés aux contacts (SAERP)';


DROP TABLE IF EXISTS contacts_events CASCADE;
CREATE TABLE contacts_events(
    id SERIAL NOT NULL PRIMARY KEY,
    contact_id INTEGER NOT NULL REFERENCES contacts(id) ON UPDATE CASCADE ON DELETE CASCADE,
    event_id INTEGER NOT NULL REFERENCES events(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE contacts_events IS 'Table de liaison entre les contacts et les événements(SAERP)';

DROP TABLE IF EXISTS contacts_operations CASCADE;
CREATE TABLE contacts_operations(
    id SERIAL NOT NULL PRIMARY KEY,
    contact_id INTEGER NOT NULL REFERENCES contacts(id) ON UPDATE CASCADE ON DELETE CASCADE,
    operation_id INTEGER NOT NULL REFERENCES operations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE contacts_operations IS 'Table de liaison entre les contacts et les opérations (SAERP)';

DROP TABLE IF EXISTS organismes_operations CASCADE;
CREATE TABLE organismes_operations(
    id SERIAL NOT NULL PRIMARY KEY,
    organisme_id INTEGER NOT NULL REFERENCES organismes(id) ON UPDATE CASCADE ON DELETE CASCADE,
    operation_id INTEGER NOT NULL REFERENCES operations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE organismes_operations IS 'Table de liaison entre les organismes et les opérations (SAERP)';

SELECT add_missing_table_field ('public', 'organismes', 'antenne', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'organismes', 'siret', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'contacts', 'lignedirecte', 'VARCHAR(20)');
SELECT add_missing_table_field ('public', 'contacts', 'observations', 'TEXT');


DROP TABLE IF EXISTS activites_organismes CASCADE;
CREATE TABLE activites_organismes(
    id SERIAL NOT NULL PRIMARY KEY,
    activite_id INTEGER NOT NULL REFERENCES activites(id) ON UPDATE CASCADE ON DELETE CASCADE,
    organisme_id INTEGER NOT NULL REFERENCES organismes(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE activites_organismes IS 'Table de liaison entre les activités et les organismes (SAERP)';


SELECT add_missing_table_field ('public', 'addressbooks', 'private', 'BOOLEAN');
ALTER TABLE addressbooks ALTER COLUMN private SET DEFAULT 'false';
UPDATE addressbooks SET private = 'false';

SELECT add_missing_table_field ('public', 'users', 'addressbook_private', 'BOOLEAN');
ALTER TABLE users ALTER COLUMN addressbook_private SET DEFAULT 'false';
UPDATE users SET addressbook_private = 'false';



DROP TABLE IF EXISTS fonctions CASCADE;
CREATE TABLE fonctions(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NULL,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE fonctions IS 'Table permettant de stocker les fonctions liées aux contacts (SAERP)';

SELECT add_missing_table_field ('public', 'contacts', 'fonction_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'contacts', 'contacts_fonction_id_fkey', 'fonctions', 'fonction_id', false );

SELECT add_missing_table_field ('public', 'metadonnees', 'champfusion', 'VARCHAR(255)');

ALTER TABLE contacts DROP CONSTRAINT contacts_organisme_id_fkey, ADD CONSTRAINT contacts_organismes_fkey FOREIGN KEY (organisme_id) REFERENCES organismes(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE organismes DROP CONSTRAINT organismes_addressbook_id_fkey, ADD CONSTRAINT organismes_addressbook_id_fkey FOREIGN KEY (addressbook_id) REFERENCES addressbooks(id) ON UPDATE CASCADE ON DELETE CASCADE;

SELECT add_missing_table_field ('public', 'plisconsultatifs', 'courrier_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'plisconsultatifs', 'plisconsultatifs_courrier_id_fkey', 'courriers', 'courrier_id', false );

alter table plisconsultatifs alter column  name drop not null;

UPDATE aros_acos SET (_create, _read, _delete, _update)=(1,1,1,1) WHERE aro_id IN (SELECT id FROM aros WHERE parent_id=7) AND aco_id IN (SELECT id FROM acos WHERE alias ILIKE 'dispenv');
UPDATE aros_acos SET (_create, _read, _delete, _update)=(1,1,1,1) WHERE aro_id IN (SELECT id FROM aros) AND aco_id IN (select id from acos where alias = 'setInfos' and parent_id = (select parent_id from acos where alias ='getMeta'));
COMMIT;
