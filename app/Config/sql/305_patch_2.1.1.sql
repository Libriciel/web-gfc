-- 304_patch_2.1.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

BEGIN;

SELECT add_missing_table_field( 'public', 'contacts', 'titre', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'bansadresses', 'canton', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'contacts', 'canton', 'VARCHAR(255)' );
SELECT add_missing_table_field( 'public', 'organismes', 'canton', 'VARCHAR(255)' );

ALTER TABLE documents ALTER COLUMN courrier_id DROP NOT NULL;
SELECT add_missing_table_field( 'public', 'documents', 'ishistorique', 'BOOLEAN' );

DROP TABLE IF EXISTS titres;
CREATE TABLE titres(
    id SERIAL NOT NULL PRIMARY KEY,
    name character varying(255),
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE titres IS 'Table des titres de contacts';

DROP INDEX IF EXISTS titres_name_idx;
CREATE UNIQUE INDEX titres_name_idx ON titres( name );

SELECT add_missing_table_field( 'public', 'contacts', 'titre_id', 'INTEGER' );
SELECT add_missing_constraint ( 'public', 'contacts', 'contacts_titre_id_fkey', 'contacts', 'titre_id', false );

COMMIT;
