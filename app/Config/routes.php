<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'users', 'action' => 'login'));


/**
 * Router pour l'API REST
 *
 */
/**
 * check
 */
Router::connect(
	'/api/v1/version',
	['controller' => 'Apiv2', 'action' => 'version', "[method]" => "GET"]
);
Router::connect(
	'/api/v1/check',
	['controller' => 'Apiv2', 'action' => 'check', "[method]" => "POST"]
);


/**
 * API REST du flux
 */
/**
 * Création d'un flux
 */
Router::connect(
	'/api/v1/courriers',
	['controller' => 'CourrierApi', 'action' => 'add', "[method]" => "POST"]
);

/**
 * Modification d'un flux
 */
Router::connect(
	'/api/v1/courriers/:courrierId',
	['controller' => 'CourrierApi', 'action' => 'update', "[method]" => "PUT"],
	['pass' => ['courrierId']]
);
/**
 * Statut d'un flux
 */
Router::connect(
	'/api/v1/courriers/status/:courrierId',
	['controller' => 'CourrierApi', 'action' => 'status', "[method]" => "GET"],
	['pass' => ['courrierId']]
);

/**
 * Suppression d'un flux
 */
Router::connect(
	'/api/v1/courriers/:courrierId',
	['controller' => 'CourrierApi', 'action' => 'delete', "[method]" => "DELETE"],
	['pass' => ['courrierId']]
);

/**
 * Information d'un flux
 */
Router::connect(
	'/api/v1/courriers/:courrierId',
	['controller' => 'CourrierApi', 'action' => 'find', "[method]" => "GET"],
	['pass' => ['courrierId']]
);
/**
 * Information de toutes les flux
 */
Router::connect(
	'/api/v1/courriers',
	['controller' => 'CourrierApi', 'action' => 'list', "[method]" => "GET"]
);


/**
 * API REST de la collectivité
 */
/**
 * Création d'une collectivité
 */
Router::connect(
	'/api/v1/collectivites',
	['controller' => 'CollectiviteApi', 'action' => 'add', "[method]" => "POST"]
);

/**
 * Information d'une collectivité
 */
Router::connect(
	'/api/v1/collectivites/:collectiviteId',
	['controller' => 'CollectiviteApi', 'action' => 'find', "[method]" => "GET"],
	['pass' => ['collectiviteId']]
);
/**
 * Information de toutes les collectivités
 */
Router::connect(
	'/api/v1/collectivites',
	['controller' => 'CollectiviteApi', 'action' => 'list', "[method]" => "GET"]
);
/**
 * Modification d'une collectivité
 */
Router::connect(
	'/api/v1/collectivites/:collectiviteId',
	['controller' => 'CollectiviteApi', 'action' => 'update', "[method]" => "PUT"],
	['pass' => ['collectiviteId']]
);


/**
 * API REST des types/sous-types
 */
/**
 * Création d'un type
 */
Router::connect(
	'/api/v1/types',
	['controller' => 'TypeApi', 'action' => 'add', "[method]" => "POST"]
);

/**
 * Suppression d'un type
 */
Router::connect(
	'/api/v1/types/:typeId',
	['controller' => 'TypeApi', 'action' => 'delete', "[method]" => "DELETE"],
	['pass' => ['typeId']]
);

/**
 * Information d'un type
 */
Router::connect(
	'/api/v1/types/:typeId',
	['controller' => 'TypeApi', 'action' => 'find', "[method]" => "GET"],
	['pass' => ['typeId']]
);
/**
 * Information de tous les types
 */
Router::connect(
	'/api/v1/types',
	['controller' => 'TypeApi', 'action' => 'list', "[method]" => "GET"]
);
/**
 * Modification d'un type
 */
Router::connect(
	'/api/v1/types/:typeId',
	['controller' => 'TypeApi', 'action' => 'update', "[method]" => "PUT"],
	['pass' => ['typeId']]
);

/**
 * API REST des desktops
 */
/**
 * Création d'un desktop (profil)
 */
Router::connect(
	'/api/v1/desktops/:userId',
	['controller' => 'DesktopApi', 'action' => 'add', "[method]" => "POST"],
	['pass' => ['userId']]
);

/**
 * Suppression d'un desktop (profil)
 */
Router::connect(
	'/api/v1/desktops/:desktopId',
	['controller' => 'DesktopApi', 'action' => 'delete', "[method]" => "DELETE"],
	['pass' => ['desktopId']]
);

/**
 * Information d'un desktop (profil)
 */
Router::connect(
	'/api/v1/desktops/:desktopId',
	['controller' => 'DesktopApi', 'action' => 'find', "[method]" => "GET"],
	['pass' => ['desktopId']]
);
/**
 * Information de tous les desktops (profils)
 */
Router::connect(
	'/api/v1/desktops',
	['controller' => 'DesktopApi', 'action' => 'list', "[method]" => "GET"]
);
/**
 * Modification d'un desktop (profil)
 */
Router::connect(
	'/api/v1/desktops/:desktopId',
	['controller' => 'DesktopApi', 'action' => 'update', "[method]" => "PUT"],
	['pass' => ['desktopId']]
);


/**
 * API REST des services
 */
/**
 * Création d'un service
 */
Router::connect(
	'/api/v1/services',
	['controller' => 'ServiceApi', 'action' => 'add', "[method]" => "POST"]
);

/**
 * Suppression d'un service
 */
Router::connect(
	'/api/v1/services/:serviceId',
	['controller' => 'ServiceApi', 'action' => 'delete', "[method]" => "DELETE"],
	['pass' => ['serviceId']]
);

/**
 * Information d'un service
 */
Router::connect(
	'/api/v1/services/:serviceId',
	['controller' => 'ServiceApi', 'action' => 'find', "[method]" => "GET"],
	['pass' => ['serviceId']]
);
/**
 * Information de tous les services
 */
Router::connect(
	'/api/v1/services',
	['controller' => 'ServiceApi', 'action' => 'list', "[method]" => "GET"]
);
/**
 * Modification d'un service
 */
Router::connect(
	'/api/v1/services/:serviceId',
	['controller' => 'ServiceApi', 'action' => 'update', "[method]" => "PUT"],
	['pass' => ['serviceId']]
);

/**
 * API REST des users
 */
/**
 * Création d'un user
 */
Router::connect(
	'/api/v1/users',
	['controller' => 'UserApi', 'action' => 'add', "[method]" => "POST"]
);
/**
 * Modification d'un user
 */
Router::connect(
	'/api/v1/users/:userId',
	['controller' => 'UserApi', 'action' => 'update', "[method]" => "PUT"],
	['pass' => ['userId']]
);
/**
 * Suppression d'un user
 */
Router::connect(
	'/api/v1/users/:userId',
	['controller' => 'UserApi', 'action' => 'delete', "[method]" => "DELETE"],
	['pass' => ['userId']]
);

/**
 * Information d'un user
 */
Router::connect(
	'/api/v1/users/:userId',
	['controller' => 'UserApi', 'action' => 'find', "[method]" => "GET"],
	['pass' => ['userId']]
);
/**
 * Information de tous les users
 */
Router::connect(
	'/api/v1/users',
	['controller' => 'UserApi', 'action' => 'list', "[method]" => "GET"]
);

/**
 * API REST des contacts
 */
/**
 * Création d'un contact
 */
Router::connect(
	'/api/v1/contacts',
	['controller' => 'ContactApi', 'action' => 'add', "[method]" => "POST"]
);

/**
 * Suppression d'un contact
 */
Router::connect(
	'/api/v1/contacts/:contactId',
	['controller' => 'ContactApi', 'action' => 'delete', "[method]" => "DELETE"],
	['pass' => ['contactId']]
);

/**
 * Information d'un contact
 */
Router::connect(
	'/api/v1/contacts/:contactId',
	['controller' => 'ContactApi', 'action' => 'find', "[method]" => "GET"],
	['pass' => ['contactId']]
);
/**
 * Information de tous les contacts
 */
Router::connect(
	'/api/v1/contacts',
	['controller' => 'ContactApi', 'action' => 'list', "[method]" => "GET"]
);

/**
 * Modification d'un contact
 */
Router::connect(
	'/api/v1/contacts/:contactId',
	['controller' => 'ContactApi', 'action' => 'update', "[method]" => "PUT"],
	['pass' => ['contactId']]
);


/**
 * API REST des services
 */
/**
 * Test d'impression ODT
 */
Router::connect(
	'/api/v1/checks',
	['controller' => 'CheckApi', 'action' => 'testgedooo', "[method]" => "POST"]
);



/**
 * API REST des soustypes/sous-soustypes
 */
/**
 * Création d'un soustype
 */
Router::connect(
	'/api/v1/soustypes',
	['controller' => 'SoustypeApi', 'action' => 'add', "[method]" => "POST"]
);

/**
 * Suppression d'un soustype
 */
Router::connect(
	'/api/v1/soustypes/:soustypeId',
	['controller' => 'SoustypeApi', 'action' => 'delete', "[method]" => "DELETE"],
	['pass' => ['soustypeId']]
);

/**
 * Information d'un soustype
 */
Router::connect(
	'/api/v1/soustypes/:soustypeId',
	['controller' => 'SoustypeApi', 'action' => 'find', "[method]" => "GET"],
	['pass' => ['soustypeId']]
);
/**
 * Information de tous les soustypes
 */
Router::connect(
	'/api/v1/soustypes',
	['controller' => 'SoustypeApi', 'action' => 'list', "[method]" => "GET"]
);
/**
 * Modification d'un soustype
 */
Router::connect(
	'/api/v1/soustypes/:soustypeId',
	['controller' => 'SoustypeApi', 'action' => 'update', "[method]" => "PUT"],
	['pass' => ['soustypeId']]
);


/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));


/**
* Ajout de la route pour la réponse REST à Allo.
*/
Router::connect( '/api/rest/allo/version', array( 'controller' => 'allos', 'action' => 'version' ) );

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
