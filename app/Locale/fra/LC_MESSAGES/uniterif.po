##
#
# Uniterif translation file
#
# web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
#
# PHP version 7
# @author Arnaud AUZOLAT
# @copyright Initié par ADULLACT - Développé par ADULLACT Projet
# @link http://adullact.org/
# @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
#

##############
# Uniterif
##############

#titre
msgid "Uniterif.name"
msgstr "Nom de Service RIF"

msgid "Uniterif.void"
msgstr "Aucun service défini"

msgid "Uniterif.description"
msgstr "Description du service"

msgid "Uniterif.placeholderNameAdd"
msgstr "Intitulé visible par les utilisateurs"

msgid "Uniterif.liste"
msgstr "Liste des services"

msgid "Uniterif.infos"
msgstr "Informations"

msgid "Uniterif.details"
msgstr "Détails"

msgid "Uniterif.active"
msgstr "Service actif ?"
