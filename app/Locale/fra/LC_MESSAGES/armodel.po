##
#
# Armodel translation file
#
# web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
#
# PHP version 7
# @author Stéphane Sampaio
# @copyright Initié par ADULLACT - Développé par ADULLACT Projet
# @link http://adullact.org/
# @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
#

msgid "Armodel.bttn.add"
msgstr "Ajouter un modèle"

msgid "Armodel.format"
msgstr "Type de modèle"

msgid "Armodel.ott"
msgstr "Modèle de document (odt, ott)"

msgid "Armodel.audio"
msgstr "Fichier audio"

msgid "Armodel.video"
msgstr "Fichier video"

msgid "Armodel.sms"
msgstr "Modèle de SMS"

msgid "Armodel.email"
msgstr "Modèle d'e-mail"

msgid "Armodel.name"
msgstr "Nom"

msgid "Armodel.file"
msgstr "Fichier"

msgid "Armodel.email_title"
msgstr "Titre"

msgid "Armodel.email_content"
msgstr "Contenu"

msgid "Armodel.sms_content"
msgstr "Contenu"


msgid "Bttn.generate.disabled.text"
msgstr "Veuillez choisir un modèle d'accusé de réception"

msgid "Bttn.generate.disabled"
msgstr "Aucun modèle choisi"


msgid "Armodel.warning.sms"
msgstr "La connexion au serveur d'envoi de sms n'est pas paramétrée."

msgid "Armodel.warning.email"
msgstr "La connexion au serveur d'envoi d'email n'est pas paramétrée."
