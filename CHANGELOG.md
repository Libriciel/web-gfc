# Change Log

Toutes les modifications apportées au projet seront documentées dans ce fichier.

Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/)
et adhère aux principes du [Semantic Versioning](http://semver.org/).

## [3.1.22] - 2024-07-01
### Modifications
- [OAUTH] Dans le cas de la scrutation de mails via Oauth2, les informations du mail (De, Répondre à, Date, Objet) n'étaient pas reprises dans le contenu du document généré
- [OAUTH] Ajout du mail entre parenthèses après les informations From, To et ReplyTo de la scrutaiton de mails
  [OAUTH] Si le contenu du mail n'est pas en HTML, on n'ajoute pas les informations From, ReplyTo et To au contenu du document affiché dans la visionneuse
- [OAUTH] Si le sujet du mail possède des accents on les enlève pour ne pas "casser" l'affichage

### Corrections
- L'ID de la métadonnée "Date de clôture" avait été oublié dans le modèle Bancontenu.php au moment de la clôture
- L'affichage des commentaires dans le flux d'origine provoque une erreur si aucun commentaire n'est disponible
- Le tri sur la colonne "Date de réception" des bannettes de l'agent ne fonctionnait pas correctement, le tri se faisait sur le jour et non pas sur l'ensemble de la date
- La recherche par nom d'organisme et nom de contact pose souci si le champ possède des quote ou simple quote (' ou ’)
- Dans le cas de la scrutation de mails en OAUTH2, les champs De, A, Objet, Date n'étaient pas repris dans le document généré
- Au moment de clore et répondre, il ne faut pas répercuter la date de clôture sinon cela pose soucis.

## [3.1.21] - 2024-05-06
### Ajouts
- Ajout d'un champ "Complément d'adresse 2" pour les contacts et les organismes
- Ajout d'un champ "cedex" pour les contacts et les organismes
- Ajout d'un critère de recherche "Fonction" pour le menu de recherche des carnets d'adresse, partie contact
- Ajout d'un critère de recherche "Activité" pour le menu de recherche des carnets d'adresse, partie organisme
- Ajout des colonnes Titre et Fonction sur la recherche de contact depuis le courrier

### Modifications
- En cas de refus, les flux positionnés en copie ne seront plus détachés
- Prise en compte des titres de contact dans le script d'intégration de ces derniers
- Le champ antenne pour organismes est désormais repris dans les modèles de réponse (organisme_antenne)
- Seuls les documents de mimetype pdf, odt ou donc peuvent être passés en document principal. De plus, seuls ces documents peuvent être consultés via la visionneuse
- Au moment de la clôture d'un flux, on stocke la date de clôture dans une nouvelle métadonnée "Date de clôture"

### Corrections
- A la remontée des entités de Pastell, on affiche toutes les entités même si l'entité est sélectionnée, afin de pouvoir changer le cas échéant.
- En cas de récupération de mails, si les mails sont en ASCII, l'encodage ne remontait pas les informations dans le PDF joint
- En cas de ré-auiguillage, certains flux restaient chez les agents partageant le même bureau que celui ré-aiguillant
- La fonction n'est pas enregistrée lors de l'insertion par script ContactShell


## [3.1.20] - 2024-02-29
### Modifications
- Lorsqu'on consulte les habilitations d'un utilisateur, on n'affiche plus les profils désactivés
- Dans le cas où le fichier n'est pas stocké en webdav, utilisation de la fonction $this->response->file pour le téléchargement des PJs
- Dans l'historique du flux, affichage de l'action permettant l'envoi pour copie
- Lors d'un envoi en copie, on stocke l'information dans le journal des événements

### Corrections
- Lorsque Pastell v4.x ne répond pas ou remonte un erreur, on récupère le message d'erreur et on ne bloque pas l'utilisateur lors de la connexion
- La suppression d'un contact depuis la carnet d'adresse n'était pas possible
- Si le iparapheur renvoie une erreur 500 (Erreur interne du serveur), l'accès à webgfc est bloqué
- Le traitement par lots entraînait la création d'entrée dans la table bancontenus autant de fois que de flux sélectionnés.
- Dans les métadonnées d'un flux, si la métadonnée est de type bouton radio, le champ required n'était pas pris en compte
- [Recherche] Si la variable Configure::write('Recherche.FluxCopie', false); est à false, et que l'on fait une recherche par n° de référence, la jointure sur bancontenus est manquante
- Dans l'historique du flux, si un envoi pour copie est fait sur un flux clos, l'information de son statut était modifiée indiquant que le flux était en cours
- Si le sujet du mail est trop long (> 220 caract.), il n'est pas possible d'afficher son contenu dans la visionneuse
- [Recherche] Lors de la recherche par n° de référence, si la variable Configure::write('Recherche.FluxCopie', false); est à false, les flux enfants/parents ne ressortent pas en même temps après une recherche
- [Recherche] L'affichage du bouton de suppression des recherches enregistrées était mal positionné
- En cas de modification d'une métadonnée, on perdait la notion d'obligatoire dans les sous-types associés

## [3.1.19] - 2023-11-30
### Ajouts
- [Civilité] Une variable est ajoutée permettant d'indiquer si on souhaite que la civilité soit en majuscule ou non
- Dans l'onglet reprenant les informations du flux d'origine ou refusé, les commentaires sont désormais repris.
- Ajout d'une variable (Configure::write('PdfConvert.In14Version', false);) permettant de ne pas forcer la conversion des PDFs en version 1.4

### Modifications
- Lorsqu'un mail est envoyé depuis un flux, il est possible d'afficher le contenu du mail via une variable à activer
- [Scrutation de mails] Le champ "Répertoire où le mail sera déplacé après traitement" n'est pas obligatoire
- [Scrutation de mails] Si le répertoire de déplacement des mails est défini, les mails sont désormais bien déplacés
- [Outils d'administrateur] La suppression de tous les flux est mise sous condition afin d'éviter les erreurs de manipulation
- Les liens présents dans les mails de notifications quotidiennes sont désormais cliquables
- Pour la civilité, "Monsieur et Madame" devient "Madame et Monsieur"

### Corrections
- Suite à une recherche depuis le carnet d'adresse, l'organisme n'était pas modifiable
- Lors de l'import d'un organisme, le champ adresse complète n'était pas bien repris
- Suite à une recherche depuis le carnet d'adresse, le contact n'était pas modifiable

## [3.1.18] - 2023-10-09
### Ajouts
- [Recherche] Il est désormais possible de rechercher par plusieurs numéros de référence séparés par une virgule (ex: 203000123,2023000140)
- [Recherche] Il est désormais possible de supprimer ses recherches enregistrées
- [Recherche] Il est désormais possible de retrouver les flux adressés en copie
- Ajout d'un paramètre permettant (Affichage.ColonneSupForTreatment) d'afficher ou non une colonne supplémentaire, dans les bannettes de l'utilisateur, reprenant le nombre d'étapes restantes (si 1 seule restante = A clore)
- Ajout d'une colonne supplémentaire (affichable selon la variable (Affichage.ColonneSupForTreatment)), reprenant le nom de l'affaire à laquelle le flux est associée
- Lors de la création d'une réponse (flux sortant via Clore et répondre), une variable a été ajoutée permettant d'ajouter en copie, dans le flux réponse, les personnes ayant précédemment reçu le flux d'origine en copie

### Modifications
- [DM] Les informations sur les coordonnées et le SIREN n'étaient pas remontées au moment de la création d'un flux depuis DirectMairie
- [DM] Les coordonnées sont reprises dans un lien openstreetmap au cas où la collectivité souhaite exploiter l'information
- Ajout d'une variable permettant de décoder le body d'un mail en base64 si la longueur du body est > à 2000
- Le tableau récapitulatif des mails envoyés depuis l'application reprend désormais les mails adressés en copie
- Les contacts se voient ajouter le nom de la ville entre parenthèses dans le menu carnet d'adresses
- Dans le carnet d'adresse, on ne recharge pas la page une fois la modification du contact réalisée afin de laisser l'utilisateur sur la page de résultats
- En cas de refus sur un flux réponse, selon une variable, le flux refusé peut être redirigé vers le profil initiateur de l'agent ayant créé la réponse plutôt que vers l'initiateur du flux d'origine
- [Carnet d'adresses] Il est désormais possible de modifier le carnet d'adresses auquel l'organisme est associé
- Les liens présents dans les mails de notifications sont désormais cliquables

### Corrections
- Erreur dans le code en cas de clore et répondre, le code 'OK' était écrasé et passé à vide si la dernière étape est de type concurrente
- Par défaut, l'accès à la pop-up Information d'un dossier/affaire doit être autorisée
- Lors de l'ajout/édition d'un contact (via carnet ou via courrier), le rafraîchissement du code postal au moment de la sélection d'un organisme doit se faire uniquement si le champ banadresse du contact n'est pas nul
- La variable indiquant la connexion ciblée par le connecteur Pastell a été corrigée car elle était vide par défaut
- [Mail] La scrutation des mails posait souci si le contenu du corps présentait des commentaires (<!--) car cela empêchait le "désencodage" de l'objet.
- En cas de double refus dans le traitement le flux était perdu
- [Mail] Dans le cas d'Office 365, le déplacement des mails vers un répertoire d'"archive" doit être défini dans le connecteur et non pas avec "INBOX/" en préfixe dans le code
- [Mail] Si les mails sont encodés en windows-1258, la transformation de HTML vers PDF pose souci
- L'envoi d'un flux depuis l'écran d'historique, ajout d'une étape à la volée, posait souci
- En cas de flux Réponses à traiter, après insertion dans le circuit, les flux restaient dans la bannette des utilisateurs
- Si le champ Intitulé du flux possède un guillemet double ("), le nom est tronqué à l'affichage par le javascript
- Dans le cas où la métadonnée possède un guillemet double dans son nom, l'export CSV de la recherche plante
- [Carnet d'adresses] Lors de la modification d'un contact il était possible de l'associer à un organisme d'un autre carnet

## [3.1.17] - 2023-09-06
### Modifications
- [Gedooo] La variable "documents", insérable en tant que section dans un modèle de réponse, convertit désormais les champs de fusion définis dans un gabarit
- A l'enregistrement d'un contact ou un organisme, les intitulés des villes sont passés en majuscule
- Ajout d'une variable pour gérer les refus sur des flux réponses issus d'un flux d'origine
- Amélioration des résultats de recherche, notamment pour les flux sans sous-types qui ne ressortaient pas lors des recherches (ajout de la variable Configure::read('DisplaySearch.FluxWithoutSoustype'))
- Amélioration des messages d'erreur du script de récupération d'emails via oauth2
- Lorsque l'utilisateur clique sur clore et répondre, il est désormais possible de ne pas préfixer l'intitulé du flux réponse par "Réponse à" via la variable Configure::write( 'CourrierReponseName.AddReponseA', true );
- Amélioration de la recherche par services: désormais tous les flux passés par un profil du service sont retournés

### Corrections
- Lorsqu'un agent possède une délégation sur un compte, s'il accède à la modification d'un flux via l'historique global, l'identifiant du profil passé en paramètre était le sien alors qu'il doit s'agir de celui de l'agent qui a délégué son travail (celui qui possède le flux)
- La suppression des modèles de réponse et ARs associés à un sous-type n'était plus possible
- Lorsque l'administrateur déclot un flux (par lot ou unitairement), l'état des flux en copie n'était pas remis à zéro, ni les flux refusés (et sans bannette), ce qui bloquait le traitement une fois le flux déclot
- Les pièces jointes sont désormais versées à la GED
- L'administrateur ne pouvait plus déclore un flux clos
- [Pastell] Lorsque le script est exécuté en cron, la variable de connexion ($conn) utilisée en paramètre n'est pas récupérée correctement.
- Certains commentaires privés n'étaient pas visibles
- Dans le cas d'une recherche de flux par n° de référence, si ce dernier était dans une bannette en copie, il fallait saisir le N° de référence complet pour qu'il apparaisse dans les résultats de la recherche
- Au chargement des profils en session, on ne prend en compte que les profils actifs
- Si le gabarit de réponse associé à un sous-type ne présente pas d'extension .odt, il n'apparaît pas dans la gestion des éléments de réponse
- A l'édition d'un contact depuis le carnet d'adresse, s'il possède une simple quote dans son champ name, le javascript casse
- [Traitement par lot] Dans le cas d'une délégation, un même flux peut être amené à devoir être traités 2 fois par un même agent. La double sélection de ce flux via le traitement par lot ne traitait qu'une des 2 entrées
- [Déclore] Lorsque l'on déclot un flux, tous les acteurs sont repassés en agent traitant, or il ne doit y avoir que ceux liés à la dernière étape.

## [3.1.16] - 2023-07-17
### Ajouts
- Il est désormais possible d'envoyer un mail sans forcément devoir sélectionner un document
- L'agent peut désormais sauvegarder ses recherches (si la variable Configure::read('Display.SearchSave') est activée).

### Modifications
- [Pastell] Dans le cycle de vie du flux, si une étape est de Pastell est dans iParapheur, on affiche désormais le dernier message retourné par IP au lieu de la dernière action réalisée
- Les métadonnées renseignées lors de la création d'un flux sont sauvegardées de façon automatique, comme pour le traitement "normal" d'un flux
- Le nom des documents utilisés pour les gabarits et modèles de réponse sont désormais affichés par ordre alphabétique
- Dans le cas d'une réponse, les documents du flux d'origine sont désormais consultables dans la visionneuse depuis le flux réponse

### Corrections
- [BAN] Si le fichier CSV que l'on tente d'intégrer fait + de 1M de lignes, l'intégration rencontre un dépassement de mémoire et plante
- Le fichier SQL reprenant le schéma complet de la BDD avait une erreur suite à la sortie du patch 3.1.15
- Le document principal n'était pas supprimable même en tant qu'administrateur
- Les chemins vers les gabarits stockés sont erronés ce qui génère une erreur au moment du téléchargement
- Depuis l'écran d'historique du flux, le passage d'une pièce jointe en document principal ne rechargeait pas l'écran de visualisation du document
- Lors de la création d'un flux, la génération d'un modèle de réponse ne redirigeait pas l'agent vers l'écran de visualisation
- Le template de notification dans le cas des flux restés bloqués dans i-Parapheur n'était pas présent en base de données
- Lors de la création d'un nouveau flux, si un document de réponse est généré, la page ne se rechargeait pas pour afficher le document généré
- Les statistiques quantitatives et qualitatives posaient soucis sur le calcul des flux par services

## [3.1.15] - 2023-06-13
### Ajouts
- Lors de l'ajout d'un commentaire privé, les personnes associées peuvent désormais s'abonner à une nouvelle notification et recevront un mail indiquant qu'un nouveau commentaire leur a été adressé.
- [Outils de l'administrateur] L'administrateur se voit ajouter la possibilité de "déclore" un flux. L'action permet de revenir UNIQUEMENT à l'étape précédant la clôture.
- Afin de pouvoir remonter le bon email lors de la génération de la réponse (notamment celui du service intervenant), ajout d'une variable permettant de définir le mail du service au sein d'un sous-type
- Lors de la scrutation des mails, il faudra désormais définir un répertoire vers lequel les mails seront déplacés après traitement

### Modifications
- Le champ objet n'est plus limité à 90 caractères dans la page d'historique du flux (historique_global, visualisation uniquement)
- [webdav] Le chemin défini dans le core.php pour accéder au webdav pointe désormais sur $_SERVER['SERVER_NAME'] au lieu de $_SERVER['HTTP_HOST']
- L'administrateur peut désormais modifier un flux clos (ajout/modification d'un organisme, d'un contact, d'informations, ...)

### Corrections
- Les valeurs du ratio n'apparaissaient pas correctement sur le tableau des origines de flux (si la variable Configure::read('Display.RatioOrigineflux') est activée)
- [Outils de l'administrateur] Le lancement de la recherche de flux clos à supprimer, sans critères, ne fonctionnait pas
- Les gabarits attachés aux sous-types n'étaient pas modifiables
- La suppression des gabarits ne fonctionnait pas si le nom du gabarit était trop long car on se basait sur le contenu affiché (avec les ...) et on pas sur le title du gabarit
- Au moment de l'enregistrement d'un service, l'utilisateur n'est plus redirigé en haut de la liste mais reste là où il était positionné avant l'enregistrement
- Depuis le menu Carnet d'adresses, l'organisme est désormais par défaut à la valeur "Sans organisme"
- A la validation d'une étape collaborative (ET), si un des bureaux présents possèdent beaucoup de profils associés (>3), le détachement des flux ne se faisait pas sur les profils du bureau validant, hormis celui qui réalisait l'action
- L'origine du flux n'était pas récupérée lors de la génération d'une réponse
- [Pastell] En cas de droits d'accès bloqués, on renvoie un message d'erreur
- [Pastell] Le script ne fonctionne pas si on fait une vérification des informations de l'utilisateur en session dans le constructeur
- Lors de l'affichage du flux dans la visionneuse, le chemin de stockage du document était erroné, il manquait la n° de référence du flux
- Au moment de la sélection d'un organisme dans un flux, l'appel à la fonction permettant de remonter les informations des contacts associés se déclenchait trop "tôt" et générait une erreur
- Les carnets d'adresse privés n'étaient pas masqués pour les agents n'ayant pas les droits d'accès
- [Pastell] Lors de la définition d'un sous-type, à la sélection du cheminement dans Pastell, si un sous-type I-parapheur est sélectionné, il n'est pas possible de le désélectionner
- Lors de la création d'un flux, l'action Suivant/Enregistrer devenait inaccessible si on uploadait + de 3 documents dans le flux
- Lors de la définition de la composition d'une étape de circuit, les champs n'étaient pas obligatoires
- Lorsqu'un pdf possède des simples quote (') dans son nom, cela ne fonctionne pas au niveau du ghostscript pour l'affichage dans la visionneuse
- La valeur du créateur du flux est modifiée dès lors qu'une sauvegarde auto est déclenchée, car cette dernière ne renvoie pas l'ID du desktop_creator_id
- Le téléchargement des gabarits de documents (modèle de documents) ne fonctionnait pas. Désormais, les gabarits sont stockés sur le disque, dans /data/worksapce/coll/gabarits

## [3.1.14] - 2023-04-20
### Modifications
- Un paramètre est ajouté permettant d'indiquer si on souhaite bloquer la redirection une fois l'envoi en copie réalisé
  - Lors d'un envoi pour copie, on peut définir si la redirection se fait ou non via la variable Configure::write('ReloadPage.AfterCopy', true);
- Lors du ré-aiguillage, il est désormais possible de définir si le commentaire associé est privé ou non.

### Corrections
- Lors de la récupération des mails, si le sujet du mail contient + de 255 caractères, on limite la récupération à seulement 90 caractères
- [MAIl] Si l'encodage du mail est en windows-1258, les accents étaient transformés en caractères spéciaux
- Prise en compte de la compatibilité de version 5 du parapheur pour le connecteur direct
- [Mail] Dans le cas où le mail ne possède pas de HtmlBody(), on prend le textBody() et non pas le rawBody() pour les mails issus de outlook
- Les contacts nommés "Sans contact" ne doivent pas être modifiables
- [LDAP] Dans le cas où l'admin décide de surcharger les filtres sur les groupes et utilisateurs pour l'interrogation LDAP, les conditions n'étaient pas prises en compte pour chacun des cas.
	- Les conditions sur les groupes écrasaient celles des utilisateurs
- Dans l'historique, on se base sur la date de réception du flux dans la collectivité alors qu'il faut se baser sur la date de création en base
- Si le connecteur Pastell est activé, les liens reçus par mail rencontraient une erreur 500 empêchant la redirection vers la page d'accueil en cas de non authentification
- Les commentaires privés étaient masqués dans certains cas alors qu'ils ne devaient pas
- La visionneuse rencontre des blocages avec des PDFs en version supérieure à 1.4. On convertit via GhostScript afin de s'assurer du bon fonctionnement
- [Pastell] En cas de connexion échouée, les documents créés pour récupérer les sous-types IP n'étaient pas supprimés côté Pastell
- Les entrées "Sans organisme" et "Sans contact" ne doivent pas être modifiables
- Le ré-aiguillage ne se faisait pas correctement
- L'action "Mot de passe oublié ?" ne fonctionnait pas
- L'application du ratio défini selon l'origine du flux est mis sous condition via la variable Configure::write('Display.RatioOrigineflux', false );
- En cas de refus, on ajoute le flux qu'une seule fois dans la bannette "Mes flux refusés" de l'initiateur même si celui-ci est intervenu dessus plusieurs fois en amont
- Les gabarits liés aux sous-types ne pouvaient pas être téléchargés car le contenu se trouve désormais sur le disque et non plus en base de données
- [Pastell] Lorsqu'aucun connecteur IP n'est associé au document gfc-dossier, il doit tout de même être possible d'envoyer à la GED uniquement, cela n'était pas possible
- [Pastell] L'envoi à Pastell, hors mail sécurisé, ne peut pas se faire si aucun document n'est associé au flux
- En cas de refus d'un "flux réponse" inséré directement dans un circuit, donc sans passage par un initiateur, le flux était perdu, il est désormais positionné chez l'initiateur du flux entrant
- L'action de clôture et réponse ne fonctionnait pas si un envoi au mail sécurisé était fait en amont de la validation

## [3.1.13] - 2023-03-07
### Ajout
- L'administrateur se voit ajouter la possibilité de supprimer TOUS les flux de la base, notamment pour un passage en production.

### Corrections
- [Pastell] Prise en compte des erreurs de connexion au connecteur Pastell (absence d'entité, absence d'association entre les types de document et les connecteurs ...) afin de ne pas bloquer les utilisateurs
- Le champ Affaire suivie par remonte la bonne information lors de l'export CSV issu des recherches
- Lors de l'édition d'une origine de flux, le ratio mis en place n'était pas affiché correctement à l'édition
- [Pastell] Les données présentes dans le fichier flux_datas.json envoyé à Pastell étaient encodées 2 fois ce qui ne permettait pas de traiter les transformations par la suite
- Le "ratio" mis en place pour les origines de flux n'était pas appliqué dans le formulaire de gestion du flux et donc non pris en compte à l'affichage
- [LDAP] En cas d'absence de valeurs pour la partie targetDn, on récupère la valeur de la base Dn définie dans la config du connecteur, sinon la synchro des groupes ne fonctionne pas
- Lors de la scrutation de mails, si l'encodage fourni n'est pas bon, on force une conversion en utf-8 via iconv
- [Pastell] Les erreurs 500 remontées en cas de problème de connexion ou de problème d'authentification sur Pastell ont été corrigées
- Les flux clos sont désormais modifiables par tout agent étant intervenu au moins une fois dessus ou par un administrateur
- Les champs mail et mail en copie ont été passés en text car la limite de 255 caractères étaient trop limite
- Nettoyage du changelog (on conserve à partir de la 3.0.x)
- La fonction permettant de remplacer les caractères spéciaux retournait parfois 2 fois les e (créée => creeee)
- Une fois la recherche exécutée, les habilitations sur les types n'étaient pas totalement prises en compte.
  - Si l'agent possède un accès à un sous-type mais pas à son type "parent", les flux ressortaient tout de même dans la recherche
- Au moment du ré-aiguillage d'un flux, depuis un profil aiguilleur, les bannettes des aiguilleurs n'étaient pas vidées car seuls les initiateurs étaient pris en compte
- La fonction de changement de mot de passe par les utilisateurs ne fonctionnait pas correctement
- Lorsqu'on l'on souhaite utiliser les documents disponibles pour insérer dans le modèle, le téléchargement ainsi que l'ajout ne fonctionnaient pas
- Le champ objet, dans l'historique ou les bannettes d'environnement, se voit enlever les accents et limités à 10 caractères, ceci afin de ne pas "casser" le javascript
- [Outils de l'adminsitrateur] Dans le cas où l'admin souhaite traiter des flux via son menu Outils, l'objet peut poser soucis, du coup, on remplace les accents de ce dernier
- Le champ "Titre" était manquant dans le formulaire d'ajout d'un organisme du côté de l'administration


## [3.1.12] - 2023-01-05
### Corrections
- Le LDAP ne doit pas avoir la condition qui ajoute le "targetDn" en fin de test de connexion sinon aucune synchro possible
- Lors de l'envoi du mail quotidien reprenant la liste des flux (à traiter, en retard, refusés ...), les retours à la ligne sont perdus ce qui rend le mail difficilement lisible
- Afin de pouvoir récupérer le jeton de connexion du connecteur DirectMairie, le champ ne doit pas être obligatoire au moment du paramétrage
- Les bureaux inactifs apparaissaient toujours lors d'un ré-aiguillage
- Le chemin défini pour récupérer les gabarits de modèle de réponse prenait comme valeur le chemin /var/www/webgfc-3.x. et non pas le lien symbolique fixé.
  - le chemin est désormais fixé dans le webgfc.inc Configure::write('AppPathFix', '/var/www/webgfc' ); pour ne pas avoir de souci lors des montées de version.
- Au moment de la synchronisation LDAP, si 2 profils sont associés en parallèle à un utilisateur, un des deux bureaux nouvellement créés ne récupère pas l'information isautocreated = true

## [3.1.11] - 2022-11-03
### Modifications
- Suite à l'arrêt par Microsoft de l'authentification "basique" pour les boîtes mail, le connecteur mail se voit ajouter la possibilité d'interroger les serveurs dont l'autorisation se fait en oauth2, via jeton d'accès.

### Corrections
- Le script permettant d'extraire les gabarits (ContentToFile -G true) directement depuis la BDD ne retournait pas toutes les valeurs correctement
- Lors du chargement des bureaux, on exclut les bureaux créés automatiquement mais il faut conserver les bureaux PASTELL et Parapheur
- Lors de l'envoi à la GED, le numéro de dépôt stocké n'était pas bon
- A la connexion, la mise en cache plante si le connecteur Pastell et le connecteur de signature sont activés
- Lors de la modification du mot de passe du superadmin, l'encodage ne se faisait pas en sha256
- Le calcul du nombre de jours de retard ne se faisait pas correctement lorsque l'on applique le ratio dans les origines de flux
- Le service initiant le flux (service_creator_id) n'était pas repris correctement
- Lors d'une nouvelle installation, on constatait une perte des droits par moment, cela est lié au fait que les nouveaux profils ajoutés possédaient un ID identique à un des droits fournis par défaut (Valideur, Documentaliste ...).
  - Une simple remise à niveau de la valeur de l'ID du profil (desktops.id) corrige l'erreur.
- Le service principal fourni par défaut sur une nouvelle installation ne pose plus souci lorsqu'on le supprime ou le modifie car les données de la table daros (daros_dacos) ont été corrigés pour ne plus générer d'erreurs
- Le réaiguillage du flux se faisait toujours vers une banette d'initiateur, même si on sélectionnait un aiguilleur. Le flux était donc "perdu".
- Lorsque l'on clique sur le bouton Retour présent dans chaque menu de l'administration, on est désormais bien redirigé vers le menu précédent et non pas sur le premier menu d'administration


## [3.1.10] - 2022-09-13
### Corrections
- Le filtre de recherche "Agent étant intervenu sur le flux" retourne désormais bien tous les flux sur lesquels un agent est intervenu (même s'il ne le possède plus)
- [LDAP] La synchronisation LDAP ne fonctionne pas si la base DN est ajoutée au nom de domaine fourni (targetDn), au moment de la recherche LDAP (ldap_search)
- La civilité du contact n'était pas reprise lors de l'import via fichier CSV
- En cas d'un trop grand nombre de flux, le refus ne marche pas car l'identifiant de référence de l'ancien flux est de trop grande taille (> 32767 taille limite du smallint)
- Les flux en copie ne sont plus aiguillables depuis l'environnement de l'aiguilleur, seulement adressables en copie
- Les flux en copie ne doivent pas être ré-aiguillables
- Au moment du ré-aiguillage, certaines bannettes n'étaient pas vidées car seul le bureau principal de l'initiateur ré-aiguillant était pris en compte
- Dans l'historique, la traduction du mot "days" n'était pas faite
- Suite à la mise à jour de LibreOffice en version 7.3.3.2, les caractères spéciaux et accentués posent soucis dans les noms des modèles de documents lors de l'utilisation du webdav.
  - Les ARs voient leur préfixe "[AR]_" remplacés par "AR_"
- La délégation posait souci sur les profils secondaires. Désormais un administrateur peut supprimer toutes délégations, quelque soit le profil.
- L'export CSV des bureaux ne fonctionnaient pas correctement
- Lors de l'édition d'un bureau, la liste déroulante permettant de définir le profil était parfois vide car aucun rôle n'était précédemment associé donc on ne récupérait aucune valeur
- Si l'utilisateur est abonné aux mails de notifications de façon quotidienne, si aucune notification n'est créée, l'enregistrement quotidien ne se fait pas

## [3.1.9] - 2022-06-23
### Corrections
- Mise à jour des droits dans le template SQL
- La fonction getAdresses permettant de charger les adresses de la BAN n'a pas besoin d'être placée dans les droits
- A la génération d'une réponse, prise en compte des caractères spéciaux dans les initiales de la personne enregistrée dans la liste déroulante "Affaire suivie par"
- Prise en compte des images stockées en base64 de façon multiples à l'intérieur du mail
- Mise en paramétrage du nombre de secondes avant déclenchement de la sauvegarde automatique
- Lors de l'envoi d'un document par mail, ajout d'une action permettant de charger automatiquement l'adresse mail du contact
- Mise en paramétrage du choix permettant de définir si on souhaite afficher les bureaux créés automatiquement dans les différentes listes déroulantes disponibles
- Mise en paramétrage du choix permettant de définir si on souhaite utiliser ou non la sauvegarde automatique du formulaire du flux
- Mise en paramétrage du choix permettant de définir si on souhaite récupérer le contenu du champ objet lors du scan des mails
- Les retours à la ligne étaient mal interprétés dans l'affichage des commentaires
- Au moment de l'envoi à Pastell, le json reprenant les métadonnées ne se créait pas côté PASTELL si le document visé n'était pas nommé "gfc-dossier"
- L'URL est encodée proprement pour pallier les soucis d'affichage des documents dans la visionneuse

## [3.1.8] - 2022-05-13
### Corrections
- Rectificatif pour la valeur du champ objet lors de l'accès à l'environnement de l'agent.
  - On remplace les accents APRES avoir réduit le champ objet à 10 caractères
- [MAIL] Lors de la scrutation des mails, le document reprenant le contenu du mail ne se créait pas si le sujet présente des caractères spéciaux, du type « ou »
- Lors de l'envoi d'un document par mail, si le sujet possède une variable du type #NOM_FLUX" ou autre, cette variable n'était pas remplacée car non prise en compte
- [CAS] Pour que la connexion au CAS fonctionne (sans LemonLdap ou tout autre application), des fonctionnalités ont dû être corrigées et améliorées
- Les documents associés à un flux supprimés via le traitement par lot (Outils de l'administrateur) sont supprimés du disque
- Au moment du refus, l'id de la notification n'était pas stocké et donc la notification non envoyée et l'enregistrement du refus bloqué.
- Dans le cas où la dernière étape possède une étape concurrente (OU), le flux est désormais traité pour tout le monde lorsque la clôture est déclenchée
- Les commentaires privés apparaissaient chez certains agents alors qu'il ne fallait pas
- Le problème de perte de droits lors de l'association d'un utilisateur à un nouveau service (depuis l'écran du service) a été corrigé
- Lors de l'envoi pour copie, l'envoi dans un circuit ou le ré-aiguillage, les champs obligatoires non renseignés ne bloquaient pas le formulaire et cela générait une erreur
- Les messages ont été corrigés lors du test de connexion au connecteur i-Parapheur

## [3.1.7] - 2022-04-20
### Corrections
- Les fichiers en extension et au format .htm bloquaient l'envoi au i-Parapheur (comme les zip)

## [3.1.6] - 2022-03-24
### Corrections
- [Pastell] Si plusieurs connecteurs de type signature sont présents du côté de PAstell, le type sélectionné était le 1er trouvé ... sans regarder les associations de connecteur.
  - Désormais, on regarde uniquement le connecteur de signature associé au flux gfc-dossier
- [Pastell] Les données envoyées en json vers Pastell n'étaient reprises dans le champ "metadonnees"
- Lors d'un flux réponse, s'il est refusé, il est perdu car aucun initiateur n'est récupéré
- Si les commentaires présentent des "line break" (retour à la ligne), l'affichage ne se fait pas et le javascript "casse"
- [MAIL] Les images intégrées directement dans les mails ou en pièces jointes n'étaient pas reprises dans les documents créés pour les flux générés
- [MAIL] Le contenu HTML des mails récupérés a été corrigé car ne se faisait pas pour chaque mail
- [MAIL] Le header dans le corps du pdf ne reprenait pas tous les éléments requis à l'affichage

## [3.1.5] - 2022-03-09
### Modifications
- Création de la base de données webgfc_template lors du script AdminTools install afin que chef puisse faire son travail

## [3.1.4] - 2022-03-07
### Corrections
- Le modèle Connecteur n'était pas chargé lorsque l'on modifie un flux ce qui générait une erreur
- A l'enregistrement d'une modification de bureau, les valeurs des profils déjà enregistrés n'étaient pas prises en compte.
- Lors de l'envoi d'un document par mail, les retours à la ligne étaient perdus pour les mail-types


## [3.1.3] - 2022-03-01
### Corrections
- A la création d'un flux (via scan de documents, de mail ou de script), le profil du créateur n'est pas tout le temps renseigné ce qui bloque les refus
- Le refus est par moment adressé au mauvais profil de l'agent ce qui entraîne un blocage.
  - Le desktop_creator_id prend par moment le profil_id du valideur et non pas celui de l'initiateur car les valeurs stockées en session ne sont pas prises en compte au bon moment


## [3.1.2] - 2022-02-14
### Corrections
- GRC Localeo: lorsque la GRC Localeo crée un nouveau flux, elle envoie parfois un contactId = -1.
  - Du coup, les flux se voient créés temporairement et leur n° de référence utilisé puis supprimé ce qui crée un décalage dans les IDs et les n° de référence par la suite
- L'administrateur ne pouvait se déléguer un profil vers son propre environnement
- Lors de l'envoi de documents par mail, les PJs ne sont pas toujours prises en compte si les noms présentent des caractères spéciaux
- Le script de mise à jour des BAN adresses ne mettait pas correctement à jour les entrées en base, le format des données fourni ayant évolué

## [3.1.1] - 2022-01-24
### Corrections
- Une fois les documents scannés, on déplace désormais les fichiers dans un répertoire "tampon" nommé **inprogress** pour s'assurer que chaque flux généré possède bien un document associé sinon des flux se retrouvent "vides"
  - Uniquement si la variable Configure::write('Scan.UseFolderInprogress', false); est à true
- Les sous-types désactivés ne sont plus sélectionnables pour l'action "Clore et répondre", ni pour la sélection des sous-types de référence
- Le délai de traitement ne se mettait pas à jour lorsque l'on changeait le sous-type d'un flux
- Le sens des flux (entrant/sortant) ne se mettait pas à jour lorsque l'on changeait le sous-type d'un flux
- Le composant utilisé pour Pastell ne doit aps être chargé si le connecteur n'est pas, utilisé.
- À la récupération des informations du circuit, en cas de suppression d'une étape du circuit, on continue d'afficher l'information pour les flux en cours de traitement pour savoir où se trouve le flux
- Plus aucun fichier zip n'est adressé au IP lors de l'envoi d'un document et ses pièces jointes
- DirectMairie: Lors de la recherche d'une demande, si le n° n'existe pas du côte DM, une page d'erreur apparaissait

## [3.1.0] - 2022-01-07
### Ajouts
- Ajout de la possibilité d'envoyer à PASTELL directement depuis la gestion du flux. IL faut pour cela avoir défini un flux studio du côté Pastell
- Lors d'un envoi vers Pastell, les cheminements possibles sont :
    - Visa/Signature -> on définit un sous-type IP dans le circuit
    - Visa/Signature - Mail Sécurisé -> on définit un sous-type IP dans le circuit
    - Cheminement défini dans le sous-type -> on coche les cases dédiées au cheminement dans le sous-type, et on y définit le sous-type IP
    - Etape à la volée :
        - Si on n'a pas défini de cheminement personnalisé : tous les choix sont laissés à l'utilisateur, qui devra renseigner un sous-type IP si l'étape est de type "Visa/Signature"
        - Si on a défini un cheminement personnalisé : seul ce cheminement est disponible, et s'il y a une étape IP, elle a été définie par l'administrateur dans le sous-type
- Ajout dans le cycle de vie du flux de l'état des documents dans Pastell sur la ligne dédiée à Pastell
- Il est désormais possible de définir plusieurs superadmin sur une instance de web-GFC (environnement super-administrateur)
- Une API pour tester l'impression des ODT a été ajoutée
- Au niveau des métadonnées, on pourra définir si on souhaite répercuter automatiquement la valeur de cette métadonnée d'un flux entrant vers son flux sortant associé
- Au niveau de la délégation, on pourra désormais savoir quel utilisateur a réalisé l'action à une étape donnée
- Lorsqu'un commentaire vient d'être créé dans un flux, l'utilisateur accédant au flux verra une pop-up s'afficher, indiquant qu'un nouveau commentaire est disponible
- Un nouveau paramètre est ajouté pour que l'administrateur puisse choisir la limite pour l'affichage du nombre de flux dans l'historique (par défaut -1 mois)
- Ajout dans l'historique d'un message indiquant que les flux affichés pour chaque profil ne sont que ceux datant de moins de 1 mois
- Ajout de la possibilité de ré-aiguiller un flux directement depuis le formulaire du flux (uniquement pour les initiateurs)
- Ajout du replyTo lors de l'envoi d'un document par mail
- Ajout dans le menu Outils de l'administrateur de la possibilité de supprimer des flux: par n° de référence ou sur une plage de dates donnée
- [RGPD] Ajout de la notion de consentement au sein même du journal d'événements lorsque l'agent coche/décoche la case d'acceptation des notifications.


### Modifications
- Depuis le profil aiguilleur, l'action d'insertion dans un circuit ne sera pas visible dans la liste des actions mais elle sera remplacée par la possibilité d'aiguiller directement depuis la gestion du flux.
- Lorsque l'on fait un export CSV en recherche, la boite de dialogue se ferme désormais dès lors que le fichier d'export est retourné
- Il est désormais possible de définir des variables pour les mails-types envoyés depuis la gestion du flux, de même que pour les notifications par mail
- Dans la gestion des bureaux, on masque désormais les bureaux créés de façon automatique à la création d'un agent (notamment via la synchronisation LDAP) afin de ne pas être "pollué"
- Lorsque l'on consulte un flux et qu'on clique sur détacher de la bannette des flux en copie, l'agent est redirigé vers l'environnement principal de l'utilisateur
- Lors d'un envoi à la volée vers Pastell, on fait comme pour IP, on ne force pas le retour, sauf si c'est une dernière étape
- A l'activation du connecteur Pastell, on active automatiquement le connecteur IP en le positionnant sur le protocole PASTELL
- Il n'y a plus de connecteur Mail Sécurisé, la connexion se fait désormais directement dans le connecteur Pastell.
- Le connecteur IP garde le choix de la connexion (direct ou Pastell) mais aucune autre information n'est à renseigner si on choisit Pastell.
- Le connecteur Pastell se voit ajouter les infos du type de doc pour IP + le choix d'activer ou non l'envoi via le mailsec.
- Enfin, le type IP est directement récupéré depuis Pastell
- Avant envoi au mail sécurisé, si le contact ne possède pas de mail, l'action de validation est grisée.  Au moment de la sélection du contact ou de sa modification afin de li associe run mail, on vérifie et on active l'action si tout est ok
- Les bureaux Pastell et Parapheur (fournis par défaut dans l'application) ne sont visibles que si les connecteurs associés sont activés.
    - A noter également que si le connecteur Pastell est activé, le bureau Parapheur est automatiquement masqué.

### Corrections
- Renommage des fichiers envoyés par mail dans le cas où des caractères spéciaux sont présents dans le nom de ces fichiers.
- Amélioration du temps de traitement, avec notamment la mise en cache des données issues de Pastell.
- Amélioration de la récupération des contenus et pièces jointes des mails.
- Certains formulaires ne présentaient pas de blocages sur les champs obligatoires
- Intégration des BANs, la taille du champ "ident" de la table _bansadresses_ a été modifié car la BAN a vu sa valeur augmentée.
- Au moment où on crée un commentaire privé, la liste des agents affichés reprenait les profils alors qu'il faut que ce soit le nom des agents
- Le clic sur les lignes des bannettes ne "répondait" plus car une action (location.reload()) faisait obstruction au traitement
- Lorsque l'on recherche les adresses associées à une commune, la fonction envoyant les données ne gérait pas correctement la présence d'un proxy


## [3.0.14] - 2021-12-17
### Corrections
- Lors de l'envoi d'un mail, le replyTo est activé par défaut alors qu'il faut pouvoir le conditionner selon la collectivité.

## [3.0.13] - 2021-11-16
### Corrections
- Lors de la scrutation par mail, la récupération des documents posait soucis à cause des caractères accentués
- L'envoi par mail d'un document PDF avec des accents ou caractères spéciaux ne se faisait pas
- L'envoi d'un AR généré en ODT se fait en PDF mais ce dernier était encore vu comme un ODT ce qui empêchait son ouverture dans le mail adressé
- Lors de l'ajout d'un flux, les PJs passaient en doc principal sans raison valable

## [3.0.12] - 2021-08-06
### Corrections
- [API] Les limites et offset n'étaient pas pris en compte lors de la requête en GET
- [API] L'action PUT n'était pas prise en compte pour les collectivités
- [API] Les retours des appels sur les rôles et les services étaient erronés (text/html au lieu de application/json)
- [API] La suppression d'un profil directement lié à un utilisateur (profil principal) n'est pas possible et un message d'alerte est remonté
- [API] Un utilisateur possédant le même username/nom/prénom ne peut être créé deux fois


## [3.0.11] - 2021-06-??
### Corrections
- Un warning apparaissait lors du déclenchement de la création du courrier depuis la GRC car l'envoi du json n'est pas possible ici
- Lors de la synchronisation entre la GRC et webGFC, le script ne trouvait pas les tables liées à la collectivité cible
- Si aucun document n'est présent lors de l'envoi depuis la GRC, une erreur est remontée vers la GRC ce qui entraine un blocage
- Le script de notification de flux envoyés au IP, mais non encore traité depuis 7 jours, a été corrigé car les notifications n'étaient pas bien adressées
- Lors de l'accès aux bannettes, si le flux comporte un objet avec des caractères spéciaux, l'affichage est cassé
- Lors de la recherche par valeurs de métadonnées, la combinaison de plusieurs valeur des métadonnées retournait des résultats erronés
- L'enregistrement du service à la création du flux ne retournait pas de résultats dans certains cas

## [3.0.10] - 2021-05-28
### Corrections
- Les quote étaient mal encodées lors de l'affichage de l'objet dans les bannettes des agents
- Lors de l'édition via webdav d'un AR, la variable $conn n'était pas envoyée à la vue pour la fonction getArs donc l'édition ne fonctionnait pas
- Lors des échanges avec IP, suppression du fsockopen sur le port 80 qui n'a plus lieu d'être et surtout qui bloque les échanges si le port 80 est bloqué
- La variable $desktopIdUse n'était pas déclarée par défaut ce qui générait un undefined index
- Le script de mise à jour des sous-types issus du i-Parapheur ne prenait pas en compte la table wkf_visas ce qui bloquait les traitements des flux passés/en cours (pas les nouveaux)

## [3.0.9] - 2021-05-21
### Corrections
- [API] Une limite à 10 persistait sur les profils et services ce qui limitait les résultats retournés
- [API] Le component API ne reprenait pas la bonne clé d'API (globale à l'application) dans le cas du superadmin
- Lors de la recherche par sens du flux + avec ou sans réponse, la requête était erronée
- L'océrisation via cron était bloquée dans le cas où la variable Ocerisation.active n'était pas à true mais cela ralentissait le système donc le cron se voit supprimer cette variable par souci d'efficacité
- Les actions disponibles depuis l'écran d'historique étaient mal gérées. Désormais, si on vient de l'historique, on regarde le profil présent en session pour passage à l'étape suivante. De plus, les actions ne sont affichées que si l'acteur est bien celui en cours d'action sur le flux (bancontenus.etat=1)
- Afin de pouvoir valider un flux après l'avoir édité depuis l'écran d'historique, il faut ajouter l'ID du profil réalisant l'action sinon il ne se passe rien
- L'appel sur le port 80 ne doit plus être utilisé lors de la connexion entre web-GFC et le i-Parapheur

## [3.0.8] - 2021-05-03
### Corrections
- La description de l'étape n'était plus reprise, ni affichée dans la gestion du flux
- Le paramètre prenant en compte le proxy pour les requêtes Curl n'était pas correctement défini
- La visionneuse était décalée vers le bas lors de l'accès au flux, ce qui oblige l'agent a scrollé pour voir le nom du document
- L'Océrisation ne marchait pas tout le temps, notamment pour les documents dont le nom possède des espaces et caractères spéciaux
- La redirection ne se faisait pas tout le temps lors de la génération d'un flux depuis Démarches Simplifiées ou Direct Mairie
- L'utilisateur et le rôle administrateur ne sont plus modifiables via l'API


## [3.0.7] - 2021-04-16
### Corrections
- Les flux récupérés de i-Parapheur peuvent avoir des noms avec des apostrophes ou autres caractères spéciaux, donc on les gère désormais en connaissance de cause
- Lors de la scrutation par mails, la fonction wkhtmltopdf ne se lançait pas correctement car le chemin de la librairie n'était pas le bon, cela ne marchait que lancé manuellement.
- Lors de la scrutation par mails, si le mail possède un bodyHTML présentant un charset base64 alors qu'il n'est pas encodé en base64 alors la pièce jointe reprenant le contenu du mail n'était pas générée
- Les actions possibles sur le flux, visibles depuis l'historique, ne le sont désormais que pour les admins ou les agents ayant le droit d'intervenir sur le flux (acteurs du circuit du flux) car elles apparaissaient tout le temps.
- Océrisation: le script a été corrigé afin d'être positionné en cron et exécuté la nuit car trop gourmand en performance
- LDAP: le prénom devait être encodé en UTF-8 parfois mais cela générait des doublons lors de la synchrnisation
- Lors de l'exécution par lots, l'action cakeflowExecuteSend ne reprenait pas la bonne valeur pour le profil passé en paramètre (desktopId) car on se basait sur l'URL en cours (ce qu'il n' pas lieu d'être pour le traitement par lots)
- Lors de l'export CSV de la recherche, le service et la direction n'était pas remontés et le champ objet était mal formaté pour les mails
- Lors de l'export CSV de la recherche, les dates de validation et de clôture étaient vides

## [3.0.6] - 2021-04-02
### Corrections
- Lors de la recherche, on ne prenait pas en compte les courriers dont le sous-type n'était pas encore défini
- Lorsque le i-Parapheur interrogé est inaccessible, le système est bloqué jusqu'au timeout défini. Du coup, un contrôle est ajouté pour rectifier cette erreur
- Lors de la recherche par n° de référence, la recherche "cassait" en cas d'agent connecté ne possédant pas de profil admin
- Dans les bannettes de l'environnement utilisateur, la colonne Bureau a été corrigée car elle n'affichait pas les bonnes informations
- Si l'adresse ou le nom de voie du contact/organisme possède une simple quote, (ex: rue de l'église) alors une erreur apparaît. Du coup, un sanitize::clean permet d'éviter cette erreur
- L'export CSV de la recherche ne retournait pas le nom du service, ni l'état du flux alors que les colonnes sont bien présentes
- L'API gérant les profils ne présentait pas certains champs indispensables à la modification depuis l'extérieur
- Lors de la récupération des mails, ceux dont le format n'est pas forcément en HTML ne remontaient aps correctement
- A la création du flux, si le service créateur n'est pas présent, on ne le met pas à jour, désormais on prend celui de l'initiateur agissant dessus
- Les actions de traitement du flux (validation, refus, envoi) n'apparaissaient plus dans l'historique du flux


## [3.0.5] - 2021-03-10
### Corrections
- Le bouton d'Envoi à la GRC apparaît alors que le connecteur est inactif
- Lorsqu'un agent est associé à plusieurs services, seul le premier retourné est disponible pour la variable service_email lors de la génération de la réponse
- Lors de l'édition en ligne du fichier ODT, la visionneuse n'est pas mise à jour car le contenu du fichier (désormais stocké sur le disque dans /data/workspace) n'était pas mis à jour par le webdav
- Lors d'une réponse à un flux, la date stockée en base (date réelle pour la prise en compte des retards) reprenait la date du flux d'origine ce qui posait souci en terme de cohérence (contrôle) entre la date d'entrée et la date d'enregistrement, la date d'entrée ne pouvant être postérieure à la date d'enregistrement
- La variable pour la concaténation des PDFs était par défaut en read au lieu de write
- Lors de l'appel des webservices du i-Parapheur, la vérification des certifications SSL (CURLOPT_SSL_VERIFYHOST) ne se faisait pas correctement
- Suite au stockage des fichiers sur le disque et du mode multi collectivités, il manquait le nom de la collectivité dans le lien webdav qui permet d'éditer le document
- Le script bloqueparapheur, permettant de notifier l'admin en cas de dossier inactif dans le i-Parapheur depuis + de 7 jours a été corrigé pour prendre en compte plus précisément les jours renseignés et non pas une date erronée
- Le template SQL posait souci sur la table acos car la séquence ne repartait pas après la dernière valeur utilisée (à savoir 1873) mais à 1093 ce qui entrainait des doublons d'enregistrement et cassait la script de mis eà jour des droits des nouvelles collectivités créées (update_rights)
- Au moment de la récupération des mails, si l'objet ou le nom du mail possède un " alors l'affichage est cassé, du coup on les remplace par des espaces  ou des _
- Une faille dans l'API a été corrigée. L'API se voit récupérer le token depuis la BDD sans possibilité de lecture depuis l'extérieur
- Le nom de la bannette n'était plus récupérable dans le cycle de vie du flux
- Le champ objet a été enlevé des colonnes de la bannette des flux à aiguiller car était cause d'erreur alors qu'il n'est pas affiché
- Correction sur la recherche qui était beaucoup trop lente

## [3.0.4] - 2021-02-18
### Modifications
- Lorsqu'on accède à la visionneuse, il est désormais possible de concaténer automatiquement les PDFs présents dans le flux (pièce principale + PJs) si la variable _Concatenation.Pdf_ est activée.
- Lorsqu'on sélectionne le sens du sous-type, on affiche le sous-type de référence uniquement quand le flux est sortant. De plus, le message indiquant le sens du flux dans le nom du sous-type a été enlevé.
- Suppression du champ obligatoire sur le délai de traitement d'une étape de circuit car non utilisé
- Les pré-requis se voient ajouter un contrôle sur les répertoires de stockage des documents, par collectivités, ceci afin de s'assurer que les répertoires cibles existent bien
- Dans le cycle de vie du flux, l'agent ayant réalisé le refus est désormais matérialisé par une ligne en bleu et italique
- Chaque flux refusé voit son motif de refus stocké en tant que commentaire public, ceci afin de conserver l'historique des différents refus

### Corrections
- Le formulaire d'édition des sous-types ne s'affichait pas correctement
- Lors de la délégation d'un agent depuis son propre compte, une erreur apparaît car les contrôles de contraintes étaient mal gérés depuis cet écran
- La récupération des informations des organismes et des contacts prenait trop de temps lors de l'accès au flux, du coup la recherche des informations a été améliorée
- Lorsqu'on utilise la fonctionnalité de concaténation des fichiers pdfs dans un seul et unique PDF, si un fichier autre que PDF est présent, cela bloquait le traitement


## [3.0.3] - 2021-01-27
### Modifications
- Passage en 2021 en pied de page
- Le script permettant l'import des contacts a été modifié pour être plus générique
- Les fichiers de gabarits, liés aux sous-types, se voient remplacer les espaces vides dans leur nom par des underscores
- La colonne reprenant le SIRET a été corrigé dans le script d'import des contacts
- Lors de la création d'un nouveau flux, le panneau latéral permettant d'associer un document, ne s'affiche qu'à partir de l'étape 2

### Corrections
- Le calcul des retards affiche J +18651 si le nombre dans le délai de retard du flux est laissé à vide
- Mise à jour du template SQL pour les habilitations du profil Valideur
- Si l'objet du flux présente la balise <html> en premier, on passe l'objet à null pour l'affichage des bannettes
- Le chemin permettant d'utiliser les documents à éditer via la webdav a été corrigé avec l'ajout de la connexion utilisée
- Les fichiers de gabarits, liés aux sous-types, se voient ajouter automatiquement l'extension .odt si la valeur n'est pas présente

## [3.0.2] - 2021-01-15
### Modifications
- Recherche: Le filtre sur les flux possédant ou non une réponse a été amélioré
- LDAP - lors de la rcherche dn d'un groupe, on passe le dn en encodage utf-8 pour ne pas se retrouver bloqué lors de la récupération des groupes

### Corrections
- Recherche: Le filtre sur le sens du flux a été corrigé
- Lors du traitement par lot, on recharge désormais la page car la redirection ne se fait pas
- L'ajour d'un Dossier/Affaire s'appuyait sur la table compteurs, ce qui n'est désormais plus le cas
- Lors de la sélection du profil dans la gestion des bureaux, une erreur apparaissait si la liste était vide

## [3.0.1] - 2021-01-08
### Ajouts
- L'API v2 se voit ajouter la possibilité de créer une collectivité avec sa BDD et son compte administrateur

### Modifications
- L'API v2 a été améliorée, notamment pour la création des profils utilisateurs
- Le champ objet est limité aux 90 premiers caractères pour ne pas "casser" l'affichage des bannettes
- Remplacement du terme Rôle par Bureau
- Amélioration des conditions pour les appels au I-Parapheur, notamment pour le check et ajout de la condition use_signature = true pour s'assurer que le Parapheur est actif ET utilisé
- Le script d'installation peut être lancé autant de fois que l'on souhaite, la première fois la collectivité est créée, ensuite il ne se passe rien

### Corrections
- Sous Firefox, les icônes font-awesome apparaissaient en bleue au lieu de blanc
- Dans le cas où l'utilisation du I-Parapheur est désactivée, la variable $soustype ne prend pas comme valeur $soustype['soustype']
- Lors de l'intégration des mails, suppression de la condition sur le contenu HTML du mail comme pour le cas où aucune PJ n'est présente sinon cela ne retourne pas les données correctement
- Lors de l'OCérisation, on fait en sorte que le nom du fichier ne comporte pas d'espace, que l'on remplace par des _
- Lorsque l'on clique sur le lien de l'application fourni dans les notifications par mail, si l'utilisateur n'est pas connecté, alors une erreur apparaît (page blanche en debug = 0). Désormais, on est redirigé vers la page de connexion


## [3.0] - 2020-12-11
### Ajouts
- Ajout: passage sous Serveur Ubuntu 20.04, PHP 7.4 et CakePHP 2.10.22
- Ajout: possibilité d'utiliser l'OCérisation des fichiers scannés avant intégration dans web-GFC
- Ajout: possibilité d'envoyer des mails sécurisés en passant par la plateforme PASTELL
- Ajout: les documents liés aux flux sont désormais stockés sur l'espace disque et non plus en base de données
- Ajout: interconnexion de web-GFC avec le service Démarches-simplifiées fournies par la DINUM (https://www.demarches-simplifiees.fr/)
- Ajout: interconnexion de web-GFC avec le service Direct-Mairie fournies par l'ADULLACT (https://directmairie.adullact.org)
- Ajout: possibilité d'envoyer plusieurs documents principaux (pouvant être signés) au i-Parapheur. (Ceci est en lien avec les dossiers multi documents principaux disponibles depuis la version 4.3 du i-Parapheur)
- Ajout: l'administrateur se voit désormais offrir la possiblité de "déclore" un flux, i.e., ajouter/modifier une métadonnée, un commentaire, un dossier/affaire
- Ajout: la liste des acteurs possédant le flux actuellement est ajoutée dans les bannettes de recherche (et l'export CSV) et de l'historique
- Ajout: un nouveau menu Outils est ajouté permettant à l'administrateur :
    - de retrouver les flux non clos présents chez des agents inactifs
    - de retrouver un flux non clos qui est à l'étape i-Parapheur et qui doit être ré-injecté dans le i-Parapheur (car perdu ou mal aiguillé)
    - de retrouver les flux en cours de traitement qui sont toujours dans la bannette des flux à insérer des initiateurs (alors qu'ils sont bien dans un circuit)
- Ajout: récupération du nom et du mail du service associé à la personne définie dans la liste déroulante "affaire suivie par"
- Ajout: possibilité d'envoyer des SMS depuis la gestion du flux
- Ajout: en lien avec le RGPD, ajout d'une page de politique de confidentialité, paramétrable par collectivités
- Ajout: prise en compte des ActiveDirectory en LDAPs, suite à la préconisation du pôle système et la mise en oeuvre sous Windows du LDAPs
- Ajout: de nouveaux tests unitaires sont pris en charge
- Ajout: dans l'export des types/sous-types, ajout d'une colonne reprenant le nom du circuit associé au sous-type

### Modifications
- Modification: le script d'intégration des données de la BAN (Base d'Adresse Nationale) se voit ajouter la possibilité de mettre à jour les données intégrées
- Modification: la recherche se voit ajouter le filtre "Sens du flux"
- Modification: ajout du nombre de jours de retard, ou du nombre de jours avant retard si un délai de traitement est défini, sur toutes les bannettes des agents (en bleu, le nombre de jours avant retard, en rouge le nombre de jours en retard)
- Modification: lors de la consultation d'un flux, le panneau latéral de droite est fixé à la page, ceci afin que l'on continue de pouvoir consulter le document au fur et à mesure que l'on descend dans la page.
- Modification: lors de la génération documentaire, le nom et l'e_mail repris pour le service concerné par le flux sont ceux du service associé à la personne sélectionnée dans la liste déroulante "Affaire suivie par", si cette dernière valeur est renseignée.
- Modificaion: un flux ne peut plus être inséré dans un circuit désactivé
- Modification: lors de l'envoi du document principal dans l'i-Parapheur, le numéro de référence du flux est ajouté dans le nom du dossier créé côté i-Parapheur

### Corrections
- Correction: la recherche par valeur de métadonnées n'était pas correctement définie
- Correction: lors de la synchronisation LDAP, le cas des associations bureaux/profils n'était pas prise en compte et une erreur de contrainte unique "cassait" le script de mise à jour LDAP. Les nouveaux utilisateurs n'étaient du coup pas redescendus
- Correction: [GRC Localeo] lorsque le contact doit être créé dans la GRC depuis web-GFC, on vérifie que les données sont bien présentes avant envoi vers la GRC
- Correction: lors de la notification aux utilisateurs, si parmi les profils cibles, l'un d'entre eux est inactif, alors on ne le notifie plus
- Correction: lors de l'envoi au i-Parapheur, si le document de réponse vient tout juste d'être généré, il n'est pas pris en compte par l'interface ce qui bloque l'envoi. Désormais, on stocke en session l'id du document qui vient d'être généré, si la page n'est pas rechargée avant l'envoi
